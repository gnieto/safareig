use std::{str::FromStr, sync::Arc};

use clap::Parser;
use safareig::{federation::worker::Checkpoint, storage::CheckpointStorage};
use safareig_core::{
    command::FrozenContainer,
    id::RoomIdExt,
    ruma::{
        api::federation::{
            event::get_event::v1 as get_event,
            query::get_room_information::v1 as get_room_information,
        },
        EventId, OwnedEventId, OwnedRoomAliasId, OwnedRoomId, OwnedServerName, RoomId, ServerName,
    },
    storage::StorageError,
    StreamToken,
};
use safareig_federation::{
    client::{resolution::ServerDiscovery, ClientBuilder},
    server::storage::{ServerState, ServerStorage},
    FederationError,
};

#[derive(Parser, Debug)]
pub enum FederationSubCommand {
    ServerStatus(ServerStatusParams),
    Event(FederationEvent),
    ServerResolution(ServerResolution),
    Alias(AliasParam),
}

#[derive(Parser, Debug)]
pub struct FederationEvent {
    room_id: OwnedRoomId,
    event_id: OwnedEventId,
}

#[derive(Parser, Debug)]
pub struct AliasParam {
    alias: OwnedRoomAliasId,
}

#[derive(Parser, Debug)]
pub struct ServerResolution {
    server_name: OwnedServerName,
}

#[derive(Parser, Debug)]
pub struct ServerStatusParams {
    /// Target server
    server_name: OwnedServerName,

    /// Change status server status
    #[clap(short = 's', long)]
    new_state: Option<NewServerState>,

    /// Advance checkpoint to this stream token
    #[clap(short = 'p', long)]
    new_pdu: Option<u64>,
}

#[derive(Debug)]
pub enum NewServerState {
    Online,
    Offline,
}

impl FromStr for NewServerState {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_str() {
            "offline" => Ok(NewServerState::Offline),
            "online" => Ok(NewServerState::Online),
            _ => Err("could not parse new server state".to_string()),
        }
    }
}

pub async fn handle_federation_command(
    command: FederationSubCommand,
    container: &FrozenContainer,
) -> Result<(), anyhow::Error> {
    match command {
        FederationSubCommand::Event(event) => {
            federation_event(&event.room_id, &event.event_id, container).await?;
        }
        FederationSubCommand::ServerResolution(server) => {
            server_resolution(&server.server_name, container).await?;
        }
        FederationSubCommand::ServerStatus(status) => {
            if let Some(new_state) = status.new_state {
                change_server_state(&status.server_name, new_state, container).await?;
            }

            if let Some(pdu) = status.new_pdu {
                change_pdu_checkpoint(&status.server_name, pdu, container).await?;
            }

            report_server_status(&status.server_name, container).await?;
        }
        FederationSubCommand::Alias(alias) => {
            let client_builder = container.service::<Arc<ClientBuilder>>();
            let client = client_builder.client_for(alias.alias.server_name()).await?;

            let request = get_room_information::Request {
                room_alias: alias.alias,
            };

            let response = client.auth_request(request).await?;

            println!("Room Id: \t\t {}", response.room_id);
            println!("Servers: \t\t {:?}", response.servers);
        }
    }

    Ok(())
}

pub async fn federation_event(
    room_id: &RoomId,
    event_id: &EventId,
    container: &FrozenContainer,
) -> Result<(), FederationError> {
    let client_builder = container.service::<Arc<ClientBuilder>>();
    let client = client_builder
        .client_for(
            room_id
                .try_server_name()
                .map_err(|_e| FederationError::Unknown)?,
        )
        .await?;

    let request = get_event::Request::new(event_id.to_owned());
    let response: get_event::Response = client.auth_request(request).await?;

    println!("{}", serde_json::to_string(&response.pdu)?);

    Ok(())
}

pub async fn server_resolution(
    server_name: &ServerName,
    container: &FrozenContainer,
) -> Result<(), FederationError> {
    let resolutor = container.service::<Arc<dyn ServerDiscovery>>();

    let resolution = resolutor.resolve(server_name).await?;
    println!("Host: \t\t\t {}", resolution.host);
    println!("Host Header: \t\t {}", resolution.host_header);
    println!("TLS server name: \t {}", resolution.tls_server_name);

    Ok(())
}

pub async fn report_server_status(
    server_name: &ServerName,
    container: &FrozenContainer,
) -> Result<(), FederationError> {
    let server_storage = container.service::<Arc<dyn ServerStorage>>();
    let server = server_storage.load_server(server_name).await?;
    println!("Status: \t\t {:?}", server.state());

    let resolutor = container.service::<Arc<dyn ServerDiscovery>>();
    let resolution = resolutor.resolve(server_name).await?;
    println!("Host: \t\t\t {}", resolution.host);
    println!("Host Header: \t\t {}", resolution.host_header);
    println!("TLS server name: \t {}", resolution.tls_server_name);

    let checkpoint_storage = container.service::<Arc<dyn CheckpointStorage<Checkpoint>>>();
    let checkpoint = checkpoint_storage
        .get_checkpoint(&format!("fed_{server_name}"))
        .await?;

    if let Some(checkpoint) = checkpoint {
        println!("Latest PDU: \t\t {}", checkpoint.latest_pdu());
        println!("Latest EDU: \t\t {}", checkpoint.latest_edu());
    }

    Ok(())
}

pub async fn change_server_state(
    server_name: &ServerName,
    new_state: NewServerState,
    container: &FrozenContainer,
) -> Result<(), StorageError> {
    let server_storage = container.service::<Arc<dyn ServerStorage>>();
    let mut server = server_storage.load_server(server_name).await?;

    match new_state {
        NewServerState::Offline => {
            *server.state_mut() = ServerState::PermanentlyOffline;
        }
        NewServerState::Online => {
            *server.state_mut() = ServerState::Online;
        }
    }

    server_storage.store_server(server_name, &server).await?;

    Ok(())
}

pub async fn change_pdu_checkpoint(
    server_name: &ServerName,
    stream_token: u64,
    container: &FrozenContainer,
) -> Result<(), StorageError> {
    let worker_id = format!("fed_{server_name}");
    let checkpoint_storage = container.service::<Arc<dyn CheckpointStorage<Checkpoint>>>();
    let checkpoint = checkpoint_storage.get_checkpoint(&worker_id).await?;

    match checkpoint {
        Some(mut checkpoint) => {
            checkpoint.update_pdu_checkpoint(StreamToken::from(stream_token));
            checkpoint_storage
                .update_checkpoint(&worker_id, &checkpoint)
                .await?;
        }
        None => {
            tracing::error!("No checkpoint stored for target server");
        }
    }
    Ok(())
}
