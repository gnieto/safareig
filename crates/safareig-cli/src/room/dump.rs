use std::collections::HashMap;

use safareig::{
    client::room::state::RoomStateId,
    storage::{BackwardExtremity, Leaf, RoomStorage, RoomTopologyStorage, StateStorage},
};
use safareig_core::{
    pagination::{self, Pagination, Range},
    ruma::{OwnedEventId, OwnedRoomId},
};
use serde::{Deserialize, Serialize};
use serde_json::Value;

pub struct RoomDumper {
    room_id: OwnedRoomId,
    dump: RoomDump,
}

impl RoomDumper {
    pub fn new(room: OwnedRoomId) -> Self {
        RoomDumper {
            room_id: room,
            dump: RoomDump::default(),
        }
    }
}

#[derive(Serialize, Deserialize, Default)]
pub struct RoomDump {
    events: Vec<serde_json::Value>,
    backward: Vec<BackwardExtremity>,
    forward: Vec<Leaf>,
    states_ids: HashMap<String, HashMap<String, OwnedEventId>>,
}

impl RoomDumper {
    pub async fn room_dump(
        mut self,
        topology_storage: &dyn RoomTopologyStorage,
        room_storage: &dyn RoomStorage,
        state: &dyn StateStorage,
    ) -> Result<RoomDump, anyhow::Error> {
        self.dump_backward_extremities(topology_storage).await?;
        self.dump_forward_extremities(topology_storage, state)
            .await?;

        let query =
            Pagination::new(Range::all(), ()).with_direction(pagination::Direction::Backward);
        // TODO: Iterate over all events in topological order
        let events = room_storage
            .room_events(&self.room_id, query)
            .await?
            .records();

        for e in events {
            // TODO: Think about redacting event content
            let mut raw_event = serde_json::to_value(&e).unwrap();
            let state_id = state
                .state_at(&self.room_id, e.event_ref.event_id())
                .await?;

            let state_id = match state_id {
                Some(id) => {
                    self.dump_state(&id, state).await?;
                    Value::String(id.to_string())
                }
                None => Value::Null,
            };

            raw_event
                .as_object_mut()
                .unwrap()
                .insert("state_id".to_string(), state_id);

            self.dump.events.push(raw_event);
        }

        Ok(self.dump)
    }

    async fn dump_state(
        &mut self,
        state_id: &RoomStateId,
        state: &dyn StateStorage,
    ) -> Result<(), anyhow::Error> {
        let str_id = state_id.to_string();

        #[allow(clippy::map_entry)]
        if !self.dump.states_ids.contains_key(&str_id) {
            let mut current_state = HashMap::new();
            let indexes = state.states(state_id, None).await?;

            for index in indexes {
                let key = format!("{}:{}", index.kind, index.state_key.unwrap_or_default());

                current_state.insert(key, index.event_ref.owned_event_id());
            }

            self.dump.states_ids.insert(str_id, current_state);
        }

        Ok(())
    }

    async fn dump_backward_extremities(
        &mut self,
        topology_storage: &dyn RoomTopologyStorage,
    ) -> Result<(), anyhow::Error> {
        let backward_extremities = topology_storage
            .backward_extremities(&self.room_id, pagination::Range::all())
            .await?;

        self.dump.backward = backward_extremities;

        Ok(())
    }

    async fn dump_forward_extremities(
        &mut self,
        topology: &dyn RoomTopologyStorage,
        state: &dyn StateStorage,
    ) -> Result<(), anyhow::Error> {
        let forward_extremities = topology.forward_extremities(&self.room_id).await?;

        for forward_extremity in &forward_extremities {
            self.dump_state(&forward_extremity.state_at, state).await?;
        }

        self.dump.forward = forward_extremities;

        Ok(())
    }
}
