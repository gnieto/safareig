use std::{collections::HashSet, sync::Arc};

use safareig::{
    client::room::state::{ruma_stateres::StateResParameters, RoomStateId},
    core::room::RoomVersionRegistry,
    storage::{RoomStorage, RoomTopologyStorage, StateStorage},
};
use safareig_core::ruma::{OwnedEventId, OwnedRoomId};

use crate::StateResolution;

pub struct RoomResolver {
    room_id: OwnedRoomId,
    events: Vec<String>,
    state: Arc<dyn StateStorage>,
    room_version_loader: Arc<RoomVersionRegistry>,
}

impl RoomResolver {
    pub fn new(
        args: &StateResolution,
        room_version_loader: Arc<RoomVersionRegistry>,
        state: Arc<dyn StateStorage>,
    ) -> Self {
        RoomResolver {
            room_id: args.room_id.parse().unwrap(),
            events: args.event_ids.clone(),
            state,
            room_version_loader,
        }
    }
}

impl RoomResolver {
    pub async fn resolve(
        &self,
        room_storage: Arc<dyn RoomStorage>,
        topology: Arc<dyn RoomTopologyStorage>,
    ) -> Result<(), anyhow::Error> {
        let event_ids = self.collect_events(topology.as_ref()).await?;
        let state_ids = self.state_ids(&event_ids, self.state.as_ref()).await?;
        let descriptor = room_storage.room_descriptor(&self.room_id).await?.unwrap();

        let params = StateResParameters {
            state_ids,
            room_id: self.room_id.to_owned(),
            room_version: descriptor.room_version,
        };

        let room_version = self.room_version_loader.get(&params.room_version).unwrap();
        let room_state = room_version.state_resolution().resolve(params).await?;
        let events = room_state.state_events(None).await?;

        for e in events {
            println!("{}", serde_json::to_string(&e).unwrap());
        }

        Ok(())
    }

    async fn collect_events(
        &self,
        topology_storage: &dyn RoomTopologyStorage,
    ) -> Result<Vec<OwnedEventId>, anyhow::Error> {
        let mut event_ids: Vec<OwnedEventId> = self
            .events
            .iter()
            .map(|event_id| event_id.parse().unwrap())
            .collect();

        if event_ids.is_empty() {
            let leaves = topology_storage
                .forward_extremities(&self.room_id)
                .await?
                .into_iter()
                .map(|leaf| leaf.event_id.event_id().to_owned())
                .collect();
            event_ids = leaves;
        }

        Ok(event_ids)
    }

    async fn state_ids(
        &self,
        events: &[OwnedEventId],
        state: &dyn StateStorage,
    ) -> Result<HashSet<RoomStateId>, anyhow::Error> {
        let mut states = HashSet::new();

        for event_id in events {
            states.insert(
                state
                    .state_at(&self.room_id, event_id)
                    .await?
                    .ok_or_else(|| anyhow::anyhow!("missing state id for event {}", event_id))?,
            );
        }

        Ok(states)
    }
}
