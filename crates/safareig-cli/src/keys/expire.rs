use std::time::SystemTime;

use safareig_core::config::HomeserverConfig;
use safareig_federation::keys::storage::ServerKeyStorage;

pub struct KeyExpireCommand;

impl KeyExpireCommand {
    pub async fn run(
        params: crate::KeyExpire,
        config: &HomeserverConfig,
        server_keys: &dyn ServerKeyStorage,
    ) -> Result<(), anyhow::Error> {
        let all_keys = server_keys.get_server_keys(&config.server_name).await?;
        let mut target_key = all_keys
            .into_iter()
            .find(|k| k.id == params.key_id)
            .ok_or_else(|| anyhow::anyhow!("could not find target key"))?;

        match target_key.expire {
            Some(_) => {
                println!("Key is already expired");
            }
            None => {
                target_key.expire = Some(SystemTime::now());
                let _ = server_keys
                    .add_server_key(&config.server_name, &target_key)
                    .await;

                println!("Key has been expired");
            }
        }

        Ok(())
    }
}
