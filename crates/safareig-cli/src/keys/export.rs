use std::{
    io::Write,
    path::PathBuf,
    time::{SystemTime, UNIX_EPOCH},
};

use safareig_federation::keys::LocalKeyProvider;

pub struct KeyExportCommand;

impl KeyExportCommand {
    pub async fn run(
        params: crate::KeyExport,
        local_key: &LocalKeyProvider,
    ) -> Result<(), anyhow::Error> {
        let final_path = generate_final_path(params)?;
        let keys = local_key.all_keys().await?;
        let json = serde_json::to_vec(&keys)?;

        let mut handle = std::fs::File::create(&final_path)?;
        handle.write_all(&json)?;

        println!(
            "Exported {} keys to path: {}",
            keys.len(),
            final_path.as_os_str().to_str().unwrap()
        );

        Ok(())
    }
}

fn generate_final_path(params: crate::KeyExport) -> Result<PathBuf, anyhow::Error> {
    let mut base_path = PathBuf::new();
    base_path.push(&params.path);

    if base_path.is_dir() {
        let current_timestamp = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_millis();
        base_path.push(format!("keys_{current_timestamp}.json"));
    }

    Ok(base_path)
}
