use std::io::Read;

use safareig_core::config::HomeserverConfig;
use safareig_federation::keys::storage::{Key, ServerKeyStorage};

pub struct KeyImportCommand;

impl KeyImportCommand {
    pub async fn run(
        params: crate::KeyImport,
        config: &HomeserverConfig,
        server_keys: &dyn ServerKeyStorage,
    ) -> Result<(), anyhow::Error> {
        let mut file = std::fs::File::open(params.path)?;
        let mut content = Vec::new();
        let _ = file.read_to_end(&mut content)?;
        let server_name = config.server_name.to_owned();
        let keys: Vec<Key> = serde_json::from_slice(&content)?;

        let current_keys = server_keys.get_server_keys(&server_name).await?;
        for k in keys {
            let already_exists = current_keys.iter().any(|current| current.id == k.id);

            if already_exists {
                println!(
                    "Key {} already exists on the database. Refusing to import it",
                    k.id
                );
                continue;
            }

            server_keys.add_server_key(&server_name, &k).await?;
        }

        Ok(())
    }
}
