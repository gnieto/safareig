use std::{io, path::PathBuf, sync::Arc};

use clap::Parser;
use federation::{handle_federation_command, FederationSubCommand};
use keys::{expire::KeyExpireCommand, import::KeyImportCommand};
use safareig::{
    client::room::loader::RoomLoader,
    core::room::RoomVersionRegistry,
    storage::{EventStorage, RoomStorage, RoomTopologyStorage, StateStorage},
};
use safareig_app::default_app;
use safareig_authentication::{auth::TokenHandler, PasswordHasher};
use safareig_core::{
    app::AppOptions,
    auth::Identity,
    command::{Command, DummyRouter},
    config::HomeserverConfig,
    ddi::ServiceResolverExt,
    ruma::{events::TimelineEventType, DeviceId, OwnedRoomId, OwnedUserId, ServerName, UserId},
};
use safareig_federation::keys::{storage::ServerKeyStorage, LocalKeyProvider};
use safareig_server_notices::command::send_notice::{SendNoticeRequest, SendServerNoticeCommand};
use safareig_users::storage::{User, UserStorage};
use tracing_subscriber::{prelude::*, EnvFilter, Registry};

use crate::{
    keys::export::KeyExportCommand,
    room::{dot_room, dump::RoomDumper, resolve_state::RoomResolver, room_state},
};

mod federation;
mod keys;
mod room;

#[derive(Parser, Debug)]
#[clap(version = "0.1.0")]
struct Opts {
    #[clap(subcommand)]
    subcmd: SubCommand,
}

#[derive(Parser, Debug)]
enum SubCommand {
    #[clap(version = "0.1.0")]
    AddUser(AddUserParams),
    #[clap(version = "0.1.0")]
    Token(TokenParams),
    #[clap(version = "0.1.0")]
    ChangePassword(ChangePasswordParams),
    #[clap(version = "0.1.0")]
    Graph(GraphParams),
    #[clap(version = "0.1.0")]
    StateAtLeaf(StateLeaf),
    #[clap(version = "0.1.0")]
    RoomDump(RoomDump),
    #[clap(version = "0.1.0")]
    RoomStateRes(StateResolution),
    #[clap(version = "0.1.0")]
    KeyExport(KeyExport),
    #[clap(version = "0.1.0")]
    KeyImport(KeyImport),
    #[clap(version = "0.1.0")]
    KeyExpire(KeyExpire),
    #[clap(subcommand)]
    ServerNotices(ServerNoticesSubcommand),
    #[clap(subcommand)]
    Federation(FederationSubCommand),
    #[clap(version = "0.1.0")]
    ShowConfig,
}

#[derive(Parser, Debug)]
struct AddUserParams {
    username: String,
    password: Option<String>,
}

#[derive(Parser, Debug)]
struct TokenParams {
    user_id: OwnedUserId,
}

#[derive(Parser, Debug)]
struct ChangePasswordParams {
    username: String,
    password: String,
}

#[derive(Parser, Debug)]
struct GraphParams {
    room_id: String,
}

#[derive(Parser, Debug)]
struct StateLeaf {
    room_id: String,
    event_type: Option<String>,
}

#[derive(Parser, Debug)]
struct RoomDump {
    room_id: String,
}

#[derive(Parser, Debug)]
pub struct StateResolution {
    room_id: String,
    event_ids: Vec<String>,
}

#[derive(Parser, Debug)]
pub struct KeyExport {
    path: String,
    key_id: Option<String>,
}

#[derive(Parser, Debug)]
pub struct KeyImport {
    path: String,
}

#[derive(Parser, Debug)]
pub struct KeyExpire {
    key_id: String,
}

#[derive(Parser, Debug)]
pub enum ServerNoticesSubcommand {
    SendNotice(SendNoticeParams),
}

#[derive(Parser, Debug)]
pub struct SendNoticeParams {
    user_id: String,
    message: String,
}

#[tokio::main]
async fn main() -> Result<(), anyhow::Error> {
    let opts: Opts = Opts::parse();
    let fmt_layer = ::tracing_subscriber::fmt::layer()
        .with_writer(io::stderr)
        .with_target(false);
    let filter_layer = EnvFilter::try_from_default_env()
        .or_else(|_| EnvFilter::try_new("info"))
        .unwrap();
    Registry::default()
        .with(fmt_layer)
        .with(filter_layer)
        .init();

    let cfg_file = PathBuf::from(&"Settings.toml");
    let config = Arc::new(HomeserverConfig::from_file(cfg_file.as_ref()));

    let app_opts = AppOptions {
        spawn_worker: None,
        run_migrations: false,
    };

    let router = DummyRouter {};
    let (container, _) = default_app::<DummyRouter>(router, app_opts, config.clone())
        .take()
        .await;

    match opts.subcmd {
        SubCommand::AddUser(params) => {
            let cfg = container.service_provider.get::<Arc<HomeserverConfig>>()?;
            let server_name = &cfg.server_name;

            let user_id =
                UserId::parse_with_server_name(params.username.as_str(), server_name.as_ref())
                    .unwrap();
            let hasher = container
                .service_provider
                .get::<Arc<dyn PasswordHasher + Send + Sync>>()?;
            let users = container.service_provider.get::<Arc<dyn UserStorage>>()?;
            let hash = hasher.hash(&params.password.unwrap());
            let user = User::new(&user_id, hash);

            if let Err(e) = users.create_user(&user).await {
                eprintln!("Error: {e:?   }");
            }
        }
        SubCommand::Token(params) => {
            let user_storage = container.service_provider.get::<Arc<dyn UserStorage>>()?;
            let user = user_storage.user_by_id(&params.user_id).await?;

            match user {
                Some(u) => {
                    let token_handler =
                        container.service_provider.get::<Arc<dyn TokenHandler>>()?;
                    let device = DeviceId::new();

                    let token = token_handler.generate_token(&params.user_id, &device).await;

                    println!("{}", token.access_token());
                }
                None => {
                    tracing::error!("Could not generate token for non-existing user");
                }
            }
        }
        SubCommand::ChangePassword(params) => {
            let cfg = container.service_provider.get::<Arc<HomeserverConfig>>()?;
            let server_name = &cfg.server_name;
            let users = container.service_provider.get::<Arc<dyn UserStorage>>()?;

            let user_id =
                UserId::parse_with_server_name(params.username.as_str(), server_name.as_ref())
                    .unwrap();
            let hasher = container.service::<Arc<dyn PasswordHasher + Send + Sync>>();
            let hash = hasher.hash(&params.password);
            let user = User::new(&user_id, hash);

            if let Err(e) = users.update_user(&user).await {
                eprintln!("Error: {e:?}");
            }
        }
        SubCommand::Graph(params) => {
            let room: OwnedRoomId = params.room_id.parse().unwrap();
            let events = container.service_provider.get::<Arc<dyn EventStorage>>()?;
            let topology = container
                .service_provider
                .get::<Arc<dyn RoomTopologyStorage>>()?;

            let dot = dot_room(&room, topology.as_ref(), events.as_ref())
                .await
                .unwrap();

            println!("{dot}");
        }
        SubCommand::StateAtLeaf(params) => {
            let room: OwnedRoomId = params.room_id.parse().unwrap();
            let room_loader = container.service_provider.get::<RoomLoader>()?;

            let ty = params.event_type.map(TimelineEventType::from);
            let state = container.service_provider.get::<Arc<dyn StateStorage>>()?;

            room_state(&room, room_loader, state.as_ref(), ty)
                .await
                .unwrap();
        }
        SubCommand::RoomDump(params) => {
            let room: OwnedRoomId = params.room_id.parse().unwrap();
            let state = container.service_provider.get::<Arc<dyn StateStorage>>()?;
            let rooms = container.service_provider.get::<Arc<dyn RoomStorage>>()?;
            let topology = container
                .service_provider
                .get::<Arc<dyn RoomTopologyStorage>>()?;

            let dumper = RoomDumper::new(room);
            let dump = dumper
                .room_dump(topology.as_ref(), rooms.as_ref(), state.as_ref())
                .await
                .unwrap();

            println!("{}", serde_json::to_string(&dump).unwrap());
        }
        SubCommand::RoomStateRes(params) => {
            let room_registry = container
                .service_provider
                .get::<Arc<RoomVersionRegistry>>()?;
            let state = container.service_provider.get::<Arc<dyn StateStorage>>()?;
            let rooms = container.service_provider.get::<Arc<dyn RoomStorage>>()?;
            let topology = container
                .service_provider
                .get::<Arc<dyn RoomTopologyStorage>>()?;

            let room_res = RoomResolver::new(&params, room_registry.clone(), state.clone());
            if let Err(e) = room_res.resolve(rooms.clone(), topology.clone()).await {
                println!("Error while resolving an state: {e}");
            }
        }
        SubCommand::KeyExport(params) => {
            let local_key_provider = container.service_provider.get::<LocalKeyProvider>()?;

            if let Err(e) = KeyExportCommand::run(params, local_key_provider).await {
                eprintln!("Error: {e:?}");
            }
        }
        SubCommand::KeyImport(params) => {
            let server_keys = container
                .service_provider
                .get::<Arc<dyn ServerKeyStorage>>()?;
            let config = container.service_provider.get::<Arc<HomeserverConfig>>()?;

            if let Err(e) =
                KeyImportCommand::run(params, config.as_ref(), server_keys.as_ref()).await
            {
                eprintln!("Error: {e:?}");
            }
        }
        SubCommand::KeyExpire(params) => {
            let server_keys = container
                .service_provider
                .get::<Arc<dyn ServerKeyStorage>>()?;
            let config = container.service_provider.get::<Arc<HomeserverConfig>>()?;

            if let Err(e) =
                KeyExpireCommand::run(params, config.as_ref(), server_keys.as_ref()).await
            {
                eprintln!("Error: {e:?}");
            }
        }
        SubCommand::ServerNotices(server_notices) => match server_notices {
            ServerNoticesSubcommand::SendNotice(params) => {
                let cfg = container.service_provider.get::<Arc<HomeserverConfig>>()?;
                let server_name = &cfg.server_name;

                let user_id = local_user(&params.user_id, server_name);
                let cmd = container.command::<SendServerNoticeCommand>().unwrap();
                let request = SendNoticeRequest {
                    user_id,
                    content: params.message.to_owned(),
                };

                cmd.execute(request, Identity::None).await.unwrap();
            }
        },
        SubCommand::Federation(command) => {
            handle_federation_command(command, &container).await?;
        }
        SubCommand::ShowConfig => {
            println!("{:?}", config);
        }
    };

    Ok(())
}

fn local_user(username: &str, server: &ServerName) -> OwnedUserId {
    let user_id = UserId::parse(username)
        .or_else(|_| UserId::parse_with_server_name(username, server))
        .unwrap();

    if user_id.server_name() != server {
        panic!("Invalid user id");
    }

    user_id
}
