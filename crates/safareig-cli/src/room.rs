pub(crate) mod dump;
pub(crate) mod resolve_state;

use std::collections::{HashMap, HashSet};

use petgraph::{
    dot::{Config, Dot},
    graph::NodeIndex,
    Graph,
};
use safareig::{
    client::room::loader::RoomLoader,
    core::events::EventRef,
    storage::{EventStorage, RoomTopologyStorage, StateStorage},
};
use safareig_core::{
    ruma::{events::TimelineEventType, EventId, OwnedEventId, RoomId},
    storage::StorageError,
};

struct GraphWalk {
    pending: Vec<EventRef>,
    indexes: HashMap<OwnedEventId, NodeIndex<u32>>,
    edges: HashSet<(NodeIndex<u32>, NodeIndex<u32>, String)>,
    height: u32,
}

pub async fn room_state(
    room: &RoomId,
    room_loader: &RoomLoader,
    state: &dyn StateStorage,
    filter: Option<TimelineEventType>,
) -> Result<(), StorageError> {
    let room = room_loader
        .get(room)
        .await
        .expect("room could be loaded from storage")
        .expect("room exists");

    let room_state = room
        .state_at_leaves()
        .await
        .map_err(|e| StorageError::Custom(e.to_string()))?;
    let events = state.states(room_state.id(), None).await?;

    for e in events {
        if let Some(t) = &filter {
            if &e.kind != t {
                continue;
            }
        }

        println!("{}", serde_json::to_string(&e).unwrap());
    }

    Ok(())
}

pub async fn dot_room(
    room: &RoomId,
    topology: &dyn RoomTopologyStorage,
    storage: &dyn EventStorage,
) -> Result<String, StorageError> {
    let mut graph = Graph::<String, String>::new();
    let leafs = topology.forward_extremities(room).await?;

    let leafs: Vec<EventRef> = leafs.into_iter().map(|l| l.event_id).collect();

    let mut walk = GraphWalk {
        pending: leafs,
        indexes: HashMap::new(),
        edges: HashSet::new(),
        height: 100,
    };

    iterate(&mut graph, &mut walk, storage).await;

    let dot = Dot::with_config(&graph, &[Config::EdgeNoLabel]);

    Ok(format!("{dot:?}"))
}

async fn iterate(
    graph: &mut Graph<String, String>,
    walk: &mut GraphWalk,
    storage: &dyn EventStorage,
) {
    for _ in 0..100 {
        add_current_depth(graph, walk, storage).await;
    }

    for (a, b, edge) in &walk.edges {
        graph.add_edge(*a, *b, edge.clone());
    }
}

async fn add_current_depth(
    graph: &mut Graph<String, String>,
    walk: &mut GraphWalk,
    event_storage: &dyn EventStorage,
) {
    let current: Vec<EventRef> = walk.pending.drain(..).collect();
    let mut parents = Vec::new();
    for c in current {
        // Add current event
        let current_idx = add_event_id(graph, walk, c.event_id(), event_storage).await;
        let event = event_storage.get_event(c.event_id()).await.unwrap();

        if let Some(event) = event {
            parents.extend_from_slice(&event.prev_events);

            // Add Parents
            for prev in &event.prev_events {
                let parent_idx = add_event_id(graph, walk, prev.event_id(), event_storage).await;
                walk.edges.insert((current_idx, parent_idx, "".into()));
            }
        }
    }

    walk.height -= 1;
    walk.pending = parents;
}

async fn add_event_id(
    graph: &mut Graph<String, String>,
    walk: &mut GraphWalk,
    event_id: &EventId,
    storage: &dyn EventStorage,
) -> NodeIndex<u32> {
    if !walk.indexes.contains_key(event_id) {
        let event = storage.get_event(event_id).await.unwrap();

        match event {
            Some(event) => {
                let content = format!(
                    "{} ({}) - {} => {}",
                    event.depth, event.kind, event.sender, event_id
                );
                let index = graph.add_node(content);

                walk.indexes.insert(event_id.to_owned(), index);
            }
            None => {
                let content = format!("(missing) => {event_id}");

                let index = graph.add_node(content);
                walk.indexes.insert(event_id.to_owned(), index);
            }
        }
    }

    return *walk.indexes.get(event_id).unwrap();
}
