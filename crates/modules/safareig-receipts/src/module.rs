use std::sync::Arc;

use safareig_core::{
    command::{Container, InjectServices, Module, Router, ServiceCollectionExtension},
    ddi::{ServiceCollectionExt, ServiceProvider},
};
use safareig_ephemeral::services::EphemeralHandler;

use crate::{command::SetReceiptCommand, ephemeral::ReceiptEduHandler, ReceiptManager};

#[derive(Default)]
pub struct ReceiptsModule {}

#[async_trait::async_trait]
impl<R: Router> Module<R> for ReceiptsModule {
    fn register(&self, container: &mut Container, router: &mut R) {
        container.service_collection.register::<ReceiptManager>();
        container.inject_endpoint::<_, SetReceiptCommand, _>(router);
        container
            .service_collection
            .service_factory_var("m.receipt", |sp: &ServiceProvider| {
                let handler: Arc<dyn EphemeralHandler> = Arc::new(ReceiptEduHandler::inject(sp)?);

                Ok(handler)
            });
    }
}
