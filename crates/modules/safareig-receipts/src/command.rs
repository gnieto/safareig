use async_trait::async_trait;
use safareig::client::room::loader::RoomLoader;
use safareig_core::{
    auth::Identity,
    command::Command,
    ruma::{
        api::client::{
            error::{ErrorBody, ErrorKind},
            receipt::create_receipt::{self, v3::ReceiptType},
        },
        exports::http::StatusCode,
    },
    Inject,
};

use crate::ReceiptManager;

#[derive(Inject)]
pub struct SetReceiptCommand {
    room_loader: RoomLoader,
    receipt_manager: ReceiptManager,
}

#[async_trait]
impl Command for SetReceiptCommand {
    type Input = create_receipt::v3::Request;
    type Output = create_receipt::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let room = self
            .room_loader
            .get_for_user(&input.room_id, id.user())
            .await?;

        match &input.receipt_type {
            ReceiptType::Read | ReceiptType::FullyRead | ReceiptType::ReadPrivate => (),
            _ => {
                return Err(safareig_core::ruma::api::client::Error {
                    status_code: StatusCode::BAD_REQUEST,
                    body: ErrorBody::Standard {
                        kind: ErrorKind::InvalidParam,
                        message: "unexpected receipt type".to_string(),
                    },
                })
            }
        };

        match input.receipt_type {
            ReceiptType::ReadPrivate => {
                self.receipt_manager
                    .handle_private_read_receipt(room.id(), id.user(), &input.event_id)
                    .await?;
            }
            ReceiptType::Read => {
                self.receipt_manager
                    .handle_read_receipt(room.id(), id.user(), &input.event_id)
                    .await?;
            }
            ReceiptType::FullyRead => {
                self.receipt_manager
                    .handle_fully_read_receipt(&room, id.user(), &input.event_id)
                    .await?;
            }
            _ => {
                tracing::error!("Marker not supported {:?}", input.receipt_type);
            }
        }

        Ok(create_receipt::v3::Response::new())
    }
}
