use std::{sync::Arc, time::SystemTime};

use safareig::{
    client::room::{Room, RoomError},
    storage::RoomStreamStorage,
};
use safareig_client_config::{storage::AccountDataEvent, AccountDataManager};
use safareig_core::{
    bus::{Bus, BusMessage},
    events::Content,
    ruma::{events::fully_read::FullyReadEventContent, EventId, RoomId, UserId},
    storage::StorageError,
    Inject, StreamToken,
};
use safareig_ephemeral::services::{EphemeralEvent, EphemeralStream};
use safareig_push_notifications::storage::NotificationStorage;

pub mod command;
mod ephemeral;
pub mod module;

#[derive(Clone, Inject)]
pub struct ReceiptManager {
    ephemeral_stream: EphemeralStream,
    notification: Arc<dyn NotificationStorage>,
    room_stream_storage: Arc<dyn RoomStreamStorage>,
    account_data: AccountDataManager,
    bus: Bus,
}

impl ReceiptManager {
    pub async fn handle_read_receipt(
        &self,
        room: &RoomId,
        user_id: &UserId,
        event_id: &EventId,
    ) -> Result<(), StorageError> {
        self.store_read_marker(room, user_id, event_id).await?;

        let event = EphemeralEvent::Marker(
            room.to_owned(),
            user_id.to_owned(),
            event_id.to_owned(),
            SystemTime::now(),
        );
        self.ephemeral_stream.push_event(event);

        Ok(())
    }

    pub async fn handle_private_read_receipt(
        &self,
        room: &RoomId,
        user_id: &UserId,
        event_id: &EventId,
    ) -> Result<(), StorageError> {
        self.store_read_marker(room, user_id, event_id).await?;

        Ok(())
    }

    pub async fn handle_fully_read_receipt(
        &self,
        room: &Room,
        user_id: &UserId,
        event_id: &EventId,
    ) -> Result<(), RoomError> {
        if !room.has_event(event_id).await? {
            return Err(RoomError::InvalidEventId);
        }

        let fully_read_event = FullyReadEventContent {
            event_id: event_id.to_owned(),
        };

        let content = Content::from_event_content(&fully_read_event)?;
        let event = AccountDataEvent::Room(room.id().to_owned(), content);
        self.account_data
            .update_account_data(user_id, event)
            .await
            .map_err(|e| RoomError::Custom(e.to_string()))?;

        self.bus.send_message(BusMessage::FullyReadMarker(
            user_id.to_owned(),
            room.id().to_owned(),
        ));

        Ok(())
    }

    async fn store_read_marker(
        &self,
        room_id: &RoomId,
        user_id: &UserId,
        event_id: &EventId,
    ) -> Result<(), StorageError> {
        let token = match self
            .room_stream_storage
            .stream_token_at(room_id, event_id)
            .await?
        {
            Some(token) => token,
            None => StreamToken::horizon(),
        };

        self.notification
            .store_read_marker(user_id, room_id, &token)
            .await?;

        Ok(())
    }
}
