use std::{collections::BTreeMap, time::SystemTime};

use safareig_core::{
    auth::FederationAuth,
    ruma::{
        api::federation::transactions::edu::{Edu, ReceiptContent, ReceiptData, ReceiptMap},
        events::receipt::Receipt,
    },
    time::{system_time_to_millis, uint_to_system_time},
    Inject,
};
use safareig_ephemeral::services::{
    EphemeralAggregator, EphemeralEvent, EphemeralHandler, EphemeralStream,
};

#[derive(Inject)]
pub struct ReceiptEduHandler {
    ephemeral_stream: EphemeralStream,
}

impl ReceiptEduHandler {
    pub fn handle(&self, content: ReceiptContent) {
        for (room, receipts) in content.receipts {
            for (user, data) in receipts.read {
                // TODO: Should we check that user belong to remote server?
                // TODO: Should we check that read marker does not go backward?

                if let Some(event_id) = data.event_ids.first() {
                    let system_time = data
                        .data
                        .ts
                        .as_ref()
                        .map(uint_to_system_time)
                        .unwrap_or_else(SystemTime::now);

                    let event =
                        EphemeralEvent::Marker(room.clone(), user, event_id.clone(), system_time);
                    self.ephemeral_stream.push_event(event);
                }
            }
        }
    }
}

#[async_trait::async_trait]
impl EphemeralHandler for ReceiptEduHandler {
    async fn on_federation(&self, edu: Edu, _: &FederationAuth) {
        let receipt = match edu {
            Edu::Receipt(receipt) => receipt,
            _ => return,
        };

        self.handle(receipt);
    }

    fn edu_aggregator(&self) -> Box<dyn EphemeralAggregator> {
        Box::new(EphemeralReceiptAggregator::new())
    }
}

pub struct EphemeralReceiptAggregator {
    content: ReceiptContent,
}

impl EphemeralReceiptAggregator {
    pub fn new() -> Self {
        EphemeralReceiptAggregator {
            content: ReceiptContent::new(BTreeMap::new()),
        }
    }
}

#[async_trait::async_trait]
impl EphemeralAggregator for EphemeralReceiptAggregator {
    async fn aggregate_event(&mut self, event: EphemeralEvent) {
        if let EphemeralEvent::Marker(room, user, event_id, time) = event {
            let room_entry = self
                .content
                .receipts
                .entry(room)
                .or_insert_with(|| ReceiptMap::new(BTreeMap::new()));

            let marker_time = system_time_to_millis(time);
            let user_entry = room_entry
                .read
                .entry(user)
                .or_insert_with(|| ReceiptData::new(Receipt::new(marker_time), Vec::new()));

            user_entry.event_ids.push(event_id);
            user_entry.data.ts = std::cmp::max(user_entry.data.ts, Some(marker_time));
        }
    }

    fn edus(self: Box<Self>) -> Vec<Edu> {
        if self.content.receipts.is_empty() {
            Vec::new()
        } else {
            vec![Edu::Receipt(self.content)]
        }
    }
}
