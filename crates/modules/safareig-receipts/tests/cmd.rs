use std::sync::Arc;

use safareig::storage::RoomStreamStorage;
use safareig_core::{
    command::Command,
    ruma::{
        api::client::receipt::create_receipt::{self, v3::ReceiptType},
        events::receipt::ReceiptThread,
        EventId,
    },
};
use safareig_ephemeral::services::EphemeralEvent;
use safareig_push_notifications::{storage::NotificationStorage, PushRulesModule};
use safareig_receipts::{command::SetReceiptCommand, module::ReceiptsModule};
use safareig_testing::TestingApp;

#[tokio::test]
async fn it_does_not_set_marker_if_user_not_in_room() {
    let app = TestingApp::with_modules(vec![
        Box::new(ReceiptsModule::default()),
        Box::new(PushRulesModule),
    ])
    .await;
    let marker_cmd = app.command::<SetReceiptCommand>();
    let (_, room) = app.public_room().await;

    let input = create_receipt::v3::Request {
        room_id: room.id().to_owned(),
        receipt_type: ReceiptType::Read,
        event_id: EventId::new(&app.server_name()),
        thread: ReceiptThread::Unthreaded,
    };

    let cmd_result = marker_cmd.execute(input, TestingApp::new_identity()).await;

    assert!(cmd_result.is_err());
}

#[tokio::test]
async fn it_sets_fully_read_marker_and_read_marker() {
    let app = TestingApp::with_modules(vec![
        Box::new(ReceiptsModule::default()),
        Box::new(PushRulesModule),
    ])
    .await;
    let (identity, room) = app.public_room().await;
    let event_id = app.talk(room.id(), &identity, "hi!").await.unwrap();

    let marker_cmd = app.command::<SetReceiptCommand>();
    let input = create_receipt::v3::Request {
        room_id: room.id().to_owned(),
        receipt_type: ReceiptType::Read,
        event_id: event_id.clone(),
        thread: ReceiptThread::Unthreaded,
    };

    let _ = marker_cmd.execute(input, identity.clone()).await.unwrap();

    match app.latest_ephemeral().unwrap() {
        EphemeralEvent::Marker(room_id, user, event, _) => {
            assert_eq!(room.id(), &room_id);
            assert_eq!(identity.user(), &user);
            assert_eq!(event, event_id);
        }
        _ => {
            panic!("expected marker event")
        }
    }
}

#[tokio::test]
async fn private_marker_does_not_generate_any_ephemeral_event() {
    let app = TestingApp::with_modules(vec![
        Box::new(ReceiptsModule::default()),
        Box::new(PushRulesModule),
    ])
    .await;
    let (identity, room) = app.public_room().await;
    let event_id = app.talk(room.id(), &identity, "hi!").await.unwrap();

    let marker_cmd = app.command::<SetReceiptCommand>();
    let input = create_receipt::v3::Request {
        room_id: room.id().to_owned(),
        receipt_type: ReceiptType::ReadPrivate,
        event_id: event_id.clone(),
        thread: ReceiptThread::Unthreaded,
    };

    let _ = marker_cmd.execute(input, identity.clone()).await.unwrap();

    let notifications = app.service::<Arc<dyn NotificationStorage>>();
    let room_stream_storage = app.service::<Arc<dyn RoomStreamStorage>>();
    assert!(app.latest_ephemeral().is_none());

    let marker = notifications
        .get_read_marker(identity.user(), room.id())
        .await
        .unwrap();
    let latest_token = room_stream_storage.current_stream_token().await.unwrap();
    assert_eq!(marker, latest_token);
}
