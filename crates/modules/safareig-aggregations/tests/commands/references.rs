use std::collections::HashSet;

use ruma_events::{
    macros::EventContent,
    relation::{Reference, RelationType},
    AnySyncMessageLikeEvent, AnySyncTimelineEvent, TimelineEventType,
};
use safareig_aggregations::{
    api::event_relations, command::GetRelationsCommand, module::AggregationsModule,
};
use safareig_core::command::Command;
use safareig_testing::TestingApp;
use serde::{Deserialize, Serialize};
use tracing_test::traced_test;

#[tokio::test]
#[traced_test]
async fn reference_bundled_aggregations() {
    let app = TestingApp::with_modules(vec![Box::new(AggregationsModule {})]).await;
    let (creator, room) = app.public_room().await;

    let msg = TestContent::default();
    let event_id = app.send(room.id(), &creator, msg).await.unwrap();

    let msg2 = TestContent {
        relates_to: Some(Reference::new(event_id.clone())),
    };
    let ev_id2 = app.send(room.id(), &creator, msg2).await.unwrap();
    let msg3 = TestContent {
        relates_to: Some(Reference::new(event_id.clone())),
    };
    let ev_id3 = app.send(room.id(), &creator, msg3).await.unwrap();

    let cmd = app.command::<GetRelationsCommand>();
    let input = event_relations::Request::new(
        room.id().to_owned(),
        event_id.clone(),
        RelationType::Reference,
        TimelineEventType::from("safareig.test"),
    );
    let result = cmd.execute(input, creator.clone()).await.unwrap();

    assert_eq!(2, result.chunk.len());
    let event3 = result.chunk.first().unwrap().deserialize().unwrap();
    assert_eq!(ev_id3, event3.event_id());
    let event2 = result.chunk.get(1).unwrap().deserialize().unwrap();
    assert_eq!(ev_id2, event2.event_id());

    let mut sync = app.sync(&creator, None, None).await;
    let room = sync.rooms.join.remove(room.id()).unwrap();
    let first_event = room
        .timeline
        .events
        .into_iter()
        .map(|e| e.deserialize().unwrap())
        .find(|e| e.event_id() == event_id)
        .unwrap();

    let relations = match first_event {
        AnySyncTimelineEvent::MessageLike(AnySyncMessageLikeEvent::_Custom(msg)) => {
            msg.as_original().unwrap().unsigned.relations.clone()
        }
        _ => panic!("unexpected type"),
    };

    let reference_chunk = relations.reference.as_ref().unwrap();
    let reference_events: HashSet<_> = reference_chunk
        .chunk
        .iter()
        .map(|c| c.event_id.to_owned())
        .collect();
    assert!(reference_events.contains(&ev_id2));
    assert!(reference_events.contains(&ev_id3));
}

#[derive(Clone, Debug, Serialize, Deserialize, EventContent, Default)]
#[ruma_event(type = "safareig.test", kind = MessageLike)]
pub struct TestContent {
    #[serde(skip_serializing_if = "Option::is_none", rename = "m.relates_to")]
    pub relates_to: Option<Reference>,
}
