use std::sync::Arc;

use safareig::client::room::events::{RoomEventDecorator, RoomEventListener};
use safareig_core::{
    command::{Container, InjectServices, Module, Router},
    database::DatabaseConnection,
    ddi::{ServiceCollectionExt, ServiceProvider, ServiceResolverExt},
    storage::StorageError,
};
use safareig_sqlx::run_migration;

use crate::{
    command::GetRelationsCommand, event_decorator::AggregatorEventDecorator,
    event_listener::RelationsEventListener, storage::AggregationStorage,
};

#[derive(Default)]
pub struct AggregationsModule {}

impl AggregationsModule {
    fn create_aggregation_storage(db: DatabaseConnection) -> Arc<dyn AggregationStorage> {
        match db {
            #[cfg(feature = "backend-sqlx")]
            DatabaseConnection::Sqlx(pool) => {
                Arc::new(crate::storage::sqlx::SqlxAggregationStorage::new(pool))
            }
            _ => unimplemented!(),
        }
    }
}

#[async_trait::async_trait]
impl<R: Router> Module<R> for AggregationsModule {
    fn register(&self, container: &mut Container, router: &mut R) {
        container
            .service_collection
            .service_factory(|db: &DatabaseConnection| {
                Ok(Self::create_aggregation_storage(db.clone()))
            });

        container
            .service_collection
            .service_factory_var("relations", |sp: &ServiceProvider| {
                let listener: Arc<dyn RoomEventListener> =
                    Arc::new(RelationsEventListener::inject(sp)?);
                Ok(listener)
            });

        container
            .service_collection
            .service_factory_var("aggregator", |sp: &ServiceProvider| {
                let decorator: Arc<dyn RoomEventDecorator> =
                    Arc::new(AggregatorEventDecorator::inject(sp)?);
                Ok(decorator)
            });

        container.inject_endpoint::<_, GetRelationsCommand, _>(router);
    }

    async fn run_migrations(&self, container: &ServiceProvider) -> Result<(), StorageError> {
        #[cfg(feature = "backend-sqlx")]
        {
            let db = container.get::<DatabaseConnection>()?;
            run_migration(db, "sqlite", "safareig-aggregations").await?;
        }

        Ok(())
    }
}
