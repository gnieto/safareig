#[cfg(feature = "backend-sqlx")]
pub mod sqlx;

use std::collections::{BTreeMap, HashSet};

use async_trait::async_trait;
use safareig::storage::{EventStorage, TopologicalToken};
use safareig_core::{
    pagination::{Pagination, PaginationResponse},
    ruma::{
        events::{
            relation::{BundledReference, BundledThread, ReferenceChunk, RelationType},
            BundledStateRelations,
        },
        EventId, OwnedEventId, OwnedUserId, UInt,
    },
    storage::Result,
    StreamToken,
};
use serde::{Deserialize, Serialize};

#[derive(Debug)]
pub struct EventRelation {
    pub(crate) stream_token: StreamToken,
    pub(crate) topological_token: TopologicalToken,
    pub(crate) relation_type: RelationType,
    pub(crate) parent: OwnedEventId,
    pub(crate) event_id: OwnedEventId,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ThreadState {
    pub(crate) participants: HashSet<OwnedUserId>,
    pub(crate) count: u64,
    pub(crate) latest_event: OwnedEventId,
}

impl ThreadState {
    pub fn new(event: OwnedEventId) -> Self {
        Self {
            participants: HashSet::new(),
            count: 0,
            latest_event: event,
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Default)]
pub struct ReferenceState {
    pub(crate) referenced_ids: HashSet<OwnedEventId>,
}

#[derive(Serialize, Deserialize, Default, Debug)]
pub struct ServerSideAggregation {
    #[serde(rename = "m.thread")]
    pub(crate) thread: Option<ThreadState>,

    #[serde(rename = "m.references")]
    pub(crate) references: Option<ReferenceState>,

    #[serde(flatten)]
    unknown: BTreeMap<String, serde_json::Value>,
}

impl ServerSideAggregation {
    #[allow(clippy::wrong_self_convention)]
    pub async fn to_relations(self, events: &dyn EventStorage) -> BundledStateRelations {
        let mut relations = BundledStateRelations::default();

        if let Some(thread) = self.thread {
            let event = events
                .get_event(&thread.latest_event)
                .await
                .ok()
                .flatten()
                .map(TryInto::try_into)
                .transpose()
                .ok()
                .flatten();

            if let Some(event) = event {
                let thread_bundle = BundledThread {
                    latest_event: event,
                    count: UInt::new_saturating(thread.count),
                    current_user_participated: true,
                };
                relations.thread = Some(Box::new(thread_bundle));
            }
        }

        if let Some(reference_state) = self.references {
            let events = reference_state
                .referenced_ids
                .into_iter()
                .map(BundledReference::new)
                .collect();

            let reference_chunk = ReferenceChunk::new(events);
            relations.reference = Some(Box::new(reference_chunk));
        }

        relations
    }
}

// TODO: Allow query by room id / filters
#[async_trait]
pub trait AggregationStorage: Send + Sync {
    async fn event_relation(&self, event: &EventId) -> Result<ServerSideAggregation>;
    async fn update_relation(
        &self,
        event: &EventId,
        aggregations: ServerSideAggregation,
    ) -> Result<()>;
    async fn events_aggregations<'a>(
        &self,
        events: &'a [&'a EventId],
    ) -> Result<BTreeMap<OwnedEventId, ServerSideAggregation>>;
    // async fn event_aggregations(&self, event_id: &EventId) -> Result<AggregationState>;
    async fn store_relation(&self, event_relation: EventRelation) -> Result<()>;
    async fn relations_stream(
        &self,
        parent: &EventId,
        relation_type: RelationType,
        query: Pagination<StreamToken>,
    ) -> Result<PaginationResponse<StreamToken, OwnedEventId>>;
}
