use std::{str::FromStr, sync::Arc};

use async_trait::async_trait;
use safareig::storage::EventStorage;
use safareig_core::{
    auth::Identity,
    command::Command,
    pagination::{Boundary, Pagination, Range},
    ruma::{
        api::{
            client::error::{ErrorBody, ErrorKind},
            Direction,
        },
        exports::http::StatusCode,
        EventId,
    },
    Inject, StreamToken,
};
use thiserror::Error;

use crate::{api::event_relations, storage::AggregationStorage};

#[derive(Inject)]
pub struct GetRelationsCommand {
    aggregations: Arc<dyn AggregationStorage>,
    event_storage: Arc<dyn EventStorage>,
}

#[derive(Error, Debug)]
pub enum AggregationError {
    #[error("Invalid stream token")]
    InvalidToken,
    #[error("Event not found")]
    EventNotFound,
}

impl From<AggregationError> for safareig_core::ruma::api::client::Error {
    fn from(e: AggregationError) -> Self {
        match e {
            AggregationError::InvalidToken => safareig_core::ruma::api::client::Error {
                status_code: StatusCode::BAD_REQUEST,
                body: ErrorBody::Standard {
                    kind: ErrorKind::Unknown,
                    message: e.to_string(),
                },
            },
            AggregationError::EventNotFound => safareig_core::ruma::api::client::Error {
                status_code: StatusCode::BAD_REQUEST,
                body: ErrorBody::Standard {
                    kind: ErrorKind::Unknown,
                    message: e.to_string(),
                },
            },
        }
    }
}

#[async_trait]
impl Command for GetRelationsCommand {
    type Input = event_relations::Request;
    type Output = event_relations::Response;
    type Error = safareig_core::ruma::api::client::Error;
    const OPT_PARAMS: u8 = 2;

    async fn execute(&self, input: Self::Input, _: Identity) -> Result<Self::Output, Self::Error> {
        let limit: u32 = input
            .limit
            .and_then(|limit| u32::try_from(limit).ok())
            .unwrap_or_default();
        let limit = std::cmp::min(limit, 100);

        let from = input
            .from
            .map(|s| StreamToken::from_str(&s))
            .transpose()
            .map_err(|_| AggregationError::InvalidToken)?;

        let to = input
            .to
            .map(|s| StreamToken::from_str(&s))
            .transpose()
            .map_err(|_| AggregationError::InvalidToken)?;

        let range: Range<_> = match input.dir {
            Direction::Backward => (
                to.map(Boundary::Inclusive).unwrap_or(Boundary::Unlimited),
                from.map(Boundary::Exclusive).unwrap_or(Boundary::Unlimited),
            ),
            Direction::Forward => (
                to.map(Boundary::Inclusive).unwrap_or(Boundary::Unlimited),
                from.map(Boundary::Exclusive).unwrap_or(Boundary::Unlimited),
            ),
        }
        .into();

        let query = Pagination::new(range, ())
            .with_limit(limit.into())
            .with_direction(input.dir.into());

        let response = self
            .aggregations
            .relations_stream(&input.event_id, input.relation_type, query)
            .await?;
        let head = response.next().cloned();
        let previous = response.previous().cloned();
        let chunk = response.records();
        let events = self.event_storage.get_events(&chunk).await?;
        let events = events
            .into_iter()
            .map(TryInto::try_into)
            .collect::<Result<Vec<_>, _>>()?;

        let mut response = self.create_response(input.event_id.as_ref()).await?;
        response.chunk = events;
        response.next_batch = head.map(|token| token.to_string());
        response.prev_batch = previous.map(|token| token.to_string());

        Ok(response)
    }
}

impl GetRelationsCommand {
    pub async fn create_response(
        &self,
        event_id: &EventId,
    ) -> Result<event_relations::Response, AggregationError> {
        let event = self
            .event_storage
            .get_event(event_id)
            .await
            .unwrap()
            .ok_or(AggregationError::EventNotFound)?;

        let any_error = event.try_into().unwrap();

        Ok(event_relations::Response::new(any_error))
    }
}
