use std::sync::Arc;

use safareig::{
    client::room::events::RoomEventListener,
    core::events::{Event, Relation},
    storage::EventOptions,
};
use safareig_core::{
    events::EventError, ruma::events::relation::RelationType, Inject, StreamToken,
};

use crate::storage::{AggregationStorage, EventRelation, ThreadState};

#[derive(Inject)]
pub struct RelationsEventListener {
    aggregations: Arc<dyn AggregationStorage>,
}

#[async_trait::async_trait]
impl RoomEventListener for RelationsEventListener {
    async fn on_stream_event(
        &self,
        event: &Event,
        stream_token: StreamToken,
        _options: &EventOptions,
    ) -> Result<(), EventError> {
        // This might calculate wrong aggregations on rooms with high concurrence. A worker approach might be more correct
        // if we want to ensure a high consistency on the aggregation calculation.
        // If we change this for a worker processor, we may need to synchronize syncs with this new processor checkpoints (otherwise, we may
        // be serving events which are not yet calculated, which may lead to events missing it's aggregation in th unsigned field)
        let relation = match event.relates_to() {
            Some(relates_to) => relates_to,
            None => return Ok(()),
        };

        let topological_token = event.topological_token();
        let current_event_id = event.event_ref.event_id();
        let sender = &event.sender;

        match relation {
            Relation::Thread(thread) => {
                let mut current_relations = self
                    .aggregations
                    .event_relation(&thread.event_id)
                    .await
                    .map_err(|e| EventError::Custom(e.to_string()))?;

                let event_relation = EventRelation {
                    stream_token,
                    topological_token,
                    relation_type: RelationType::Thread,
                    parent: thread.event_id.to_owned(),
                    event_id: current_event_id.to_owned(),
                };

                let mut thread_state = current_relations
                    .thread
                    .take()
                    .unwrap_or_else(|| ThreadState::new(current_event_id.to_owned()));

                thread_state.count += 1;
                thread_state.participants.insert(sender.to_owned());
                current_relations.thread = Some(thread_state);

                self.aggregations
                    .store_relation(event_relation)
                    .await
                    .map_err(|e| EventError::Custom(e.to_string()))?;
                self.aggregations
                    .update_relation(&thread.event_id, current_relations)
                    .await
                    .map_err(|e| EventError::Custom(e.to_string()))?;
            }
            Relation::References(reference) => {
                let mut current_relations = self
                    .aggregations
                    .event_relation(&reference.event_id)
                    .await
                    .map_err(|e| EventError::Custom(e.to_string()))?;

                let event_relation = EventRelation {
                    stream_token,
                    topological_token,
                    relation_type: RelationType::Reference,
                    parent: reference.event_id.to_owned(),
                    event_id: current_event_id.to_owned(),
                };

                let mut reference_state = current_relations.references.take().unwrap_or_default();

                reference_state
                    .referenced_ids
                    .insert(current_event_id.to_owned());
                current_relations.references = Some(reference_state);

                self.aggregations
                    .store_relation(event_relation)
                    .await
                    .map_err(|e| EventError::Custom(e.to_string()))?;
                self.aggregations
                    .update_relation(&reference.event_id, current_relations)
                    .await
                    .map_err(|e| EventError::Custom(e.to_string()))?;
            }
            _ => return Ok(()),
        }

        Ok(())
    }
}
