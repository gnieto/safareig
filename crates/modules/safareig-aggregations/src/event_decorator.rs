use std::sync::Arc;

use safareig::{
    client::room::events::RoomEventDecorator, core::events::Event, storage::EventStorage,
};
use safareig_core::{
    ruma::{EventId, OwnedEventId},
    Inject,
};

use crate::storage::AggregationStorage;

#[derive(Inject)]
pub struct AggregatorEventDecorator {
    aggregations: Arc<dyn AggregationStorage>,
    events: Arc<dyn EventStorage>,
}

#[async_trait::async_trait]
impl RoomEventDecorator for AggregatorEventDecorator {
    async fn decorate_events(&self, events: &mut [Event]) {
        let owned_event_ids: Vec<OwnedEventId> = events
            .iter()
            .map(|e| e.event_ref.owned_event_id())
            .collect();
        let event_ids: Vec<&EventId> = owned_event_ids.iter().map(|e| e.as_ref()).collect();

        let mut aggregations_by_event = self
            .aggregations
            .events_aggregations(event_ids.as_slice())
            .await
            .unwrap_or_default();

        for event in events.iter_mut() {
            if let Some(aggregation) = aggregations_by_event.remove(event.event_ref.event_id()) {
                let relations = aggregation.to_relations(self.events.as_ref()).await;
                event.bundle_relations(relations);
            }
        }
    }
}
