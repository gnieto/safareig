pub mod event_relations {

    use safareig_core::ruma::{
        api::{metadata, request, response, Direction, Metadata},
        events::{relation::RelationType, AnyTimelineEvent, TimelineEventType},
        serde::Raw,
        OwnedEventId, OwnedRoomId, UInt,
    };
    use serde::Serialize;

    #[derive(Serialize, Clone, Debug)]
    pub struct Relation {
        #[serde(rename = "type")]
        kind: String,
        sender: String,
        content: serde_json::Value,
    }

    const METADATA: Metadata = metadata! {
        method: GET,
        rate_limited: false,
        authentication: AccessToken,
        history: {
            unstable => "/_matrix/client/unstable/rooms/:room_id/relations/:event_id/:relation_type/:event_type",
            1.3 => "/_matrix/client/v1/rooms/:room_id/relations/:event_id/:relation_type/:event_type",
        }
    };

    #[request(error = safareig_core::ruma::api::client::Error)]
    pub struct Request {
        /// RoomId
        #[ruma_api(path)]
        pub room_id: OwnedRoomId,

        /// EventId
        #[ruma_api(path)]
        pub event_id: OwnedEventId,

        // TODO: Add ad-hoc type for relation type
        /// RelationType
        #[ruma_api(path)]
        pub relation_type: RelationType,

        // TODO: Add ad-hoc type for relation type
        /// RelationType
        #[ruma_api(path)]
        pub event_type: TimelineEventType,

        /// The direction to return events from.
        #[ruma_api(query)]
        #[serde(default = "default_direction")]
        pub dir: Direction,

        /// `from` token. Used for pagination like other matrix endpoints
        #[ruma_api(query)]
        pub from: Option<String>,

        /// `to` token. Used for pagination like other matrix endpoints
        #[ruma_api(query)]
        pub to: Option<String>,

        /// `to` token. Used for pagination like other matrix endpoints
        #[ruma_api(query)]
        pub limit: Option<UInt>,
    }

    impl Request {
        /// Creates a new `Request` with the given room ID and parent event ID.
        pub fn new(
            room_id: OwnedRoomId,
            event_id: OwnedEventId,
            relation_type: RelationType,
            event_type: TimelineEventType,
        ) -> Self {
            Self {
                room_id,
                event_id,
                relation_type,
                event_type,
                dir: Direction::default(),
                from: None,
                to: None,
                limit: None,
            }
        }
    }

    #[response(error = safareig_core::ruma::api::client::Error)]
    pub struct Response {
        /// Details of the requested event.
        #[serde(skip_serializing_if = "Vec::is_empty")]
        pub chunk: Vec<Raw<AnyTimelineEvent>>,

        /// Details of the requested event.
        #[serde(skip_serializing_if = "Option::is_none")]
        pub prev_batch: Option<String>,

        /// Details of the requested event.
        #[serde(skip_serializing_if = "Option::is_none")]
        pub next_batch: Option<String>,

        pub original_event: Raw<AnyTimelineEvent>,
    }

    fn default_direction() -> Direction {
        Direction::Backward
    }

    impl Response {
        pub fn new(event: Raw<AnyTimelineEvent>) -> Self {
            Self {
                original_event: event,
                prev_batch: Default::default(),
                next_batch: Default::default(),
                chunk: Default::default(),
            }
        }
    }
}
