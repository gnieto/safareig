use std::{collections::BTreeMap, ops::Deref};

use async_trait::async_trait;
use safareig_core::{
    pagination::{Pagination, PaginationResponse},
    ruma::{events::relation::RelationType, EventId, OwnedEventId},
    storage::{Result, StorageError},
    StreamToken,
};
use safareig_sqlx::{
    decode_from_str, decode_i64_to_u64, DirectionExt, PaginatedIterator, RangeWrapper,
};
use sea_query::{Alias, Expr, Iden, Query, SqliteQueryBuilder};
use sea_query_binder::SqlxBinder;
use sqlx::{Pool, Row, Sqlite};

use super::{AggregationStorage, EventRelation, ServerSideAggregation};

pub struct SqlxAggregationStorage {
    pool: Pool<Sqlite>,
}

impl SqlxAggregationStorage {
    // TODO: Generic or dyn dispatch
    pub fn new(pool: Pool<Sqlite>) -> Self {
        Self { pool }
    }
}

#[async_trait]
impl AggregationStorage for SqlxAggregationStorage {
    async fn event_relation(&self, event: &EventId) -> Result<ServerSideAggregation> {
        let query_str = "SELECT * FROM event_aggregations WHERE event_id = ?1";

        let aggregation = sqlx::query(query_str)
            .bind(event.as_str())
            .fetch_optional(&self.pool)
            .await?
            .map::<Result<_>, _>(|row| {
                Ok(serde_json::from_value(
                    row.try_get::<serde_json::Value, _>("aggregation")?,
                )?)
            })
            .transpose()?;

        Ok(aggregation.unwrap_or_default())
    }

    async fn update_relation(
        &self,
        event: &EventId,
        aggregations: ServerSideAggregation,
    ) -> Result<()> {
        let value = serde_json::to_value(&aggregations)?;
        let sql = "INSERT INTO event_aggregations (event_id, aggregation)
        VALUES(?1, ?2)
        ON CONFLICT DO UPDATE SET aggregation = ?2
        ";

        sqlx::query(sql)
            .bind(event.as_str())
            .bind(value)
            .execute(&self.pool)
            .await?;

        Ok(())
    }

    async fn events_aggregations<'a>(
        &self,
        events: &'a [&'a EventId],
    ) -> Result<BTreeMap<OwnedEventId, ServerSideAggregation>> {
        let params = format!("?{}", ", ?".repeat(events.len().saturating_sub(1)));
        let query_str = format!("SELECT * FROM event_aggregations WHERE event_id IN ( {params} )");

        let mut query = sqlx::query(&query_str);
        for event_id in events {
            query = query.bind(event_id.as_str());
        }

        let aggregations = query
            .fetch_all(&self.pool)
            .await?
            .into_iter()
            .map(|row| {
                let event_id = decode_from_str(row.try_get::<&str, _>("event_id")?)?;
                let aggregation =
                    serde_json::from_value(row.try_get::<serde_json::Value, _>("aggregation")?)?;

                Ok((event_id, aggregation))
            })
            .collect::<Result<BTreeMap<OwnedEventId, ServerSideAggregation>>>()?;

        Ok(aggregations)
    }

    async fn store_relation(&self, event_relation: EventRelation) -> Result<()> {
        let depth = i64::try_from(event_relation.topological_token.depth())
            .map_err(|_| StorageError::CorruptedData)?;
        let subsequence = i64::try_from(event_relation.topological_token.subsequence())
            .map_err(|_| StorageError::CorruptedData)?;

        let sql = "INSERT INTO event_relations (parent, event_id, relation_type, topological_depth, topological_sub, stream_token)
        VALUES(?1, ?2, ?3, ?4, ?5, ?6)
        ";

        sqlx::query(sql)
            .bind(event_relation.parent.as_str())
            .bind(event_relation.event_id.as_str())
            .bind(event_relation.relation_type.as_str())
            .bind(depth)
            .bind(subsequence)
            .bind(event_relation.stream_token)
            .execute(&self.pool)
            .await?;

        Ok(())
    }

    async fn relations_stream(
        &self,
        parent: &EventId,
        _relation_type: RelationType,
        query: Pagination<StreamToken>,
    ) -> Result<PaginationResponse<StreamToken, OwnedEventId>> {
        let wrapper = RangeWrapper(query.range().map(|t| *t.deref()));
        let (sql, values) = Query::select()
            .columns([EventRelations::EventId, EventRelations::StreamToken])
            .from(EventRelations::Table)
            .and_where(Expr::col(EventRelations::Parent).eq(parent.as_str()))
            .cond_where(wrapper.condition(Expr::col(Alias::new("stream_token"))))
            .order_by(EventRelations::StreamToken, query.direction().order())
            .build_sqlx(SqliteQueryBuilder);

        let rows = sqlx::query_with(&sql, values).fetch_all(&self.pool).await?;

        let iterator = PaginatedIterator::new(rows.into_iter(), |row| {
            let token = decode_i64_to_u64(row.try_get::<i64, _>("stream_token")?)?;
            let event_id = decode_from_str(row.get::<&str, _>("event_id"))?;
            let stream_token = StreamToken::from(token);

            Ok((stream_token, event_id))
        });

        iterator.into_pagination_response()
    }
}

#[derive(Iden)]
pub enum EventRelations {
    Table,
    EventId,
    StreamToken,
    Parent,
}
