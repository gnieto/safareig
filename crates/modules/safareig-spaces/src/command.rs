use safareig::client::room::loader::RoomLoader;
use safareig_core::{
    auth::Identity,
    command::Command,
    pagination::{Boundary, Limit, Pagination},
    ruma::{api::client::space::get_hierarchy, UInt},
    Inject,
};

use crate::{HierarchyExplorer, HierarchyState};

#[derive(Inject)]
pub struct GetSpaceHierarchyCommand {
    explorer: HierarchyExplorer,
    room_loader: RoomLoader,
}

#[async_trait::async_trait]
impl Command for GetSpaceHierarchyCommand {
    type Input = get_hierarchy::v1::Request;
    type Output = get_hierarchy::v1::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let _ = self
            .room_loader
            .get_for_user(&input.room_id, id.user())
            .await?;

        let max_depth = max_depth_or_default(input.max_depth);
        let token = input
            .from
            .map(|token| HierarchyState::parse(token.as_str(), input.suggested_only, max_depth))
            .transpose()?
            .unwrap_or_else(|| {
                HierarchyState::new(
                    input.suggested_only,
                    max_depth_or_default(input.max_depth),
                    input.room_id,
                )
            });

        let limit = input
            .limit
            .and_then(|limit| u32::try_from(limit).ok())
            .unwrap_or(100u32);
        let range = (Boundary::Exclusive(token), Boundary::Unlimited);
        let pagination = Pagination::new(range.into(), ()).with_limit(Limit::from(limit));

        let response = self.explorer.explore(&id, pagination).await?;
        let next_token = response
            .next()
            .and_then(|token| {
                if token.is_terminal() {
                    None
                } else {
                    Some(token)
                }
            })
            .map(|token| token.encode())
            .transpose()?;
        let rooms = response.records();

        Ok(get_hierarchy::v1::Response {
            next_batch: next_token,
            rooms,
        })
    }
}

fn max_depth_or_default(max_depth: Option<UInt>) -> u8 {
    max_depth
        .and_then(|depth| depth.try_into().ok())
        .unwrap_or(u8::MAX)
}
