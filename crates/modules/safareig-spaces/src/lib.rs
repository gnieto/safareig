pub mod command;
mod error;
pub mod module;

use std::{collections::HashSet, fmt::Display, sync::Arc};

use base64::decode;
use error::SpacesError;
use ruma_common::space::SpaceRoomJoinRule;
use safareig::{
    client::room::{loader::RoomLoader, RoomError},
    core::events::Event,
};
use safareig_core::{
    auth::Identity,
    error::error_chain,
    events::EventError,
    id::RoomIdExt,
    pagination::{Pagination, PaginationResponse, Token},
    ruma::{
        api::{client::space::SpaceHierarchyRoomsChunk, federation::space::get_hierarchy},
        events::{space::child::SpaceChildEventContent, TimelineEventType},
        OwnedRoomId,
    },
    Inject, ServerInfo,
};
use safareig_federation::client::ClientBuilder;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Eq)]
pub struct HierarchyState {
    visited_spaces: HashSet<OwnedRoomId>,
    suggested_only: bool,
    max_depth: u8,
    ord: u8,
    pending: Vec<SpaceToExplore>,
}

#[derive(Serialize, Deserialize, Clone, Eq, PartialEq)]
pub struct SpaceToExplore {
    depth: u8,
    room_id: OwnedRoomId,
}

impl HierarchyState {
    pub fn new(suggested_only: bool, max_depth: u8, root: OwnedRoomId) -> Self {
        Self {
            visited_spaces: HashSet::new(),
            suggested_only,
            max_depth,
            ord: 0,
            pending: vec![SpaceToExplore {
                depth: 0,
                room_id: root,
            }],
        }
    }

    pub fn parse(token: &str, suggested_only: bool, max_depth: u8) -> Result<Self, SpacesError> {
        let base64_decoded = decode(token).map_err(|e| {
            tracing::error!(err = e.to_string().as_str(), "Invalid base64 from token");
            SpacesError::InvalidToken
        })?;

        let hierarchy_state: Self = serde_json::from_slice(&base64_decoded).map_err(|e| {
            tracing::error!(
                err = e.to_string().as_str(),
                "Could not deserialize incoming token"
            );
            SpacesError::InvalidToken
        })?;

        if !hierarchy_state.is_valid(suggested_only, max_depth) {
            return Err(SpacesError::InvalidState);
        }

        Ok(hierarchy_state)
    }

    pub fn encode(&self) -> Result<String, SpacesError> {
        let json = serde_json::to_string(&self).map_err(|_| SpacesError::InvalidState)?;

        Ok(base64::encode(json))
    }

    pub fn is_terminal(&self) -> bool {
        self.pending.is_empty()
    }

    fn is_valid(&self, suggested_only: bool, max_depth: u8) -> bool {
        self.suggested_only == suggested_only && self.max_depth == max_depth
    }
}

impl Display for HierarchyState {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self.encode() {
            Ok(encoded) => write!(f, "{encoded}"),
            Err(_) => unimplemented!(),
        }
    }
}

impl Ord for HierarchyState {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.ord.cmp(&other.ord)
    }
}

impl PartialEq for HierarchyState {
    fn eq(&self, other: &Self) -> bool {
        self.visited_spaces == other.visited_spaces
            && self.suggested_only == other.suggested_only
            && self.max_depth == other.max_depth
            && self.ord == other.ord
    }
}

impl PartialOrd for HierarchyState {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.ord.partial_cmp(&other.ord)
    }
}

impl Token for HierarchyState {}

#[async_trait::async_trait]
pub trait HierachyLoader: Send + Sync {
    async fn load_hierarchy(
        &self,
        state: &HierarchyState,
        space: &SpaceToExplore,
        identity: &Identity,
    ) -> Result<SpaceHierarchy, SpacesError>;
}

pub struct SpaceHierarchy {
    chunk: SpaceHierarchyRoomsChunk,
    spaces: Vec<SpaceToExplore>,
}

struct RoomHierarchyLoader {
    room_loader: RoomLoader,
}

#[async_trait::async_trait]
impl HierachyLoader for RoomHierarchyLoader {
    async fn load_hierarchy(
        &self,
        _: &HierarchyState,
        space: &SpaceToExplore,
        identity: &Identity,
    ) -> Result<SpaceHierarchy, SpacesError> {
        let room_id = space.room_id.to_owned();
        let current_depth = space.depth;

        let room = match identity {
            Identity::Device(_, user) => self.room_loader.get_for_user(&room_id, user).await?,
            Identity::Federation(_) => self
                .room_loader
                .get(&room_id)
                .await?
                .ok_or_else(|| RoomError::RoomNotFound(room_id.to_string()))?,
            _ => {
                return Err(SpacesError::RoomError(RoomError::RoomNotFound(
                    room_id.to_string(),
                )));
            }
        };

        let state = room.state_at_leaves().await?;
        let summary = room.room_summary().await?;

        let space_childs = room
            .events_for_type(TimelineEventType::SpaceChild, Some(&state))
            .await?;

        let mut childs: Vec<(Event, SpaceChildEventContent)> = space_childs
            .into_iter()
            .filter_map(|event| {
                let content = serde_json::from_value(event.content.clone()).ok();

                content.map(|content| (event, content))
            })
            .collect();
        childs.sort_by(|(_, a), (_, b)| a.order.cmp(&b.order));

        let mut rooms_to_explore = vec![];
        let children_state = childs
            .into_iter()
            .map(|(event, _)| {
                let room_id = event.state_key.as_ref().unwrap().parse().unwrap();
                rooms_to_explore.push(SpaceToExplore {
                    depth: current_depth + 1,
                    room_id,
                });
                event.to_hierarchy_child_event()
            })
            .collect::<Result<_, EventError>>()
            .map_err(RoomError::from)?;

        let chunk = SpaceHierarchyRoomsChunk {
            canonical_alias: summary.canoncical_alias,
            name: summary.name,
            num_joined_members: summary.num_joined_members.into(),
            room_id: summary.room_id,
            topic: summary.topic,
            world_readable: summary.world_readable,
            guest_can_join: summary.guest_can_join,
            avatar_url: summary.avatar_url,
            join_rule: SpaceRoomJoinRule::Public,
            room_type: summary.room_type,
            children_state,
        };

        Ok(SpaceHierarchy {
            chunk,
            spaces: rooms_to_explore,
        })
    }
}

impl RoomHierarchyLoader {
    pub fn new(room_loader: RoomLoader) -> Self {
        Self { room_loader }
    }
}

struct FederatedHierarchyLoader {
    client_builder: Arc<ClientBuilder>,
}

#[async_trait::async_trait]
impl HierachyLoader for FederatedHierarchyLoader {
    async fn load_hierarchy(
        &self,
        state: &HierarchyState,
        space: &SpaceToExplore,
        _: &Identity,
    ) -> Result<SpaceHierarchy, SpacesError> {
        let room_server = space.room_id.try_server_name()?;
        let client = self.client_builder.client_for(room_server).await?;

        let request = get_hierarchy::v1::Request {
            room_id: space.room_id.to_owned(),
            suggested_only: state.suggested_only,
        };
        let response = client.auth_request(request).await?;

        let chunk = SpaceHierarchyRoomsChunk {
            canonical_alias: response.room.canonical_alias,
            name: response.room.name,
            num_joined_members: response.room.num_joined_members,
            room_id: response.room.room_id,
            topic: response.room.topic,
            world_readable: response.room.world_readable,
            guest_can_join: response.room.guest_can_join,
            avatar_url: response.room.avatar_url,
            join_rule: SpaceRoomJoinRule::Public,
            room_type: response.room.room_type,
            children_state: response.room.children_state,
        };

        let spaces_to_explore = response
            .children
            .iter()
            .map(|children| SpaceToExplore {
                room_id: children.room_id.to_owned(),
                depth: space.depth + 1,
            })
            .collect();

        Ok(SpaceHierarchy {
            chunk,
            spaces: spaces_to_explore,
        })
    }
}

impl FederatedHierarchyLoader {
    pub fn new(client_builder: Arc<ClientBuilder>) -> Self {
        Self { client_builder }
    }
}

pub struct FederationAwareHierarchyLoader {
    local: RoomHierarchyLoader,
    federated: FederatedHierarchyLoader,
    server_info: ServerInfo,
}

#[async_trait::async_trait]
impl HierachyLoader for FederationAwareHierarchyLoader {
    async fn load_hierarchy(
        &self,
        state: &HierarchyState,
        space: &SpaceToExplore,
        identity: &Identity,
    ) -> Result<SpaceHierarchy, SpacesError> {
        if self
            .server_info
            .is_local_server(space.room_id.try_server_name()?)
        {
            self.local.load_hierarchy(state, space, identity).await
        } else {
            self.federated.load_hierarchy(state, space, identity).await
        }
    }
}

impl FederationAwareHierarchyLoader {
    pub(crate) fn new(
        local: RoomHierarchyLoader,
        federated: FederatedHierarchyLoader,
        server_info: ServerInfo,
    ) -> Self {
        Self {
            local,
            federated,
            server_info,
        }
    }
}

#[derive(Inject, Clone)]
pub struct HierarchyExplorer {
    loader: Arc<dyn HierachyLoader>,
}

impl HierarchyExplorer {
    async fn explore(
        &self,
        identity: &Identity,
        pagination: Pagination<HierarchyState, ()>,
    ) -> Result<PaginationResponse<HierarchyState, SpaceHierarchyRoomsChunk>, SpacesError> {
        let initial_state = pagination
            .from()
            .clone()
            .token()
            .ok_or(SpacesError::InvalidToken)?;
        let mut working_state = initial_state.clone();
        let mut records = vec![];

        loop {
            if records.len() >= pagination.limit().unwrap() as usize {
                break;
            }

            if working_state.pending.is_empty() {
                break;
            }
            let room_to_explore = working_state.pending.pop().unwrap();

            if working_state
                .visited_spaces
                .contains(&room_to_explore.room_id)
                || room_to_explore.depth >= working_state.max_depth
            {
                continue;
            }

            match self
                .loader
                .load_hierarchy(&working_state, &room_to_explore, identity)
                .await
            {
                Ok(hierarchy) => {
                    records.push(hierarchy.chunk);
                    working_state
                        .pending
                        .extend(hierarchy.spaces.into_iter().rev());
                }
                Err(e) => {
                    tracing::error!(
                        err = error_chain(&e).as_str(),
                        "could not load hierarchy information",
                    );
                }
            }

            working_state.visited_spaces.insert(room_to_explore.room_id);
        }

        Ok(PaginationResponse::new(
            records,
            Some(initial_state.clone()),
            Some(working_state),
        ))
    }
}
