use safareig::client::room::RoomError;
use safareig_core::{
    id::IdError,
    ruma::{
        api::client::error::{ErrorBody, ErrorKind},
        exports::http::StatusCode,
    },
};
use safareig_federation::FederationError;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum SpacesError {
    #[error("Provided token could not be parsed")]
    InvalidToken,
    #[error("Pagination token is out of sync. Suggested only or max depth changed.")]
    InvalidState,
    #[error("Room error: {0}")]
    RoomError(#[from] RoomError),
    #[error("Federation error: {0}")]
    FederationError(#[from] FederationError),
    #[error("Id error")]
    IdError(#[from] IdError),
}

impl From<SpacesError> for safareig_core::ruma::api::client::Error {
    fn from(e: SpacesError) -> Self {
        match e {
            SpacesError::InvalidToken | SpacesError::InvalidState => {
                safareig_core::ruma::api::client::Error {
                    status_code: StatusCode::BAD_REQUEST,
                    body: ErrorBody::Standard {
                        kind: ErrorKind::InvalidParam,
                        message: e.to_string(),
                    },
                }
            }
            SpacesError::RoomError(error) => error.into(),
            SpacesError::FederationError(error) => error.into(),
            SpacesError::IdError(error) => error.into(),
        }
    }
}
