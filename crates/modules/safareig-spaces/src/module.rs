use std::sync::Arc;

use safareig::client::room::loader::RoomLoader;
use safareig_core::{
    command::{Container, Module, ServiceCollectionExtension},
    config::HomeserverConfig,
    ddi::{ServiceCollectionExt, ServiceProvider, ServiceResolverExt},
    ServerInfo,
};
use safareig_federation::client::ClientBuilder;

use crate::{
    command::GetSpaceHierarchyCommand, FederationAwareHierarchyLoader, HierachyLoader,
    HierarchyExplorer, RoomHierarchyLoader,
};

#[derive(Default)]
pub struct SpacesModule;

#[async_trait::async_trait]
impl<R: safareig_core::command::Router> Module<R> for SpacesModule {
    fn register(&self, container: &mut Container, router: &mut R) {
        container.service_collection.service_factory(
            |rl: &RoomLoader, config: &Arc<HomeserverConfig>, sp: &ServiceProvider| {
                let local = RoomHierarchyLoader::new(rl.clone());
                let loader: Arc<dyn HierachyLoader> = if config.federation.enabled {
                    let client_builder = sp.get::<Arc<ClientBuilder>>()?;
                    let federated = crate::FederatedHierarchyLoader::new(client_builder.clone());
                    let server_info = sp.get::<ServerInfo>()?;

                    let federation_aware =
                        FederationAwareHierarchyLoader::new(local, federated, server_info.clone());

                    Arc::new(federation_aware)
                } else {
                    Arc::new(local)
                };

                Ok(loader)
            },
        );
        container.service_collection.register::<HierarchyExplorer>();

        container.inject_endpoint::<_, GetSpaceHierarchyCommand, _>(router);
    }
}
