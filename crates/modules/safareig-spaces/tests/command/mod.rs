use safareig::client::room::command::{
    create_room::CreateRoomCommand, send_state_event::SendStateEventCommand,
};
use safareig_core::{
    auth::Identity,
    command::{Command, Module},
    ruma::{
        api::client::{
            room::{
                create_room::{
                    self,
                    v3::{CreationContent, RoomPreset},
                },
                Visibility,
            },
            space,
            state::send_state_event,
        },
        events::{
            room::history_visibility::{HistoryVisibility, RoomHistoryVisibilityEventContent},
            space::child::SpaceChildEventContent,
            AnyStateEventContent, EmptyStateKey, EventContent, InitialStateEvent, StateEventType,
        },
        exports::http::StatusCode,
        room::RoomType,
        serde::Raw,
        OwnedRoomId, RoomId,
    },
};
use safareig_spaces::{command::GetSpaceHierarchyCommand, module::SpacesModule};
use safareig_testing::TestingApp;

#[tokio::test]
pub async fn it_paginates_spaces() {
    let app = testing_app().await;
    let id = app.register().await;
    let root = prepare_spaces(&app, &id).await;

    let cmd = app.command::<GetSpaceHierarchyCommand>();

    let mut token = None;
    let mut spaces = vec![];
    loop {
        let request = space::get_hierarchy::v1::Request {
            from: token,
            room_id: root.clone(),
            limit: Some(1u32.into()),
            max_depth: Some(2u32.into()),
            suggested_only: false,
        };

        let result = cmd.execute(request, id.clone()).await.unwrap();

        if result.rooms.is_empty() {
            break;
        }
        token = result.next_batch;
        spaces.extend(result.rooms.into_iter());
    }

    assert_eq!(5, spaces.len());
    assert_eq!(root, spaces[0].room_id);
    assert_eq!("child0", spaces[1].name.as_ref().unwrap());
    let state = spaces[1].children_state[0].deserialize().unwrap();
    assert_eq!(StateEventType::SpaceChild, state.content.event_type());
}

#[tokio::test]
pub async fn private_spaces_can_not_be_read_by_users_which_are_not_members() {
    let app = testing_app().await;
    let id = app.register().await;
    let non_member = app.register().await;
    let root = prepare_small_private_space(&app, &id).await;

    let request = space::get_hierarchy::v1::Request {
        from: None,
        room_id: root.clone(),
        limit: Some(1u32.into()),
        max_depth: Some(2u32.into()),
        suggested_only: false,
    };

    let cmd = app.command::<GetSpaceHierarchyCommand>();
    let result = cmd.execute(request, non_member.clone()).await;

    assert!(result.is_err());
    let result = result.err().unwrap();
    assert_eq!(result.status_code, StatusCode::FORBIDDEN);
}

#[tokio::test]
pub async fn public_spaces_can_be_read_by_any_user() {
    let app = testing_app().await;
    let id = app.register().await;
    let non_member = app.register().await;
    let root = prepare_small_public_space(&app, &id).await;

    let request = space::get_hierarchy::v1::Request {
        from: None,
        room_id: root.clone(),
        limit: Some(1u32.into()),
        max_depth: Some(2u32.into()),
        suggested_only: false,
    };

    let cmd = app.command::<GetSpaceHierarchyCommand>();
    let result = cmd.execute(request, non_member.clone()).await.unwrap();

    assert_eq!(1, result.rooms.len());
}

#[tokio::test]
pub async fn ignores_rooms_with_depth_greater_than_max_depth() {
    let app = testing_app().await;
    let id = app.register().await;
    let root = prepare_spaces(&app, &id).await;

    let cmd = app.command::<GetSpaceHierarchyCommand>();

    let mut token = None;
    let mut spaces = vec![];
    loop {
        let request = space::get_hierarchy::v1::Request {
            from: token,
            room_id: root.clone(),
            limit: Some(1u32.into()),
            max_depth: Some(1u32.into()),
            suggested_only: false,
        };

        let result = cmd.execute(request, id.clone()).await.unwrap();

        if result.rooms.is_empty() {
            break;
        }
        token = result.next_batch;
        spaces.extend(result.rooms.into_iter());
    }

    assert_eq!(1, spaces.len());
    assert_eq!(root, spaces[0].room_id);
}

#[tokio::test]
pub async fn it_breaks_cycles_in_recursive_spaces() {
    let app = testing_app().await;
    let id = app.register().await;
    let non_member = app.register().await;

    let root = create_public_space(&app, &id, "root").await;
    let child = create_public_space(&app, &id, "child").await;
    let grandson = create_public_space(&app, &id, "grandson").await;
    assing_child_to_space(&app, &id, &root, &child, "order").await;
    assing_child_to_space(&app, &id, &child, &grandson, "order").await;
    assing_child_to_space(&app, &id, &grandson, &root, "order").await;

    let request = space::get_hierarchy::v1::Request {
        from: None,
        room_id: root.clone(),
        limit: Some(5u32.into()),
        max_depth: Some(5u32.into()),
        suggested_only: false,
    };

    let cmd = app.command::<GetSpaceHierarchyCommand>();
    let result = cmd.execute(request, non_member.clone()).await.unwrap();

    assert_eq!(3, result.rooms.len());
}

#[tokio::test]
pub async fn it_loads_spaces_from_remote_servers() {}

#[tokio::test]
pub async fn remote_non_public_spaces_gets_filtered() {}

async fn testing_app() -> TestingApp {
    TestingApp::with_modules(vec![Box::new(SpacesModule)]).await
}

async fn prepare_spaces(app: &TestingApp, id: &Identity) -> OwnedRoomId {
    let root = create_public_space(app, id, "root").await;

    for i in 0..4 {
        let name = format!("child{i}");
        // Assign inverse insertion order for sorting
        let order = format!("{}", 10 - i);
        let child = create_public_space(app, id, &name).await;
        assing_child_to_space(app, id, &root, &child, &order).await;

        for _j in 0..4 {
            let _grandson = format!("grandson{i}");
            // Assign inverse insertion order for sorting
            let order = format!("grandson{}", 10 - i);
            let grandson = create_public_space(app, id, &name).await;
            assing_child_to_space(app, id, &child, &grandson, &order).await;
        }
    }

    root
}

async fn prepare_small_private_space(app: &TestingApp, id: &Identity) -> OwnedRoomId {
    create_space(app, id, "root", RoomPreset::PrivateChat).await
}

async fn prepare_small_public_space(app: &TestingApp, id: &Identity) -> OwnedRoomId {
    create_space(app, id, "root", RoomPreset::PublicChat).await
}

async fn create_public_space(app: &TestingApp, id: &Identity, name: &str) -> OwnedRoomId {
    create_space(app, id, name, RoomPreset::PublicChat).await
}

async fn create_space(
    app: &TestingApp,
    id: &Identity,
    name: &str,
    preset: RoomPreset,
) -> OwnedRoomId {
    let mut initial_state = vec![];

    if let RoomPreset::PublicChat = preset {
        let content = RoomHistoryVisibilityEventContent {
            history_visibility: HistoryVisibility::WorldReadable,
        };
        let world_readable = InitialStateEvent {
            content,
            state_key: EmptyStateKey,
        };
        let raw = Raw::new(&world_readable).unwrap();
        initial_state.push(raw.cast());
    }

    let create_room_request = create_room::v3::Request {
        creation_content: Some(
            Raw::new(&CreationContent {
                federate: false,
                predecessor: None,
                room_type: Some(RoomType::Space),
            })
            .unwrap(),
        ),
        power_level_content_override: None,
        initial_state,
        invite: vec![],
        invite_3pid: vec![],
        is_direct: false,
        name: Some(name.to_string()),
        preset: Some(preset),
        room_alias_name: None,
        room_version: None,
        topic: Some("Description".to_string()),
        visibility: Visibility::Public,
    };

    let cmd = app.command::<CreateRoomCommand>();
    let result = cmd.execute(create_room_request, id.clone()).await.unwrap();

    result.room_id
}

async fn assing_child_to_space(
    app: &TestingApp,
    id: &Identity,
    parent: &RoomId,
    child: &RoomId,
    order: &str,
) {
    let request = send_state_event::v3::Request {
        room_id: parent.to_owned(),
        event_type: StateEventType::SpaceChild,
        state_key: child.to_string(),
        body: Raw::new(&AnyStateEventContent::SpaceChild(SpaceChildEventContent {
            via: Vec::new(),
            order: Some(order.to_string()),
            suggested: false,
        }))
        .unwrap(),
        timestamp: None,
    };

    let cmd = app.command::<SendStateEventCommand>();
    let _result = cmd.execute(request, id.clone()).await.unwrap();
}
