use std::sync::Arc;

use safareig::{
    client::room::RoomStream,
    storage::{Checkpoint, CheckpointStorage, FilterStorage, RoomStorage},
};
use safareig_core::{
    bus::Bus,
    command::{Container, Module, Router},
    ddi::{ServiceCollectionExt, ServiceProvider, ServiceResolverExt},
};
use safareig_ignored_users::IgnoreUsersRepository;
use safareig_sync_api::SyncExtension;

use crate::command::SyncCommand;

#[derive(Clone, Default)]
pub struct SyncModule {}

#[async_trait::async_trait]
impl<R: Router> Module<R> for SyncModule {
    fn register(&self, container: &mut Container, router: &mut R) {
        container
            .service_collection
            .service_factory(|sp: &ServiceProvider| {
                let mut extensions: Vec<Arc<dyn SyncExtension>> = sp
                    .get_all::<Arc<dyn SyncExtension>>()?
                    .into_iter()
                    .map(|t| t.1.clone())
                    .collect();
                extensions.sort_by_key(|x| x.priority());

                Ok(extensions)
            });

        container.register_lazy_endpoint(
            |sp: &ServiceProvider| {
                let cmd = SyncCommand::new(
                    sp.get::<Bus>()?.clone(),
                    sp.get::<Arc<dyn RoomStorage>>()?.clone(),
                    sp.get::<RoomStream>()?.clone(),
                    sp.get::<Arc<dyn FilterStorage>>()?.clone(),
                    sp.get::<Arc<dyn CheckpointStorage<Checkpoint>>>()?.clone(),
                    sp.get::<Vec<Arc<dyn SyncExtension>>>()?.clone(),
                    sp.get::<Arc<dyn IgnoreUsersRepository>>().ok().cloned(),
                );

                Ok(cmd)
            },
            router,
        );
    }
}
