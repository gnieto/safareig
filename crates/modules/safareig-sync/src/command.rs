use std::{
    collections::{HashMap, HashSet},
    convert::TryFrom,
    sync::Arc,
};

use async_trait::async_trait;
use futures::{select, FutureExt};
use safareig::{
    client::room::{RoomError, RoomStream},
    core::waker::Waker,
    storage::{Checkpoint, CheckpointStorage, FilterStorage, RoomStorage},
};
use safareig_core::{
    auth::Identity,
    bus::Bus,
    command::Command,
    ruma::{
        api::client::{
            filter::FilterDefinition,
            sync::{sync_events, sync_events::v3::Filter},
        },
        events::room::member::MembershipState,
        UserId,
    },
    storage::StorageError,
    Inject, StreamToken,
};
use safareig_ignored_users::IgnoreUsersRepository;
use safareig_sync_api::{
    context::{Context, SyncMode, UserRoomsMemberhip},
    error::SyncError,
    SyncExtension, SyncToken,
};
use tokio::time::sleep;
use tracing::instrument;

#[derive(Inject)]
pub struct SyncCommand {
    bus: Bus,
    rooms: Arc<dyn RoomStorage>,
    room_stream: RoomStream,
    filters: Arc<dyn FilterStorage>,
    checkpoints: Arc<dyn CheckpointStorage<Checkpoint>>,
    extensions: Vec<Arc<dyn SyncExtension>>,
    ignore_users: Option<Arc<dyn IgnoreUsersRepository>>,
}

#[async_trait]
impl Command for SyncCommand {
    type Input = sync_events::v3::Request;
    type Output = sync_events::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let rx = self.bus.receiver();
        let wakeup = Waker::sync(rx, id.user(), id.device());

        let timeout_duration = input.timeout.unwrap_or_else(|| {
            #[cfg(feature = "sytest")]
            return std::time::Duration::from_secs(5);

            #[cfg(not(feature = "sytest"))]
            return std::time::Duration::from_secs(60);
        });
        let mut context = self.sync_context(&id, &input).await?;

        let response = self.sync_response(&mut context).await?;
        let has_content = !response.is_empty();
        if has_content || context.is_initial_sync() {
            return Ok(response);
        }

        select! {
            _ = wakeup.fuse() => {},
            _ = sleep(timeout_duration).fuse() => {},
        };

        let mut context = self.sync_context(&id, &input).await?;
        let response = self.sync_response(&mut context).await?;

        Ok(response)
    }
}

impl SyncCommand {
    async fn sync_context<'a>(
        &'a self,
        id: &'a Identity,
        request: &'a sync_events::v3::Request,
    ) -> Result<Context<'a>, RoomError> {
        let incoming_from = request
            .since
            .as_ref()
            .and_then(|since| SyncToken::try_from(since.as_ref()).ok());
        let from = incoming_from.clone().unwrap_or_default();
        let to = self.current_event_token().await?;
        let user_id = id.user();
        let membership = self.membership_changes(user_id, from.event(), to).await?;
        let filter = self.load_filter(request).await?;
        let sync_mode = SyncMode::new(request, incoming_from);

        let ignored = match &self.ignore_users {
            Some(repository) => match repository.ignored_users(user_id).await {
                Err(error) => {
                    tracing::error!(?error, "could not get ingored user list");
                    HashSet::new()
                }
                Ok(list) => list,
            },
            None => HashSet::new(),
        };

        Ok(Context {
            request,
            from_token: from,
            to_event_token: to,
            id,
            user_rooms: membership,
            filter,
            sync_mode,
            ignored,
        })
    }

    #[instrument(skip_all, fields(token = context.from_token().to_string()))]
    async fn sync_response<'a>(
        &'a self,
        context: &mut Context<'a>,
    ) -> Result<sync_events::v3::Response, SyncError> {
        // Initialize sync response with an empty/dummy next token. This will make easier to fill the response by
        // section.
        let mut response = sync_events::v3::Response::new("".to_string());
        let mut next_token = context.from_token().clone();

        for sync_extension in &self.extensions {
            sync_extension
                .fill_sync(context, &mut response, &mut next_token)
                .await?;
        }

        response.next_batch = next_token.to_string();

        Ok(response)
    }

    async fn load_filter(
        &self,
        input: &sync_events::v3::Request,
    ) -> Result<FilterDefinition, StorageError> {
        Ok(self
            .load_filter_from_request(input)
            .await?
            .unwrap_or_default())
    }

    async fn load_filter_from_request(
        &self,
        input: &sync_events::v3::Request,
    ) -> Result<Option<FilterDefinition>, StorageError> {
        if let Some(filter) = &input.filter {
            return match filter {
                Filter::FilterId(id) => self.filters.get(id).await,
                Filter::FilterDefinition(filter_def) => Ok(Some(filter_def.clone())),
            };
        }

        Ok(None)
    }

    async fn membership_changes(
        &self,
        user_id: &UserId,
        from: StreamToken,
        to: StreamToken,
    ) -> Result<UserRoomsMemberhip, StorageError> {
        let rooms = self.rooms.membership(user_id).await?;
        let mut memberships = UserRoomsMemberhip {
            joined: HashMap::new(),
            joined_since_last_sync: HashMap::new(),
            invited: HashMap::new(),
            leave: HashMap::new(),
        };

        for (room_id, current_membership) in rooms.into_iter() {
            match current_membership.state {
                MembershipState::Invite => {
                    if current_membership.stream > from && current_membership.stream <= to {
                        memberships
                            .invited
                            .insert(room_id, current_membership.into());
                    }
                }
                MembershipState::Leave | MembershipState::Ban => {
                    if current_membership.stream > from && current_membership.stream <= to {
                        memberships.leave.insert(room_id, current_membership.into());
                    }
                }
                MembershipState::Join => {
                    if current_membership.stream > from && current_membership.stream <= to {
                        memberships
                            .joined_since_last_sync
                            .insert(room_id, current_membership.into());
                    } else {
                        memberships
                            .joined
                            .insert(room_id, current_membership.into());
                    }
                }
                state => tracing::info!("Unhandled membership state: {}", state),
            }
        }

        Ok(memberships)
    }

    /// Get the current sync token. Note that this is kind of a hack, which waits until the
    /// push rules worker reaches some point of the stream.
    /// Until the notification worker does not process the event, the unread notifications counter
    /// won't be updated and the sync can't be flushed.
    /// Clients (at least Element), do not update the notifications counters if this is the unique
    /// data on the room data.
    ///
    /// Synapse applies the push rules evaluation on event reception. The result of both approaches
    /// is that sync responses needs to evaluate all the push rules before an event is added to the
    /// timeline.
    async fn current_event_token(&self) -> Result<StreamToken, StorageError> {
        let checkpoint = self
            .checkpoints
            .get_checkpoint("push_rules") // TODO: This string is hardcoded, but this should be removed togehther with this little hack
            .await?;

        let event = match checkpoint {
            Some(c) => Ok(c.latest),
            None => self.room_stream.current_stream_token().await,
        }?;

        Ok(event)
    }
}

trait SyncResponseExt {
    fn is_empty(&self) -> bool;
}

impl SyncResponseExt for sync_events::v3::Response {
    fn is_empty(&self) -> bool {
        self.rooms.is_empty()
            && self.presence.is_empty()
            && self.account_data.is_empty()
            && self.to_device.is_empty()
            && self.device_lists.is_empty()
    }
}
