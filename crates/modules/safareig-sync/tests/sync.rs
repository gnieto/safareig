use std::{collections::BTreeMap, str::FromStr};

use safareig::{
    client::room::RoomStream,
    core::events::{EventBuilder, EventContentExt},
    storage::{EventOptions, TopologicalTokenWithLazyLoading},
};
use safareig_core::{
    auth::Identity,
    command::Command,
    ruma::{
        api::client::{
            filter::*,
            sync::{
                sync_events,
                sync_events::v3::{JoinedRoom, Rooms},
            },
            to_device::send_event_to_device::{self, v3::Messages},
        },
        events::{
            dummy::ToDeviceDummyEventContent, AnyStrippedStateEvent, AnySyncStateEvent,
            AnyToDeviceEventContent, StateEventType, ToDeviceEventType,
        },
        presence::PresenceState,
        serde::Raw,
        to_device::DeviceIdOrAllDevices,
        TransactionId, UInt, UserId,
    },
    StreamToken,
};
use safareig_sync::SyncCommand;
use safareig_sync_api::SyncToken;
use safareig_testing::TestingApp;
use safareig_to_device::{command::SendToDeviceCommand, module::ToDeviceModule};
use safareig_users::profile::ProfileLoader;
use tokio::time::Duration;

#[tokio::test]
async fn it_receives_new_events_when_someone_talks_on_a_room() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let joiner = db.register().await;
    let _ = db
        .join_room(room.id().to_owned(), joiner.to_owned())
        .await
        .unwrap();

    let token_before_talk: StreamToken = db
        .service::<RoomStream>()
        .current_stream_token()
        .await
        .unwrap();

    db.talk(room.id(), &joiner, "Hi!").await.unwrap();
    db.talk(room.id(), &creator, "How are you?").await.unwrap();
    db.talk(room.id(), &joiner, "Good!").await.unwrap();

    let cmd = db.command::<SyncCommand>();

    let sync_token = SyncToken::new_event(token_before_talk);
    let input = sync_events::v3::Request {
        filter: None,
        full_state: false,
        set_presence: PresenceState::Unavailable,
        timeout: Some(Duration::from_secs(1)),
        since: Some(sync_token.to_string()),
    };

    let result = cmd.execute(input, creator.to_owned()).await.unwrap();
    let joined_room = result.rooms.join.get(room.id()).unwrap();

    assert_eq!(3, joined_room.timeline.events.len());
    // TODO: This test deserves a little bit more of assertions
}

#[tokio::test]
async fn it_receives_an_invitation_state_after_being_invited_to_a_room() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let invitee = db.register().await;
    let token_before_invite: StreamToken = db
        .service::<RoomStream>()
        .current_stream_token()
        .await
        .unwrap();
    db.invite_room(room.id(), &creator, &invitee).await.unwrap();

    let cmd = db.command::<SyncCommand>();

    let sync_token = SyncToken::new_event(token_before_invite);
    let input = sync_events::v3::Request {
        filter: None,
        full_state: false,
        set_presence: PresenceState::Unavailable,
        timeout: Some(Duration::from_secs(1)),
        since: Some(sync_token.to_string()),
    };

    let result = cmd.execute(input, invitee.to_owned()).await.unwrap();
    let invite_room = result.rooms.invite.get(room.id()).unwrap();

    let invitee_member_event =
        invite_room
            .invite_state
            .events
            .iter()
            .find(|raw| match raw.deserialize().unwrap() {
                AnyStrippedStateEvent::RoomMember(state) => state.state_key == *invitee.user(),
                _ => false,
            });

    assert!(invitee_member_event.is_some());
    assert!(invite_room.invite_state.events.len() > 5);
}

#[tokio::test]
async fn it_returns_topological_tokens_on_limited_timelines() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let joiner = db.register().await;
    let _ = db
        .join_room(room.id().to_owned(), joiner.to_owned())
        .await
        .unwrap();

    let token_before_talk: StreamToken = db
        .service::<RoomStream>()
        .current_stream_token()
        .await
        .unwrap();

    for _i in 0..100 {
        db.talk(room.id(), &joiner, "Hi!").await.unwrap();
    }

    let cmd = db.command::<SyncCommand>();

    let sync_token = SyncToken::new_event(token_before_talk);
    let input = sync_events::v3::Request {
        filter: None,
        full_state: false,
        set_presence: PresenceState::Unavailable,
        timeout: Some(Duration::from_secs(1)),
        since: Some(sync_token.to_string()),
    };

    let result = cmd.execute(input, creator.to_owned()).await.unwrap();
    let joined_room: &JoinedRoom = result.rooms.join.get(room.id()).unwrap();
    let prev_batch = joined_room
        .timeline
        .prev_batch
        .as_ref()
        .map(|prev| TopologicalTokenWithLazyLoading::from_str(prev.as_str()));

    assert_eq!(15, joined_room.timeline.events.len());
    assert!(matches!(prev_batch, Some(Ok(_))));
}

/// Updates to the state, between the time indicated by the since parameter, and the start of
/// the timeline (or all state up to the start of the timeline, if since is not given, or
/// full_state is true).
#[tokio::test]
async fn sync_returns_full_state_if_no_token_is_provided() {
    let db = TestingApp::default().await;
    let (_, room1) = db.public_room().await;
    let (_, room2) = db.public_room().await;

    let joiner = db.register().await;
    let join1 = db
        .join_room(room1.id().to_owned(), joiner.to_owned())
        .await
        .unwrap()
        .room_id;
    let join2 = db
        .join_room(room2.id().to_owned(), joiner.to_owned())
        .await
        .unwrap()
        .room_id;

    for _ in 0..50 {
        db.talk(room1.id(), &joiner, "Hi!").await.unwrap();
        db.talk(room2.id(), &joiner, "Hi on room 2").await.unwrap();
    }

    let cmd = db.command::<SyncCommand>();
    let input = sync_events::v3::Request {
        filter: None,
        full_state: false,
        set_presence: PresenceState::Unavailable,
        timeout: Some(Duration::from_secs(1)),
        since: None,
    };

    let result = cmd.execute(input, joiner.to_owned()).await.unwrap();

    let rooms: Rooms = result.rooms;
    let room1 = rooms.join.get(&join1).unwrap();
    let room2 = rooms.join.get(&join2).unwrap();

    assert!(!room1.state.events.is_empty());
    assert!(room1.state.events.iter().any(|e| {
        let event: AnySyncStateEvent = e.deserialize().unwrap();

        event.event_type() == StateEventType::RoomMember
            && event.state_key() == joiner.user().as_str()
    }));
    assert!(!room2.state.events.is_empty());
    assert!(room2.state.events.iter().any(|e| {
        let event: AnySyncStateEvent = e.deserialize().unwrap();

        event.event_type() == StateEventType::RoomMember
            && event.state_key() == joiner.user().as_str()
    }));
}

#[tokio::test]
async fn initial_sync_with_lazy_loading_includes_only_members_who_talked() {
    let db = TestingApp::default().await;
    let (_, room1) = db.public_room().await;
    let joiner = db.register().await;
    let joiner2 = db.register().await;
    let joiner3 = db.register().await;
    let joiner4 = db.register().await;

    let room_id = db
        .join_room(room1.id().to_owned(), joiner.to_owned())
        .await
        .unwrap()
        .room_id;
    db.join_room(room_id.clone(), joiner2.clone())
        .await
        .unwrap();
    db.join_room(room_id.clone(), joiner3.clone())
        .await
        .unwrap();
    db.join_room(room_id.clone(), joiner4.clone())
        .await
        .unwrap();

    //  `joiner` talks in this room exceeding the timeline limit
    for _ in 0..50 {
        db.talk(room1.id(), &joiner, "Hi!").await.unwrap();
    }
    // Joiner2 will belong to the timeline and we will need to send it to the state as well
    db.talk(room1.id(), &joiner2, "Bon dia").await.unwrap();

    let mut lazy_load_filter = FilterDefinition::default();
    lazy_load_filter.room.state.lazy_load_options = LazyLoadOptions::Enabled {
        include_redundant_members: false,
    };

    let cmd = db.command::<SyncCommand>();
    let input = sync_events::v3::Request {
        filter: Some(sync_events::v3::Filter::FilterDefinition(
            lazy_load_filter.clone(),
        )),
        full_state: false,
        set_presence: PresenceState::Unavailable,
        timeout: Some(Duration::from_secs(1)),
        since: None,
    };

    let result = cmd.execute(input, joiner.to_owned()).await.unwrap();

    let rooms: Rooms = result.rooms;
    let room1 = rooms.join.get(&room_id).unwrap();
    let next_batch = result.next_batch;

    assert!(!room1.state.events.is_empty());
    let member1 = find_room_member(room1.state.events.iter(), joiner.user());
    assert!(member1.is_some());
    let member2 = find_room_member(room1.state.events.iter(), joiner2.user());
    assert!(member2.is_some());
    let member3 = find_room_member(room1.state.events.iter(), joiner3.user());
    assert!(member3.is_none());

    // Talk again member1 and member4. It should send member4 room member
    db.talk(&room_id, &joiner, "joiner1").await.unwrap();
    db.talk(&room_id, &joiner4, "joiner4").await.unwrap();

    let input = sync_events::v3::Request {
        filter: Some(sync_events::v3::Filter::FilterDefinition(lazy_load_filter)),
        full_state: false,
        set_presence: PresenceState::Unavailable,
        timeout: Some(Duration::from_secs(1)),
        since: Some(next_batch),
    };
    let result = cmd.execute(input, joiner.to_owned()).await.unwrap();
    let rooms: Rooms = result.rooms;
    let room1 = rooms.join.get(&room_id).unwrap();

    assert!(!room1.state.events.is_empty());
    let member1 = find_room_member(room1.state.events.iter(), joiner.user());
    assert!(member1.is_none());
    let member4 = find_room_member(room1.state.events.iter(), joiner4.user());
    assert!(member4.is_some());
}

#[tokio::test]
async fn initial_sync_with_lazy_loading_with_redundant_members() {
    let db = TestingApp::default().await;
    let (_, room1) = db.public_room().await;
    let joiner = db.register().await;
    let joiner2 = db.register().await;
    let joiner3 = db.register().await;
    let joiner4 = db.register().await;

    let room_id = db
        .join_room(room1.id().to_owned(), joiner.to_owned())
        .await
        .unwrap()
        .room_id;
    db.join_room(room_id.clone(), joiner2.clone())
        .await
        .unwrap();
    db.join_room(room_id.clone(), joiner3.clone())
        .await
        .unwrap();
    db.join_room(room_id.clone(), joiner4.clone())
        .await
        .unwrap();

    //  `joiner` talks in this room exceeding the timeline limit
    for _ in 0..50 {
        db.talk(room1.id(), &joiner, "Hi!").await.unwrap();
    }
    // Joiner2 will belong to the timeline and we will need to send it to the state as well
    db.talk(room1.id(), &joiner2, "Bon dia").await.unwrap();

    let mut lazy_load_filter = FilterDefinition::default();
    lazy_load_filter.room.state.lazy_load_options = LazyLoadOptions::Enabled {
        include_redundant_members: true,
    };

    let cmd = db.command::<SyncCommand>();
    let input = sync_events::v3::Request {
        filter: Some(sync_events::v3::Filter::FilterDefinition(
            lazy_load_filter.clone(),
        )),
        full_state: false,
        set_presence: PresenceState::Unavailable,
        timeout: Some(Duration::from_secs(1)),
        since: None,
    };

    let result = cmd.execute(input, joiner.to_owned()).await.unwrap();

    let rooms: Rooms = result.rooms;
    let room1 = rooms.join.get(&room_id).unwrap();
    let next_batch = result.next_batch;

    assert!(!room1.state.events.is_empty());
    let member1 = find_room_member(room1.state.events.iter(), joiner.user());
    assert!(member1.is_some());
    let member2 = find_room_member(room1.state.events.iter(), joiner2.user());
    assert!(member2.is_some());
    let member3 = find_room_member(room1.state.events.iter(), joiner3.user());
    assert!(member3.is_none());

    // Talk again member1 and member4. It should send member4 room member
    db.talk(&room_id, &joiner, "joiner1").await.unwrap();
    db.talk(&room_id, &joiner4, "joiner4").await.unwrap();

    let input = sync_events::v3::Request {
        filter: Some(sync_events::v3::Filter::FilterDefinition(lazy_load_filter)),
        full_state: false,
        set_presence: PresenceState::Unavailable,
        timeout: Some(Duration::from_secs(1)),
        since: Some(next_batch),
    };
    let result = cmd.execute(input, joiner.to_owned()).await.unwrap();
    let rooms: Rooms = result.rooms;
    let room1 = rooms.join.get(&room_id).unwrap();

    assert!(!room1.state.events.is_empty());
    // Joiner (member1) talked since last sync and lazy loading is configured with redundant memberships
    let member1 = find_room_member(room1.state.events.iter(), joiner.user());
    assert!(member1.is_some());
    let member4 = find_room_member(room1.state.events.iter(), joiner4.user());
    assert!(member4.is_some());
}

#[tokio::test]
async fn sync_returns_room_members_events_even_if_it_uses_lazy_loading() {
    let db = TestingApp::default().await;
    let (_, room1) = db.public_room().await;
    let joiner = db.register().await;
    let joiner2 = db.register().await;
    let joiner3 = db.register().await;
    let joiner4 = db.register().await;

    let room_id = db
        .join_room(room1.id().to_owned(), joiner.to_owned())
        .await
        .unwrap()
        .room_id;
    db.join_room(room_id.clone(), joiner2.clone())
        .await
        .unwrap();
    db.join_room(room_id.clone(), joiner3.clone())
        .await
        .unwrap();
    db.join_room(room_id.clone(), joiner4.clone())
        .await
        .unwrap();

    let mut lazy_load_filter = FilterDefinition::default();
    lazy_load_filter.room.state.lazy_load_options = LazyLoadOptions::Enabled {
        include_redundant_members: false,
    };

    let cmd = db.command::<SyncCommand>();
    let input = sync_events::v3::Request {
        filter: Some(sync_events::v3::Filter::FilterDefinition(
            lazy_load_filter.clone(),
        )),
        full_state: false,
        set_presence: PresenceState::Unavailable,
        timeout: Some(Duration::from_secs(1)),
        since: None,
    };

    let result = cmd.execute(input, joiner.to_owned()).await.unwrap();

    let rooms: Rooms = result.rooms;
    let room1 = rooms.join.get(&room_id).unwrap();
    let next_batch = result.next_batch;
    assert!(!room1.state.events.is_empty());

    db.change_display_name(&joiner2, "joiner2").await.unwrap();

    let input = sync_events::v3::Request {
        filter: Some(sync_events::v3::Filter::FilterDefinition(lazy_load_filter)),
        full_state: false,
        set_presence: PresenceState::Unavailable,
        timeout: Some(Duration::from_secs(1)),
        since: Some(next_batch),
    };
    let result = cmd.execute(input, joiner.to_owned()).await.unwrap();
    let rooms: Rooms = result.rooms;
    let room1 = rooms.join.get(&room_id).unwrap();

    assert!(room1.state.events.is_empty());
    assert_eq!(1, room1.timeline.events.len());
}

#[tokio::test]
async fn sync_with_lazy_loading_does_not_interfe_between_users() {
    let db = TestingApp::default().await;
    let (_, room1) = db.public_room().await;
    let joiner = db.register().await;
    let joiner2 = db.register().await;
    let joiner3 = db.register().await;
    let joiner4 = db.register().await;

    let room_id = db
        .join_room(room1.id().to_owned(), joiner.to_owned())
        .await
        .unwrap()
        .room_id;
    db.join_room(room_id.clone(), joiner2.clone())
        .await
        .unwrap();
    db.join_room(room_id.clone(), joiner3.clone())
        .await
        .unwrap();
    db.join_room(room_id.clone(), joiner4.clone())
        .await
        .unwrap();

    let mut lazy_load_filter = FilterDefinition::default();
    lazy_load_filter.room.state.lazy_load_options = LazyLoadOptions::Enabled {
        include_redundant_members: false,
    };

    let cmd = db.command::<SyncCommand>();
    let input = sync_events::v3::Request {
        filter: Some(sync_events::v3::Filter::FilterDefinition(
            lazy_load_filter.clone(),
        )),
        full_state: false,
        set_presence: PresenceState::Unavailable,
        timeout: Some(Duration::from_secs(1)),
        since: None,
    };

    let result = cmd.execute(input, joiner.to_owned()).await.unwrap();

    let rooms: Rooms = result.rooms;
    let room1 = rooms.join.get(&room_id).unwrap();
    assert_eq!(5, room1.state.events.len());

    // Sync with joiner2 should return the same amount of state events (from lazy loading)
    let cmd = db.command::<SyncCommand>();
    let input = sync_events::v3::Request {
        filter: Some(sync_events::v3::Filter::FilterDefinition(
            lazy_load_filter.clone(),
        )),
        full_state: false,
        set_presence: PresenceState::Unavailable,
        timeout: Some(Duration::from_secs(1)),
        since: None,
    };

    let result = cmd.execute(input, joiner2.to_owned()).await.unwrap();

    let rooms: Rooms = result.rooms;
    let room1 = rooms.join.get(&room_id).unwrap();
    assert_eq!(5, room1.state.events.len());
}

/*
// #[tokio::test]
#[allow(dead_code)]
async fn notification_counts_after_read_receipt() {
    let db = TestingApp::default().await;
    let bg = graceful::Background::default();

    let notifications_worker = PushRuleWorker::new(&db.services, bg.acquire().unwrap()).await;
    tokio::spawn(notifications_worker.run());
    let (creator, room) = db.public_room().await;
    let token_after_create = db
        .services
        .room_stream
        .current_stream_token()
        .await
        .unwrap();
    let new_joiner = db.register_with_cmd().await;
    let _ = db
        .join_room(room.id().to_owned(), new_joiner.to_owned())
        .await;
    let current_stream_token = db
        .services
        .room_stream
        .current_stream_token()
        .await
        .unwrap();
    let expected_token = StreamToken::from(current_stream_token.saturating_add(3));

    let _eid = db
        .talk(room.id(), &new_joiner, &format!("Hi {}", creator.user()))
        .await
        .unwrap();
    let event_id_middle = db.talk(room.id(), &new_joiner, "Msg 1").await.unwrap();
    db.talk(room.id(), &new_joiner, "Msg 2").await.unwrap();
    let last_event_id = db.talk(room.id(), &new_joiner, "Msg 3").await.unwrap();
    let _current_stream_token = db
        .services
        .room_stream
        .current_stream_token()
        .await
        .unwrap();

    wait_for_stream_token(&db.services, expected_token).await;

    let _current_stream_token = db
        .services
        .room_stream
        .current_stream_token()
        .await
        .unwrap();

    let (not, highlight) =
        execute_sync_with_marker_at(&db, &room, &creator, token_after_create, None).await;
    assert_eq!(4, not);
    assert_eq!(1, highlight);

    // Set read marker to the middle of the stream and request from the same sync token
    let (not, highlight) = execute_sync_with_marker_at(
        &db,
        &room,
        &creator,
        token_after_create,
        Some(&event_id_middle),
    )
    .await;
    assert_eq!(2, not);
    assert_eq!(0, highlight);

    // Set read marker to the end of the stream and request from the same sync token
    let (not, highlight) = execute_sync_with_marker_at(
        &db,
        &room,
        &creator,
        token_after_create,
        Some(&last_event_id),
    )
    .await;
    assert_eq!(0, not);
    assert_eq!(0, highlight);
}
*/

#[tokio::test]
async fn it_does_not_receive_m_room_messages_on_sync_with_a_filter_disabling_them() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let joiner = db.register().await;
    let _ = db
        .join_room(room.id().to_owned(), joiner.to_owned())
        .await
        .unwrap();

    let token_before_talk: StreamToken = db
        .service::<RoomStream>()
        .current_stream_token()
        .await
        .unwrap();

    db.talk(room.id(), &joiner, "Hi!").await.unwrap();
    db.talk(room.id(), &creator, "How are you?").await.unwrap();
    db.talk(room.id(), &joiner, "Good!").await.unwrap();

    let content = TestingApp::dummy_content().into_content().unwrap();
    let builder = EventBuilder::builder(creator.user(), content, room.id());

    let _ = room
        .add_event(builder, EventOptions::default())
        .await
        .expect("event was added to room");

    let cmd = db.command::<SyncCommand>();

    let sync_token = SyncToken::new_event(token_before_talk);
    let filter_definition = FilterDefinition {
        room: RoomFilter {
            timeline: RoomEventFilter {
                not_types: vec!["m.room.message".to_string()],
                ..Default::default()
            },
            ..Default::default()
        },
        ..Default::default()
    };

    let input = sync_events::v3::Request {
        filter: Some(
            safareig_core::ruma::api::client::sync::sync_events::v3::Filter::FilterDefinition(
                filter_definition,
            ),
        ),
        full_state: false,
        set_presence: PresenceState::Unavailable,
        timeout: Some(Duration::from_secs(1)),
        since: Some(sync_token.to_string()),
    };

    let result = cmd.execute(input, creator.to_owned()).await.unwrap();
    let joined_room = result.rooms.join.get(room.id()).unwrap();

    assert_eq!(1, joined_room.timeline.events.len());
    let event_type = joined_room.timeline.events[0]
        .get_field::<String>("type")
        .unwrap();
    assert_eq!(event_type.as_deref(), Some("safareig.dummy"));
}

#[tokio::test]
async fn it_returns_state_events_not_in_timeline() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let joiner = db.register().await;
    let additional_user = db.register().await;
    let additional_user2 = db.register().await;
    let _ = db
        .join_room(room.id().to_owned(), joiner.to_owned())
        .await
        .unwrap();

    let token_before_talk: StreamToken = db
        .service::<RoomStream>()
        .current_stream_token()
        .await
        .unwrap();

    let _ = db
        .join_room(room.id().to_owned(), additional_user.clone())
        .await;
    let _ = db
        .join_room(room.id().to_owned(), additional_user2.clone())
        .await;
    db.talk(room.id(), &joiner, "Hi!").await.unwrap();
    db.talk(room.id(), &creator, "How are you?").await.unwrap();
    db.talk(room.id(), &joiner, "Good!").await.unwrap();

    let cmd = db.command::<SyncCommand>();

    let sync_token = SyncToken::new_event(token_before_talk);
    let mut filter_definition = FilterDefinition::default();
    filter_definition.room.timeline.limit = Some(UInt::new_wrapping(1));

    let input = sync_events::v3::Request {
        filter: Some(
            safareig_core::ruma::api::client::sync::sync_events::v3::Filter::FilterDefinition(
                filter_definition,
            ),
        ),
        full_state: false,
        set_presence: PresenceState::Unavailable,
        timeout: Some(Duration::from_secs(1)),
        since: Some(sync_token.to_string()),
    };

    let result = cmd.execute(input, creator.to_owned()).await.unwrap();
    let joined_room = result.rooms.join.get(room.id()).unwrap();

    assert_eq!(1, joined_room.timeline.events.len());
    assert!(joined_room.timeline.limited);
    assert_eq!(2, joined_room.state.events.len());
    let token = joined_room.timeline.prev_batch.as_ref().unwrap();
    let parsed_token = TopologicalTokenWithLazyLoading::from_str(token.as_str()).unwrap();
    assert!(parsed_token.1.is_some());
}

#[tokio::test]
async fn it_returns_limited_timelines_on_recently_joined_rooms() {
    let db = TestingApp::default().await;
    let (_creator, room) = db.public_room().await;
    let joiner = db.register().await;
    let token_before_join: StreamToken = db
        .service::<RoomStream>()
        .current_stream_token()
        .await
        .unwrap();

    let _ = db
        .join_room(room.id().to_owned(), joiner.to_owned())
        .await
        .unwrap();

    let cmd = db.command::<SyncCommand>();

    let sync_token = SyncToken::new_event(token_before_join);

    let input = sync_events::v3::Request {
        filter: None,
        full_state: false,
        set_presence: PresenceState::Unavailable,
        timeout: Some(Duration::from_secs(1)),
        since: Some(sync_token.to_string()),
    };

    let result = cmd.execute(input, joiner.to_owned()).await.unwrap();
    let joined_room = result.rooms.join.get(room.id()).unwrap();

    assert_eq!(1, joined_room.timeline.events.len());
    assert_eq!(10, joined_room.state.events.len());
}

#[tokio::test]
async fn it_does_not_return_events_after_a_user_left_the_room() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let joiner = db.register().await;
    let _ = db
        .join_room(room.id().to_owned(), joiner.to_owned())
        .await
        .unwrap();

    let token_after_join: StreamToken = db
        .service::<RoomStream>()
        .current_stream_token()
        .await
        .unwrap();

    db.talk(room.id(), &joiner, "hi!").await.unwrap();
    db.talk(room.id(), &creator, "Welcome! Please, leave the room")
        .await
        .unwrap();
    db.talk(room.id(), &joiner, ":(").await.unwrap();
    let profile_loader = db.service::<ProfileLoader>();
    room.leave_room(joiner.user(), &profile_loader)
        .await
        .unwrap();
    db.talk(room.id(), &creator, "not visible 1").await.unwrap();
    db.talk(room.id(), &creator, "not visible 2").await.unwrap();
    db.talk(room.id(), &creator, "not visible 3").await.unwrap();

    let cmd = db.command::<SyncCommand>();

    let sync_token = SyncToken::new_event(token_after_join);

    let input = sync_events::v3::Request {
        filter: None,
        full_state: false,
        set_presence: PresenceState::Unavailable,
        timeout: Some(Duration::from_secs(1)),
        since: Some(sync_token.to_string()),
    };

    let result = cmd.execute(input, joiner.to_owned()).await.unwrap();
    let left_room = result.rooms.leave.get(room.id()).unwrap();

    assert!(!left_room.timeline.limited);
    assert_eq!(4, left_room.timeline.events.len());
    assert_eq!(0, left_room.state.events.len());
}

#[tokio::test]
async fn it_returns_to_device_events_if_there_are_missing_events_since_last_sync() {
    let db = TestingApp::with_modules(vec![Box::new(ToDeviceModule)]).await;
    let user = db.register().await;
    let sender = TestingApp::new_identity();
    let cmd = db.command::<SyncCommand>();

    let sync_token = SyncToken::default();
    let input = sync_events::v3::Request {
        filter: None,
        full_state: false,
        set_presence: PresenceState::Unavailable,
        timeout: Some(Duration::from_secs(1)),
        since: Some(sync_token.to_string()),
    };

    add_to_device_event(&db, &user, &sender).await;

    let result = cmd.execute(input, user.clone()).await.unwrap();
    let to_device_events = result.to_device.events;

    assert_eq!(1, to_device_events.len());
}

#[tokio::test]
async fn it_only_returns_transaction_id_for_the_device_which_generated_the_transaction() {
    let db = TestingApp::default().await;
    let (sender, room) = db.public_room().await;
    let device2 = db.login(sender.user(), "pwd").await.unwrap().device_id;
    let sender_device2 = Identity::Device(device2, sender.user().to_owned());
    let another_user = db.register().await;
    db.join_room(room.id().to_owned(), another_user.clone())
        .await
        .unwrap();
    db.talk(room.id(), &sender, "Hi!").await.unwrap();

    // Sync with sender and verify that transaction_id is present
    let cmd = db.command::<SyncCommand>();
    let sync_token = SyncToken::default();
    let input = sync_events::v3::Request {
        filter: None,
        full_state: false,
        set_presence: PresenceState::Unavailable,
        timeout: Some(Duration::from_secs(1)),
        since: Some(sync_token.to_string()),
    };
    let result = cmd.execute(input, sender.clone()).await.unwrap();
    let latest_event = result
        .rooms
        .join
        .get(room.id())
        .unwrap()
        .timeline
        .events
        .last()
        .unwrap()
        .deserialize()
        .unwrap();
    assert!(latest_event.transaction_id().is_some());

    // Sync with another sender's device should not return a transaction id
    let sync_token = SyncToken::default();
    let input = sync_events::v3::Request {
        filter: None,
        full_state: false,
        set_presence: PresenceState::Unavailable,
        timeout: Some(Duration::from_secs(1)),
        since: Some(sync_token.to_string()),
    };
    let result = cmd.execute(input, sender_device2).await.unwrap();
    let latest_event = result
        .rooms
        .join
        .get(room.id())
        .unwrap()
        .timeline
        .events
        .last()
        .unwrap()
        .deserialize()
        .unwrap();
    assert!(latest_event.transaction_id().is_none());

    // Sync with another user should not return a transaction id
    let sync_token = SyncToken::default();
    let input = sync_events::v3::Request {
        filter: None,
        full_state: false,
        set_presence: PresenceState::Unavailable,
        timeout: Some(Duration::from_secs(1)),
        since: Some(sync_token.to_string()),
    };
    let result = cmd.execute(input, another_user).await.unwrap();
    let latest_event = result
        .rooms
        .join
        .get(room.id())
        .unwrap()
        .timeline
        .events
        .last()
        .unwrap()
        .deserialize()
        .unwrap();
    assert!(latest_event.transaction_id().is_none());
}

/*
#[allow(dead_code)]
async fn execute_sync_with_marker_at(
    db: &TestingDB,
    room: &Room,
    syncer: &Identity,
    start_stream_token: StreamToken,
    marker: Option<&EventId>,
) -> (u32, u32) {
    use safareig_core::ruma::api::client::read_marker::set_read_marker;

    let cmd = db.command::<SyncCommand>();

    if let Some(event) = marker {
        let cmd = SetReadMarkerCommand::from(&db.services);
        let input = set_read_marker::v3::Request {
            fully_read: event.to_owned(),
            read_receipt: None,
            room_id: room.id().to_owned(),
        };

        cmd.execute(input, syncer.clone())
            .await
            .expect("set marker worked");
    }

    let sync_token = SyncToken {
        event: start_stream_token,
        ephemeral: db.services.ephemeral.next(),
        ..Default::default()
    };

    let input = sync_events::v3::Request {
        filter: None,
        full_state: false,
        set_presence: PresenceState::Unavailable,
        timeout: Some(Duration::from_secs(5)),
        since: Some(sync_token.to_string()),
    };

    let result = cmd.execute(input, syncer.clone()).await.unwrap();
    let join_room: &JoinedRoom = result.rooms.join.get(room.id()).unwrap();

    (
        u32::try_from(join_room.unread_notifications.notification_count.unwrap()).unwrap(),
        u32::try_from(join_room.unread_notifications.highlight_count.unwrap()).unwrap(),
    )
}

#[allow(dead_code)]
async fn wait_for_stream_token(services: &ClientComponent, token: StreamToken) {
    for _i in 0..25 {
        // TODO: This is an internal logic from push rules worker. We should find a better way to handle this.
        let checkpoint = services
            .checkpoints
            .get_checkpoint("push_rules")
            .await
            .unwrap()
            .map(|c| c.latest)
            .unwrap_or_else(StreamToken::horizon);

        // Check if we are already on the current sync token. Otherwise, wait until we get the token.
        if checkpoint == token {
            return;
        }

        tokio::time::sleep(Duration::from_millis(50)).await;
    }

    panic!("did not reach the expected token: {}", token);
}
*/

fn find_room_member<'a>(
    mut events: impl Iterator<Item = &'a Raw<AnySyncStateEvent>>,
    user: &UserId,
) -> Option<AnySyncStateEvent> {
    events
        .find(|e| {
            let event = e.deserialize().unwrap();

            event.event_type() == StateEventType::RoomMember && event.state_key() == user.as_str()
        })
        .map(|e| e.deserialize().unwrap())
}

async fn add_to_device_event(db: &TestingApp, target_user: &Identity, sender_user: &Identity) {
    let content = AnyToDeviceEventContent::Dummy(ToDeviceDummyEventContent);
    let raw_json = Raw::new(&content).unwrap();

    let mut target_messages = BTreeMap::new();
    target_messages.insert(
        DeviceIdOrAllDevices::DeviceId(target_user.device().unwrap().to_owned()),
        raw_json,
    );
    let mut messages = Messages::new();
    messages.insert(target_user.user().to_owned(), target_messages);

    let cmd = db.command::<SendToDeviceCommand>();
    let input = send_event_to_device::v3::Request {
        event_type: ToDeviceEventType::Dummy,
        txn_id: TransactionId::new(),
        messages,
    };

    cmd.execute(input, sender_user.to_owned()).await.unwrap();
}
