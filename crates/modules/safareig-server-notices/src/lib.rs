use std::sync::Arc;

use ruma_common::{OwnedEventId, OwnedUserId, RoomId};
use safareig::{
    client::room::{loader::RoomLoader, RoomError},
    core::{
        events::EventBuilder,
        room::{CreateRoomParameters, RoomCreator, RoomVersionRegistry},
    },
    storage::EventOptions,
};
use safareig_client_config::{
    command::AccountDataError, storage::AccountDataEvent, AccountDataManager,
};
use safareig_core::{
    config::HomeserverConfig,
    error::RumaExt,
    events::{Content, EventError},
    ruma::{
        api::client::room::{create_room::v3::RoomPreset, Visibility},
        events::room::power_levels::RoomPowerLevelsEventContent,
        exports::ruma_macros::EventContent,
        Int, OwnedRoomId, UserId,
    },
    Inject, ServerInfo,
};
use serde::{Deserialize, Serialize};

pub mod command;
mod module;
pub use module::ServerNoticesModule;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum ServerNoticeError {
    #[error("Room error")]
    RoomError(#[from] RoomError),
    #[error("Account data error")]
    AccountDataError(#[from] AccountDataError),
    #[error("Event error")]
    EventError(#[from] EventError),
}

impl From<ServerNoticeError> for safareig_core::ruma::api::client::Error {
    fn from(room_error: ServerNoticeError) -> Self {
        match room_error {
            ServerNoticeError::AccountDataError(e) => e.into(),
            ServerNoticeError::RoomError(e) => e.into(),
            ServerNoticeError::EventError(e) => e.into(),
        }
    }
}

impl From<ServerNoticeError> for safareig_core::ruma::api::error::MatrixError {
    fn from(error: ServerNoticeError) -> Self {
        let client = safareig_core::ruma::api::client::Error::from(error);
        client.to_generic()
    }
}

#[derive(Inject)]
pub struct ServerNotices {
    account_data: AccountDataManager,
    room_loader: RoomLoader,
    server_info: ServerInfo,
    room_creator: RoomCreator,
    config: Arc<HomeserverConfig>,
    room_versions: Arc<RoomVersionRegistry>,
}

impl ServerNotices {
    pub async fn send_notice(
        &self,
        user_id: &UserId,
        content: Content,
    ) -> Result<Option<OwnedEventId>, ServerNoticeError> {
        let room_id = self.find_server_notices_room(user_id).await?;
        if let Some(room) = self.room_loader.get(&room_id).await? {
            let options = EventOptions::default();
            let sender = self.server_user();
            let event_builder = EventBuilder::builder(&sender, content, room.id());

            let (event_id, _) = room.add_event(event_builder, options).await?;

            return Ok(Some(event_id));
        }

        Ok(None)
    }

    async fn find_server_notices_room(
        &self,
        user_id: &UserId,
    ) -> Result<OwnedRoomId, ServerNoticeError> {
        let room_id = self
            .account_data
            .get_account_event_content::<ServerNoticeRoomEventContent>(user_id)
            .await
            .ok()
            .flatten()
            .map(|content| content.room_id);

        match room_id {
            Some(room_id) => Ok(room_id),
            None => {
                // Create room
                let server_user = self.server_user();
                let room_id = RoomId::new(self.server_info.server_name());
                let room_version = self
                    .room_versions
                    .get(&self.config.default_room_version())
                    .ok_or_else(|| {
                        ServerNoticeError::RoomError(RoomError::UnsupportedRoomVersion(
                            self.config.default_room_version(),
                        ))
                    })?;

                let params = CreateRoomParameters::new(
                    server_user,
                    room_version.clone(),
                    room_id.to_owned(),
                )
                .add_join_user(user_id.to_owned())
                .with_is_direct(true)
                .with_visibility(Visibility::Private)
                .with_preset(RoomPreset::PrivateChat)
                .with_room_name("Server notices".to_string())
                .with_power_levels(self.server_notices_power_levels());
                self.room_creator.create_room(params).await?;

                // Store it to user global data
                let server_notice_event = ServerNoticeRoomEventContent {
                    room_id: room_id.to_owned(),
                };

                self.account_data
                    .update_account_data(
                        user_id,
                        AccountDataEvent::Global(Content::from_event_content(
                            &server_notice_event,
                        )?),
                    )
                    .await?;

                Ok(room_id)
            }
        }
    }

    fn server_user(&self) -> OwnedUserId {
        UserId::parse_with_server_name("server", self.server_info.server_name()).unwrap()
    }

    fn server_notices_power_levels(&self) -> RoomPowerLevelsEventContent {
        let mut power_levels = RoomPowerLevelsEventContent::default();
        let server = self.server_user();
        power_levels.users.insert(server, Int::new_saturating(100));
        power_levels.users_default = Int::new_saturating(0);
        power_levels.events_default = Int::new_saturating(100);
        power_levels.state_default = Int::new_saturating(100);

        power_levels
    }
}

#[derive(Clone, Debug, Deserialize, Serialize, EventContent)]
#[cfg_attr(not(feature = "unstable-exhaustive-types"), non_exhaustive)]
#[ruma_event(type = "safareig.server_notice_room", kind = GlobalAccountData)]
pub struct ServerNoticeRoomEventContent {
    pub room_id: OwnedRoomId,
}
