use std::sync::Arc;

use async_trait::async_trait;
use ruma_common::OwnedUserId;
use safareig_core::{
    auth::Identity, command::Command, events::Content,
    ruma::events::room::message::RoomMessageEventContent, Inject,
};

use crate::ServerNotices;

#[derive(Inject)]
pub struct SendServerNoticeCommand {
    server_notices: Arc<ServerNotices>,
}

#[async_trait]
impl Command for SendServerNoticeCommand {
    type Input = SendNoticeRequest;
    type Output = SendNoticeResponse;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(
        &self,
        input: Self::Input,
        _id: Identity,
    ) -> Result<Self::Output, Self::Error> {
        let notice_event_content = RoomMessageEventContent::text_plain(input.content);
        let content = Content::from_event_content(&notice_event_content).unwrap();
        self.server_notices
            .send_notice(&input.user_id, content)
            .await?;

        Ok(SendNoticeResponse {})
    }
}

pub struct SendNoticeRequest {
    pub user_id: OwnedUserId,
    pub content: String,
}

pub struct SendNoticeResponse {}
