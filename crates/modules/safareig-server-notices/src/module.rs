use std::sync::Arc;

use safareig_core::{
    command::{Container, InjectServices, Module, ServiceCollectionExtension},
    config::HomeserverConfig,
    ddi::{ServiceCollectionExt, ServiceProvider, ServiceResolverExt},
    ruma::UserId,
    storage::StorageError,
};
use safareig_users::storage::{User, UserStorage};

use crate::{command::send_notice::SendServerNoticeCommand, ServerNotices};

#[derive(Default)]
pub struct ServerNoticesModule;

#[async_trait::async_trait]
impl<R: safareig_core::command::Router> Module<R> for ServerNoticesModule {
    fn register(&self, container: &mut Container, _router: &mut R) {
        container.service_collection.register_arc::<ServerNotices>();
        container
            .service_collection
            .service_factory(|sp: &ServiceProvider| SendServerNoticeCommand::inject(sp));
    }

    async fn run_migrations(&self, service_provider: &ServiceProvider) -> Result<(), StorageError> {
        let config = service_provider.get::<Arc<HomeserverConfig>>()?;
        let user_storage = service_provider.get::<Arc<dyn UserStorage>>()?;

        let user_id = UserId::parse(format!("@server:{}", config.server_name)).unwrap();
        let maybe_server_user = user_storage.user_by_id(&user_id).await.unwrap();

        match maybe_server_user {
            Some(mut user) => {
                user.display = Some("Server notices".to_string());
                user_storage.update_user(&user).await?;
            }
            None => {
                let mut user = User::appservice(&user_id);
                user.display = Some("Sever notices".to_string());
                // TODO: Allow configure a default avatar for server notices
                user_storage.create_user(&user).await?;
            }
        }

        Ok(())
    }
}
