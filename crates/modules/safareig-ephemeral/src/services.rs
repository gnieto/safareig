use std::{
    collections::{BTreeMap, HashMap},
    fmt::Display,
    sync::{Arc, RwLock},
    time::SystemTime,
};

use async_trait::async_trait;
use rusty_ulid::Ulid;
use safareig_core::{
    auth::FederationAuth,
    bus::{Bus, BusMessage},
    pagination::{Pagination, PaginationResponse, Token},
    ruma::{
        api::{
            client::typing::create_typing_event::v3::Typing,
            federation::transactions::edu::{DirectDeviceContent, Edu},
        },
        events::EphemeralRoomEventType,
        exports::serde_json,
        OwnedDeviceId, OwnedEventId, OwnedRoomId, OwnedServerName, OwnedUserId,
    },
    StreamToken,
};
use serde::{Deserialize, Serialize};

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Debug, Serialize, Deserialize)]
pub struct EphemeralToken(Ulid);

impl EphemeralToken {
    pub fn new() -> Self {
        EphemeralToken(Ulid::generate())
    }

    pub fn next(self) -> Self {
        let next = Ulid::next_strictly_monotonic(self.0).unwrap_or_else(Ulid::generate);

        EphemeralToken(next)
    }
}

impl Default for EphemeralToken {
    fn default() -> Self {
        Self(Ulid::from(0))
    }
}

impl From<Ulid> for EphemeralToken {
    fn from(u: Ulid) -> Self {
        EphemeralToken(u)
    }
}

impl Display for EphemeralToken {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl Token for EphemeralToken {}

impl TryFrom<&str> for EphemeralToken {
    type Error = ();

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        value
            .parse::<Ulid>()
            .map_err(|e| {
                tracing::error!(
                    err = e.to_string().as_str(),
                    token = "ephemeral",
                    "Invalid stream token"
                );
            })
            .map(EphemeralToken)
    }
}

#[derive(Clone)]
pub struct EphemeralStream {
    // TODO: tokio mutex?
    edus: Arc<RwLock<BTreeMap<EphemeralToken, EphemeralEvent>>>,
    bus: Bus,
}

impl EphemeralStream {
    pub fn new(bus: Bus) -> Self {
        EphemeralStream {
            edus: Arc::new(RwLock::new(BTreeMap::default())),
            bus,
        }
    }

    pub fn push_event(&self, event: EphemeralEvent) {
        let mut guard = self.edus.write().unwrap();
        let next = Self::internal_next(&guard);

        guard.insert(next, event.clone());

        self.bus.send_message(event.into());
    }

    pub fn next(&self) -> EphemeralToken {
        let guard = self.edus.read().unwrap();

        Self::internal_next(&guard)
    }

    pub fn events(
        &self,
        query: Pagination<EphemeralToken>,
    ) -> PaginationResponse<EphemeralToken, EphemeralEvent> {
        let guard = self.edus.read().unwrap();
        let range = guard.range(query.range().clone());

        let mut latest = None;
        let mut previous = None;
        let mut events = Vec::new();

        for (index, event) in range {
            let token = *index;
            latest = std::cmp::max(latest, Some(token));
            previous = std::cmp::min(previous, Some(token));
            events.push(event.clone());
        }

        PaginationResponse::new(events, previous, latest)
    }

    fn internal_next(events: &BTreeMap<EphemeralToken, EphemeralEvent>) -> EphemeralToken {
        events
            .iter()
            .next_back()
            .map(|tuple| *tuple.0)
            .map(EphemeralToken::next)
            .unwrap_or_default()
    }
}

#[derive(Clone)]
pub struct EphemeralRegistry {
    edus: HashMap<EphemeralRoomEventType, Arc<dyn EphemeralHandler>>,
    default_handler: Arc<dyn EphemeralHandler>,
}

impl Default for EphemeralRegistry {
    fn default() -> Self {
        Self {
            edus: Default::default(),
            default_handler: Arc::new(DefaultEphemeralHandler {}),
        }
    }
}

impl EphemeralRegistry {
    pub fn new(handlers: HashMap<EphemeralRoomEventType, Arc<dyn EphemeralHandler>>) -> Self {
        Self {
            edus: handlers,
            default_handler: Arc::new(DefaultEphemeralHandler {}),
        }
    }

    pub fn handler(&self, event_type: EphemeralRoomEventType) -> Arc<dyn EphemeralHandler> {
        self.edus
            .get(&event_type)
            .cloned()
            .unwrap_or_else(|| self.default_handler.clone())
    }
}

#[derive(Debug, Clone)]
pub enum EphemeralEvent {
    Typing(OwnedUserId, OwnedRoomId, Typing),
    Presence(OwnedUserId),
    Marker(OwnedRoomId, OwnedUserId, OwnedEventId, SystemTime),
    DeviceUpdate(OwnedUserId, OwnedDeviceId, StreamToken),
    // TODO: DeviceKeyUpdate
    ToDevice(OwnedServerName, DirectDeviceContent),
    CrossSigning(OwnedUserId),
}

impl From<EphemeralEvent> for BusMessage {
    fn from(e: EphemeralEvent) -> Self {
        match e {
            EphemeralEvent::Typing(user, room, _) => BusMessage::Typing(user, room),
            EphemeralEvent::Presence(user) => BusMessage::Presence(user),
            EphemeralEvent::Marker(room, _, _, _) => BusMessage::Marker(room),
            EphemeralEvent::DeviceUpdate(_, device_id, _) => BusMessage::DeviceUpdate(device_id),
            EphemeralEvent::ToDevice(server_name, _device) => {
                BusMessage::ToDeviceFederation(server_name)
            }
            EphemeralEvent::CrossSigning(user_id) => BusMessage::E2eeUpdate(user_id),
        }
    }
}

impl EphemeralEvent {
    pub fn event_type(&self) -> EphemeralRoomEventType {
        match self {
            EphemeralEvent::Typing(..) => EphemeralRoomEventType::from("m.typing"),
            EphemeralEvent::Presence(..) => EphemeralRoomEventType::from("m.presence"),
            EphemeralEvent::Marker(..) => EphemeralRoomEventType::from("m.receipt"),
            EphemeralEvent::DeviceUpdate(..) => {
                EphemeralRoomEventType::from("m.device_list_update")
            }
            EphemeralEvent::ToDevice(..) => EphemeralRoomEventType::from("m.direct_to_device"),
            EphemeralEvent::CrossSigning(..) => {
                EphemeralRoomEventType::from("m.signing_key_update")
            }
        }
    }
}

#[async_trait]
pub trait EphemeralHandler: Send + Sync {
    async fn on_federation(&self, edu: Edu, auth: &FederationAuth);
    fn edu_aggregator(&self) -> Box<dyn EphemeralAggregator>;
}

#[async_trait]
pub trait EphemeralAggregator: Send + Sync {
    async fn aggregate_event(&mut self, event: EphemeralEvent);
    fn edus(self: Box<Self>) -> Vec<Edu>;
}

struct DefaultEphemeralHandler {}

#[async_trait]
impl EphemeralHandler for DefaultEphemeralHandler {
    async fn on_federation(&self, edu: Edu, _: &FederationAuth) {
        let deserialized: Option<serde_json::Value> = serde_json::to_value(edu).ok();
        let kind = deserialized
            .as_ref()
            .and_then(|value| value.as_object())
            .and_then(|object| object.get("edu_type"))
            .and_then(|value| value.as_str())
            .unwrap_or("unknown");

        tracing::info!(edu_type = kind, "ignoring incoming edu type");
    }

    fn edu_aggregator(&self) -> Box<dyn EphemeralAggregator> {
        Box::new(DefaultEphemeralAggregator {})
    }
}

struct DefaultEphemeralAggregator {}

#[async_trait]
impl EphemeralAggregator for DefaultEphemeralAggregator {
    async fn aggregate_event(&mut self, _: EphemeralEvent) {}
    fn edus(self: Box<Self>) -> Vec<Edu> {
        Vec::new()
    }
}
