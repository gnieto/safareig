use std::sync::Arc;

use safareig_core::{
    bus::Bus,
    command::{Container, Module},
    ddi::{ServiceCollectionExt, ServiceProvider, ServiceResolverExt},
    ruma::events::EphemeralRoomEventType,
};

use crate::services::{EphemeralHandler, EphemeralRegistry, EphemeralStream};

#[derive(Default)]
pub struct EphemeralModule {}

impl<R: safareig_core::command::Router> Module<R> for EphemeralModule {
    fn register(&self, container: &mut Container, _router: &mut R) {
        container
            .service_collection
            .service_factory(|sp: &ServiceProvider| {
                let handlers = sp
                    .get_all::<Arc<dyn EphemeralHandler>>()?
                    .into_iter()
                    .map(|(k, v)| (EphemeralRoomEventType::from(k.to_string()), v.clone()))
                    .collect();

                Ok(EphemeralRegistry::new(handlers))
            });

        container
            .service_collection
            .service_factory(|bus: &Bus| Ok(EphemeralStream::new(bus.clone())));
    }
}
