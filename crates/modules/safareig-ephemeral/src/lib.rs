mod module;
pub mod services;

pub use module::EphemeralModule;
use safareig_core::ruma::events::EphemeralRoomEventType;

pub enum EduType {
    Typing,
    Presence,
    Receipt,
    DeviceListUpdate,
    DirectToDevice,
    SigningKeyUpdate,
    Custom(String),
}

impl From<EduType> for EphemeralRoomEventType {
    fn from(value: EduType) -> Self {
        let edu_type = match value {
            EduType::DeviceListUpdate => "m.device_list_update",
            EduType::Custom(ref custom) => custom.as_str(),
            EduType::DirectToDevice => "m.direct_to_device",
            EduType::Presence => "m.presence",
            EduType::Receipt => "m.receipt",
            EduType::SigningKeyUpdate => "m.signing_key_update",
            EduType::Typing => "m.typing",
        };

        EphemeralRoomEventType::from(edu_type)
    }
}
