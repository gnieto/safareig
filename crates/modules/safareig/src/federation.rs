use safareig_core::ruma::{events::pdu::Pdu, serde::Raw};
use serde_json::value::RawValue;

pub mod backfill;
pub mod component;
pub(crate) mod edu;
pub mod room;
mod version;
pub mod worker;

fn raw_json_to_raw_pdu(pdus: Vec<Box<RawValue>>) -> Vec<Raw<Pdu>> {
    pdus.into_iter().map(Raw::from_json).collect()
}
