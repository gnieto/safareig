use std::{collections::HashMap, fmt::Display, num::NonZeroU128, str::FromStr};

use async_trait::async_trait;
use rusty_ulid::Ulid;
use safareig_core::{
    pagination::{self, Pagination, PaginationResponse, Range, Token},
    ruma::{
        api::client::{
            filter::{FilterDefinition, RoomFilter},
            sync::sync_events::v3::InviteState,
        },
        events::{
            room::member::MembershipState, AnyStrippedStateEvent, StateEventType, TimelineEventType,
        },
        EventId, OwnedEventId, OwnedRoomId, OwnedTransactionId, OwnedUserId, RoomId, RoomVersionId,
        TransactionId, UserId,
    },
    storage::{Result, StorageError},
    StreamToken,
};
use safareig_sync_api::context::SyncMembership;
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use thiserror::Error;

use crate::{
    client::{
        room::state::{RoomStateId, StateIndex},
        sync::stream::LazyLoadToken,
    },
    core::{
        events::{Event, EventRef},
        filter::Filter,
        room::RoomDescriptor,
    },
};

#[cfg(feature = "backend-sqlx")]
pub mod sqlx;

#[async_trait]
pub trait FilterStorage: Send + Sync {
    async fn get(&self, id: &str) -> Result<Option<FilterDefinition>>;
    async fn put(&self, id: &str, filter: &FilterDefinition) -> Result<()>;
}

#[async_trait]
pub trait RoomStorage: Send + Sync {
    // TODO: This should be synchronized with add_event
    async fn update_membership(
        &self,
        user_id: &UserId,
        room_id: &RoomId,
        membership: Membership,
    ) -> Result<()>;
    async fn membership(&self, user_id: &UserId) -> Result<HashMap<OwnedRoomId, Membership>>;
    async fn remove_membership(&self, user_id: &UserId, room_id: &RoomId) -> Result<()>;

    /// Returns a set of events in topological order. Range inclusiveness is controlled by the
    /// boundaries values.
    async fn room_events(
        &self,
        room: &RoomId,
        query: Pagination<TopologicalToken>,
    ) -> Result<PaginationResponse<TopologicalToken, Event>>;

    async fn room_descriptor(&self, room_id: &RoomId) -> Result<Option<RoomDescriptor>>;
    async fn update_room_descriptor(&self, descriptor: &RoomDescriptor) -> Result<()>;
    async fn find_all_rooms(&self) -> Result<Vec<RoomDescriptor>>;
}

#[derive(Default, Clone, Debug)]
pub struct EventOptions {
    previous_room_state_id: Option<RoomStateId>,
    room_state_id: Option<RoomStateId>,
    transaction_id: Option<OwnedTransactionId>,
    rejected: bool,
}

impl EventOptions {
    pub fn set_room_state_id(mut self, room_state_id: RoomStateId) -> Self {
        self.room_state_id = Some(room_state_id);
        self
    }

    pub fn set_previous_room_state_id(mut self, room_state_id: RoomStateId) -> Self {
        self.previous_room_state_id = Some(room_state_id);
        self
    }

    pub fn set_transaction_id(mut self, transaction_id: &TransactionId) -> Self {
        self.transaction_id = Some(transaction_id.to_owned());
        self
    }

    pub fn set_rejected(mut self, rejected: bool) -> Self {
        self.rejected = rejected;
        self
    }

    pub fn is_rejected(&self) -> bool {
        self.rejected
    }

    fn previous_room_state_id(&self) -> Option<&RoomStateId> {
        self.previous_room_state_id.as_ref()
    }

    fn transaction_id(&self) -> Option<&TransactionId> {
        self.transaction_id.as_ref().map(|t| t.as_ref())
    }
}

#[async_trait]
pub trait EventStorage: Send + Sync {
    /// Insert an event outside of the room stream. This is used from federation endpoints which
    /// can insert state or auth events into the room's DAG. Those events are usually untrusted and
    /// don't have a state snapshot previous to the insertion of the event.
    async fn put_event(&self, event: &Event, options: EventOptions) -> Result<()>;

    /// Retrieves a single event
    async fn get_event(&self, event_id: &EventId) -> Result<Option<Event>>;

    /// Retrieves the event associated to the transaction id
    async fn get_event_by_transaction_id(
        &self,
        room: &RoomId,
        txn_id: &TransactionId,
    ) -> Result<Option<Event>>;

    /// Retrieves multiple events.
    async fn get_events(&self, events: &[OwnedEventId]) -> Result<Vec<Event>>;
}

pub enum Extremity {
    Forward(Leaf),
    Backward(BackwardExtremity),
}

#[async_trait]
pub trait RoomTopologyStorage: Send + Sync {
    async fn forward_extremities(&self, room: &RoomId) -> Result<Vec<Leaf>>;
    async fn adjust_extremities(
        &self,
        room: &RoomId,
        event: &Event,
        room: RoomStateId,
    ) -> Result<()>;
    async fn add_backward_extremities(
        &self,
        room: &RoomId,
        extremity: &[BackwardExtremity],
    ) -> Result<()>;
    async fn remove_backward_extremity(
        &self,
        room: &RoomId,
        extremity: &BackwardExtremity,
    ) -> Result<()>;
    async fn backward_extremities(
        &self,
        room: &RoomId,
        range: pagination::Range<TopologicalToken>,
    ) -> Result<Vec<BackwardExtremity>>;
}

#[derive(Clone)]
pub struct StreamFilter<'a> {
    pub filter: Option<&'a Filter<'a>>,
    pub room_id: Option<&'a RoomId>,
    pub source: StreamSource,
}

#[derive(Clone)]
pub enum StreamSource {
    All,
    State,
    Timeline,
}

impl<'a> StreamFilter<'a> {
    pub fn definition(&self) -> Option<&'a FilterDefinition> {
        Some(self.filter?.definition())
    }
}

#[async_trait]
pub trait RoomStreamStorage: Send + Sync {
    async fn stream_token_at(
        &self,
        room_id: &RoomId,
        event: &EventId,
    ) -> Result<Option<StreamToken>>;
    async fn current_stream_token(&self) -> Result<StreamToken>;
    async fn insert_event_to_stream(&self, event: &Event) -> Result<StreamToken>;
    async fn stream_events<'a>(
        &'a self,
        query: Pagination<StreamToken, Option<StreamFilter<'a>>>,
    ) -> Result<PaginationResponse<StreamToken, (StreamToken, OwnedEventId)>>;
}

#[derive(Serialize, Deserialize, Debug)]
pub struct RemoteInvite {
    pub state: InviteState,
    pub room: OwnedRoomId,
    pub room_version: RoomVersionId,
}

impl RemoteInvite {
    pub fn get_inviter(&self, invitee: &UserId) -> Result<OwnedUserId> {
        let maybe_inviter = self
            .state
            .events
            .iter()
            .filter_map(|e| {
                let event_type = e.get_field::<StateEventType>("type").ok()?;
                match event_type {
                    Some(StateEventType::RoomMember) => (),
                    _ => return None,
                };

                let member = e.deserialize().ok()?;

                match &member {
                    AnyStrippedStateEvent::RoomMember(room_member) => {
                        match room_member.content.membership {
                            MembershipState::Invite => {
                                if member.state_key() == invitee.as_str() {
                                    return Some(member.sender().to_owned());
                                }

                                None
                            }
                            _ => None,
                        }
                    }
                    _ => None,
                }
            })
            .next();

        maybe_inviter.ok_or_else(|| StorageError::Custom("could not found inviter".to_string()))
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct LocalInvite {
    pub room: OwnedRoomId,
    pub event_id: OwnedEventId,
}

#[derive(Serialize, Deserialize, Debug)]
pub enum Invite {
    Local(LocalInvite),
    Remote(RemoteInvite),
}

impl Invite {
    pub fn room_id(&self) -> &RoomId {
        match self {
            Invite::Local(local) => local.room.as_ref(),
            Invite::Remote(remote) => remote.room.as_ref(),
        }
    }
}

#[async_trait]
pub trait InviteStorage: Send + Sync {
    async fn add_invite(&self, user_id: &UserId, invite: Invite) -> Result<()>;
    async fn query_invites(
        &self,
        user_id: &UserId,
        range: Range<Ulid>,
    ) -> Result<PaginationResponse<Ulid, Invite>>;
    async fn remove_invite(&self, user_id: &UserId, room_id: &RoomId) -> Result<()>;
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct BackwardExtremity {
    event_id: OwnedEventId,
    topological_token: TopologicalToken,
}

impl BackwardExtremity {
    pub fn new(event_id: OwnedEventId, token: TopologicalToken) -> Self {
        BackwardExtremity {
            event_id,
            topological_token: token,
        }
    }

    pub fn event_id(&self) -> &EventId {
        &self.event_id
    }

    pub fn token(&self) -> &TopologicalToken {
        &self.topological_token
    }
}

impl From<&Event> for BackwardExtremity {
    fn from(value: &Event) -> Self {
        Self {
            event_id: value.event_ref.owned_event_id(),
            topological_token: value.topological_token(),
        }
    }
}

pub struct TimelineQuery {
    pub from: Option<StreamToken>,
    pub to: StreamToken,
    pub room_filter: Option<RoomFilter>,
    pub limited: bool,
}

#[derive(Error, Debug)]
pub enum TokenError {
    #[error("Expected an int number")]
    NotANumber(#[from] std::num::ParseIntError),
    #[error("Expected a known marker")]
    UnknownMarker,
}

#[derive(Clone, Debug)]
pub struct TopologicalTokenWithLazyLoading(pub TopologicalToken, pub Option<LazyLoadToken>);

impl Display for TopologicalTokenWithLazyLoading {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match &self.1 {
            Some(lazy_token) => {
                write!(f, "{}#{}", self.0, lazy_token.to_string())
            }
            None => {
                write!(f, "{}", self.0)
            }
        }
    }
}

impl FromStr for TopologicalTokenWithLazyLoading {
    type Err = TokenError;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let mut split = s.split('#');
        let raw_topological = split.next().ok_or(TokenError::UnknownMarker)?;
        let token = TopologicalToken::from_str(raw_topological)?;

        if let Some(raw_lazy) = split.next() {
            let lazy_token = LazyLoadToken::from_str(raw_lazy).unwrap();

            return Ok(Self(token, Some(lazy_token)));
        }

        Ok(Self(token, None))
    }
}

// TODO: Remove duplicated code with StreamToken
#[derive(Ord, PartialOrd, PartialEq, Eq, Clone, Copy, Serialize, Deserialize, Debug)]
pub struct TopologicalToken(u64, u64);

impl Token for TopologicalToken {}

impl TopologicalToken {
    pub fn new(depth: u64) -> TopologicalToken {
        TopologicalToken(depth, 0)
    }

    pub fn new_subsequence(depth: u64, subsequence: u64) -> TopologicalToken {
        TopologicalToken(depth, subsequence)
    }

    pub fn depth(&self) -> u64 {
        self.0
    }

    pub fn subsequence(&self) -> u64 {
        self.1
    }

    pub fn next(self) -> TopologicalToken {
        TopologicalToken::new(self.depth().saturating_add(1))
    }

    pub fn prev(self) -> TopologicalToken {
        TopologicalToken::new(self.depth().saturating_sub(1))
    }

    pub fn with_lazy_load_token(self, token: LazyLoadToken) -> TopologicalTokenWithLazyLoading {
        TopologicalTokenWithLazyLoading(self, Some(token))
    }

    pub fn with_optional_lazy_load_token(
        self,
        token: Option<LazyLoadToken>,
    ) -> TopologicalTokenWithLazyLoading {
        TopologicalTokenWithLazyLoading(self, token)
    }
}

impl Display for TopologicalToken {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.1 == 0 {
            write!(f, "t{}", self.0)
        } else {
            write!(f, "t{}:{}", self.0, self.1)
        }
    }
}

impl FromStr for TopologicalToken {
    type Err = TokenError;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let unprefixed = s.strip_prefix('t').ok_or(TokenError::UnknownMarker)?;
        let mut subsequence_marker = unprefixed.split(':');

        let first = subsequence_marker.next().unwrap();
        let depth = u64::from_str(first)?;

        if let Some(subsequence) = subsequence_marker.next() {
            let subsequence = u64::from_str(subsequence)?;
            return Ok(TopologicalToken::new_subsequence(depth, subsequence));
        }

        Ok(TopologicalToken::new(depth))
    }
}

impl TopologicalToken {
    /// Returns a marker value which designates the lowest depth
    pub fn root() -> Self {
        TopologicalToken(0, u64::MIN)
    }

    /// Returns a marker value which designates the largest depth
    pub fn infinite() -> Self {
        TopologicalToken(u64::MAX, u64::MAX)
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Leaf {
    pub event_id: EventRef,
    pub depth: u64,
    pub state_at: RoomStateId,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct Membership {
    pub state: MembershipState,
    pub stream: StreamToken, // TODO: Evalute using depth (or add depth)
    pub event_id: OwnedEventId,
}

impl From<Membership> for SyncMembership {
    fn from(m: Membership) -> Self {
        Self {
            state: m.state,
            stream: m.stream,
            event_id: m.event_id,
        }
    }
}

#[async_trait]
pub trait StateStorage: Send + Sync {
    async fn event_for_key(
        &self,
        state: &RoomStateId,
        index: &StateIndex,
    ) -> Result<Option<OwnedEventId>>;
    async fn event_for_keys(
        &self,
        state: &RoomStateId,
        indexes: &[StateIndex],
    ) -> Result<Vec<OwnedEventId>>;
    async fn state_keys_for(
        &self,
        state_id: &RoomStateId,
        filter: Option<TimelineEventType>,
    ) -> Result<Vec<StateIndex>>;
    async fn states(
        &self,
        state_id: &RoomStateId,
        excluded_types: Option<Vec<StateEventType>>,
    ) -> Result<Vec<Event>>;
    async fn state_at(&self, room_id: &RoomId, event_id: &EventId) -> Result<Option<RoomStateId>>;
    async fn fork_state(
        &self,
        base: &RoomStateId,
        index: &StateIndex,
        event_id: &EventId,
    ) -> Result<RoomStateId>;
    async fn store_state(&self, state: &HashMap<OwnedEventId, StateIndex>) -> Result<RoomStateId>;
    async fn assign_state(
        &self,
        room_id: &RoomId,
        event_id: &EventId,
        state_id: &RoomStateId,
    ) -> Result<()>;
    async fn internalize(&self, input: &str) -> Result<rusty_ulid::Ulid>;
    async fn revert(&self, input: rusty_ulid::Ulid) -> Result<String>;
}

#[async_trait]
pub trait StreamStorage<K: Send + Sync + Token, I: Send + Sync>: Send + Sync {
    async fn remove_range(&self, stream_name: &str, range: pagination::Range<K>) -> Result<()>;
    async fn remove_by_value(&self, stream_name: &str, value: &I) -> Result<()>;
    async fn stream_head(&self, stream_name: &str) -> Result<K>;
    async fn push_to_stream(&self, stream_name: &str, item: &I) -> Result<K>;
    async fn stream_query(&self, query: &Pagination<K, String>)
        -> Result<PaginationResponse<K, I>>;
    async fn stream_indexed_query(
        &self,
        query: &Pagination<K, String>,
    ) -> Result<PaginationResponse<K, (K, I)>>;
}

#[derive(Deserialize, Serialize, Debug)]
pub struct CompressedStateIndex {
    kind: NonZeroU128,
    state_key: Option<NonZeroU128>,
}

impl CompressedStateIndex {
    pub async fn from_state_index(
        state: &StateIndex,
        internalizer: &dyn StateStorage,
    ) -> CompressedStateIndex {
        let mut state_key = None;

        if let Some(str) = &state.state_key {
            let ulid = internalizer.internalize(str.as_str()).await.unwrap();
            state_key = Some(NonZeroU128::new(u128::from(ulid)).unwrap());
        }

        let kind_id = internalizer
            .internalize(state.kind.to_string().as_str())
            .await
            .unwrap();

        CompressedStateIndex {
            kind: NonZeroU128::new(u128::from(kind_id)).unwrap(),
            state_key,
        }
    }

    #[allow(clippy::wrong_self_convention)]
    pub async fn to_state_index(self, internalizer: &dyn StateStorage) -> StateIndex {
        let mut state_key = None;
        if let Some(str_id) = self.state_key {
            state_key = Some(
                internalizer
                    .revert(rusty_ulid::Ulid::from(str_id.get()))
                    .await
                    .unwrap(),
            );
        }

        let kind_str = internalizer
            .revert(rusty_ulid::Ulid::from(self.kind.get()))
            .await
            .unwrap();

        StateIndex {
            kind: TimelineEventType::from(kind_str),
            state_key,
        }
    }

    pub fn into_byte_slice(&self) -> Vec<u8> {
        let mut bytes = Vec::new();
        bytes.extend_from_slice(&self.kind.get().to_be_bytes());

        if let Some(key) = self.state_key {
            bytes.extend_from_slice(&key.get().to_be_bytes());
        }

        bytes
    }

    pub fn from_byte_slice(bytes: &[u8]) -> Result<Self> {
        if bytes.len() == 32 {
            let sixteen_bytes: [u8; 16] = TryFrom::try_from(&bytes[0..16])
                .map_err(|_| StorageError::Custom("slice has not the expected size".to_string()))?;
            let kind = u128::from_be_bytes(sixteen_bytes);
            let kind = NonZeroU128::try_from(kind).map_err(|_| StorageError::CorruptedData)?;

            let sixteen_bytes: [u8; 16] = TryFrom::try_from(&bytes[16..32])
                .map_err(|_| StorageError::Custom("slice has not the expected size".to_string()))?;
            let state_key = u128::from_be_bytes(sixteen_bytes);
            let state_key =
                NonZeroU128::try_from(state_key).map_err(|_| StorageError::CorruptedData)?;

            Ok(CompressedStateIndex {
                kind,
                state_key: Some(state_key),
            })
        } else if bytes.len() == 16 {
            let sixteen_bytes: [u8; 16] = TryFrom::try_from(&bytes[0..16])
                .map_err(|_| StorageError::Custom("slice has not the expected size".to_string()))?;
            let kind = u128::from_be_bytes(sixteen_bytes);
            let kind = NonZeroU128::try_from(kind).map_err(|_| StorageError::CorruptedData)?;

            Ok(CompressedStateIndex {
                kind,
                state_key: None,
            })
        } else {
            Err(StorageError::CorruptedData)
        }
    }
}

#[async_trait]
pub trait CheckpointStorage<C: Serialize + DeserializeOwned + Clone + Send + Sync>:
    Send + Sync
{
    async fn get_checkpoint(&self, worker_id: &str) -> Result<Option<C>>;
    async fn update_checkpoint(&self, worker_id: &str, checkpoint: &C) -> Result<()>;
}

#[derive(Deserialize, Serialize, Clone)]
pub struct Checkpoint {
    pub latest: StreamToken,
    pub in_flight: Option<(StreamToken, StreamToken)>,
    pub txn_id: Option<OwnedTransactionId>,
}

#[async_trait]
pub trait ServerConfig: Send + Sync {
    async fn get_config(&self, key: &str) -> Result<Option<Vec<u8>>>;
    async fn put_config(&self, key: &str, value: &[u8]) -> Result<()>;
}

#[cfg(test)]
mod tests {
    use std::{collections::HashSet, str::FromStr, sync::Arc};

    use futures::{stream::FuturesUnordered, StreamExt};
    use safareig_core::{ruma::TransactionId, StreamToken};

    use crate::{
        storage::{Checkpoint, CheckpointStorage, StateStorage, TopologicalToken},
        testing::TestingDB,
    };

    #[test]
    fn do_not_parse_incorrect_topological_tokens() {
        let token_result = TopologicalToken::from_str("");
        assert!(token_result.is_err());

        let token_result = TopologicalToken::from_str("r454:a");
        assert!(token_result.is_err());

        let token_result = TopologicalToken::from_str("ta:b");
        assert!(token_result.is_err());

        let token_result = TopologicalToken::from_str("ta:465");
        assert!(token_result.is_err());
    }

    #[test]
    fn parses_token_without_subsequence() {
        let token_result = TopologicalToken::from_str("t123").unwrap();

        assert_eq!(123, token_result.depth());
    }

    #[test]
    fn parses_token_with_subsequence() {
        let token_result = TopologicalToken::from_str("t123:456").unwrap();

        assert_eq!(123, token_result.depth());
        assert_eq!(456, token_result.subsequence());
    }

    #[tokio::test]
    async fn internalize_string_is_idempotent() {
        let db = TestingDB::default();
        let internalizer = db.container.service::<Arc<dyn StateStorage>>();

        let id = internalizer.internalize("string").await.unwrap();
        let new_id = internalizer.internalize("string").await.unwrap();

        assert_eq!(id, new_id);
    }

    #[tokio::test]
    async fn internalizer_concurrency() {
        let db = TestingDB::default();

        let internalizer = db.container.service::<Arc<dyn StateStorage>>();
        let mut futures = FuturesUnordered::new();

        for _ in 0..15 {
            let int = internalizer.clone();
            let handle = tokio::spawn(async move { int.internalize("internalize").await.unwrap() });

            futures.push(handle);
        }

        let mut set = HashSet::new();
        while let Some(task) = futures.next().await {
            set.insert(task.unwrap());
        }

        assert_eq!(1, set.len());
    }

    #[tokio::test]
    async fn checkpoint_storage() {
        let db = TestingDB::default();
        let checkpoints = db
            .container
            .service::<Arc<dyn CheckpointStorage<Checkpoint>>>();
        let current_checkpoint = checkpoints.get_checkpoint("test_worker").await.unwrap();
        assert!(current_checkpoint.is_none());

        let checkpoint = Checkpoint {
            latest: StreamToken::from(6u64),
            in_flight: None,
            txn_id: Some(TransactionId::new()),
        };

        checkpoints
            .update_checkpoint("test_worker", &checkpoint)
            .await
            .unwrap();

        let current_checkpoint = checkpoints
            .get_checkpoint("test_worker")
            .await
            .unwrap()
            .unwrap();

        assert_eq!(6u64, *current_checkpoint.latest);
        assert!(current_checkpoint.txn_id.is_some());
    }
}
