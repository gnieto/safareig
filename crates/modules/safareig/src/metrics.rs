use lazy_static::lazy_static;
use opentelemetry::metrics::Counter;

lazy_static! {
    pub static ref EVENTS_PROCESSED: Counter<u64> = opentelemetry::global::meter("safareig/client")
        .u64_counter("events")
        .with_description("Amount of events received to current homeserver")
        .init();
    pub static ref REJECTED_CALLS: Counter<u64> =
        opentelemetry::global::meter("safareig/federation")
            .u64_counter("rejected_federation_calls")
            .with_description("Amount of rejected requests due to CircuitBreaker is closed")
            .init();
    pub static ref RATE_LIMITED_CALLS: Counter<u64> =
        opentelemetry::global::meter("safareig/federation")
            .u64_counter("rate_limited_federation_calls")
            .with_description("Amount of rejected requests due to rate-limit hit")
            .init();
    pub static ref OUTGOING_FEDERATION_EVENTS: Counter<u64> =
        opentelemetry::global::meter("safareig/federation")
            .u64_counter("outgoing_federation_events")
            .with_description("Amount of outgoing federation events")
            .init();
    pub static ref OUTGOING_FEDERATION_EDUS: Counter<u64> =
        opentelemetry::global::meter("safareig/federation")
            .u64_counter("outgoing_federation_edus")
            .with_description("Amount of outgoing federation EDUs")
            .init();
    pub static ref OUTGOING_PUSH_EVENTS: Counter<u64> =
        opentelemetry::global::meter("safareig/push")
            .u64_counter("outgoing_push_events")
            .with_description("Amount of outgoing push events")
            .init();
    pub static ref NOTIFICATION_PROCESSED_EVENTS: Counter<u64> =
        opentelemetry::global::meter("safareig/notification")
            .u64_counter("notification_processed_events")
            .with_description("Amount of user notification events")
            .init();
}
