use std::collections::{BTreeSet, HashMap, HashSet};

use safareig_core::ruma::{
    events::{pdu::Pdu, room::create::RoomCreateEventContent, TimelineEventType},
    serde::Raw,
    EventId, OwnedEventId, OwnedServerName, RoomVersionId,
};
use serde::{Deserialize, Serialize};
use tracing::instrument;

use crate::{
    client::room::state::StateIndex,
    core::{
        events::{auth::AuthChain, pdu_list_to_event, Event},
        room::{Authorizer, RoomVersion},
    },
    federation::room::RoomError,
    storage::BackwardExtremity,
};

// TODO: Remove this non-spec compliant struct. See Synapse issue: https://github.com/matrix-org/synapse/issues/8786
// In fact, reading less fields do not make this implementation less spec-compliant
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct RoomState {
    /// The full set of authorization events that make up the state of the room,
    /// and their authorization events, recursively.
    pub auth_chain: Vec<Raw<Pdu>>,

    /// The room state.
    pub state: Vec<Raw<Pdu>>,
}

// TODO: Remove this non-spec compliant struct. See Synapse issue: https://github.com/matrix-org/synapse/issues/8786
// In fact, reading less fields do not make this implementation less spec-compliant
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct RoomStateV1(pub u32, pub RoomState);

pub struct RemoteState<'a> {
    pub auth_chain: HashMap<OwnedEventId, Event>,
    pub state: HashMap<OwnedEventId, Event>,
    pub event: &'a Event,
}

impl<'a> RemoteState<'a> {
    pub async fn new(
        room_state: &RoomState,
        event: &'a Event,
        room_version: &RoomVersion,
    ) -> Result<RemoteState<'a>, RoomError> {
        let state_events = pdu_list_to_event(room_version.event_format(), &room_state.state).await;

        let state: HashMap<OwnedEventId, Event> = state_events
            .into_iter()
            .map(|e| (e.event_ref.owned_event_id(), e))
            .collect();

        let auth_chain_events =
            pdu_list_to_event(room_version.event_format(), &room_state.state).await;

        let auth_chain: HashMap<OwnedEventId, Event> = auth_chain_events
            .into_iter()
            .map(|e| (e.event_ref.owned_event_id(), e))
            .collect();

        Ok(RemoteState {
            auth_chain,
            state,
            event,
        })
    }

    pub fn find_backward_extremities(&self) -> Vec<BackwardExtremity> {
        let known_ids = self.collect_event_ids();
        let mut backward_extremities = Vec::new();
        let all_events = self.state.iter().chain(self.auth_chain.iter());

        for (_, e) in all_events {
            let backward_extremity = Self::backward_extremity(e, &known_ids);
            backward_extremities.extend(backward_extremity.into_iter());
        }

        let join_event_backward = Self::backward_extremity(self.event, &known_ids);
        backward_extremities.extend(join_event_backward);

        backward_extremities
    }

    pub fn participating_servers(&self) -> Vec<OwnedServerName> {
        self.state
            .values()
            .chain(self.auth_chain.values())
            .map(|e| e.sender.server_name().to_owned())
            .collect::<HashSet<OwnedServerName>>()
            .into_iter()
            .collect()
    }

    pub fn validate_room_version(&self, room_version: &RoomVersionId) -> Result<(), RoomError> {
        let create_event = self
            .state
            .values()
            .chain(self.auth_chain.values())
            .find(|e| e.kind == TimelineEventType::RoomCreate)
            .map(|e| e.content_as::<RoomCreateEventContent>())
            .transpose()?
            .ok_or(RoomError::MissingAuthEvent(TimelineEventType::RoomCreate))?;

        if &create_event.room_version != room_version {
            return Err(RoomError::Custom("room version mismatch".to_string()));
        }

        Ok(())
    }

    pub fn validate(&self, authorizer: &dyn Authorizer) -> Result<(), RoomError> {
        // Check auth_events for join event
        self.validate_event(self.event, authorizer)?;

        // Check auth_events for all the state events on the DAG (and fetch remote events if needed)
        for e in self.auth_chain.values() {
            self.validate_event(e, authorizer)?;
        }

        // Check state events
        for e in self.state.values() {
            self.validate_event(e, authorizer)?;
        }

        Ok(())
    }

    // TODO: Maybe we should just pass a StateSotrage
    // TODO: Make sure the roomstate is validated!
    pub fn collect_state_indexes(&self) -> HashMap<OwnedEventId, StateIndex> {
        self.state
            .iter()
            .map(|(eid, e)| {
                (
                    eid.clone(),
                    StateIndex::state_key(e.kind.clone(), e.state_key.clone()),
                )
            })
            .collect()
    }

    fn backward_extremity(
        event: &Event,
        known_ids: &BTreeSet<&EventId>,
    ) -> Option<BackwardExtremity> {
        let has_missing_events = event
            .prev_events
            .iter()
            .any(|event_id| !known_ids.contains(event_id.event_id()));

        if has_missing_events {
            let backward_extremity =
                BackwardExtremity::new(event.event_ref.owned_event_id(), event.topological_token());
            Some(backward_extremity)
        } else {
            None
        }
    }

    fn collect_event_ids(&self) -> BTreeSet<&EventId> {
        let mut state_ids = BTreeSet::new();
        for event_id in self.state.keys() {
            state_ids.insert((*event_id).as_ref() as &EventId);
        }
        for event_id in self.auth_chain.keys() {
            state_ids.insert((*event_id).as_ref() as &EventId);
        }
        state_ids.insert(self.event.event_ref.event_id());

        state_ids
    }

    fn validate_event(&self, event: &Event, authorizer: &dyn Authorizer) -> Result<(), RoomError> {
        let auth_chain = self.build_auth_chain(event);
        let auth_result = authorizer.authorize(&auth_chain, event);

        if let Err(e) = auth_result {
            tracing::error!("Authorization error: {}", e);
            return Err(RoomError::Custom("Authorization error".to_string()));
        }

        Ok(())
    }

    #[instrument(skip(self), fields(event = %event.event_ref.owned_event_id()))]
    pub fn build_auth_chain(&self, event: &Event) -> AuthChain {
        let auth_chain: HashMap<StateIndex, &Event> = event
            .auth_events
            .iter()
            .flat_map(|eid| {
                self.state.get(eid.event_id()).map(|event_ref| {
                    let state_index =
                        StateIndex::state_key(event_ref.kind.clone(), event_ref.state_key.clone());
                    (state_index, event_ref)
                })
            })
            .collect();

        auth_chain
    }
}

#[cfg(test)]
mod tests {
    use std::collections::BTreeSet;

    use safareig_core::ruma::{server_name, user_id, EventId, RoomId};

    use crate::{
        client::room::versions::EventIdV3,
        core::events::{EventBuilder, EventContentExt, EventRef},
        federation::room::join::RemoteState,
        testing::TestingDB,
    };

    #[test]
    fn backward_extremity_with_one_unknown_prev_event() {
        let mut known_ids = BTreeSet::new();
        let server_name = server_name!("localhost:8080");
        let user_id = user_id!("@user:localhost:8080");
        let room_id = RoomId::new(server_name);

        let event_id1 = EventId::new(server_name);
        let event_id2 = EventId::new(server_name);
        let event_id3 = EventId::new(server_name);

        known_ids.insert(event_id1.as_ref());
        known_ids.insert(event_id2.as_ref());

        let event = EventBuilder::builder(
            user_id,
            TestingDB::dummy_content().into_content().unwrap(),
            &room_id,
        )
        .prev_events(vec![
            EventRef::V2(event_id1.clone()),
            EventRef::V2(event_id2.clone()),
            EventRef::V2(event_id3),
        ])
        .build(&EventIdV3 {})
        .unwrap();

        let maybe_backward = RemoteState::backward_extremity(&event, &known_ids);

        assert!(maybe_backward.is_some());
    }

    #[test]
    fn is_not_backward_extremity_if_all_prev_events_are_known() {
        let mut known_ids = BTreeSet::new();
        let server_name = server_name!("localhost:8080");
        let user_id = user_id!("@user:localhost:8080");
        let room_id = RoomId::new(server_name);

        let event_id1 = EventId::new(server_name);
        let event_id2 = EventId::new(server_name);
        let event_id3 = EventId::new(server_name);

        known_ids.insert(event_id1.as_ref());
        known_ids.insert(event_id2.as_ref());
        known_ids.insert(event_id3.as_ref());

        let event = EventBuilder::builder(
            user_id,
            TestingDB::dummy_content().into_content().unwrap(),
            &room_id,
        )
        .prev_events(vec![
            EventRef::V2(event_id1.clone()),
            EventRef::V2(event_id2.clone()),
            EventRef::V2(event_id3.clone()),
        ])
        .build(&EventIdV3 {})
        .unwrap();

        let maybe_backward = RemoteState::backward_extremity(&event, &known_ids);

        assert!(maybe_backward.is_none());
    }

    #[test]
    fn is_backward_extremity_if_none_of_the_prev_events_are_known() {
        let known_ids = BTreeSet::new();
        let server_name = server_name!("localhost:8080");
        let user_id = user_id!("@user:localhost:8080");
        let room_id = RoomId::new(server_name);

        let event_id1 = EventId::new(server_name);
        let event_id2 = EventId::new(server_name);
        let event_id3 = EventId::new(server_name);

        let event = EventBuilder::builder(
            user_id,
            TestingDB::dummy_content().into_content().unwrap(),
            &room_id,
        )
        .prev_events(vec![
            EventRef::V2(event_id1),
            EventRef::V2(event_id2),
            EventRef::V2(event_id3),
        ])
        .build(&EventIdV3 {})
        .unwrap();

        let maybe_backward = RemoteState::backward_extremity(&event, &known_ids);

        assert!(maybe_backward.is_some());
    }
}
