use std::{collections::BTreeMap, sync::Arc};

use async_trait::async_trait;
use futures::StreamExt;
use safareig_core::{
    auth::Identity,
    bus::{Bus, BusMessage},
    command::Command,
    config::HomeserverConfig,
    ruma::{
        api::federation::transactions::send_transaction_message::v1 as send_message,
        events::pdu::Pdu, serde::Raw, OwnedEventId, ServerName,
    },
    storage::StorageError,
    Inject,
};
use safareig_federation::{keys::KeyProvider, server::storage::ServerStorage, FederationError};

use crate::{
    client::room::{events::SignError, loader::RoomLoader, RoomError},
    core::{
        events::{
            formatter::{EventFormatterV1, EventFormatterV2},
            Event,
        },
        room::EventFormat,
    },
    federation::{
        backfill::{BackfillJob, Backfiller, RemoteSelectorPolicy},
        edu::EduHandler,
        raw_json_to_raw_pdu,
    },
};

#[derive(Inject)]
pub struct SendCommand {
    room_loader: RoomLoader,
    key_provider: Arc<KeyProvider>,
    backfiller: Backfiller,
    edu_handler: EduHandler,
    config: Arc<HomeserverConfig>,
    server_storage: Arc<dyn ServerStorage>,
    bus: Bus,
}

#[async_trait]
impl Command for SendCommand {
    type Input = send_message::Request;
    type Output = send_message::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let auth = id
            .clone()
            .to_federation_auth()
            .ok_or(FederationError::Unknown)?;
        let raw_pdus: Vec<Raw<Pdu>> = raw_json_to_raw_pdu(input.pdus.clone());

        let results = self.handle_pdus(auth.server.as_ref(), raw_pdus).await?;
        if let Err(e) = self.edu_handler.handle_edus(id, &input.edus).await {
            tracing::error!(
                server = auth.server.as_str(),
                err = e.to_string().as_str(),
                "could not decode incoming EDUs"
            )
        }

        let _ = self.update_status(&auth.server).await;

        Ok(send_message::Response { pdus: results })
    }
}

impl SendCommand {
    /// Insert events the corresponding events to the target room. Note that at this point, we've
    /// validated that incoming events have valid hashes and are properly signed by the remote
    /// server. What we've not checked is that the event can be inserted on the desired position
    /// of the DAG.
    /// TODO: Add auth checks and retrieves the missing events / states in case that we need moew
    /// information to authorize the event.
    async fn insert_room_event(
        &self,
        origin_server: &ServerName,
        event: Event,
    ) -> Result<(), RoomError> {
        let room = self
            .room_loader
            .get_for_server(&event.room_id, origin_server)
            .await?
            .ok_or_else(|| RoomError::RoomNotFound(event.room_id.to_string()))?;

        let mut backfill_job = BackfillJob::new(
            Vec::new(),
            RemoteSelectorPolicy::Fixed(origin_server.to_owned()),
        );
        backfill_job.set_stream_insertion();
        backfill_job
            .handle_incoming_event(event, &room, &self.backfiller)
            .await?;

        Ok(())
    }

    async fn update_status(&self, server_name: &ServerName) -> Result<(), StorageError> {
        let mut server = self.server_storage.load_server(server_name).await?;
        let was_online = server.state().is_online();
        server.received_incoming_request();
        self.server_storage
            .store_server(server_name, &server)
            .await?;

        if !was_online {
            self.bus
                .send_message(BusMessage::ServerServiceRestored(server_name.to_owned()))
        }

        Ok(())
    }

    async fn handle_pdus(
        &self,
        origin_server: &ServerName,
        raw_pdus: Vec<Raw<Pdu>>,
    ) -> Result<BTreeMap<OwnedEventId, Result<(), String>>, RoomError> {
        let events = self.decode_mixed_list_of_pdus(raw_pdus).await;

        let mut results = BTreeMap::new();
        for pdu_result in events {
            match pdu_result {
                Err(e) => {
                    // At this point, we do not have an event id (because deserialization failed).
                    // It would be nice that, if possible, we propagate the event id to this point.
                    // For example, if the source error is a verification error, we could propagate the event id to this point.
                    tracing::error!(
                        err = e.to_string().as_str(),
                        "could not deserialize incoming pdu",
                    )
                }
                Ok(e) => {
                    let event_id = e.event_ref.owned_event_id();
                    let current_event_result = self
                        .insert_room_event(origin_server, e)
                        .await
                        .map_err(|e| e.to_string());

                    if let Err(ref err) = current_event_result {
                        tracing::error!(
                            err = err.to_string().as_str(),
                            event_id = event_id.as_str(),
                            "could not process incoming event",
                        );
                    }

                    results.insert(event_id, current_event_result);
                }
            }
        }

        Ok(results)
    }

    /// Decode a mixed list of pdus from distinct room versions. Note that this is not specially efficient as we are doing some
    /// deserialization round trips to delegate to Ruma the decission of which formats are using these PDUs.
    /// Once we have them deserialized and we know which event format are they using, we can deserialize again from the raw value
    /// with our PDU -> Event deserializer.
    async fn decode_mixed_list_of_pdus(
        &self,
        raw_pdus: Vec<Raw<Pdu>>,
    ) -> Vec<Result<Event, SignError>> {
        let event_format_v1 =
            EventFormatterV1::new(self.key_provider.clone(), self.config.server_name.clone());
        let event_format_v2 =
            EventFormatterV2::new(self.key_provider.clone(), self.config.server_name.clone());

        futures::stream::iter(raw_pdus.iter())
            .then(|raw_pdu| match raw_pdu.deserialize() {
                Err(e) => Box::pin(futures::future::err::<_, SignError>(SignError::from(e))),
                Ok(decoded) => match decoded {
                    Pdu::RoomV1Pdu(_) => event_format_v1.pdu_to_event(raw_pdu),
                    Pdu::RoomV3Pdu(_) => event_format_v2.pdu_to_event(raw_pdu),
                },
            })
            .collect()
            .await
    }
}
