use std::{collections::HashSet, sync::Arc};

use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    command::Command,
    config::HomeserverConfig,
    ruma::{
        api::federation::backfill::get_backfill::v1 as backfill_events, events::pdu::Pdu,
        serde::Raw, MilliSecondsSinceUnixEpoch, OwnedEventId,
    },
    Inject,
};
use safareig_federation::keys::LocalKeyProvider;

use crate::{
    client::room::{loader::RoomLoader, RoomError},
    core::{events::Event, limit},
};

#[derive(Inject)]
pub struct BackfillCommand {
    room_loader: RoomLoader,
    key_provider: Arc<LocalKeyProvider>,
    config: Arc<HomeserverConfig>,
}

#[async_trait]
impl Command for BackfillCommand {
    type Input = backfill_events::Request;
    type Output = backfill_events::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let mut events_to_fetch: Vec<OwnedEventId> = input.v.clone();
        let mut pdus: Vec<Raw<Pdu>> = Vec::new();
        let mut pdu_ids: HashSet<OwnedEventId> = HashSet::new();
        let mut reached_limit = false;
        let limit = limit(input.limit, 100);
        let auth = id.federation_auth();
        let room = self
            .room_loader
            .get_for_server(&input.room_id, &auth.server)
            .await?
            .ok_or_else(|| RoomError::RoomNotFound(input.room_id.to_string()))?;
        let current_key = self.key_provider.current_key().await?;
        // TODO: Check if server belongs to room. Depends on FederationCommand to be implemented.

        while !reached_limit {
            let current_events = room.events(&events_to_fetch).await?;
            let next_ids = Self::find_next_events(&pdu_ids, &current_events);
            let current_events_len = current_events.len();

            for event in current_events {
                if !pdu_ids.contains(event.event_ref.event_id()) {
                    let pdu = room
                        .room_version()
                        .event_format()
                        .event_to_pdu(&event, current_key.as_ref())?;
                    pdus.push(pdu);
                    pdu_ids.insert(event.event_ref.owned_event_id());
                }
            }

            if current_events_len == 0 || pdu_ids.len() >= limit {
                reached_limit = true;
            }

            events_to_fetch = next_ids;
        }

        Ok(backfill_events::Response {
            origin: self.config.server_name.clone(),
            origin_server_ts: MilliSecondsSinceUnixEpoch::now(),
            pdus: pdus.into_iter().map(|raw| raw.json().to_owned()).collect(),
        })
    }
}

impl BackfillCommand {
    fn find_next_events(
        pdu_ids: &HashSet<OwnedEventId>,
        current_events: &[Event],
    ) -> Vec<OwnedEventId> {
        current_events
            .iter()
            .flat_map(|event| event.prev_event_ids())
            .filter(|event_id| !pdu_ids.contains(event_id))
            .collect()
    }
}

#[cfg(test)]
mod tests {
    use safareig_core::{
        command::Command,
        ruma::{
            api::federation::backfill::get_backfill::v1 as backfill_events,
            events::{pdu::Pdu, TimelineEventType},
            serde::Raw,
            UInt,
        },
    };

    use crate::{federation::room::command::backfill::BackfillCommand, testing::TestingDB};

    #[tokio::test]
    async fn stops_iteration_when_backfill_events_near_to_root() {
        let db = TestingDB::default();
        let (creator, room) = db.public_room().await;
        let event_id = db.talk(room.id(), &creator, "msg1").await.unwrap();

        let cmd = db.command::<BackfillCommand>();
        let input = backfill_events::Request {
            v: vec![event_id.clone()],
            room_id: room.id().to_owned(),
            limit: UInt::new_wrapping(100),
        };
        let response = cmd
            .execute(input, TestingDB::federation_identity())
            .await
            .unwrap();

        assert_eq!(11, response.pdus.len());
        let pdus: Vec<Raw<Pdu>> = response.pdus.into_iter().map(Raw::from_json).collect();
        let create = db
            .contains_pdu(&pdus, |p| p.kind == TimelineEventType::RoomCreate)
            .await;
        assert!(create.is_some());
        let given = db.find_pdu_with_id(&pdus, &event_id).await;
        assert!(given.is_some());
    }

    #[tokio::test]
    async fn stops_iteration_when_it_reaches_the_limit() {
        let db = TestingDB::default();
        let (creator, room) = db.public_room().await;
        let event_id = db.talk(room.id(), &creator, "msg1").await.unwrap();

        let cmd = db.command::<BackfillCommand>();
        let input = backfill_events::Request {
            v: vec![event_id.to_owned()],
            room_id: room.id().to_owned(),
            limit: UInt::new_wrapping(2),
        };
        let response = cmd
            .execute(input, TestingDB::federation_identity())
            .await
            .unwrap();

        let pdu_count = response.pdus.len();
        let pdus: Vec<Raw<Pdu>> = response.pdus.into_iter().map(Raw::from_json).collect();
        assert_eq!(2, pdu_count);
        let given = db.find_pdu_with_id(&pdus, &event_id).await;
        assert!(given.is_some());
    }

    #[tokio::test]
    async fn returns_events_for_multiple_requested_events() {
        let db = TestingDB::default();
        let (creator, room) = db.public_room().await;
        let event_id = db.talk(room.id(), &creator, "msg1").await.unwrap();
        let event_id2 = db.talk(room.id(), &creator, "msg2").await.unwrap();

        let cmd = db.command::<BackfillCommand>();
        let input = backfill_events::Request {
            v: vec![event_id.to_owned(), event_id2.to_owned()],
            room_id: room.id().to_owned(),
            limit: UInt::new_wrapping(100),
        };
        let response = cmd
            .execute(input, TestingDB::federation_identity())
            .await
            .unwrap();

        assert_eq!(12, response.pdus.len());
    }

    #[tokio::test]
    async fn does_not_allow_to_backfill_events_to_servers_non_participating_on_the_room() {}

    #[tokio::test]
    async fn backfill_is_stopped_if_server_was_not_present_on_the_correspoing_depth() {}
}
