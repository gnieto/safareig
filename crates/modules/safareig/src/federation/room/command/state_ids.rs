use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity, command::Command,
    ruma::api::federation::event::get_room_state_ids::v1 as state_ids, Inject,
};

use crate::{
    client::room::{loader::RoomLoader, state::RoomState, RoomError},
    storage::{EventStorage, StateStorage},
};

#[derive(Inject)]
pub struct StateIdsCommand {
    room_loader: RoomLoader,
    state: Arc<dyn StateStorage>,
    events: Arc<dyn EventStorage>,
}

#[async_trait]
impl Command for StateIdsCommand {
    type Input = state_ids::Request;
    type Output = state_ids::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let auth = id.federation_auth();
        let room = self
            .room_loader
            .get_for_server(&input.room_id, &auth.server)
            .await?
            .ok_or_else(|| RoomError::RoomNotFound(input.room_id.to_string()))?;

        let state = self
            .state
            .state_at(room.id(), &input.event_id)
            .await?
            .ok_or_else(|| {
                RoomError::Custom("TODO: should fetch state from federation".to_string())
            })?;
        let room_state = RoomState::load(state, self.state.clone());
        let (ids, auth_chain) = room_state.state_events_ids(self.events.as_ref()).await;

        let r = state_ids::Response {
            auth_chain_ids: auth_chain,
            pdu_ids: ids,
        };

        Ok(r)
    }
}
