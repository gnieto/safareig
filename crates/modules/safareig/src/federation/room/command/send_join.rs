use std::sync::Arc;

use async_trait::async_trait;
use reqwest::StatusCode;
use safareig_core::{
    auth::Identity,
    bus::{Bus, BusMessage},
    command::Command,
    config::HomeserverConfig,
    error::RumaExt,
    ruma::{
        api::{
            client::error::{ErrorBody, ErrorKind},
            federation::membership::create_join_event::{
                v1 as send_v1, v1::RoomState as SendJoinRoomStateV1, v2 as send_join,
                v2::RoomState as SendJoinRoomStateV2,
            },
        },
        events::pdu::Pdu,
        serde::Raw,
        OwnedRoomId, OwnedServerName,
    },
    Inject,
};
use safareig_federation::{keys::LocalKeyProvider, server::storage::FederationStorage};

use crate::{
    client::room::{loader::RoomLoader, state::RoomState, RoomError},
    federation::room::sender_matches_identity,
    storage::{EventStorage, StateStorage},
};

#[derive(Clone, Inject)]
pub struct SendJoinCommand {
    room_loader: RoomLoader,
    state_storage: Arc<dyn StateStorage>,
    event_storage: Arc<dyn EventStorage>,
    config: Arc<HomeserverConfig>,
    local_key_provider: Arc<LocalKeyProvider>,
    federation_storage: Arc<dyn FederationStorage>,
    bus: Bus,
}

#[async_trait]
impl Command for SendJoinCommand {
    type Input = send_join::Request;
    type Output = send_join::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let auth = id.federation_auth();
        let raw_pdu: Raw<Pdu> = Raw::from_json(input.pdu.to_owned());

        let room_id = raw_pdu
            .get_field::<OwnedRoomId>("room_id")
            .map_err(RoomError::Serde)?
            .ok_or_else(|| RoomError::Custom("missing expected room_id".to_string()))?;

        let room = self
            .room_loader
            .get_for_server(&room_id, &auth.server)
            .await?
            .ok_or_else(|| RoomError::RoomNotFound(room_id.to_string()))?;

        let version = room.version();

        let event = room
            .room_version()
            .event_format()
            .pdu_to_event(&raw_pdu)
            .await?;

        if !sender_matches_identity(&event.sender, &self.config.server_name, auth) {
            return Err(safareig_core::ruma::api::client::Error {
                body: ErrorBody::Standard {
                    kind: ErrorKind::forbidden(),
                    message: "target user is local or does not match with requesting server"
                        .to_string(),
                },
                status_code: StatusCode::FORBIDDEN,
            }
            .to_generic());
        }

        let remote_server: OwnedServerName = event.sender.server_name().to_owned();

        if event.event_ref.event_id() != input.event_id {
            return Err(RoomError::InvalidEventId.into());
        }

        let state_id = room.insert_event(&event, version, true, true).await?;
        let state_previous_to_event = RoomState::load(state_id, self.state_storage.clone());

        let current_key = self.local_key_provider.current_key().await?;
        let (state_event, auth_chain) = state_previous_to_event
            .state_and_auth_chain(self.event_storage.as_ref())
            .await?;

        let state_raw = state_event
            .into_iter()
            .map(|e| {
                room.room_version()
                    .event_format()
                    .event_to_pdu(&e, current_key.as_ref())
                    .map(|pdu| pdu.into_json())
            })
            .collect::<Result<_, _>>()?;

        let auth_chain_raw = auth_chain
            .into_iter()
            .map(|e| {
                room.room_version()
                    .event_format()
                    .event_to_pdu(&e, current_key.as_ref())
                    .map(|pdu| pdu.into_json())
            })
            .collect::<Result<_, _>>()?;

        self.federation_storage
            .track_server(&room_id, remote_server.as_ref())
            .await?;

        let send_join_room_state = SendJoinRoomStateV2 {
            origin: self.config.server_name.to_string(),
            auth_chain: auth_chain_raw,
            state: state_raw,
            // TODO: Fill the evnet when restricted join gets implemented
            event: None,
            members_omitted: false,
            servers_in_room: None,
        };

        self.bus.send_message(BusMessage::ServerTracked(
            remote_server.to_owned(),
            room_id.to_owned(),
        ));

        Ok(send_join::Response::new(send_join_room_state))
    }
}

#[derive(Inject)]
pub struct SendJoinV1Command {
    cmd: SendJoinCommand,
}

#[async_trait]
impl Command for SendJoinV1Command {
    type Input = send_v1::Request;
    type Output = send_v1::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let request = send_join::Request {
            room_id: input.room_id.to_owned(),
            event_id: input.event_id.to_owned(),
            pdu: input.pdu.to_owned(),
            omit_members: false,
        };

        let response = self.cmd.execute(request, id).await?;

        let room_state = SendJoinRoomStateV1 {
            origin: response.room_state.origin,
            auth_chain: response.room_state.auth_chain,
            state: response.room_state.state,
            event: response.room_state.event,
        };

        Ok(send_v1::Response { room_state })
    }
}

#[cfg(test)]
mod tests {}
