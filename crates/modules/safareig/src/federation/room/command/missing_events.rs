use std::{cmp::min, sync::Arc};

use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    command::Command,
    ruma::{api::federation::event::get_missing_events::v1 as missing_events, OwnedEventId},
    Inject,
};
use safareig_federation::keys::LocalKeyProvider;
use serde_json::value::RawValue;

use crate::client::room::{loader::RoomLoader, Room, RoomError};

#[derive(Inject)]
pub struct MissingEventCommand {
    room_loader: RoomLoader,
    key_provider: Arc<LocalKeyProvider>,
}

#[async_trait]
impl Command for MissingEventCommand {
    type Input = missing_events::Request;
    type Output = missing_events::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let auth = id.federation_auth();
        let room = self
            .room_loader
            .get_for_server(&input.room_id, &auth.server)
            .await?
            .ok_or_else(|| RoomError::RoomNotFound(input.room_id.to_string()))?;
        let mut pdus = Vec::new();
        let mut pending = self.prev_events(&room, &input.latest_events).await;
        let limit = u64::from(input.limit) as usize;
        let limit = min(limit, 50);
        let min_depth = u64::from(input.min_depth);

        while pdus.len() < limit && !pending.is_empty() {
            let next_ids = self
                .append_missing_events(
                    &mut pdus,
                    &room,
                    &pending,
                    &input.earliest_events,
                    min_depth,
                )
                .await?;
            pending = next_ids;
        }

        Ok(missing_events::Response { events: pdus })
    }
}

impl MissingEventCommand {
    #[tracing::instrument(skip_all)]
    async fn prev_events(&self, room: &Room, event_ids: &[OwnedEventId]) -> Vec<OwnedEventId> {
        let mut prev_events = Vec::new();
        for eid in event_ids {
            if let Ok(Some(e)) = room.event(eid).await {
                let current_prev_events: Vec<OwnedEventId> =
                    e.prev_events.into_iter().map(|r| r.into()).collect();
                prev_events.extend_from_slice(&current_prev_events);
            }
        }

        prev_events
    }

    #[tracing::instrument(skip_all)]
    async fn append_missing_events(
        &self,
        pdus: &mut Vec<Box<RawValue>>,
        room: &Room,
        pending: &[OwnedEventId],
        earliest: &[OwnedEventId],
        min_depth: u64,
    ) -> Result<Vec<OwnedEventId>, RoomError> {
        let mut next_ids = Vec::new();
        let hs_private_key = self.key_provider.current_key().await?;

        for eid in pending {
            if earliest.contains(eid) {
                continue;
            }

            if let Some(room_event) = room.event(eid).await? {
                if room_event.depth < min_depth {
                    // If current event is below the minimum depth, check the following event
                    continue;
                }

                next_ids.extend_from_slice(&room_event.prev_event_ids());

                let raw_pdu = room
                    .room_version()
                    .event_format()
                    .event_to_pdu(&room_event, hs_private_key.as_ref())?
                    .json()
                    .to_owned();

                pdus.push(raw_pdu);
            }
        }

        Ok(next_ids)
    }
}

#[cfg(test)]
mod tests {

    use std::sync::Arc;

    use safareig_core::{
        command::Command,
        ruma::{
            api::federation::event::get_missing_events::v1 as missing_events,
            events::{pdu::Pdu, TimelineEventType},
            serde::Raw,
            UInt,
        },
    };

    use crate::{
        client::room::{events::PduExt, versions::EventIdV3},
        core::events::{EventBuilder, EventRef},
        federation::room::command::missing_events::MissingEventCommand,
        storage::{
            EventOptions, EventStorage, RoomStreamStorage, RoomTopologyStorage, StateStorage,
        },
        testing::TestingDB,
    };

    #[tokio::test]
    async fn it_returns_prev_events() {
        let testing = TestingDB::default();
        let (creator, room) = testing.public_room().await;
        let a = testing.talk(room.id(), &creator, "msg1").await.unwrap();
        let b = testing.talk(room.id(), &creator, "msg2").await.unwrap();
        let _c = testing.talk(room.id(), &creator, "msg3").await.unwrap();
        let d = testing.talk(room.id(), &creator, "msg4").await.unwrap();

        let cmd = testing.command::<MissingEventCommand>();
        let input = missing_events::Request {
            earliest_events: vec![a.clone()],
            latest_events: vec![d],
            limit: UInt::new_wrapping(10),
            min_depth: UInt::new_wrapping(0),
            room_id: room.id().to_owned(),
        };
        let response = cmd
            .execute(input, TestingDB::federation_identity())
            .await
            .unwrap();

        assert_eq!(2, response.events.len());
        let raw: Raw<Pdu> = Raw::from_json(response.events.first().unwrap().clone());
        let event1: Pdu = raw.deserialize().unwrap();
        assert_eq!(event1.prev_events(), &vec![b]);
        let raw: Raw<Pdu> = Raw::from_json(response.events.get(1).unwrap().clone());
        let event2: Pdu = raw.deserialize().unwrap();
        assert_eq!(event2.prev_events(), &vec![a]);
    }

    #[tokio::test]
    async fn it_does_not_return_prev_events_if_min_depth_is_below() {
        let testing = TestingDB::default();
        let (creator, room) = testing.public_room().await;
        let a = testing.talk(room.id(), &creator, "msg1").await.unwrap();
        let _b = testing.talk(room.id(), &creator, "msg2").await.unwrap();
        let _c = testing.talk(room.id(), &creator, "msg3").await.unwrap();
        let d = testing.talk(room.id(), &creator, "msg4").await.unwrap();

        let cmd = testing.command::<MissingEventCommand>();
        let input = missing_events::Request {
            earliest_events: vec![a.to_owned()],
            latest_events: vec![d],
            limit: UInt::new_wrapping(10),
            min_depth: UInt::new_wrapping(15),
            room_id: room.id().to_owned(),
        };
        let response = cmd
            .execute(input, TestingDB::federation_identity())
            .await
            .unwrap();

        assert_eq!(0, response.events.len());
    }

    #[tokio::test]
    async fn it_returns_limited_events() {
        let testing = TestingDB::default();
        let (creator, room) = testing.public_room().await;
        let _a = testing.talk(room.id(), &creator, "msg1").await.unwrap();
        let b = testing.talk(room.id(), &creator, "msg2").await.unwrap();
        let _c = testing.talk(room.id(), &creator, "msg3").await.unwrap();
        let d = testing.talk(room.id(), &creator, "msg4").await.unwrap();

        let cmd = testing.command::<MissingEventCommand>();
        let input = missing_events::Request {
            earliest_events: vec![],
            latest_events: vec![d],
            limit: UInt::new_wrapping(1),
            min_depth: UInt::new_wrapping(0),
            room_id: room.id().to_owned(),
        };
        let response = cmd
            .execute(input, TestingDB::federation_identity())
            .await
            .unwrap();

        assert_eq!(1, response.events.len());
        let raw: Raw<Pdu> = Raw::from_json(response.events.first().unwrap().clone());
        let event1: Pdu = raw.deserialize().unwrap();
        assert_eq!(event1.prev_events(), &vec![b]);
    }

    #[tokio::test]
    async fn it_returns_all_events_with_events_with_multiple_prev_events() {
        let testing = TestingDB::default();
        let (creator, room) = testing.public_room().await;
        let a = testing.talk(room.id(), &creator, "msg1").await.unwrap();
        let a = EventRef::V2(a);
        let b = testing.talk(room.id(), &creator, "msg2").await.unwrap();
        let b = EventRef::V2(b);
        let event = room.event(b.event_id()).await.unwrap().unwrap();
        // Assign same state id as the previous one
        let state = testing.container.service::<Arc<dyn StateStorage>>();
        let stream = testing.container.service::<Arc<dyn RoomStreamStorage>>();
        let topology = testing.container.service::<Arc<dyn RoomTopologyStorage>>();
        let events = testing.container.service::<Arc<dyn EventStorage>>();

        let state_id = state
            .state_at(room.id(), b.event_id())
            .await
            .unwrap()
            .unwrap();

        let new_event = EventBuilder::from(&event)
            .prev_events(vec![a.clone(), b.clone()])
            .depth(event.depth + 1)
            .auth_events(event.auth_events.clone())
            .build(&EventIdV3 {})
            .unwrap();
        let new_event_id = new_event.event_ref.owned_event_id();

        let options = EventOptions::default().set_previous_room_state_id(state_id);
        events.put_event(&new_event, options).await.unwrap();

        let _ = stream.insert_event_to_stream(&new_event).await.unwrap();

        topology
            .adjust_extremities(room.id(), &new_event, state_id)
            .await
            .unwrap();

        let cmd = testing.command::<MissingEventCommand>();
        let input = missing_events::Request {
            earliest_events: vec![],
            latest_events: vec![new_event_id.clone()],
            limit: UInt::new_wrapping(1),
            min_depth: UInt::new_wrapping(0),
            room_id: room.id().to_owned(),
        };
        let response = cmd
            .execute(input, TestingDB::federation_identity())
            .await
            .unwrap();

        assert_eq!(2, response.events.len());
        let raw: Raw<Pdu> = Raw::from_json(response.events.first().unwrap().clone());
        let event1: Pdu = raw.deserialize().unwrap();
        assert_eq!(event1.kind(), &TimelineEventType::RoomMessage);
        let raw: Raw<Pdu> = Raw::from_json(response.events.get(1).unwrap().clone());
        let event1: Pdu = raw.deserialize().unwrap();
        assert_eq!(event1.kind(), &TimelineEventType::RoomMessage);
    }
}
