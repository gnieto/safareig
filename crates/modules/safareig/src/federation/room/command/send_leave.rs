use std::sync::Arc;

use async_trait::async_trait;
use reqwest::StatusCode;
use safareig_core::{
    auth::Identity,
    command::Command,
    config::HomeserverConfig,
    error::RumaExt,
    ruma::{
        api::{
            client::error::{ErrorBody, ErrorKind},
            federation::membership::create_leave_event::v2 as send_leave,
        },
        events::pdu::Pdu,
        serde::Raw,
        OwnedRoomId, OwnedServerName,
    },
    Inject,
};

use crate::{
    client::room::{loader::RoomLoader, RoomError},
    federation::room::sender_matches_identity,
};

#[derive(Inject)]
pub struct SendLeaveCommand {
    room_loader: RoomLoader,
    config: Arc<HomeserverConfig>,
}

#[async_trait]
impl Command for SendLeaveCommand {
    type Input = send_leave::Request;
    type Output = send_leave::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let auth = id.federation_auth();
        let raw_pdu: Raw<Pdu> = Raw::from_json(input.pdu.to_owned());

        let room_id = raw_pdu
            .get_field::<OwnedRoomId>("room_id")
            .map_err(RoomError::Serde)?
            .ok_or_else(|| RoomError::Custom("missing expected room_id".to_string()))?;

        let room = self
            .room_loader
            .get_for_server(&room_id, &auth.server)
            .await?
            .ok_or_else(|| RoomError::RoomNotFound(room_id.to_string()))?;
        let version = room.version();

        let event = room
            .room_version()
            .event_format()
            .pdu_to_event(&raw_pdu)
            .await?;

        if !sender_matches_identity(&event.sender, &self.config.server_name, auth) {
            return Err(safareig_core::ruma::api::client::Error {
                body: ErrorBody::Standard {
                    kind: ErrorKind::forbidden(),
                    message: "target user is local or does not match with requesting server"
                        .to_string(),
                },
                status_code: StatusCode::FORBIDDEN,
            }
            .to_generic());
        }

        let _remote_server: OwnedServerName = event.sender.server_name().to_owned();

        if event.event_ref.event_id() != input.event_id {
            return Err(RoomError::InvalidEventId.into());
        }

        room.insert_event(&event, version, true, true).await?;

        Ok(send_leave::Response::new())
    }
}
