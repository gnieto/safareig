use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    bus::{Bus, BusMessage},
    command::Command,
    ruma::{
        api::{client::sync::sync_events::v3::InviteState, federation::membership::create_invite},
        events::{pdu::Pdu, room::member::MembershipState},
        serde::Raw,
        UserId,
    },
    Inject, ServerInfo,
};
use safareig_federation::{keys::KeyProvider, FederationError};

use crate::{
    client::room::{loader::RoomLoader, RoomError},
    core::{
        events::event_to_raw_pdu,
        room::{OutOfRoomMemberEvents, RoomVersionRegistry},
    },
    storage::{Invite, InviteStorage, RemoteInvite},
};

#[derive(Inject)]
pub struct InviteRoomCommand {
    invite: Arc<dyn InviteStorage>,
    room_version_loader: Arc<RoomVersionRegistry>,
    key_provider: Arc<KeyProvider>,
    server_info: ServerInfo,
    bus: Bus,
    room_loader: RoomLoader,
    out_of_room_events: OutOfRoomMemberEvents,
}

#[async_trait]
impl Command for InviteRoomCommand {
    type Input = create_invite::v2::Request;
    type Output = create_invite::v2::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;

    async fn execute(
        &self,
        mut input: Self::Input,
        id: Identity,
    ) -> Result<Self::Output, Self::Error> {
        let auth = id.federation_auth();
        let room = self
            .room_loader
            .get_for_server(&input.room_id, &auth.server)
            .await?;

        // Verify if room version is allowed
        let version = self
            .room_version_loader
            .get(&input.room_version)
            .ok_or_else(|| RoomError::UnsupportedRoomVersion(input.room_version.clone()))?;

        let raw_pdu: Raw<Pdu> = Raw::from_json(input.event.to_owned());
        let event = version.event_format().pdu_to_event(&raw_pdu).await?;
        let inviter = event.sender.to_owned();
        let raw_signed_pdu = event_to_raw_pdu(
            &event,
            self.server_info.server_name(),
            version.id(),
            self.key_provider.current_key().await?.as_ref(),
        )?;

        // Verify matching sender
        if event.sender.server_name() != auth.server {
            return Err(FederationError::Custom(
                "Sender user should match with remote identity".to_string(),
            )
            .into());
        }

        // Parse state key as target invited user
        let invitee = event
            .state_key
            .as_ref()
            .ok_or_else(|| FederationError::Custom("expected state key".to_string()))?;
        let invitee = UserId::parse(invitee.as_str())
            .map_err(|_| RoomError::Custom("state key is not a valid user_id".to_string()))?;

        // Add current event to invite room state
        input
            .invite_room_state
            .push(event.clone().try_into().map_err(RoomError::from)?);

        let remote_invite = RemoteInvite {
            state: InviteState {
                events: input.invite_room_state,
            },
            room: input.room_id.to_owned(),
            room_version: input.room_version,
        };

        let _ = self
            .invite
            .add_invite(&invitee, Invite::Remote(remote_invite))
            .await?;

        match room {
            Some(room) => {
                // The homeserver already knows about this room. We will insert the event as we receive it from the invitee homeserver.
                room.insert_event(&event, room.version(), true, true)
                    .await?;
                tracing::debug!(
                    event_id = event.event_ref.event_id().as_str(),
                    "Inserting invite to room's graph because room is already tracked",
                );
            }
            None => {
                tracing::debug!(
                    event_id = event.event_ref.event_id().as_str(),
                    "Inserting invite directly to event stream",
                );
                // Homeserver does not participate in the room. We need to store the event out of band
                self.out_of_room_events
                    .insert_event(&invitee, event, MembershipState::Invite)
                    .await?;
            }
        }

        self.bus.send_message(BusMessage::RoomInvited(
            input.room_id.to_owned(),
            invitee.to_owned(),
            inviter,
        ));

        Ok(create_invite::v2::Response {
            event: raw_signed_pdu.into_json(),
        })
    }
}
