use std::sync::Arc;

use async_trait::async_trait;
use reqwest::StatusCode;
use safareig_core::{
    auth::Identity,
    command::Command,
    config::HomeserverConfig,
    error::RumaExt,
    ruma::api::{
        client::error::{ErrorBody, ErrorKind},
        federation::membership::prepare_leave_event::v1 as make_leave,
    },
    Inject,
};
use safareig_federation::FederationError;
use safareig_users::profile::ProfileLoader;

use crate::{
    client::room::{loader::RoomLoader, RoomError},
    federation::room::sender_matches_identity,
};

#[derive(Inject)]
pub struct MakeLeaveCommand {
    room_loader: RoomLoader,
    profile_loader: ProfileLoader,
    config: Arc<HomeserverConfig>,
}

#[async_trait]
impl Command for MakeLeaveCommand {
    type Input = make_leave::Request;
    type Output = make_leave::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let auth = id.federation_auth();
        // TODO: ¿Check if current homeserver still has presence on the room?
        if !sender_matches_identity(&input.user_id, &self.config.server_name, auth) {
            return Err(safareig_core::ruma::api::client::Error {
                body: ErrorBody::Standard {
                    kind: ErrorKind::forbidden(),
                    message: "target user is local or does not match with requesting server"
                        .to_string(),
                },
                status_code: StatusCode::FORBIDDEN,
            }
            .to_generic());
        }

        let room = self
            .room_loader
            .get_for_server(&input.room_id, &auth.server)
            .await?
            .ok_or_else(|| RoomError::RoomNotFound(input.room_id.to_string()))?;

        let room_version = room.version();

        let event = room
            .make_leave(&input.user_id, &self.profile_loader)
            .await?;

        let pdu_template = room
            .room_version()
            .event_format()
            .event_to_pdu_template(event);
        let raw_event =
            serde_json::value::to_raw_value(&pdu_template).map_err(FederationError::from)?;

        let response = make_leave::Response {
            room_version: Some(room_version.to_owned()),
            event: raw_event,
        };

        Ok(response)
    }
}
