use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity, command::Command,
    ruma::api::federation::event::get_room_state::v1 as get_state, Inject,
};
use safareig_federation::keys::KeyProvider;

use crate::{
    client::room::{loader::RoomLoader, state::RoomState, RoomError},
    storage::{EventStorage, StateStorage},
};

#[derive(Inject)]
pub struct StateCommand {
    room_loader: RoomLoader,
    state: Arc<dyn StateStorage>,
    events: Arc<dyn EventStorage>,
    key_provider: Arc<KeyProvider>,
}

#[async_trait]
impl Command for StateCommand {
    type Input = get_state::Request;
    type Output = get_state::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let auth = id.federation_auth();
        // TODO: Check if remote server belongs to target room id
        let room = self
            .room_loader
            .get_for_server(&input.room_id, &auth.server)
            .await?
            .ok_or_else(|| RoomError::RoomNotFound(input.room_id.to_string()))?;

        let state = self
            .state
            .state_at(room.id(), &input.event_id)
            .await?
            .ok_or_else(|| {
                RoomError::Custom("TODO: should fetch state from federation".to_string())
            })?;

        let room_state = RoomState::load(state, self.state.clone());
        let (ids, auth_chain) = room_state
            .state_and_auth_chain(self.events.as_ref())
            .await?;

        let current_key = self.key_provider.current_key().await?;
        // TODO: Duplicated from /send_join
        let state_raw = ids
            .into_iter()
            .map(|e| {
                room.room_version()
                    .event_format()
                    .event_to_pdu(&e, current_key.as_ref())
                    .map(|pdu| pdu.into_json())
            })
            .collect::<Result<_, _>>()?;

        let auth_chain_raw = auth_chain
            .into_iter()
            .map(|e| {
                room.room_version()
                    .event_format()
                    .event_to_pdu(&e, current_key.as_ref())
                    .map(|pdu| pdu.into_json())
            })
            .collect::<Result<_, _>>()?;

        Ok(get_state::Response {
            auth_chain: auth_chain_raw,
            pdus: state_raw,
        })
    }
}
