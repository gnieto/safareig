use std::sync::Arc;

use async_trait::async_trait;
use reqwest::StatusCode;
use safareig_core::{
    auth::Identity,
    command::Command,
    config::HomeserverConfig,
    error::RumaExt,
    ruma::api::{
        client::error::{ErrorBody, ErrorKind},
        federation::membership::prepare_join_event::v1 as make_join,
    },
    Inject,
};
use safareig_federation::FederationError;
use safareig_users::profile::ProfileLoader;

use crate::{
    client::room::{loader::RoomLoader, RoomError},
    federation::room::sender_matches_identity,
};

#[derive(Inject)]
pub struct MakeJoinCommand {
    room_loader: RoomLoader,
    profile_loader: ProfileLoader,
    config: Arc<HomeserverConfig>,
}

#[async_trait]
impl Command for MakeJoinCommand {
    type Input = make_join::Request;
    type Output = make_join::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let federation_auth = id.federation_auth();

        // TODO: ¿Check if current homeserver still has presence on the room?
        if !sender_matches_identity(&input.user_id, &self.config.server_name, federation_auth) {
            return Err(safareig_core::ruma::api::client::Error {
                body: ErrorBody::Standard {
                    kind: ErrorKind::forbidden(),
                    message: "target user is local or does not match with requesting server"
                        .to_string(),
                },
                status_code: StatusCode::FORBIDDEN,
            }
            .to_generic());
        }

        let room = self
            .room_loader
            .get_for_server(&input.room_id, &federation_auth.server)
            .await?
            .ok_or_else(|| RoomError::RoomNotFound(input.room_id.to_string()))?;
        let room_version = room.version();

        if !input.ver.contains(room_version) {
            return Err(safareig_core::ruma::api::client::Error {
                body: ErrorBody::Standard {
                    kind: ErrorKind::IncompatibleRoomVersion {
                        room_version: room_version.to_owned(),
                    },
                    message: "non supported room version".to_string(),
                },
                status_code: StatusCode::BAD_REQUEST,
            }
            .to_generic());
        }

        let event = room.make_join(&input.user_id, &self.profile_loader).await?;

        let pdu_template = room
            .room_version()
            .event_format()
            .event_to_pdu_template(event);
        let raw_event =
            serde_json::value::to_raw_value(&pdu_template).map_err(FederationError::from)?;

        let response = make_join::Response {
            room_version: Some(room_version.to_owned()),
            event: raw_event,
        };

        Ok(response)
    }
}

#[cfg(test)]
mod tests {
    use reqwest::StatusCode;
    use safareig_core::{
        command::Command,
        ruma::{
            api::federation::membership::prepare_join_event::v1 as make_event, user_id, RoomId,
        },
    };

    use crate::{federation::room::command::make_join::MakeJoinCommand, testing::TestingDB};

    #[tokio::test]
    async fn checks_room_version_incompatbility_on_make_join() {}

    #[tokio::test]
    async fn it_returns_forbidden_on_local_user() {
        let db = TestingDB::default();
        let local_user = TestingDB::new_identity();
        let cmd = db.command::<MakeJoinCommand>();

        let input = make_event::Request {
            room_id: RoomId::new(&db.server_name()),
            user_id: local_user.user().to_owned(),
            ver: vec![db.default_room_version()],
        };
        let id = TestingDB::federation_identity();

        let err = cmd.execute(input, id).await.err().unwrap();

        assert_eq!(StatusCode::FORBIDDEN, err.status_code);
    }

    #[tokio::test]
    async fn it_returns_forbidden_if_auth_does_not_match() {
        let db = TestingDB::default();
        let cmd = db.command::<MakeJoinCommand>();
        let input = make_event::Request {
            room_id: RoomId::new(&db.server_name()),
            user_id: user_id!("@usuari:localhost:8448").to_owned(),
            ver: vec![db.default_room_version()],
        };
        let id = TestingDB::federation_identity();

        let err = cmd.execute(input, id).await.err().unwrap();

        assert_eq!(StatusCode::FORBIDDEN, err.status_code);
    }
}
