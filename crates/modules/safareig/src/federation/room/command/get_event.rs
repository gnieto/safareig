use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    command::Command,
    config::HomeserverConfig,
    error::{ResourceNotFound, RumaExt},
    ruma::{
        api::federation::event::get_event::v1 as get_event, events::pdu::Pdu, serde::Raw,
        MilliSecondsSinceUnixEpoch,
    },
    Inject,
};
use safareig_federation::{
    keys::LocalKeyProvider, server::storage::FederationStorage, FederationError,
};

use crate::{client::room::loader::RoomLoader, core::events::Event, storage::EventStorage};

#[derive(Inject)]
pub struct GetEventCommand {
    events: Arc<dyn EventStorage>,
    room_loader: RoomLoader,
    federation_storage: Arc<dyn FederationStorage>,
    key_provider: Arc<LocalKeyProvider>,
    config: Arc<HomeserverConfig>,
}

#[async_trait]
impl Command for GetEventCommand {
    type Input = get_event::Request;
    type Output = get_event::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let auth = id.to_federation_auth().ok_or(FederationError::Unknown)?;

        let event = self
            .events
            .get_event(&input.event_id)
            .await?
            .ok_or(ResourceNotFound)?;

        let tracked_rooms_for_incoming_server = self
            .federation_storage
            .server_rooms(auth.server.as_ref())
            .await?;

        if !tracked_rooms_for_incoming_server.contains(&event.room_id) {
            tracing::error!(
                incoming = auth.server.to_string().as_str(),
                event_id = event.event_ref.to_string().as_str(),
                "Server is requesting an event id from a room in which it does not participate"
            );
            return Err(ResourceNotFound.into());
        }

        let pdu = self
            .build_pdu(&event)
            .await
            .map_err(|e| e.to_generic())?
            .json()
            .to_owned();

        Ok(get_event::Response {
            origin: self.config.server_name.clone(),
            origin_server_ts: MilliSecondsSinceUnixEpoch::now(),
            pdu,
        })
    }
}

impl GetEventCommand {
    async fn build_pdu(
        &self,
        event: &Event,
    ) -> Result<Raw<Pdu>, safareig_core::ruma::api::client::Error> {
        let room = self
            .room_loader
            .get(&event.room_id)
            .await?
            .ok_or(ResourceNotFound)?;

        let hs_private_key = self.key_provider.current_key().await?;

        Ok(room
            .room_version()
            .event_format()
            .event_to_pdu(event, hs_private_key.as_ref())?)
    }
}

#[cfg(test)]
mod tests {
    use std::sync::Arc;

    use reqwest::StatusCode;
    use safareig_core::{
        command::Command,
        pagination,
        ruma::{
            api::{federation::event::get_event::v1 as get_event, Direction},
            events::pdu::Pdu,
            serde::Raw,
        },
    };
    use safareig_federation::server::storage::FederationStorage;

    use crate::{
        client::room::events::PduExt,
        federation::room::command::get_event::GetEventCommand,
        testing::{generate_event_id, generate_federation_auth, TestingDB},
    };

    #[tokio::test]
    async fn get_unexisting_event_returns_404() {
        let db = TestingDB::default();

        let input = get_event::Request {
            event_id: generate_event_id(),
        };
        let auth = generate_federation_auth();
        let cmd = db.command::<GetEventCommand>();
        let result = cmd.execute(input, auth).await;

        assert!(result.is_err());
        assert_eq!(result.err().unwrap().status_code, StatusCode::NOT_FOUND);
    }

    #[tokio::test]
    async fn event_from_a_non_tracked_server_returns_404() {
        let db = TestingDB::default();
        let (_, room) = db.public_room().await;

        let event = room
            .topological_events(pagination::Range::all(), 10, Direction::Forward)
            .await
            .unwrap()
            .records()[0]
            .clone();

        let input = get_event::Request {
            event_id: event.event_ref.owned_event_id(),
        };
        let auth = generate_federation_auth();
        let cmd = db.command::<GetEventCommand>();
        let result = cmd.execute(input, auth).await;

        assert!(result.is_err());
        assert_eq!(result.err().unwrap().status_code, StatusCode::NOT_FOUND);
    }

    #[tokio::test]
    async fn existing_event_is_signed_and_returned() {
        let db = TestingDB::default();
        let (_, room) = db.public_room().await;
        let event = room
            .topological_events(pagination::Range::all(), 10, Direction::Forward)
            .await
            .unwrap()
            .records()[0]
            .clone();

        let input = get_event::Request {
            event_id: event.event_ref.owned_event_id(),
        };
        let id = generate_federation_auth();
        let auth = id.clone().to_federation_auth().unwrap();

        db.container
            .service::<Arc<dyn FederationStorage>>()
            .track_server(&event.room_id, &auth.server)
            .await
            .unwrap();

        let cmd = db.command::<GetEventCommand>();
        let result = cmd.execute(input, id).await.unwrap();

        let raw: Raw<Pdu> = Raw::from_json(result.pdu);
        let pdu = raw.deserialize().unwrap();
        assert_eq!(pdu.prev_events(), &event.prev_event_ids());
    }
}
