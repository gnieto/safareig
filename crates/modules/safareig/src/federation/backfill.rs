use std::{
    collections::{BTreeSet, HashMap, HashSet},
    sync::Arc,
};

use futures::{future::BoxFuture, FutureExt};
use itertools::Itertools;
use rand::seq::SliceRandom;
use safareig_core::{
    pagination::Range,
    ruma::{serde::Raw, EventId, OwnedEventId, OwnedRoomId, OwnedServerName, TransactionId, UInt},
    Inject,
};
use safareig_federation::{
    client::{ClientBuilder, FederationClient},
    server::storage::FederationStorage,
};

use super::raw_json_to_raw_pdu;
use crate::{
    client::room::{
        state::{RoomState, RoomStateId, StateIndex, StateInfo},
        Room, RoomError,
    },
    core::events::{auth::AuthError, pdu_list_to_event, Event},
    storage::{
        BackwardExtremity, EventOptions, EventStorage, RoomStorage, RoomTopologyStorage,
        StateStorage,
    },
};

/// Retrieves missing events from remote servers.
#[derive(Clone, Inject)]
pub struct Backfiller {
    pub room_storage: Arc<dyn RoomStorage>,
    pub event_storage: Arc<dyn EventStorage>,
    pub topology: Arc<dyn RoomTopologyStorage>,
    pub state: Arc<dyn StateStorage>,
    pub federation: Arc<ClientBuilder>,
    remote_selector: RemoteSelector,
}

#[derive(Clone)]
pub enum RemoteSelectorPolicy {
    /// Strictly use the given remote server
    Fixed(OwnedServerName),

    /// Select a random participating server on the given room
    Random(OwnedRoomId),

    /// (unimplemented): From the participating server, select the one of the nth with the highest score.
    /// Score is an heuristic calculated from latest latencies and also the quality of it's responses
    Best(OwnedRoomId, u32),
}

pub struct BackfillJob {
    backward_extremities: Vec<BackwardExtremity>,

    /// Fuel indicates the amount of events that can be fetched from remote servers
    /// until the backfill process fail.
    /// This is in place to prevent infinite loops derived of fetching missing events.
    fuel: u8,

    /// Stream insertion controls wheter the backfilled events should be added to the event
    /// stream. Usually, only events backfilled from events received via federation, but not when
    /// joining to a new room
    stream_insertion: bool,

    /// Execute the backfill targetting this server
    selector: RemoteSelectorPolicy,

    /// Mapping of event to RoomStateId backfilled during this job
    event_to_state_id: HashMap<OwnedEventId, RoomStateId>,
}

impl BackfillJob {
    pub fn new(extremities: Vec<BackwardExtremity>, selector: RemoteSelectorPolicy) -> Self {
        BackfillJob {
            backward_extremities: extremities,
            fuel: 50,
            stream_insertion: false,
            selector,
            event_to_state_id: HashMap::new(),
        }
    }

    pub fn set_stream_insertion(&mut self) {
        self.stream_insertion = true;
    }

    pub fn get_state_id_for_event(&self, event_id: &EventId) -> Option<RoomStateId> {
        self.event_to_state_id.get(event_id).cloned()
    }

    #[tracing::instrument(skip(self, room, backfiller, event_fetcher))]
    pub async fn backfill(
        &mut self,
        room: &Room,
        backfiller: &Backfiller,
        event_fetcher: &dyn EventFetcher,
    ) -> Result<(), RoomError> {
        let server = backfiller
            .remote_selector
            .select_server(&self.selector)
            .await?;
        let client = backfiller.federation.client_for(&server).await?;
        let pdus = event_fetcher
            .missing_events(client, room, backfiller.topology.as_ref())
            .await?;

        for event in pdus {
            let has_event = room.has_event(event.event_ref.event_id()).await?;
            if has_event {
                tracing::info!(
                    event_id = event.event_ref.event_id().as_str(),
                    "received event which we already have"
                );

                // We received this event from a backfill/get_missing_events, but we already have this event tracked.
                // This does not mean that we have the state of this event tracked. In Synapse, those events are called "outliers".
                // We check if we have the state and, if we don't, we try to obtain the state at the current event. This
                // will try to calculate the state if we have the state at prev events or it will fetch the state ids/state from a
                // remote server.
                if backfiller
                    .state
                    .state_at(room.id(), event.event_ref.event_id())
                    .await
                    .ok()
                    .flatten()
                    .is_none()
                {
                    if let Err(error) = room.state_on_event(&event).await {
                        tracing::error!(
                            ?error,
                            event_id = event.event_ref.event_id().as_str(),
                            "could not resolve the state at event",
                        )
                    }
                }

                continue;
            }

            let event_id = event.event_ref.owned_event_id();
            if let Err(e) = self.backfill_single(event, room, backfiller).await {
                tracing::error!(
                    event_id = event_id.as_str(),
                    err = e.to_string().as_str(),
                    "could not accept incoming event."
                );
            }
        }

        // Prune current backward extermities
        for extremity in &self.backward_extremities {
            if let Some(event) = room.event(extremity.event_id()).await? {
                if !self.is_backward_extremity(&event, room).await? {
                    backfiller
                        .topology
                        .remove_backward_extremity(room.id(), extremity)
                        .await?;
                }
            }
        }

        Ok(())
    }

    #[tracing::instrument(skip(self, room, backfiller), fields(event = %event.event_ref.owned_event_id()))]
    pub async fn handle_incoming_event(
        &mut self,
        mut event: Event,
        room: &Room,
        backfiller: &Backfiller,
    ) -> Result<(), RoomError> {
        // TODO: Find a better place for this method
        self.handle_missing_events(room, backfiller, &event).await?;
        let soft_failed = self.check_soft_failure(room, &event, backfiller).await?;
        let version = room.version();

        if soft_failed {
            event
                .unsigned
                .insert("soft_failed".to_string(), serde_json::Value::Bool(true));
            let _ = room.insert_event(&event, version, false, false).await?;
        } else {
            let _ = room.insert_event(&event, version, true, true).await?;
        }

        Ok(())
    }

    /// Checks if event is acceptable/authorized at leaves (or forward extremities). If the state at
    /// the forward extremities is the same as the state at the event, we've already authorized it
    /// and we do not need to check it again.
    /// Otherwise, we will reauthorize the event and, if is not authorized, we should `soft-fail`
    /// the event.
    #[tracing::instrument(skip_all)]
    async fn check_soft_failure(
        &self,
        room: &Room,
        event: &Event,
        backfiller: &Backfiller,
    ) -> Result<bool, RoomError> {
        let state_leaves = room.state_at_leaves().await?;

        let auth_result = self
            .authorize_with_room_state(event, &state_leaves, room, backfiller)
            .await;
        if auth_result.is_err() {
            tracing::warn!(
                "Soft-failing event due event not authorized on forward extremities: {:?}",
                auth_result.as_ref().err().unwrap()
            )
        }

        Ok(auth_result.is_err())
    }

    #[tracing::instrument(skip_all)]
    async fn authorize_with_room_state(
        &self,
        event: &Event,
        room_state: &RoomState,
        room: &Room,
        backfiller: &Backfiller,
    ) -> Result<(), RoomError> {
        let state_info = event.state_key.as_ref().map(|_| event.into());
        let auth_events = room_state
            .auth_events(&event.sender, state_info, backfiller.event_storage.as_ref())
            .await?;

        let auth_chain = Room::build_auth_chain(&auth_events);

        Ok(room
            .room_version()
            .authorizer()
            .authorize(&auth_chain, event)?)
    }

    #[tracing::instrument(skip(self, room, backfiller), fields(event = %event.event_ref.owned_event_id()))]
    async fn backfill_single(
        &mut self,
        event: Event,
        room: &Room,
        backfiller: &Backfiller,
    ) -> Result<(), RoomError> {
        self.fuel = self.fuel.saturating_sub(1);
        if self.fuel == 0 {
            return Err(RoomError::Custom(
                "no more fuel to backfill events".to_string(),
            ));
        }

        let mut options = EventOptions::default().set_transaction_id(&TransactionId::new());

        // Check auth rules
        let result = self.check_auth_rules(&event, room).await;
        if let Err(RoomError::Unauthorized(_)) = result {
            options = options.set_rejected(true);
        } else {
            result?;
        }

        if !options.is_rejected() {
            // Check with state at the given prev event state
            self.check_with_state(&event, room, backfiller).await?;

            // Maybe this should return the state id. If we do, we could assign the same state id to the
            // following events, which would lead to much less calls to the /state endpoint.
            self.handle_backward_extremities(&event, &room, backfiller)
                .await?;
        }

        // Insert event if current event passed all the state checks
        room.insert_backfill_event(&event, self.stream_insertion, options)
            .await?;

        Ok(())
    }

    #[tracing::instrument(skip_all)]
    async fn handle_backward_extremities(
        &mut self,
        event: &Event,
        room: &&Room,
        backfiller: &Backfiller,
    ) -> Result<(), RoomError> {
        let topological_token = event.topological_token();
        if self.is_backward_extremity(event, room).await? {
            let extremity =
                BackwardExtremity::new(event.event_ref.owned_event_id(), topological_token);
            backfiller
                .topology
                .add_backward_extremities(room.id(), &[extremity.clone()])
                .await?;
            self.backward_extremities.push(extremity);
        }

        Ok(())
    }

    #[tracing::instrument(skip_all)]
    async fn check_auth_rules(&mut self, event: &Event, room: &Room) -> Result<(), RoomError> {
        let auth_event_ids: Vec<OwnedEventId> = event
            .auth_events
            .iter()
            .map(|r| r.event_id().to_owned())
            .collect();
        let auth_events = room.events(&auth_event_ids).await?;
        if auth_events.len() != event.auth_events.len() {
            return Err(RoomError::Unauthorized(AuthError::MissingAuthEvents));
        }

        let auth_chain = Room::build_auth_chain(&auth_events);
        room.room_version()
            .authorizer()
            .authorize(&auth_chain, event)?;

        Ok(())
    }

    #[tracing::instrument(skip_all)]
    async fn check_with_state(
        &mut self,
        event: &Event,
        room: &Room,
        backfiller: &Backfiller,
    ) -> Result<(), RoomError> {
        let mut state_at_event = self
            .event_to_state_id
            .get(&event.event_ref.owned_event_id())
            .map(|state_id| {
                crate::client::room::state::RoomState::load(*state_id, backfiller.state.clone())
            });

        if state_at_event.is_none() {
            state_at_event = room.state_on_event(event).await.ok();
        }

        match state_at_event {
            Some(room_state) => {
                let state_info = StateInfo::from(event);
                let calculated_auth_events: BTreeSet<OwnedEventId> = room_state
                    .auth_events(
                        &event.sender,
                        Some(state_info),
                        backfiller.event_storage.as_ref(),
                    )
                    .await?
                    .into_iter()
                    .map(|event| event.event_ref.owned_event_id())
                    .collect();

                check_auth_chain(event, calculated_auth_events)?;

                Ok(())
            }
            None => Err(RoomError::Custom(
                "could not fetch remote state".to_string(),
            )),
        }
    }

    #[tracing::instrument(skip_all)]
    pub fn backfill_missing_event<'a>(
        &'a mut self,
        event_id: OwnedEventId,
        room: &'a Room,
        backfiller: &'a Backfiller,
    ) -> BoxFuture<'a, Result<(), RoomError>> {
        async move {
            self.fuel = self.fuel.saturating_sub(1);
            if self.fuel == 0 {
                return Err(RoomError::Custom(
                    "no more fuel to backfill events".to_string(),
                ));
            }

            let server = backfiller
                .remote_selector
                .select_server(&self.selector)
                .await?;
            let client = backfiller.federation.client_for(&server).await?;

            let request =
                safareig_core::ruma::api::federation::event::get_event::v1::Request { event_id };
            let response = client.auth_request(request).await?;
            let raw = Raw::from_json(response.pdu);
            let event = room
                .room_version()
                .event_format()
                .pdu_to_event(&raw)
                .await?;
            self.backfill_single(event, room, backfiller).await?;

            Ok(())
        }
        .boxed()
    }

    #[tracing::instrument(skip_all)]
    async fn is_backward_extremity(&self, event: &Event, room: &Room) -> Result<bool, RoomError> {
        let mut is_backward = false;
        for prev in &event.prev_events {
            if !room.has_event(prev.event_id()).await? {
                is_backward = true;
                break;
            }
        }

        Ok(is_backward)
    }

    #[tracing::instrument(skip_all)]
    async fn handle_missing_events(
        &mut self,
        room: &Room,
        backfiller: &Backfiller,
        event: &Event,
    ) -> Result<(), RoomError> {
        let required_events = event.prev_event_ids();
        let mut has_missing_event = false;

        for previous in required_events {
            has_missing_event = !room.has_event(&previous).await?;

            if has_missing_event {
                break;
            }
        }

        if !has_missing_event {
            return Ok(());
        }

        self.get_missing_events(room, backfiller, event).await?;

        Ok(())
    }

    #[tracing::instrument(skip_all)]
    async fn get_missing_events(
        &mut self,
        room: &Room,
        backfiller: &Backfiller,
        event: &Event,
    ) -> Result<(), RoomError> {
        let fetcher = GetMissingEventsEventFetcher::new(vec![event.event_ref.owned_event_id()]);

        let server = backfiller
            .remote_selector
            .select_server(&self.selector)
            .await?;
        let client = backfiller.federation.client_for(&server).await?;

        let response = fetcher
            .missing_events(client.clone(), room, backfiller.topology.as_ref())
            .await?;

        for missing_event in response {
            self.backfill_single(missing_event, room, backfiller)
                .await?;
        }

        Ok(())
    }
}

#[derive(Inject)]
pub struct RemoteStateFetcher {
    remote_selector: RemoteSelector,
    client_builder: Arc<ClientBuilder>,
    state: Arc<dyn StateStorage>,
}

impl RemoteStateFetcher {
    #[tracing::instrument(skip(self, room, event, selector))]
    pub async fn fetch_state(
        &self,
        room: &Room,
        event: &EventId,
        selector: &RemoteSelectorPolicy,
    ) -> Result<RoomStateId, RoomError> {
        // Fetch state from remote and calculate auth chain from there.
        // Compare to given auth events.
        let server = self.remote_selector.select_server(selector).await?;
        let client = self.client_builder.client_for(&server).await?;

        let state_id_result = self.state_ids(room, event, client.clone()).await;

        if let Err(error) = &state_id_result {
            tracing::info!(?error, "could not obtain state_ids from remote server",);
        }

        if let Some(state_id) = state_id_result.ok().flatten() {
            return Ok(state_id);
        }

        let request = safareig_core::ruma::api::federation::event::get_room_state::v1::Request {
            room_id: room.id().to_owned(),
            event_id: event.to_owned(),
        };
        let room_state: safareig_core::ruma::api::federation::event::get_room_state::v1::Response =
            client.auth_request(request).await?;

        let auth_chain = raw_json_to_raw_pdu(room_state.auth_chain);
        let state = raw_json_to_raw_pdu(room_state.pdus);

        for pdu in auth_chain {
            let event = room
                .room_version()
                .event_format()
                .pdu_to_event(&pdu)
                .await?;

            // TODO: Add rejected/outlier flag
            let options = EventOptions::default();
            room.insert_backfill_event(&event, false, options).await?;
        }

        let mut state_indexes = HashMap::new();
        for pdu in state {
            let event = room
                .room_version()
                .event_format()
                .pdu_to_event(&pdu)
                .await?;
            state_indexes.insert(event.event_ref.owned_event_id(), StateIndex::from(&event));

            // TODO: Add rejected/outlier flag
            let options = EventOptions::default();
            room.insert_backfill_event(&event, false, options).await?;
        }

        let state_id = self.state.store_state(&state_indexes).await?;

        self.state.assign_state(room.id(), event, &state_id).await?;

        Ok(state_id)
    }

    #[tracing::instrument(skip_all)]
    async fn state_ids(
        &self,
        room: &Room,
        event: &EventId,
        client: FederationClient,
    ) -> Result<Option<RoomStateId>, RoomError> {
        let request =
            safareig_core::ruma::api::federation::event::get_room_state_ids::v1::Request {
                room_id: room.id().to_owned(),
                event_id: event.to_owned(),
            };

        let room_state_ids = client.auth_request(request).await?;
        let mut missing_events = HashSet::new();

        for eid in room_state_ids
            .pdu_ids
            .iter()
            .chain(room_state_ids.auth_chain_ids.iter())
        {
            if !room.has_event(eid).await? {
                missing_events.insert(eid);
            }

            if missing_events.len() >= 50 {
                // If there are more than 50 unkonwn events, return none, which will signal the caller to
                // try to obtain the missing events via a /state call.
                return Ok(None);
            }
        }

        // TODO: Add first auth events?
        for missing in missing_events {
            // Obtain events via /event. Those events will be stored in the event storage and flagged
            // as as unauhtorized if they do not pass the auth rules.
            let request = safareig_core::ruma::api::federation::event::get_event::v1::Request {
                event_id: missing.to_owned(),
            };
            let event = client.auth_request(request).await?;
            let event = room
                .room_version()
                .event_format()
                .pdu_to_event(&Raw::from_json(event.pdu))
                .await?;

            // TODO: Add rejected/outlier flag
            let options = EventOptions::default();
            room.insert_backfill_event(&event, false, options).await?;
        }

        let state_indexes = room
            .events(&room_state_ids.pdu_ids)
            .await?
            .iter()
            .map(|e| (e.event_ref.owned_event_id(), StateIndex::from(e)))
            .collect();

        let state_id = self.state.store_state(&state_indexes).await?;

        self.state.assign_state(room.id(), event, &state_id).await?;

        Ok(Some(state_id))
    }
}

#[async_trait::async_trait]
pub trait EventFetcher: Send + Sync {
    async fn missing_events(
        &self,
        client: FederationClient,
        room: &Room,
        topology: &dyn RoomTopologyStorage,
    ) -> Result<Vec<Event>, RoomError>;
}

pub struct BackfillEventFetcher;

impl Default for BackfillEventFetcher {
    fn default() -> Self {
        Self::new()
    }
}

impl BackfillEventFetcher {
    pub fn new() -> Self {
        Self {}
    }
}

#[async_trait::async_trait]
impl EventFetcher for BackfillEventFetcher {
    async fn missing_events(
        &self,
        client: FederationClient,
        room: &Room,
        topology: &dyn RoomTopologyStorage,
    ) -> Result<Vec<Event>, RoomError> {
        let backward_extremities = topology
            .backward_extremities(room.id(), Range::all())
            .await?;
        let backfill_for: Vec<OwnedEventId> = backward_extremities
            .iter()
            .map(|e| e.event_id().to_owned())
            .collect();

        let backfill = safareig_core::ruma::api::federation::backfill::get_backfill::v1::Request {
            room_id: room.id().to_owned(),
            v: backfill_for,
            limit: UInt::new_wrapping(100),
        };
        let response = client.auth_request(backfill).await?;
        let raw_pdus = raw_json_to_raw_pdu(response.pdus);
        let pdus = pdu_list_to_event(room.room_version().event_format(), &raw_pdus).await;

        let pdus: Vec<Event> = pdus
            .into_iter()
            .sorted_by(|left, right| Ord::cmp(&left.depth, &right.depth))
            .collect();

        Ok(pdus)
    }
}

pub struct GetMissingEventsEventFetcher {
    missing_events: Vec<OwnedEventId>,
}

impl GetMissingEventsEventFetcher {
    pub fn new(missing_events: Vec<OwnedEventId>) -> Self {
        Self { missing_events }
    }
}

#[async_trait::async_trait]
impl EventFetcher for GetMissingEventsEventFetcher {
    async fn missing_events(
        &self,
        client: FederationClient,
        room: &Room,
        topology: &dyn RoomTopologyStorage,
    ) -> Result<Vec<Event>, RoomError> {
        let forward_extremities = topology.forward_extremities(room.id()).await?;
        let earliest = forward_extremities
            .into_iter()
            .map(|forward| forward.event_id.owned_event_id())
            .collect();
        let latest = self.missing_events.to_vec();

        let get_missing_events =
            safareig_core::ruma::api::federation::event::get_missing_events::v1::Request {
                room_id: room.id().to_owned(),
                limit: UInt::new_saturating(100),
                min_depth: UInt::new_saturating(0),
                earliest_events: earliest,
                latest_events: latest,
            };

        let response = client.auth_request(get_missing_events).await?;
        let raw_pdus = raw_json_to_raw_pdu(response.events);
        let pdus = pdu_list_to_event(room.room_version().event_format(), &raw_pdus).await;

        let pdus: Vec<Event> = pdus
            .into_iter()
            .sorted_by(|left, right| Ord::cmp(&left.depth, &right.depth))
            .collect();

        Ok(pdus)
    }
}

fn check_auth_chain(
    event: &Event,
    calculated_auth_events: BTreeSet<OwnedEventId>,
) -> Result<(), RoomError> {
    let given_auth_events: BTreeSet<OwnedEventId> = event
        .auth_events
        .iter()
        .map(|r| r.event_id().to_owned())
        .collect();

    if given_auth_events != calculated_auth_events {
        tracing::error!(
            given_auth_events = given_auth_events.iter().join(",").as_str(),
            calculated = calculated_auth_events.iter().join(",").as_str(),
            "Mismatching auth chain",
        );

        return Err(RoomError::Unauthorized(AuthError::MismatchingAuthEvents));
    }

    Ok(())
}

#[derive(Clone, Inject)]
pub struct RemoteSelector {
    federation_storage: Arc<dyn FederationStorage>,
}

impl RemoteSelector {
    async fn select_server(
        &self,
        policy: &RemoteSelectorPolicy,
    ) -> Result<OwnedServerName, RoomError> {
        match &policy {
            RemoteSelectorPolicy::Fixed(server) => Ok(server.to_owned()),
            RemoteSelectorPolicy::Best(room_id, _) | RemoteSelectorPolicy::Random(room_id) => {
                let participating_servers = self
                    .federation_storage
                    .participating_servers(room_id)
                    .await?;

                let random_server = participating_servers
                    .choose(&mut rand::thread_rng())
                    .cloned()
                    .ok_or_else(|| RoomError::Custom("current room does not have any participating server other than current homeserver".to_string()))?;
                Ok(random_server)
            }
        }
    }
}
