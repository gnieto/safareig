use std::{
    collections::{HashMap, HashSet},
    sync::Arc,
    time::Duration,
};

use futures::FutureExt;
use graceful::{BackgroundHandler, Link, WaitResult};
use opentelemetry::KeyValue;
use safareig_core::{
    bus::{Bus, BusMessage},
    config::HomeserverConfig,
    error::error_chain,
    pagination::{self, Pagination},
    ruma::{
        api::federation::transactions::{
            edu::Edu, send_transaction_message::v1 as federation_send,
        },
        serde::Raw,
        MilliSecondsSinceUnixEpoch, OwnedRoomId, OwnedServerName, OwnedTransactionId, ServerName,
        TransactionId,
    },
    storage::StorageError,
    StreamToken,
};
use safareig_ephemeral::services::{
    EphemeralEvent, EphemeralRegistry, EphemeralStream, EphemeralToken,
};
use safareig_federation::{
    client::ClientBuilder,
    keys::KeyProvider,
    server::storage::{FederationStorage, ServerStorage},
    FederationError,
};
use serde::{Deserialize, Serialize};
use serde_json::value::to_raw_value;
use tokio::{task::JoinHandle, time::sleep};
use tracing::instrument;

use crate::{
    client::room::RoomStream,
    core::{
        events::{
            formatter::{EventFormatterV1, EventFormatterV2},
            Event, EventRef,
        },
        room::EventFormat,
        waker::Waker,
    },
    metrics::{OUTGOING_FEDERATION_EDUS, OUTGOING_FEDERATION_EVENTS},
    storage::CheckpointStorage,
};

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Checkpoint {
    latest: FederationToken,
    in_flight: Option<(FederationToken, FederationToken)>,
    txn_id: Option<OwnedTransactionId>,
    #[serde(default)]
    error_count: u32,
}

impl Checkpoint {
    pub fn latest_pdu(&self) -> StreamToken {
        self.latest.pdus
    }

    pub fn latest_edu(&self) -> EphemeralToken {
        self.latest.edus
    }

    pub fn update_pdu_checkpoint(&mut self, token: StreamToken) {
        self.latest.pdus = token;
    }

    fn increase_error(&mut self, token: &FederationToken) -> bool {
        self.error_count = self.error_count.saturating_add(1);
        let has_too_many_errors = self.error_count >= 5;

        if has_too_many_errors {
            self.update_checkpoint(token);
        }

        has_too_many_errors
    }

    fn update_checkpoint(&mut self, token: &FederationToken) {
        self.latest = token.clone();
        self.txn_id = None;
        self.in_flight = None;
        self.error_count = 0;
    }
}

#[derive(Deserialize, Serialize, Debug, Clone)]
struct FederationToken {
    pdus: StreamToken,
    edus: EphemeralToken,
}

pub struct FederationWorkerFactory {
    worker_services: WorkerServices,
}

impl FederationWorkerFactory {
    #[allow(clippy::too_many_arguments)]
    pub fn new(
        homeserver_config: Arc<HomeserverConfig>,
        room_stream: RoomStream,
        checkpoints: Arc<dyn CheckpointStorage<Checkpoint>>,
        key_provider: Arc<KeyProvider>,
        federation_builder: Arc<ClientBuilder>,
        bus: Bus,
        ephemeral_stream: EphemeralStream,
        federation_storage: Arc<dyn FederationStorage>,
        ephemeral_registry: EphemeralRegistry,
        server_storage: Arc<dyn ServerStorage>,
    ) -> Self {
        Self {
            worker_services: WorkerServices {
                homeserver_config,
                room_stream,
                checkpoints,
                key_provider,
                federation_builder,
                bus,
                ephemeral_stream,
                federation_storage,
                ephemeral_registry,
                server_storage,
            },
        }
    }

    pub async fn build(
        &self,
        link: graceful::Link,
        server_name: &ServerName,
    ) -> Option<FederationWorker> {
        if self.worker_services.homeserver_config.server_name == server_name {
            tracing::error!("Refusing to create a federation worker for local server");

            return None;
        }

        Some(FederationWorker::new(self.worker_services.clone(), link, server_name).await)
    }
}

// TODO: Probably this should not be public
pub struct FederationWorker {
    services: WorkerServices,
    link: graceful::Link,
    checkpoint: Checkpoint,
    worker_id: String,
    server_name: OwnedServerName,
    tracked_rooms: HashSet<OwnedRoomId>,
    formatter_v1: EventFormatterV1,
    formatter_v2: EventFormatterV2,
}

impl FederationWorker {
    async fn new(
        worker_services: WorkerServices,
        link: graceful::Link,
        server_name: &ServerName,
    ) -> Self {
        let worker_id = format!("fed_{server_name}");
        let current = worker_services
            .room_stream
            .current_stream_token()
            .await
            .unwrap_or_default();

        let checkpoint = worker_services
            .checkpoints
            .get_checkpoint(&worker_id)
            .await
            .ok()
            .flatten()
            .unwrap_or(Checkpoint {
                latest: FederationToken {
                    pdus: current,
                    edus: EphemeralToken::new(),
                },
                in_flight: None,
                txn_id: None,
                error_count: 0,
            });

        let rooms = worker_services
            .federation_storage
            .server_rooms(server_name)
            .await
            .unwrap_or_default();

        let formatter_v1 = EventFormatterV1::new(
            worker_services.key_provider.clone(),
            worker_services.homeserver_config.server_name.clone(),
        );
        let formatter_v2 = EventFormatterV2::new(
            worker_services.key_provider.clone(),
            worker_services.homeserver_config.server_name.clone(),
        );

        FederationWorker {
            services: worker_services,
            link,
            checkpoint,
            worker_id,
            tracked_rooms: rooms,
            server_name: server_name.to_owned(),
            formatter_v1,
            formatter_v2,
        }
    }

    #[instrument(name = "federation_worker::run", skip(self), fields(server = self.server_name.as_str()))]
    pub async fn run(mut self) {
        tracing::info!("Running worker {}", self.worker_id);
        if let Err(e) = self.send_pending_events().await {
            tracing::error!("Error sending pending events: {}", e);
        }

        let mut rx = self.services.bus.receiver();

        loop {
            let event_result = self.retrieve_events().await;
            let wait_for_events = match &event_result {
                Ok(r) => {
                    let has_new_content = (r.token.pdus > self.checkpoint.latest.pdus)
                        || !r.edus.is_empty()
                        || !r.pdus.is_empty();

                    !has_new_content
                }
                Err(e) => {
                    tracing::error!(
                        pdu_token = self.checkpoint.latest.pdus.to_string().as_str(),
                        edu_token = self.checkpoint.latest.edus.to_string().as_str(),
                        worker_id = self.worker_id.as_str(),
                        err = e.to_string().as_str(),
                        "Errored while fetching new events to send to app service",
                    );

                    true
                }
            };

            let in_flight_events = event_result.unwrap();
            if wait_for_events {
                let wakeup = Waker::federation_event(&self.server_name, &mut rx)
                    .boxed()
                    .fuse();

                match self.link.wait_select(wakeup).await {
                    WaitResult::Stop => break,
                    WaitResult::Result(bus_message) => {
                        if let BusMessage::ServerTracked(_, room) = bus_message {
                            self.tracked_rooms.insert(room);
                        }

                        continue;
                    }
                }
            }

            match self.send_events(in_flight_events).await {
                Err(e) => {
                    tracing::error!(
                        err = error_chain(&e).as_str(),
                        server_name = self.server_name.as_str(),
                        "could not send events to federation. Will wait some time to retry.",
                    );

                    let server = self
                        .services
                        .server_storage
                        .load_server(&self.server_name)
                        .await;

                    let delay = match server {
                        Ok(mut server) => {
                            let new_state = server.state_mut().error_received();
                            let delay = new_state.delay();
                            let _ = self
                                .services
                                .server_storage
                                .store_server(&self.server_name, &server)
                                .await;

                            if delay.is_none() {
                                tracing::error!(
                                    server_name = self.server_name.as_str(),
                                    "Server reached the maximum amount of failed send requests. Federation worker will be stopped until the server is allowed by an admin or a successful incoming send request is received",
                                );

                                break;
                            }

                            delay.unwrap()
                        }
                        Err(e) => {
                            tracing::error!(
                                server_name = self.server_name.as_str(),
                                err = error_chain(&e).as_str(),
                                "could not load server information. Using default timeout",
                            );

                            Duration::from_secs(5)
                        }
                    };

                    let delay = sleep(delay).boxed().fuse();
                    match self.link.wait_select(delay).await {
                        WaitResult::Stop => break,
                        WaitResult::Result(_) => continue,
                    }
                }
                Ok(_) => {
                    let server = self
                        .services
                        .server_storage
                        .load_server(&self.server_name)
                        .await;

                    if let Ok(mut server) = server {
                        server.state_mut().set_online();
                        let _ = self
                            .services
                            .server_storage
                            .store_server(&self.server_name, &server)
                            .await;
                    }
                }
            }
        }

        tracing::debug!("Closed worker: {}", self.worker_id);
    }

    async fn retrieve_events(&self) -> Result<InFlightEvents, StorageError> {
        let from_token = self.checkpoint.latest.clone();
        let range = (
            pagination::Boundary::Exclusive(self.checkpoint.latest.pdus),
            pagination::Boundary::Unlimited,
        );

        let stream_response = self
            .services
            .room_stream
            .stream_events(range.into(), None)
            .await?;

        let pdus_token = stream_response
            .next()
            .cloned()
            .unwrap_or(self.checkpoint.latest.pdus);

        let events = stream_response
            .records()
            .into_iter()
            .map(|tuple| tuple.1)
            .collect();

        let range = (
            pagination::Boundary::Exclusive(self.checkpoint.latest.edus),
            pagination::Boundary::Unlimited,
        );
        let query = Pagination::new(range.into(), ());
        let response = self.services.ephemeral_stream.events(query);

        let ephemeral_token = response
            .next()
            .cloned()
            .unwrap_or(self.checkpoint.latest.edus);
        let ephemeral_events = response.records();

        let token = FederationToken {
            pdus: pdus_token,
            edus: ephemeral_token,
        };

        Ok(InFlightEvents {
            pdus: events,
            edus: ephemeral_events,
            token,
            from_token,
        })
    }

    async fn send_pending_events(&mut self) -> Result<(), FederationError> {
        if let Some((from, mut to)) = self.checkpoint.in_flight.clone() {
            let range = (
                pagination::Boundary::Exclusive(from.pdus),
                pagination::Boundary::Inclusive(to.pdus),
            );

            let pdus = self
                .services
                .room_stream
                .stream_events(range.into(), None)
                .await?
                .records()
                .into_iter()
                .map(|(_, e)| e)
                .collect();

            let txn_id = self
                .checkpoint
                .txn_id
                .clone()
                .unwrap_or_else(TransactionId::new);

            // Ignore previous unsent edus
            to.edus = EphemeralToken::new();

            let events = InFlightEvents {
                pdus,
                edus: vec![],
                from_token: from.clone(),
                token: to.clone(),
            };

            if !events.pdus.is_empty() || !events.edus.is_empty() {
                self.send_events_to_server(txn_id, events).await?;
            }

            self.checkpoint_to(to).await?;
        }

        Ok(())
    }

    async fn checkpoint_preflight(
        &mut self,
        current: FederationToken,
    ) -> Result<OwnedTransactionId, StorageError> {
        let mut checkpoint = self.checkpoint.clone();
        checkpoint.in_flight = Some((checkpoint.latest.clone(), current));
        let txn_id = TransactionId::new();
        checkpoint.txn_id = Some(txn_id.clone());

        self.services
            .checkpoints
            .update_checkpoint(&self.worker_id, &checkpoint)
            .await?;
        self.checkpoint = checkpoint;

        Ok(txn_id)
    }

    async fn send_events_to_server(
        &mut self,
        txn_id: OwnedTransactionId,
        events: InFlightEvents,
    ) -> Result<(), FederationError> {
        let mut pdus = Vec::new();

        let client = self
            .services
            .federation_builder
            .client_for(&self.server_name)
            .await?;

        let hs_private_key = self.services.key_provider.current_key().await?;

        for room_event in events.pdus.into_iter() {
            if !self.should_send_to_remote_homeserver(&room_event) {
                continue;
            }

            // TODO: Prevent hash and sign for each server. Probably this could be cached
            // or maybe it could be sent via some kind of broadcast channel
            let raw_pdu = match room_event.event_ref {
                EventRef::V1(_, _) => self
                    .formatter_v1
                    .event_to_pdu(&room_event, hs_private_key.as_ref())
                    .map_err(|e| {
                        tracing::error!("Could not convert event to Pdu: {}", e);
                        FederationError::Unknown
                    })?,
                EventRef::V2(_) => self
                    .formatter_v2
                    .event_to_pdu(&room_event, hs_private_key.as_ref())
                    .map_err(|e| {
                        tracing::error!("Could not convert event to Pdu: {}", e);
                        FederationError::Unknown
                    })?,
            };

            // If conversion of a single pdu fails, we need to bubble up the error to retry the whole
            // batch of events.
            pdus.push(raw_pdu.json().to_owned());
        }

        let edus = self
            .prepare_edus(events.edus)
            .await
            .into_iter()
            .map(|edu: Edu| to_raw_value(&edu).map(Raw::from_json))
            .collect::<Result<Vec<Raw<Edu>>, _>>()?;

        let pdu_len = pdus.len();
        let edu_len = edus.len();
        let request = federation_send::Request {
            origin: self.services.homeserver_config.server_name.to_owned(),
            origin_server_ts: MilliSecondsSinceUnixEpoch::now(),
            transaction_id: txn_id.to_owned(),
            pdus,
            edus,
        };
        let response = client.auth_request(request).await.map_err(|e| {
            tracing::error!(
                err = e.to_string().as_str(),
                server = self.server_name.as_str(),
                "Could not send new events to remote server",
            );
            FederationError::Unknown
        })?;

        let mut contains_error = false;
        for (eid, r) in response.pdus {
            if let Err(e) = r {
                tracing::error!("Error sending event id {}. Reason: {}", eid, e);
                contains_error = true;
            }
        }

        OUTGOING_FEDERATION_EVENTS.add(
            u64::try_from(pdu_len).unwrap_or(0),
            &[KeyValue::new("server", self.server_name.to_string())],
        );
        OUTGOING_FEDERATION_EDUS.add(
            u64::try_from(edu_len).unwrap_or(0),
            &[KeyValue::new("server", self.server_name.to_string())],
        );

        match contains_error {
            false => Ok(()),
            true => Err(FederationError::Unknown),
        }
    }

    async fn prepare_edus(&self, ephemeral_events: Vec<EphemeralEvent>) -> Vec<Edu> {
        let mut aggregators = HashMap::new();
        let mut edus = Vec::new();

        for e in ephemeral_events {
            let event_type = e.event_type();
            let handler = aggregators.entry(event_type.clone()).or_insert_with(|| {
                self.services
                    .ephemeral_registry
                    .handler(event_type)
                    .edu_aggregator()
            });

            handler.aggregate_event(e).await;
        }

        for aggregator in aggregators.into_values() {
            edus.extend_from_slice(&aggregator.edus());
        }

        edus
    }

    /// Checks if the given event should be sent to the homeserver which controls the current worker
    /// The event should be sent to the remote homeserver if it belongs to the room
    /// and the sender is from the local homeserver.
    fn should_send_to_remote_homeserver(&mut self, room_event: &Event) -> bool {
        self.tracked_rooms.contains(&room_event.room_id)
            && room_event.sender.server_name() == self.services.homeserver_config.server_name
    }

    async fn checkpoint_to(&mut self, latest: FederationToken) -> Result<(), FederationError> {
        tracing::info!("Checkpoint {} up to {:?}", self.worker_id, latest);
        let mut checkpoint = self.checkpoint.clone();
        checkpoint.update_checkpoint(&latest);

        self.services
            .checkpoints
            .update_checkpoint(&self.worker_id, &checkpoint)
            .await?;
        self.checkpoint = checkpoint;

        Ok(())
    }

    async fn checkpoint_error(
        &mut self,
        from: FederationToken,
        latest: FederationToken,
    ) -> Result<(), FederationError> {
        tracing::info!(
            worker_id = self.worker_id,
            error_count = self.checkpoint.error_count,
            "Error while sending events via federation"
        );

        let mut checkpoint = self.checkpoint.clone();
        let has_skipped = checkpoint.increase_error(&latest);

        if has_skipped {
            tracing::warn!(
                worker_id = self.worker_id,
                from_pdus = from.pdus.to_string(),
                to_pdus = latest.pdus.to_string(),
                "Target server errored many times. Will update checkpoint up to next batch."
            );
        }

        self.services
            .checkpoints
            .update_checkpoint(&self.worker_id, &checkpoint)
            .await?;
        self.checkpoint = checkpoint;

        Ok(())
    }

    #[instrument(name = "federation_worker::send_events", skip(self, events), fields(pdus = events.pdus.len(), edus = events.edus.len(), server = self.server_name.as_str()))]
    async fn send_events(&mut self, events: InFlightEvents) -> Result<(), FederationError> {
        let to = events.token.clone();
        let from = events.from_token.clone();
        let txn_id = self.checkpoint_preflight(to.clone()).await?;
        let result = self.send_events_to_server(txn_id, events).await;

        match &result {
            Ok(_) => self.checkpoint_to(to).await?,
            Err(_) => self.checkpoint_error(from, to).await?,
        }

        result
    }
}

struct InFlightEvents {
    pdus: Vec<Event>,
    edus: Vec<EphemeralEvent>,
    from_token: FederationToken,
    token: FederationToken,
}

#[derive(Clone)]
struct WorkerServices {
    homeserver_config: Arc<HomeserverConfig>,
    room_stream: RoomStream,
    checkpoints: Arc<dyn CheckpointStorage<Checkpoint>>,
    key_provider: Arc<KeyProvider>,
    federation_builder: Arc<ClientBuilder>,
    bus: Bus,
    ephemeral_stream: EphemeralStream,
    federation_storage: Arc<dyn FederationStorage>,
    ephemeral_registry: EphemeralRegistry,
    server_storage: Arc<dyn ServerStorage>,
}

pub struct FederationSupervisor {
    servers: HashMap<OwnedServerName, JoinHandle<()>>,
    background: BackgroundHandler,
    link: Link,
    bus: Bus,
    factory: FederationWorkerFactory,
}

impl FederationSupervisor {
    pub fn new(background: BackgroundHandler, bus: Bus, factory: FederationWorkerFactory) -> Self {
        Self {
            servers: HashMap::new(),
            link: background.acquire().unwrap(),
            background,
            factory,
            bus,
        }
    }

    pub async fn run(mut self) {
        loop {
            let wakeup = Waker::server_tracked(self.bus.receiver()).boxed().fuse();

            match self.link.wait_select(wakeup).await {
                WaitResult::Stop => break,
                WaitResult::Result(server) => {
                    self.start_server(&server).await;
                    continue;
                }
            }
        }
    }

    pub async fn start_server(&mut self, server: &ServerName) {
        let is_federation_worker_running = self
            .servers
            .get(server)
            .map(|handle| !handle.is_finished())
            .unwrap_or(false);

        if !is_federation_worker_running {
            tracing::debug!(server = server.as_str(), "Spawned federation worker",);

            if let Some(new_link) = self.background.acquire() {
                if let Some(federation_worker) = self.factory.build(new_link, server).await {
                    let handle = tokio::spawn(federation_worker.run());
                    self.servers.insert(server.to_owned(), handle);
                }
            } else {
                tracing::warn!("Trying to spawn a federation worker after shutdown started");
            }
        }
    }
}
