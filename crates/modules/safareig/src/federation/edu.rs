use safareig_core::{
    auth::{FederationAuth, Identity},
    ruma::{api::federation::transactions::edu::Edu, serde::Raw},
    Inject,
};
use safareig_ephemeral::{services::EphemeralRegistry, EduType};

use crate::client::room::RoomError;

#[derive(Inject, Clone)]
pub struct EduHandler {
    ephemeral_registry: EphemeralRegistry,
}

impl EduHandler {
    // TODO: Create an ad-hoc edu handler error, maybe?
    pub async fn handle_edus(&self, id: Identity, edus: &[Raw<Edu>]) -> Result<(), RoomError> {
        let auth = id
            .to_federation_auth()
            .ok_or_else(|| RoomError::Custom("Missing federation signatures".to_string()))?;

        let decoded_edus: Vec<Edu> = edus
            .iter()
            .map(|edu| edu.deserialize())
            .collect::<Result<Vec<Edu>, serde_json::Error>>()?;

        for edu in decoded_edus {
            let kind = match &edu {
                Edu::Typing(_) => EduType::Typing,
                Edu::Presence(_) => EduType::Presence,
                Edu::Receipt(_) => EduType::Receipt,
                Edu::DeviceListUpdate(_) => EduType::DeviceListUpdate,
                Edu::DirectToDevice(_) => EduType::DirectToDevice,
                Edu::SigningKeyUpdate(_) => EduType::SigningKeyUpdate,
                _ => EduType::Custom("".to_string()),
            };

            self.handle_edu(kind, edu, &auth).await;
        }

        Ok(())
    }

    async fn handle_edu(&self, edu_type: EduType, edu: Edu, auth: &FederationAuth) {
        let handler = self.ephemeral_registry.handler(edu_type.into());

        handler.on_federation(edu, auth).await;
    }
}
