use safareig_core::{
    auth::Identity,
    command::Command,
    ruma::api::federation::discovery::get_server_version::v1::{self as version, Server},
    Inject,
};

#[derive(Inject)]
pub struct VersionCommand;

#[async_trait::async_trait]
impl Command for VersionCommand {
    type Input = version::Request;
    type Output = version::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;

    async fn execute(&self, _: Self::Input, _: Identity) -> Result<Self::Output, Self::Error> {
        Ok(version::Response {
            server: Some(Server {
                name: Some("safareig (gitlab.com/gnieto/safareig)".to_string()),
                version: Some("unreleased".to_string()),
            }),
        })
    }
}
