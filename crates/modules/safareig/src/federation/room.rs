pub mod command;
pub(crate) mod join;

use safareig_core::{
    auth::FederationAuth,
    ruma::{
        events::{pdu::EventHash, TimelineEventType},
        MilliSecondsSinceUnixEpoch, OwnedEventId, OwnedRoomId, OwnedUserId, RoomId, ServerName,
        UInt, UserId,
    },
};
use serde::{Deserialize, Serialize};

use crate::{client::room::RoomError, core::events::EventRef};

// TODO: Check if this was finally moved to Ruma. See: https://github.com/ruma/ruma/pull/750
/// Enum for PDU template schemas
#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(untagged)]
pub enum PduTemplate {
    /// PDU for room versions 1 and 2.
    RoomV1PduTemplate(RoomV1PduTemplate),

    /// PDU for room versions 3 and above.
    RoomV3PduTemplate(RoomV3PduTemplate),
}

impl PduTemplate {
    pub fn content(&self) -> &serde_json::Value {
        match self {
            Self::RoomV1PduTemplate(template) => &template.content,
            Self::RoomV3PduTemplate(template) => &template.content,
        }
    }

    pub fn content_mut(&mut self) -> &mut serde_json::Value {
        match self {
            Self::RoomV1PduTemplate(template) => &mut template.content,
            Self::RoomV3PduTemplate(template) => &mut template.content,
        }
    }

    pub fn sender(&self) -> &UserId {
        match self {
            Self::RoomV1PduTemplate(template) => &template.sender,
            Self::RoomV3PduTemplate(template) => &template.sender,
        }
    }

    pub fn state_key(&self) -> Option<&str> {
        match self {
            Self::RoomV1PduTemplate(template) => template.state_key.as_deref(),
            Self::RoomV3PduTemplate(template) => template.state_key.as_deref(),
        }
    }

    pub fn depth(&self) -> u64 {
        match self {
            Self::RoomV1PduTemplate(template) => u64::from(template.depth),
            Self::RoomV3PduTemplate(template) => u64::from(template.depth),
        }
    }

    pub fn kind(&self) -> TimelineEventType {
        match self {
            Self::RoomV1PduTemplate(template) => template.kind.clone(),
            Self::RoomV3PduTemplate(template) => template.kind.clone(),
        }
    }

    pub fn room(&self) -> &RoomId {
        match self {
            Self::RoomV1PduTemplate(template) => &template.room_id,
            Self::RoomV3PduTemplate(template) => &template.room_id,
        }
    }

    pub fn prev_events(&self) -> Vec<EventRef> {
        match self {
            Self::RoomV1PduTemplate(template) => template
                .prev_events
                .iter()
                .cloned()
                .map(|tuple| EventRef::V1(tuple.0, tuple.1))
                .collect(),
            Self::RoomV3PduTemplate(template) => template
                .prev_events
                .iter()
                .cloned()
                .map(EventRef::V2)
                .collect(),
        }
    }

    pub fn auth_events(&self) -> Vec<EventRef> {
        match self {
            Self::RoomV1PduTemplate(template) => template
                .auth_events
                .iter()
                .cloned()
                .map(|tuple| EventRef::V1(tuple.0, tuple.1))
                .collect(),
            Self::RoomV3PduTemplate(template) => template
                .auth_events
                .iter()
                .cloned()
                .map(EventRef::V2)
                .collect(),
        }
    }

    pub fn is_valid_leave_template(
        &self,
        sender: &UserId,
        target: &UserId,
        expected_room: &RoomId,
    ) -> bool {
        self.kind() == TimelineEventType::RoomMember
            && self.sender() == sender
            && self.state_key() == Some(target.as_str())
            && self.room() == expected_room
    }
}

/// A 'persistent data unit' template for room versions 1 and 2.
#[derive(Clone, Debug, Deserialize, Serialize)]
#[allow(clippy::exhaustive_structs)]
pub struct RoomV1PduTemplate {
    /// The room this event belongs to.
    pub room_id: OwnedRoomId,

    /// The user id of the user who sent this event.
    pub sender: OwnedUserId,

    /// Timestamp (milliseconds since the UNIX epoch) on originating homeserver
    /// of when this event was created.
    pub origin_server_ts: MilliSecondsSinceUnixEpoch,

    /// The event's type.
    #[serde(rename = "type")]
    pub kind: TimelineEventType,

    /// The event's content.
    pub content: serde_json::Value,

    /// A key that determines which piece of room state the event represents.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub state_key: Option<String>,

    /// Event IDs for the most recent events in the room that the homeserver was
    /// aware of when it created this event.
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub prev_events: Vec<(OwnedEventId, EventHash)>,

    /// The maximum depth of the `prev_events`, plus one.
    pub depth: UInt,

    /// Event IDs for the authorization events that would allow this event to be
    /// in the room.
    #[serde(skip_serializing_if = "Vec::is_empty", default)]
    pub auth_events: Vec<(OwnedEventId, EventHash)>,
}

/// A 'persistent data unit' template for room versions 3 and beyond.
#[derive(Clone, Debug, Deserialize, Serialize)]
#[allow(clippy::exhaustive_structs)]
pub struct RoomV3PduTemplate {
    /// The room this event belongs to.
    pub room_id: OwnedRoomId,

    /// The user id of the user who sent this event.
    pub sender: OwnedUserId,

    /// Timestamp (milliseconds since the UNIX epoch) on originating homeserver
    /// of when this event was created.
    pub origin_server_ts: MilliSecondsSinceUnixEpoch,

    // TODO: Encode event type as content enum variant, like event enums do
    /// The event's type.
    #[serde(rename = "type")]
    pub kind: TimelineEventType,

    /// The event's content.
    pub content: serde_json::Value,

    /// A key that determines which piece of room state the event represents.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub state_key: Option<String>,

    /// Event IDs for the most recent events in the room that the homeserver was
    /// aware of when it created this event.
    pub prev_events: Vec<OwnedEventId>,

    /// The maximum depth of the `prev_events`, plus one.
    pub depth: UInt,

    /// Event IDs for the authorization events that would allow this event to be
    /// in the room.
    pub auth_events: Vec<OwnedEventId>,
}

/// Checks if given `sender`'s server is the same as the `identity` originiating the request and is not the local server.
pub fn sender_matches_identity(
    sender: &UserId,
    local_server: &ServerName,
    auth: &FederationAuth,
) -> bool {
    let request_matches_with_user_server = auth.server == sender.server_name();
    let user_is_local = sender.server_name() == local_server;

    request_matches_with_user_server && !user_is_local
}

#[cfg(test)]
mod tests {
    use super::{PduTemplate, RoomV1PduTemplate};
    use crate::federation::room::RoomV3PduTemplate;

    #[test]
    fn deserialization_pdu_template_v1() {
        let raw = r#"
        {
            "sender": "@anon-20211228_204441-1:localhost:8800",
            "auth_events": [
                [
                    "$0:localhost:40395",
                    {
                        "sha256": "4qaiiWA/QDe24M1D16n18td+aqxrkmqzryi1IQjYyXE"
                    }
                ],
                [
                    "$2:localhost:40395",
                    {
                        "sha256": "TenoBTWZFVdrEBbgK0x0h2bhy9gKeYa9VjzC+Ux7RWk"
                    }
                ]
            ],
            "prev_events": [
                [
                    "$2:localhost:40395",
                    {
                        "sha256": "TenoBTWZFVdrEBbgK0x0h2bhy9gKeYa9VjzC+Ux7RWk"
                    }
                ]
            ],
            "room_id": "!0:localhost:40395",
            "origin_server_ts": 1640724282842,
            "type": "m.room.member",
            "origin": "localhost:40395",
            "depth": 3,
            "content": {
                "membership": "join"
            },
            "state_key": "@anon-20211228_204441-1:localhost:8800"
        }
        "#;

        let _: RoomV1PduTemplate = serde_json::from_str(raw).unwrap();
        let decoded: PduTemplate = serde_json::from_str(raw).unwrap();

        assert!(matches!(decoded, PduTemplate::RoomV1PduTemplate(_)));
    }

    #[test]
    fn deserialization_pdu_template_v1_without_auth_events() {
        let raw = r#"
        {
            "room_id": "!9cepLLYyFMZSKUJtRz:localhost:8800",
            "sender": "@anon-20220710_133525-4:localhost:8801",
            "origin": "localhost:8800",
            "origin_server_ts": 1657460130995,
            "type": "m.room.member",
            "content": {
                "membership": "join"
            },
            "state_key": "@anon-20220710_133525-4:localhost:8801",
            "prev_events": [
                [
                    "$ovf6C55VBUWpSqO0zy:localhost:8800",
                    {
                        "sha256": "Jvj2nt6j+vGnvRUB6ZV+byeUcHugHB3okKWzYsQGY/A"
                    }
                ]
            ],
            "depth": 9
        }
        "#;

        let _: RoomV1PduTemplate = serde_json::from_str(raw).unwrap();
        let decoded: PduTemplate = serde_json::from_str(raw).unwrap();

        assert!(matches!(decoded, PduTemplate::RoomV1PduTemplate(_)));
    }

    #[test]
    fn deserialization_pdu_template_v3() {
        let raw = r#"
        {
            "sender": "@anon-20211228_204441-1:localhost:8800",
            "auth_events": [
                "$urlsafe_base64_encoded_eventid",
                "$a-different-event-id"
            ],
            "prev_events": [
                "$urlsafe_base64_encoded_eventid",
                "$a-different-event-id"
            ],
            "room_id": "!0:localhost:40395",
            "origin_server_ts": 1640724282842,
            "type": "m.room.member",
            "origin": "localhost:40395",
            "depth": 3,
            "content": {
                "membership": "join"
            },
            "state_key": "@anon-20211228_204441-1:localhost:8800"
        }
        "#;

        let _: RoomV3PduTemplate = serde_json::from_str(raw).unwrap();
        let decoded: PduTemplate = serde_json::from_str(raw).unwrap();

        assert!(matches!(decoded, PduTemplate::RoomV3PduTemplate(_)));
    }
}
