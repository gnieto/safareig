use safareig_core::command::{Container, Module, Router, ServiceCollectionExtension};

use super::{
    edu::EduHandler,
    room::command::{
        backfill::BackfillCommand,
        get_event::GetEventCommand,
        invite::InviteRoomCommand,
        make_join::MakeJoinCommand,
        make_leave::MakeLeaveCommand,
        missing_events::MissingEventCommand,
        send::SendCommand,
        send_join::{SendJoinCommand, SendJoinV1Command},
        state::StateCommand,
        state_ids::StateIdsCommand,
        SendLeaveCommand,
    },
    version::VersionCommand,
};

#[derive(Default)]
pub struct FederationComponent;

#[async_trait::async_trait]
impl<R: Router> Module<R> for FederationComponent {
    fn register(&self, container: &mut Container, router: &mut R) {
        // Room
        container.inject_endpoint::<_, BackfillCommand, _>(router);
        container.inject_endpoint::<_, GetEventCommand, _>(router);
        container.inject_endpoint::<_, MakeJoinCommand, _>(router);
        container.inject_endpoint::<_, MakeLeaveCommand, _>(router);
        container.inject_endpoint::<_, MissingEventCommand, _>(router);
        container.inject_endpoint::<_, SendJoinCommand, _>(router);
        container.inject_endpoint::<_, SendJoinV1Command, _>(router);
        container.inject_endpoint::<_, SendCommand, _>(router);
        container.inject_endpoint::<_, StateIdsCommand, _>(router);
        container.inject_endpoint::<_, StateCommand, _>(router);
        container.inject_endpoint::<_, InviteRoomCommand, _>(router);
        container.inject_endpoint::<_, SendLeaveCommand, _>(router);

        container.service_collection.register::<EduHandler>();

        // Discovery
        container.inject_endpoint::<_, VersionCommand, _>(router);
    }
}
