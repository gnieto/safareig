pub mod events;
pub mod filter;
pub mod room;
pub mod waker;

use safareig_core::ruma::UInt;

pub fn limit(maybe_limit: UInt, default: usize) -> usize {
    u32::try_from(maybe_limit)
        .and_then(usize::try_from)
        .unwrap_or(default)
}
