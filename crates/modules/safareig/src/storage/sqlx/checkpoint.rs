use async_trait::async_trait;
use serde::{de::DeserializeOwned, Serialize};
use sqlx::{sqlite::SqliteRow, Row};

use crate::storage::{sqlx::SqlxStorage, CheckpointStorage, Result};

#[async_trait]
impl<C: Serialize + DeserializeOwned + Clone + Send + Sync> CheckpointStorage<C> for SqlxStorage {
    async fn get_checkpoint(&self, worker_id: &str) -> Result<Option<C>> {
        let checkpoint = sqlx::query("SELECT * FROM checkpoints WHERE worker_id = ?")
            .bind(worker_id)
            .fetch_optional(&self.pool)
            .await?
            .map(row_to_checkpoint)
            .transpose()?;

        Ok(checkpoint)
    }

    async fn update_checkpoint(&self, worker_id: &str, checkpoint: &C) -> Result<()> {
        let json_data = serde_json::to_string(checkpoint)?;

        sqlx::query(
            r#"
            INSERT INTO checkpoints (worker_id, checkpoint_data)
            VALUES (?1, ?2)
            ON CONFLICT DO UPDATE SET checkpoint_data = ?2
            "#,
        )
        .bind(worker_id)
        .bind(json_data)
        .execute(&self.pool)
        .await?;

        Ok(())
    }
}

fn row_to_checkpoint<C: DeserializeOwned>(r: SqliteRow) -> Result<C> {
    let data = r.get::<&str, _>("checkpoint_data");
    let checkpoint = serde_json::from_str::<C>(data)?;

    Ok(checkpoint)
}
