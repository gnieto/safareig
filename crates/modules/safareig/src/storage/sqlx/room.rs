use std::{collections::HashMap, convert::TryFrom, str::FromStr};

use async_trait::async_trait;
use rusty_ulid::Ulid;
use safareig_core::{
    pagination::{Pagination, PaginationResponse, Range},
    ruma::{
        events::{pdu::EventHash, room::member::MembershipState, StateEventType},
        EventId, OwnedEventId, OwnedRoomId, RoomId, RoomVersionId, TransactionId, UserId,
    },
    storage::StorageError,
};
use safareig_sqlx::{
    decode_from_str, decode_i64_to_u64, decode_stream_token, decode_ulid, u128_to_vec, ulid_to_vec,
    DirectionExt, PaginatedIterator, RangeWrapper,
};
use sea_query::{Alias, Expr, Iden, Query, SqliteQueryBuilder};
use sea_query_binder::SqlxBinder;
use serde_json::{Number, Value};
use sqlx::{sqlite::SqliteRow, Pool, Row, Sqlite};
use tracing::instrument;

use super::{RoomStorage, SqlxStorage, StateStorage};
use crate::{
    client::room::state::RoomStateId,
    core::{
        events::{Event, EventRef},
        room::RoomDescriptor,
    },
    storage::{
        BackwardExtremity, CompressedStateIndex, EventOptions, EventStorage, Leaf, Membership,
        Result, RoomStreamStorage, RoomTopologyStorage, StateIndex, StreamFilter, StreamSource,
        StreamToken, TopologicalToken,
    },
};

#[async_trait]
impl RoomStorage for SqlxStorage {
    async fn update_membership(
        &self,
        user_id: &safareig_core::ruma::UserId,
        room_id: &safareig_core::ruma::RoomId,
        membership: crate::storage::Membership,
    ) -> crate::storage::Result<()> {
        let state = membership.state.to_string();
        let token = *membership.stream as i64;
        let event_id = membership.event_id.to_string();

        sqlx::query(
            r#"
            INSERT INTO user_memberships (user_id, room_id, state, stream_token, event_id)
            VALUES (?1, ?2, ?3, ?4, ?5)
            ON CONFLICT(user_id, room_id) DO UPDATE SET state = ?3, stream_token = ?4, event_id = ?5
            WHERE ?3 <> state
            "#,
        )
        .bind(user_id.as_str())
        .bind(room_id.as_str())
        .bind(state)
        .bind(token)
        .bind(event_id)
        .execute(&self.pool)
        .await?;

        Ok(())
    }

    async fn membership(
        &self,
        user_id: &safareig_core::ruma::UserId,
    ) -> crate::storage::Result<std::collections::HashMap<OwnedRoomId, crate::storage::Membership>>
    {
        let user_id = user_id.to_string();

        let memberhips_rows = sqlx::query(
            r#"
            SELECT room_id, state, stream_token, event_id FROM user_memberships
            WHERE user_id = ?1
            "#,
        )
        .bind(user_id)
        .fetch_all(&self.pool)
        .await?;

        let mut memberhips = HashMap::new();

        for row in memberhips_rows {
            let room_id = decode_from_str(&row.get::<String, _>("room_id"))?;
            let token = decode_stream_token(row.get::<i64, _>("stream_token"))?;
            let state = MembershipState::from(row.get::<&str, _>("state"));
            let event_id = decode_from_str(&row.get::<String, _>("event_id"))?;

            memberhips.insert(
                room_id,
                Membership {
                    state,
                    event_id,
                    stream: token,
                },
            );
        }

        Ok(memberhips)
    }

    async fn remove_membership(&self, user_id: &UserId, room_id: &RoomId) -> Result<()> {
        sqlx::query(
            r#"
            DELETE FROM user_memberships 
            WHERE user_id = ?1 AND room_id = ?2
            "#,
        )
        .bind(user_id.as_str())
        .bind(room_id.as_str())
        .execute(&self.pool)
        .await?;

        Ok(())
    }

    /// Returns a set of events in topological order. Range inclusiveness is controlled by the
    /// boundaries values.
    async fn room_events(
        &self,
        room: &RoomId,
        query: Pagination<TopologicalToken>,
    ) -> Result<PaginationResponse<TopologicalToken, Event>> {
        let mut sql = "
            SELECT * FROM events as events
            WHERE events.room_id = ?1 {where}
            ORDER BY events.depth {order}
            LIMIT ?2
        "
        .to_string();

        let mut sql_clauses = vec![];
        let range = query.range();

        match range.from() {
            safareig_core::pagination::Boundary::Exclusive(token) => {
                sql_clauses.push(format!("events.depth > {}", token.depth()))
            }
            safareig_core::pagination::Boundary::Inclusive(token) => {
                sql_clauses.push(format!("events.depth >= {}", token.depth()))
            }
            safareig_core::pagination::Boundary::Unlimited => (),
        }

        match range.to() {
            safareig_core::pagination::Boundary::Exclusive(token) => {
                sql_clauses.push(format!("events.depth < {}", token.depth()))
            }
            safareig_core::pagination::Boundary::Inclusive(token) => {
                sql_clauses.push(format!("events.depth <= {}", token.depth()))
            }
            safareig_core::pagination::Boundary::Unlimited => (),
        }

        let wh = if sql_clauses.is_empty() {
            "".to_string()
        } else {
            format!(" AND {}", sql_clauses.join(" AND "))
        };

        let ascendence = match query.direction() {
            safareig_core::pagination::Direction::Backward => "DESC",
            safareig_core::pagination::Direction::Forward => "ASC",
        };

        sql = sql.replace("{where}", &wh);
        sql = sql.replace("{order}", ascendence);

        let events = sqlx::query(&sql)
            .bind(room.as_str())
            .bind(query.limit().unwrap_or(20))
            .fetch_all(&self.pool)
            .await?;

        let iterator = PaginatedIterator::new(events.into_iter(), |row| {
            let event = Event::try_from(&row)?;

            Ok((event.topological_token(), event))
        });

        iterator.into_pagination_response()
    }

    async fn room_descriptor(&self, room_id: &RoomId) -> Result<Option<RoomDescriptor>> {
        sqlx::query(
            r#"
            SELECT room_id, event_encoding, room_version FROM rooms
            WHERE room_id = ?1
        "#,
        )
        .bind(room_id.as_str())
        .fetch_optional(&self.pool)
        .await?
        .map(TryFrom::try_from)
        .transpose()
    }

    async fn update_room_descriptor(&self, descriptor: &RoomDescriptor) -> Result<()> {
        let encoding = serde_json::to_string(&descriptor.encoding)?;

        sqlx::query(
            r#"
            INSERT INTO rooms (room_id, event_encoding, room_version)
            VALUES (?1, ?2, ?3)
            ON CONFLICT(room_id) DO UPDATE SET event_encoding = ?2, room_version = ?3
        "#,
        )
        .bind(descriptor.room_id.as_str())
        .bind(encoding)
        .bind(descriptor.room_version.as_str())
        .execute(&self.pool)
        .await?;

        Ok(())
    }

    async fn find_all_rooms(&self) -> Result<Vec<RoomDescriptor>> {
        sqlx::query(
            r#"
            SELECT room_id, event_encoding, room_version FROM rooms
        "#,
        )
        .fetch_all(&self.pool)
        .await?
        .into_iter()
        .map(TryFrom::try_from)
        .collect::<Result<Vec<_>>>()
    }
}

impl TryFrom<SqliteRow> for RoomDescriptor {
    type Error = StorageError;

    fn try_from(row: SqliteRow) -> Result<Self> {
        let room_id = row.get::<String, _>("room_id").parse().unwrap();
        let encoding = row.get::<String, _>("event_encoding");
        let encoding = serde_json::from_str(&encoding)?;

        let room_version = row
            .try_get::<i64, _>("room_version")
            .map(|v| RoomVersionId::from_str(&v.to_string()))
            .unwrap_or_else(|_| RoomVersionId::from_str(row.get::<&str, _>("room_version")))
            .unwrap();

        Ok(RoomDescriptor {
            room_id,
            encoding,
            room_version,
        })
    }
}

#[async_trait]
impl RoomTopologyStorage for SqlxStorage {
    async fn forward_extremities(&self, room: &RoomId) -> Result<Vec<Leaf>> {
        let room_ids = sqlx::query(
            r#"
            SELECT fe.*, events.room_id FROM forward_extremities fe
                INNER JOIN events ON (events.event_id = fe.event_id)
            WHERE events.room_id = ?1;
            "#,
        )
        .bind(room.as_str())
        .fetch_all(&self.pool)
        .await?;

        room_ids
            .into_iter()
            .map(row_to_leaf)
            .collect::<Result<Vec<Leaf>>>()
    }

    async fn adjust_extremities(
        &self,
        _: &RoomId,
        event: &Event,
        state_id: RoomStateId,
    ) -> Result<()> {
        let final_state_id = ulid_to_vec(state_id);
        let event_id = event.event_ref.event_id();
        let sha256 = event
            .event_ref
            .event_hash()
            .map(|hash| hash.sha256.as_str());
        let depth = i64::try_from(event.depth).map_err(|_| StorageError::CorruptedData)?;

        let prev_events: Vec<&str> = event
            .prev_events
            .iter()
            .map(|e| e.event_id())
            .map(|event_id| event_id.as_ref())
            .collect();

        let delete_sql = format!(
            r#"
        DELETE FROM forward_extremities
        WHERE event_id IN ({});
        "#,
            &format!("?{}", ", ?".repeat(prev_events.len().saturating_sub(1)))
        );
        let mut query = sqlx::query(&delete_sql);

        for p in prev_events {
            query = query.bind(p);
        }

        let mut tx = self.pool.begin().await?;
        query.execute(&mut *tx).await?;

        sqlx::query(
            r#"
            INSERT INTO forward_extremities (event_id, sha256, depth, state)
            VALUES (?1, ?2, ?3, ?4)
            "#,
        )
        .bind(event_id.as_str())
        .bind(sha256)
        .bind(depth)
        .bind(final_state_id)
        .execute(&mut *tx)
        .await?;

        tx.commit().await?;

        Ok(())
    }

    async fn add_backward_extremities(
        &self,
        room: &RoomId,
        extremity: &[BackwardExtremity],
    ) -> Result<()> {
        if extremity.is_empty() {
            return Ok(());
        }

        let mut builder = sqlx::QueryBuilder::new(
            "INSERT INTO backward_extremities (event_id, room_id, topological_depth, topological_sub)",
        );

        builder.push_values(extremity.iter(), |mut b, extremity: &BackwardExtremity| {
            b.push_bind(extremity.event_id.as_str())
                .push_bind(room.as_str())
                .push_bind(i64::try_from(extremity.topological_token.0).unwrap())
                .push_bind(i64::try_from(extremity.topological_token.1).unwrap());
        });

        builder.build().execute(&self.pool).await?;

        Ok(())
    }

    async fn remove_backward_extremity(
        &self,
        room: &RoomId,
        extremity: &BackwardExtremity,
    ) -> Result<()> {
        let sql = "DELETE FROM backward_extremities
        WHERE event_id = ?1 AND room_id = ?2";

        sqlx::query(sql)
            .bind(extremity.event_id.as_str())
            .bind(room.as_str())
            .execute(&self.pool)
            .await?;

        Ok(())
    }

    async fn backward_extremities(
        &self,
        room: &RoomId,
        range: Range<TopologicalToken>,
    ) -> Result<Vec<BackwardExtremity>> {
        let mut sql = "SELECT be.* FROM backward_extremities be
        WHERE be.room_id = ?1 {where}"
            .to_string();

        let mut sql_clauses = vec![];

        match range.from() {
            safareig_core::pagination::Boundary::Exclusive(token) => {
                sql_clauses.push(format!("be.topological_depth > {}", token.depth()))
            }
            safareig_core::pagination::Boundary::Inclusive(token) => {
                sql_clauses.push(format!("be.topological_depth >= {}", token.depth()))
            }
            safareig_core::pagination::Boundary::Unlimited => (),
        }

        match range.to() {
            safareig_core::pagination::Boundary::Exclusive(token) => {
                sql_clauses.push(format!("be.topological_depth < {}", token.depth()))
            }
            safareig_core::pagination::Boundary::Inclusive(token) => {
                sql_clauses.push(format!("be.topological_depth <= {}", token.depth()))
            }
            safareig_core::pagination::Boundary::Unlimited => (),
        }

        let wh = if sql_clauses.is_empty() {
            "".to_string()
        } else {
            format!(" AND {}", sql_clauses.join(" AND "))
        };

        sql = sql.replace("{where}", &wh);

        let extremities = sqlx::query(&sql)
            .bind(room.as_str())
            .fetch_all(&self.pool)
            .await?
            .into_iter()
            .map(|row| {
                let event_id = decode_from_str(row.try_get::<&str, _>("event_id")?)?;
                let depth = decode_i64_to_u64(row.try_get::<i64, _>("topological_depth")?)?;
                let sub = decode_i64_to_u64(row.try_get::<i64, _>("topological_sub")?)?;

                let topological = TopologicalToken::new_subsequence(depth, sub);

                Ok(BackwardExtremity {
                    event_id,
                    topological_token: topological,
                })
            })
            .collect::<Result<Vec<_>>>()?;

        Ok(extremities)
    }
}

#[async_trait]
impl EventStorage for SqlxStorage {
    async fn put_event(
        &self,
        event: &crate::core::events::Event,
        options: EventOptions,
    ) -> crate::storage::Result<()> {
        // TODO: Copy paste above
        let event_id = event.event_ref.event_id().to_string();
        let sha256 = event.event_ref.event_hash().map(|hash| hash.sha256.clone());
        let event_type = event.kind.to_string();
        let depth = i64::try_from(event.depth).map_err(|_| StorageError::CorruptedData)?;
        let room_id = event.room_id.to_string();
        let sender = event.sender.to_string();

        let mut partial = serde_json::to_value(event)?;
        let partial = partial.as_object_mut().unwrap();
        partial.remove("event_id");
        partial.remove("type");
        partial.remove("depth");
        partial.remove("room_id");
        partial.remove("sender");

        let event_data = serde_json::to_string(partial)?;
        let transaction_id = options.transaction_id().as_ref().map(|txn| txn.as_str());
        let maybe_state_id = options
            .previous_room_state_id()
            .map(|state_id| ulid_to_vec(*state_id));

        let mut tx = self.pool.begin().await?;

        let mut flags = 0u32;

        if options.is_rejected() {
            flags |= 1 << 1;
        }

        sqlx::query(
            r#"
            INSERT INTO events (event_id, event_type, depth, room_id, sender, event_data, transaction_id, sha256, flags)
            VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9)
            -- if transaction_id unique restriction fails, set event_id to null, which will violate the primary key restriction to provoke a failure
            ON CONFLICT (room_id, transaction_id) DO UPDATE SET event_id = NULL
            ON CONFLICT (event_id) DO UPDATE SET event_data = ?6
            "#
        )
        .bind(event_id.as_str())
        .bind(event_type)
        .bind(depth)
        .bind(room_id)
        .bind(sender)
        .bind(event_data)
        .bind(transaction_id)
        .bind(sha256)
        .bind(flags)
        .execute(&mut *tx)
        .await?;

        if let Some(state_id) = maybe_state_id {
            sqlx::query(
                "
                INSERT OR IGNORE INTO event_state (event_id, state_id)
                VALUES (?1, ?2)",
            )
            .bind(event_id.as_str())
            .bind(&state_id)
            .execute(&mut *tx)
            .await?;
        }

        tx.commit().await?;

        Ok(())
    }

    async fn get_event(
        &self,
        event_id: &safareig_core::ruma::EventId,
    ) -> crate::storage::Result<Option<crate::core::events::Event>> {
        let row = sqlx::query("SELECT * FROM events WHERE event_id = ?")
            .bind(event_id.as_str())
            .fetch_optional(&self.pool)
            .await?;

        row.map(|row| Event::try_from(&row)).transpose()
    }

    async fn get_events(
        &self,
        events: &[safareig_core::ruma::OwnedEventId],
    ) -> crate::storage::Result<Vec<crate::core::events::Event>> {
        let (query, values) = Query::select()
            .expr(Expr::asterisk())
            .from(Events::Table)
            .and_where(Expr::col(Events::EventId).is_in(events.iter().map(|e| e.as_str())))
            .build_sqlx(SqliteQueryBuilder);

        let state_events = sqlx::query_with(&query, values)
            .fetch_all(&self.pool)
            .await?;

        let mut events_by_id = state_events
            .iter()
            .map(|row| Event::try_from(row).map(|e| (e.event_ref.owned_event_id(), e)))
            .collect::<Result<HashMap<OwnedEventId, Event>>>()?;

        let sorted_events = events
            .iter()
            .filter_map(|id| events_by_id.remove(id))
            .collect();

        Ok(sorted_events)
    }

    async fn get_event_by_transaction_id(
        &self,
        room_id: &RoomId,
        txn_id: &TransactionId,
    ) -> Result<Option<Event>> {
        let row = sqlx::query("SELECT * FROM events WHERE transaction_id = ?1 AND room_id = ?2")
            .bind(txn_id.as_str())
            .bind(room_id.as_str())
            .fetch_optional(&self.pool)
            .await?;

        row.map(|row| Event::try_from(&row)).transpose()
    }
}

#[derive(Iden)]
pub enum Events {
    Table,
    EventId,
    RoomId,
    EventType,
}

#[derive(Iden)]
pub enum EventStream {
    Table,
    StreamToken,
    EventId,
}

#[async_trait]
impl RoomStreamStorage for SqlxStorage {
    async fn insert_event_to_stream(
        &self,
        event: &crate::core::events::Event,
    ) -> crate::storage::Result<crate::storage::StreamToken> {
        let event_id = event.event_ref.event_id();

        let row = sqlx::query(
            r#"
            INSERT INTO event_stream (event_id)
            VALUES(?1)
            RETURNING stream_token
            "#,
        )
        .bind(event_id.as_str())
        .fetch_optional(&self.pool)
        .await?
        .map(|row| decode_i64_to_u64(row.get::<i64, _>("stream_token")))
        .transpose()?
        .map(StreamToken::from)
        .ok_or_else(|| StorageError::Custom("could not insert event to stream".to_string()))?;

        Ok(row)
    }

    async fn stream_token_at(
        &self,
        _room_id: &safareig_core::ruma::RoomId,
        event: &safareig_core::ruma::EventId,
    ) -> crate::storage::Result<Option<crate::storage::StreamToken>> {
        let row = sqlx::query("SELECT stream_token FROM event_stream WHERE event_id = ?")
            .bind(event.as_str())
            .fetch_optional(&self.pool)
            .await?;

        match row {
            Some(row) => {
                let token = decode_stream_token(row.get::<i64, _>("stream_token"))?;
                Ok(Some(token))
            }
            None => Ok(None),
        }
    }

    async fn current_stream_token(&self) -> crate::storage::Result<crate::storage::StreamToken> {
        let row = sqlx::query("SELECT MAX(stream_token) as stream_token FROM event_stream")
            .fetch_optional(&self.pool)
            .await?;

        match row {
            Some(row) => {
                let token = row.get::<i64, _>("stream_token");
                let unsigned = u64::try_from(token).map_err(|_| StorageError::CorruptedData)?;

                Ok(crate::storage::StreamToken::from(unsigned))
            }
            None => Ok(crate::storage::StreamToken::horizon()),
        }
    }

    #[instrument(skip_all)]
    async fn stream_events<'a>(
        &'a self,
        query: Pagination<StreamToken, Option<StreamFilter<'a>>>,
    ) -> Result<PaginationResponse<StreamToken, (StreamToken, OwnedEventId)>> {
        let wrapper = RangeWrapper(query.range().map(|t| t.to_string()));

        let mut select = Query::select();
        select
            .column((EventStream::Table, EventStream::StreamToken))
            .column((Events::Table, Events::EventId))
            .from(EventStream::Table)
            .inner_join(
                Events::Table,
                Expr::col((Events::Table, Events::EventId))
                    .equals((EventStream::Table, EventStream::EventId)),
            )
            .cond_where(wrapper.condition(Expr::col(Alias::new("stream_token"))))
            .order_by(EventStream::StreamToken, query.direction().order())
            .limit(query.limit().unwrap_or(100).into());

        if let Some(filter) = query.context() {
            if let Some(room) = filter.room_id {
                select.and_where(Expr::col(Events::RoomId).eq(room.as_str()));
            }

            if let Some(definition) = filter.definition() {
                match filter.source {
                    StreamSource::Timeline => {
                        if let Some(allowed) = &definition.room.timeline.types {
                            select.and_where(Expr::col(Events::EventType).is_in(allowed));
                        }
                    }
                    StreamSource::State => {
                        if let Some(allowed) = &definition.room.state.types {
                            select.and_where(Expr::col(Events::EventType).is_in(allowed));
                        }
                    }
                    _ => {}
                };
            }
        }

        let (sql, values) = select.build_sqlx(SqliteQueryBuilder);
        let events = sqlx::query_with(&sql, values).fetch_all(&self.pool).await?;

        let iterator = PaginatedIterator::new(events.into_iter(), |row| {
            let token = decode_i64_to_u64(row.try_get::<i64, _>("stream_token")?)?;
            let event_id = decode_from_str(row.get::<&str, _>("event_id"))?;
            let stream_token = StreamToken::from(token);

            Ok((stream_token, (stream_token, event_id)))
        });

        iterator.into_pagination_response()
    }
}

#[async_trait]
impl StateStorage for SqlxStorage {
    async fn event_for_key(
        &self,
        state: &crate::client::room::state::RoomStateId,
        index: &crate::client::room::state::StateIndex,
    ) -> super::Result<Option<safareig_core::ruma::OwnedEventId>> {
        let state_id: [u8; 16] = (*state).into();
        let state_id = state_id.to_vec();
        let csi = CompressedStateIndex::from_state_index(index, self).await;
        let event_type = u128_to_vec(csi.kind);
        let state_key = csi.state_key.map(u128_to_vec).unwrap_or_default();

        let state_events = if state_key.is_empty() {
            sqlx::query(
                r#"
                SELECT s.event_id FROM states s
                WHERE s.state_id = ? AND s.event_type = ?
                "#,
            )
            .bind(state_id)
            .bind(event_type)
            .fetch_optional(&self.pool)
            .await?
        } else {
            sqlx::query(
                r#"
                SELECT s.event_id FROM states s
                WHERE s.state_id = ? AND s.event_type = ? AND s.state_key = ? 
                "#,
            )
            .bind(state_id)
            .bind(event_type)
            .bind(state_key)
            .fetch_optional(&self.pool)
            .await?
        };

        state_events
            .map(|row| {
                decode_from_str::<safareig_core::ruma::OwnedEventId>(
                    &row.get::<String, _>("event_id"),
                )
            })
            .transpose()
    }

    #[tracing::instrument(skip(self, state, indexes))]
    async fn event_for_keys(
        &self,
        state: &crate::client::room::state::RoomStateId,
        indexes: &[crate::client::room::state::StateIndex],
    ) -> super::Result<Vec<safareig_core::ruma::OwnedEventId>> {
        if indexes.is_empty() {
            return Ok(Vec::new());
        }

        let state_id: [u8; 16] = (*state).into();
        let state_id = state_id.to_vec();

        let mut base_query = "
        SELECT s.event_id FROM states s
            INNER JOIN internalized_strings ie ON (ie.int_id = s.event_type)
            LEFT JOIN internalized_strings ie_kind ON (ie_kind.int_id = s.state_key)
        WHERE s.state_id = ? 
        "
        .to_string();

        let mut clauses = vec![];
        for state_index in indexes {
            match &state_index.state_key {
                Some(key) => {
                    clauses.push(format!(
                        "(ie.int_string = \"{}\" AND ie_kind.int_string = \"{}\")",
                        state_index.kind, key
                    ));
                }
                None => {
                    clauses.push(format!("(ie.int_string = \"{}\")", state_index.kind));
                }
            }
        }

        base_query.push_str(&format!(" AND ({})", clauses.join(" OR ")));

        let results = sqlx::query(&base_query)
            .bind(state_id)
            .fetch_all(&self.pool)
            .await?;

        let event_ids = results
            .into_iter()
            .map(|row| {
                decode_from_str::<safareig_core::ruma::OwnedEventId>(
                    &row.get::<String, _>("event_id"),
                )
            })
            .collect::<Result<Vec<OwnedEventId>>>()?;

        Ok(event_ids)
    }

    async fn state_keys_for(
        &self,
        state_id: &crate::client::room::state::RoomStateId,
        filter: Option<safareig_core::ruma::events::TimelineEventType>,
    ) -> super::Result<Vec<crate::client::room::state::StateIndex>> {
        let state_id: [u8; 16] = (*state_id).into();
        let state_id = state_id.to_vec();
        let event_type = filter.unwrap();
        let internalized = self.internalize(event_type.to_string().as_str()).await?;
        let raw_event_type = ulid_to_vec(internalized);

        let rows = sqlx::query(
            r#"
            SELECT state_key FROM states
            WHERE state_id = ? AND event_type = ?
            "#,
        )
        .bind(state_id)
        .bind(raw_event_type)
        .fetch_all(&self.pool)
        .await?;

        let mut state_indexes = vec![];
        for row in rows {
            let maybe_key = row.get::<Vec<u8>, _>("state_key");

            if maybe_key.is_empty() {
                state_indexes.push(StateIndex::new(event_type.clone()))
            } else {
                let internalized = decode_ulid(&maybe_key)?;
                let original = self.revert(internalized).await?;

                state_indexes.push(StateIndex::with_state_key(event_type.clone(), original));
            }
        }

        Ok(state_indexes)
    }

    async fn states(
        &self,
        state_id: &crate::client::room::state::RoomStateId,
        excluded_types: Option<Vec<StateEventType>>,
    ) -> super::Result<Vec<crate::core::events::Event>> {
        let state_id: [u8; 16] = (*state_id).into();
        let state_id = state_id.to_vec();

        let mut excluded_internalized = vec![];
        if let Some(excluded) = excluded_types {
            for ty in excluded {
                let internalized = self.internalize(ty.to_string().as_str()).await?;
                excluded_internalized.push(internalized);
            }
        }
        // &format!("?{}", ", ?".repeat(events.len().saturating_sub(1)))
        let in_clause: Vec<&str> = excluded_internalized.iter().map(|_| "?").collect();
        let in_clause = in_clause.join(",");

        let query_str = format!(
            r#"
        SELECT e.* FROM states s
            INNER JOIN events e ON (s.event_id = e.event_id) 
        WHERE s.state_id = ? and s.event_type NOT IN({in_clause})
        "#
        );

        let mut query = sqlx::query(&query_str).bind(state_id);

        for id in excluded_internalized {
            query = query.bind(ulid_to_vec(id));
        }

        let state_events = query.fetch_all(&self.pool).await?;

        state_events
            .into_iter()
            .map(|row| Event::try_from(&row))
            .collect::<Result<Vec<Event>>>()
    }

    async fn state_at(
        &self,
        _: &safareig_core::ruma::RoomId,
        event_id: &safareig_core::ruma::EventId,
    ) -> super::Result<Option<crate::client::room::state::RoomStateId>> {
        let event_id = event_id.to_string();
        let maybe_row = sqlx::query(
            r#"
            SELECT es.state_id FROM event_state es 
            WHERE es.event_id = ?
            "#,
        )
        .bind(event_id)
        .fetch_optional(&self.pool)
        .await?;

        match maybe_row {
            Some(row) => {
                let state_at = decode_state_id(&row.get::<Vec<u8>, _>("state_id"))?;
                Ok(Some(state_at))
            }
            None => Ok(None),
        }
    }

    async fn fork_state(
        &self,
        base: &crate::client::room::state::RoomStateId,
        index: &crate::client::room::state::StateIndex,
        event_id: &safareig_core::ruma::EventId,
    ) -> super::Result<crate::client::room::state::RoomStateId> {
        let room_state_id = RoomStateId::generate();
        let state_id = ulid_to_vec(room_state_id);
        let base_id = ulid_to_vec(*base);

        let event_id = event_id.to_string();
        let csi = CompressedStateIndex::from_state_index(index, self).await;
        let event_type = u128_to_vec(csi.kind);
        let state_key = csi.state_key.map(u128_to_vec).unwrap_or_default();

        let mut tx = self.pool.begin().await?;
        sqlx::query(
            r#"
            INSERT INTO states (state_id, event_id, event_type, state_key)
            SELECT ?, s2.event_id, s2.event_type, s2.state_key FROM states s2
            WHERE s2.state_id = ?
            "#,
        )
        .bind(&state_id)
        .bind(base_id)
        .execute(&mut *tx)
        .await
        .unwrap();

        sqlx::query(
            r#"
            INSERT INTO states (state_id, event_type, state_key, event_id)
            VALUES (?1, ?2, ?3, ?4)
            ON CONFLICT DO UPDATE SET event_id = ?4
            "#,
        )
        .bind(&state_id)
        .bind(event_type)
        .bind(state_key)
        .bind(event_id)
        .execute(&mut *tx)
        .await
        .unwrap();

        tx.commit().await?;

        Ok(room_state_id)
    }

    async fn store_state(
        &self,
        state: &std::collections::HashMap<OwnedEventId, crate::client::room::state::StateIndex>,
    ) -> super::Result<crate::client::room::state::RoomStateId> {
        let mut compressed_state = HashMap::new();
        for (event_id, index) in state {
            let csi = CompressedStateIndex::from_state_index(index, self).await;
            compressed_state.insert(event_id, csi);
        }

        let mut tx = self.pool.begin().await?;
        let room_state_id = RoomStateId::generate();
        let state_id = ulid_to_vec(room_state_id);

        for (event_id, csi) in compressed_state {
            let event_id = event_id.to_string();
            let event_type = u128_to_vec(csi.kind);
            let state_key = csi.state_key.map(u128_to_vec).unwrap_or_default();

            sqlx::query(
                r#"
                INSERT INTO states (state_id, event_type, state_key, event_id)
                VALUES (?1, ?2, ?3, ?4)
                "#,
            )
            .bind(&state_id)
            .bind(event_type)
            .bind(state_key)
            .bind(event_id)
            .execute(&mut *tx)
            .await?;
        }

        tx.commit().await?;

        Ok(room_state_id)
    }

    async fn internalize(&self, input: &str) -> super::Result<rusty_ulid::Ulid> {
        let input = input.to_string();

        let internalized_string = get_internalized_string(&input, &self.pool).await?;
        if let Some(id) = internalized_string {
            return Ok(id);
        }

        let next = rusty_ulid::generate_ulid_bytes().to_vec();
        let maybe_row = sqlx::query(
            r#"
            INSERT OR IGNORE INTO internalized_strings (int_string, int_id)
            VALUES (?1, ?2)
            RETURNING int_id
            "#,
        )
        .bind(&input)
        .bind(next)
        .fetch_optional(&self.pool)
        .await?;

        let maybe_ulid = maybe_row
            .map(|row| {
                let bytes = row.get::<Vec<u8>, _>("int_id");

                rusty_ulid::Ulid::try_from(bytes.as_ref()).map_err(|_| StorageError::CorruptedData)
            })
            .transpose()?;

        match maybe_ulid {
            Some(ulid) => Ok(ulid),
            None => get_internalized_string(&input, &self.pool)
                .await?
                .ok_or(StorageError::Io),
        }
    }

    async fn revert(&self, input: rusty_ulid::Ulid) -> super::Result<String> {
        let raw_id: [u8; 16] = input.into();
        let raw_id = raw_id.to_vec();

        let filter = sqlx::query(
            r#"
            SELECT int_string FROM internalized_strings
            WHERE int_id = ?
            "#,
        )
        .bind(raw_id)
        .fetch_optional(&self.pool)
        .await?;

        match filter {
            Some(row) => {
                let int_string = row.get::<String, _>("int_string");

                Ok(int_string)
            }
            None => Err(StorageError::CorruptedData),
        }
    }

    async fn assign_state(
        &self,
        _room_id: &RoomId,
        event_id: &EventId,
        state_id: &RoomStateId,
    ) -> Result<()> {
        let sql = "INSERT OR IGNORE INTO event_state (event_id, state_id)
            VALUES (?1, ?2)
        ";

        let state_id = ulid_to_vec(*state_id);

        sqlx::query(sql)
            .bind(event_id.as_str())
            .bind(state_id)
            .execute(&self.pool)
            .await?;

        Ok(())
    }
}

async fn get_internalized_string(input: &str, pool: &Pool<Sqlite>) -> Result<Option<Ulid>> {
    let maybe_row = sqlx::query(
        r#"
        SELECT int_id FROM internalized_strings
        WHERE int_string = ?1
        "#,
    )
    .bind(input)
    .fetch_optional(pool)
    .await?;

    maybe_row
        .map(|row| {
            let bytes = row.get::<Vec<u8>, _>("int_id");

            return rusty_ulid::Ulid::try_from(bytes.as_ref())
                .map_err(|_| StorageError::CorruptedData);
        })
        .transpose()
}

impl TryFrom<&SqliteRow> for Event {
    type Error = StorageError;

    fn try_from(row: &SqliteRow) -> std::result::Result<Self, Self::Error> {
        let partial = row.get::<String, _>("event_data");
        let event_type = row.get::<String, _>("event_type");
        let depth = row.get::<i64, _>("depth");
        let room_id = row.get::<String, _>("room_id");
        let sender = row.get::<String, _>("sender");
        let event_ref = EventRef::try_from(row)?;

        let mut serialized_event: serde_json::Map<String, Value> = serde_json::from_str(&partial)?;
        serialized_event.insert("event_id".to_string(), serde_json::to_value(event_ref)?);
        serialized_event.insert("type".to_string(), Value::String(event_type));
        serialized_event.insert("depth".to_string(), Value::Number(Number::from(depth)));
        serialized_event.insert("room_id".to_string(), Value::String(room_id));
        serialized_event.insert("sender".to_string(), Value::String(sender));

        let event = serde_json::from_value(Value::Object(serialized_event))?;

        Ok(event)
    }
}

struct EventTuple((StreamToken, Event));

impl TryFrom<&SqliteRow> for EventTuple {
    type Error = StorageError;

    fn try_from(value: &SqliteRow) -> std::result::Result<Self, Self::Error> {
        let event: Event = Event::try_from(value)?;
        let token: StreamToken = decode_stream_token(value.get::<i64, _>("stream_token"))?;

        Ok(EventTuple((token, event)))
    }
}

impl TryFrom<&SqliteRow> for EventRef {
    type Error = StorageError;

    fn try_from(row: &SqliteRow) -> Result<Self> {
        let event_id = decode_from_str(&row.get::<String, _>("event_id"))?;
        let event_sha = row
            .try_get::<Option<String>, _>("sha256")
            .map_err(|_| StorageError::CorruptedData)?;

        let event_ref = match event_sha {
            Some(sha) => EventRef::V1(event_id, EventHash::new(sha)),
            None => EventRef::V2(event_id),
        };

        Ok(event_ref)
    }
}

fn row_to_leaf(row: SqliteRow) -> Result<Leaf> {
    let event_ref = EventRef::try_from(&row)?;
    let depth = decode_i64_to_u64(row.get::<i64, _>("depth"))?;
    let state_at = decode_state_id(&row.get::<Vec<u8>, _>("state"))?;

    Ok(Leaf {
        event_id: event_ref,
        depth,
        state_at,
    })
}

pub fn decode_state_id(input: &[u8]) -> Result<RoomStateId> {
    decode_ulid(input)
}
