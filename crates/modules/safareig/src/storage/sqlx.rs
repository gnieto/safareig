mod checkpoint;
mod room;

use std::convert::TryFrom;

use async_trait::async_trait;
use rusty_ulid::Ulid;
use safareig_core::{
    pagination::{PaginationResponse, Range},
    ruma::{RoomId, RoomVersionId, UserId},
    storage::StorageError,
};
use safareig_sqlx::{decode_from_str, PaginatedIterator, RangeWrapper};
use sea_query::{Alias, Expr, Iden, Query, SqliteQueryBuilder};
use sea_query_binder::SqlxBinder;
use sqlx::{sqlite::SqliteRow, Row, Sqlite};

use super::{
    FilterStorage, Invite, InviteStorage, LocalInvite, RemoteInvite, RoomStorage, ServerConfig,
    StateStorage,
};
use crate::storage::Result;

pub struct SqlxStorage {
    pool: sqlx::Pool<Sqlite>,
}

impl SqlxStorage {
    pub fn new(pool: sqlx::Pool<Sqlite>) -> Self {
        Self { pool }
    }
}

#[async_trait]
impl FilterStorage for SqlxStorage {
    async fn get(
        &self,
        id: &str,
    ) -> super::Result<Option<safareig_core::ruma::api::client::filter::FilterDefinition>> {
        let filter = sqlx::query(
            r#"
            SELECT filter_data FROM user_filters
            WHERE filter_id = ?
            "#,
        )
        .bind(id.to_string())
        .fetch_optional(&self.pool)
        .await?;

        match filter {
            Some(row) => {
                let row_data = row.get::<Vec<u8>, _>("filter_data");
                let filter = serde_json::from_slice(&row_data)?;

                Ok(Some(filter))
            }
            None => Ok(None),
        }
    }

    async fn put(
        &self,
        id: &str,
        filter: &safareig_core::ruma::api::client::filter::FilterDefinition,
    ) -> super::Result<()> {
        let filter_data = serde_json::to_vec(&filter)?;
        let filter_id = id.to_string();

        sqlx::query(
            r#"
            INSERT INTO user_filters (filter_id, filter_data)
            VALUES (?1, ?2)
            ON CONFLICT(filter_id)
            DO UPDATE SET filter_data = ?2
            "#,
        )
        .bind(filter_id)
        .bind(filter_data)
        .execute(&self.pool)
        .await?;

        Ok(())
    }
}

#[async_trait]
impl InviteStorage for SqlxStorage {
    async fn add_invite(&self, user_id: &UserId, invite: Invite) -> Result<()> {
        let next_ulid = Ulid::generate();
        match invite {
            Invite::Local(local) => {
                let sql = "
                    INSERT INTO invites (user_id, stream_token, room_id, event_id) 
                    VALUES(?1, ?2, ?3, ?4)
                ";

                sqlx::query(sql)
                    .bind(user_id.as_str())
                    .bind(next_ulid.to_string())
                    .bind(local.room.as_str())
                    .bind(local.event_id.as_str())
                    .execute(&self.pool)
                    .await?;
            }
            Invite::Remote(remote) => {
                let data = serde_json::to_value(remote.state)?;
                let sql = "
                    INSERT INTO invites (user_id, stream_token, room_id, room_version, state) 
                    VALUES(?1, ?2, ?3, ?4, ?5)
                ";

                sqlx::query(sql)
                    .bind(user_id.as_str())
                    .bind(next_ulid.to_string())
                    .bind(remote.room.as_str())
                    .bind(remote.room_version.as_str())
                    .bind(data)
                    .execute(&self.pool)
                    .await?;
            }
        };

        Ok(())
    }

    async fn query_invites(
        &self,
        user_id: &UserId,
        range: Range<Ulid>,
    ) -> Result<PaginationResponse<Ulid, Invite>> {
        let wrapper = RangeWrapper(range.map(|t| t.to_string()));
        let (sql, values) = Query::select()
            .columns([
                Invites::UserId,
                Invites::StreamToken,
                Invites::RoomId,
                Invites::State,
                Invites::EventId,
            ])
            .expr_as(
                Expr::col(Invites::RoomVersion).cast_as(Alias::new("TEXT")),
                Alias::new("room_version"),
            )
            .from(Invites::Table)
            .and_where(Expr::col(Invites::UserId).eq(user_id.as_str()))
            .cond_where(wrapper.condition(Expr::col(Alias::new("stream_token"))))
            .build_sqlx(SqliteQueryBuilder);
        let invites = sqlx::query_with(&sql, values).fetch_all(&self.pool).await?;

        let iterator = PaginatedIterator::new(invites.into_iter(), |row| {
            let token = decode_from_str(row.try_get::<&str, _>("stream_token")?)?;
            let invite = Invite::try_from(row)?;

            Ok((token, invite))
        });

        iterator.into_pagination_response()
    }

    async fn remove_invite(&self, user_id: &UserId, room_id: &RoomId) -> Result<()> {
        let (query, values) = Query::delete()
            .from_table(Invites::Table)
            .and_where(Expr::col(Invites::UserId).eq(user_id.as_str()))
            .and_where(Expr::col(Invites::RoomId).eq(room_id.as_str()))
            .build_sqlx(SqliteQueryBuilder);

        sqlx::query_with(&query, values).execute(&self.pool).await?;

        Ok(())
    }
}

#[derive(Iden)]
pub enum Invites {
    Table,
    UserId,
    RoomId,
    StreamToken,
    State,
    EventId,
    RoomVersion,
}

impl TryFrom<SqliteRow> for Invite {
    type Error = StorageError;

    fn try_from(row: SqliteRow) -> Result<Self> {
        let room_id = decode_from_str(row.try_get::<&str, _>("room_id")?)?;
        let event_id = row.try_get::<Option<&str>, _>("event_id")?;

        let invite = match event_id {
            Some(event_id) => Invite::Local(LocalInvite {
                room: room_id,
                event_id: event_id.try_into()?,
            }),
            None => {
                let room_version =
                    RoomVersionId::try_from(row.try_get::<&str, _>("room_version")?)?;
                let state = row.try_get::<serde_json::Value, _>("state")?;

                Invite::Remote(RemoteInvite {
                    state: serde_json::from_value(state)?,
                    room: room_id,
                    room_version,
                })
            }
        };

        Ok(invite)
    }
}

#[async_trait]
impl ServerConfig for SqlxStorage {
    async fn get_config(&self, key: &str) -> Result<Option<Vec<u8>>> {
        let sql = "
            SELECT value FROM config
            WHERE key = ?1
        ";
        let result = sqlx::query(sql)
            .bind(key)
            .fetch_optional(&self.pool)
            .await?
            .map(|row| row.get::<Vec<u8>, _>("value"));

        Ok(result)
    }

    async fn put_config(&self, key: &str, value: &[u8]) -> Result<()> {
        sqlx::query(
            r#"
            INSERT INTO config (key, value)
            VALUES (?1, ?2)
            ON CONFLICT(key)
            DO UPDATE SET value = ?2
            "#,
        )
        .bind(key)
        .bind(value)
        .execute(&self.pool)
        .await?;

        Ok(())
    }
}
