use safareig_core::ruma::api::{client::error::ErrorKind, exports::http};

pub struct ResourceNotFound;

impl From<ResourceNotFound> for safareig_core::ruma::api::client::Error {
    fn from(_: ResourceNotFound) -> Self {
        safareig_core::ruma::api::client::Error {
            kind: ErrorKind::NotFound,
            message: "resource not found".to_string(),
            status_code: http::StatusCode::NOT_FOUND,
        }
    }
}
