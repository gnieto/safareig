use std::{collections::HashSet, sync::Arc};

use safareig_core::{
    bus::Bus,
    command::{Container, InjectServices, Module, Router, ServiceCollectionExtension},
    config::HomeserverConfig,
    database::DatabaseConnection,
    ddi::{ServiceCollectionExt, ServiceProvider, ServiceResolverExt},
    listener::Listener,
    ruma::{OwnedServerName, RoomVersionId},
    storage::StorageError,
};
use safareig_ephemeral::services::{EphemeralRegistry, EphemeralStream};
use safareig_federation::{
    client::ClientBuilder,
    keys::KeyProvider,
    server::storage::{FederationStorage, ServerStorage},
};
use safareig_ignored_users::IgnoreUsersRepository;
use safareig_presence::{PresenceAllower, PresenceUpdater};
use safareig_sync_api::SyncExtension;
use safareig_typing::TypingTracker;
use safareig_users::profile::ProfileUpdated;

use crate::{
    client::{
        capabilities::GetCapabilitesCommand,
        filter::{CreateFilter, GetFilter},
        presence::SharedRoomPresenceAllower,
        profile::ProfileBroadcaster,
        room::{
            command::{
                context::ContextCommand,
                create_room::CreateRoomCommand,
                get_members::GetMembersCommand,
                get_messages::GetMessagesCommand,
                invite_room::InviteRoomCommand,
                join_room::{JoinByRoomOrAliasCommand, JoinRoomCommand},
                joined_members::JoinedMembersCommand,
                joined_rooms::JoinedRoomsCommand,
                kick_room::KickRoomCommand,
                leave_room::LeaveRoomCommand,
                send_message::SendMessageCommand,
                send_state_event::SendStateEventCommand,
                state::GetStateCommand,
                state_event_for_key::StateEventForKeyCommand,
                unban::UnbanUserCommand,
                BanUserCommand, ForgetRoomCommand, GetEventCommand, RedactEventCommand,
            },
            events::{EventDecorator, RoomEventDecorator, RoomEventListener, RoomEventPublisher},
            hooks::{CanonicalAliasHook, EventHook, EventHooks},
            loader::RoomLoader,
            membership::{FederatedMembership, LocalMembership, RoomMembershipHandler},
            state::{ruma_stateres::StateResolutionV2, BloomLazyLoadCache, LazyLoadCache},
            versions::EventIdV3,
            RoomStream,
        },
        sync::{
            state::{
                ContextAwareRoomSyncStateProvider, LazyLoadMemberAdder,
                RoomStateAtForwardExtremities, RoomStateAtTimelineStart, RoomStateIncremental,
                RoomSyncStateProvider,
            },
            stream::{EphemeralSync, EventFullSync, EventStreamSync, EventSync, InviteSync},
        },
        thirdparty::ThirdpartyProtocolsCommand,
        topological::{FederatedTopologicalEvents, TopologicalEvents},
        versions::VersionsCommand,
    },
    core::{
        events::{auth::RumaAuthorizer, formatter::EventFormatterV2},
        room::{OutOfRoomMemberEvents, RoomCreator, RoomOptions, RoomVersion, RoomVersionRegistry},
    },
    federation::{
        self,
        backfill::{Backfiller, RemoteSelector, RemoteStateFetcher},
        worker::{FederationSupervisor, FederationWorkerFactory},
    },
    storage::{
        Checkpoint, CheckpointStorage, EventStorage, FilterStorage, InviteStorage, RoomStorage,
        RoomStreamStorage, RoomTopologyStorage, ServerConfig, StateStorage,
    },
};

#[derive(Default)]
pub struct ClientComponent;

impl ClientComponent {
    #[cfg(feature = "backend-sqlx")]
    fn create_sqlx_storage(database: DatabaseConnection) -> Arc<crate::storage::sqlx::SqlxStorage> {
        #[allow(unreachable_patterns)]
        let pool = match database {
            DatabaseConnection::Sqlx(pool) => pool,
            _ => panic!("Configuration mismatch"),
        };
        Arc::new(crate::storage::sqlx::SqlxStorage::new(pool))
    }

    fn build_room_versions(
        state: Arc<dyn StateStorage>,
        event_storage: Arc<dyn EventStorage>,
        key_provider: Arc<KeyProvider>,
        config: Arc<HomeserverConfig>,
    ) -> RoomVersionRegistry {
        // State resolutions
        let state_res2 = Arc::new(StateResolutionV2::new(state, event_storage));
        let event_id_v3 = Arc::new(EventIdV3 {});
        let format_v2 = Arc::new(EventFormatterV2::new(
            #[allow(clippy::redundant_clone)]
            key_provider.clone(),
            config.server_name.clone(),
        ));

        // If compiled for sytest, we change state resolution version for v1 room. Sytest federation
        // tests use exclusively v1 rooms which we almost support, but we are missing state res v1. Instead
        // of failing due to errors in state res, we ue the v2 state resolution algorithm which will
        // give a consistent view in case of forks during test execution.
        // Once we add proper support for state resolution v1, this will be removed.
        #[cfg(feature = "sytest")]
        let v1_state_res = { state_res2.clone() };

        // Build standard room versions
        #[cfg(feature = "sytest")]
        let v1 = {
            let format_v1 = Arc::new(crate::core::events::formatter::EventFormatterV1::new(
                key_provider.clone(),
                config.server_name.clone(),
            ));
            let event_id_v1 = Arc::new(crate::client::room::versions::EventIdV1 {});

            RoomVersion::new(
                RoomVersionId::V1,
                v1_state_res.clone(),
                Arc::new(RumaAuthorizer::from(RoomVersionId::V1)),
                event_id_v1,
                format_v1,
                RoomOptions::PRE_V11,
            )
        };

        #[cfg(feature = "sytest")]
        let v2 = {
            let format_v1 = Arc::new(crate::core::events::formatter::EventFormatterV1::new(
                key_provider.clone(),
                config.server_name.clone(),
            ));
            let event_id_v1 = Arc::new(crate::client::room::versions::EventIdV1 {});

            RoomVersion::new(
                RoomVersionId::V2,
                v1_state_res.clone(),
                Arc::new(RumaAuthorizer::from(RoomVersionId::V2)),
                event_id_v1.clone(),
                format_v1,
                RoomOptions::PRE_V11,
            )
        };

        #[cfg(feature = "sytest")]
        let v5 = RoomVersion::new(
            RoomVersionId::V5,
            state_res2.clone(),
            Arc::new(RumaAuthorizer::from(RoomVersionId::V5)),
            event_id_v3.clone(),
            format_v2.clone(),
            RoomOptions::PRE_V11,
        );

        let v6 = RoomVersion::new(
            RoomVersionId::V6,
            state_res2.clone(),
            Arc::new(RumaAuthorizer::from(RoomVersionId::V6)),
            event_id_v3.clone(),
            format_v2.clone(),
            RoomOptions::PRE_V11,
        );

        let v7 = RoomVersion::new(
            RoomVersionId::V7,
            state_res2.clone(),
            Arc::new(RumaAuthorizer::from(RoomVersionId::V7)),
            event_id_v3.clone(),
            format_v2.clone(),
            RoomOptions::PRE_V11,
        );

        let v8 = RoomVersion::new(
            RoomVersionId::V8,
            state_res2.clone(),
            Arc::new(RumaAuthorizer::from(RoomVersionId::V8)),
            event_id_v3.clone(),
            format_v2.clone(),
            RoomOptions::PRE_V11,
        );

        let v9 = RoomVersion::new(
            RoomVersionId::V9,
            state_res2.clone(),
            Arc::new(RumaAuthorizer::from(RoomVersionId::V9)),
            event_id_v3.clone(),
            format_v2.clone(),
            RoomOptions::PRE_V11,
        );

        let v10 = RoomVersion::new(
            RoomVersionId::V10,
            state_res2.clone(),
            Arc::new(RumaAuthorizer::from(RoomVersionId::V10)),
            event_id_v3.clone(),
            format_v2.clone(),
            RoomOptions::PRE_V11,
        );

        let v10_custom = RoomVersion::new(
            RoomVersionId::try_from("s.v10").unwrap(),
            state_res2.clone(),
            Arc::new(RumaAuthorizer::from(RoomVersionId::V10)),
            event_id_v3.clone(),
            format_v2.clone(),
            RoomOptions::PRE_V11,
        );

        let v11 = RoomVersion::new(
            RoomVersionId::V11,
            state_res2.clone(),
            Arc::new(RumaAuthorizer::from(RoomVersionId::V11)),
            event_id_v3.clone(),
            format_v2.clone(),
            RoomOptions::POST_V11,
        );

        // Build custom room version
        let mut room_version_loader = RoomVersionRegistry::default();
        #[cfg(feature = "sytest")]
        room_version_loader.register(v1);
        #[cfg(feature = "sytest")]
        room_version_loader.register(v2);
        #[cfg(feature = "sytest")]
        room_version_loader.register(v5);
        room_version_loader.register(v6);
        room_version_loader.register(v7);
        room_version_loader.register(v8);
        room_version_loader.register(v9);
        room_version_loader.register(v10);
        room_version_loader.register(v10_custom);
        room_version_loader.register(v11);

        room_version_loader
    }
}

#[async_trait::async_trait]
impl<R: Router> Module<R> for ClientComponent {
    fn register(&self, container: &mut Container, router: &mut R) {
        container
            .service_collection
            .service_factory(|sp: &ServiceProvider| {
                let room_versions = Self::build_room_versions(
                    sp.get::<Arc<dyn StateStorage>>()?.clone(),
                    sp.get::<Arc<dyn EventStorage>>()?.clone(),
                    sp.get::<Arc<KeyProvider>>()?.clone(),
                    sp.get::<Arc<HomeserverConfig>>()?.clone(),
                );

                Ok(Arc::new(room_versions))
            });

        container
            .service_collection
            .service_factory_var("canonical", |sp: &ServiceProvider| {
                let canonical: Arc<dyn EventHook> = Arc::new(CanonicalAliasHook::inject(sp)?);
                Ok(canonical)
            });
        container
            .service_collection
            .service_factory(|sp: &ServiceProvider| {
                let hooks = sp.get_all::<Arc<dyn EventHook>>()?;
                let event_hooks = EventHooks::new(hooks.into_iter().map(|t| t.1.clone()).collect());

                Ok(Arc::new(event_hooks))
            });

        container
            .service_collection
            .service_factory(|sp: &ServiceProvider| {
                let listeners = sp
                    .get_all::<Arc<dyn RoomEventListener>>()?
                    .into_iter()
                    .map(|t| t.1.clone())
                    .collect();
                Ok(Arc::new(RoomEventPublisher::new(listeners)))
            });

        container
            .service_collection
            .service_factory(|sp: &ServiceProvider| {
                let decorators = sp
                    .get_all::<Arc<dyn RoomEventDecorator>>()?
                    .into_iter()
                    .map(|t| t.1.clone())
                    .collect();
                Ok(Arc::new(EventDecorator::new(decorators)))
            });

        container.service_collection.register::<RoomLoader>();

        container
            .service_collection
            .service_factory(|db: &DatabaseConnection| {
                let storage: Arc<dyn RoomStorage> = Self::create_sqlx_storage(db.clone());
                Ok(storage)
            });
        tracing::info!("Register event storage");
        container
            .service_collection
            .service_factory(|db: &DatabaseConnection| {
                let storage: Arc<dyn EventStorage> = Self::create_sqlx_storage(db.clone());
                Ok(storage)
            });
        container
            .service_collection
            .service_factory(|db: &DatabaseConnection| {
                let storage: Arc<dyn StateStorage> = Self::create_sqlx_storage(db.clone());
                Ok(storage)
            });
        container
            .service_collection
            .service_factory(|db: &DatabaseConnection| {
                let storage: Arc<dyn InviteStorage> = Self::create_sqlx_storage(db.clone());
                Ok(storage)
            });
        container
            .service_collection
            .service_factory(|db: &DatabaseConnection| {
                let storage: Arc<dyn CheckpointStorage<federation::worker::Checkpoint>> =
                    Self::create_sqlx_storage(db.clone());
                Ok(storage)
            });
        container.service_collection.register::<RoomStream>();
        container
            .service_collection
            .service_factory(|db: &DatabaseConnection| {
                let storage: Arc<dyn RoomTopologyStorage> = Self::create_sqlx_storage(db.clone());
                Ok(storage)
            });
        container
            .service_collection
            .service_factory(|db: &DatabaseConnection| {
                let storage: Arc<dyn FilterStorage> = Self::create_sqlx_storage(db.clone());
                Ok(storage)
            });
        container
            .service_collection
            .service_factory(|db: &DatabaseConnection| {
                let storage: Arc<dyn CheckpointStorage<Checkpoint>> =
                    Self::create_sqlx_storage(db.clone());
                Ok(storage)
            });
        container
            .service_collection
            .service_factory(|db: &DatabaseConnection| {
                let storage: Arc<dyn RoomStreamStorage> = Self::create_sqlx_storage(db.clone());
                Ok(storage)
            });

        container.service_collection.register::<RoomCreator>();
        container
            .service_collection
            .register::<ProfileBroadcaster>();
        container
            .service_collection
            .service_factory(|sp: &ServiceProvider| {
                let presence_allower = SharedRoomPresenceAllower::new(
                    sp.get::<Arc<dyn RoomStorage>>()?.clone(),
                    sp.get::<Arc<dyn IgnoreUsersRepository>>().ok().cloned(),
                );
                let presence_allower: Arc<dyn PresenceAllower> = Arc::new(presence_allower);
                Ok(presence_allower)
            });
        container
            .service_collection
            .service_factory_var("ephemeral", |sp: &ServiceProvider| {
                let sync: Arc<dyn SyncExtension> = Arc::new(EphemeralSync::new(
                    sp.get::<EphemeralStream>()?.clone(),
                    sp.get::<TypingTracker>()?.clone(),
                    sp.get::<Arc<dyn IgnoreUsersRepository>>().ok().cloned(),
                ));

                Ok(sync)
            });
        container
            .service_collection
            .service_factory_var("invite", |sp: &ServiceProvider| {
                let sync: Arc<dyn SyncExtension> = Arc::new(InviteSync::inject(sp)?);

                Ok(sync)
            });

        container
            .service_collection
            .service_factory_var("event", |sp: &ServiceProvider| {
                let sync: Arc<dyn SyncExtension> = Arc::new(EventSync::new(
                    sp.get::<EventFullSync>()?.clone(),
                    sp.get::<EventStreamSync>()?.clone(),
                ));

                Ok(sync)
            });
        container
            .service_collection
            .register_arc::<RemoteStateFetcher>();
        container.service_collection.register::<RemoteSelector>();
        container.service_collection.register::<EventFullSync>();
        container.service_collection.register::<EventStreamSync>();
        container
            .service_collection
            .register::<RoomStateAtForwardExtremities>();
        container
            .service_collection
            .register::<RoomStateAtTimelineStart>();
        container
            .service_collection
            .register::<RoomStateIncremental>();
        container
            .service_collection
            .register::<LazyLoadMemberAdder>();
        container
            .service_collection
            .register::<RoomMembershipHandler>();
        container.service_collection.register::<LocalMembership>();
        container
            .service_collection
            .register::<FederatedMembership>();
        container
            .service_collection
            .register::<OutOfRoomMemberEvents>();

        container.service_collection.service_factory(|| {
            let cache: Arc<dyn LazyLoadCache> = Arc::new(BloomLazyLoadCache::default());
            Ok(cache)
        });
        container.service_collection.register::<TopologicalEvents>();
        container
            .service_collection
            .register::<FederatedTopologicalEvents>();
        container.service_collection.register::<Backfiller>();

        container
            .service_collection
            .service_factory(|sp: &ServiceProvider| {
                let state: Arc<dyn RoomSyncStateProvider> =
                    Arc::new(ContextAwareRoomSyncStateProvider::inject(sp)?);
                Ok(state)
            });

        container.service_collection.service_factory_var(
            "profile-broadcast",
            |sp: &ServiceProvider| {
                let listener: Arc<dyn Listener<ProfileUpdated>> =
                    Arc::new(ProfileBroadcaster::inject(sp)?);

                Ok(listener)
            },
        );

        // Filter
        container.inject_endpoint::<_, GetFilter, _>(router);
        container.inject_endpoint::<_, CreateFilter, _>(router);

        // Rooms (split room directory)
        container.inject_endpoint::<_, ContextCommand, _>(router);
        container.inject_endpoint::<_, CreateRoomCommand, _>(router);
        container.inject_endpoint::<_, GetMembersCommand, _>(router);
        container.inject_endpoint::<_, GetMessagesCommand, _>(router);
        container.inject_endpoint::<_, InviteRoomCommand, _>(router);
        container.inject_endpoint::<_, JoinRoomCommand, _>(router);
        container.inject_endpoint::<_, JoinByRoomOrAliasCommand, _>(router);
        container.inject_endpoint::<_, JoinedRoomsCommand, _>(router);
        container.inject_endpoint::<_, JoinedMembersCommand, _>(router);
        container.inject_endpoint::<_, KickRoomCommand, _>(router);
        container.inject_endpoint::<_, LeaveRoomCommand, _>(router);
        container.inject_endpoint::<_, SendStateEventCommand, _>(router);
        container.inject_endpoint::<_, StateEventForKeyCommand, _>(router);
        container.inject_endpoint::<_, GetStateCommand, _>(router);
        container.inject_endpoint::<_, GetEventCommand, _>(router);
        container.inject_endpoint::<_, RedactEventCommand, _>(router);
        container.inject_endpoint::<_, BanUserCommand, _>(router);
        container.inject_endpoint::<_, UnbanUserCommand, _>(router);
        container.inject_endpoint::<_, ForgetRoomCommand, _>(router);
        container.register_lazy_endpoint(
            |sp: &ServiceProvider| {
                Ok(SendMessageCommand::new(
                    sp.get::<RoomLoader>()?.clone(),
                    sp.get::<PresenceUpdater>().ok().cloned(),
                    sp.get::<TypingTracker>()?.clone(),
                ))
            },
            router,
        );

        // Client-common
        container.inject_endpoint::<_, VersionsCommand, _>(router);
        container.inject_endpoint::<_, GetCapabilitesCommand, _>(router);

        // Third-party
        container.inject_endpoint::<_, ThirdpartyProtocolsCommand, _>(router);
    }

    async fn spawn_workers(
        &self,
        container: &ServiceProvider,
        background: &graceful::BackgroundHandler,
    ) {
        // Federation
        let config = container.get::<Arc<HomeserverConfig>>().unwrap();

        if config.federation.enabled {
            let server_storage = container.get::<Arc<dyn ServerStorage>>().unwrap();
            let federation_storage = container.get::<Arc<dyn FederationStorage>>().unwrap();
            let bus = container.get::<Bus>().unwrap().clone();

            let servers: HashSet<OwnedServerName> = federation_storage
                .tracked_servers()
                .await
                .unwrap_or_default()
                .into_iter()
                .collect();

            let factory = FederationWorkerFactory::new(
                config.clone(),
                container.get::<RoomStream>().unwrap().clone(),
                container
                    .get::<Arc<dyn CheckpointStorage<federation::worker::Checkpoint>>>()
                    .unwrap()
                    .clone(),
                container.get::<Arc<KeyProvider>>().unwrap().clone(),
                container.get::<Arc<ClientBuilder>>().unwrap().clone(),
                bus.clone(),
                container.get::<EphemeralStream>().unwrap().clone(),
                container
                    .get::<Arc<dyn FederationStorage>>()
                    .unwrap()
                    .clone(),
                container.get::<EphemeralRegistry>().unwrap().clone(),
                server_storage.clone(),
            );

            let mut supervisor =
                FederationSupervisor::new(background.clone(), bus.clone(), factory);

            for server in &servers {
                let is_server_online = server_storage
                    .load_server(server)
                    .await
                    .map(|server| server.state().is_online())
                    .unwrap_or(true);

                if is_server_online {
                    supervisor.start_server(server).await;
                }
            }

            tokio::spawn(supervisor.run());
        }
    }

    async fn run_migrations(&self, container: &ServiceProvider) -> Result<(), StorageError> {
        #[cfg(feature = "backend-sqlx")]
        {
            let db = container.get::<DatabaseConnection>()?;
            safareig_sqlx::run_migration(db, "sqlite", "safareig").await?;
        }

        Ok(())
    }
}

#[derive(Default)]
pub struct ServerConfigModule;

impl ServerConfigModule {
    fn create_server_config(db: DatabaseConnection) -> Arc<dyn ServerConfig> {
        match db {
            #[cfg(feature = "backend-sqlx")]
            DatabaseConnection::Sqlx(pool) => {
                Arc::new(crate::storage::sqlx::SqlxStorage::new(pool))
            }
            _ => unimplemented!(""),
        }
    }
}

#[async_trait::async_trait]
impl<R: Router> Module<R> for ServerConfigModule {
    fn register(&self, container: &mut Container, _router: &mut R) {
        container
            .service_collection
            .service_factory(|c: &DatabaseConnection| Ok(Self::create_server_config(c.clone())));
    }

    async fn run_migrations(&self, container: &ServiceProvider) -> Result<(), StorageError> {
        #[cfg(feature = "backend-sqlx")]
        {
            let db = container.get::<DatabaseConnection>()?;
            safareig_sqlx::run_migration(db, "sqlite", "serverconfig").await?;
        }

        Ok(())
    }
}
