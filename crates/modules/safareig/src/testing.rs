use std::{any::Any, collections::HashMap, convert::TryFrom, sync::Arc};

use futures::StreamExt;
use reqwest::StatusCode;
use rusty_ulid::Ulid;
use safareig_client_config::ClientConfigModule;
use safareig_core::{
    app::{App, AppOptions},
    auth::{FederationAuth, Identity},
    command::{Command, Container, DummyRouter, FrozenContainer, Module, Router},
    component::CoreModule,
    config::{
        AccountConfig, DatabaseConfig, FederationConfig, HomeserverConfig, MediaConfig,
        MonitoringConfig, ServerSideSearchConfig, SqliteOptions, SslConfig, UiaaConfig,
    },
    ruma::{
        api::client::{
            membership::{
                invite_user, invite_user::v3::InvitationRecipient, join_room_by_id_or_alias,
            },
            message::send_message_event,
            profile::set_display_name,
            room::{create_room, Visibility},
            Error,
        },
        events::{
            pdu::Pdu,
            room::message::{MessageType, RoomMessageEventContent, TextMessageEventContent},
            AnyMessageLikeEventContent, EventContent, MessageLikeEventType, TimelineEventType,
        },
        serde::Raw,
        EventId, OwnedEventId, OwnedRoomId, OwnedServerName, RoomId, RoomVersionId, ServerName,
        ServerSigningKeyId, SigningKeyAlgorithm, TransactionId,
    },
};
use safareig_devices::DeviceModule;
use safareig_ephemeral::EphemeralModule;
use safareig_federation::ServerKeysModule;
use safareig_rooms::alias::module::AliasesModule;
use safareig_sync::module::SyncModule;
use safareig_testing::CustomEventContent;
use safareig_typing::module::TypingModule;
use safareig_uiaa::module::UiaaModule;
use safareig_users::{
    module::UsersModule,
    profile::command::client::SetDisplaynameCommand,
    storage::{User, UserStorage},
};
use serde_json::value::RawValue;

use crate::{
    client::room::{
        command::{
            create_room::CreateRoomCommand, invite_room::InviteRoomCommand,
            join_room::JoinByRoomOrAliasCommand, send_message::SendMessageCommand,
        },
        loader::RoomLoader,
        state::{RoomState, StateInfo},
        Room,
    },
    core::{
        events::{Event, EventBuilder},
        room::RoomVersionRegistry,
    },
    federation::component::FederationComponent,
    services::{ClientComponent, ServerConfigModule},
    storage::{EventStorage, Leaf, RoomTopologyStorage, StateStorage},
};

pub struct TestingDB {
    pub container: FrozenContainer,
}

impl Default for TestingDB {
    fn default() -> Self {
        Self::with_config(Self::default_config())
    }
}

impl TestingDB {
    pub fn with_config(config: HomeserverConfig) -> Self {
        let config = Arc::new(config);
        let router = DummyRouter {};
        let options = AppOptions {
            spawn_worker: None,
            run_migrations: true,
        };
        let app = App::new(router, options, config.clone())
            .with_module(Box::new(CoreModule::new(config.clone())))
            .with_module(Box::<ClientConfigModule>::default())
            .with_module(Box::<UiaaModule>::default())
            .with_module(Box::<DeviceModule>::default())
            .with_module(Box::<EphemeralModule>::default())
            .with_module(Box::<TypingModule>::default())
            .with_module(Box::<ServerConfigModule>::default())
            .with_module(Box::<ClientComponent>::default())
            .with_module(Box::<FederationComponent>::default())
            .with_module(Box::<SyncModule>::default())
            .with_module(Box::<AliasesModule>::default())
            .with_module(Box::<UsersModule>::default())
            .with_module(Box::<ServerKeysModule>::default());

        #[allow(clippy::disallowed_methods)]
        let (container, _) = ::futures::executor::block_on(app.take());

        TestingDB { container }
    }

    pub fn server_name(&self) -> OwnedServerName {
        let cfg = self.container.service::<Arc<HomeserverConfig>>();
        cfg.server_name.clone()
    }

    fn get_database_config() -> DatabaseConfig {
        let db = std::env::var("SAFAREIG_TEST_DB").unwrap_or_else(|_| "sqlite".to_string());

        if db == "sqlite" {
            // Lazy static to initialize and run migrations?
            DatabaseConfig::Sqlite(SqliteOptions::testing())
        } else {
            unimplemented!("")
        }
    }

    pub fn default_config() -> HomeserverConfig {
        HomeserverConfig {
            server_name: "test.cat".parse().unwrap(),
            bind: "".to_string(),
            ssl_bind: "".to_string(),
            account: AccountConfig {
                account_data_max_size: 100,
            },
            federation: FederationConfig {
                skip_insecure: false,
                enabled: true,
                notary_servers: vec![],
                rate_limit_quota_minute: 300,
                circuit_breaker_consecutive_failures: 10,
            },
            ssl: SslConfig {
                enabled: false,
                cert_path: None,
                key_path: None,
            },
            appservices: HashMap::new(),
            database: Self::get_database_config(),
            uiaa: UiaaConfig {
                register_stages: vec![],
                login_stages: vec![],
                shared_token: None,
                device_stages: vec![],
                password_stages: vec![],
            },
            media: MediaConfig::default(),
            monitoring: MonitoringConfig::default(),
            content_reporting: Default::default(),
            admin: Default::default(),
            auth: Default::default(),
            server_side_search: ServerSideSearchConfig::Sqlite,
            rooms: Default::default(),
        }
    }

    pub fn default_room_version(&self) -> RoomVersionId {
        let config = &self.container.service::<Arc<HomeserverConfig>>();
        config.default_room_version()
    }

    pub fn command<T: Any + Send + Sync>(&self) -> &T {
        self.container.command::<T>().unwrap()
    }

    pub async fn register(&self) -> Identity {
        let id = Self::new_identity();
        let user = User::new(id.user(), "pwd".to_string());
        let user_storage = self.container.service::<Arc<dyn UserStorage>>();
        user_storage.create_user(&user).await.unwrap();

        id
    }

    pub async fn register_with_profile(
        &self,
        displayname: Option<String>,
        _avatar: Option<String>,
    ) -> Identity {
        let id = self.register().await;

        if displayname.is_some() {
            let cmd = self.command::<SetDisplaynameCommand>();
            let input = set_display_name::v3::Request {
                user_id: id.user().to_owned(),
                displayname,
            };

            cmd.execute(input, id.clone()).await.unwrap();
        }

        id
    }

    pub async fn public_room(&self) -> (Identity, Room) {
        let cmd = self.command::<CreateRoomCommand>();
        let id = self.register().await;
        let room_name = "Room name".parse().unwrap();

        let input = create_room::v3::Request {
            creation_content: None,
            power_level_content_override: None,
            initial_state: vec![],
            invite: vec![],
            invite_3pid: vec![],
            is_direct: false,
            name: Some(room_name),
            preset: None,
            room_alias_name: Some(id.user().localpart().to_string()),
            room_version: None,
            topic: Some("Description".to_string()),
            visibility: Visibility::Public,
        };
        let output = cmd.execute(input, id.clone()).await.unwrap();

        let room_loader = self.container.service::<RoomLoader>();
        let room = room_loader.get(&output.room_id).await.unwrap().unwrap();

        (id, room)
    }

    pub async fn public_room_version(&self, version: RoomVersionId) -> (Identity, Room) {
        let cmd = self.command::<CreateRoomCommand>();
        let id = self.register().await;
        let room_name = "Room name".parse().unwrap();

        let input = create_room::v3::Request {
            creation_content: None,
            power_level_content_override: None,
            initial_state: vec![],
            invite: vec![],
            invite_3pid: vec![],
            is_direct: false,
            name: Some(room_name),
            preset: None,
            room_alias_name: Some(id.user().localpart().to_string()),
            room_version: Some(version),
            topic: Some("Description".to_string()),
            visibility: Visibility::Public,
        };
        let output = cmd.execute(input, id.clone()).await.unwrap();

        let room_loader = self.container.service::<RoomLoader>();
        let room = room_loader.get(&output.room_id).await.unwrap().unwrap();

        (id, room)
    }

    pub async fn private_room(&self) -> (Room, Identity) {
        let cmd = self.command::<CreateRoomCommand>();
        let id = Self::new_identity();
        let room_name = "Room name".parse().unwrap();

        let input = create_room::v3::Request {
            creation_content: None,
            power_level_content_override: None,
            initial_state: vec![],
            invite: vec![],
            invite_3pid: vec![],
            is_direct: false,
            name: Some(room_name),
            preset: None,
            room_alias_name: Some("private_alias_test".to_string()),
            room_version: None,
            topic: Some("Description".to_string()),
            visibility: Visibility::Private,
        };
        let output = cmd.execute(input, id.clone()).await.unwrap();

        let room_loader = self.container.service::<RoomLoader>();

        (room_loader.get(&output.room_id).await.unwrap().unwrap(), id)
    }

    pub async fn join_room(
        &self,
        room: OwnedRoomId,
        id: Identity,
    ) -> Result<join_room_by_id_or_alias::v3::Response, Error> {
        let cmd = self.command::<JoinByRoomOrAliasCommand>();

        let input = join_room_by_id_or_alias::v3::Request {
            room_id_or_alias: room.to_string().parse().unwrap(),
            server_name: Vec::new(),
            third_party_signed: None,
            reason: None,
        };

        cmd.execute(input, id).await
    }

    pub async fn invite_room(
        &self,
        room: &RoomId,
        inviter: &Identity,
        invitee: &Identity,
    ) -> Result<invite_user::v3::Response, Error> {
        let cmd = self.command::<InviteRoomCommand>();

        let input = invite_user::v3::Request {
            room_id: room.to_owned(),
            recipient: InvitationRecipient::UserId {
                user_id: invitee.user().to_owned(),
            },
            reason: None,
        };

        cmd.execute(input, inviter.clone()).await
    }

    pub async fn talk(
        &self,
        room: &RoomId,
        id: &Identity,
        msg: &str,
    ) -> Result<OwnedEventId, Error> {
        let cmd = self.command::<SendMessageCommand>();

        let content = AnyMessageLikeEventContent::RoomMessage(RoomMessageEventContent {
            msgtype: MessageType::Text(TextMessageEventContent {
                body: msg.to_string(),
                formatted: None,
            }),
            relates_to: None,
            mentions: None,
        });
        let (event_type, body) = event_content_to_raw(content);

        let input = send_message_event::v3::Request {
            room_id: room.to_owned(),
            txn_id: TransactionId::new(),
            event_type: MessageLikeEventType::from(event_type.to_string().as_str()),
            body,
            timestamp: None,
        };
        let response = cmd.execute(input, id.clone()).await?;

        Ok(response.event_id)
    }

    pub async fn add_auth_data(&self, builder: EventBuilder, leaf: &Leaf) -> EventBuilder {
        let state = RoomState::load(
            leaf.state_at,
            self.container.service::<Arc<dyn StateStorage>>().clone(),
        );
        let state_info = StateInfo::try_from(&builder).ok();
        let events = self.container.service::<Arc<dyn EventStorage>>().clone();

        let auth_events = state
            .auth_events(&builder.sender, state_info, events.as_ref())
            .await
            .expect("auth event can be calculated")
            .into_iter()
            .map(|event| event.event_ref)
            .collect();

        builder
            .depth(leaf.depth)
            .prev_events(vec![leaf.event_id.clone()])
            .auth_events(auth_events)
    }

    pub async fn event_at_leaf(&self, room: &RoomId) -> Event {
        let topology = self
            .container
            .service::<Arc<dyn RoomTopologyStorage>>()
            .clone();
        let events = self.container.service::<Arc<dyn EventStorage>>().clone();

        let leaves = topology.forward_extremities(room).await.unwrap();
        let leaf = leaves.first().unwrap();

        events
            .get_event(leaf.event_id.event_id())
            .await
            .unwrap()
            .unwrap()
    }

    pub fn new_identity() -> Identity {
        let device = "device".into();
        let user_id = format!(
            "@{}:test.cat",
            Ulid::generate().to_string().to_ascii_lowercase()
        )
        .parse()
        .unwrap();

        Identity::Device(device, user_id)
    }

    pub fn federation_identity() -> Identity {
        let auth = FederationAuth {
            server: "localhost:8888".parse().unwrap(),
            key: "ed25519:key1".parse().unwrap(),
            signature: "".to_string(),
        };

        Identity::Federation(auth)
    }

    pub async fn contains_pdu<F: FnMut(&Event) -> bool>(
        &self,
        events: &[Raw<Pdu>],
        f: F,
    ) -> Option<Event> {
        let room_versions = self.container.service::<Arc<RoomVersionRegistry>>().clone();
        let config = self.container.service::<Arc<HomeserverConfig>>();
        let room_version = room_versions.get(&config.default_room_version()).unwrap();

        let events: Vec<Event> = futures::stream::iter(events.iter())
            .then(|r| async move { room_version.event_format().pdu_to_event(r).await.unwrap() })
            .collect()
            .await;

        events.into_iter().find(f)
    }

    pub async fn find_pdu_with_id(&self, events: &[Raw<Pdu>], event_id: &EventId) -> Option<Event> {
        self.contains_pdu(events, |p| p.event_ref.event_id() == event_id)
            .await
    }

    pub fn dummy_content() -> CustomEventContent {
        CustomEventContent {
            key: "value".to_string(),
        }
    }
}

pub fn assert_not_found<V>(response: &Result<V, safareig_core::ruma::api::client::Error>) {
    assert!(response.is_err());
    assert_eq!(
        StatusCode::NOT_FOUND,
        response.as_ref().err().unwrap().status_code
    );
}

pub fn generate_event_id() -> OwnedEventId {
    "$Rqnc-F-dvnEYJTyHq_iKxU2bZ1CI92-kuZq3a5lr5Zg"
        .parse()
        .unwrap()
}

pub fn generate_federation_auth() -> Identity {
    let auth = FederationAuth {
        server: ServerName::parse("localhost:8888").unwrap(),
        key: ServerSigningKeyId::from_parts(SigningKeyAlgorithm::Ed25519, "testing".into()),
        signature: "".to_string(),
    };

    Identity::Federation(auth)
}

pub fn event_content_to_raw<E>(event_content: E) -> (TimelineEventType, Raw<E>)
where
    E: EventContent,
    E::EventType: ToString,
{
    let event_type = TimelineEventType::from(event_content.event_type().to_string());
    let content_str = serde_json::to_string(&event_content).unwrap();

    (
        event_type,
        Raw::from_json(RawValue::from_string(content_str).unwrap()),
    )
}

pub struct DummySync {}

#[async_trait::async_trait]
impl<R: Router> Module<R> for DummySync {
    fn register(&self, _container: &mut Container, _router: &mut R) {}
}
