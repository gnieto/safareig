pub(crate) mod auth;
pub mod formatter;

use std::{
    collections::BTreeMap,
    convert::{TryFrom, TryInto},
    fmt::Display,
    hash::Hash,
    time::{Duration, SystemTime},
};

use futures::StreamExt;
use safareig_core::{
    auth::Identity,
    events::{Content, EventError},
    ruma::{
        canonical_json::{redact, RedactedBecause},
        events::{
            pdu::{EventHash, Pdu as RumaPdu, Pdu},
            relation::{InReplyTo, Reference},
            space::child::HierarchySpaceChildEvent,
            AnyStateEvent, AnyStrippedStateEvent, AnySyncStateEvent, AnySyncTimelineEvent,
            AnyTimelineEvent, EventContent, TimelineEventType,
        },
        serde::Raw,
        signatures::{hash_and_sign_event, KeyPair},
        CanonicalJsonObject, CanonicalJsonValue, EventId, OwnedEventId, OwnedRoomId,
        OwnedServerName, OwnedServerSigningKeyId, OwnedUserId, RoomId, RoomVersionId, ServerName,
        UserId,
    },
    time::system_time_to_u64,
};
use safareig_federation::keys::CanonicalJsonValueExt;
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use serde_json::value::to_raw_value;

use super::room::EventFormat;
use crate::{
    client::room::{versions::EventIdCalculation, RoomError},
    storage::TopologicalToken,
};

#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(untagged)]
pub enum EventRef {
    V1(OwnedEventId, EventHash),
    V2(OwnedEventId),
}

impl PartialEq for EventRef {
    fn eq(&self, other: &Self) -> bool {
        self.event_id() == other.event_id()
    }
}

impl Eq for EventRef {}

impl Hash for EventRef {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.event_id().hash(state)
    }
}

impl Display for EventRef {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.event_id())
    }
}

impl EventRef {
    pub fn event_id(&self) -> &EventId {
        match self {
            Self::V1(event_id, _) => event_id.as_ref(),
            Self::V2(event_id) => event_id.as_ref(),
        }
    }

    pub fn event_hash(&self) -> Option<&EventHash> {
        match self {
            Self::V1(_, hash) => Some(hash),
            Self::V2(_) => None,
        }
    }

    pub fn owned_event_id(&self) -> OwnedEventId {
        match self {
            Self::V1(event_id, _) => event_id.to_owned(),
            Self::V2(event_id) => event_id.to_owned(),
        }
    }

    #[allow(clippy::borrowed_box)]
    pub fn boxed_event_id(&self) -> &OwnedEventId {
        match self {
            Self::V1(event_id, _) => event_id,
            Self::V2(event_id) => event_id,
        }
    }
}

impl From<EventRef> for OwnedEventId {
    fn from(r: EventRef) -> Self {
        match r {
            EventRef::V1(eid, _) => eid,
            EventRef::V2(eid) => eid,
        }
    }
}

impl TryFrom<Event> for Raw<AnyStateEvent> {
    type Error = EventError;

    fn try_from(value: Event) -> Result<Self, Self::Error> {
        let sync_event = StateEvent::from(&value);
        let raw_value = serde_json::value::to_raw_value(&sync_event)?;

        Ok(Raw::from_json(raw_value))
    }
}

impl TryFrom<Event> for Raw<AnySyncStateEvent> {
    type Error = EventError;

    fn try_from(value: Event) -> Result<Self, Self::Error> {
        if value.state_key.is_none() {
            return Err(EventError::MissingField("state_key".to_string()));
        }

        let state = StateEvent::from(&value);
        let raw_value = serde_json::value::to_raw_value(&state)?;

        Ok(Raw::from_json(raw_value))
    }
}

impl TryFrom<Event> for Raw<AnyTimelineEvent> {
    type Error = EventError;

    fn try_from(mut value: Event) -> Result<Self, Self::Error> {
        if let TimelineEventType::RoomRedaction = value.kind {
            let content = value
                .content
                .as_object_mut()
                .ok_or_else(|| EventError::Custom("expect content is an object".to_string()))?;
            let redacts = content
                .get("redacts")
                .and_then(|c| c.as_str())
                .and_then(|event_id| EventId::parse(event_id).ok())
                .or(value.redacts);

            value.redacts = redacts.clone();

            if let Some(redacts) = redacts {
                content.insert(
                    "redacts".to_string(),
                    serde_json::Value::String(redacts.to_string()),
                );
            }
        }

        let room_event = RoomEvent {
            content: value.content,
            kind: value.kind,
            event_id: value.event_ref.owned_event_id(),
            sender: value.sender,
            origin_server_ts: value.origin_server_ts,
            unsigned: value.unsigned,
            room_id: value.room_id,
            prev_content: value.prev_content,
            state_key: value.state_key,
            redacts: value.redacts,
        };

        let raw_event: Raw<RoomEvent> = Raw::new(&room_event)?;

        Ok(raw_event.cast())
    }
}

impl TryFrom<Event> for Raw<AnyStrippedStateEvent> {
    type Error = EventError;

    fn try_from(value: Event) -> Result<Self, Self::Error> {
        let stripped = StrippedStateEvent::from(&value);
        let raw_value = serde_json::value::to_raw_value(&stripped)?;

        Ok(Raw::from_json(raw_value))
    }
}

impl TryFrom<Event> for Raw<safareig_core::ruma::events::AnyMessageLikeEvent> {
    type Error = EventError;

    fn try_from(value: Event) -> Result<Self, Self::Error> {
        let stripped = SyncMessageEvent::from(&value);
        let raw_value = serde_json::value::to_raw_value(&stripped)?;

        Ok(Raw::from_json(raw_value))
    }
}

// TODO: Reconsider serde dependency
// TODO: Reconsider pub fields
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Event {
    #[serde(rename = "event_id")]
    pub event_ref: EventRef,
    pub sender: OwnedUserId,
    pub origin_server_ts: u64,
    #[serde(rename = "type")]
    pub kind: TimelineEventType,
    pub room_id: OwnedRoomId,
    pub content: serde_json::Value,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub state_key: Option<String>,
    pub prev_events: Vec<EventRef>,
    pub depth: u64,
    pub auth_events: Vec<EventRef>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub redacts: Option<OwnedEventId>,
    #[serde(default, skip_serializing_if = "BTreeMap::is_empty")]
    pub unsigned: BTreeMap<String, serde_json::Value>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub prev_content: Option<serde_json::Value>,
    #[serde(flatten)]
    pub extra: serde_json::Map<String, serde_json::Value>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub signature: Option<Signature>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub hash: Option<String>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Signature {
    sever_name: OwnedServerName,
    key_id: OwnedServerSigningKeyId,
    signature: String,
}

impl Event {
    pub fn to_hierarchy_child_event(&self) -> Result<Raw<HierarchySpaceChildEvent>, EventError> {
        let sync_event = HierarchyChildEvent::from(self);
        let raw_value = serde_json::value::to_raw_value(&sync_event)?;

        Ok(Raw::from_json(raw_value))
    }

    pub fn to_sync_timeline_event(
        mut self,
        id: &Identity,
    ) -> Result<Raw<AnySyncTimelineEvent>, EventError> {
        if let TimelineEventType::RoomRedaction = self.kind {
            let content = self
                .content
                .as_object_mut()
                .ok_or_else(|| EventError::Custom("expect content is an object".to_string()))?;
            let redacts = content
                .get("redacts")
                .and_then(|c| c.as_str())
                .and_then(|event_id| EventId::parse(event_id).ok())
                .or(self.redacts);

            self.redacts = redacts.clone();

            if let Some(redacts) = redacts {
                content.insert(
                    "redacts".to_string(),
                    serde_json::Value::String(redacts.to_string()),
                );
            }
        }

        let maybe_device_tx = self.unsigned.remove("device");
        let device_tx = maybe_device_tx.as_ref().and_then(|d| d.as_str());
        let current_device = id.device().map(|device| device.as_str());

        if device_tx.is_none() || current_device != device_tx {
            self.unsigned.remove("transaction_id");
        }

        let sync_event = RoomSyncEvent {
            content: &self.content,
            kind: &self.kind,
            event_id: self.event_ref.event_id(),
            sender: &self.sender,
            origin_server_ts: self.origin_server_ts,
            unsigned: &self.unsigned,
            prev_content: self.prev_content.as_ref(),
            state_key: self.state_key.as_ref(),
            redacts: self.redacts.as_ref().map(|e| e.as_ref()),
        };

        let raw_value = serde_json::value::to_raw_value(&sync_event)?;

        Ok(Raw::from_json(raw_value))
    }

    pub fn topological_token(&self) -> TopologicalToken {
        let origin_duration = Duration::from_millis(self.origin_server_ts);
        TopologicalToken::new_subsequence(self.depth, origin_duration.as_secs())
    }

    /// Extract relation from event's content
    pub fn relates_to(&self) -> Option<Relation> {
        let content_object = self.content.as_object()?;
        let relates_to = match content_object.get("m.relates_to") {
            Some(value) => value.clone(),
            None => return None,
        };

        let relates_to = serde_json::from_value(relates_to);

        match relates_to {
            Ok(relates) => Some(relates),
            Err(e) => {
                tracing::error!(
                    err = e.to_string().as_str(),
                    "could not deserialize relates-to field",
                );
                None
            }
        }
    }

    pub fn bundle_relations(
        &mut self,
        relation: safareig_core::ruma::events::BundledStateRelations,
    ) {
        match serde_json::to_value(relation) {
            Ok(raw_relation) => {
                self.unsigned
                    .insert("m.relations".to_string(), raw_relation);
            }
            Err(e) => {
                tracing::error!(
                    err = e.to_string().as_str(),
                    "relations could not be encodedand could not be added to unsigned",
                )
            }
        }
    }

    /// Checks if the event should be signed by the given homeserver.
    ///
    /// Right now, we are checking if sender's is the same as the given server name. In the future, when 3pid invites will get implemented, it will also check if the invite is from a 3pid.
    pub fn should_be_signed_by(&self, server_name: &ServerName) -> bool {
        self.sender.server_name() == server_name
    }

    pub fn has_auth_event(&self, event_id: &EventId) -> bool {
        self.auth_events.iter().any(|r| r.event_id() == event_id)
    }

    pub fn has_prev_event(&self, event_id: &EventId) -> bool {
        self.prev_events.iter().any(|r| r.event_id() == event_id)
    }

    pub fn prev_event_ids(&self) -> Vec<OwnedEventId> {
        self.prev_events
            .iter()
            .map(|r| r.event_id().to_owned())
            .collect()
    }

    pub fn auth_event_ids(&self) -> Vec<OwnedEventId> {
        self.auth_events
            .iter()
            .map(|r| r.event_id().to_owned())
            .collect()
    }

    pub fn content_as<E: EventContent + DeserializeOwned>(&self) -> Result<E, RoomError> {
        serde_json::from_value(self.content.clone()).map_err(RoomError::Serde)
    }

    pub fn prev_content_as<E: EventContent + DeserializeOwned>(
        &self,
    ) -> Result<Option<E>, RoomError> {
        self.prev_content
            .as_ref()
            .map(|v| serde_json::from_value(v.clone()).map_err(RoomError::Serde))
            .transpose()
    }

    pub fn redact(
        &self,
        room_version: &RoomVersionId,
        reason: Option<RedactedBecause>,
    ) -> Result<Event, RoomError> {
        // TODO: Use high-level redact

        // Convert event to CanonicalJson
        let value = serde_json::to_value(self)?;
        let canonical: CanonicalJsonObject = serde_json::from_value(value)?;

        // redact event
        let canonical = redact(canonical, room_version, reason)?;

        // rehydrate the canonical json to an Event
        let value = serde_json::to_value(canonical)?;
        let event: Event = serde_json::from_value(value)?;

        Ok(event)
    }
}

impl From<Event> for Content {
    fn from(e: Event) -> Self {
        Content::new(e.content, e.kind.to_string())
    }
}

#[derive(Deserialize, Debug)]
#[serde(tag = "rel_type")]
pub enum Relation {
    #[serde(rename = "m.thread")]
    Thread(Thread),

    #[serde(rename = "m.reference")]
    References(Reference),

    #[serde(other)]
    Unknown,
}

#[derive(Deserialize, Debug)]
pub struct Thread {
    pub event_id: OwnedEventId,

    #[serde(rename = "m.in_reply_to")]
    pub in_reply_to: InReplyTo,

    #[serde(default)]
    pub is_falling_back: bool,
}

#[derive(Serialize)]
struct SyncStateEvent<'a> {
    content: &'a serde_json::Value,
    event_id: &'a EventId,
    origin_server_ts: u64,
    #[serde(skip_serializing_if = "Option::is_none")]
    prev_content: Option<&'a serde_json::Value>,
    sender: &'a UserId,
    state_key: Option<&'a str>,
    #[serde(rename = "type")]
    event_type: &'a TimelineEventType,
    #[serde(default, skip_serializing_if = "BTreeMap::is_empty")]
    unsigned: &'a BTreeMap<String, serde_json::Value>,
}

impl<'a> From<&'a Event> for SyncStateEvent<'a> {
    fn from(event: &'a Event) -> Self {
        SyncStateEvent {
            content: &event.content,
            event_id: event.event_ref.event_id(),
            origin_server_ts: event.origin_server_ts,
            prev_content: event.prev_content.as_ref(),
            sender: event.sender.as_ref(),
            state_key: event.state_key.as_deref(),
            event_type: &event.kind,
            unsigned: &event.unsigned,
        }
    }
}

#[derive(Serialize)]
struct HierarchyChildEvent<'a> {
    content: &'a serde_json::Value,
    origin_server_ts: u64,
    sender: &'a UserId,
    state_key: Option<&'a str>,
    #[serde(rename = "type")]
    kind: &'a TimelineEventType,
}

impl<'a> From<&'a Event> for HierarchyChildEvent<'a> {
    fn from(event: &'a Event) -> Self {
        HierarchyChildEvent {
            content: &event.content,
            origin_server_ts: event.origin_server_ts,
            sender: event.sender.as_ref(),
            state_key: event.state_key.as_deref(),
            kind: &event.kind,
        }
    }
}

#[derive(Serialize)]
struct SyncMessageEvent<'a> {
    content: &'a serde_json::Value,
    event_id: &'a EventId,
    sender: &'a UserId,
    origin_server_ts: u64,
    #[serde(rename = "type")]
    event_type: &'a TimelineEventType,
    #[serde(default, skip_serializing_if = "BTreeMap::is_empty")]
    unsigned: &'a BTreeMap<String, serde_json::Value>,
    room_id: &'a RoomId,
}

impl<'a> From<&'a Event> for SyncMessageEvent<'a> {
    fn from(event: &'a Event) -> Self {
        SyncMessageEvent {
            content: &event.content,
            event_id: event.event_ref.event_id(),
            origin_server_ts: event.origin_server_ts,
            sender: event.sender.as_ref(),
            event_type: &event.kind,
            unsigned: &event.unsigned,
            room_id: event.room_id.as_ref(),
        }
    }
}

#[derive(Serialize)]
struct StateEvent<'a> {
    content: &'a serde_json::Value,
    #[serde(rename = "type")]
    event_type: &'a TimelineEventType,
    event_id: &'a EventId,
    sender: &'a UserId,
    origin_server_ts: u64,
    room_id: &'a RoomId,
    state_key: &'a str,
    #[serde(skip_serializing_if = "Option::is_none")]
    prev_content: Option<&'a serde_json::Value>,
    #[serde(default, skip_serializing_if = "BTreeMap::is_empty")]
    unsigned: &'a BTreeMap<String, serde_json::Value>,
}

impl<'a> From<&'a Event> for StateEvent<'a> {
    fn from(event: &'a Event) -> Self {
        StateEvent {
            content: &event.content,
            event_id: event.event_ref.event_id(),
            origin_server_ts: event.origin_server_ts,
            prev_content: event.prev_content.as_ref(),
            sender: event.sender.as_ref(),
            state_key: event.state_key.as_ref().unwrap(),
            event_type: &event.kind,
            unsigned: &event.unsigned,
            room_id: &event.room_id,
        }
    }
}

#[derive(Serialize)]
struct StrippedStateEvent<'a> {
    content: &'a serde_json::Value,
    #[serde(rename = "type")]
    event_type: &'a TimelineEventType,
    sender: &'a UserId,
    state_key: &'a str,
}

impl<'a> From<&'a Event> for StrippedStateEvent<'a> {
    fn from(event: &'a Event) -> Self {
        StrippedStateEvent {
            content: &event.content,
            sender: event.sender.as_ref(),
            state_key: event.state_key.as_ref().unwrap(),
            event_type: &event.kind,
        }
    }
}

#[derive(Deserialize, Serialize, Debug)]
struct RoomEvent {
    pub content: serde_json::Value,
    #[serde(rename = "type")]
    pub kind: TimelineEventType,
    pub event_id: OwnedEventId,
    pub sender: OwnedUserId,
    pub origin_server_ts: u64,
    #[serde(default, skip_serializing_if = "BTreeMap::is_empty")]
    pub unsigned: BTreeMap<String, serde_json::Value>,
    pub room_id: OwnedRoomId,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub prev_content: Option<serde_json::Value>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub state_key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub redacts: Option<OwnedEventId>,
}

#[derive(Serialize, Debug)]
struct RoomSyncEvent<'a> {
    pub content: &'a serde_json::Value,
    #[serde(rename = "type")]
    pub kind: &'a TimelineEventType,
    pub event_id: &'a EventId,
    pub sender: &'a UserId,
    pub origin_server_ts: u64,
    #[serde(default, skip_serializing_if = "BTreeMap::is_empty")]
    pub unsigned: &'a BTreeMap<String, serde_json::Value>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub prev_content: Option<&'a serde_json::Value>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub state_key: Option<&'a String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub redacts: Option<&'a EventId>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct EventBuilder {
    pub sender: OwnedUserId,
    pub origin_server_ts: u64,
    #[serde(rename = "type")]
    pub kind: TimelineEventType,
    pub room_id: OwnedRoomId,
    pub content: serde_json::Value,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub state_key: Option<String>,
    pub prev_events: Vec<EventRef>,
    pub depth: u64,
    pub auth_events: Vec<EventRef>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub redacts: Option<OwnedEventId>,
    #[serde(default, skip_serializing_if = "BTreeMap::is_empty")]
    pub unsigned: BTreeMap<String, serde_json::Value>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub prev_content: Option<serde_json::Value>,
    #[serde(flatten)]
    pub extra: serde_json::Map<String, serde_json::Value>,
}

impl EventBuilder {
    #[must_use]
    pub fn builder(sender: &UserId, content: Content, room_id: &RoomId) -> Self {
        let (event_type, content) = content.to_parts();

        EventBuilder {
            sender: sender.to_owned(),
            origin_server_ts: system_time_to_u64(SystemTime::now()),
            kind: event_type,
            room_id: room_id.to_owned(),
            content,
            state_key: None,
            prev_events: vec![],
            depth: 0, // TODO: This should probably be an optional
            auth_events: vec![],
            redacts: None,
            unsigned: Default::default(),
            prev_content: None,
            extra: Default::default(),
        }
    }

    #[must_use]
    pub fn state(
        sender: &UserId,
        content: Content,
        room_id: &RoomId,
        state_key: Option<String>,
    ) -> Self {
        let (event_type, content) = content.to_parts();

        EventBuilder {
            sender: sender.to_owned(),
            origin_server_ts: system_time_to_u64(SystemTime::now()),
            kind: event_type,
            room_id: room_id.to_owned(),
            content,
            state_key: Some(state_key.unwrap_or_default()),
            prev_events: vec![],
            depth: 0, // TODO: This should probably be an optional
            auth_events: vec![],
            redacts: None,
            unsigned: Default::default(),
            prev_content: None,
            extra: Default::default(),
        }
    }

    #[must_use]
    pub fn room_id(mut self, room_id: OwnedRoomId) -> EventBuilder {
        self.room_id = room_id;
        self
    }

    #[must_use]
    pub fn prev_events(mut self, prev_events: Vec<EventRef>) -> EventBuilder {
        self.prev_events = prev_events;
        self
    }

    #[must_use]
    pub fn auth_events(mut self, auth_events: Vec<EventRef>) -> EventBuilder {
        self.auth_events = auth_events;
        self
    }

    #[must_use]
    pub fn depth(mut self, depth: u64) -> EventBuilder {
        self.depth = depth;
        self
    }

    #[must_use]
    pub fn state_key(mut self, state_key: Option<String>) -> EventBuilder {
        self.state_key = state_key;
        self
    }

    #[must_use]
    pub fn event_time(mut self, event_time: SystemTime) -> EventBuilder {
        self.origin_server_ts = system_time_to_u64(event_time);
        self
    }

    #[must_use]
    pub fn prev_content(mut self, prev_content: Option<serde_json::Value>) -> EventBuilder {
        self.prev_content = prev_content;
        self
    }

    #[must_use]
    pub fn add_extra_field(mut self, key: String, value: serde_json::Value) -> EventBuilder {
        self.extra.insert(key, value);
        self
    }

    #[must_use]
    pub fn add_unsigned(mut self, key: String, value: serde_json::Value) -> EventBuilder {
        self.unsigned.insert(key, value);
        self
    }

    #[must_use]
    pub fn unsigned(mut self, unsigned: BTreeMap<String, serde_json::Value>) -> EventBuilder {
        self.unsigned = unsigned;
        self
    }

    #[must_use]
    pub fn redacts(mut self, event_id: OwnedEventId) -> EventBuilder {
        self.redacts = Some(event_id);
        self
    }

    #[must_use]
    pub fn transaction(mut self, transaction_id: String, id: &Identity) -> EventBuilder {
        // See: https://spec.matrix.org/v1.4/client-server-api/#types-of-room-events
        // We store the `transaction_id` and the device which generated this event. This will allow us to
        // send the `transaction_id` field only if the syncing/requesting device is the same as the one which
        // generated the event.
        // Device verification/SAS use this field in order to determine if it's a local or remote echo.

        // In the future, we might want to prevent storing this fields in the unsigned data and use a cache in order to
        // store the transaction <-> event relations. This data is mostly ephemeral and we might not want to
        // store it in long-term storage.

        self = self.add_unsigned(
            "transaction_id".to_string(),
            serde_json::Value::String(transaction_id),
        );

        if let Identity::Device(device, _) = id {
            self = self.add_unsigned(
                "device".to_string(),
                serde_json::Value::String(device.to_string()),
            );
        }

        self
    }

    // TODO: EventError
    pub fn build(self, event_id_calculator: &dyn EventIdCalculation) -> Result<Event, RoomError> {
        let id = event_id_calculator.calculate_event_id(&self)?;

        if self.prev_events.is_empty() && self.depth > 1 {
            return Err(RoomError::Custom(
                "Missing prev events with depth > 1".to_string(),
            ));
        }

        // TODO: Check prev_events < 20

        if self.auth_events.is_empty() && self.depth > 1 {
            return Err(RoomError::Custom(
                "Missing auth events with depth > 1".to_string(),
            ));
        }

        // TODO: If event kind is a state event, we should validate that it contains a state key

        let event = Event {
            event_ref: id,
            sender: self.sender,
            origin_server_ts: self.origin_server_ts,
            kind: self.kind,
            room_id: self.room_id,
            content: self.content,
            state_key: self.state_key,
            prev_events: self.prev_events,
            depth: self.depth,
            auth_events: self.auth_events,
            redacts: self.redacts,
            unsigned: self.unsigned,
            prev_content: self.prev_content,
            extra: self.extra,
            hash: None,
            signature: None,
        };

        Ok(event)
    }
}

impl From<&Event> for EventBuilder {
    fn from(event: &Event) -> Self {
        EventBuilder {
            sender: event.sender.to_owned(),
            origin_server_ts: event.origin_server_ts,
            kind: event.kind.clone(),
            room_id: event.room_id.to_owned(),
            content: event.content.clone(),
            state_key: event.state_key.clone(),
            prev_events: event.prev_events.clone(),
            depth: event.depth,
            auth_events: event.auth_events.clone(),
            redacts: event.redacts.clone(),
            unsigned: event.unsigned.clone(),
            prev_content: event.prev_content.clone(),
            extra: event.extra.clone(),
        }
    }
}

pub trait EventContentExt {
    fn into_content(self) -> Result<Content, EventError>;
}

impl<E> EventContentExt for E
where
    E: EventContent,
    <E as EventContent>::EventType: std::fmt::Display,
{
    fn into_content(self) -> Result<Content, EventError> {
        Content::from_event_content(&self)
    }
}
/// Converts an event to a raw PDU. If the sender is from the current homeserver and it does not contain the required signatures, it will sign it with the current key.
pub fn event_to_raw_pdu<K: KeyPair>(
    event: &Event,
    server_name: &ServerName,
    room_version: &RoomVersionId,
    key: &K,
) -> Result<Raw<RumaPdu>, EventError> {
    if !event.should_be_signed_by(server_name) {
        let mut value = serde_json::to_value(event)?;
        value.as_object_mut().map(|obj| {
            obj.remove("event_id");

            obj
        });
        return Ok(Raw::from_json(
            to_raw_value(&value).map_err(|_| EventError::CanonicalJsonConversion)?,
        ));
    }

    let canonical_json: CanonicalJsonValue = serde_json::to_value(event)
        .map_err(|_| EventError::CanonicalJsonConversion)?
        .try_into()
        .map_err(|_| EventError::CanonicalJsonConversion)?;

    let mut canonical_json = canonical_json
        .into_object()
        .ok_or(EventError::CanonicalJsonConversion)?;
    // PDU v3+ does no accept an event_id field.
    // TODO: (I think that) Sentence above is false. Synapse and Dendrite DOES NOT allow it, but the
    // spec is fine with this.
    if !(room_version == &RoomVersionId::V1 || room_version == &RoomVersionId::V2) {
        canonical_json.remove("event_id");
    }

    hash_and_sign_event(server_name.as_str(), key, &mut canonical_json, room_version).map_err(
        |e| {
            tracing::error!("Could not sign pdu: {}", e);
            EventError::SigningError
        },
    )?;

    let raw = to_raw_value(&canonical_json).map_err(|_| EventError::CanonicalJsonConversion)?;

    Ok(Raw::from_json(raw))
}

pub fn convert_to_raw<T: serde::Serialize, O>(event: &T) -> Option<Raw<O>> {
    serde_json::value::to_raw_value(&event)
        .map(|raw| Raw::<T>::from_json(raw))
        .map(|raw| raw.cast::<O>())
        .ok()
}

pub async fn pdu_list_to_event(event_formatter: &dyn EventFormat, pdus: &[Raw<Pdu>]) -> Vec<Event> {
    let event_results: Vec<_> = futures::stream::iter(pdus.iter())
        .then(|e| event_formatter.pdu_to_event(e))
        .collect()
        .await;

    event_results
        .into_iter()
        .filter_map(|r| match r {
            Ok(event) => Some(event),
            Err(error) => {
                tracing::error!(
                    ?error,
                    "ignoring event because it could not be parsed and verified",
                );

                None
            }
        })
        .collect()
}

pub trait RelationExt {
    fn relates_to_event(&self) -> Option<&EventId>;
}

impl<C> RelationExt for safareig_core::ruma::events::room::message::Relation<C> {
    fn relates_to_event(&self) -> Option<&EventId> {
        match self {
            safareig_core::ruma::events::room::message::Relation::Thread(thread) => {
                Some(thread.event_id.as_ref())
            }
            _ => None,
        }
    }
}

#[cfg(test)]
mod tests {
    use std::{
        sync::Arc,
        time::{Duration, SystemTime},
    };

    use safareig_core::{
        events::Content,
        ruma::{
            events::{
                pdu::Pdu,
                room::{
                    message::{AddMentions, ReplyWithinThread},
                    redaction::RoomRedactionEventContent,
                },
                AnyTimelineEvent,
            },
            serde::Raw,
            EventId, UserId,
        },
    };
    use safareig_federation::keys::{
        storage::{Key, KeyType, ServerKeyStorage},
        KeyProvider,
    };

    use super::EventBuilder;
    use crate::{
        client::room::versions::EventIdV3,
        core::events::{formatter::EventFormatterV1, pdu_list_to_event},
        testing::TestingDB,
    };

    #[tokio::test]
    async fn convert_v1_pdu_to_events() {
        let db = TestingDB::default();
        let key_storage = db.container.service::<Arc<dyn ServerKeyStorage>>();

        let server_name = safareig_core::ruma::server_name!("localhost:41693");
        let key = Key {
            content: base64::decode("ilcbMVvh3GLQHxbXwo154Uk+JXmcCrZ/14nu8GGPHsU")
                .unwrap()
                .to_vec(),
            key_type: KeyType::PublicKey,
            id: safareig_core::ruma::server_signing_key_id!("ed25519:1").to_owned(),
            expire: None,
            valid_until_ts: Some(SystemTime::now() + Duration::from_secs(3600)),
        };
        let _ = key_storage.add_server_key(server_name, &key).await;

        let pdu_list = r#"
        [
            {
                "content": {
                    "creator": "@__ANON__-0:localhost:41693"
                },
                "type": "m.room.create",
                "state_key": "",
                "auth_events": [],
                "depth": 0,
                "signatures": {
                    "localhost:41693": {
                        "ed25519:1": "ptuoa+GdxokVoLo8Tg+Jat/EBZ8lbrAx9W2Tf1jLXn/+hIhbYJAWHF+WtZ/zZyPcMpyVgBujm6Cf044UrCdWDw"
                    }
                },
                "sender": "@__ANON__-0:localhost:41693",
                "event_id": "$0:localhost:41693",
                "hashes": {
                    "sha256": "KPC03sfF262Rv/28NwPJKevxtCqV+XTxRaNHWCxrr8Q"
                },
                "prev_events": [],
                "room_id": "!0:localhost:41693",
                "origin": "localhost:41693",
                "origin_server_ts": 1640795824242
            },
            {
                "hashes": {
                    "sha256": "SSL94+hjvKh83/tX3Wv1F1hwpPGzil7uscWYkxnw+3M"
                },
                "event_id": "$1:localhost:41693",
                "origin_server_ts": 1640795824243,
                "origin": "localhost:41693",
                "room_id": "!0:localhost:41693",
                "prev_events": [
                    [
                        "$0:localhost:41693",
                        {
                            "sha256": "KPC03sfF262Rv/28NwPJKevxtCqV+XTxRaNHWCxrr8Q"
                        }
                    ]
                ],
                "state_key": "@__ANON__-0:localhost:41693",
                "auth_events": [
                    [
                        "$0:localhost:41693",
                        {
                            "sha256": "KPC03sfF262Rv/28NwPJKevxtCqV+XTxRaNHWCxrr8Q"
                        }
                    ]
                ],
                "type": "m.room.member",
                "content": {
                    "membership": "join"
                },
                "signatures": {
                    "localhost:41693": {
                        "ed25519:1": "TqX/WAp0/tcUCmeuF7VZjlIVRJcPr5QvuT9Hgz7d3+zVLJyxixePKVDb7ML+oZeJynmpqfFmiOO0qBRT0Nf4AQ"
                    }
                },
                "sender": "@__ANON__-0:localhost:41693",
                "depth": 1
            },
            {
                "origin_server_ts": 1640795824245,
                "origin": "localhost:41693",
                "prev_events": [
                    [
                        "$1:localhost:41693",
                        {
                            "sha256": "SSL94+hjvKh83/tX3Wv1F1hwpPGzil7uscWYkxnw+3M"
                        }
                    ]
                ],
                "room_id": "!0:localhost:41693",
                "hashes": {
                    "sha256": "xiGYKomwMaQVFRKSeqfWfETymY0AYVniXrwH0k4mbb0"
                },
                "event_id": "$2:localhost:41693",
                "signatures": {
                    "localhost:41693": {
                        "ed25519:1": "Y5K+oZ+33ePOiii3bySXW1HG5K8Rr9Wd31AeWA1Hn9gbdbKGpSyIhEj9MERftJN9PMzAsgtkyjEPrrI+qjROBQ"
                    }
                },
                "sender": "@__ANON__-0:localhost:41693",
                "depth": 2,
                "auth_events": [
                    [
                        "$0:localhost:41693",
                        {
                            "sha256": "KPC03sfF262Rv/28NwPJKevxtCqV+XTxRaNHWCxrr8Q"
                        }
                    ],
                    [
                        "$1:localhost:41693",
                        {
                            "sha256": "SSL94+hjvKh83/tX3Wv1F1hwpPGzil7uscWYkxnw+3M"
                        }
                    ]
                ],
                "type": "m.room.join_rules",
                "state_key": "",
                "content": {
                    "join_rule": "public"
                }
            }
        ]"#;
        let pdus: Vec<Raw<Pdu>> = serde_json::from_str(pdu_list).unwrap();

        let key_provider = db.container.service::<Arc<KeyProvider>>();
        let events = pdu_list_to_event(
            &EventFormatterV1::new(key_provider, db.server_name().clone()),
            pdus.as_ref(),
        )
        .await;

        assert_eq!("$0:localhost:41693", events[0].event_ref.event_id());
        assert_eq!("$1:localhost:41693", events[1].event_ref.event_id());
        assert_eq!("$2:localhost:41693", events[2].event_ref.event_id());
    }

    #[test]
    fn it_extract_relation_from_event_with_relation() {
        use safareig_core::ruma::{
            events::room::message::{OriginalRoomMessageEvent, RoomMessageEventContent},
            UserId,
        };

        use super::{Content, EventBuilder};
        use crate::client::room::versions::EventIdV3;

        let server = safareig_core::ruma::server_name!("localhost:8448");
        let message_content = RoomMessageEventContent::text_plain("test");
        let content = Content::from_event_content(&message_content).unwrap();
        let room_id = safareig_core::ruma::RoomId::new(server);
        let event_id_calc = EventIdV3;
        let user_id = UserId::parse("@a:localhost:8448").unwrap();
        let event = EventBuilder::builder(&user_id, content, &room_id)
            .build(&event_id_calc)
            .unwrap();

        let v = serde_json::to_value(event).unwrap();
        let original: OriginalRoomMessageEvent = serde_json::from_value(v).unwrap();

        let message_content = RoomMessageEventContent::text_plain("test").make_for_thread(
            &original,
            ReplyWithinThread::Yes,
            AddMentions::No,
        );

        let content = Content::from_event_content(&message_content).unwrap();
        let room_id = safareig_core::ruma::RoomId::new(server);
        let event_id_calc = EventIdV3;
        let user_id = UserId::parse("@a:localhost:8448").unwrap();
        let event = EventBuilder::builder(&user_id, content, &room_id)
            .build(&event_id_calc)
            .unwrap();

        assert!(event.relates_to().is_some());
    }

    #[test]
    fn it_does_not_extract_relation_from_event_withhout_relations() {
        use safareig_core::ruma::{events::room::message::RoomMessageEventContent, UserId};

        use super::{Content, EventBuilder};
        use crate::client::room::versions::EventIdV3;

        let server = safareig_core::ruma::server_name!("localhost:8448");
        let message_content = RoomMessageEventContent::text_plain("test");

        let content = Content::from_event_content(&message_content).unwrap();
        let room_id = safareig_core::ruma::RoomId::new(server);
        let event_id_calc = EventIdV3;
        let user_id = UserId::parse("@a:localhost:8448").unwrap();
        let event = EventBuilder::builder(&user_id, content, &room_id)
            .build(&event_id_calc)
            .unwrap();

        assert!(event.relates_to().is_none());
    }

    #[test]
    fn old_redact_event_projects_redacts_field_to_both_event_and_content() {
        let content = RoomRedactionEventContent::new_v1().with_reason("redacted".to_string());
        let user_id = UserId::parse("@a:localhost:8448").unwrap();
        let server = safareig_core::ruma::server_name!("localhost:8448");
        let room_id = safareig_core::ruma::RoomId::new(server);
        let event_id_calc = EventIdV3;
        let event_id = EventId::parse("$1:localhost:41693").unwrap();

        let event = EventBuilder::builder(
            &user_id,
            Content::from_event_content(&content).unwrap(),
            &room_id,
        )
        .redacts(event_id.clone())
        .build(&event_id_calc)
        .unwrap();

        let timeline = Raw::<AnyTimelineEvent>::try_from(event).unwrap();
        let raw_json = timeline.json().get();
        let raw_values: serde_json::Value = serde_json::from_str(raw_json).unwrap();

        let redacts_event_level = raw_values
            .as_object()
            .unwrap()
            .get("redacts")
            .unwrap()
            .as_str()
            .unwrap();
        assert_eq!(redacts_event_level, event_id.as_str());
        let redacts_content = raw_values
            .as_object()
            .unwrap()
            .get("content")
            .unwrap()
            .as_object()
            .unwrap()
            .get("redacts")
            .unwrap()
            .as_str()
            .unwrap();
        assert_eq!(redacts_event_level, event_id.as_str());
        assert_eq!(redacts_content, event_id.as_str());
    }

    #[test]
    fn new_redact_event_projects_redacts_field_to_both_event_and_content() {
        let event_id = EventId::parse("$1:localhost:41693").unwrap();
        let content = RoomRedactionEventContent::new_v11(event_id.clone())
            .with_reason("redacted".to_string());
        let user_id = UserId::parse("@a:localhost:8448").unwrap();
        let server = safareig_core::ruma::server_name!("localhost:8448");
        let room_id = safareig_core::ruma::RoomId::new(server);
        let event_id_calc = EventIdV3;

        let event = EventBuilder::builder(
            &user_id,
            Content::from_event_content(&content).unwrap(),
            &room_id,
        )
        .build(&event_id_calc)
        .unwrap();

        let timeline = Raw::<AnyTimelineEvent>::try_from(event).unwrap();
        let raw_json = timeline.json().get();
        let raw_values: serde_json::Value = serde_json::from_str(raw_json).unwrap();

        let redacts_event_level = raw_values
            .as_object()
            .unwrap()
            .get("redacts")
            .unwrap()
            .as_str()
            .unwrap();
        assert_eq!(redacts_event_level, event_id.as_str());
        let redacts_content = raw_values
            .as_object()
            .unwrap()
            .get("content")
            .unwrap()
            .as_object()
            .unwrap()
            .get("redacts")
            .unwrap()
            .as_str()
            .unwrap();
        assert_eq!(redacts_event_level, event_id.as_str());
        assert_eq!(redacts_content, event_id.as_str());
    }
}
