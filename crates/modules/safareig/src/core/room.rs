use std::{
    collections::{HashMap, HashSet},
    sync::Arc,
};

use bitflags::bitflags;
use itertools::Itertools;
use safareig_core::{
    bus::{Bus, BusMessage},
    events::{Content, EventError},
    ruma::{
        api::client::room::{
            create_room::v3::{CreationContent, RoomPreset},
            Visibility,
        },
        events::{
            pdu::Pdu,
            room::{
                canonical_alias::RoomCanonicalAliasEventContent,
                create::RoomCreateEventContent,
                guest_access::{GuestAccess, RoomGuestAccessEventContent},
                history_visibility::{HistoryVisibility, RoomHistoryVisibilityEventContent},
                join_rules::{JoinRule, RoomJoinRulesEventContent},
                member::MembershipState,
                name::RoomNameEventContent,
                power_levels::RoomPowerLevelsEventContent,
                topic::RoomTopicEventContent,
            },
            TimelineEventType,
        },
        serde::Raw,
        signatures::Ed25519KeyPair,
        Int, OwnedRoomAliasId, OwnedRoomId, OwnedUserId, RoomId, RoomVersionId, UserId,
    },
    storage::StorageError,
    Inject,
};
use safareig_rooms::alias::storage::{AliasStorage, RoomAlias};
use safareig_users::profile::{member_event_with_profile, Profile, ProfileLoader};
use serde::{Deserialize, Serialize};
use serde_json::Value;
use tracing::instrument;

use super::events::{
    auth::{AuthChain, AuthError},
    EventBuilder, EventContentExt,
};
use crate::{
    client::room::{
        events::{CreateRoomExtra, SignError},
        loader::RoomLoader,
        state::{
            ruma_stateres::{ResolutionError, StateResParameters},
            RoomState,
        },
        versions::EventIdCalculation,
        RoomError,
    },
    core::events::Event,
    federation::room::PduTemplate,
    storage::{EventOptions, EventStorage, Membership, RoomStorage, RoomStreamStorage},
};

#[derive(Serialize, Deserialize)]
pub struct RoomDescriptor {
    pub room_id: OwnedRoomId,
    pub encoding: EventEncoding,
    pub room_version: RoomVersionId,
}

#[derive(Serialize, Deserialize, Debug)]
pub enum EventEncoding {
    Json,
}

#[derive(Default)]
pub struct RoomVersionRegistry {
    room_versions: HashMap<RoomVersionId, RoomVersion>,
}

impl RoomVersionRegistry {
    pub fn register(&mut self, room_version: RoomVersion) {
        self.room_versions
            .insert(room_version.id().to_owned(), room_version);
    }

    pub fn get(&self, room_version_id: &RoomVersionId) -> Option<&RoomVersion> {
        self.room_versions.get(room_version_id)
    }

    pub fn get_supported_versions(&self) -> Vec<RoomVersionId> {
        self.room_versions.keys().cloned().collect_vec()
    }

    pub fn is_supported(&self, room_version_id: &RoomVersionId) -> bool {
        self.get(room_version_id).is_some()
    }
}

#[derive(Clone)]
pub struct RoomVersion {
    room_version_id: RoomVersionId,
    state_res: Arc<dyn StateResolutionAlgorithm>,
    authorizer: Arc<dyn Authorizer>,
    event_id: Arc<dyn EventIdCalculation>,
    event_format: Arc<dyn EventFormat>,
    room_options: RoomOptions,
}

impl RoomVersion {
    pub fn new(
        room_version_id: RoomVersionId,
        state_res: Arc<dyn StateResolutionAlgorithm>,
        authorizer: Arc<dyn Authorizer>,
        event_id: Arc<dyn EventIdCalculation>,
        pdu_formatter: Arc<dyn EventFormat>,
        room_options: RoomOptions,
    ) -> RoomVersion {
        RoomVersion {
            room_version_id,
            state_res,
            authorizer,
            event_id,
            event_format: pdu_formatter,
            room_options,
        }
    }

    pub fn id(&self) -> &RoomVersionId {
        &self.room_version_id
    }

    pub fn state_resolution(&self) -> &dyn StateResolutionAlgorithm {
        self.state_res.as_ref()
    }

    pub fn authorizer(&self) -> &dyn Authorizer {
        self.authorizer.as_ref()
    }

    pub fn event_id_calculator(&self) -> &dyn EventIdCalculation {
        self.event_id.as_ref()
    }

    pub fn event_format(&self) -> &dyn EventFormat {
        self.event_format.as_ref()
    }

    pub fn redaction_shape(&self) -> RedactionShape {
        if self.room_options.contains(RoomOptions::REDACTION_CONTENT) {
            RedactionShape::EventIdInContent
        } else {
            RedactionShape::EventIdInTopLevel
        }
    }

    pub fn create_room_shape(&self) -> CreateContentShape {
        if self
            .room_options
            .contains(RoomOptions::ROOM_CREATOR_AS_SENDER)
        {
            CreateContentShape::CreatorInSender
        } else {
            CreateContentShape::CreatorInContent
        }
    }
}

bitflags! {
    #[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub struct RoomOptions: u32 {
        const REDACTION_CONTENT = 0b00000001;
        const ROOM_CREATOR_AS_SENDER = 0b00000010;
        const PRE_V11 = 0;
        const POST_V11 = Self::REDACTION_CONTENT.bits() | Self::ROOM_CREATOR_AS_SENDER.bits();
    }
}

pub enum RedactionShape {
    EventIdInTopLevel,
    EventIdInContent,
}

pub enum CreateContentShape {
    CreatorInContent,
    CreatorInSender,
}

pub trait Authorizer: Send + Sync {
    fn authorize(&self, auth_chain: &AuthChain, event: &Event) -> Result<(), AuthError>;
}

#[async_trait::async_trait]
pub trait StateResolutionAlgorithm: Send + Sync {
    async fn resolve(&self, params: StateResParameters) -> Result<RoomState, ResolutionError>;
}

pub struct StateResolutionV1;

#[async_trait::async_trait]
// TODO: Implementation for v1 state resolution
impl StateResolutionAlgorithm for StateResolutionV1 {
    async fn resolve(&self, _params: StateResParameters) -> Result<RoomState, ResolutionError> {
        Err(ResolutionError::Unknown)
    }
}

#[async_trait::async_trait]
pub trait EventFormat: Send + Sync {
    fn event_to_pdu(
        &self,
        event: &Event,
        current_key: &Ed25519KeyPair,
    ) -> Result<Raw<Pdu>, EventError>;

    fn event_to_pdu_template(&self, event: Event) -> PduTemplate;
    async fn pdu_to_event(&self, pdu: &Raw<Pdu>) -> Result<Event, SignError>;
}

pub struct CreateRoomParameters {
    creator: OwnedUserId,
    room_id: OwnedRoomId,
    room_version: RoomVersion,
    power_levels: Option<RoomPowerLevelsEventContent>,
    visibility: Visibility,
    preset: Preset,
    initial_events: HashMap<TimelineEventType, EventBuilder>,
    join: HashSet<OwnedUserId>,
    invites: HashSet<OwnedUserId>,
    room_name: Option<String>,
    room_topic: Option<String>,
    create_content: Option<Raw<CreationContent>>,
    is_direct: bool,
    alias: Option<OwnedRoomAliasId>,
}

impl CreateRoomParameters {
    pub fn new(creator: OwnedUserId, room_version: RoomVersion, room_id: OwnedRoomId) -> Self {
        Self {
            creator,
            room_id,
            room_version,
            power_levels: Default::default(),
            visibility: Default::default(),
            preset: Preset(RoomPreset::PrivateChat),
            initial_events: HashMap::new(),
            join: HashSet::new(),
            invites: HashSet::new(),
            room_name: Default::default(),
            room_topic: Default::default(),
            create_content: Default::default(),
            is_direct: false,
            alias: Default::default(),
        }
    }

    pub fn with_power_levels(mut self, content: RoomPowerLevelsEventContent) -> Self {
        self.power_levels = Some(content);
        self
    }

    pub fn with_visibility(mut self, visibility: Visibility) -> Self {
        self.visibility = visibility;
        self
    }

    pub fn with_preset(mut self, preset: RoomPreset) -> Self {
        self.preset = Preset(preset);
        self
    }

    pub fn with_room_name(mut self, name: String) -> Self {
        self.room_name = Some(name);
        self
    }

    pub fn with_room_topic(mut self, topic: String) -> Self {
        self.room_topic = Some(topic);
        self
    }

    pub fn with_creation_content(mut self, creation: Raw<CreationContent>) -> Self {
        self.create_content = Some(creation);
        self
    }

    pub fn with_is_direct(mut self, is_direct: bool) -> Self {
        self.is_direct = is_direct;
        self
    }

    pub fn with_alias(mut self, alias: OwnedRoomAliasId) -> Self {
        self.alias = Some(alias);
        self
    }

    pub fn add_initial_event(mut self, event_builder: EventBuilder) -> Self {
        self.initial_events
            .insert(event_builder.kind.clone(), event_builder);
        self
    }

    pub fn add_join_user(mut self, user_id: OwnedUserId) -> Self {
        self.join.insert(user_id);
        self
    }

    pub fn add_invite_user(mut self, user_id: OwnedUserId) -> Self {
        self.invites.insert(user_id);
        self
    }
}

#[derive(Clone, Inject)]
pub struct RoomCreator {
    room_storage: Arc<dyn RoomStorage>,
    room_loader: RoomLoader,
    profile_loader: ProfileLoader,
    alias: Arc<dyn AliasStorage>,
}

impl RoomCreator {
    pub async fn create_room(&self, parameters: CreateRoomParameters) -> Result<(), RoomError> {
        if let Some(alias) = &parameters.alias {
            self.store_alias(&parameters, alias.to_owned()).await?;
        }

        self.store_descriptor(&parameters).await?;

        let room = self
            .room_loader
            .get(&parameters.room_id)
            .await?
            .ok_or_else(|| RoomError::RoomNotFound(parameters.room_id.to_string()))?;

        let events = self.create_room_events(parameters).await?;

        for e in events {
            room.add_state(e).await?;
        }

        Ok(())
    }

    async fn store_descriptor(
        &self,
        parameters: &CreateRoomParameters,
    ) -> Result<(), StorageError> {
        let descriptor = RoomDescriptor {
            room_id: parameters.room_id.to_owned(),
            encoding: EventEncoding::Json,
            room_version: parameters.room_version.room_version_id.to_owned(),
        };

        self.room_storage
            .update_room_descriptor(&descriptor)
            .await?;

        Ok(())
    }

    async fn store_alias(
        &self,
        parameters: &CreateRoomParameters,
        alias: OwnedRoomAliasId,
    ) -> Result<(), RoomError> {
        let room_alias = RoomAlias {
            alias,
            room_id: parameters.room_id.to_owned(),
            creator: parameters.creator.to_owned(),
        };

        self.alias
            .set_alias(room_alias, false)
            .await
            .map_err(|_| RoomError::AliasInUse)?;

        Ok(())
    }

    async fn create_room_events(
        &self,
        parameters: CreateRoomParameters,
    ) -> Result<Vec<EventBuilder>, RoomError> {
        let mut events = Vec::new();

        let mut users: Vec<OwnedUserId> = parameters
            .join
            .iter()
            .cloned()
            .chain(parameters.invites.iter().cloned())
            .collect();
        users.push(parameters.creator.clone());

        let profiles = self.profile_loader.load_multiple(&users).await?;

        events.push(self.create_room_event(&parameters)?);
        events.push(self.creator_room_member(&parameters, &profiles)?);
        events.push(self.power_level(&parameters)?);
        events.push(self.custom_visibility(&parameters)?);
        self.add_preset_events(&parameters, &mut events)?;

        if let Some(room_name) = &parameters.room_name {
            if !parameters
                .initial_events
                .contains_key(&TimelineEventType::RoomName)
            {
                events.push(self.room_name(&parameters, room_name)?);
            }
        }

        if let Some(room_topic) = &parameters.room_topic {
            if !parameters
                .initial_events
                .contains_key(&TimelineEventType::RoomTopic)
            {
                events.push(self.room_topic(&parameters, room_topic)?);
            }
        }

        let invites = parameters.invites.clone();
        let joins = parameters.join.clone();
        let creator = parameters.creator.clone();
        let room_id = parameters.room_id.clone();
        let is_direct = parameters.is_direct;
        let preset = parameters.preset.clone();
        let alias = parameters.alias.clone();

        for (_, initial_event) in parameters.initial_events {
            events.push(initial_event);
        }

        if let Some(alias) = alias {
            let content = RoomCanonicalAliasEventContent {
                alias: Some(alias),
                alt_aliases: Default::default(),
            };

            events.push(EventBuilder::state(
                &parameters.creator,
                content.into_content()?,
                &parameters.room_id,
                None,
            ));
        }

        for invite in invites {
            let invite_event = self.invite(
                invite.as_ref(),
                creator.as_ref(),
                &room_id,
                is_direct,
                &profiles,
            )?;
            events.push(invite_event);
        }

        for join in joins {
            let join_events = self.join_events(
                join.as_ref(),
                creator.as_ref(),
                &room_id,
                is_direct,
                &profiles,
                &preset,
            )?;
            events.extend(join_events.into_iter());
        }

        Ok(events)
    }

    fn create_room_event(
        &self,
        parameters: &CreateRoomParameters,
    ) -> Result<EventBuilder, RoomError> {
        let mut content = match parameters.room_version.create_room_shape() {
            CreateContentShape::CreatorInContent => {
                RoomCreateEventContent::new_v1(parameters.creator.to_owned())
            }
            CreateContentShape::CreatorInSender => RoomCreateEventContent::new_v11(),
        };
        content.room_version = parameters.room_version.room_version_id.to_owned();

        // TODO: simplify this after Ruma upgrade
        let mut content_as_json = serde_json::to_value(&content)?;

        if let Some(extra_content) = &parameters.create_content {
            let extra_content_value =
                extra_content.deserialize_as::<serde_json::Map<String, Value>>()?;

            if let Some(content) = content_as_json.as_object_mut() {
                for (key, value) in extra_content_value {
                    if !content.contains_key(&key) {
                        content.insert(key, value);
                    }
                }
            }
        }

        let content: Raw<serde_json::Value> = Raw::new(&content_as_json)?;
        let content: Raw<RoomCreateEventContent> = Raw::from_json(content.into_json());
        let content =
            Content::from_raw_event_content(TimelineEventType::RoomCreate.to_string(), &content)?;

        Ok(EventBuilder::state(
            &parameters.creator,
            content,
            &parameters.room_id,
            None,
        ))
    }

    fn creator_room_member(
        &self,
        parameters: &CreateRoomParameters,
        profiles: &HashMap<OwnedUserId, Profile>,
    ) -> Result<EventBuilder, RoomError> {
        let profile = profiles.get(&parameters.creator);
        let content = member_event_with_profile(MembershipState::Join, profile);

        Ok(EventBuilder::state(
            &parameters.creator,
            content.into_content()?,
            &parameters.room_id,
            Some(parameters.creator.to_string()),
        ))
    }

    fn power_level(&self, parameters: &CreateRoomParameters) -> Result<EventBuilder, RoomError> {
        let content = match parameters.power_levels.clone() {
            Some(content) => content,
            None => {
                let mut content = RoomPowerLevelsEventContent::default();
                content
                    .users
                    .insert(parameters.creator.to_owned(), Int::new(100).unwrap());

                content
            }
        };

        Ok(EventBuilder::state(
            &parameters.creator,
            content.into_content()?,
            &parameters.room_id,
            None,
        ))
    }

    fn custom_visibility(
        &self,
        parameters: &CreateRoomParameters,
    ) -> Result<EventBuilder, RoomError> {
        let content = CreateRoomExtra {
            visibility: parameters.visibility.clone(),
        };

        Ok(EventBuilder::state(
            &parameters.creator,
            content.into_content()?,
            &parameters.room_id,
            None,
        ))
    }

    fn room_name(
        &self,
        parameters: &CreateRoomParameters,
        room_name: &str,
    ) -> Result<EventBuilder, EventError> {
        let mut room_name = room_name.to_string();
        room_name.truncate(255);
        let content = RoomNameEventContent::new(room_name);

        Ok(EventBuilder::state(
            &parameters.creator,
            content.into_content()?,
            &parameters.room_id,
            None,
        ))
    }

    fn room_topic(
        &self,
        parameters: &CreateRoomParameters,
        topic: &str,
    ) -> Result<EventBuilder, EventError> {
        let content = RoomTopicEventContent {
            topic: topic.to_string(),
        };

        Ok(EventBuilder::state(
            &parameters.creator,
            content.into_content()?,
            &parameters.room_id,
            None,
        ))
    }

    fn add_preset_events(
        &self,
        parameters: &CreateRoomParameters,
        events: &mut Vec<EventBuilder>,
    ) -> Result<(), EventError> {
        let content = RoomJoinRulesEventContent::new(parameters.preset.join_rule());
        let join_rules = EventBuilder::state(
            &parameters.creator,
            content.into_content()?,
            &parameters.room_id,
            None,
        );
        events.push(join_rules);

        let content =
            RoomHistoryVisibilityEventContent::new(parameters.preset.history_visibility());
        let history = EventBuilder::state(
            &parameters.creator,
            content.into_content()?,
            &parameters.room_id,
            None,
        );
        events.push(history);

        let content = RoomGuestAccessEventContent::new(parameters.preset.guest_access());
        let guest = EventBuilder::state(
            &parameters.creator,
            content.into_content()?,
            &parameters.room_id,
            None,
        );
        events.push(guest);

        Ok(())
    }

    fn invite(
        &self,
        user: &UserId,
        creator: &UserId,
        room_id: &RoomId,
        is_direct: bool,
        profiles: &HashMap<OwnedUserId, Profile>,
    ) -> Result<EventBuilder, EventError> {
        let is_direct = if is_direct { Some(true) } else { None };

        let profile = profiles.get(user);
        let mut content = member_event_with_profile(MembershipState::Invite, profile);
        content.is_direct = is_direct;

        Ok(EventBuilder::state(
            creator,
            content.into_content()?,
            room_id,
            Some(user.to_string()),
        ))
    }

    fn join_events(
        &self,
        user: &UserId,
        creator: &UserId,
        room_id: &RoomId,
        is_direct: bool,
        profiles: &HashMap<OwnedUserId, Profile>,
        preset: &Preset,
    ) -> Result<Vec<EventBuilder>, EventError> {
        let mut events = Vec::new();
        let is_direct = if is_direct { Some(true) } else { None };

        match preset.join_rule() {
            JoinRule::Public => {
                let profile = profiles.get(user);
                let mut content = member_event_with_profile(MembershipState::Join, profile);
                content.is_direct = is_direct;

                let join_event = EventBuilder::state(
                    user,
                    content.into_content()?,
                    room_id,
                    Some(user.to_string()),
                );
                events.push(join_event);
            }
            JoinRule::Invite => {
                let profile = profiles.get(user);
                let mut content = member_event_with_profile(MembershipState::Invite, profile);
                content.is_direct = is_direct;

                let invite_event = EventBuilder::state(
                    creator,
                    content.into_content()?,
                    room_id,
                    Some(user.to_string()),
                );
                events.push(invite_event);

                let mut content = member_event_with_profile(MembershipState::Join, profile);
                content.is_direct = is_direct;

                let join_event = EventBuilder::state(
                    user,
                    content.into_content()?,
                    room_id,
                    Some(user.to_string()),
                );
                events.push(join_event);
            }
            _ => (),
        };

        Ok(events)
    }
}

#[derive(Clone, Debug)]
struct Preset(RoomPreset);

impl Preset {
    pub fn history_visibility(&self) -> HistoryVisibility {
        HistoryVisibility::Shared
    }

    pub fn join_rule(&self) -> JoinRule {
        match self.0 {
            RoomPreset::PrivateChat | RoomPreset::TrustedPrivateChat => JoinRule::Invite,
            RoomPreset::PublicChat => JoinRule::Public,
            _ => JoinRule::Invite,
        }
    }

    pub fn guest_access(&self) -> GuestAccess {
        match self.0 {
            RoomPreset::PrivateChat | RoomPreset::TrustedPrivateChat => GuestAccess::CanJoin,
            RoomPreset::PublicChat => GuestAccess::Forbidden,
            _ => GuestAccess::Forbidden,
        }
    }
}

/// There are some member events which needs to be handled outside of the room graph, like leave (rejection) and
/// invites to remote rooms.
/// Even if they are not added to the room's graph (the local homeserver do not have access to room's state), they still
/// need to be added to event's stream and update the membership of those users
#[derive(Clone, Inject)]
pub struct OutOfRoomMemberEvents {
    room_stream: Arc<dyn RoomStreamStorage>,
    event_storage: Arc<dyn EventStorage>,
    room_storage: Arc<dyn RoomStorage>,
    bus: Bus,
}

impl OutOfRoomMemberEvents {
    #[instrument(name = "OutOfRoomMemberEvents::insert_event", skip(self, event, state))]
    pub async fn insert_event(
        &self,
        sender: &UserId,
        event: Event,
        state: MembershipState,
    ) -> Result<(), RoomError> {
        let options = EventOptions::default();
        self.event_storage.put_event(&event, options).await?;
        let stream_id = self.room_stream.insert_event_to_stream(&event).await?;

        let membership = Membership {
            state,
            stream: stream_id,
            event_id: event.event_ref.event_id().to_owned(),
        };

        self.room_storage
            .update_membership(sender, &event.room_id, membership)
            .await?;

        self.bus.send_message(BusMessage::StreamUpdate(stream_id));

        Ok(())
    }
}
