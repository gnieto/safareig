use safareig_core::{
    bus::BusMessage,
    ruma::{DeviceId, OwnedServerName, OwnedUserId, ServerName, UserId},
};
use tokio::sync::broadcast::Receiver;

pub struct Waker;

impl Waker {
    // Probably needs to receive a filter
    pub async fn sync(
        mut rx: Receiver<BusMessage>,
        user_id: &UserId,
        maybe_device: Option<&DeviceId>,
    ) {
        while let Ok(msg) = rx.recv().await {
            match msg {
                // BusMessage::RoomCreated(_) | BusMessage::RoomJoined(_, _) => return,
                BusMessage::RoomInvited(_, invitee, _) => {
                    if invitee == user_id {
                        return;
                    }
                }
                BusMessage::E2eeUpdate(_) => return,
                BusMessage::StreamUpdate(_) => return,
                BusMessage::Marker(_) => return,
                BusMessage::FullyReadMarker(u, _) => {
                    if u == user_id {
                        return;
                    }
                }
                BusMessage::AccountData(user) => {
                    if user == user_id {
                        return;
                    }
                }
                BusMessage::Typing(a, b) => {
                    tracing::info!("Woke up for typing notification: {} {}", a, b);
                    // TODO: Check user_id -> rooom membership
                    return;
                }
                BusMessage::ToDevice(_, incoming) => {
                    if let Some(expected) = &maybe_device {
                        if &incoming == expected {
                            return;
                        }
                    }
                }
                BusMessage::Shutdown => {
                    tracing::debug!("Waking up sync waiter: Server shutdown");
                    return;
                }
                _ => (),
            }
        }

        // Load filter
        // Wait for new events matching the filter (rooms)
        // Presence changes
        // Account data
        // to_device events
    }

    pub async fn any_event(mut rx: Receiver<BusMessage>) {
        while let Ok(msg) = rx.recv().await {
            match msg {
                BusMessage::RoomUpdate { .. } => return,
                BusMessage::RoomCreated(_) | BusMessage::RoomJoined(_, _) => return,
                _ => (),
            }
        }
    }

    pub async fn user_registered(mut rx: Receiver<BusMessage>) -> OwnedUserId {
        while let Ok(msg) = rx.recv().await {
            if let BusMessage::UserRegistered(user) = msg {
                return user;
            }
        }

        unreachable!()
    }

    pub async fn server_tracked(mut rx: Receiver<BusMessage>) -> OwnedServerName {
        while let Ok(msg) = rx.recv().await {
            match msg {
                BusMessage::ServerTracked(server, _) => {
                    return server;
                }
                BusMessage::ServerServiceRestored(server) => {
                    return server;
                }
                _ => (),
            }
        }

        unreachable!()
    }

    pub async fn wait_for(
        mut rx: Receiver<BusMessage>,
        f: impl Fn(&BusMessage) -> bool,
    ) -> BusMessage {
        while let Ok(msg) = rx.recv().await {
            if f(&msg) {
                return msg;
            }
        }

        unreachable!()
    }

    pub async fn federation_event(
        server: &ServerName,
        rx: &mut Receiver<BusMessage>,
    ) -> BusMessage {
        while let Ok(msg) = rx.recv().await {
            match msg {
                BusMessage::RoomUpdate { .. } => {
                    return msg;
                }
                BusMessage::RoomCreated(_) | BusMessage::RoomJoined(_, _) => {
                    return msg;
                }
                BusMessage::ServerTracked(ref tracked, _) => {
                    if tracked == server {
                        return msg;
                    }
                }
                BusMessage::Typing(_, _) | BusMessage::Presence(_) | BusMessage::E2eeUpdate(_) => {
                    return msg;
                }
                BusMessage::ToDeviceFederation(ref tracked) => {
                    if tracked == server {
                        return msg;
                    }
                }
                _ => (),
            }
        }

        unreachable!();
    }
}
