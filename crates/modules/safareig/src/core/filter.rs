use std::{
    collections::{BTreeSet, HashSet},
    convert::TryFrom,
};

use safareig_core::ruma::{
    api::client::filter::{FilterDefinition, LazyLoadOptions, RoomEventFilter},
    OwnedRoomId, OwnedUserId, RoomId,
};

use crate::core::events::Event;

pub struct Filter<'a> {
    filter: &'a FilterDefinition,
    user_rooms: BTreeSet<&'a RoomId>,
    ignored: &'a HashSet<OwnedUserId>,
}

impl<'a> Filter<'a> {
    pub fn new(
        filter: &'a FilterDefinition,
        rooms: BTreeSet<&'a RoomId>,
        ignored: &'a HashSet<OwnedUserId>,
    ) -> Self {
        Filter {
            filter,
            user_rooms: rooms,
            ignored,
        }
    }

    pub fn new_single_room(
        filter: &'a FilterDefinition,
        room: &'a RoomId,
        ignored: &'a HashSet<OwnedUserId>,
    ) -> Self {
        let mut rooms = BTreeSet::new();
        rooms.insert(room);

        Self::new(filter, rooms, ignored)
    }

    pub fn has_lazy_load(&self) -> bool {
        matches!(
            self.filter.room.state.lazy_load_options,
            LazyLoadOptions::Enabled { .. }
        )
    }

    pub fn lazy_load_options(&self) -> &LazyLoadOptions {
        &self.filter.room.state.lazy_load_options
    }

    pub fn definition(&self) -> &FilterDefinition {
        self.filter
    }

    /// Checks if the given room is allowed by the filter definition and the user's rooms. This is
    /// returning if the user should receive events from the given room.
    pub fn is_timeline_event_allowed(&self, event: &Event) -> bool {
        let filter_definition = &self.filter.room.timeline;
        let timeline_filter = RoomFilter::new(filter_definition);

        if event.state_key.is_none() && self.ignored.contains(&event.sender) {
            return false;
        }

        Self::allowed(
            &event.room_id,
            &filter_definition.not_rooms,
            &filter_definition.rooms,
        ) && timeline_filter.event_allowed(event)
    }

    /// Checks if the given state event is allowed by the filter definition and the user's rooms. This is
    /// returning true if the user should receive events from the given room.
    pub fn is_state_event_allowed(&self, event: &Event) -> bool {
        let filter_definition = &self.filter.room.state;
        let room_filter = RoomFilter::new(filter_definition);

        Self::allowed(
            &event.room_id,
            &filter_definition.not_rooms,
            &filter_definition.rooms,
        ) && room_filter.event_allowed(event)
    }

    /// Checks if the given room is allowed by the filter definition and the user's rooms. This is
    /// returning if the user should receive events from the given room.
    #[allow(clippy::borrowed_box)]
    pub fn is_room_allowed(&self, room: &OwnedRoomId) -> bool {
        if !Self::allowed(
            room,
            self.filter.room.not_rooms.as_ref(),
            &self.filter.room.rooms,
        ) {
            return false;
        }

        let room: &RoomId = room.as_ref();
        self.user_rooms.contains(room)
    }

    pub fn timeline_limit(&self) -> usize {
        let default = 15usize;
        self.filter
            .room
            .timeline
            .limit
            .map(|uint| match u32::try_from(uint) {
                Ok(limit) => limit as usize,
                Err(_) => default,
            })
            .unwrap_or(default)
            .min(default)
    }

    fn allowed<T: Eq>(value: &T, no_accepted: &[T], accepted: &Option<Vec<T>>) -> bool {
        if no_accepted.contains(value) {
            return false;
        }

        accepted
            .as_ref()
            .map(|accepted_values| accepted_values.contains(value))
            .unwrap_or(true)
    }
}

pub struct RoomFilter<'a> {
    room_filter: &'a RoomEventFilter,
}

impl<'a> RoomFilter<'a> {
    pub fn new(room_filter: &'a RoomEventFilter) -> Self {
        Self { room_filter }
    }

    pub fn event_allowed(&self, event: &Event) -> bool {
        Self::allowed(&event.room_id, self.room_filter.not_rooms.as_ref(), &self.room_filter.rooms) &&
        Self::allowed(&event.sender, self.room_filter.not_senders.as_ref(), &self.room_filter.senders) &&
            // TODO: Wildcard filtering. Probably we want to store a specialized version of the filter
            // to prevent recompiling a regex everytime.
            // TODO: Allocation on `to_string`.
            Self::allowed(&event.kind.to_string(), self.room_filter.not_types.as_ref(), &self.room_filter.types)
    }

    pub fn has_lazy_load(&self) -> bool {
        matches!(
            self.room_filter.lazy_load_options,
            LazyLoadOptions::Enabled { .. }
        )
    }

    pub fn lazy_load_options(&self) -> &LazyLoadOptions {
        &self.room_filter.lazy_load_options
    }

    fn allowed<T: Eq>(value: &T, no_accepted: &[T], accepted: &Option<Vec<T>>) -> bool {
        if no_accepted.contains(value) {
            return false;
        }

        accepted
            .as_ref()
            .map(|accepted_values| accepted_values.contains(value))
            .unwrap_or(true)
    }
}
