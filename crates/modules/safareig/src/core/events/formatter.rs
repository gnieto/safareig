use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    events::EventError,
    ruma::{
        canonical_json::redact,
        events::pdu::EventHash,
        serde::Raw,
        signatures::{
            hash_and_sign_event, reference_hash, verify_event, Ed25519KeyPair, PublicKeyMap,
            PublicKeySet, Verified,
        },
        CanonicalJsonObject, CanonicalJsonValue, MilliSecondsSinceUnixEpoch, OwnedEventId,
        OwnedServerName, RoomVersionId, ServerName, ServerSigningKeyId, UInt,
    },
};
use safareig_federation::keys::{CanonicalJsonValueExt, KeyProvider};
use serde_json::{value::to_raw_value, Value};

use super::{Event, EventRef};
use crate::{
    client::room::events::SignError,
    core::room::EventFormat,
    federation::room::{PduTemplate, RoomV1PduTemplate, RoomV3PduTemplate},
};

pub struct EventFormatterV1 {
    key_provider: Arc<KeyProvider>,
    server_name: OwnedServerName,
}

impl EventFormatterV1 {
    pub fn new(key_provider: Arc<KeyProvider>, server_name: OwnedServerName) -> Self {
        Self {
            key_provider,
            server_name,
        }
    }
}

#[async_trait]
impl EventFormat for EventFormatterV1 {
    fn event_to_pdu(
        &self,
        event: &super::Event,
        key: &Ed25519KeyPair,
    ) -> Result<safareig_core::ruma::serde::Raw<safareig_core::ruma::events::pdu::Pdu>, EventError>
    {
        if !event.should_be_signed_by(self.server_name.as_ref()) {
            let value = serde_json::to_value(event)?;

            return Ok(Raw::from_json(
                to_raw_value(&value).map_err(|_| EventError::CanonicalJsonConversion)?,
            ));
        }

        let canonical_json: CanonicalJsonValue = serde_json::to_value(event)
            .map_err(|_| EventError::CanonicalJsonConversion)?
            .try_into()
            .map_err(|_| EventError::CanonicalJsonConversion)?;

        let mut canonical_json = canonical_json
            .into_object()
            .ok_or(EventError::CanonicalJsonConversion)?;
        canonical_json.insert(
            "event_id".to_string(),
            CanonicalJsonValue::String(event.event_ref.event_id().to_string()),
        );

        let room_version = RoomVersionId::V2;
        hash_and_sign_event(
            self.server_name.as_str(),
            key,
            &mut canonical_json,
            &room_version,
        )
        .map_err(|e| {
            tracing::error!("Could not sign pdu: {}", e);
            EventError::SigningError
        })?;
        let raw = to_raw_value(&canonical_json).map_err(|_| EventError::CanonicalJsonConversion)?;

        Ok(Raw::from_json(raw))
    }

    fn event_to_pdu_template(&self, event: Event) -> PduTemplate {
        let prev_events = event
            .prev_events
            .into_iter()
            .filter_map(|r| match r {
                EventRef::V1(event_id, hashes) => Some((event_id, hashes)),
                _ => {
                    tracing::warn!(
                        event_id = event.event_ref.event_id().as_str(),
                        "Found non-v1 event id in prev events"
                    );

                    None
                }
            })
            .collect();

        let auth_events = event
            .auth_events
            .into_iter()
            .filter_map(|r| match r {
                EventRef::V1(event_id, hashes) => Some((event_id, hashes)),
                _ => {
                    tracing::warn!(
                        event_id = event.event_ref.event_id().as_str(),
                        "Found non-v1 event id in auth events"
                    );

                    None
                }
            })
            .collect();

        let v1_pdu_template = RoomV1PduTemplate {
            room_id: event.room_id,
            sender: event.sender,
            origin_server_ts: MilliSecondsSinceUnixEpoch(UInt::new_wrapping(
                event.origin_server_ts,
            )),
            kind: event.kind,
            content: event.content,
            state_key: event.state_key,
            prev_events,
            depth: UInt::new_wrapping(event.depth),
            auth_events,
        };

        PduTemplate::RoomV1PduTemplate(v1_pdu_template)
    }

    async fn pdu_to_event(
        &self,
        pdu: &safareig_core::ruma::serde::Raw<safareig_core::ruma::events::pdu::Pdu>,
    ) -> Result<super::Event, crate::client::room::events::SignError> {
        let pdu_value: CanonicalJsonValue = serde_json::to_value(pdu)?.try_into()?;

        let mut raw_pdu = match pdu_value {
            CanonicalJsonValue::Object(o) => Some(o),
            _ => None,
        }
        .ok_or(SignError::RootIsNotObject)?;
        let event_id = raw_pdu
            .get("event_id")
            .and_then(|v| v.as_str().map(|v| v.to_string()))
            .ok_or(crate::client::room::events::SignError::CouldNotConvertToEvent)?;
        let sha256 = raw_pdu
            .get("hashes")
            .and_then(|v| v.as_object())
            .and_then(|hashes| hashes.get("sha256"))
            .and_then(|sha256| sha256.as_str())
            .map(|sha256| sha256.to_string())
            .ok_or(crate::client::room::events::SignError::CouldNotConvertToEvent)?;

        // Pick an event-formatter v2 copatible version to pass to ruma signatures functions
        let version = RoomVersionId::V2;
        let public_map = build_key_map(self.key_provider.as_ref(), &raw_pdu).await;
        let verified_event = verify_event(&public_map, &raw_pdu, &version)?;

        let mut redacted = false;
        if let Verified::Signatures = verified_event {
            redacted = true;
            raw_pdu = redact(raw_pdu, &version, None)?;
        }

        if redacted {
            let event_id = raw_pdu
                .get("event_id")
                .and_then(|v| v.as_str().map(|v| v.to_string()))
                .unwrap_or_default();

            tracing::info!(
                event_id = event_id.as_str(),
                "Redacting event with mismatching hash but correct signature"
            );
        }

        let mut json_value = serde_json::to_value(raw_pdu)?;
        let json_obj = json_value.as_object_mut().unwrap();
        let event_id = OwnedEventId::try_from(event_id)
            .map_err(|_| crate::client::room::events::SignError::CouldNotConvertToEvent)?;
        let event_ref = EventRef::V1(event_id, EventHash { sha256 });
        json_obj.insert("event_id".to_string(), serde_json::to_value(&event_ref)?);

        Ok(serde_json::from_value(json_value)?)
    }
}

pub struct EventFormatterV2 {
    key_provider: Arc<KeyProvider>,
    server_name: OwnedServerName,
}

impl EventFormatterV2 {
    pub fn new(key_provider: Arc<KeyProvider>, server_name: OwnedServerName) -> Self {
        Self {
            key_provider,
            server_name,
        }
    }
}

#[async_trait]
impl EventFormat for EventFormatterV2 {
    fn event_to_pdu(
        &self,
        event: &super::Event,
        key: &Ed25519KeyPair,
    ) -> Result<safareig_core::ruma::serde::Raw<safareig_core::ruma::events::pdu::Pdu>, EventError>
    {
        if !event.should_be_signed_by(self.server_name.as_ref()) {
            let mut value = serde_json::to_value(event)?;
            value.as_object_mut().map(|obj| {
                obj.remove("event_id");

                obj
            });

            return Ok(Raw::from_json(
                to_raw_value(&value).map_err(|_| EventError::CanonicalJsonConversion)?,
            ));
        }

        let canonical_json: CanonicalJsonValue = serde_json::to_value(event)
            .map_err(|_| EventError::CanonicalJsonConversion)?
            .try_into()
            .map_err(|_| EventError::CanonicalJsonConversion)?;

        let mut canonical_json = canonical_json
            .into_object()
            .ok_or(EventError::CanonicalJsonConversion)?;

        canonical_json.remove("event_id");

        let room_version = RoomVersionId::V4;
        hash_and_sign_event(
            self.server_name.as_str(),
            key,
            &mut canonical_json,
            &room_version,
        )
        .map_err(|e| {
            tracing::error!("Could not sign pdu: {}", e);
            EventError::SigningError
        })?;

        let raw = to_raw_value(&canonical_json).map_err(|_| EventError::CanonicalJsonConversion)?;

        Ok(Raw::from_json(raw))
    }

    fn event_to_pdu_template(&self, event: Event) -> PduTemplate {
        let prev_events = event
            .prev_events
            .into_iter()
            .filter_map(|r| match r {
                EventRef::V2(event_id) => Some(event_id),
                _ => None,
            })
            .collect();

        let auth_events = event
            .auth_events
            .into_iter()
            .filter_map(|r| match r {
                EventRef::V2(event_id) => Some(event_id),
                _ => None,
            })
            .collect();

        let v3_pdu_template = RoomV3PduTemplate {
            room_id: event.room_id,
            sender: event.sender,
            origin_server_ts: MilliSecondsSinceUnixEpoch(UInt::new_wrapping(
                event.origin_server_ts,
            )),
            kind: event.kind,
            content: event.content,
            state_key: event.state_key,
            prev_events,
            depth: UInt::new_wrapping(event.depth),
            auth_events,
        };

        PduTemplate::RoomV3PduTemplate(v3_pdu_template)
    }

    async fn pdu_to_event(
        &self,
        pdu: &safareig_core::ruma::serde::Raw<safareig_core::ruma::events::pdu::Pdu>,
    ) -> Result<super::Event, crate::client::room::events::SignError> {
        let pdu_value: CanonicalJsonValue = serde_json::to_value(pdu)?.try_into()?;

        let mut raw_pdu = match pdu_value {
            CanonicalJsonValue::Object(o) => Some(o),
            _ => None,
        }
        .ok_or(SignError::RootIsNotObject)?;

        // Pick an event-formatter v2 copatible version to pass to ruma signatures functions
        let version = RoomVersionId::V4;
        let public_map = build_key_map(self.key_provider.as_ref(), &raw_pdu).await;
        let verified_event = verify_event(&public_map, &raw_pdu, &version).map_err(|e| {
            let hash =
                reference_hash(&raw_pdu, &version).expect("reference hash can be calculated");
            let raw_id = format!("${hash}");
            let event_id: OwnedEventId =
                raw_id.as_str().try_into().expect("id could be calculated");

            tracing::error!(
                ?event_id,
                raw_event = pdu.json().get(),
                "could not verify target event"
            );

            SignError::SignatureError(e)
        })?;
        let mut redacted = false;
        if let Verified::Signatures = verified_event {
            redacted = true;
            raw_pdu = redact(raw_pdu, &version, None)?;
        }

        let hash = reference_hash(&raw_pdu, &version).expect("reference hash can be calculated");
        let raw_id = format!("${hash}");
        let event_id: OwnedEventId = raw_id.as_str().try_into().expect("id could be calculated");

        if redacted {
            tracing::info!(
                event_id = event_id.as_str(),
                "Redacting event with mismatching hash but correct signature"
            );
        }

        let mut json_value = serde_json::to_value(raw_pdu)?;

        if let Some(map) = json_value.as_object_mut() {
            map.insert("event_id".to_string(), Value::String(event_id.to_string()));
        }

        Ok(serde_json::from_value(json_value)?)
    }
}

async fn build_key_map(key_provider: &KeyProvider, pdu: &CanonicalJsonObject) -> PublicKeyMap {
    let mut public_key_map = PublicKeyMap::new();

    let signatures = pdu.get("signatures").and_then(|value| value.as_object());

    if signatures.is_none() {
        return public_key_map;
    }
    let signatures = signatures.unwrap();

    for (server, canonical) in signatures {
        let server_name = ServerName::parse(server.as_str());
        if let Err(e) = server_name {
            tracing::error!("Could not decode servername: {}", e);
            continue;
        }
        let server_name = server_name.unwrap();

        match canonical.as_object() {
            None => continue,
            Some(server_sig) => {
                let mut server_keys = PublicKeySet::new();

                for key in server_sig.keys() {
                    let maybe_ssid: Result<&ServerSigningKeyId, _> = key.as_str().try_into();

                    match maybe_ssid {
                        Ok(ssid) => {
                            let key = key_provider.load(&server_name, ssid).await;

                            match key {
                                Some(key) => {
                                    server_keys.insert(ssid.to_string(), key.base64());
                                }
                                None => {
                                    tracing::error!(
                                        key_id = ssid.as_str(),
                                        server_name = server.as_str(),
                                        "target key could not be loaded from the key chain. If the key is required to verify the event, verify_event will fail",
                                    );
                                }
                            }
                        }
                        Err(e) => {
                            tracing::error!(
                                err = e.to_string().as_str(),
                                "Could not parse key as SigningKeyId"
                            );
                        }
                    }
                }

                public_key_map.insert(server.clone(), server_keys);
            }
        }
    }

    public_key_map
}

#[cfg(test)]
mod tests {
    use std::{collections::HashMap, sync::Arc};

    use safareig_core::ruma::{
        events::{pdu::Pdu, room::create::RoomCreateEventContent},
        serde::Raw,
        RoomVersionId,
    };
    use safareig_federation::keys::KeyProvider;

    use crate::{
        client::room::events::PduExt,
        core::{
            events::{Content, EventBuilder},
            room::RoomVersionRegistry,
        },
        testing::TestingDB,
    };

    #[tokio::test]
    async fn redacts_an_incoming_pdu_if_signature_matches_but_hash_does_not() {
        let db = TestingDB::default();
        let user_id = db.register().await;
        let user_id = user_id.user();
        let room_id = safareig_core::ruma::room_id!("!sala:localhost");
        let create = RoomCreateEventContent {
            creator: Some(user_id.to_owned()),
            federate: false,
            room_version: db.default_room_version(),
            predecessor: None,
            room_type: None,
        };
        let key_provider = db.container.service::<Arc<KeyProvider>>();
        let current_key = key_provider.current_key().await.unwrap();

        let room_versions = db.container.service::<Arc<RoomVersionRegistry>>();
        let room_version_id = db.default_room_version();
        let room_version = room_versions.get(&room_version_id).unwrap();

        let event = EventBuilder::builder(
            user_id,
            Content::from_event_content(&create).unwrap(),
            room_id,
        )
        .build(room_version.event_id_calculator())
        .unwrap();

        let pdu_original = room_version
            .event_format()
            .event_to_pdu(&event, &current_key)
            .unwrap();
        let mut modified_pdu: HashMap<String, serde_json::Value> =
            serde_json::from_str(pdu_original.json().get()).unwrap();
        let event_content = modified_pdu.get_mut("content").unwrap();
        let event_content = event_content.as_object_mut().unwrap();
        event_content.insert(
            "room_version".to_string(),
            serde_json::Value::String("5".to_string()),
        );

        let raw_modified = serde_json::value::to_raw_value(&modified_pdu).unwrap();
        let modified_pdu = Raw::<Pdu>::from_json(raw_modified);

        let good = pdu_original.deserialize().unwrap();
        let good_signature = good
            .signatures()
            .get(&db.server_name())
            .unwrap()
            .values()
            .next()
            .unwrap();
        let modified = modified_pdu.deserialize().unwrap();
        let modified_signature = modified
            .signatures()
            .get(&db.server_name())
            .unwrap()
            .values()
            .next()
            .unwrap();

        assert_eq!(good_signature, modified_signature);
        // We've modified the event, but the signatures matched (because signatures are calculated based on the reference hash and not the content hash)
        // Deserializing this PDU should not fail, but we will get a redacted version of this event. In this case, for example, the room_version key will
        // be redacted and it will default to room_version = "1".
        let event_from_modified_pdu = room_version
            .event_format()
            .pdu_to_event(&modified_pdu)
            .await
            .unwrap();
        let create_content = event_from_modified_pdu
            .content_as::<RoomCreateEventContent>()
            .unwrap();

        assert_eq!(&create_content.creator.unwrap(), user_id);
        assert_eq!(create_content.room_version, RoomVersionId::V1);
    }
}
