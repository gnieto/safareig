use std::{collections::HashMap, fmt::Debug};

use safareig_core::ruma::{
    events::TimelineEventType, state_res::RoomVersion as RumaRoomVersion, OwnedEventId,
    RoomVersionId,
};
use thiserror::Error;

use crate::{
    client::room::state::{ruma_stateres::ResolutionEvent, StateIndex},
    core::{events::Event, room::Authorizer},
};

// TODO: Wrap in a proper type and add some cache mechanism
pub type AuthChain<'a> = HashMap<StateIndex, &'a Event>;

#[derive(Error, Debug)]
pub enum AuthError {
    #[error("Event ID {0} was not authorized")]
    Unauthorized(OwnedEventId),

    #[error("Given auth events do not match with the expected auth events calculated from the state at event")]
    MismatchingAuthEvents,

    #[error("Missing auth events. Probably remote state could not be fetched.")]
    MissingAuthEvents,
}

pub struct RumaAuthorizer {
    version: RumaRoomVersion,
}

impl From<RoomVersionId> for RumaAuthorizer {
    fn from(room_version: RoomVersionId) -> Self {
        Self {
            version: RumaRoomVersion::new(&room_version).unwrap(),
        }
    }
}

impl Authorizer for RumaAuthorizer {
    fn authorize(&self, auth_chain: &AuthChain, event: &Event) -> Result<(), AuthError> {
        let auth_check = safareig_core::ruma::state_res::auth_check(
            &self.version,
            ResolutionEvent::from(event.clone()),
            None::<ResolutionEvent>,
            |event_type, state_key| {
                let legacy_event_type = TimelineEventType::from(event_type.to_string());

                let state_index =
                    StateIndex::with_state_key(legacy_event_type, state_key.to_owned());
                auth_chain
                    .get(&state_index)
                    .map(|e| ResolutionEvent::from((*e).clone()))
            },
        )
        .unwrap_or(false);

        if !auth_check {
            return Err(AuthError::Unauthorized(event.event_ref.owned_event_id()));
        }

        Ok(())
    }
}
