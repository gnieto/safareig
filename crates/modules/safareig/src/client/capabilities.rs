use std::{collections::BTreeMap, sync::Arc};

use async_trait::async_trait;
use reqwest::StatusCode;
use safareig_core::{
    auth::Identity,
    command::Command,
    config::HomeserverConfig,
    ruma::api::client::{
        discovery::get_capabilities::{
            self, Capabilities, ChangePasswordCapability, RoomVersionStability,
            RoomVersionsCapability,
        },
        error::{ErrorBody, ErrorKind},
    },
    Inject,
};

use crate::core::room::RoomVersionRegistry;

#[derive(Inject)]
pub struct GetCapabilitesCommand {
    room_versions: Arc<RoomVersionRegistry>,
    config: Arc<HomeserverConfig>,
}

#[async_trait]
impl Command for GetCapabilitesCommand {
    type Input = get_capabilities::v3::Request;
    type Output = get_capabilities::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, _: Self::Input, _: Identity) -> Result<Self::Output, Self::Error> {
        let mut available_rooms = BTreeMap::new();
        for version in self.room_versions.get_supported_versions() {
            available_rooms.insert(version.clone(), RoomVersionStability::Stable);
        }

        let mut capabilities = Capabilities::default();
        capabilities
            .set(
                "m.change_password",
                serde_json::to_value(ChangePasswordCapability::new(true)).unwrap(),
            )
            .map_err(|_| safareig_core::ruma::api::client::Error {
                status_code: StatusCode::INTERNAL_SERVER_ERROR,
                body: ErrorBody::Standard {
                    kind: ErrorKind::Unknown,
                    message: "Could not set change password capability".to_string(),
                },
            })?;

        let room_versions = RoomVersionsCapability {
            default: self.config.default_room_version(),
            available: available_rooms,
        };
        capabilities
            .set(
                "m.room_versions",
                serde_json::to_value(room_versions).unwrap(),
            )
            .map_err(|_| safareig_core::ruma::api::client::Error {
                status_code: StatusCode::INTERNAL_SERVER_ERROR,
                body: ErrorBody::Standard {
                    kind: ErrorKind::Unknown,
                    message: "Could not set available room versions".to_string(),
                },
            })?;

        Ok(get_capabilities::v3::Response { capabilities })
    }
}
