use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    command::Command,
    error::ResourceNotFound,
    ruma::api::client::filter::{create_filter, get_filter},
    Inject,
};

use crate::storage::FilterStorage;

#[derive(Inject)]
pub struct GetFilter {
    filter: Arc<dyn FilterStorage>,
}

#[async_trait]
impl Command for GetFilter {
    type Input = get_filter::v3::Request;
    type Output = get_filter::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let _ = id.check_user(&input.user_id)?;
        let definition = self
            .filter
            .get(&input.filter_id)
            .await?
            .ok_or(ResourceNotFound)?;

        Ok(get_filter::v3::Response::new(definition))
    }
}

#[derive(Inject)]
pub struct CreateFilter {
    filter: Arc<dyn FilterStorage>,
}

#[async_trait]
impl Command for CreateFilter {
    type Input = create_filter::v3::Request;
    type Output = create_filter::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let _ = id.check_user(&input.user_id)?;
        let filter_id = uuid::Uuid::new_v4().simple().to_string();
        let _ = self.filter.put(&filter_id, &input.filter).await?;

        Ok(create_filter::v3::Response::new(filter_id))
    }
}

#[cfg(test)]
mod tests {
    use safareig_core::{
        command::Command,
        ruma::{
            api::client::filter::{
                create_filter, get_filter, EventFormat, Filter, FilterDefinition, RoomEventFilter,
                RoomFilter,
            },
            RoomId,
        },
    };

    use crate::{
        client::filter::{CreateFilter, GetFilter},
        testing::TestingDB,
    };

    #[tokio::test]
    async fn it_can_load_just_stored_filter() {
        let db = TestingDB::default();
        let id = TestingDB::new_identity();
        let create_cmd = db.command::<CreateFilter>();
        let get_cmd = db.command::<GetFilter>();

        let filter_def = FilterDefinition {
            event_fields: None,
            event_format: EventFormat::Client,
            presence: Filter::default(),
            account_data: Filter::default(),
            room: RoomFilter {
                include_leave: false,
                account_data: RoomEventFilter::default(),
                timeline: RoomEventFilter::default(),
                ephemeral: RoomEventFilter::default(),
                state: RoomEventFilter::default(),
                not_rooms: vec![RoomId::new(db.server_name().as_ref())],
                rooms: None,
            },
        };
        let create = create_filter::v3::Request {
            user_id: id.user().to_owned(),
            filter: filter_def.clone(),
        };
        let response = create_cmd.execute(create, id.clone()).await.unwrap();

        let get = get_filter::v3::Request {
            user_id: id.user().to_owned(),
            filter_id: response.filter_id.clone(),
        };
        let response = get_cmd.execute(get, id.clone()).await.unwrap();

        assert_eq!(response.filter.room.not_rooms, filter_def.room.not_rooms,)
    }
}
