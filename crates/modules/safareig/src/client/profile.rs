use std::sync::Arc;

use safareig_core::{
    listener::Listener,
    ruma::{
        events::room::member::{MembershipState, RoomMemberEventContent},
        OwnedRoomId, UserId,
    },
    Inject,
};
use safareig_users::profile::ProfileUpdated;

use crate::{
    client::room::{loader::RoomLoader, RoomError},
    core::events::{EventBuilder, EventContentExt},
    storage::RoomStorage,
};

#[derive(Inject)]
pub struct ProfileBroadcaster {
    rooms: Arc<dyn RoomStorage>,
    room_loader: RoomLoader,
}

impl ProfileBroadcaster {
    pub async fn broadcast_profile(&self, event: &ProfileUpdated) -> Result<(), RoomError> {
        let joined_rooms = self.joined_rooms(event.user()).await;

        for room_id in joined_rooms {
            if let Some(room) = self.room_loader.get(&room_id).await? {
                let content = RoomMemberEventContent {
                    avatar_url: event.avatar().map(|a| a.to_owned()),
                    displayname: event.displayname().map(|d| d.to_owned()),
                    // Join events can't be direct, only invites
                    is_direct: None,
                    membership: MembershipState::Join,
                    third_party_invite: None,
                    reason: None,
                    join_authorized_via_users_server: None,
                };

                let event_builder = EventBuilder::state(
                    event.user(),
                    content.into_content()?,
                    &room_id,
                    Some(event.user().to_string()),
                );

                room.add_state(event_builder).await?;
            }
        }

        Ok(())
    }

    async fn joined_rooms(&self, user: &UserId) -> Vec<OwnedRoomId> {
        self.rooms
            .membership(user)
            .await
            .unwrap_or_default()
            .into_iter()
            .filter(|(_room, m)| m.state == MembershipState::Join)
            .map(|(r, _)| r)
            .collect()
    }
}

#[async_trait::async_trait]
impl Listener<ProfileUpdated> for ProfileBroadcaster {
    async fn on_event(&self, event: &ProfileUpdated) {
        if let Err(error) = self.broadcast_profile(event).await {
            tracing::error!(?error, "could not broadcast profile update to all rooms");
        }
    }
}
