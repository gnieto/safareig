use reqwest::StatusCode;
use safareig_core::ruma::{
    api::client::error::{ErrorBody, ErrorKind},
    OwnedUserId,
};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum InviteError {
    #[error("can not invite banned users to a room")]
    UserBanned(OwnedUserId),
    #[error("can not invite already joined users to a room")]
    UserAlreadyJoined(OwnedUserId),
    #[error("inviter not in room")]
    InviterNotInRoom(OwnedUserId),
    #[error("insufficient inviter power")]
    InsufficientInviterPower(OwnedUserId),
}

impl From<InviteError> for safareig_core::ruma::api::client::Error {
    fn from(e: InviteError) -> Self {
        match e {
            InviteError::UserBanned(_)
            | InviteError::UserAlreadyJoined(_)
            | InviteError::InsufficientInviterPower(_)
            | InviteError::InviterNotInRoom(_) => safareig_core::ruma::api::client::Error {
                status_code: StatusCode::FORBIDDEN,
                body: ErrorBody::Standard {
                    kind: ErrorKind::Unknown,
                    message: e.to_string(),
                },
            },
        }
    }
}
