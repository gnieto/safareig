mod lazy_cache;
pub mod ruma_stateres;

use std::{
    collections::{HashMap, HashSet},
    convert::TryFrom,
    fmt::Display,
    sync::Arc,
};

pub use lazy_cache::{BloomLazyLoadCache, LazyLoadCache, LazyLoadKey};
use safareig_core::{
    ruma::{
        events::{
            room::member::{MembershipState, RoomMemberEventContent},
            StateEventType, TimelineEventType,
        },
        EventId, OwnedEventId, OwnedUserId, UserId,
    },
    storage::StorageError,
};

use crate::{
    client::room::RoomError,
    core::events::{Event, EventBuilder},
    storage::{EventStorage, StateStorage},
};

pub type RoomStateId = rusty_ulid::Ulid;

#[derive(Clone, Hash, PartialEq, Eq, Debug)]
pub struct StateIndex {
    pub kind: TimelineEventType,
    pub state_key: Option<String>,
}

impl StateIndex {
    pub fn new(event_type: TimelineEventType) -> StateIndex {
        StateIndex {
            kind: event_type,
            state_key: None,
        }
    }

    pub fn state_key(event_type: TimelineEventType, state_key: Option<String>) -> StateIndex {
        let mut final_key = None;
        if let Some(key) = state_key {
            if !key.is_empty() {
                final_key = Some(key);
            }
        }

        StateIndex {
            kind: event_type,
            state_key: final_key,
        }
    }

    pub fn with_state_key(event_type: TimelineEventType, state_key: String) -> StateIndex {
        let mut sk = None;
        if !state_key.is_empty() {
            sk = Some(state_key)
        }

        StateIndex {
            kind: event_type,
            state_key: sk,
        }
    }

    pub fn create() -> StateIndex {
        StateIndex {
            kind: TimelineEventType::RoomCreate,
            state_key: None,
        }
    }

    pub fn canonical_alias() -> StateIndex {
        StateIndex {
            kind: TimelineEventType::RoomCanonicalAlias,
            state_key: None,
        }
    }

    pub fn name() -> StateIndex {
        StateIndex {
            kind: TimelineEventType::RoomName,
            state_key: None,
        }
    }

    pub fn topic() -> StateIndex {
        StateIndex {
            kind: TimelineEventType::RoomTopic,
            state_key: None,
        }
    }

    pub fn power_levels() -> StateIndex {
        StateIndex {
            kind: TimelineEventType::RoomPowerLevels,
            state_key: None,
        }
    }

    pub fn history_visibility() -> StateIndex {
        StateIndex {
            kind: TimelineEventType::RoomHistoryVisibility,
            state_key: None,
        }
    }

    pub fn guest_access() -> StateIndex {
        StateIndex {
            kind: TimelineEventType::RoomGuestAccess,
            state_key: None,
        }
    }

    pub fn room_avatar() -> StateIndex {
        StateIndex {
            kind: TimelineEventType::RoomAvatar,
            state_key: None,
        }
    }

    pub fn join_rule() -> StateIndex {
        StateIndex {
            kind: TimelineEventType::RoomJoinRules,
            state_key: None,
        }
    }

    pub fn memberhip(user: String) -> StateIndex {
        StateIndex {
            kind: TimelineEventType::RoomMember,
            state_key: Some(user),
        }
    }
}

impl From<&Event> for StateIndex {
    fn from(e: &Event) -> Self {
        StateIndex::state_key(e.kind.clone(), e.state_key.clone())
    }
}

impl Display for StateIndex {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} - {:?}", self.kind, self.state_key)
    }
}

#[derive(Clone)]
pub struct RoomState {
    id: RoomStateId,
    state: Arc<dyn StateStorage>,
}

impl RoomState {
    pub fn new(state: Arc<dyn StateStorage>) -> RoomState {
        RoomState {
            id: rusty_ulid::Ulid::generate(),
            state,
        }
    }

    pub fn load(id: RoomStateId, state: Arc<dyn StateStorage>) -> RoomState {
        RoomState { id, state }
    }

    pub fn id(&self) -> &RoomStateId {
        &self.id
    }

    pub async fn auth_events(
        &self,
        sender: &UserId,
        state: Option<StateInfo>,
        event_storage: &dyn EventStorage,
    ) -> Result<Vec<Event>, StorageError> {
        if RoomState::is_create_room(&state) {
            return Ok(vec![]);
        }

        let mut keys = vec![
            StateIndex::new(TimelineEventType::RoomCreate),
            StateIndex::new(TimelineEventType::RoomPowerLevels),
            StateIndex::with_state_key(TimelineEventType::RoomMember, sender.to_string()),
        ];

        if let Some(membership) = Self::membership(&state) {
            let target_membership_user =
                state
                    .map(|state_info| state_info.state_key)
                    .ok_or_else(|| {
                        StorageError::Custom("missing state key on member event".to_string())
                    })?;
            keys.push(StateIndex::with_state_key(
                TimelineEventType::RoomMember,
                target_membership_user,
            ));

            match membership {
                MembershipState::Invite | MembershipState::Join => {
                    keys.push(StateIndex::new(TimelineEventType::RoomJoinRules));
                }
                _ => (),
            }
        }

        let event_ids = self.state.event_for_keys(&self.id, &keys).await?;
        let events = event_storage.get_events(&event_ids).await?;

        if event_ids.len() != events.len() {
            return Err(StorageError::Custom(format!(
                "Expected to load {} events, but {} were loaded",
                event_ids.len(),
                events.len()
            )));
        }

        Ok(events)
    }

    pub async fn state_events_ids(
        &self,
        event_storage: &dyn EventStorage,
    ) -> (Vec<OwnedEventId>, Vec<OwnedEventId>) {
        let events = self.state.states(&self.id, None).await.unwrap();
        let events_ids: Vec<OwnedEventId> = events
            .iter()
            .map(|e| e.event_ref.owned_event_id())
            .collect();
        let mut auth_chain: HashSet<OwnedEventId> = events
            .iter()
            .flat_map(|e| {
                let auth_events: Vec<OwnedEventId> = e
                    .auth_events
                    .iter()
                    .map(|r| r.event_id().to_owned())
                    .collect();

                auth_events
            })
            .collect();
        let mut current_auth_ids: HashSet<OwnedEventId> = auth_chain.clone();

        while !current_auth_ids.is_empty() {
            let mut next_ids = HashSet::new();

            for e in current_auth_ids {
                if let Ok(Some(auth_event)) = event_storage.get_event(&e).await {
                    for eid in auth_event.auth_events {
                        let event_id = eid.event_id();
                        if !auth_chain.contains(event_id) {
                            next_ids.insert(event_id.to_owned());
                            auth_chain.insert(event_id.to_owned());
                        }
                    }
                }
            }

            current_auth_ids = next_ids;
        }

        (events_ids, auth_chain.iter().cloned().collect())
    }

    // TODO: Reduce duplicated code with method above
    pub async fn state_and_auth_chain(
        &self,
        event_storage: &dyn EventStorage,
    ) -> Result<(Vec<Event>, Vec<Event>), RoomError> {
        let events = self.state.states(&self.id, None).await?;
        let mut auth_chain: HashSet<OwnedEventId> = events
            .iter()
            .flat_map(|e| {
                let auth_events: Vec<OwnedEventId> = e
                    .auth_events
                    .iter()
                    .map(|r| r.event_id().to_owned())
                    .collect();

                auth_events
            })
            .collect();
        let mut current_auth_ids: HashSet<OwnedEventId> = auth_chain.clone();
        let mut auth_chain_events = HashMap::new();

        while !current_auth_ids.is_empty() {
            let mut next_ids = HashSet::new();

            for e in current_auth_ids {
                if let Ok(Some(auth_event)) = event_storage.get_event(&e).await {
                    for eid in &auth_event.auth_events {
                        let event_id = eid.event_id();

                        if !auth_chain.contains(event_id) {
                            next_ids.insert(event_id.to_owned());
                            auth_chain.insert(event_id.to_owned());
                        }
                    }

                    auth_chain_events.insert(e, auth_event);
                }
            }

            current_auth_ids = next_ids;
        }

        Ok((events, auth_chain_events.into_iter().map(|t| t.1).collect()))
    }

    pub async fn fork(
        mut self,
        index: StateIndex,
        event_id: &EventId,
    ) -> Result<Self, StorageError> {
        self.id = self.state.fork_state(&self.id, &index, event_id).await?;

        Ok(self)
    }

    pub async fn event_id_for_state_key(
        &self,
        index: &StateIndex,
    ) -> Result<Option<OwnedEventId>, StorageError> {
        self.state.event_for_key(&self.id, index).await
    }

    pub async fn state_events(
        &self,
        excluded_types: Option<Vec<StateEventType>>,
    ) -> Result<Vec<Event>, StorageError> {
        self.state.states(&self.id, excluded_types).await
    }

    pub async fn invite_state(
        &self,
        event_storage: &dyn EventStorage,
    ) -> Result<Vec<Event>, StorageError> {
        let keys = [
            StateIndex::new(TimelineEventType::RoomName),
            StateIndex::new(TimelineEventType::RoomCanonicalAlias),
            StateIndex::new(TimelineEventType::RoomAvatar),
            StateIndex::new(TimelineEventType::RoomJoinRules),
        ];

        let event_ids = self.state.event_for_keys(self.id(), &keys).await?;

        event_storage.get_events(&event_ids).await
    }

    pub async fn members(&self) -> Result<Vec<OwnedUserId>, StorageError> {
        // TODO: Iter
        let members = self
            .state
            .state_keys_for(self.id(), Some(TimelineEventType::RoomMember))
            .await?
            .iter()
            .filter_map(|state_key| {
                if let Some(sk) = &state_key.state_key {
                    return UserId::parse(sk.as_str()).ok();
                }

                None
            })
            .collect();

        Ok(members)
    }

    pub async fn event_ids_for_type(
        &self,
        kind: TimelineEventType,
    ) -> Result<Vec<OwnedEventId>, StorageError> {
        let keys = self.state.state_keys_for(self.id(), Some(kind)).await?;
        let event_ids = self.state.event_for_keys(&self.id, &keys).await?;

        Ok(event_ids)
    }

    pub async fn event_content_for_keys(
        &self,
        keys: &[StateIndex],
        event_storage: &dyn EventStorage,
    ) -> Result<HashMap<StateEventType, serde_json::Value>, StorageError> {
        let event_ids = self.state.event_for_keys(self.id(), keys).await?;

        let event_contents = event_storage
            .get_events(&event_ids)
            .await?
            .into_iter()
            .map(|event| (StateEventType::from(event.kind.to_string()), event.content))
            .collect();

        Ok(event_contents)
    }

    fn is_create_room(event: &Option<StateInfo>) -> bool {
        event
            .as_ref()
            .map(|state_info| state_info.kind == TimelineEventType::RoomCreate)
            .unwrap_or(false)
    }

    fn membership(event: &Option<StateInfo>) -> Option<MembershipState> {
        event
            .as_ref()
            .map(|info| {
                serde_json::from_value(info.content.clone())
                    .ok()
                    .map(|mc: RoomMemberEventContent| mc.membership)
            })
            .unwrap_or(None)
    }
}

#[derive(Debug)]
pub struct StateInfo {
    kind: TimelineEventType,
    content: serde_json::Value,
    state_key: String,
}

impl From<&Event> for StateInfo {
    fn from(e: &Event) -> Self {
        StateInfo {
            kind: e.kind.clone(),
            content: e.content.clone(),
            state_key: e.state_key.clone().unwrap_or_default(),
        }
    }
}

// TODO: Change to From
impl TryFrom<&EventBuilder> for StateInfo {
    type Error = ();

    fn try_from(builder: &EventBuilder) -> Result<Self, Self::Error> {
        // TODO: TryFrom -> From

        Ok(StateInfo {
            kind: builder.kind.clone(),
            content: builder.content.clone(),
            state_key: builder.state_key.clone().unwrap_or_default(),
        })
    }
}
