use std::{collections::HashMap, sync::Arc};

use reqwest::StatusCode;
use safareig_core::{
    bus::{Bus, BusMessage},
    config::HomeserverConfig,
    error::error_chain,
    events::Content,
    id::RoomIdExt,
    ruma::{
        api::federation::membership::{
            create_join_event::{v1 as send_join_v1, v2 as send_join},
            create_leave_event::v2 as send_leave,
            prepare_join_event::v1 as make_join,
            prepare_leave_event::v1 as make_leave,
        },
        events::{
            pdu::Pdu,
            room::member::{MembershipState, RoomMemberEventContent},
            TimelineEventType,
        },
        serde::Raw,
        OwnedEventId, OwnedServerName, RoomId, RoomVersionId, TransactionId, UserId,
    },
    Inject,
};
use safareig_federation::{
    client::ClientBuilder, keys::KeyProvider, server::storage::FederationStorage,
};
use safareig_users::profile::{member_event_with_profile, ProfileLoader};

use crate::{
    client::room::{loader::RoomLoader, state::StateIndex, Room, RoomError},
    core::{
        events::{Event, EventBuilder, EventContentExt},
        room::{
            EventEncoding, OutOfRoomMemberEvents, RoomDescriptor, RoomVersion, RoomVersionRegistry,
        },
    },
    federation::room::{
        join::{RemoteState, RoomState, RoomStateV1},
        PduTemplate,
    },
    storage::{EventOptions, InviteStorage, RoomStorage, RoomTopologyStorage, StateStorage},
};

const MAX_REASON_LENGTH: usize = 100;

pub struct Reason(String);

impl From<String> for Reason {
    fn from(reason: String) -> Self {
        if reason.len() >= MAX_REASON_LENGTH {
            tracing::info!("Truncating reason because it's too long");
            let mut new_reason = reason;
            new_reason.truncate(MAX_REASON_LENGTH);

            Reason(new_reason)
        } else {
            Reason(reason)
        }
    }
}

pub struct MembershipChange<'a> {
    sender: &'a UserId,
    target: Option<&'a UserId>,
    room_id: &'a RoomId,
    reason: Option<Reason>,
}

impl<'a> MembershipChange<'a> {
    pub fn delegated(
        sender: &'a UserId,
        target: &'a UserId,
        room_id: &'a RoomId,
        reason: Option<Reason>,
    ) -> Self {
        Self {
            sender,
            target: Some(target),
            room_id,
            reason,
        }
    }

    pub fn reflective(sender: &'a UserId, room_id: &'a RoomId, reason: Option<Reason>) -> Self {
        Self {
            sender,
            target: None,
            room_id,
            reason,
        }
    }

    pub fn sender(&self) -> &'a UserId {
        self.sender
    }

    pub fn room_id(&self) -> &'a RoomId {
        self.room_id
    }

    pub fn target(&self) -> &'a UserId {
        self.target.unwrap_or(self.sender)
    }

    pub fn reason(&self) -> Option<String> {
        self.reason.as_ref().map(|reason| reason.0.to_string())
    }
}

#[async_trait::async_trait]
pub trait RoomMembership {
    async fn join(&self, sender: &UserId, room_id: &RoomId) -> Result<(), RoomError>;
    async fn ban(
        &self,
        sender: &UserId,
        banned: &UserId,
        room_id: &RoomId,
        reason: Option<Reason>,
    ) -> Result<(), RoomError>;
    async fn leave<'a>(&self, change: &'a MembershipChange<'a>) -> Result<(), RoomError>;
}

// TODO: I'm un happy with this name
#[derive(Clone, Inject)]
#[inject(supress_constructor = true)]
pub struct RoomMembershipHandler {
    config: Arc<HomeserverConfig>,
    local: LocalMembership,
    federated: FederatedMembership,
    invite: Arc<dyn InviteStorage>,
    room_loader: RoomLoader,
}

#[async_trait::async_trait]
impl RoomMembership for RoomMembershipHandler {
    async fn join(&self, sender: &UserId, room_id: &RoomId) -> Result<(), RoomError> {
        let result = if room_id.try_server_name()? == self.config.server_name {
            self.local.join(sender, room_id).await
        } else {
            self.federated.join(sender, room_id).await
        };

        self.invite.remove_invite(sender, room_id).await?;

        result
    }

    async fn ban(
        &self,
        sender: &UserId,
        banned: &UserId,
        room_id: &RoomId,
        reason: Option<Reason>,
    ) -> Result<(), RoomError> {
        let result = if room_id.try_server_name()? == self.config.server_name {
            self.local.ban(sender, banned, room_id, reason).await
        } else {
            self.federated.ban(sender, banned, room_id, reason).await
        };

        self.invite.remove_invite(banned, room_id).await?;

        result
    }

    async fn leave<'a>(&self, change: &'a MembershipChange<'a>) -> Result<(), RoomError> {
        let room = self.room_loader.get(change.room_id()).await?;

        // If current homeserver already participates in this room, it can send a regular leave event, which will then
        // be propagated as usual to other participaing servers
        if change.room_id().try_server_name()? == self.config.server_name || room.is_some() {
            self.local.leave(change).await?
        } else {
            self.federated.leave(change).await?
        };

        self.invite
            .remove_invite(change.target(), change.room_id())
            .await?;

        Ok(())
    }
}

#[derive(Clone, Inject)]
pub(crate) struct LocalMembership {
    profile_loader: ProfileLoader,
    room_loader: RoomLoader,
}

#[async_trait::async_trait]
impl RoomMembership for LocalMembership {
    async fn join(&self, sender: &UserId, room_id: &RoomId) -> Result<(), RoomError> {
        let room = self
            .room_loader
            .get(room_id)
            .await?
            .ok_or_else(|| RoomError::RoomNotFound(room_id.to_string()))?;

        let profile = self.profile_loader.load(sender).await?;
        let content = member_event_with_profile(MembershipState::Join, profile.as_ref());
        let builder = EventBuilder::state(
            sender,
            content.into_content()?,
            room.id(),
            Some(sender.to_string()),
        );

        room.add_state(builder).await?;

        Ok(())
    }

    async fn ban(
        &self,
        sender: &UserId,
        banned: &UserId,
        room_id: &RoomId,
        reason: Option<Reason>,
    ) -> Result<(), RoomError> {
        let reason = reason.as_ref().map(|r| r.0.as_str());
        let room = self
            .room_loader
            .get(room_id)
            .await?
            .ok_or_else(|| RoomError::RoomNotFound(room_id.to_string()))?;
        let profile = self.profile_loader.load(sender).await?;
        let mut content = member_event_with_profile(MembershipState::Ban, profile.as_ref());
        content.reason = reason.map(|reason| reason.to_string());
        let builder = EventBuilder::state(
            sender,
            content.into_content()?,
            room.id(),
            Some(banned.to_string()),
        );

        room.add_state(builder).await?;

        Ok(())
    }

    async fn leave<'a>(&self, change: &'a MembershipChange<'a>) -> Result<(), RoomError> {
        let room = self
            .room_loader
            .get(change.room_id())
            .await?
            .ok_or_else(|| RoomError::RoomNotFound(change.room_id().to_string()))?;
        let profile = self.profile_loader.load(change.target()).await?;
        let mut content = member_event_with_profile(MembershipState::Leave, profile.as_ref());
        content.reason = change.reason();

        let builder = EventBuilder::state(
            change.sender(),
            content.into_content()?,
            room.id(),
            Some(change.target().to_string()),
        );

        room.add_state(builder).await?;

        Ok(())
    }
}

#[derive(Clone, Inject)]
pub(crate) struct FederatedMembership {
    client_builder: Arc<ClientBuilder>,
    bus: Bus,
    federation_storage: Arc<dyn FederationStorage>,
    key_provider: Arc<KeyProvider>,
    room_loader: RoomLoader,
    room_storage: Arc<dyn RoomStorage>,
    topology: Arc<dyn RoomTopologyStorage>,
    state_storage: Arc<dyn StateStorage>,
    room_version_loader: Arc<RoomVersionRegistry>,
    out_of_room_events: OutOfRoomMemberEvents,
}

#[async_trait::async_trait]
impl RoomMembership for FederatedMembership {
    async fn join(&self, sender: &UserId, room_id: &RoomId) -> Result<(), RoomError> {
        let (room_version, template) = self.make_join(sender, room_id).await?;
        let (join_event, room_state) = self.send_join(room_id, &room_version, template).await?;

        let partipating_servers = self
            .insert_partial_state(room_id, &room_version, &room_state, join_event)
            .await?;

        for remote_server in partipating_servers {
            self.federation_storage
                .track_server(room_id, remote_server.as_ref())
                .await?;

            self.bus.send_message(BusMessage::ServerTracked(
                remote_server.to_owned(),
                room_id.to_owned(),
            ));
        }

        Ok(())
    }

    async fn ban(
        &self,
        _sender: &UserId,
        _banned: &UserId,
        _room_id: &RoomId,
        _reason: Option<Reason>,
    ) -> Result<(), RoomError> {
        unimplemented!("")
    }

    async fn leave<'a>(&self, change: &'a MembershipChange<'a>) -> Result<(), RoomError> {
        // This code is only executed if the homesever do not participate in this room. In this scenario, it means that we
        // are rejecting an invite. We need to execute the server-to-server handshake to reject an invitation.
        let (version, template) = self.make_leave(change.target(), change.room_id()).await?;
        if !template.is_valid_leave_template(change.sender(), change.target(), change.room_id()) {
            tracing::error!(
                template = serde_json::to_string(&template)
                    .unwrap_or_default()
                    .as_str(),
                "Found invalid template",
            );

            return Err(RoomError::Custom(
                "remote server created a template with unexpected fields".to_string(),
            ));
        }

        // The current homeserver does not belong to the room, but we need to store the event and publish it to the
        // room stream. This will allow to send the event when the rejecting user `/sync`s
        let event = self
            .send_leave(change.room_id(), &version, template, change.reason())
            .await?;
        self.out_of_room_events
            .insert_event(change.sender(), event, MembershipState::Leave)
            .await?;

        Ok(())
    }
}

impl FederatedMembership {
    pub async fn make_join(
        &self,
        sender: &UserId,
        room_id: &RoomId,
    ) -> Result<(RoomVersion, PduTemplate), RoomError> {
        let mut request = make_join::Request::new(room_id.to_owned(), sender.to_owned());
        let supported_versions = self.room_version_loader.get_supported_versions();
        request.ver = supported_versions;

        let client = self
            .client_builder
            .client_for(room_id.try_server_name()?)
            .await?;
        let response: make_join::Response = client.auth_request(request).await?;
        let event: Raw<Pdu> = Raw::from_json(response.event);

        let json_str = event.json().get();

        let template: PduTemplate = serde_json::from_str(json_str).map_err(|e| {
            tracing::error!(err = error_chain(&e), "could not deserialize incoming PDU",);

            RoomError::from(e)
        })?;

        // Spec: The version of the room where the server is trying to join. If not provided, the room version is assumed to be either “1” or “2”.
        let room_version_id = response.room_version.unwrap_or(RoomVersionId::V1);
        let room_version = self
            .room_version_loader
            .get(&room_version_id)
            .ok_or_else(|| RoomError::Custom("target room version is not supported".to_string()))?
            .clone();

        Ok((room_version, template))
    }

    pub async fn make_leave(
        &self,
        sender: &UserId,
        room_id: &RoomId,
    ) -> Result<(RoomVersion, PduTemplate), RoomError> {
        let request = make_leave::Request::new(room_id.to_owned(), sender.to_owned());
        let client = self
            .client_builder
            .client_for(room_id.try_server_name()?)
            .await?;
        let response: make_leave::Response = client.auth_request(request).await?;
        let raw_template: Raw<PduTemplate> = Raw::from_json(response.event);

        let template: PduTemplate =
            serde_json::from_str(raw_template.json().get()).map_err(|e| {
                tracing::error!(err = error_chain(&e), "could not deserialize incoming PDU",);

                RoomError::from(e)
            })?;

        // Spec: The version of the room where the server is trying to join. If not provided, the room version is assumed to be either “1” or “2”.
        let room_version_id = response.room_version.unwrap_or(RoomVersionId::V1);
        let room_version = self
            .room_version_loader
            .get(&room_version_id)
            .ok_or_else(|| RoomError::Custom("target room version is not supported".to_string()))?
            .clone();

        Ok((room_version, template))
    }

    pub async fn send_join(
        &self,
        room_id: &RoomId,
        room_version: &RoomVersion,
        join_template: PduTemplate,
    ) -> Result<(Event, RoomState), RoomError> {
        let key = self.key_provider.current_key().await?;
        let client = self
            .client_builder
            .client_for(room_id.try_server_name()?)
            .await?;

        let raw_content = serde_json::value::to_raw_value(&join_template.content())?;
        let member_content = Raw::<RoomMemberEventContent>::from_json(raw_content);

        let event = EventBuilder::builder(
            join_template.sender(),
            Content::from_raw_event_content(
                TimelineEventType::RoomMember.to_string(),
                &member_content,
            )?,
            room_id,
        )
        .state_key(join_template.state_key().map(|key| key.to_string()))
        .prev_events(join_template.prev_events())
        .depth(join_template.depth())
        .auth_events(join_template.auth_events())
        .build(room_version.event_id_calculator())?;

        let pdu = room_version
            .event_format()
            .event_to_pdu(&event, key.as_ref())?;

        let pdu_content = pdu.into_json();
        let join_req = send_join::Request::new(
            room_id.to_owned(),
            event.event_ref.event_id().to_owned(),
            pdu_content.clone(),
        );
        let response = client.auth_request_custom::<_, RoomState>(join_req).await;

        let room_state = match response {
            Ok(room_state) => room_state,
            Err(e) => {
                tracing::error!("Federation error: {:?}", e);

                // Fallback to v1 if v2 endpoint is not found
                if e.matches_status_code(StatusCode::NOT_FOUND) {
                    let join_req = send_join_v1::Request::new(
                        room_id.to_owned(),
                        event.event_ref.event_id().to_owned(),
                        pdu_content,
                    );
                    client
                        .auth_request_custom::<_, RoomStateV1>(join_req)
                        .await?
                        .1
                } else {
                    return Err(e.into());
                }
            }
        };

        Ok((event, room_state))
    }

    pub async fn send_leave(
        &self,
        room_id: &RoomId,
        room_version: &RoomVersion,
        mut template: PduTemplate,
        reason: Option<String>,
    ) -> Result<Event, RoomError> {
        // TODO: This method is almost the same as `send_join`. Could we extract a generic version? It might be useful for knocks.
        let key = self.key_provider.current_key().await?;
        let client = self
            .client_builder
            .client_for(room_id.try_server_name()?)
            .await?;

        // TODO: Can this be done in a more type-safe way? Deserializing to RoomMemberEventContent may lose information in the
        // round-trip.
        if let Some(reason) = reason {
            let content = template.content_mut();
            content.as_object_mut().map(|content| {
                content.insert("reason".to_string(), serde_json::Value::String(reason))
            });
        }

        let raw_content: Raw<RoomMemberEventContent> = Raw::new(template.content())?.cast();
        let event = EventBuilder::builder(
            template.sender(),
            Content::from_raw_event_content(
                TimelineEventType::RoomMember.to_string(),
                &raw_content,
            )?,
            room_id,
        )
        .state_key(template.state_key().map(|key| key.to_string()))
        .prev_events(template.prev_events())
        .depth(template.depth())
        .auth_events(template.auth_events())
        .build(room_version.event_id_calculator())?;

        let pdu = room_version
            .event_format()
            .event_to_pdu(&event, key.as_ref())?;
        let pdu_content = pdu.into_json();
        let join_req = send_leave::Request::new(
            room_id.to_owned(),
            event.event_ref.event_id().to_owned(),
            pdu_content,
        );
        let _response = client.auth_request(join_req).await?;

        // TODO: Implement fallback to V1. Ruma's API do not expose all the required fields.

        Ok(event)
    }

    async fn insert_partial_state(
        &self,
        room_id: &RoomId,
        room_version: &RoomVersion,
        room_state: &RoomState,
        event: Event,
    ) -> Result<Vec<OwnedServerName>, RoomError> {
        // Validate DAG
        let remote_state = RemoteState::new(room_state, &event, room_version).await?;
        remote_state.validate_room_version(room_version.id())?;
        remote_state.validate(room_version.authorizer())?;
        let room = self.load_or_create_room(room_id, room_version.id()).await?;

        let backward_extremities = remote_state.find_backward_extremities();
        let participating_servers = remote_state.participating_servers();
        self.topology
            .add_backward_extremities(room.id(), &backward_extremities)
            .await?;

        let state_indexes: HashMap<OwnedEventId, StateIndex> = remote_state.collect_state_indexes();
        let state_id = self.state_storage.store_state(&state_indexes).await?;

        self.state_storage
            .assign_state(room.id(), event.event_ref.event_id(), &state_id)
            .await?;

        // Bulk insert into local room
        let deduplicated_events: HashMap<OwnedEventId, Event> = remote_state
            .auth_chain
            .into_iter()
            .chain(remote_state.state.into_iter())
            .collect();

        for (_, e) in deduplicated_events {
            let options = EventOptions::default().set_transaction_id(&TransactionId::new());

            room.insert_backfill_event(&e, false, options).await?;
        }

        // Insert join event
        room.insert_event(&event, room.version(), true, true)
            .await?;

        Ok(participating_servers)
    }

    async fn load_or_create_room(
        &self,
        room_id: &RoomId,
        room_version: &RoomVersionId,
    ) -> Result<Room, RoomError> {
        let maybe_room = self.room_loader.get(room_id).await?;

        let room = match maybe_room {
            Some(room) => room,
            None => {
                let descriptor = RoomDescriptor {
                    room_id: room_id.to_owned(),
                    encoding: EventEncoding::Json,
                    room_version: room_version.to_owned(),
                };
                self.room_storage
                    .update_room_descriptor(&descriptor)
                    .await?;

                self.room_loader
                    .get(room_id)
                    .await?
                    .ok_or_else(|| RoomError::RoomNotFound(room_id.to_string()))?
            }
        };

        Ok(room)
    }
}
