use safareig_core::ruma::{
    events::{room::power_levels::RoomPowerLevelsEventContent, TimelineEventType},
    Int, UserId,
};

pub struct PowerLevelChecker {
    content: RoomPowerLevelsEventContent,
}

impl PowerLevelChecker {
    pub fn new(content: RoomPowerLevelsEventContent) -> Self {
        Self { content }
    }

    pub fn can_redact(&self, user: &UserId) -> bool {
        self.user_power_level(user) >= self.content.redact
    }

    pub fn can_send(&self, user: &UserId, kind: &TimelineEventType) -> bool {
        let required_event_power = self
            .content
            .events
            .get(kind)
            .unwrap_or(&self.content.events_default);

        self.user_power_level(user) >= *required_event_power
    }

    fn user_power_level(&self, user: &UserId) -> Int {
        self.content
            .users
            .get(user)
            .cloned()
            .unwrap_or(self.content.users_default)
    }
}
