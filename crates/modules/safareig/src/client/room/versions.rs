use std::convert::{TryFrom, TryInto};

use safareig_core::ruma::{
    events::pdu::EventHash, signatures::reference_hash, CanonicalJsonValue, EventId, OwnedUserId,
    RoomVersionId,
};

use crate::{
    client::room::RoomError,
    core::events::{EventBuilder, EventRef},
};

pub trait EventIdCalculation: Send + Sync {
    fn calculate_event_id(&self, event_builder: &EventBuilder) -> Result<EventRef, RoomError>;
}

pub struct EventIdV3;

impl EventIdCalculation for EventIdV3 {
    fn calculate_event_id(&self, event_builder: &EventBuilder) -> Result<EventRef, RoomError> {
        let version = RoomVersionId::V4;
        let pdu_value: CanonicalJsonValue = serde_json::to_value(event_builder)?
            .try_into()
            .map_err(|_| RoomError::Custom("canonical object conversion".to_string()))?;
        let mut raw_pdu = match pdu_value {
            CanonicalJsonValue::Object(o) => Some(o),
            _ => None,
        }
        .unwrap();

        // Calculate content hash before generating the event id
        let content_hash = safareig_core::ruma::signatures::content_hash(&raw_pdu)?;
        let hashes = EventHash {
            sha256: content_hash.encode(),
        };
        raw_pdu.insert(
            "hashes".to_string(),
            CanonicalJsonValue::try_from(serde_json::to_value(hashes).unwrap()).unwrap(),
        );

        let eid = format!("${}", reference_hash(&raw_pdu, &version)?);
        eid.parse()
            .map_err(|_e| RoomError::InvalidEventId)
            .map(EventRef::V2)
    }
}

pub struct EventIdV1 {}

impl EventIdCalculation for EventIdV1 {
    fn calculate_event_id(&self, event_builder: &EventBuilder) -> Result<EventRef, RoomError> {
        let value = serde_json::to_value(event_builder)?;
        let sender_server = value
            .as_object()
            .and_then(|map| map.get("sender"))
            .and_then(|value| value.as_str())
            .and_then(|sender| {
                sender
                    .parse()
                    .map(|user: OwnedUserId| user.server_name().to_owned())
                    .ok()
            })
            .ok_or_else(|| RoomError::Custom("could not find sender".to_string()))?;

        let pdu_value: CanonicalJsonValue = serde_json::to_value(event_builder)?
            .try_into()
            .map_err(|_| RoomError::Custom("canonical object conversion".to_string()))?;
        let raw_pdu = match pdu_value {
            CanonicalJsonValue::Object(o) => Some(o),
            _ => None,
        }
        .unwrap();

        let content_hash = safareig_core::ruma::signatures::content_hash(&raw_pdu)?;
        let hashes = EventHash {
            sha256: content_hash.encode(),
        };
        let event_id = EventId::new(&sender_server);

        Ok(EventRef::V1(event_id, hashes))
    }
}

#[cfg(test)]
mod tests {
    use std::{collections::BTreeMap, time::UNIX_EPOCH};

    use safareig_core::ruma::{
        events::room::create::RoomCreateEventContent, RoomId, RoomVersionId, UserId,
    };
    use serde_json::json;
    use tokio::time::Duration;

    use crate::{
        client::room::versions::EventIdV3,
        core::events::{EventBuilder, EventContentExt},
    };

    #[test]
    pub fn it_calculates_a_v3_event_id_with_event_builder_origin_as_extra_field() {
        let user_id = UserId::parse("@test:test.com").unwrap();
        let mut content = RoomCreateEventContent::new_v1(user_id.clone());
        content.federate = false;
        content.predecessor = None;
        content.room_version = RoomVersionId::V6;

        let mut unsigned = BTreeMap::new();
        unsigned.insert("some".to_string(), json!("value"));
        let room_id = RoomId::parse("!asdfgh:example.com").unwrap();

        let event = EventBuilder::builder(&user_id, content.into_content().unwrap(), &room_id)
            .prev_events(vec![])
            .auth_events(vec![])
            .depth(0)
            .unsigned(unsigned)
            .state_key(Some("".to_string()))
            .event_time(UNIX_EPOCH + Duration::from_secs(1601218345))
            .add_extra_field(
                "origin".to_string(),
                serde_json::Value::String("test.com".to_string()),
            )
            .build(&EventIdV3 {})
            .unwrap();

        assert_eq!(
            "$_WecjAf12B3VIroZwnjSW78Je--uz_Iv4QWYMbAGNzk",
            event.event_ref.event_id(),
        );
    }

    #[test]
    pub fn it_calculates_a_v3_event_id_with_event_builder() {
        let user_id = UserId::parse("@test:test.com").unwrap();
        let mut content = RoomCreateEventContent::new_v1(user_id.clone());
        content.federate = false;
        content.predecessor = None;
        content.room_version = RoomVersionId::V6;

        let mut unsigned = BTreeMap::new();
        unsigned.insert("some".to_string(), json!("value"));
        let room_id = RoomId::parse("!asdfgh:example.com").unwrap();

        let event = EventBuilder::builder(&user_id, content.into_content().unwrap(), &room_id)
            .prev_events(vec![])
            .auth_events(vec![])
            .depth(0)
            .unsigned(unsigned)
            .state_key(Some("".to_string()))
            .event_time(UNIX_EPOCH + Duration::from_secs(1601218345))
            .build(&EventIdV3 {})
            .unwrap();

        assert_eq!(
            "$DEtC0-xInwN-CrFaf5WvCZXcb8VL65aVS3ObAvfVL6c",
            event.event_ref.event_id(),
        );
    }

    #[test]
    pub fn it_calculates_a_v3_event_id_with_event_builder_extra_unsigned_keys() {
        let user_id = UserId::parse("@test:test.com").unwrap();
        let mut content = RoomCreateEventContent::new_v1(user_id.clone());
        content.federate = false;
        content.predecessor = None;
        content.room_version = RoomVersionId::V6;

        let mut unsigned = BTreeMap::new();
        unsigned.insert("some".to_string(), json!("value"));
        unsigned.insert("another".to_string(), json!("value"));
        unsigned.insert("more".to_string(), json!("value"));
        let room_id = RoomId::parse("!asdfgh:example.com").unwrap();

        let event = EventBuilder::builder(&user_id, content.into_content().unwrap(), &room_id)
            .prev_events(vec![])
            .auth_events(vec![])
            .depth(0)
            .unsigned(unsigned)
            .state_key(Some("".to_string()))
            .event_time(UNIX_EPOCH + Duration::from_secs(1601218345))
            .build(&EventIdV3 {})
            .unwrap();

        assert_eq!(
            "$DEtC0-xInwN-CrFaf5WvCZXcb8VL65aVS3ObAvfVL6c",
            event.event_ref.event_id(),
        );
    }

    #[test]
    pub fn it_calculates_a_v3_event_id_with_prev_content() {
        let user_id = UserId::parse("@test:test.com").unwrap();
        let mut content = RoomCreateEventContent::new_v1(user_id.clone());
        content.federate = false;
        content.predecessor = None;
        content.room_version = RoomVersionId::V6;

        let mut unsigned = BTreeMap::new();
        unsigned.insert("some".to_string(), json!("value"));
        unsigned.insert("another".to_string(), json!("value"));
        unsigned.insert("more".to_string(), json!("value"));
        let room_id = RoomId::parse("!asdfgh:example.com").unwrap();

        let event = EventBuilder::builder(&user_id, content.into_content().unwrap(), &room_id)
            .prev_events(vec![])
            .auth_events(vec![])
            .depth(0)
            .unsigned(unsigned)
            .state_key(Some("".to_string()))
            .event_time(UNIX_EPOCH + Duration::from_secs(1601218345))
            .build(&EventIdV3 {})
            .unwrap();

        assert_eq!(
            "$DEtC0-xInwN-CrFaf5WvCZXcb8VL65aVS3ObAvfVL6c",
            event.event_ref.event_id(),
        );
    }

    #[test]
    pub fn it_calculates_a_v3_event_id_with_event_builder_extra_keys() {
        let user_id = UserId::parse("@test:test.com").unwrap();
        let mut content = RoomCreateEventContent::new_v1(user_id.clone());
        content.federate = false;
        content.predecessor = None;
        content.room_version = RoomVersionId::V6;

        let mut unsigned = BTreeMap::new();
        unsigned.insert("some".to_string(), json!("value"));
        let room_id = RoomId::parse("!asdfgh:example.com").unwrap();

        let event = EventBuilder::builder(&user_id, content.into_content().unwrap(), &room_id)
            .prev_events(vec![])
            .auth_events(vec![])
            .depth(0)
            .unsigned(unsigned)
            .state_key(Some("".to_string()))
            .event_time(UNIX_EPOCH + Duration::from_secs(1601218345))
            .add_extra_field("prev_state".to_string(), json!([]))
            .build(&EventIdV3 {})
            .unwrap();

        assert_eq!(
            "$M4XwyFAp7oY2E2GJi5uuioD1V2UJQq_1ehpY4v573_c",
            event.event_ref.event_id(),
        );
    }
}
