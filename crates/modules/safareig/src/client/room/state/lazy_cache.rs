use std::{collections::HashSet, hash::Hash, sync::RwLock};

use growable_bloom_filter::GrowableBloom;
use rusty_ulid::Ulid;
use safareig_core::ruma::{RoomId, UserId};

pub trait LazyLoadCache: Send + Sync {
    fn find_missing<'a>(&'a self, key: HashSet<LazyLoadKey<'a>>) -> HashSet<LazyLoadKey<'a>>;
    fn insert_keys(&self, keys: HashSet<LazyLoadKey>);
}

#[derive(Hash, Eq, PartialEq, Debug)]
pub struct LazyLoadKey<'a> {
    pub token: Ulid,
    pub user_id: &'a UserId,
    pub room_id: &'a RoomId,
}

pub struct BloomLazyLoadCache {
    bloom: RwLock<GrowableBloom>,
}

impl Default for BloomLazyLoadCache {
    fn default() -> Self {
        Self {
            bloom: RwLock::new(GrowableBloom::new(0.001, 1_000_000)),
        }
    }
}

impl LazyLoadCache for BloomLazyLoadCache {
    fn find_missing<'a>(&'a self, keys: HashSet<LazyLoadKey<'a>>) -> HashSet<LazyLoadKey<'a>> {
        let read_guard = self.bloom.read().unwrap();

        keys.into_iter()
            .filter(|key| !read_guard.contains(key))
            .collect()
    }

    fn insert_keys(&self, keys: HashSet<LazyLoadKey>) {
        let mut write_guard = self.bloom.write().unwrap();

        keys.iter().for_each(|key| {
            write_guard.insert(key);
        });
    }
}
