use std::{
    collections::{HashMap, HashSet},
    sync::Arc,
};

use safareig_core::{
    ruma::{
        events::{StateEventType, TimelineEventType},
        state_res::{resolve, StateMap},
        MilliSecondsSinceUnixEpoch, OwnedEventId, OwnedRoomId, RoomId, RoomVersionId, UserId,
    },
    storage::StorageError,
    time::{system_time_to_millis, u64_to_system_time},
};
use serde_json::value::RawValue;
use thiserror::Error;
use tokio::runtime::Handle;
use tracing::instrument;

use crate::{
    client::room::state::{RoomState, RoomStateId, StateIndex},
    core::{events::Event, room::StateResolutionAlgorithm},
    storage::{EventStorage, StateStorage},
};

#[derive(Error, Debug)]
pub enum ResolutionError {
    #[error("Error accessing underlying storage: {0}")]
    Storage(#[from] StorageError),
    // state_res::Error seems to be non-Send which makes it hard to work with async
    // TODO: Check if state-res Error can be made Send
    #[error("Error resolving the given state sets: {0}")]
    Resolution(String),
    #[error("Given state resolution contains an empty state id: {0:?}")]
    MissingState(HashSet<RoomStateId>),
    #[error("Unknown error while executing state resoultion algorithm")]
    Unknown,
}

/// Contains the parameters required to perform a state resolution
pub struct StateResParameters {
    pub state_ids: HashSet<RoomStateId>,
    pub room_id: OwnedRoomId,
    pub room_version: RoomVersionId,
}

/// `StateResResolver` resolves a set of states via the `state_res` crate. Note that this library
/// requires to preload all the state events before calling to the algorithm.
/// Note also that, in case of missing events, we will need to retrieve them beforehand (and apply
/// all the incoming event logic before starting the algorithm).
/// Ideally, we should be able to store the state of the resolution and resume the resolution with
/// that state resolution with the new events and the old partial state.
pub struct StateResolutionV2 {
    state: Arc<dyn StateStorage>,
    event_storage: Arc<dyn EventStorage>,
}

#[async_trait::async_trait]
impl StateResolutionAlgorithm for StateResolutionV2 {
    #[instrument(skip(self, params), fields(size = params.state_ids.len()))]
    async fn resolve(&self, params: StateResParameters) -> Result<RoomState, ResolutionError> {
        let state_for_resolution = self.prepare_states_for_resolution(&params).await?;
        let auth_events = state_for_resolution.auth_events;
        let state_sets = state_for_resolution.state_sets;

        // TODO: Unwrap JoinHandle
        let room_storage = self.event_storage.clone();
        let resolved_events = tokio::task::spawn_blocking(move || {
            Self::blocking_resolution(params, state_sets, auth_events, room_storage)
        })
        .await
        .map_err(|_| ResolutionError::Unknown)??;

        let resolved_state: HashMap<OwnedEventId, StateIndex> = resolved_events
            .into_iter()
            .map(|(state_index, event_id)| {
                let legacy = TimelineEventType::from(state_index.0.to_string());
                let state_index = StateIndex::with_state_key(legacy, state_index.1);
                (event_id, state_index)
            })
            .collect();

        let state_id = self.state.store_state(&resolved_state).await?;

        Ok(RoomState::load(state_id, self.state.clone()))
    }
}

impl StateResolutionV2 {
    pub fn new(state: Arc<dyn StateStorage>, event_storage: Arc<dyn EventStorage>) -> Self {
        Self {
            state,
            event_storage,
        }
    }

    #[instrument(skip(self, params), fields(size = params.state_ids.len()))]
    async fn prepare_states_for_resolution(
        &self,
        params: &StateResParameters,
    ) -> Result<StateForResolution, ResolutionError> {
        let mut state_sets = Vec::with_capacity(params.state_ids.len());
        let mut auth_events = Vec::with_capacity(params.state_ids.len());

        for state_id in params.state_ids.iter() {
            let events = self.state.states(state_id, None).await.unwrap();
            let mut current_auth = HashSet::new();
            let mut current_set = StateMap::new();

            for e in events {
                let kind = StateEventType::from(e.kind.to_string());

                current_set.insert(
                    (kind, e.state_key.clone().unwrap_or_default()),
                    e.event_ref.owned_event_id(),
                );
                current_auth.extend(e.auth_events.iter().map(|r| r.event_id().to_owned()));
            }

            state_sets.push(current_set);
            auth_events.push(current_auth.into_iter().collect());
        }

        let state_for_resolution = StateForResolution {
            auth_events,
            state_sets,
        };

        Ok(state_for_resolution)
    }

    fn blocking_resolution(
        params: StateResParameters,
        state_sets: Vec<StateMap<OwnedEventId>>,
        auth_chain_sets: Vec<HashSet<OwnedEventId>>,
        event_storage: Arc<dyn EventStorage>,
    ) -> Result<StateMap<OwnedEventId>, ResolutionError> {
        resolve(
            &params.room_version,
            state_sets.iter(),
            auth_chain_sets,
            |event_id| {
                let maybe_event = Handle::current().block_on(event_storage.get_event(event_id));

                if let Err(e) = maybe_event {
                    tracing::error!(
                        event_id = event_id.as_str(),
                        err = e.to_string().as_str(),
                        "could not fetch event from resolution",
                    );

                    return None;
                }

                maybe_event
                    .ok()
                    .flatten()
                    .map(ResolutionEvent::from)
                    .map(Arc::new)
            },
        )
        .map_err(|e| ResolutionError::Resolution(e.to_string()))
    }
}

struct StateForResolution {
    auth_events: Vec<HashSet<OwnedEventId>>,
    state_sets: Vec<StateMap<OwnedEventId>>,
}

/// `ResoulutionEvent` contains an `Event` with some additional fields required by
/// current implementation of safareig_core::ruma::state_res. This will have a impact on performance due to
/// conversions and allocations.
pub struct ResolutionEvent {
    event: Event,
    kind: TimelineEventType,
    raw_content: Box<serde_json::value::RawValue>,
}

impl From<Event> for ResolutionEvent {
    fn from(e: Event) -> Self {
        // TODO: This is not efficient performance-wise as it needs yet-another serialization round-trip
        let raw_content = serde_json::value::to_raw_value(&e.content).unwrap();
        let room_event_type = TimelineEventType::from(e.kind.to_string());

        ResolutionEvent {
            event: e,
            kind: room_event_type,
            raw_content,
        }
    }
}

impl safareig_core::ruma::state_res::Event for ResolutionEvent {
    type Id = OwnedEventId;

    fn event_id(&self) -> &Self::Id {
        self.event.event_ref.boxed_event_id()
    }

    fn room_id(&self) -> &RoomId {
        &self.event.room_id
    }

    fn sender(&self) -> &UserId {
        &self.event.sender
    }

    fn origin_server_ts(&self) -> MilliSecondsSinceUnixEpoch {
        system_time_to_millis(u64_to_system_time(self.event.origin_server_ts))
    }

    fn event_type(&self) -> &TimelineEventType {
        &self.kind
    }

    fn content(&self) -> &RawValue {
        &self.raw_content
    }

    fn state_key(&self) -> Option<&str> {
        self.event.state_key.as_deref()
    }

    fn prev_events(&self) -> Box<dyn DoubleEndedIterator<Item = &Self::Id> + '_> {
        // TODO: Check perf optimitzations here
        Box::new(self.event.prev_events.iter().map(|r| r.boxed_event_id()))
    }

    /// All the authenticating events for this event.
    // Requires GATs to avoid boxing (and TAIT for making it convenient).
    fn auth_events(&self) -> Box<dyn DoubleEndedIterator<Item = &Self::Id> + '_> {
        // TODO: Check perf optimitzations here
        Box::new(self.event.auth_events.iter().map(|r| r.boxed_event_id()))
    }

    fn redacts(&self) -> Option<&Self::Id> {
        self.event.redacts.as_ref()
    }
}

#[cfg(test)]
mod tests {
    use std::sync::Arc;

    use safareig_core::{
        auth::Identity,
        ruma::{
            events::{
                room::member::{MembershipState, RoomMemberEventContent},
                TimelineEventType,
            },
            RoomId, UserId,
        },
    };

    use crate::{
        client::room::state::{RoomState, StateIndex},
        core::{
            events::{Event, EventBuilder, EventContentExt},
            room::RoomVersionRegistry,
        },
        storage::{Leaf, StateStorage},
        testing::TestingDB,
    };

    #[tokio::test]
    async fn simple_join_on_multiple_branches() {
        let db = TestingDB::default();
        let (creator, room) = db.public_room().await;
        let user1 = db.register().await;
        let user2 = db.register().await;
        let user3 = db.register().await;
        let forward_extremity = room
            .forward_extremities()
            .await
            .expect("dag has forward extermities")
            .pop()
            .expect("dag has one fwd extermity");
        db.talk(room.id(), &creator, "hi!")
            .await
            .expect("can send message");
        let version = room.version();

        // Join user 1 in branch A.
        let event1 = build_join_event(&db, room.id(), &user1, &forward_extremity).await;
        let _ = room
            .insert_event(&event1, version, true, true)
            .await
            .unwrap();

        // Join user 2 in branch B.
        let event2 = build_join_event(&db, room.id(), &user2, &forward_extremity).await;
        let _ = room
            .insert_event(&event2, version, true, true)
            .await
            .unwrap();

        // Join user 3 in branch C.
        let event3 = build_join_event(&db, room.id(), &user3, &forward_extremity).await;
        let _ = room
            .insert_event(&event3, version, true, true)
            .await
            .unwrap();

        let forward_extremity = room
            .forward_extremities()
            .await
            .expect("dag has forward extermities");
        assert_eq!(4, forward_extremity.len());

        db.talk(room.id(), &creator, "hi after branch merging!")
            .await
            .expect("can send message");

        let mut forward_extremity = room
            .forward_extremities()
            .await
            .expect("dag has forward extermities");
        assert_eq!(1, forward_extremity.len());

        let state = db.container.service::<Arc<dyn StateStorage>>();

        let state = RoomState::load(forward_extremity.pop().unwrap().state_at, state.clone());

        // Check state keys for room members
        let event_id = state
            .event_id_for_state_key(&StateIndex::with_state_key(
                TimelineEventType::RoomMember,
                event1.sender.to_string(),
            ))
            .await
            .expect("state key for user 1 exists")
            .unwrap();
        assert_eq!(event1.event_ref.event_id(), event_id);

        let event_id = state
            .event_id_for_state_key(&StateIndex::with_state_key(
                TimelineEventType::RoomMember,
                event2.sender.to_string(),
            ))
            .await
            .expect("state key for user 1 exists")
            .unwrap();
        assert_eq!(event2.event_ref.event_id(), event_id);

        let event_id = state
            .event_id_for_state_key(&StateIndex::with_state_key(
                TimelineEventType::RoomMember,
                event3.sender.to_string(),
            ))
            .await
            .expect("state key for user 1 exists")
            .unwrap();
        assert_eq!(event3.event_ref.event_id(), event_id);
    }

    #[tokio::test]
    async fn ban_conflict_resolution() {
        let db = TestingDB::default();
        let (creator, room) = db.public_room().await;
        let user1 = db.register().await;
        let forward_extremity = room
            .forward_extremities()
            .await
            .expect("dag has forward extermities")
            .pop()
            .expect("dag has one fwd extermity");
        db.talk(room.id(), &creator, "hi!")
            .await
            .expect("can send message");
        let version = room.version();
        // Join user 1 in branch A.
        let event1 = build_join_event(&db, room.id(), &user1, &forward_extremity).await;
        let _ = room
            .insert_event(&event1, version, true, true)
            .await
            .unwrap();

        // Kick user 1 in Branch B
        let event2 = build_ban_event(
            &db,
            room.id(),
            creator.user(),
            user1.user(),
            &forward_extremity,
        )
        .await;
        let _ = room
            .insert_event(&event2, version, true, true)
            .await
            .unwrap();

        let forward_extremity = room
            .forward_extremities()
            .await
            .expect("dag has forward extremities");
        assert_eq!(3, forward_extremity.len());

        // User talking in the room should lead to an error, as it has been baned
        let talk_result = db.talk(room.id(), &user1, "it should not appear").await;
        assert!(talk_result.is_err());

        // Creator should be allowed to talk and merge should succeed
        let creator_result = db.talk(room.id(), &creator, "is should work").await;
        assert!(creator_result.is_ok());

        let forward_extremity = room
            .forward_extremities()
            .await
            .expect("dag has forward extremities");
        assert_eq!(1, forward_extremity.len());
    }

    async fn build_join_event(
        db: &TestingDB,
        room: &RoomId,
        sender: &Identity,
        forward_extremity: &Leaf,
    ) -> Event {
        let content = RoomMemberEventContent {
            avatar_url: None,
            displayname: None,
            is_direct: None,
            membership: MembershipState::Join,
            third_party_invite: None,
            reason: None,
            join_authorized_via_users_server: None,
        };
        let room_versions = db.container.service::<Arc<RoomVersionRegistry>>();
        let room_version = room_versions.get(&db.default_room_version()).unwrap();

        let builder = EventBuilder::builder(sender.user(), content.into_content().unwrap(), room)
            .state_key(Some(sender.user().to_string()));

        let builder = db.add_auth_data(builder, forward_extremity).await;
        builder.build(room_version.event_id_calculator()).unwrap()
    }

    async fn build_ban_event(
        db: &TestingDB,
        room: &RoomId,
        sender: &UserId,
        kicked: &UserId,
        forward_extremity: &Leaf,
    ) -> Event {
        let content = RoomMemberEventContent {
            avatar_url: None,
            displayname: None,
            is_direct: None,
            membership: MembershipState::Ban,
            third_party_invite: None,
            reason: None,
            join_authorized_via_users_server: None,
        };
        let room_versions = db.container.service::<Arc<RoomVersionRegistry>>();
        let room_version = room_versions.get(&db.default_room_version()).unwrap();

        let builder = EventBuilder::builder(sender, content.into_content().unwrap(), room)
            .state_key(Some(kicked.to_string()));

        let builder = db.add_auth_data(builder, forward_extremity).await;
        builder.build(room_version.event_id_calculator()).unwrap()
    }

    // TODO: Find state resolution test cases on other repos
}
