use std::sync::Arc;

use safareig_core::{
    bus::Bus,
    ruma::{OwnedServerName, RoomId, ServerName, UserId},
    Inject,
};

use super::{events::RoomEventPublisher, hooks::EventHooks};
use crate::{
    client::room::{Room, RoomError, Storage},
    core::room::RoomVersionRegistry,
    federation::backfill::RemoteStateFetcher,
    storage::{
        EventStorage, InviteStorage, RoomStorage, RoomStreamStorage, RoomTopologyStorage,
        StateStorage,
    },
};

#[derive(Clone, Inject)]
pub struct RoomLoader {
    room: Arc<dyn RoomStorage>,
    room_stream_storage: Arc<dyn RoomStreamStorage>,
    events: Arc<dyn EventStorage>,
    topology: Arc<dyn RoomTopologyStorage>,
    state: Arc<dyn StateStorage>,
    server_name: OwnedServerName,
    bus: Bus,
    room_version_loader: Arc<RoomVersionRegistry>,
    invite: Arc<dyn InviteStorage>,
    event_publisher: Arc<RoomEventPublisher>,
    hooks: Arc<EventHooks>,
    remote_state_fetcher: Arc<RemoteStateFetcher>,
}

impl RoomLoader {
    pub async fn get(&self, room_id: &RoomId) -> Result<Option<Room>, RoomError> {
        let storage = Storage {
            room: self.room.clone(),
            room_stream_storage: self.room_stream_storage.clone(),
            events: self.events.clone(),
            topology: self.topology.clone(),
            state: self.state.clone(),
            server_name: self.server_name.clone(),
            bus: self.bus.clone(),
            invite: self.invite.clone(),
            publisher: self.event_publisher.clone(),
            hooks: self.hooks.clone(),
            remote_state_fetcher: self.remote_state_fetcher.clone(),
        };

        let room = self
            .room
            .room_descriptor(room_id)
            .await?
            .and_then(|descriptor| {
                self.room_version_loader
                    .get(&descriptor.room_version)
                    .map(|room_version| {
                        Room::load(storage, room_id, descriptor, room_version.clone())
                    })
            });

        Ok(room)
    }

    pub async fn get_for_user(
        &self,
        room_id: &RoomId,
        user_id: &UserId,
    ) -> Result<Room, RoomError> {
        let storage = Storage {
            room: self.room.clone(),
            room_stream_storage: self.room_stream_storage.clone(),
            events: self.events.clone(),
            topology: self.topology.clone(),
            state: self.state.clone(),
            server_name: self.server_name.clone(),
            bus: self.bus.clone(),
            invite: self.invite.clone(),
            publisher: self.event_publisher.clone(),
            hooks: self.hooks.clone(),
            remote_state_fetcher: self.remote_state_fetcher.clone(),
        };

        let room = self
            .room
            .room_descriptor(room_id)
            .await?
            .and_then(|descriptor| {
                self.room_version_loader
                    .get(&descriptor.room_version)
                    .map(|room_version| {
                        Room::load(storage, room_id, descriptor, room_version.clone())
                    })
            });

        let room = match room {
            Some(room) => room,
            None => return Err(RoomError::UserNotAllowed(user_id.to_owned())),
        };

        // TODO: This will lead on multiple load of loadign the state at leaves.
        // This can probably be cached on Room or move room when loading state at leaves.
        room.check_user_in_room(user_id).await?;

        Ok(room)
    }

    pub async fn get_for_server(
        &self,
        room_id: &RoomId,
        server_name: &ServerName,
    ) -> Result<Option<Room>, RoomError> {
        let room = self.get(room_id).await?;

        let room = match room {
            Some(room) => room,
            None => return Ok(None),
        };

        // TODO: This will lead on multiple load of loadign the state at leaves.
        // This can probably be cached on Room or move room when loading state at leaves.
        let state = room.state_at_leaves().await?;
        if let Some(acl_content) = room.acl(&state).await? {
            if !acl_content.is_allowed(server_name) {
                return Err(RoomError::ServerNotAllowed(server_name.to_owned()));
            }
        }

        Ok(Some(room))
    }
}
