use std::{
    collections::{HashMap, HashSet},
    sync::Arc,
};

use safareig_core::{
    ruma::{
        events::{room::canonical_alias::RoomCanonicalAliasEventContent, TimelineEventType},
        OwnedRoomAliasId,
    },
    Inject,
};
use safareig_rooms::alias::{error::AliasError, AliasResolver};

use super::RoomError;
use crate::core::events::Event;

#[async_trait::async_trait]
pub trait EventHook: Send + Sync {
    fn event_type(&self) -> TimelineEventType;
    async fn before_insert(&self, event: &Event) -> Result<(), RoomError>;
}

pub struct EventHooks {
    hooks: HashMap<TimelineEventType, Arc<dyn EventHook>>,
}

impl EventHooks {
    pub fn new(hooks: Vec<Arc<dyn EventHook>>) -> Self {
        let hooks = hooks
            .into_iter()
            .map(|hook| (hook.event_type(), hook))
            .collect();

        Self { hooks }
    }

    pub fn hook(&self, event_type: &TimelineEventType) -> Option<&dyn EventHook> {
        self.hooks.get(event_type).map(|hook| hook.as_ref())
    }
}

#[derive(Inject)]
pub struct CanonicalAliasHook {
    alias: Arc<dyn AliasResolver>,
}

impl CanonicalAliasHook {
    async fn verify_alias(&self, event: &Event, alias: &OwnedRoomAliasId) -> Result<(), RoomError> {
        match self.alias.resolve_alias(alias).await {
            Err(AliasError::AliasNotFound(_)) => {
                return Err(RoomError::BadAlias);
            }
            Ok(alias_info) => {
                if alias_info.room_id != event.room_id {
                    return Err(RoomError::BadAlias);
                }
            }
            Err(_) => {}
        };

        Ok(())
    }
}

#[async_trait::async_trait]
impl EventHook for CanonicalAliasHook {
    fn event_type(&self) -> TimelineEventType {
        TimelineEventType::RoomCanonicalAlias
    }

    async fn before_insert(&self, event: &Event) -> Result<(), RoomError> {
        let alias_content = event.content_as::<RoomCanonicalAliasEventContent>()?;
        let previous_content = event
            .prev_content_as::<RoomCanonicalAliasEventContent>()?
            .unwrap_or_default();

        let current_alias: HashSet<OwnedRoomAliasId> =
            alias_content.alt_aliases.into_iter().collect();

        let previous_alias: HashSet<OwnedRoomAliasId> =
            previous_content.alt_aliases.into_iter().collect();

        for alias in current_alias.difference(&previous_alias) {
            self.verify_alias(event, alias).await?;
        }

        if alias_content.alias != previous_content.alias {
            if let Some(alias) = alias_content.alias {
                self.verify_alias(event, &alias).await?;
            }
        }

        Ok(())
    }
}
