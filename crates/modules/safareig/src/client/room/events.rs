use std::{collections::BTreeMap, convert::TryInto, sync::Arc};

use reqwest::StatusCode;
use safareig_core::{
    error::RumaExt,
    events::EventError,
    ruma::{
        api::client::{
            error::{ErrorBody, ErrorKind},
            room::Visibility,
        },
        canonical_json::RedactionError,
        events::{
            pdu::{Pdu, RoomV3Pdu},
            EventContent, TimelineEventType,
        },
        signatures::{hash_and_sign_event, verify_event, KeyPair, PublicKeyMap},
        CanonicalJsonObject, CanonicalJsonValue, OwnedEventId, OwnedServerName,
        OwnedServerSigningKeyId, RoomId, RoomVersionId, UInt, UserId,
    },
    StreamToken,
};
use safareig_federation::keys::CanonicalJsonValueExt;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use thiserror::Error;

use crate::{client::room::RoomError, core::events::Event, storage::EventOptions};

#[derive(Error, Debug)]
pub enum SignError {
    #[error("Conversion from JSON failed: {0}")]
    SerdeJson(#[from] serde_json::Error),
    #[error("Conversion to canonical JSON failed: {0}")]
    CanonicalJson(#[from] safareig_core::ruma::CanonicalJsonError),
    #[error("Root value is not an object")]
    RootIsNotObject,
    #[error("Signature error: {0}")]
    SignatureError(#[from] safareig_core::ruma::signatures::Error),
    #[error("Could not convert to event")]
    CouldNotConvertToEvent,
    #[error("Redaction error: {0}")]
    RedactionError(#[from] RedactionError),
}

impl From<SignError> for safareig_core::ruma::api::client::Error {
    fn from(error: SignError) -> Self {
        safareig_core::ruma::api::client::Error {
            status_code: StatusCode::FORBIDDEN,
            body: ErrorBody::Standard {
                kind: ErrorKind::forbidden(),
                message: error.to_string(),
            },
        }
    }
}

impl From<SignError> for safareig_core::ruma::api::error::MatrixError {
    fn from(error: SignError) -> Self {
        let client = safareig_core::ruma::api::client::Error::from(error);
        client.to_generic()
    }
}

pub trait PduExt {
    fn room_id(&self) -> &RoomId;
    fn prev_events(&self) -> &Vec<OwnedEventId>;
    fn depth(&self) -> &UInt;
    fn as_v3(&self) -> Option<&RoomV3Pdu>;
    fn content(self) -> serde_json::Value;
    fn state_key(&self) -> Option<&String>;
    fn kind(&self) -> &TimelineEventType;
    fn sign<K: KeyPair>(
        self,
        origin: &str,
        key: &K,
        version: &RoomVersionId,
    ) -> Result<Pdu, RoomError>;
    fn signatures(&self) -> &BTreeMap<OwnedServerName, BTreeMap<OwnedServerSigningKeyId, String>>;
    #[allow(clippy::borrowed_box)]
    fn required_signature(&self) -> Option<(&OwnedServerSigningKeyId, &String)>;
    fn sender(&self) -> &UserId;
    fn hash_sha256(&self) -> &String;
}

impl PduExt for Pdu {
    fn room_id(&self) -> &RoomId {
        match self {
            Pdu::RoomV1Pdu(v1) => &v1.room_id,
            Pdu::RoomV3Pdu(v3) => &v3.room_id,
        }
    }

    fn prev_events(&self) -> &Vec<OwnedEventId> {
        match self {
            Pdu::RoomV1Pdu(_v1) => unimplemented!(""),
            Pdu::RoomV3Pdu(v3) => &v3.prev_events,
        }
    }

    fn depth(&self) -> &UInt {
        match self {
            Pdu::RoomV1Pdu(v1) => &v1.depth,
            Pdu::RoomV3Pdu(v3) => &v3.depth,
        }
    }

    fn as_v3(&self) -> Option<&RoomV3Pdu> {
        match self {
            Pdu::RoomV1Pdu(_v1) => None,
            Pdu::RoomV3Pdu(v3) => Some(v3),
        }
    }

    fn content(self) -> Value {
        let raw_value = match self {
            Pdu::RoomV1Pdu(v1) => v1.content,
            Pdu::RoomV3Pdu(v3) => v3.content,
        };

        // TODO: Check this unwrap after Ruma upgrade
        serde_json::from_str(raw_value.get()).unwrap()
    }

    fn state_key(&self) -> Option<&String> {
        match self {
            Pdu::RoomV1Pdu(v1) => v1.state_key.as_ref(),
            Pdu::RoomV3Pdu(v3) => v3.state_key.as_ref(),
        }
    }

    fn kind(&self) -> &TimelineEventType {
        match self {
            Pdu::RoomV1Pdu(v1) => &v1.kind,
            Pdu::RoomV3Pdu(v3) => &v3.kind,
        }
    }

    fn sign<K: KeyPair>(
        mut self,
        origin: &str,
        key: &K,
        version: &RoomVersionId,
    ) -> Result<Self, RoomError> {
        let value: CanonicalJsonValue = serde_json::to_value(&self)?.try_into().unwrap(); // TODO:a
        let mut canonical = value.into_object().unwrap();
        hash_and_sign_event(origin, key, &mut canonical, version)?;

        match &mut self {
            Pdu::RoomV1Pdu(stub) => {
                if let Some(CanonicalJsonValue::Object(o)) = canonical.get("hashes") {
                    if let Some(CanonicalJsonValue::String(sha)) = o.get("sha256") {
                        stub.hashes.sha256 = sha.clone();
                    }
                }

                stub.signatures = serde_json::from_value(
                    serde_json::to_value(canonical.get("signatures").unwrap()).unwrap(),
                )
                .unwrap();
            }
            Pdu::RoomV3Pdu(stub) => {
                if let Some(CanonicalJsonValue::Object(o)) = canonical.get("hashes") {
                    if let Some(CanonicalJsonValue::String(sha)) = o.get("sha256") {
                        stub.hashes.sha256 = sha.clone();
                    }
                }

                stub.signatures = serde_json::from_value(
                    serde_json::to_value(canonical.get("signatures").unwrap()).unwrap(),
                )
                .unwrap();
            }
        };

        Ok(self)
    }

    fn signatures(&self) -> &BTreeMap<OwnedServerName, BTreeMap<OwnedServerSigningKeyId, String>> {
        match self {
            Pdu::RoomV1Pdu(v1) => &v1.signatures,
            Pdu::RoomV3Pdu(v3) => &v3.signatures,
        }
    }

    fn required_signature(&self) -> Option<(&OwnedServerSigningKeyId, &String)> {
        let sender = self.sender();
        let server = sender.server_name();
        let signature = self
            .signatures()
            .get(server)
            .and_then(|server_signatures| server_signatures.iter().next());

        signature
    }

    fn sender(&self) -> &UserId {
        match self {
            Pdu::RoomV1Pdu(pdu) => &pdu.sender,
            Pdu::RoomV3Pdu(pdu) => &pdu.sender,
        }
    }

    fn hash_sha256(&self) -> &String {
        match self {
            Pdu::RoomV1Pdu(pdu) => &pdu.hashes.sha256,
            Pdu::RoomV3Pdu(pdu) => &pdu.hashes.sha256,
        }
    }
}

pub const CREATE_ROOM_EXTRA_TYPE: &str = "safareig.create_extra";
#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct CreateRoomExtra {
    pub visibility: Visibility,
}

impl EventContent for CreateRoomExtra {
    type EventType = &'static str;

    fn event_type(&self) -> Self::EventType {
        CREATE_ROOM_EXTRA_TYPE
    }
}

// TODO: Check if this could accept a Pdu reference instead and if this make sense
pub enum CanonicalPdu {
    NonCanonical(Pdu),
    Canonical(Pdu, CanonicalJsonObject),
}

// TODO: Remove?!
impl CanonicalPdu {
    #[must_use]
    pub fn canonical(self) -> Self {
        match self {
            CanonicalPdu::NonCanonical(pdu) => {
                let pdu_value: CanonicalJsonValue = serde_json::to_value(&pdu)
                    .expect("pdu can be converted to value")
                    .try_into()
                    .expect("JsonValue can be converted to CanonicalJson");

                let raw_pdu = match pdu_value {
                    CanonicalJsonValue::Object(o) => Some(o),
                    _ => None,
                }
                .expect("a PDU always contains an object on the root");

                CanonicalPdu::Canonical(pdu, raw_pdu)
            }
            _ => self,
        }
    }

    pub fn canonical_json(&self) -> Option<&CanonicalJsonObject> {
        match self {
            CanonicalPdu::NonCanonical(_) => None,
            CanonicalPdu::Canonical(_, canonical_json) => Some(canonical_json),
        }
    }

    // TODO: This should invalidate the underlying PDU
    pub fn canonical_json_mut(&mut self) -> Option<&mut CanonicalJsonObject> {
        match self {
            CanonicalPdu::NonCanonical(_) => None,
            CanonicalPdu::Canonical(_, ref mut canonical_json) => Some(canonical_json),
        }
    }

    pub fn pdu(&self) -> &Pdu {
        match self {
            CanonicalPdu::NonCanonical(p) => p,
            CanonicalPdu::Canonical(p, _) => p,
        }
    }

    pub fn take_pdu(self) -> Pdu {
        match self {
            CanonicalPdu::NonCanonical(p) => p,
            CanonicalPdu::Canonical(p, _) => p,
        }
    }

    pub fn is_canonical(&self) -> bool {
        match self {
            CanonicalPdu::Canonical(_, _) => true,
            CanonicalPdu::NonCanonical(_) => false,
        }
    }

    #[cfg(feature = "federation")]
    pub async fn verify(
        &self,
        room_version: RoomVersionId,
        _key_provider: &safareig_federation::keys::LocalKeyProvider,
    ) -> bool {
        // TODO: This should probably return a custom error instead of a bool value
        if !self.is_canonical() {
            return false;
        }
        let canonical_json = self.canonical_json().unwrap();
        let key_map = PublicKeyMap::new();

        let verification = verify_event(&key_map, canonical_json, &room_version);

        verification.is_ok()
    }
}

#[async_trait::async_trait]
pub trait RoomEventListener: Send + Sync {
    async fn on_stream_event(
        &self,
        event: &Event,
        stream_token: StreamToken,
        options: &EventOptions,
    ) -> Result<(), EventError>;
}

pub struct RoomEventPublisher {
    listeners: Vec<Arc<dyn RoomEventListener>>,
}

impl RoomEventPublisher {
    pub fn new(listeners: Vec<Arc<dyn RoomEventListener>>) -> Self {
        Self { listeners }
    }

    pub async fn publish_room_event(
        &self,
        event: &Event,
        stream_token: StreamToken,
        options: &EventOptions,
    ) {
        for l in &self.listeners {
            if let Err(e) = l.on_stream_event(event, stream_token, options).await {
                tracing::error!(
                    err = e.to_string().as_str(),
                    "Errored while processing an event via an event listener",
                )
            }
        }
    }
}

#[async_trait::async_trait]
pub trait RoomEventDecorator: Send + Sync {
    async fn decorate_events(&self, events: &mut [Event]);
}

pub struct EventDecorator {
    decorators: Vec<Arc<dyn RoomEventDecorator>>,
}

impl EventDecorator {
    pub fn new(decorators: Vec<Arc<dyn RoomEventDecorator>>) -> Self {
        Self { decorators }
    }

    pub async fn decorate(&self, event: Event) -> Event {
        let mut events = vec![event];
        self.decorate_events(events.as_mut_slice()).await;

        events.remove(0)
    }

    pub async fn decorate_events(&self, events: &mut [Event]) {
        for d in &self.decorators {
            d.decorate_events(events).await;
        }
    }
}
