use std::{collections::HashSet, convert::TryFrom, sync::Arc};

use async_trait::async_trait;
use itertools::Itertools;
use safareig_core::{
    auth::Identity,
    command::Command,
    error::ResourceNotFound,
    events::EventError,
    pagination,
    ruma::{
        api::{
            client::{context::get_context, filter::RoomEventFilter},
            Direction,
        },
        events::{AnyStateEvent, AnyTimelineEvent, StateEventType},
        serde::Raw,
        EventId, OwnedEventId, OwnedUserId,
    },
    Inject,
};

use crate::{
    client::{
        room::{events::EventDecorator, loader::RoomLoader, Room},
        sync::{state::LazyLoadMemberAdder, LazyLoadToken},
        topological::TopologicalEvents,
    },
    core::{events::Event, filter::RoomFilter},
    storage::{EventStorage, TopologicalToken},
};

#[derive(Inject)]
pub struct ContextCommand {
    room_loader: RoomLoader,
    events: Arc<dyn EventStorage>,
    topological: TopologicalEvents,
    event_decorator: Arc<EventDecorator>,
    lazy_adder: LazyLoadMemberAdder,
}

#[async_trait]
impl Command for ContextCommand {
    type Input = get_context::v3::Request;
    type Output = get_context::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let event_id = &input.event_id;

        let room = self
            .room_loader
            .get_for_user(&input.room_id, id.user())
            .await?;

        let filter_definition = input.filter;
        let room_filter = RoomFilter::new(&filter_definition);
        let mut lazy_state = LazyState::new(&filter_definition);

        let limit: u16 = u16::try_from(input.limit).unwrap_or_default();
        let limit = std::cmp::min(limit, 100);

        let event = self
            .events
            .get_event(event_id)
            .await?
            .ok_or(ResourceNotFound)?;
        lazy_state.collect_user_from_event(&event);
        let event = self.event_decorator.decorate(event).await;
        let topological_token = event.topological_token();
        let room_event = event.try_into()?;

        let (start_token, events_before) = self
            .before_data(
                &room,
                topological_token,
                &room_filter,
                limit,
                &mut lazy_state,
            )
            .await?;

        let (end_token, latest_event_id, events_after) = self
            .after_data(
                &room,
                topological_token,
                limit,
                &room_filter,
                &input.event_id,
                &mut lazy_state,
            )
            .await?;

        let response = get_context::v3::Response {
            start: Some(start_token.to_string()),
            end: Some(end_token.to_string()),
            events_after,
            event: Some(room_event),
            events_before,
            state: self
                .context_state(&room, &latest_event_id, lazy_state)
                .await?,
        };

        Ok(response)
    }
}

impl ContextCommand {
    async fn before_data(
        &self,
        room: &Room,
        to: TopologicalToken,
        room_filter: &RoomFilter<'_>,
        limit: u16,
        lazy_state: &mut LazyState<'_>,
    ) -> Result<
        (TopologicalToken, Vec<Raw<AnyTimelineEvent>>),
        safareig_core::ruma::api::client::Error,
    > {
        let range = (
            pagination::Boundary::Unlimited,
            pagination::Boundary::Exclusive(to),
        );
        let previous = self
            .topological
            .topological_events(room.id(), range.into(), limit, Direction::Backward)
            .await?
            .records();

        let start = previous
            .first()
            .map(|t| t.topological_token())
            .unwrap_or_else(TopologicalToken::root);

        let mut previous_events: Vec<Event> = previous
            .into_iter()
            .filter(|event| room_filter.event_allowed(event))
            .collect();

        lazy_state.collect_user_from_events(&previous_events);

        self.event_decorator
            .decorate_events(previous_events.as_mut_slice())
            .await;

        let previous_events = previous_events
            .into_iter()
            .map(TryInto::try_into)
            .collect::<Result<Vec<_>, EventError>>()?;

        Ok((start, previous_events))
    }

    async fn after_data(
        &self,
        room: &Room,
        from: TopologicalToken,
        limit: u16,
        room_filter: &RoomFilter<'_>,
        reference_event_id: &EventId,
        lazy_state: &mut LazyState<'_>,
    ) -> Result<
        (TopologicalToken, OwnedEventId, Vec<Raw<AnyTimelineEvent>>),
        safareig_core::ruma::api::client::Error,
    > {
        let range = (
            pagination::Boundary::Exclusive(from),
            pagination::Boundary::Unlimited,
        );

        let after = self
            .topological
            .topological_events(room.id(), range.into(), limit, Direction::Forward)
            .await?
            .records();

        let (end, last_event_id) = after
            .last()
            .map(|event| (event.topological_token(), event.event_ref.owned_event_id()))
            .unwrap_or((from, reference_event_id.to_owned()));

        let mut events_after: Vec<Event> = after
            .into_iter()
            .filter(|event| room_filter.event_allowed(event))
            .collect();
        lazy_state.collect_user_from_events(&events_after);

        self.event_decorator
            .decorate_events(events_after.as_mut_slice())
            .await;

        let events_after = events_after
            .into_iter()
            .map(TryInto::try_into)
            .collect::<Result<Vec<_>, EventError>>()?;

        Ok((end, last_event_id, events_after))
    }

    async fn context_state<'a>(
        &self,
        room: &Room,
        event_id: &EventId,
        lazy_state: LazyState<'a>,
    ) -> Result<Vec<Raw<AnyStateEvent>>, safareig_core::ruma::api::client::Error> {
        let state_at_event = room.state_at(event_id).await?.ok_or(ResourceNotFound)?;
        let has_lazy_load = !lazy_state.filter_definition.lazy_load_options.is_disabled();
        let excluded_types = if has_lazy_load {
            vec![StateEventType::RoomMember]
        } else {
            vec![]
        };

        let events = state_at_event.state_events(Some(excluded_types)).await?;
        let mut state_events = events
            .into_iter()
            .filter(|event| lazy_state.filter.event_allowed(event))
            .sorted_by_key(|e| e.depth)
            .map(|e| e.try_into())
            .collect::<Result<Vec<Raw<_>>, _>>()?;

        if let Some(users) = lazy_state.users {
            self.lazy_adder
                .add_lazy_load_members(
                    &mut state_events,
                    room.id(),
                    users,
                    &lazy_state.token,
                    &lazy_state.filter_definition.lazy_load_options,
                )
                .await?;
        }

        Ok(state_events)
    }
}

struct LazyState<'a> {
    users: Option<HashSet<OwnedUserId>>,
    token: LazyLoadToken,
    filter: RoomFilter<'a>,
    filter_definition: &'a RoomEventFilter,
}

impl<'a> LazyState<'a> {
    fn new(filter: &'a RoomEventFilter) -> Self {
        Self {
            users: if filter.lazy_load_options.is_disabled() {
                None
            } else {
                Some(HashSet::new())
            },
            token: LazyLoadToken::default(),
            filter: RoomFilter::new(filter),
            filter_definition: filter,
        }
    }

    fn collect_user_from_event(&mut self, event: &Event) {
        if self.filter_definition.lazy_load_options.is_disabled() {
            return;
        }

        self.users.as_mut().unwrap().insert(event.sender.clone());
    }

    fn collect_user_from_events(&mut self, events: &[Event]) {
        if self.filter_definition.lazy_load_options.is_disabled() {
            return;
        }

        for e in events {
            self.users.as_mut().unwrap().insert(e.sender.clone());
        }
    }
}
