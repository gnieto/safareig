use async_trait::async_trait;
use safareig_core::{
    auth::Identity, command::Command, ruma::api::client::membership::ban_user, Inject,
};

use crate::client::room::membership::{Reason, RoomMembership, RoomMembershipHandler};

#[derive(Inject)]
pub struct BanUserCommand {
    room_membership: RoomMembershipHandler,
}

#[async_trait]
impl Command for BanUserCommand {
    type Input = ban_user::v3::Request;
    type Output = ban_user::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let reason = input.reason.map(Reason::from);

        self.room_membership
            .ban(id.user(), &input.user_id, &input.room_id, reason)
            .await?;

        Ok(ban_user::v3::Response {})
    }
}
