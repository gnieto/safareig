use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    command::Command,
    error::ResourceNotFound,
    ruma::{api::client::state::get_state_events_for_key, events::TimelineEventType, serde::Raw},
    Inject,
};
use serde_json::value::RawValue;

use crate::client::room::{loader::RoomLoader, state::StateIndex, RoomError};

#[derive(Inject)]
pub struct StateEventForKeyCommand {
    room_loader: RoomLoader,
}

#[async_trait]
impl Command for StateEventForKeyCommand {
    type Input = get_state_events_for_key::v3::Request;
    type Output = get_state_events_for_key::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;
    const OPT_PARAMS: u8 = 1;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let room = self
            .room_loader
            .get(&input.room_id)
            .await?
            .ok_or(ResourceNotFound)?;
        let state = room
            .state_for_user(id.user())
            .await?
            .ok_or_else(|| RoomError::UserNotAllowed(id.user().to_owned()))?;

        let legacy = TimelineEventType::from(input.event_type.to_string());
        let index = StateIndex::with_state_key(legacy, input.state_key);
        let content = room
            .current_content(&state, &index)
            .await?
            .ok_or(ResourceNotFound)?;

        let raw_value = serde_json::to_string(&content)
            .and_then(RawValue::from_string)
            .map_err(RoomError::Serde)?;

        Ok(get_state_events_for_key::v3::Response::new(Raw::from_json(
            raw_value,
        )))
    }
}

#[cfg(test)]
mod tests {
    use safareig_core::{
        command::Command,
        ruma::{
            api::client::state::get_state_events_for_key,
            events::{
                room::{
                    member::{MembershipState, RoomMemberEventContent},
                    name::RoomNameEventContent,
                },
                StateEventType,
            },
        },
    };

    use crate::{
        client::room::command::state_event_for_key::StateEventForKeyCommand, testing::TestingDB,
    };

    #[tokio::test]
    async fn it_returns_a_state_event_with_empty_state_key() {
        let testing = TestingDB::default();
        let (id, room) = testing.public_room().await;
        let cmd = testing.command::<StateEventForKeyCommand>();
        let input = get_state_events_for_key::v3::Request {
            room_id: room.room_id.to_owned(),
            event_type: StateEventType::RoomName,
            state_key: "".to_string(),
        };

        let response = cmd.execute(input, id).await.unwrap();

        let room_name =
            serde_json::from_str::<RoomNameEventContent>(response.content.json().get()).unwrap();
        assert_eq!("Room name", room_name.name);
    }

    #[tokio::test]
    async fn it_returns_a_state_event_with_state_key() {
        let testing = TestingDB::default();
        let (id, room) = testing.public_room().await;

        let cmd = testing.command::<StateEventForKeyCommand>();
        let input = get_state_events_for_key::v3::Request {
            room_id: room.room_id.to_owned(),
            event_type: StateEventType::RoomMember,
            state_key: id.user().to_string(),
        };

        let response = cmd.execute(input, id).await.unwrap();

        let member_content =
            serde_json::from_str::<RoomMemberEventContent>(response.content.json().get()).unwrap();
        assert_eq!(MembershipState::Join, member_content.membership);
    }
}
