use async_trait::async_trait;
use safareig_core::{
    auth::Identity, command::Command, ruma::api::client::membership::leave_room, Inject,
};

use crate::client::room::membership::{
    MembershipChange, Reason, RoomMembership, RoomMembershipHandler,
};

#[derive(Inject)]
pub struct LeaveRoomCommand {
    membership: RoomMembershipHandler,
}

#[async_trait]
impl Command for LeaveRoomCommand {
    type Input = leave_room::v3::Request;
    type Output = leave_room::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let change =
            MembershipChange::reflective(id.user(), &input.room_id, input.reason.map(Reason::from));
        self.membership.leave(&change).await?;

        Ok(leave_room::v3::Response {})
    }
}
