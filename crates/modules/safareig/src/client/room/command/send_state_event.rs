use async_trait::async_trait;
use safareig_core::{
    auth::Identity, command::Command, error::ResourceNotFound, events::Content,
    ruma::api::client::state::send_state_event, Inject,
};

use crate::{
    client::room::{loader::RoomLoader, RoomError, SafareigStateEventContent},
    core::events::EventBuilder,
};

#[derive(Inject)]
pub struct SendStateEventCommand {
    room_loader: RoomLoader,
}

#[async_trait]
impl Command for SendStateEventCommand {
    type Input = send_state_event::v3::Request;
    type Output = send_state_event::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;
    const OPT_PARAMS: u8 = 1;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let room = self
            .room_loader
            .get(&input.room_id)
            .await?
            .ok_or(ResourceNotFound)?;

        let _ = SafareigStateEventContent::from_parts(&input.event_type, input.body.json())
            .map_err(|_| RoomError::InvalidContent(input.event_type.to_string()))?;
        let content = Content::new(
            input
                .body
                .deserialize_as::<serde_json::Value>()
                .map_err(|_| RoomError::InvalidContent(input.event_type.to_string()))?,
            input.event_type.to_string(),
        );

        let builder =
            EventBuilder::state(id.user(), content, room.id(), Some(input.state_key.clone()));
        let (eid, _) = room.add_state(builder).await?;

        Ok(send_state_event::v3::Response::new(eid))
    }
}
