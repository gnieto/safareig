use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity, command::Command, error::ResourceNotFound,
    ruma::api::client::room::get_room_event, Inject,
};

use crate::client::room::{events::EventDecorator, loader::RoomLoader};

#[derive(Inject)]
pub struct GetEventCommand {
    room_loader: RoomLoader,
    event_decorator: Arc<EventDecorator>,
}

#[async_trait]
impl Command for GetEventCommand {
    type Input = get_room_event::v3::Request;
    type Output = get_room_event::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let room = self
            .room_loader
            .get_for_user(&input.room_id, id.user())
            .await
            .map_err(|_| ResourceNotFound)?;

        // TODO: Apply visibility checks
        let event = room.event(&input.event_id).await?.ok_or(ResourceNotFound)?;
        let mut events = vec![event];
        self.event_decorator.decorate_events(&mut events).await;

        let response = get_room_event::v3::Response {
            event: events.pop().unwrap().try_into()?,
        };

        Ok(response)
    }
}
