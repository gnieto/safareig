use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    bus::{Bus, BusMessage},
    command::Command,
    config::HomeserverConfig,
    events::Content,
    ruma::{
        api::client::room::{create_room, create_room::v3::RoomPreset, Visibility},
        events::{
            room::power_levels::RoomPowerLevelsEventContent, AnyInitialStateEvent, StateEventType,
        },
        serde::Raw,
        Int, RoomId, UserId,
    },
    Inject,
};
use serde_json::value::to_raw_value;

use crate::{
    client::room::{RoomError, SafareigStateEventContent},
    core::{
        events::EventBuilder,
        room::{CreateRoomParameters, RoomCreator, RoomVersionRegistry},
    },
};

#[derive(Inject)]
pub struct CreateRoomCommand {
    bus: Bus,
    config: Arc<HomeserverConfig>,
    room_versions: Arc<RoomVersionRegistry>,
    room_creator: RoomCreator,
}

#[async_trait]
impl Command for CreateRoomCommand {
    type Input = create_room::v3::Request;
    type Output = create_room::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let server_name = &self.config.server_name;
        let room_id = RoomId::new(server_name.as_ref());

        let room_version =
            input
                .room_version
                .clone()
                .unwrap_or_else(|| match input.room_alias_name.as_ref() {
                    Some(alias) => self.config.room_version_for_alias(alias),
                    None => self.config.default_room_version(),
                });

        let room_version = self
            .room_versions
            .get(&room_version)
            .ok_or_else(|| RoomError::UnsupportedRoomVersion(room_version))?;

        let mut params = CreateRoomParameters::new(
            id.user().to_owned(),
            room_version.clone(),
            room_id.to_owned(),
        )
        .with_power_levels(Self::power_level_content(&input, id.user())?)
        .with_visibility(input.visibility.clone())
        .with_preset(Self::preset(&input))
        .with_is_direct(input.is_direct);

        if let Some(ref alias) = input.room_alias_name {
            let fully_qualified_alias = format!("#{alias}:{server_name}");
            let room_alias_id = fully_qualified_alias
                .parse()
                .map_err(|_| RoomError::AliasInUse)?;

            params = params.with_alias(room_alias_id);
        }

        if let Some(room_name) = input.name {
            params = params.with_room_name(room_name);
        }

        if let Some(topic) = input.topic {
            params = params.with_room_topic(topic);
        }

        if let Some(create_content) = input.creation_content {
            params = params.with_creation_content(create_content);
        }

        for user_id in input.invite.into_iter() {
            params = params.add_invite_user(user_id);
        }

        for initial_event in input.initial_state {
            let builder = Self::intial_event_to_builder(id.user(), &room_id, &initial_event)?;
            params = params.add_initial_event(builder);
        }

        self.room_creator.create_room(params).await?;

        let _ = self
            .bus
            .send_message(BusMessage::RoomCreated(room_id.to_owned()));

        Ok(create_room::v3::Response::new(room_id))
    }
}

impl CreateRoomCommand {
    fn power_level_content(
        input: &create_room::v3::Request,
        creator: &UserId,
    ) -> Result<RoomPowerLevelsEventContent, RoomError> {
        let mut content = RoomPowerLevelsEventContent::default();
        content
            .users
            .insert(creator.to_owned(), Int::new(100).unwrap());

        if let Some(raw_override) = &input.power_level_content_override {
            // Ruma deserializes the event with default values on all fields. Instead of this, we
            // deserialize the raw json and converts and overrides only those fields which are set
            // on the `power_level_content_override`.
            let override_pl = serde_json::to_value(raw_override.json())?;

            let override_map = override_pl
                .as_object()
                .cloned()
                .ok_or(RoomError::InvalidEventId)?; // TODO: Change error

            for (key, value) in override_map {
                match key.as_str() {
                    "ban" => content.ban = serde_json::from_value(value)?,
                    "events" => content.events = serde_json::from_value(value)?,
                    "events_default" => content.events_default = serde_json::from_value(value)?,
                    "invite" => content.invite = serde_json::from_value(value)?,
                    "kick" => content.kick = serde_json::from_value(value)?,
                    "redact" => content.redact = serde_json::from_value(value)?,
                    "state_default" => content.state_default = serde_json::from_value(value)?,
                    "users" => content.users = serde_json::from_value(value)?,
                    "users_default" => content.users_default = serde_json::from_value(value)?,
                    "notifications" => content.notifications = serde_json::from_value(value)?,
                    _ => tracing::warn!(key = key, "Ignoring unknown key in power level override"),
                }
            }
        }

        Ok(content)
    }

    fn preset(input: &create_room::v3::Request) -> RoomPreset {
        input
            .preset
            .clone()
            .unwrap_or_else(|| match input.visibility {
                Visibility::Public => RoomPreset::PublicChat,
                Visibility::Private => RoomPreset::PrivateChat,
                _ => {
                    tracing::warn!("Unknown custom visibility. Defaulting to private chat");
                    RoomPreset::PrivateChat
                }
            })
    }

    fn intial_event_to_builder(
        creator: &UserId,
        room_id: &RoomId,
        initial_event: &Raw<AnyInitialStateEvent>,
    ) -> Result<EventBuilder, RoomError> {
        let state_key = initial_event
            .get_field::<String>("state_key")
            .map_err(|_| RoomError::InvalidInitialStateEvent)?
            .unwrap_or_default();

        let json_value = initial_event
            .get_field::<serde_json::Value>("content")?
            .ok_or_else(|| RoomError::InvalidInitialStateEvent)?;
        let raw_json = to_raw_value(&json_value)?;

        let event_type = initial_event
            .get_field::<String>("type")?
            .ok_or_else(|| RoomError::InvalidInitialStateEvent)?;
        let event_type = StateEventType::from(event_type);

        let _ = SafareigStateEventContent::from_parts(&event_type, &raw_json)
            .map_err(|_e| RoomError::InvalidInitialStateEvent)?;
        let content = Content::new(json_value, event_type.to_string());

        let builder = EventBuilder::state(creator, content, room_id, Some(state_key));

        Ok(builder)
    }
}
