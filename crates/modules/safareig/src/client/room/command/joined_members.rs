use std::{collections::BTreeMap, sync::Arc};

use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    command::Command,
    ruma::{
        api::client::membership::{joined_members, joined_members::v3::RoomMember},
        OwnedUserId,
    },
    Inject,
};
use safareig_users::storage::UserStorage;

use crate::client::room::loader::RoomLoader;

#[derive(Inject)]
pub struct JoinedMembersCommand {
    room_loader: RoomLoader,
    users: Arc<dyn UserStorage>,
}

#[async_trait]
impl Command for JoinedMembersCommand {
    type Input = joined_members::v3::Request;
    type Output = joined_members::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let room = self
            .room_loader
            .get_for_user(&input.room_id, id.user())
            .await?;
        let state = room.state_at_leaves().await?;
        let members = state.members().await?;
        let users = self.users.users(&members).await?;

        let user_map: BTreeMap<OwnedUserId, RoomMember> = users
            .into_iter()
            .map(|u| {
                (
                    u.user_id,
                    RoomMember {
                        display_name: u.display,
                        avatar_url: u.avatar,
                    },
                )
            })
            .collect();

        Ok(joined_members::v3::Response { joined: user_map })
    }
}

#[cfg(test)]
mod tests {
    use reqwest::StatusCode;
    use safareig_core::{command::Command, ruma::api::client::membership::joined_members};

    use crate::{client::room::command::joined_members::JoinedMembersCommand, testing::TestingDB};

    #[tokio::test]
    async fn it_get_members_for_users_in_room() {
        let testing = TestingDB::default();
        let (id, room) = testing.public_room().await;
        let user_2 = testing.register().await;
        let user_3 = testing.register().await;
        testing
            .join_room(room.room_id.to_owned(), user_2.clone())
            .await
            .unwrap();
        testing
            .join_room(room.room_id.to_owned(), user_3.clone())
            .await
            .unwrap();

        let cmd = testing.command::<JoinedMembersCommand>();

        let response = cmd
            .execute(
                joined_members::v3::Request {
                    room_id: room.room_id.to_owned(),
                },
                id.clone(),
            )
            .await
            .unwrap();

        assert_eq!(3, response.joined.len());
        assert!(response.joined.contains_key(id.user()));
        assert!(response.joined.contains_key(user_2.user()));
        assert!(response.joined.contains_key(user_3.user()));
    }

    #[tokio::test]
    async fn it_returns_forbidden_if_user_is_not_in_room() {
        let testing = TestingDB::default();
        let (_, room) = testing.public_room().await;
        let user_2 = testing.register().await;

        let cmd = testing.command::<JoinedMembersCommand>();
        let response = cmd
            .execute(
                joined_members::v3::Request {
                    room_id: room.room_id.to_owned(),
                },
                user_2,
            )
            .await;

        assert!(response.is_err());
        assert_eq!(StatusCode::FORBIDDEN, response.err().unwrap().status_code);
    }
}
