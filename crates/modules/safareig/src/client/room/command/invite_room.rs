use std::{ops::Deref, sync::Arc};

use async_trait::async_trait;
use reqwest::StatusCode;
use safareig_core::{
    auth::Identity,
    command::Command,
    config::HomeserverConfig,
    events::EventError,
    ruma::{
        api::{
            client::{
                error::{ErrorBody, ErrorKind},
                membership::{invite_user, invite_user::v3::InvitationRecipient},
            },
            federation::membership::create_invite::{
                self, v1::UnsignedEventContent, v2 as remote_invite,
            },
        },
        events::{pdu::Pdu, room::member::MembershipState, AnyStrippedStateEvent, StateEventType},
        serde::Raw,
        RoomVersionId, UserId,
    },
    time::{system_time_to_millis, u64_to_system_time},
    Inject,
};
use safareig_federation::{client::ClientBuilder, keys::LocalKeyProvider, FederationError};
use safareig_users::profile::{member_event_with_profile, ProfileLoader};

use crate::{
    client::room::{loader::RoomLoader, Room, RoomError},
    core::events::{Event, EventBuilder, EventContentExt},
    storage::EventStorage,
};

#[async_trait]
impl Command for InviteRoomCommand {
    type Input = invite_user::v3::Request;
    type Output = invite_user::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let room = self
            .room_loader
            .get(&input.room_id)
            .await?
            .ok_or_else(|| RoomError::RoomNotFound(input.room_id.to_string()))?;

        match &input.recipient {
            InvitationRecipient::UserId { user_id: u } => {
                self.invite_user(&room, id.user(), u).await?;
            }
            _ => {
                return Err(safareig_core::ruma::api::client::Error {
                    status_code: StatusCode::INTERNAL_SERVER_ERROR,
                    body: ErrorBody::Standard {
                        kind: ErrorKind::Unknown,
                        message: "unimplemented".to_string(),
                    },
                })
            }
        }

        Ok(invite_user::v3::Response {})
    }
}

#[derive(Inject)]
pub struct InviteRoomCommand {
    room_loader: RoomLoader,
    profile_loader: ProfileLoader,
    config: Arc<HomeserverConfig>,
    keys: Arc<LocalKeyProvider>,
    clients: Arc<ClientBuilder>,
    event_storage: Arc<dyn EventStorage>,
}

impl InviteRoomCommand {
    async fn invite_user(
        &self,
        room: &Room,
        inviter: &UserId,
        invitee: &UserId,
    ) -> Result<(), RoomError> {
        let room_id = room.id().to_owned();
        let version = room.version();
        let profile = self.profile_loader.load(invitee).await?;
        let content = member_event_with_profile(MembershipState::Invite, profile.as_ref());
        let builder = EventBuilder::state(
            inviter,
            content.into_content()?,
            &room_id,
            Some(invitee.to_string()),
        );

        if invitee.server_name() != self.config.server_name.deref() {
            let event = self.remote_invite(room, builder, invitee).await?;
            room.insert_event(&event, version, true, true).await?;
        } else {
            room.add_state(builder).await?;
        }

        Ok(())
    }

    async fn remote_invite(
        &self,
        room: &Room,
        builder: EventBuilder,
        invitee: &UserId,
    ) -> Result<Event, RoomError> {
        let event = room.complete_event(builder).await?;
        let key = self.keys.current_key().await?;
        let room_version = room.room_version();
        let room_version_id = room_version.id();
        let pdu = room_version
            .event_format()
            .event_to_pdu(&event, key.as_ref())?;
        let invite_state = self.invite_room_state(room).await?;

        let v2_response = self
            .remote_invite_v2(room, invitee, &event, room_version_id, &pdu, &invite_state)
            .await?;
        let response_event = match v2_response {
            Some(response) => response.event,
            None => {
                let v1_response = self
                    .remote_invite_v1(room, invitee, &event, &invite_state)
                    .await?;
                v1_response.event
            }
        };

        let raw: Raw<Pdu> = Raw::from_json(response_event);
        let remote_event = room_version.event_format().pdu_to_event(&raw).await?;

        if remote_event.event_ref != event.event_ref {
            return Err(RoomError::Custom(
                "remote event is distinct from local event".to_string(),
            ));
        }

        Ok(event)
    }

    async fn remote_invite_v1(
        &self,
        room: &Room,
        invitee: &UserId,
        event: &Event,
        invite_state: &[Raw<AnyStrippedStateEvent>],
    ) -> Result<create_invite::v1::Response, FederationError> {
        let state_key = event.state_key.as_ref().ok_or_else(|| {
            FederationError::Custom("expected state key for room member event".to_string())
        })?;
        let state_key = UserId::parse(state_key).map_err(|_| {
            FederationError::Custom("expected valid user id in state key".to_string())
        })?;

        let request = create_invite::v1::Request {
            room_id: room.id().to_owned(),
            event_id: event.event_ref.owned_event_id(),
            sender: event.sender.to_owned(),
            origin: event.sender.server_name().to_owned(),
            origin_server_ts: system_time_to_millis(u64_to_system_time(event.origin_server_ts)),
            kind: StateEventType::from(event.kind.to_string()),
            state_key,
            content: serde_json::from_value(event.content.clone())?,
            unsigned: UnsignedEventContent {
                invite_room_state: Vec::from(invite_state),
            },
        };

        let client = self.clients.client_for(invitee.server_name()).await?;

        client.auth_request(request).await
    }

    async fn remote_invite_v2(
        &self,
        room: &Room,
        invitee: &UserId,
        event: &Event,
        room_version_id: &RoomVersionId,
        pdu: &Raw<Pdu>,
        invite_state: &[Raw<AnyStrippedStateEvent>],
    ) -> Result<Option<remote_invite::Response>, FederationError> {
        let request = remote_invite::Request::new(
            room.id().to_owned(),
            event.event_ref.event_id().to_owned(),
            room_version_id.to_owned(),
            pdu.clone().into_json(),
            Vec::from(invite_state),
        );

        let client = self.clients.client_for(invitee.server_name()).await?;

        match client.auth_request(request).await {
            Ok(response) => Ok(Some(response)),
            Err(e) => {
                if e.matches_status_code(StatusCode::NOT_FOUND) {
                    return Ok(None);
                }

                Err(e)
            }
        }
    }

    async fn invite_room_state(
        &self,
        room: &Room,
    ) -> Result<Vec<Raw<AnyStrippedStateEvent>>, RoomError> {
        let state = room.state_at_leaves().await?;
        let invite_state = state.invite_state(self.event_storage.as_ref()).await?;

        let any_invite: Vec<Raw<AnyStrippedStateEvent>> = invite_state
            .into_iter()
            .map(TryInto::try_into)
            .collect::<Result<_, EventError>>()?;

        Ok(any_invite)
    }
}
