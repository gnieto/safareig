use async_trait::async_trait;
use safareig_core::{
    auth::Identity, command::Command, error::ResourceNotFound, events::EventError,
    ruma::api::client::state::get_state_events, Inject,
};

use crate::client::room::{loader::RoomLoader, RoomError};

#[derive(Inject)]
pub struct GetStateCommand {
    room_loader: RoomLoader,
}

#[async_trait]
impl Command for GetStateCommand {
    type Input = get_state_events::v3::Request;
    type Output = get_state_events::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let room = self
            .room_loader
            .get(&input.room_id)
            .await?
            .ok_or(ResourceNotFound)?;
        let state = room
            .state_for_user(id.user())
            .await?
            .ok_or_else(|| RoomError::UserNotAllowed(id.user().to_owned()))?;

        let state_events = state
            .state_events(None)
            .await?
            .into_iter()
            .map(TryInto::try_into)
            .collect::<Result<Vec<_>, EventError>>()?;

        Ok(get_state_events::v3::Response::new(state_events))
    }
}

#[cfg(test)]
mod tests {
    use reqwest::StatusCode;
    use safareig_core::{command::Command, ruma::api::client::state::get_state_events};

    use crate::{client::room::command::state::GetStateCommand, testing::TestingDB};

    #[tokio::test]
    async fn it_returns_current_state() {
        let db = TestingDB::default();
        let (creator, public) = db.public_room().await;
        let cmd = db.command::<GetStateCommand>();
        let input = get_state_events::v3::Request {
            room_id: public.id().to_owned(),
        };

        let response = cmd.execute(input, creator).await.unwrap();

        assert_eq!(10, response.room_state.len());
        // TODO: More assertions
    }

    #[tokio::test]
    async fn it_returns_forbidden_if_user_is_not_a_room_member() {
        let db = TestingDB::default();
        let (_, public) = db.public_room().await;
        let non_allowed_user = TestingDB::new_identity();
        let cmd = db.command::<GetStateCommand>();
        let input = get_state_events::v3::Request {
            room_id: public.id().to_owned(),
        };

        let response = cmd.execute(input, non_allowed_user).await;

        assert!(response.is_err());
        let err = response.err().unwrap();
        assert_eq!(StatusCode::FORBIDDEN, err.status_code);
    }
}
