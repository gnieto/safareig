use async_trait::async_trait;
use safareig_core::{
    auth::Identity, command::Command, error::ResourceNotFound,
    ruma::api::client::membership::forget_room, Inject,
};

use crate::client::room::loader::RoomLoader;

#[derive(Inject)]
pub struct ForgetRoomCommand {
    room_loader: RoomLoader,
}

#[async_trait]
impl Command for ForgetRoomCommand {
    type Input = forget_room::v3::Request;
    type Output = forget_room::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let room = self
            .room_loader
            .get(&input.room_id)
            .await?
            .ok_or(ResourceNotFound)?;

        room.forget(id.user()).await?;

        Ok(forget_room::v3::Response {})
    }
}
