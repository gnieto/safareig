use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    command::Command,
    ruma::{
        api::client::membership::{
            get_member_events, get_member_events::v3::MembershipEventFilter,
        },
        events::{
            room::member::{MembershipState, RoomMemberEventContent},
            AnyStateEvent, TimelineEventType,
        },
        serde::Raw,
    },
    Inject,
};

use crate::{
    client::room::{loader::RoomLoader, RoomError},
    storage::StateStorage,
};

#[derive(Inject)]
pub struct GetMembersCommand {
    room_loader: RoomLoader,
    state: Arc<dyn StateStorage>,
}

#[async_trait]
impl Command for GetMembersCommand {
    type Input = get_member_events::v3::Request;
    type Output = get_member_events::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, _: Identity) -> Result<Self::Output, Self::Error> {
        let room = self
            .room_loader
            .get(&input.room_id)
            .await?
            .ok_or_else(|| RoomError::RoomNotFound(input.room_id.to_string()))?;
        let state = room.state_at_leaves().await?;

        let events: Vec<_> = self
            .state
            .states(state.id(), None)
            .await?
            .into_iter()
            .filter(|e| e.kind == TimelineEventType::RoomMember)
            .filter_map(|e| {
                let content =
                    serde_json::from_value::<RoomMemberEventContent>(e.content.clone()).unwrap();

                if Self::should_return_event(&input, content.membership.clone()) {
                    Some((e, content))
                } else {
                    None
                }
            })
            .filter_map(|(event, _content)| Raw::<AnyStateEvent>::try_from(event).ok())
            .map(|e| e.cast())
            .collect();

        Ok(get_member_events::v3::Response::new(events))
    }
}

impl GetMembersCommand {
    fn should_return_event(
        input: &get_member_events::v3::Request,
        membership_state: MembershipState,
    ) -> bool {
        let mut include = true;

        if let Some(ref filter) = input.membership {
            include = matches!(
                (filter, &membership_state),
                (MembershipEventFilter::Join, MembershipState::Join)
                    | (MembershipEventFilter::Ban, MembershipState::Ban)
                    | (MembershipEventFilter::Invite, MembershipState::Invite)
                    | (MembershipEventFilter::Leave, MembershipState::Leave)
            );
        }

        if let Some(ref filter) = input.not_membership {
            include = !matches!(
                (filter, &membership_state),
                (MembershipEventFilter::Join, MembershipState::Join)
                    | (MembershipEventFilter::Ban, MembershipState::Ban)
                    | (MembershipEventFilter::Invite, MembershipState::Invite)
                    | (MembershipEventFilter::Leave, MembershipState::Leave)
            );
        }

        include
    }
}
