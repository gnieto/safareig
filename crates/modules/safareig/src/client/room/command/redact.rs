use std::sync::Arc;

use async_trait::async_trait;
use reqwest::StatusCode;
use safareig_core::{
    auth::Identity,
    command::Command,
    error::ResourceNotFound,
    events::Content,
    ruma::{
        api::client::{
            error::{ErrorBody, ErrorKind},
            redact::redact_event,
        },
        canonical_json::RedactedBecause,
        events::{
            room::redaction::{OriginalRoomRedactionEvent, RoomRedactionEventContent},
            AnyTimelineEvent,
        },
        serde::Raw,
        OwnedRoomId, UserId,
    },
    Inject,
};
use thiserror::Error;

use crate::{
    client::room::{loader::RoomLoader, Room, RoomError},
    core::{
        events::{Event, EventBuilder},
        room::RedactionShape,
    },
    storage::{EventOptions, EventStorage},
};

#[derive(Inject)]
pub struct RedactEventCommand {
    event_storage: Arc<dyn EventStorage>,
    room_loader: RoomLoader,
}

#[async_trait]
impl Command for RedactEventCommand {
    type Input = redact_event::v3::Request;
    type Output = redact_event::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let room = self
            .room_loader
            .get_for_user(&input.room_id, id.user())
            .await?;
        let event = self
            .event_storage
            .get_event(&input.event_id)
            .await?
            .ok_or(ResourceNotFound)?;

        if event.room_id != room.id() {
            return Err(RedactionError::RoomIdMismatch(event.room_id, room.id().to_owned()).into());
        }

        if !room.can_redact_event(id.user(), &event).await? {
            return Err(RedactionError::NotAllowed.into());
        }

        if let Some(event_id) = room
            .event_id_for_transaction(room.id(), &input.txn_id)
            .await?
        {
            return Ok(redact_event::v3::Response { event_id });
        }

        // Redact old event
        let redaction_event = self.redaction_event(id.user(), &room, input).await?;
        let redaction_timeline = Raw::<AnyTimelineEvent>::try_from(redaction_event.clone())?
            .cast::<OriginalRoomRedactionEvent>();
        let redacted_because =
            RedactedBecause::from_raw_event(&redaction_timeline).map_err(RoomError::Serde)?;
        // TODO: Add test for unsigned!
        let new_event = event.redact(room.room_version().id(), Some(redacted_because))?;
        self.event_storage
            .put_event(&new_event, EventOptions::default())
            .await?;

        Ok(redact_event::v3::Response {
            event_id: redaction_event.event_ref.owned_event_id(),
        })
    }
}

impl RedactEventCommand {
    async fn redaction_event(
        &self,
        user: &UserId,
        room: &Room,
        input: redact_event::v3::Request,
    ) -> Result<Event, RoomError> {
        let version = room.room_version();
        let redact_content = match input.reason {
            Some(reason) => match version.redaction_shape() {
                RedactionShape::EventIdInContent => {
                    RoomRedactionEventContent::new_v11(input.event_id.to_owned())
                        .with_reason(reason)
                }
                RedactionShape::EventIdInTopLevel => {
                    RoomRedactionEventContent::new_v1().with_reason(reason)
                }
            },
            None => RoomRedactionEventContent::default(),
        };
        let mut redaction_event = EventBuilder::builder(
            user,
            Content::from_event_content(&redact_content)?,
            room.id(),
        );

        if let RedactionShape::EventIdInTopLevel = version.redaction_shape() {
            redaction_event = redaction_event.redacts(input.event_id.to_owned());
        }

        let options = EventOptions::default().set_transaction_id(&input.txn_id);
        let (event_id, _) = room.add_event(redaction_event, options).await?;

        let redaction_event = self
            .event_storage
            .get_event(&event_id)
            .await?
            .ok_or_else(|| RoomError::EventNotFound(event_id.to_owned()))?;

        Ok(redaction_event)
    }
}

#[derive(Debug, Error)]
enum RedactionError {
    #[error("mismatching room id")]
    RoomIdMismatch(OwnedRoomId, OwnedRoomId),
    #[error("user is not allowed to redact")]
    NotAllowed,
}

impl From<RedactionError> for safareig_core::ruma::api::client::Error {
    fn from(e: RedactionError) -> Self {
        match e {
            RedactionError::RoomIdMismatch(ref event_room, ref path_room) => {
                tracing::warn!(
                    event_room = event_room.as_str(),
                    path_room = path_room.as_str(),
                    "mismatching room id",
                );

                safareig_core::ruma::api::client::Error {
                    status_code: StatusCode::BAD_REQUEST,
                    body: ErrorBody::Standard {
                        kind: ErrorKind::InvalidParam,
                        message: e.to_string(),
                    },
                }
            }
            RedactionError::NotAllowed => safareig_core::ruma::api::client::Error {
                status_code: StatusCode::FORBIDDEN,
                body: ErrorBody::Standard {
                    kind: ErrorKind::forbidden(),
                    message: "user is not allowed to redact".to_string(),
                },
            },
        }
    }
}

#[cfg(test)]
mod tests {
    use reqwest::StatusCode;
    use safareig_core::{
        command::Command,
        ruma::{
            api::client::redact::redact_event, events::room::redaction::RoomRedactionEventContent,
            EventId, RoomVersionId, TransactionId,
        },
    };

    use super::RedactEventCommand;
    use crate::{storage::EventStorage, testing::TestingDB};

    #[tokio::test]
    async fn it_can_not_redact_a_non_existing_event() {
        let db = TestingDB::default();
        let (creator, room) = db.public_room().await;
        let cmd = db.command::<RedactEventCommand>();
        let input = redact_event::v3::Request {
            room_id: room.id().to_owned(),
            event_id: EventId::new(room.id().server_name().unwrap()),
            txn_id: TransactionId::new(),
            reason: None,
        };

        let err = cmd.execute(input, creator).await.err().unwrap();

        assert_eq!(StatusCode::NOT_FOUND, err.status_code);
    }

    #[tokio::test]
    async fn it_can_not_redact_an_event_with_mismatching_room_id() {
        let db = TestingDB::default();
        let (creator, room) = db.public_room().await;
        let (_, room2) = db.public_room().await;
        let room2_create_event_id = room2.create_event().await.unwrap().event_id;
        let cmd = db.command::<RedactEventCommand>();
        let input = redact_event::v3::Request {
            room_id: room.id().to_owned(),
            event_id: room2_create_event_id,
            txn_id: TransactionId::new(),
            reason: None,
        };

        let err = cmd.execute(input, creator).await.err().unwrap();

        assert_eq!(StatusCode::BAD_REQUEST, err.status_code);
    }

    #[tokio::test]
    async fn a_non_allowed_user_can_not_redact_an_event() {
        let db = TestingDB::default();
        let (creator, room) = db.public_room().await;
        let joiner = db.register().await;
        let event_id = db.talk(room.id(), &creator, "hola hola").await.unwrap();
        let _ = db
            .join_room(room.id().to_owned(), joiner.to_owned())
            .await
            .unwrap();
        let cmd = db.command::<RedactEventCommand>();
        let input = redact_event::v3::Request {
            room_id: room.id().to_owned(),
            event_id,
            txn_id: TransactionId::new(),
            reason: None,
        };

        let err = cmd.execute(input, joiner).await.err().unwrap();

        assert_eq!(StatusCode::FORBIDDEN, err.status_code);
    }

    #[tokio::test]
    async fn an_allowed_user_can_redact_event_and_maintains_event_id() {
        let db = TestingDB::default();
        let (creator, room) = db.public_room().await;
        let event_id = db.talk(room.id(), &creator, "hola hola").await.unwrap();
        let cmd = db.command::<RedactEventCommand>();
        let input = redact_event::v3::Request {
            room_id: room.id().to_owned(),
            event_id: event_id.to_owned(),
            txn_id: TransactionId::new(),
            reason: Some("raó".to_string()),
        };

        let response = cmd.execute(input, creator).await.unwrap();

        let redacted_event = room.event(&event_id).await.unwrap().unwrap();
        assert_eq!(redacted_event.content.as_object().unwrap().len(), 0);

        let event = db.event_at_leaf(room.id()).await;
        assert_eq!(response.event_id, event.event_ref.event_id());
        let redact_content = event.content_as::<RoomRedactionEventContent>().unwrap();
        assert_eq!(redact_content.reason.unwrap(), "raó");
        assert_eq!(event.redacts.unwrap(), event_id);
    }

    #[tokio::test]
    async fn redact_events_is_idempotent() {
        let db = TestingDB::default();
        let (creator, room) = db.public_room().await;
        let event_id = db.talk(room.id(), &creator, "hola hola").await.unwrap();
        let cmd = db.command::<RedactEventCommand>();
        let txn_id = TransactionId::new();
        let input = redact_event::v3::Request {
            room_id: room.id().to_owned(),
            event_id: event_id.to_owned(),
            txn_id: txn_id.clone(),
            reason: Some("raó".to_string()),
        };

        let second_input = redact_event::v3::Request {
            room_id: room.id().to_owned(),
            event_id: event_id.to_owned(),
            txn_id,
            reason: Some("raó".to_string()),
        };

        let response = cmd.execute(input, creator.clone()).await.unwrap();
        let second_response = cmd.execute(second_input, creator).await.unwrap();

        assert_eq!(response.event_id, second_response.event_id);
    }

    #[tokio::test]
    async fn redact_event_in_v11_sets_redacted_event_id_in_content() {
        use std::sync::Arc;

        let db = TestingDB::default();
        let (creator, room) = db.public_room_version(RoomVersionId::V11).await;
        let event_id = db.talk(room.id(), &creator, "hola hola").await.unwrap();
        let cmd = db.command::<RedactEventCommand>();
        let txn_id = TransactionId::new();
        let input = redact_event::v3::Request {
            room_id: room.id().to_owned(),
            event_id: event_id.to_owned(),
            txn_id: txn_id.clone(),
            reason: Some("raó".to_string()),
        };

        let response = cmd.execute(input, creator.clone()).await.unwrap();

        let event_storage = db.container.service::<Arc<dyn EventStorage>>();
        let event = event_storage
            .get_event(&response.event_id)
            .await
            .unwrap()
            .unwrap();

        assert!(event.redacts.is_none());
        let content = event.content_as::<RoomRedactionEventContent>().unwrap();
        assert_eq!(content.redacts.unwrap(), event_id);
    }
}
