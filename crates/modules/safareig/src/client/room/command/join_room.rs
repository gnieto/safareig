use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    command::Command,
    ruma::{
        api::client::membership::{join_room_by_id, join_room_by_id_or_alias},
        OwnedRoomAliasId,
    },
    Inject,
};
use safareig_rooms::alias::AliasResolver;

use crate::client::room::membership::{RoomMembership, RoomMembershipHandler};

#[derive(Inject)]
pub struct JoinByRoomOrAliasCommand {
    alias_resolver: Arc<dyn AliasResolver>,
    join_room: RoomMembershipHandler,
}

#[async_trait]
impl Command for JoinByRoomOrAliasCommand {
    type Input = join_room_by_id_or_alias::v3::Request;
    type Output = join_room_by_id_or_alias::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let room_id = if input.room_id_or_alias.is_room_id() {
            input.room_id_or_alias.try_into().unwrap()
        } else {
            let alias: OwnedRoomAliasId = input.room_id_or_alias.try_into().unwrap();
            self.alias_resolver.resolve_alias(&alias).await?.room_id
        };

        let _ = self.join_room.join(id.user(), &room_id).await?;

        Ok(join_room_by_id_or_alias::v3::Response { room_id })
    }
}

#[derive(Inject)]
pub struct JoinRoomCommand {
    join_room: RoomMembershipHandler,
}

#[async_trait]
impl Command for JoinRoomCommand {
    type Input = join_room_by_id::v3::Request;
    type Output = join_room_by_id::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let _ = self.join_room.join(id.user(), &input.room_id).await?;

        Ok(join_room_by_id::v3::Response {
            room_id: input.room_id.to_owned(),
        })
    }
}
