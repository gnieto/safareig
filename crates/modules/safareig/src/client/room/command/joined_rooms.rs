use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    command::Command,
    ruma::{
        api::client::membership::joined_rooms, events::room::member::MembershipState, OwnedRoomId,
    },
    Inject,
};

use crate::storage::RoomStorage;

#[derive(Inject)]
pub struct JoinedRoomsCommand {
    rooms: Arc<dyn RoomStorage>,
}

#[async_trait]
impl Command for JoinedRoomsCommand {
    type Input = joined_rooms::v3::Request;
    type Output = joined_rooms::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, _: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let joined_rooms: Vec<OwnedRoomId> = self
            .rooms
            .membership(id.user())
            .await?
            .into_iter()
            .filter(|(_k, membership)| membership.state == MembershipState::Join)
            .map(|tuple| tuple.0)
            .collect();

        Ok(joined_rooms::v3::Response { joined_rooms })
    }
}

#[cfg(test)]
mod tests {
    use safareig_core::{command::Command, ruma::api::client::membership::joined_rooms};

    use crate::{client::room::command::joined_rooms::JoinedRoomsCommand, testing::TestingDB};

    #[tokio::test]
    async fn it_retrieves_empty_joined_rooms_on_a_new_user() {
        let db = TestingDB::default();
        let user = db.register().await;
        let request = joined_rooms::v3::Request::new();
        let cmd = db.command::<JoinedRoomsCommand>();

        let response = cmd.execute(request, user).await.unwrap();

        assert_eq!(0, response.joined_rooms.len());
    }

    #[tokio::test]
    async fn it_retrieves_rooms_but_excludes_invites() {
        let db = TestingDB::default();
        let (creator, room) = db.public_room().await;
        let request = joined_rooms::v3::Request::new();
        let cmd = db.command::<JoinedRoomsCommand>();

        let response = cmd.execute(request, creator).await.unwrap();

        assert_eq!(1, response.joined_rooms.len());
        assert_eq!(&response.joined_rooms[0], room.id());
    }
}
