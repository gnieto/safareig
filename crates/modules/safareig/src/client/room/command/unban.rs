use async_trait::async_trait;
use safareig_core::{
    auth::Identity, command::Command, ruma::api::client::membership::unban_user, Inject,
};

use crate::client::room::membership::{
    MembershipChange, Reason, RoomMembership, RoomMembershipHandler,
};

#[derive(Inject)]
pub struct UnbanUserCommand {
    room_membership: RoomMembershipHandler,
}

#[async_trait]
impl Command for UnbanUserCommand {
    type Input = unban_user::v3::Request;
    type Output = unban_user::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let reason = input.reason.map(Reason::from);

        let change = MembershipChange::delegated(id.user(), &input.user_id, &input.room_id, reason);

        self.room_membership.leave(&change).await?;

        Ok(unban_user::v3::Response {})
    }
}
