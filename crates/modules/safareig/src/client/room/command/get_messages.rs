use std::{collections::HashSet, convert::TryFrom, str::FromStr};

use async_trait::async_trait;
use reqwest::StatusCode;
use safareig_core::{
    auth::Identity,
    command::Command,
    error::ResourceNotFound,
    events::EventError,
    pagination::{self, Boundary, Limit, Pagination, Range},
    ruma::{
        api::{
            client::{
                error::{ErrorBody, ErrorKind},
                message::get_message_events,
            },
            Direction,
        },
        events::room::member::MembershipState,
        RoomId, UserId,
    },
    Inject, StreamToken,
};
use safareig_sync_api::SyncToken;
use tracing::instrument;

use crate::{
    client::{
        room::{loader::RoomLoader, RoomError, RoomStream},
        sync::{state::LazyLoadMemberAdder, stream::LazyLoadToken},
        topological::TopologicalEvents,
    },
    core::filter::RoomFilter,
    storage::{StreamFilter, StreamSource, TopologicalToken, TopologicalTokenWithLazyLoading},
};

pub enum MessageToken {
    Stream(SyncToken),
    Topologic(TopologicalTokenWithLazyLoading),
}

impl TryFrom<&str> for MessageToken {
    type Error = ();

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        if let Ok(topo_token) = TopologicalTokenWithLazyLoading::from_str(value) {
            return Ok(MessageToken::Topologic(topo_token));
        }
        let stream = SyncToken::try_from(value)?;

        Ok(MessageToken::Stream(stream))
    }
}

#[derive(Inject)]
pub struct GetMessagesCommand {
    room_loader: RoomLoader,
    topological_events: TopologicalEvents,
    room_stream: RoomStream,
    lazy_load: LazyLoadMemberAdder,
}

#[async_trait]
impl Command for GetMessagesCommand {
    type Input = get_message_events::v3::Request;
    type Output = get_message_events::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let token = input
            .from
            .as_ref()
            .map(|from| MessageToken::try_from(from.as_str()))
            .transpose()
            .map_err(|_| ::safareig_core::ruma::api::client::Error {
                status_code: StatusCode::BAD_REQUEST,
                body: ErrorBody::Standard {
                    kind: ErrorKind::Unknown,
                    message: "invalid_token".to_string(),
                },
            })?;

        match token {
            Some(MessageToken::Stream(stream)) => {
                let topological_token = self
                    .get_topological_token_from_stream_token(
                        &input.room_id,
                        stream.event(),
                        input.dir,
                    )
                    .await?;

                match topological_token {
                    Some(topological_token) => {
                        let topological_token = match stream.extension_token::<LazyLoadToken>()? {
                            Some(lazy_token) => topological_token.with_lazy_load_token(lazy_token),
                            None => {
                                topological_token.with_lazy_load_token(LazyLoadToken::default())
                            }
                        };

                        Ok(self
                            .topological_messages(Some(topological_token), input, id)
                            .await?)
                    }
                    None => Ok(self.topological_messages(None, input, id).await?),
                }
            }
            None => Ok(self.topological_messages(None, input, id).await?),
            Some(MessageToken::Topologic(topo)) => {
                Ok(self.topological_messages(Some(topo), input, id).await?)
            }
        }
    }
}

impl GetMessagesCommand {
    #[instrument(skip_all)]
    async fn topological_messages(
        &self,
        from: Option<TopologicalTokenWithLazyLoading>,
        input: <Self as Command>::Input,
        id: Identity,
    ) -> Result<<Self as Command>::Output, <Self as Command>::Error> {
        let room = self
            .room_loader
            .get(&input.room_id)
            .await?
            .ok_or(ResourceNotFound)?;

        let state = room.state_at_leaves().await?;
        let membership_event = room.membership(&state, id.user()).await;
        let mut limited_topology = None;

        if let Ok(event) = membership_event {
            if let MembershipState::Leave = event.content.membership {
                let leave_event = room.event(&event.event_id).await?;

                if let Some(event) = leave_event {
                    limited_topology = Some(TopologicalToken::new(event.depth));
                }
            };
        }

        let limit: u16 = u16::try_from(input.limit).unwrap_or_default();
        let limit = std::cmp::min(limit, 100);

        let filter_definition = input.filter.clone();
        let room_filter = RoomFilter::new(&filter_definition);

        let to = input
            .to
            .map(|s| TopologicalToken::from_str(&s))
            .transpose()
            .map_err(|_| RoomError::InvalidStreamToken)?;

        let mut range: Range<_> = match input.dir {
            Direction::Backward => (
                to.map(pagination::Boundary::Inclusive)
                    .unwrap_or(pagination::Boundary::Unlimited),
                from.clone()
                    .map(|t| pagination::Boundary::Inclusive(t.0))
                    .unwrap_or(pagination::Boundary::Unlimited),
            ),
            Direction::Forward => {
                let token_at_leaves = TopologicalToken::new(room.depth_at_leaves().await);

                (
                    from.clone()
                        .map(|t| pagination::Boundary::Inclusive(t.0))
                        .unwrap_or(pagination::Boundary::Inclusive(token_at_leaves)),
                    to.map(pagination::Boundary::Exclusive)
                        .unwrap_or(pagination::Boundary::Unlimited),
                )
            }
        }
        .into();

        if let Some(token) = limited_topology {
            range = range.limit_to_upper_bound(token);
        }

        let result = self
            .topological_events
            .topological_events(room.id(), range.clone(), limit, input.dir)
            .await?;

        let next = match input.dir {
            Direction::Backward => result.next().cloned().map(|n| n.prev()),
            Direction::Forward => result.next().cloned(),
        };

        let previous = result.previous().cloned();
        let events = result.records();

        let from_token = previous
            .or_else(|| from.clone().map(|t| t.0))
            .or_else(|| events.first().map(|e| e.topological_token()))
            .or_else(|| match input.dir {
                Direction::Backward => range.to_token(),
                Direction::Forward => range.from_token(),
            })
            .ok_or_else(|| RoomError::Custom("could not find from token".into()))?;

        let mut state = Vec::new();
        let mut lazy_token = from.and_then(|token| token.1);

        if room_filter.has_lazy_load() {
            let members: HashSet<&UserId> = events.iter().map(|e| e.sender.as_ref()).collect();
            let current_lazy_token = lazy_token.unwrap_or_default();

            self.lazy_load
                .add_lazy_load_members(
                    &mut state,
                    room.id(),
                    members,
                    &current_lazy_token,
                    room_filter.lazy_load_options(),
                )
                .await?;

            lazy_token = Some(current_lazy_token)
        }

        let any_events = events
            .into_iter()
            .filter(|event| room_filter.event_allowed(event))
            .map(TryInto::try_into)
            .collect::<Result<Vec<_>, EventError>>()?;

        let response = get_message_events::v3::Response {
            chunk: any_events,
            start: from_token
                .with_optional_lazy_load_token(lazy_token.clone())
                .to_string(),
            end: next.map(|next| next.with_optional_lazy_load_token(lazy_token).to_string()),
            state,
        };

        Ok(response)
    }

    // TODO: Move this method to `RoomStream`?
    async fn get_topological_token_from_stream_token(
        &self,
        room_id: &RoomId,
        token: StreamToken,
        direction: Direction,
    ) -> Result<Option<TopologicalToken>, RoomError> {
        let range = match direction {
            Direction::Forward => (Boundary::Inclusive(token), Boundary::Unlimited),
            Direction::Backward => (Boundary::Inclusive(token), Boundary::Unlimited),
        };
        let stream_filter = StreamFilter {
            filter: None,
            room_id: Some(room_id),
            source: StreamSource::All,
        };
        let query = Pagination::new(range.into(), Some(stream_filter))
            .with_limit(Limit::from(u32::try_from(1).unwrap()))
            .with_direction(safareig_core::pagination::Direction::Forward);
        let pagination = self.room_stream.stream_room_events(query).await?;

        let topological = pagination
            .records()
            .into_iter()
            .next()
            .map(|(_, event)| event.topological_token());

        Ok(topological)
    }
}
