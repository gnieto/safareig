use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    command::Command,
    error::error_chain,
    events::Content,
    ruma::{api::client::message::send_message_event, events::TimelineEventType, UserId},
    Inject,
};
use safareig_presence::PresenceUpdater;
use safareig_typing::TypingTracker;

use crate::{
    client::room::{loader::RoomLoader, RoomError, SafareigMessageLikeEventContent},
    core::events::EventBuilder,
    storage::EventOptions,
};

#[derive(Inject)]
pub struct SendMessageCommand {
    room_loader: RoomLoader,
    presence_updater: Option<PresenceUpdater>,
    typing: TypingTracker,
}

#[async_trait]
impl Command for SendMessageCommand {
    type Input = send_message_event::v3::Request;
    type Output = send_message_event::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let room = self
            .room_loader
            .get(&input.room_id)
            .await?
            .ok_or_else(|| RoomError::RoomNotFound(input.room_id.to_string()))?;

        if input.txn_id.as_str() != "" {
            if let Some(event_id) = room
                .event_id_for_transaction(room.id(), &input.txn_id)
                .await?
            {
                return Ok(send_message_event::v3::Response::new(event_id));
            }
        }

        let _event_type = TimelineEventType::from(input.event_type.to_string());
        SafareigMessageLikeEventContent::from_parts(&input.event_type, input.body.json())
            .map_err(RoomError::Serde)?;
        let content = Content::new(
            input
                .body
                .deserialize_as::<serde_json::Value>()
                .map_err(RoomError::Serde)?,
            input.event_type.to_string(),
        );
        let builder = EventBuilder::builder(id.user(), content, room.id())
            .transaction(input.txn_id.to_string(), &id);

        let options = EventOptions::default().set_transaction_id(&input.txn_id);
        let event = room.add_event(builder, options).await?;

        self.typing.stop_typing(&room.room_id, id.user());
        self.update_activity_for_user(id.user()).await;

        Ok(send_message_event::v3::Response::new(event.0))
    }
}

impl SendMessageCommand {
    async fn update_activity_for_user(&self, user_id: &UserId) {
        if let Some(updater) = &self.presence_updater {
            if let Err(e) = updater.user_active(user_id).await {
                tracing::error!(err = error_chain(&e), "Could not update presence",);
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use std::sync::Arc;

    use reqwest::StatusCode;
    use safareig_core::{
        command::Command,
        ruma::{
            api::client::{error::ErrorKind, message::send_message_event},
            events::{
                room::message::{MessageType, RoomMessageEventContent, TextMessageEventContent},
                AnyMessageLikeEventContent, EventContent, MessageLikeEventType,
            },
            serde::Raw,
            TransactionId,
        },
    };
    use safareig_testing::error::RumaErrorExtension;
    use safareig_typing::TypingTracker;

    use crate::{
        client::room::{command::send_message::SendMessageCommand, loader::RoomLoader},
        storage::EventStorage,
        testing::{event_content_to_raw, TestingDB},
    };

    #[tokio::test]
    async fn it_adds_tx_id_on_unsigned_data() {
        let db = TestingDB::default();
        let (creator, room) = db.public_room().await;

        let event_id = db.talk(&room.room_id, &creator, "msg").await.unwrap();
        let events = db.container.service::<Arc<dyn EventStorage>>();

        let stored_event = events.get_event(&event_id).await.unwrap().unwrap();

        assert!(!stored_event
            .unsigned
            .get("transaction_id")
            .unwrap()
            .as_str()
            .unwrap()
            .is_empty());
    }

    #[tokio::test]
    async fn allows_extra_content_on_messages() {
        let db = TestingDB::default();
        let (creator, room) = db.public_room().await;
        let tracker = db.container.service::<TypingTracker>();
        let room_loader = db.container.service::<RoomLoader>();
        let cmd = SendMessageCommand::new(room_loader, None, tracker);
        let content = AnyMessageLikeEventContent::RoomMessage(RoomMessageEventContent {
            msgtype: MessageType::Text(TextMessageEventContent {
                body: "msg".to_string(),
                formatted: None,
            }),
            relates_to: None,
            mentions: None,
        });

        let mut json_value = serde_json::to_value(&content).unwrap();
        json_value.as_object_mut().unwrap().insert(
            "extra_key".to_string(),
            serde_json::Value::String("any_value".to_string()),
        );

        let raw = Raw::from_json(serde_json::value::to_raw_value(&json_value).unwrap());

        let input = send_message_event::v3::Request {
            room_id: room.id().to_owned(),
            event_type: content.event_type(),
            txn_id: (<&TransactionId>::from("txnid")).to_owned(),
            body: raw,
            timestamp: None,
        };

        let response = cmd.execute(input, creator.clone()).await.unwrap();

        let events = db.container.service::<Arc<dyn EventStorage>>();
        let stored_event = events.get_event(&response.event_id).await.unwrap().unwrap();
        assert_eq!(
            "txnid",
            stored_event
                .unsigned
                .get("transaction_id")
                .unwrap()
                .as_str()
                .unwrap()
        );
        assert_eq!(
            "any_value",
            stored_event
                .content
                .as_object()
                .unwrap()
                .get("extra_key")
                .unwrap()
        );
    }

    #[tokio::test]
    async fn non_member_can_not_send_mssages() {
        let db = TestingDB::default();
        let (_creator, room) = db.public_room().await;
        let non_member = db.register().await;

        let result = db.talk(&room.room_id, &non_member, "msg").await;

        assert!(result.is_err());
        let matrix_error = result.err().unwrap();
        assert_eq!(matrix_error.status_code, StatusCode::FORBIDDEN);
        assert_eq!(matrix_error.kind(), ErrorKind::forbidden());
    }

    #[tokio::test]
    async fn send_a_message_cancels_sender_typing_event() {
        let db = TestingDB::default();
        let typing = db.container.service::<TypingTracker>();
        let (creator, room) = db.public_room().await;
        typing.start_typing(room.id(), creator.user(), None);
        assert_eq!(1, typing.typing_users(room.id()).len());

        db.talk(room.id(), &creator, "msg").await.unwrap();

        assert_eq!(0, typing.typing_users(room.id()).len());
    }

    #[tokio::test]
    #[allow(dead_code)]
    async fn send_message_with_same_txn_id_returns_same_event_id() {
        let db = TestingDB::default();
        let (creator, room) = db.public_room().await;
        let txn_id = TransactionId::new();
        let msg = "hola!";

        let tracker = db.container.service::<TypingTracker>();
        let room_loader = db.container.service::<RoomLoader>();
        let cmd = SendMessageCommand::new(room_loader, None, tracker);
        let content = AnyMessageLikeEventContent::RoomMessage(RoomMessageEventContent {
            msgtype: MessageType::Text(TextMessageEventContent {
                body: msg.to_string(),
                formatted: None,
            }),
            relates_to: None,
            mentions: None,
        });
        let (event_type, body) = event_content_to_raw(content);

        let input = send_message_event::v3::Request {
            room_id: room.id().to_owned(),
            txn_id: txn_id.clone(),
            event_type: MessageLikeEventType::from(event_type.to_string().as_str()),
            body: body.clone(),
            timestamp: None,
        };
        let response = cmd.execute(input, creator.clone()).await.unwrap();

        let input = send_message_event::v3::Request {
            room_id: room.id().to_owned(),
            txn_id,
            event_type: MessageLikeEventType::from(event_type.to_string().as_str()),
            body,
            timestamp: None,
        };
        let response2 = cmd.execute(input, creator.clone()).await.unwrap();

        assert_eq!(response.event_id, response2.event_id);
    }
}
