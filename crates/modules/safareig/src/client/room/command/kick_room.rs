use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    command::Command,
    ruma::{
        api::client::membership::kick_user,
        events::room::member::{MembershipState, RoomMemberEventContent},
    },
    Inject,
};

use crate::client::room::{
    loader::RoomLoader,
    membership::{MembershipChange, Reason, RoomMembership, RoomMembershipHandler},
    state::StateIndex,
    RoomError,
};

#[derive(Inject)]
pub struct KickRoomCommand {
    room_membership: RoomMembershipHandler,
    room_loader: RoomLoader,
}

#[async_trait]
impl Command for KickRoomCommand {
    type Input = kick_user::v3::Request;
    type Output = kick_user::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let room = self
            .room_loader
            .get_for_user(&input.room_id, id.user())
            .await?;
        let state = room.state_at_leaves().await?;

        let key = StateIndex::memberhip(input.user_id.to_string());
        let current_content = room
            .event_content::<RoomMemberEventContent>(&key, &state)
            .await?
            .ok_or_else(|| RoomError::UserNotAllowed(id.user().to_owned()))?;

        if let MembershipState::Leave = current_content.content.membership {
            return Err(RoomError::UserNotAllowed(id.user().to_owned()).into());
        }

        let change = MembershipChange::delegated(
            id.user(),
            input.user_id.as_ref(),
            &input.room_id,
            input.reason.map(Reason::from),
        );

        self.room_membership.leave(&change).await?;

        Ok(kick_user::v3::Response {})
    }
}
