use std::{
    collections::{HashMap, HashSet},
    sync::Arc,
};

use safareig_core::{
    ruma::{events::room::member::MembershipState, OwnedRoomId, UserId},
    storage::StorageError,
    Inject,
};
use safareig_ignored_users::IgnoreUsersRepository;
use safareig_presence::PresenceAllower;

use crate::storage::{Membership, RoomStorage};

/// PresenceAllower checks if a user or server is allowed to see a given presence update
#[derive(Inject)]
pub struct SharedRoomPresenceAllower {
    room: Arc<dyn RoomStorage>,
    ignored_users: Option<Arc<dyn IgnoreUsersRepository>>,
}

impl SharedRoomPresenceAllower {
    fn filter_rooms(rooms: &HashMap<OwnedRoomId, Membership>) -> HashSet<OwnedRoomId> {
        rooms
            .iter()
            .filter(|entry| entry.1.state == MembershipState::Join)
            .map(|entry| entry.0.to_owned())
            .collect()
    }
}

#[async_trait::async_trait]
impl PresenceAllower for SharedRoomPresenceAllower {
    // TODO: Probably this could be optimitzed via cuckoo filters or other similar probabilistic data structure.
    async fn is_user_allowed_to_see(
        &self,
        requesting_user: &UserId,
        target_user: &UserId,
    ) -> Result<bool, StorageError> {
        if requesting_user == target_user {
            return Ok(true);
        }

        if let Some(repository) = &self.ignored_users {
            let ignored = repository.ignored_users(requesting_user).await?;
            tracing::error!(
                ?ignored,
                "ignored user list {}",
                ignored.contains(target_user)
            );
            if ignored.contains(target_user) {
                return Ok(false);
            }
        }

        let requesting_rooms = self.room.membership(requesting_user).await?;
        let target_rooms = self.room.membership(target_user).await?;

        let joined_requesting_rooms = Self::filter_rooms(&requesting_rooms);
        let joined_target_rooms = Self::filter_rooms(&target_rooms);

        let disjoint = joined_requesting_rooms.is_disjoint(&joined_target_rooms);

        Ok(!disjoint)
    }
}
