use std::sync::Arc;

use safareig_core::{
    pagination::{PaginationResponse, Range},
    ruma::{api::Direction, RoomId},
    Inject,
};

use super::room::{loader::RoomLoader, Room, RoomError};
use crate::{
    core::events::Event,
    federation::backfill::{BackfillEventFetcher, BackfillJob, Backfiller, RemoteSelectorPolicy},
    storage::{BackwardExtremity, RoomTopologyStorage, TopologicalToken},
};

#[derive(Clone, Inject)]
pub struct TopologicalEvents {
    federated_events: FederatedTopologicalEvents,
    room_loader: RoomLoader,
}

impl TopologicalEvents {
    pub async fn topological_events(
        &self,
        room_id: &RoomId,
        range: Range<TopologicalToken>,
        limit: u16,
        direction: Direction,
    ) -> Result<PaginationResponse<TopologicalToken, Event>, RoomError> {
        let room = self
            .room_loader
            .get(room_id)
            .await?
            .ok_or_else(|| RoomError::RoomNotFound(room_id.as_str().to_string()))?;

        self.federated_events
            .topological_events(&room, range, limit, direction)
            .await
    }
}

#[derive(Clone, Inject)]
pub struct FederatedTopologicalEvents {
    topology: Arc<dyn RoomTopologyStorage>,
    backfiller: Backfiller,
}

impl FederatedTopologicalEvents {
    #[tracing::instrument(skip_all)]
    async fn topological_events(
        &self,
        local_room: &Room,
        range: Range<TopologicalToken>,
        limit: u16,
        direction: Direction,
    ) -> Result<PaginationResponse<TopologicalToken, Event>, RoomError> {
        // TODO: Boundary<TopologicalToken> maybe should be copy?
        let result = local_room
            .topological_events(range.clone(), limit, direction)
            .await?;
        let events = result;

        // Check if there's any backward extremity on the range. Check inclusively because the
        // given token may be a backward extremity and we should backfill it's prev events.

        let backward_range: Range<TopologicalToken> =
            (range.from().inclusive(), range.to().inclusive()).into();
        let backwards = self
            .topology
            .backward_extremities(local_room.id(), backward_range.clone())
            .await?;

        if !backwards.is_empty() {
            self.backfill_room(local_room, backwards, backward_range)
                .await?;

            let response = local_room
                .topological_events(range.clone(), limit, direction)
                .await?;

            Ok(response)
        } else {
            Ok(events)
        }
    }

    async fn backfill_room(
        &self,
        local_room: &Room,
        backward_extremities: Vec<BackwardExtremity>,
        _range: Range<TopologicalToken>,
    ) -> Result<(), RoomError> {
        let policy = RemoteSelectorPolicy::Random(local_room.id().to_owned());
        let event_fetcher = BackfillEventFetcher::new();
        let mut backfill_job = BackfillJob::new(backward_extremities, policy);
        backfill_job
            .backfill(local_room, &self.backfiller, &event_fetcher)
            .await?;

        Ok(())
    }
}
