use std::collections::BTreeMap;

use safareig_core::{
    auth::Identity, command::Command, ruma::api::client::discovery::get_supported_versions, Inject,
};

#[derive(Inject)]
pub struct VersionsCommand;

#[async_trait::async_trait]
impl Command for VersionsCommand {
    type Input = get_supported_versions::Request;
    type Output = get_supported_versions::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, _input: Self::Input, _: Identity) -> Result<Self::Output, Self::Error> {
        let mut unstable_features = BTreeMap::new();

        unstable_features.insert("org.matrix.msc3440".to_string(), true);
        unstable_features.insert("m.lazy_load_members".to_string(), true);
        unstable_features.insert("org.matrix.e2e_cross_signing".to_string(), true);

        let mut response = get_supported_versions::Response::new(vec![
            "r0.6.0".to_string(),
            "r0.6.1".to_string(),
            "v1.0".to_string(),
            "v1.1".to_string(),
            "v1.2".to_string(),
            "v1.3".to_string(),
            "v1.4".to_string(),
            "v1.5".to_string(),
            "v1.6".to_string(),
            "v1.7".to_string(),
            "v1.8".to_string(),
        ]);
        response.unstable_features = unstable_features;

        Ok(response)
    }
}
