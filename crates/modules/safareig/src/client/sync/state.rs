use std::{borrow::Borrow, collections::HashSet, sync::Arc};

use itertools::Itertools;
use safareig_core::{
    auth::Identity,
    events::EventError,
    pagination::{self, Boundary},
    ruma::{
        api::client::{
            filter::LazyLoadOptions,
            sync::sync_events::v3::{State, Timeline as RumaTimeline},
        },
        events::{AnySyncTimelineEvent, StateEventType},
        serde::Raw,
        RoomId, UserId,
    },
    Inject, StreamToken,
};
use safareig_sync_api::context::SyncMode;

use super::stream::{LazyLoadToken, RoomSyncContext};
use crate::{
    client::room::{
        loader::RoomLoader,
        state::{LazyLoadCache, LazyLoadKey, StateIndex},
        RoomError, RoomStream,
    },
    core::events::Event,
    storage::TopologicalTokenWithLazyLoading,
};

#[async_trait::async_trait]
pub trait RoomSyncStateProvider: Send + Sync {
    async fn room_sync_state<'a>(
        &self,
        context: &'a RoomSyncContext<'a>,
        room_id: &RoomId,
        timeline: &Timeline,
    ) -> Result<State, RoomError>;
}

#[derive(Inject)]
pub(crate) struct ContextAwareRoomSyncStateProvider {
    forward_extremities: RoomStateAtForwardExtremities,
    begin_of_timeline: RoomStateAtTimelineStart,
    incremental: RoomStateIncremental,
}

#[async_trait::async_trait]
impl RoomSyncStateProvider for ContextAwareRoomSyncStateProvider {
    async fn room_sync_state<'a>(
        &self,
        context: &'a RoomSyncContext<'a>,
        room_id: &RoomId,
        timeline: &Timeline,
    ) -> Result<State, RoomError> {
        match context.context.sync_mode() {
            SyncMode::FullState => {
                if timeline.events.is_empty() {
                    self.forward_extremities
                        .room_sync_state(context, room_id, timeline)
                        .await
                } else {
                    self.begin_of_timeline
                        .room_sync_state(context, room_id, timeline)
                        .await
                }
            }
            SyncMode::InitialSync => {
                if timeline.events.is_empty() {
                    self.forward_extremities
                        .room_sync_state(context, room_id, timeline)
                        .await
                } else {
                    self.begin_of_timeline
                        .room_sync_state(context, room_id, timeline)
                        .await
                }
            }
            SyncMode::Incremental => {
                if context.context.user_join_since_last_sync(room_id) {
                    if timeline.events.is_empty() {
                        self.forward_extremities
                            .room_sync_state(context, room_id, timeline)
                            .await
                    } else {
                        self.begin_of_timeline
                            .room_sync_state(context, room_id, timeline)
                            .await
                    }
                } else {
                    self.incremental
                        .room_sync_state(context, room_id, timeline)
                        .await
                }
            }
        }
    }
}

#[derive(Inject, Clone)]
pub(crate) struct RoomStateAtForwardExtremities {
    room_loader: RoomLoader,
}

#[async_trait::async_trait]
impl RoomSyncStateProvider for RoomStateAtForwardExtremities {
    async fn room_sync_state<'a>(
        &self,
        context: &'a RoomSyncContext<'a>,
        room_id: &RoomId,
        _: &Timeline,
    ) -> Result<State, RoomError> {
        let room = match self.room_loader.get(room_id).await? {
            Some(room) => room,
            None => return Ok(State::default()),
        };
        let state_at_leaves = room.state_at_leaves().await?;
        let excluded_types = if context.filter().has_lazy_load() {
            vec![StateEventType::RoomMember]
        } else {
            vec![]
        };
        let events = state_at_leaves.state_events(Some(excluded_types)).await?;

        Ok(State {
            events: events
                .into_iter()
                .filter(|event| context.filter().is_state_event_allowed(event))
                .sorted_by_key(|e| e.depth)
                .map(|e| e.try_into())
                .collect::<Result<Vec<Raw<safareig_core::ruma::events::AnySyncStateEvent>>, _>>()?,
        })
    }
}

#[derive(Clone, Inject)]
pub(crate) struct RoomStateIncremental {
    room_stream: RoomStream,
    lazy_adder: LazyLoadMemberAdder,
}

#[async_trait::async_trait]
impl RoomSyncStateProvider for RoomStateIncremental {
    async fn room_sync_state<'a>(
        &self,
        context: &'a RoomSyncContext<'a>,
        room_id: &RoomId,
        timeline: &Timeline,
    ) -> Result<State, RoomError> {
        let mut state = State::default();
        let upper_bound = timeline
            .begins_at
            .map(Boundary::Exclusive)
            .unwrap_or_else(|| Boundary::Inclusive(context.context.to_event_token()));

        // Find all state events from `from` to `token`.
        let state_range = (
            pagination::Boundary::Exclusive(context.context.from_token().event()),
            upper_bound,
        );

        let state_pagination = self
            .room_stream
            .stream_room_state_events(state_range.into(), room_id)
            .await?;

        let records = state_pagination
            .records()
            .into_iter()
            .map(|tuple| tuple.1)
            .filter(|event| context.filter().is_state_event_allowed(event))
            .map(|e| e.try_into())
            .collect::<std::result::Result<Vec<_>, _>>()?;

        state.events = records;

        if context.filter().has_lazy_load() {
            self.lazy_adder
                .add_lazy_load_members(
                    &mut state.events,
                    room_id,
                    timeline.members(),
                    context.lazy_token(),
                    context.filter().lazy_load_options(),
                )
                .await?;
        }

        Ok(state)
    }
}

#[derive(Clone, Inject)]
pub(crate) struct RoomStateAtTimelineStart {
    room_loader: RoomLoader,
    lazy_adder: LazyLoadMemberAdder,
}

impl RoomStateAtTimelineStart {
    fn first_event_in_timeline(timeline: &[Event]) -> Option<&Event> {
        timeline.first()
    }
}

#[async_trait::async_trait]
impl RoomSyncStateProvider for RoomStateAtTimelineStart {
    async fn room_sync_state<'a>(
        &self,
        context: &'a RoomSyncContext<'a>,
        room_id: &RoomId,
        timeline: &Timeline,
    ) -> Result<State, RoomError> {
        let first_event = Self::first_event_in_timeline(&timeline.events);
        let has_lazy_load = context.filter().has_lazy_load();

        match first_event {
            None => Ok(State::default()),
            Some(event) => {
                let room = match self.room_loader.get(room_id).await? {
                    Some(room) => room,
                    None => return Ok(State::default()),
                };
                let state_at_event = room.state_on_event(event).await?;
                let excluded_types = if has_lazy_load {
                    vec![StateEventType::RoomMember]
                } else {
                    vec![]
                };

                let events = state_at_event.state_events(Some(excluded_types)).await?;
                let mut state_events = events
                    .into_iter()
                    .filter(|event| context.filter().is_state_event_allowed(event))
                    .sorted_by_key(|e| e.depth)
                    .map(|e| e.try_into())
                    .collect::<Result<Vec<Raw<safareig_core::ruma::events::AnySyncStateEvent>>, _>>(
                    )?;

                if has_lazy_load {
                    self.lazy_adder
                        .add_lazy_load_members(
                            &mut state_events,
                            room_id,
                            timeline.members(),
                            context.lazy_token(),
                            context.filter().lazy_load_options(),
                        )
                        .await?;
                }

                Ok(State {
                    events: state_events,
                })
            }
        }
    }
}

#[derive(Default, Debug)]
pub struct Timeline {
    pub events: Vec<Event>,
    pub limited: bool,
    pub begins_at: Option<StreamToken>,
    pub prev_batch: Option<TopologicalTokenWithLazyLoading>,
}

impl Timeline {
    pub fn members(&self) -> HashSet<&UserId> {
        self.events.iter().map(|e| e.sender.as_ref()).collect()
    }

    #[allow(clippy::wrong_self_convention)]
    pub fn to_ruma_timeline(self, id: &Identity) -> Result<RumaTimeline, RoomError> {
        let timeline_events: Vec<Raw<AnySyncTimelineEvent>> = self
            .events
            .into_iter()
            .map(|e| e.to_sync_timeline_event(id))
            .collect::<std::result::Result<Vec<_>, _>>()?;

        Ok(RumaTimeline {
            limited: self.limited,
            prev_batch: self.prev_batch.map(|token| token.to_string()),
            events: timeline_events,
        })
    }
}

#[derive(Clone, Inject)]
pub struct LazyLoadMemberAdder {
    cache: Arc<dyn LazyLoadCache>,
    room_loader: RoomLoader,
}

impl LazyLoadMemberAdder {
    pub async fn add_lazy_load_members<T, U: Borrow<UserId>>(
        &self,
        events: &mut Vec<T>,
        room_id: &RoomId,
        members: HashSet<U>,
        token: &LazyLoadToken,
        lazy_options: &LazyLoadOptions,
    ) -> Result<(), RoomError>
    where
        T: TryFrom<Event, Error = EventError>,
    {
        let include_redundants = match lazy_options {
            LazyLoadOptions::Disabled => true,
            LazyLoadOptions::Enabled {
                include_redundant_members,
            } => *include_redundant_members,
        };

        let target_members: HashSet<LazyLoadKey> = members
            .iter()
            .map(|member| LazyLoadKey {
                token: token.ulid(),
                user_id: member.borrow(),
                room_id,
            })
            .collect();

        let missing = if include_redundants {
            target_members
        } else {
            self.cache.find_missing(target_members)
        };

        if missing.is_empty() {
            return Ok(());
        }

        let room = self
            .room_loader
            .get(room_id)
            .await?
            .ok_or_else(|| RoomError::RoomNotFound(room_id.to_string()))?;
        let room_state = room.state_at_leaves().await?;

        for member in &missing {
            let state_index = StateIndex::memberhip(member.user_id.to_string());
            let event_id = room_state.event_id_for_state_key(&state_index).await?;

            if let Some(event_id) = event_id {
                let event = room.event(&event_id).await?;

                if let Some(event) = event {
                    if let Ok(sync_event) = event.try_into() {
                        events.push(sync_event);
                    }
                }
            }
        }
        self.cache.insert_keys(missing);

        Ok(())
    }
}
