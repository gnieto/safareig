use std::{
    collections::{BTreeMap, HashMap},
    time::SystemTime,
};

use safareig_core::{
    ruma::{
        api::federation::transactions::edu::{Edu, ReceiptContent, ReceiptData, ReceiptMap},
        events::{
            receipt::{Receipt, ReceiptEventContent, ReceiptThread, ReceiptType, Receipts},
            typing::TypingEventContent,
            AnySyncEphemeralRoomEvent, SyncEphemeralRoomEvent,
        },
        serde::Raw,
        EventId, OwnedEventId, OwnedRoomId, OwnedUserId, RoomId, UserId,
    },
    time::system_time_to_millis,
};
use safareig_typing::TypingTracker;

use crate::core::events::convert_to_raw;

// TODO: Move to stream/ephemeral
pub(crate) struct EphemeralRoomAggregator<'a> {
    receipt: ReceiptAggregator,
    typing: BTreeMap<&'a RoomId, Vec<OwnedUserId>>,
}

impl<'a> EphemeralRoomAggregator<'a> {
    pub fn new() -> Self {
        EphemeralRoomAggregator {
            receipt: Default::default(),
            typing: Default::default(),
        }
    }

    pub fn add_receipt(
        &mut self,
        room_id: &RoomId,
        user_id: &UserId,
        event_id: &EventId,
        time: &SystemTime,
    ) {
        self.receipt.add_receipt(room_id, user_id, event_id, time)
    }

    pub fn add_typing(&mut self, tracker: &TypingTracker, room_id: &'a RoomId) {
        if !self.typing.contains_key(room_id) {
            let typing_users = tracker.typing_users(room_id);
            self.typing.insert(room_id, typing_users);
        }
    }

    pub fn ephemeral_events(self) -> HashMap<OwnedRoomId, Vec<Raw<AnySyncEphemeralRoomEvent>>> {
        let mut ephemeral_events = HashMap::new();

        for (room, receipts) in self.receipt.room_receipts {
            let any_sync = SyncEphemeralRoomEvent {
                content: ReceiptEventContent(receipts),
            };

            let entry: &mut Vec<Raw<AnySyncEphemeralRoomEvent>> = ephemeral_events
                .entry(room)
                .or_insert_with(Default::default);
            if let Some(raw_ephemeral) = convert_to_raw::<_, AnySyncEphemeralRoomEvent>(&any_sync) {
                entry.push(raw_ephemeral);
            }
        }

        for (room, typing) in self.typing {
            let room = room.to_owned();

            let any_sync = SyncEphemeralRoomEvent {
                content: TypingEventContent { user_ids: typing },
            };

            let entry: &mut Vec<Raw<AnySyncEphemeralRoomEvent>> = ephemeral_events
                .entry(room)
                .or_insert_with(Default::default);
            if let Some(raw_ephemeral) = convert_to_raw::<_, AnySyncEphemeralRoomEvent>(&any_sync) {
                entry.push(raw_ephemeral);
            }
        }

        ephemeral_events
    }
}

#[derive(Default)]
pub(crate) struct ReceiptAggregator {
    room_receipts: BTreeMap<OwnedRoomId, BTreeMap<OwnedEventId, Receipts>>,
}

impl From<ReceiptAggregator> for Edu {
    fn from(aggregator: ReceiptAggregator) -> Self {
        let receipts = aggregator
            .room_receipts
            .into_iter()
            .map(|(room, receipts_by_event)| {
                let mut receipts_by_user = BTreeMap::new();

                for (eid, mut receipts) in receipts_by_event {
                    if let Some(user_receipt) = receipts.remove(&ReceiptType::Read) {
                        for (user, receipt) in user_receipt {
                            let current_data = receipts_by_user
                                .entry(user)
                                .or_insert_with(|| ReceiptData::new(receipt, Vec::new()));

                            current_data.event_ids.push(eid.clone());
                        }
                    }
                }

                (room, ReceiptMap::new(receipts_by_user))
            })
            .collect();

        let receipt_content = ReceiptContent::new(receipts);
        Edu::Receipt(receipt_content)
    }
}

impl ReceiptAggregator {
    pub fn add_receipt(
        &mut self,
        room_id: &RoomId,
        user_id: &UserId,
        event_id: &EventId,
        time: &SystemTime,
    ) {
        let room_id = room_id.to_owned();
        let room_entry = self.room_receipts.entry(room_id).or_default();
        let event_entry = room_entry.entry(event_id.to_owned()).or_default();
        let user_read_receipts = event_entry.entry(ReceiptType::Read).or_default();

        user_read_receipts
            .entry(user_id.to_owned())
            .or_insert_with(|| Receipt {
                ts: Some(system_time_to_millis(*time)),
                // Todo: Add support for receipts in threads
                thread: ReceiptThread::Unthreaded,
            });
    }

    pub fn has_receipts(&self) -> bool {
        !self.room_receipts.is_empty()
    }
}
