use std::{collections::HashSet, str::FromStr, sync::Arc};

use safareig_core::{
    pagination::{self, Pagination},
    ruma::api::client::sync::sync_events::{self, v3::Ephemeral},
    Inject,
};
use safareig_ephemeral::services::{EphemeralEvent, EphemeralStream, EphemeralToken};
use safareig_ignored_users::IgnoreUsersRepository;
use safareig_sync_api::{
    context::Context, error::SyncError, SyncExtension, SyncToken, SyncTokenPart,
};
use safareig_typing::TypingTracker;

use crate::client::sync::ephemeral::EphemeralRoomAggregator;

#[derive(Inject)]
pub struct EphemeralSync {
    ephemeral: EphemeralStream,
    typing: TypingTracker,
    ignored: Option<Arc<dyn IgnoreUsersRepository>>,
}

#[async_trait::async_trait]
impl SyncExtension for EphemeralSync {
    #[tracing::instrument(name = "sync-ephemeral", skip_all)]
    async fn fill_sync<'a>(
        &self,
        ctx: &'a Context<'a>,
        response: &mut sync_events::v3::Response,
        token: &mut SyncToken,
    ) -> Result<(), SyncError> {
        let ephemeral_token = ctx
            .from_token()
            .extension_token::<EphemeralSyncToken>()
            .map_err(|_| SyncError::InvalidToken)?
            .unwrap_or_default();

        let range = (
            pagination::Boundary::Exclusive(ephemeral_token.0),
            pagination::Boundary::Unlimited,
        );
        let query = Pagination::new(range.into(), ());

        let pagination_response = self.ephemeral.events(query);
        let next = pagination_response.next().cloned();
        let ephemeral = pagination_response.records();

        // Update next token up to the current ephemeal token
        if let Some(next) = next {
            token.update(EphemeralSyncToken(next));
        }

        let ignored_users = match &self.ignored {
            Some(ignored) => ignored.ignored_users(ctx.user()).await?,
            None => HashSet::new(),
        };
        let mut ephemeral_aggregator = EphemeralRoomAggregator::new();

        ephemeral.iter().for_each(|e| match e {
            EphemeralEvent::Typing(typing_user, room, _) => {
                if !ignored_users.contains(typing_user) {
                    ephemeral_aggregator.add_typing(&self.typing, room);
                }
            }
            EphemeralEvent::Presence(_) => {
                // Presence event are handled in it's own sync-extension
            }
            EphemeralEvent::Marker(room, user, event, time) => {
                if !ignored_users.contains(user) {
                    ephemeral_aggregator.add_receipt(room, user, event, time);
                }
            }
            EphemeralEvent::DeviceUpdate(..) => {
                tracing::warn!("TODO: Ignoring device update events");
            }
            EphemeralEvent::ToDevice(_, _) => {
                // Ignore ToDevice events
            }
            EphemeralEvent::CrossSigning(_) => {
                // TODO: Ignore cross signing events by now
            }
        });

        // Fill room-ephemeral events
        let ephemeral_by_room = ephemeral_aggregator.ephemeral_events();
        let memberships = ctx.user_memberships();

        for (room, events) in ephemeral_by_room {
            if memberships.joined.contains_key(&room)
                || memberships.joined_since_last_sync.contains_key(&room)
            {
                let joined_room = response.rooms.join.entry(room).or_default();
                joined_room.ephemeral = Ephemeral { events };
            }
        }

        Ok(())
    }
}

#[derive(Default)]
pub struct EphemeralSyncToken(EphemeralToken);

impl SyncTokenPart for EphemeralSyncToken {
    fn id() -> &'static str {
        "e"
    }
}

impl ToString for EphemeralSyncToken {
    fn to_string(&self) -> String {
        self.0.to_string()
    }
}

impl FromStr for EphemeralSyncToken {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(EphemeralSyncToken(EphemeralToken::try_from(s)?))
    }
}
