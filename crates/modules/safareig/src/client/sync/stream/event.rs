use safareig_core::ruma::api::client::sync::sync_events;
use safareig_sync_api::{context::Context, error::SyncError, SyncExtension, SyncToken};

use super::{EventFullSync, EventStreamSync};

pub struct EventSync {
    event_full: EventFullSync,
    event_stream: EventStreamSync,
}

impl EventSync {
    pub fn new(event_full: EventFullSync, event_stream: EventStreamSync) -> Self {
        Self {
            event_full,
            event_stream,
        }
    }
}

#[async_trait::async_trait]
impl SyncExtension for EventSync {
    #[tracing::instrument(name = "sync-stream", skip_all)]
    async fn fill_sync<'a>(
        &self,
        ctx: &'a Context<'a>,
        response: &mut sync_events::v3::Response,
        token: &mut SyncToken,
    ) -> Result<(), SyncError> {
        if ctx.is_large_gap() {
            self.event_full.fill_sync(ctx, response, token).await
        } else {
            self.event_stream.fill_sync(ctx, response, token).await
        }
    }

    fn priority(&self) -> u8 {
        64
    }
}
