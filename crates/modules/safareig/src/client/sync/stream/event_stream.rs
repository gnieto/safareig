use std::{collections::HashMap, sync::Arc};

use safareig_core::{
    events::EventError,
    pagination::{self},
    ruma::{
        api::client::sync::sync_events::{
            self,
            v3::{RoomSummary, State},
            UnreadNotificationsCount,
        },
        events::AnySyncStateEvent,
        serde::Raw,
        OwnedRoomId,
    },
    Inject, StreamToken,
};
use safareig_sync_api::{
    context::{Context, SyncMode},
    error::SyncError,
    SyncExtension, SyncToken,
};

use super::{LazyLoadToken, RoomSyncContext};
use crate::{
    client::{
        room::{events::EventDecorator, loader::RoomLoader, RoomError, RoomStream},
        sync::state::{LazyLoadMemberAdder, RoomSyncStateProvider, Timeline},
    },
    core::{events::Event, filter::Filter},
};

#[derive(Clone, Inject)]
pub struct EventStreamSync {
    room_stream: RoomStream,
    room_loader: RoomLoader,
    event_decorator: Arc<EventDecorator>,
    state_provider: Arc<dyn RoomSyncStateProvider>,
    lazy_adder: LazyLoadMemberAdder,
}

#[async_trait::async_trait]
impl SyncExtension for EventStreamSync {
    #[tracing::instrument(name = "sync-event-stream", skip_all)]
    async fn fill_sync<'a>(
        &self,
        ctx: &'a Context<'a>,
        response: &mut sync_events::v3::Response,
        token: &mut SyncToken,
    ) -> Result<(), SyncError> {
        // Get (or obtain the default token) and assign it back. This should be done only once, but
        // right now there's no easy way of initializing this token.
        let lazy_token = token
            .extension_token::<LazyLoadToken>()?
            .unwrap_or_default();
        token.update(lazy_token.clone());
        let room_context = RoomSyncContext::new(ctx, lazy_token.clone());

        let filter = room_context.filter();
        let full_state = ctx.needs_full_state();
        let range = (
            pagination::Boundary::Exclusive(ctx.from_token().event()),
            pagination::Boundary::Inclusive(ctx.to_event_token()),
        );

        let (tokens, mut events): (Vec<_>, Vec<_>) = self
            .room_stream
            .stream_events(range.into(), Some(filter))
            .await?
            .records()
            .into_iter()
            .unzip();

        self.event_decorator
            .decorate_events(events.as_mut_slice())
            .await;

        token.update_event(ctx.to_event_token());

        let mut events_by_room: HashMap<OwnedRoomId, RoomEventSlice> = HashMap::new();
        for (i, event) in events.iter().enumerate() {
            let stream_token = tokens[i];
            let event_slice = events_by_room.entry(event.room_id.to_owned()).or_default();

            if filter.is_timeline_event_allowed(event) {
                event_slice.event.push((stream_token, event));
            }

            if event.state_key.is_some() {
                event_slice.state_events.push((stream_token, event));
            }
        }

        for (room_id, _) in ctx.user_memberships().joined.iter() {
            let summary = RoomSummary::new();
            let mut state = State::new();

            let current_room_has_content =
                events_by_room.contains_key(room_id) || !state.events.is_empty();

            if current_room_has_content {
                let timeline = match events_by_room.remove(room_id) {
                    Some(event_slice) => {
                        let limit = filter.timeline_limit();
                        let (token_cut, timeline) = event_slice
                            .build_timeline(limit, &lazy_token)
                            .map_err(|e| SyncError::Custom(e.to_string()))?;

                        if !full_state && timeline.limited {
                            state = event_slice
                                .state_events_outside_timeline(token_cut, filter)
                                .map_err(|e| SyncError::Custom(e.to_string()))?;
                        }

                        timeline
                    }
                    None => Timeline::default(),
                };

                if let SyncMode::FullState = ctx.sync_mode() {
                    state = self
                        .state_provider
                        .room_sync_state(&room_context, room_id, &timeline)
                        .await
                        .map_err(|e| SyncError::Custom(e.to_string()))?;
                } else if room_context.filter().has_lazy_load() {
                    self.lazy_adder
                        .add_lazy_load_members(
                            &mut state.events,
                            room_id,
                            timeline.members(),
                            &lazy_token,
                            room_context.filter().lazy_load_options(),
                        )
                        .await
                        .map_err(|e| SyncError::Custom(e.to_string()))?;
                }

                if !timeline.events.is_empty() || !state.events.is_empty() {
                    let join = response.rooms.join.entry(room_id.to_owned()).or_default();

                    join.summary = summary;
                    join.unread_notifications = UnreadNotificationsCount::default();
                    join.timeline = timeline
                        .to_ruma_timeline(ctx.id())
                        .map_err(|e: RoomError| SyncError::Custom(e.to_string()))?;
                    join.state = state;
                }
            }
        }

        for (room_id, _membership) in ctx.user_memberships().joined_since_last_sync.iter() {
            let summary = RoomSummary::new();

            let mut timeline = match events_by_room.remove(room_id) {
                Some(event_slice) => {
                    let limit = filter.timeline_limit();
                    let (_, timeline) = event_slice
                        .build_timeline(limit, &lazy_token)
                        .map_err(|e| SyncError::Custom(e.to_string()))?;

                    timeline
                }
                None => Timeline::default(),
            };

            if !timeline.limited {
                let _room = self
                    .room_loader
                    .get(room_id)
                    .await
                    .map_err(|e| SyncError::Custom(e.to_string()))?
                    .ok_or_else(|| SyncError::Custom("could not load target room".to_string()))?;
                timeline.limited = true;
            }

            // Rooms that are joined since the given token, needs to load all the state and they
            // will be limited, to signal the client that it can load more messages via the
            // `/messages` endpoint.
            let state = self
                .state_provider
                .room_sync_state(&room_context, room_id, &timeline)
                .await
                .map_err(|e| SyncError::Custom(e.to_string()))?;
            if !timeline.events.is_empty() || !state.events.is_empty() {
                let join = response.rooms.join.entry(room_id.to_owned()).or_default();

                join.summary = summary;
                join.unread_notifications = UnreadNotificationsCount::default();
                join.timeline = timeline
                    .to_ruma_timeline(ctx.id())
                    .map_err(|e: RoomError| SyncError::Custom(e.to_string()))?;
                join.state = state;
            }
        }

        for (room_id, membership) in ctx.user_memberships().leave.iter() {
            let mut state = State::new();
            let timeline = match events_by_room.remove(room_id) {
                Some(event_slice) => {
                    let limit = filter.timeline_limit();
                    let (token_cut, timeline) = event_slice
                        .build_left_timeline(&membership.stream, limit, &lazy_token)
                        .map_err(|e| SyncError::Custom(e.to_string()))?;

                    if !full_state && timeline.limited {
                        state = event_slice
                            .state_events_outside_timeline(token_cut, filter)
                            .map_err(|e| SyncError::Custom(e.to_string()))?;
                    }

                    timeline
                }
                None => Timeline::default(),
            };

            if ctx.needs_full_state() || ctx.user_join_since_last_sync(room_id) {
                state = self
                    .state_provider
                    .room_sync_state(&room_context, room_id, &timeline)
                    .await
                    .map_err(|e| SyncError::Custom(e.to_string()))?;
            }

            let left = response.rooms.leave.entry(room_id.to_owned()).or_default();

            left.timeline = timeline
                .to_ruma_timeline(ctx.id())
                .map_err(|e: RoomError| SyncError::Custom(e.to_string()))?;
            left.state = state;
        }

        Ok(())
    }
}

#[derive(Default)]
struct RoomEventSlice<'a> {
    event: Vec<(StreamToken, &'a Event)>,
    state_events: Vec<(StreamToken, &'a Event)>,
}

impl<'a> RoomEventSlice<'a> {
    fn build_timeline(
        &self,
        limit: usize,
        lazy_token: &LazyLoadToken,
    ) -> Result<(Option<StreamToken>, Timeline), RoomError> {
        let events_to_return = self.limited_events(limit);
        let timeline_events: Vec<Event> = events_to_return
            .iter()
            .map(|event| event.1.clone())
            .collect();

        let is_limited = self.is_limited(limit);
        let stream_token_limited = events_to_return.first().map(|tuple| tuple.0);

        let (prev_batch, begins_at) = if !events_to_return.is_empty() {
            (
                Some(events_to_return[0].1.topological_token().prev()),
                Some(events_to_return[0].0),
            )
        } else {
            (None, None)
        };

        let timeline = Timeline {
            limited: is_limited,
            prev_batch: prev_batch.map(|t| t.with_lazy_load_token(lazy_token.clone())),
            events: timeline_events,
            begins_at,
        };

        Ok((stream_token_limited, timeline))
    }

    fn build_left_timeline(
        &self,
        left_at: &StreamToken,
        limit: usize,
        lazy_token: &LazyLoadToken,
    ) -> Result<(Option<StreamToken>, Timeline), RoomError> {
        let events_to_return = self.limited_events(limit);
        let timeline_events: Vec<Event> = events_to_return
            .iter()
            .filter(|token_and_event| token_and_event.0 <= *left_at)
            .map(|event| event.1.clone())
            .collect();

        let is_limited = self.is_limited(limit);
        let stream_token_limited = events_to_return.first().map(|tuple| tuple.0);

        let (prev_batch, begins_at) = if is_limited && !events_to_return.is_empty() {
            (
                Some(events_to_return[0].1.topological_token()),
                Some(events_to_return[0].0),
            )
        } else {
            (None, None)
        };

        let timeline = Timeline {
            limited: is_limited,
            prev_batch: prev_batch.map(|t| t.with_lazy_load_token(lazy_token.clone())),
            events: timeline_events,
            begins_at,
        };

        Ok((stream_token_limited, timeline))
    }

    fn state_events_outside_timeline(
        &self,
        oldest: Option<StreamToken>,
        filter: &Filter,
    ) -> Result<State, RoomError> {
        let state_events = match oldest {
            Some(token) => self
                .state_events
                .iter()
                .take_while(|(current, _)| *current < token)
                .filter(|(_, event)| filter.is_state_event_allowed(event))
                .map(|(_, event)| (*event).clone().try_into())
                .collect::<Result<Vec<Raw<AnySyncStateEvent>>, EventError>>()?,
            None => {
                vec![]
            }
        };

        Ok(State {
            events: state_events,
        })
    }

    fn is_limited(&self, limit: usize) -> bool {
        self.event.len() > limit
    }

    fn limited_events(&self, limit: usize) -> &[(StreamToken, &Event)] {
        let index = std::cmp::max(0, self.event.len().saturating_sub(limit));
        self.event.split_at(index).1
    }
}
