use std::sync::Arc;

use safareig_core::{
    pagination::{self, Limit, Pagination},
    ruma::{
        api::client::sync::sync_events::{
            self,
            v3::{RoomSummary, State},
        },
        RoomId,
    },
    Inject, StreamToken,
};
use safareig_sync_api::{context::Context, error::SyncError, SyncExtension, SyncToken};

use super::{LazyLoadToken, RoomSyncContext};
use crate::{
    client::{
        room::{events::EventDecorator, RoomError, RoomStream},
        sync::state::{RoomSyncStateProvider, Timeline},
    },
    core::events::Event,
    storage::{StreamFilter, TopologicalToken},
};

#[derive(Clone, Inject)]
pub struct EventFullSync {
    room_stream: RoomStream,
    event_decorator: Arc<EventDecorator>,
    state_provider: Arc<dyn RoomSyncStateProvider>,
}

#[async_trait::async_trait]
impl SyncExtension for EventFullSync {
    #[tracing::instrument(name = "sync-full", skip_all)]
    async fn fill_sync<'a>(
        &self,
        ctx: &'a Context<'a>,
        response: &mut sync_events::v3::Response,
        token: &mut SyncToken,
    ) -> Result<(), SyncError> {
        // Get (or obtain the default token) and assign it back. This should be done only once, but
        // right now there's no easy way of initializing this token.
        let lazy_token = token
            .extension_token::<LazyLoadToken>()?
            .unwrap_or_default();
        token.update(lazy_token.clone());

        let room_context = RoomSyncContext::new(ctx, lazy_token);
        token.update_event(ctx.to_event_token());

        let joined = ctx
            .user_memberships()
            .joined
            .keys()
            .chain(ctx.user_memberships().joined_since_last_sync.keys());

        for room_id in joined {
            let summary = RoomSummary::new();
            let range = (
                pagination::Boundary::Exclusive(ctx.from_token().event()),
                pagination::Boundary::Inclusive(ctx.to_event_token()),
            );

            let (timeline, state) = self
                .timeline_and_state_for_room(&room_context, room_id, range.into())
                .await
                .map_err(|e| SyncError::Custom(e.to_string()))?;
            let current_room_has_content = !timeline.events.is_empty() || !state.events.is_empty();

            if current_room_has_content {
                let join = response.rooms.join.entry(room_id.to_owned()).or_default();

                join.summary = summary;
                join.timeline = timeline
                    .to_ruma_timeline(ctx.id())
                    .map_err(|e| SyncError::Custom(e.to_string()))?;
                join.state = state;
            }
        }

        for (room_id, membership) in ctx.user_memberships().leave.iter() {
            let range = (
                pagination::Boundary::Exclusive(ctx.from_token().event()),
                pagination::Boundary::Inclusive(membership.stream),
            );

            let (timeline, state) = self
                .timeline_and_state_for_room(&room_context, room_id, range.into())
                .await
                .map_err(|e| SyncError::Custom(e.to_string()))?;
            let current_room_has_content = !timeline.events.is_empty() || !state.events.is_empty();

            if current_room_has_content {
                let left = response.rooms.leave.entry(room_id.to_owned()).or_default();

                left.timeline = timeline
                    .to_ruma_timeline(ctx.id())
                    .map_err(|e: RoomError| SyncError::Custom(e.to_string()))?;
                left.state = state;
            }
        }

        Ok(())
    }
}

impl EventFullSync {
    async fn timeline_and_state_for_room<'a>(
        &self,
        room_ctx: &'a RoomSyncContext<'a>,
        room_id: &RoomId,
        range: pagination::Range<StreamToken>,
    ) -> Result<(Timeline, State), RoomError> {
        let real_limit = room_ctx.filter().timeline_limit();
        let limit = real_limit.saturating_add(1);
        let stream_filter = StreamFilter {
            filter: Some(room_ctx.filter()),
            room_id: Some(room_id),
            source: crate::storage::StreamSource::Timeline,
        };

        let query = Pagination::new(range, Some(stream_filter))
            .with_limit(Limit::from(limit as u32))
            .with_direction(pagination::Direction::Backward);

        let pagination = self.room_stream.stream_room_events(query).await?;
        let mut timeline = Timeline::default();
        let records = pagination.records();
        let limited = records.len() > real_limit;

        let mut records: Vec<_> = records.into_iter().take(real_limit).collect();
        records.reverse();

        let lazy_token = room_ctx.lazy_token();
        if limited {
            timeline.limited = true;
        }

        let latest_token = records.first().map(|tuple| {
            // TODO: Should we encode "exclusive" in the topolgical token?
            tuple
                .1
                .topological_token()
                .prev()
                .with_lazy_load_token(lazy_token.clone())
        });
        if let Some(token) = latest_token {
            if token.0 != TopologicalToken::root() {
                timeline.prev_batch = Some(token);
            }
        }

        let mut events: Vec<Event> = records.into_iter().map(|t| t.1).collect();
        self.event_decorator.decorate_events(&mut events).await;
        timeline.events = events;
        let state = self
            .state_provider
            .room_sync_state(room_ctx, room_id, &timeline)
            .await?;

        Ok((timeline, state))
    }
}
