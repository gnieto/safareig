use std::{str::FromStr, sync::Arc};

use rusty_ulid::{DecodingError, Ulid};
use safareig_core::{
    pagination,
    ruma::{
        api::client::sync::sync_events::{self, v3::InviteState},
        EventId, RoomId,
    },
    Inject,
};
use safareig_sync_api::{
    context::Context, error::SyncError, SyncExtension, SyncToken, SyncTokenPart,
};

use crate::{
    client::room::{loader::RoomLoader, RoomError},
    storage::{Invite, InviteStorage},
};

#[derive(Inject)]
pub struct InviteSync {
    invite: Arc<dyn InviteStorage>,
    room_loader: RoomLoader,
}

impl InviteSync {
    async fn invite_state<'a>(
        &self,
        ctx: &'a Context<'a>,
        room_id: &RoomId,
        event_id: &EventId,
    ) -> Result<Option<InviteState>, RoomError> {
        let room = self
            .room_loader
            .get(room_id)
            .await?
            .ok_or_else(|| RoomError::RoomNotFound(room_id.to_string()))?;

        let mut events = room
            .state_at(event_id)
            .await?
            .ok_or_else(|| RoomError::Custom("state not found".into()))?
            .state_events(None)
            .await?;

        // State at event returns the state before the event, which means that the event of the
        // invited user is not included.
        if let Some(invite_event) = room.event(event_id).await? {
            if ctx.ignored.contains(&invite_event.sender) {
                return Ok(None);
            }

            events.push(invite_event);
        }

        let serialized_events = events
            .into_iter()
            .map(TryInto::try_into)
            .collect::<Result<_, _>>()?;

        let invited = InviteState {
            events: serialized_events,
        };

        Ok(Some(invited))
    }
}

#[async_trait::async_trait]
impl SyncExtension for InviteSync {
    #[tracing::instrument(name = "sync-invites", skip_all)]
    async fn fill_sync<'a>(
        &self,
        ctx: &'a Context<'a>,
        response: &mut sync_events::v3::Response,
        token: &mut SyncToken,
    ) -> Result<(), SyncError> {
        let invite_token = token
            .extension_token::<InviteToken>()
            .map_err(|_| SyncError::InvalidToken)?
            .unwrap_or_default();
        let range = (
            pagination::Boundary::Exclusive(invite_token.0),
            pagination::Boundary::Unlimited,
        );
        let stream_response = self.invite.query_invites(ctx.user(), range.into()).await?;
        // Update sync token
        if let Some(invite_token) = stream_response.next() {
            token.update(InviteToken(*invite_token));
        }
        let invites = stream_response.records();

        // Fill response with invite events
        for invite in invites {
            let room_id = invite.room_id().to_owned();

            let maybe_state = match invite {
                Invite::Local(local) => self
                    .invite_state(ctx, &local.room, &local.event_id)
                    .await
                    .map_err(|e| SyncError::Custom(e.to_string()))?,
                Invite::Remote(remote) => {
                    let inviter = remote.get_inviter(ctx.user()).ok();

                    inviter.and_then(|user| {
                        if ctx.ignored.contains(&user) {
                            return None;
                        }

                        Some(remote.state)
                    })
                }
            };

            if let Some(state) = maybe_state {
                let invited_room = response.rooms.invite.entry(room_id).or_default();

                invited_room.invite_state = state;
            }
        }

        Ok(())
    }
}

pub struct InviteToken(Ulid);

impl Default for InviteToken {
    fn default() -> Self {
        Self(Ulid::from_str("00000000000000000000000000").unwrap())
    }
}

impl SyncTokenPart for InviteToken {
    fn id() -> &'static str {
        "i"
    }
}

impl ToString for InviteToken {
    fn to_string(&self) -> String {
        self.0.to_string()
    }
}

impl FromStr for InviteToken {
    type Err = DecodingError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(InviteToken(Ulid::from_str(s)?))
    }
}
