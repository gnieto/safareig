mod ephemeral;
mod event;
mod event_full;
mod event_stream;
mod invite;

use std::str::FromStr;

pub use ephemeral::EphemeralSync;
pub use event::EventSync;
pub use event_full::EventFullSync;
pub use event_stream::EventStreamSync;
pub use invite::InviteSync;
use rusty_ulid::Ulid;
use safareig_sync_api::{context::Context, SyncTokenPart};

use crate::core::filter::Filter;

pub struct RoomSyncContext<'a> {
    pub context: &'a Context<'a>,
    filter: Filter<'a>,
    token: LazyLoadToken,
}

impl<'a> RoomSyncContext<'a> {
    pub fn new(context: &'a Context<'a>, lazy_token: LazyLoadToken) -> Self {
        let filter = Filter::new(
            &context.filter,
            context.user_memberships().rooms(),
            &context.ignored,
        );

        Self {
            context,
            filter,
            token: lazy_token,
        }
    }

    pub fn filter(&self) -> &Filter {
        &self.filter
    }

    pub fn lazy_token(&self) -> &LazyLoadToken {
        &self.token
    }
}

#[derive(Clone, Debug)]
pub struct LazyLoadToken {
    token: rusty_ulid::Ulid,
}

impl LazyLoadToken {
    pub fn ulid(&self) -> Ulid {
        self.token
    }
}

impl Default for LazyLoadToken {
    fn default() -> Self {
        Self {
            token: rusty_ulid::Ulid::generate(),
        }
    }
}

impl FromStr for LazyLoadToken {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let ulid = rusty_ulid::Ulid::from_str(s).map_err(|_| ())?;

        Ok(LazyLoadToken { token: ulid })
    }
}

impl ToString for LazyLoadToken {
    fn to_string(&self) -> String {
        self.token.to_string()
    }
}

impl SyncTokenPart for LazyLoadToken {
    fn id() -> &'static str {
        "ll"
    }
}
