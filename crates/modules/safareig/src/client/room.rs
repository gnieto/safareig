use std::{
    collections::{HashMap, HashSet},
    convert::TryFrom,
    str::FromStr,
    sync::Arc,
};

use opentelemetry::KeyValue;
use reqwest::StatusCode;
use safareig_core::{
    bus::{Bus, BusMessage},
    error::{error_chain, RumaExt},
    events::{Content, EventError},
    id::IdError,
    pagination::{self, Boundary, Limit, Pagination, PaginationResponse, Range},
    ruma::{
        api::{
            client::error::{ErrorBody, ErrorKind},
            Direction,
        },
        canonical_json::RedactionError,
        events::{
            call::{
                answer::CallAnswerEventContent, candidates::CallCandidatesEventContent,
                hangup::CallHangupEventContent, invite::CallInviteEventContent,
            },
            key::verification::{
                accept::KeyVerificationAcceptEventContent,
                cancel::KeyVerificationCancelEventContent, done::KeyVerificationDoneEventContent,
                key::KeyVerificationKeyEventContent, mac::KeyVerificationMacEventContent,
                ready::KeyVerificationReadyEventContent, start::KeyVerificationStartEventContent,
            },
            policy::rule::{
                room::PolicyRuleRoomEventContent, server::PolicyRuleServerEventContent,
                user::PolicyRuleUserEventContent,
            },
            room::{
                aliases::RoomAliasesEventContent,
                avatar::RoomAvatarEventContent,
                canonical_alias::RoomCanonicalAliasEventContent,
                create::RoomCreateEventContent,
                encrypted::RoomEncryptedEventContent,
                encryption::RoomEncryptionEventContent,
                guest_access::{GuestAccess, RoomGuestAccessEventContent},
                history_visibility::{HistoryVisibility, RoomHistoryVisibilityEventContent},
                join_rules::{JoinRule, RoomJoinRulesEventContent},
                member::{MembershipState, RoomMemberEventContent},
                message::RoomMessageEventContent,
                name::RoomNameEventContent,
                pinned_events::RoomPinnedEventsEventContent,
                power_levels::RoomPowerLevelsEventContent,
                redaction::RoomRedactionEventContent,
                server_acl::RoomServerAclEventContent,
                third_party_invite::RoomThirdPartyInviteEventContent,
                tombstone::RoomTombstoneEventContent,
                topic::RoomTopicEventContent,
            },
            space::{child::SpaceChildEventContent, parent::SpaceParentEventContent},
            sticker::StickerEventContent,
            EventContent, EventContentFromType, MessageLikeEventType, StateEventType,
            TimelineEventType,
        },
        room::RoomType,
        EventId, OwnedEventId, OwnedMxcUri, OwnedRoomAliasId, OwnedRoomId, OwnedServerName,
        OwnedUserId, RoomId, RoomVersionId, TransactionId, UserId,
    },
    storage::StorageError,
    Inject, StreamToken,
};
use safareig_federation::FederationError;
use safareig_users::profile::{member_event_with_profile, ProfileLoader};
// use safareig_to_device::ToDeviceError;
use serde::de::DeserializeOwned;
use serde_json::value::RawValue;
use thiserror::Error;
use tracing::instrument;

use self::{
    events::{RoomEventPublisher, SignError},
    hooks::EventHooks,
};
use crate::{
    client::room::{
        errors::InviteError,
        events::{CreateRoomExtra, CREATE_ROOM_EXTRA_TYPE},
        state::{
            ruma_stateres::{ResolutionError, StateResParameters},
            RoomState, RoomStateId, StateIndex, StateInfo,
        },
    },
    core::{
        events::{
            auth::{AuthChain, AuthError},
            Event, EventBuilder, EventContentExt, EventRef,
        },
        filter::Filter,
        room::{RoomDescriptor, RoomVersion},
    },
    federation::backfill::{RemoteSelectorPolicy, RemoteStateFetcher},
    metrics::EVENTS_PROCESSED,
    storage::{
        BackwardExtremity, EventOptions, EventStorage, Invite, InviteStorage, Leaf, LocalInvite,
        Membership, RoomStorage, RoomStreamStorage, RoomTopologyStorage, StateStorage,
        StreamFilter, TopologicalToken,
    },
};

pub mod command;
pub mod errors;
pub mod events;
pub(crate) mod hooks;
pub mod loader;
pub mod membership;
mod power_levels;
pub mod state;
pub(crate) mod versions;

pub(crate) use power_levels::PowerLevelChecker;

#[derive(Error, Debug)]
pub enum RoomError {
    #[error("Given alias is already in use")]
    AliasInUse,
    #[error("Alias do not exists or points to an invalid room")]
    BadAlias,
    #[error("Incompatible PDU type")]
    IncompatiblePduType,
    #[error("Could not store event: {0}")]
    StorageError(#[from] StorageError),
    #[error("Serde error")]
    Serde(#[from] serde_json::Error),
    #[error("Could not sign the event")]
    SignatureError(#[from] safareig_core::ruma::signatures::Error),
    #[error("Invalid event id")]
    InvalidEventId,
    #[error("Invalid room name")]
    InvalidRoomName,
    #[error("Invalid initial state event")]
    InvalidInitialStateEvent,
    #[error("Unsupported room vesion")]
    UnsupportedRoomVersion(RoomVersionId),
    #[error("Missing auth event: {0}")]
    MissingAuthEvent(TimelineEventType),
    #[error("Event not found: {0}")]
    EventNotFound(OwnedEventId),
    #[error("User {0} not allowed to send message to this room")]
    UserNotAllowed(OwnedUserId),
    #[error("Room {0} not found")]
    RoomNotFound(String),
    #[error("Invalid stream token")]
    InvalidStreamToken,
    #[error("{0}")]
    Custom(String),
    #[error("Missing state key: {0}")]
    MissingStateKey(StateIndex),
    #[error("Federation error: {0}")]
    Federation(#[from] FederationError),
    #[error("Invite error")]
    InviteError(#[from] InviteError),
    #[error("Unauthorized: {0}")]
    Unauthorized(#[from] AuthError),
    #[error("Error while resolving some states: {0}")]
    Resolution(#[from] ResolutionError),
    #[error("Event error: {0}")]
    EventError(#[from] EventError),
    // #[error("To deice event error: {0}")]
    // ToDeviceError(#[from] ToDeviceError),
    #[error("Signature error: {0}")]
    SignError(#[from] SignError),
    #[error("Redaction error: {0}")]
    RedactError(#[from] RedactionError),
    #[error("Server {0} not allowed")]
    ServerNotAllowed(OwnedServerName),
    #[error("Content could not be decoded with target type {0}")]
    InvalidContent(String),
    #[error("Id error")]
    IdError(#[from] IdError),
}

impl From<RoomError> for safareig_core::ruma::api::client::Error {
    fn from(room_error: RoomError) -> Self {
        tracing::error!(err = error_chain(&room_error).as_str(), "Room error",);

        match room_error {
            RoomError::AliasInUse => safareig_core::ruma::api::client::Error {
                status_code: StatusCode::BAD_REQUEST,
                body: ErrorBody::Standard {
                    kind: ErrorKind::RoomInUse,
                    message: room_error.to_string(),
                },
            },
            RoomError::BadAlias => safareig_core::ruma::api::client::Error {
                status_code: StatusCode::BAD_REQUEST,
                body: ErrorBody::Standard {
                    kind: ErrorKind::BadAlias,
                    message: room_error.to_string(),
                },
            },
            RoomError::InvalidContent(_) => safareig_core::ruma::api::client::Error {
                status_code: StatusCode::BAD_REQUEST,
                body: ErrorBody::Standard {
                    kind: ErrorKind::InvalidParam,
                    message: room_error.to_string(),
                },
            },
            RoomError::InvalidRoomName
            | RoomError::InvalidStreamToken
            | RoomError::InvalidInitialStateEvent
            | RoomError::RoomNotFound(_) => safareig_core::ruma::api::client::Error {
                status_code: StatusCode::BAD_REQUEST,
                body: ErrorBody::Standard {
                    kind: ErrorKind::Unknown,
                    message: room_error.to_string(),
                },
            },
            RoomError::IncompatiblePduType
            | RoomError::Serde(_)
            | RoomError::SignatureError(_)
            | RoomError::InvalidEventId
            | RoomError::EventNotFound(_)
            | RoomError::Custom(_)
            | RoomError::MissingStateKey(_)
            | RoomError::Resolution(_)
            | RoomError::EventError(_)
            | RoomError::SignError(_)
            | RoomError::RedactError(_)
            | RoomError::IdError(_)
            | RoomError::MissingAuthEvent(_) => safareig_core::ruma::api::client::Error {
                status_code: StatusCode::INTERNAL_SERVER_ERROR,
                body: ErrorBody::Standard {
                    kind: ErrorKind::Unknown,
                    message: room_error.to_string(),
                },
            },
            // RoomError::ToDeviceError(e) => e.into(),
            RoomError::StorageError(e) => e.into(),
            RoomError::InviteError(e) => e.into(),
            RoomError::UserNotAllowed(_)
            | RoomError::ServerNotAllowed(_)
            | RoomError::Unauthorized(_) => safareig_core::ruma::api::client::Error {
                status_code: StatusCode::FORBIDDEN,
                body: ErrorBody::Standard {
                    kind: ErrorKind::forbidden(),
                    message: "user is not allowed".to_string(),
                },
            },
            RoomError::UnsupportedRoomVersion(version) => safareig_core::ruma::api::client::Error {
                status_code: StatusCode::BAD_REQUEST,
                body: ErrorBody::Standard {
                    kind: ErrorKind::UnsupportedRoomVersion,
                    message: format!("room version {version} not supported"),
                },
            },
            RoomError::Federation(e) => e.into(),
        }
    }
}

impl From<RoomError> for safareig_core::ruma::api::error::MatrixError {
    fn from(room_error: RoomError) -> Self {
        let client = safareig_core::ruma::api::client::Error::from(room_error);
        client.to_generic()
    }
}

#[derive(Clone, Inject)]
pub struct RoomStream {
    stream_storage: Arc<dyn RoomStreamStorage>,
    event_storage: Arc<dyn EventStorage>,
}

impl RoomStream {
    pub async fn stream_events<'a>(
        &self,
        range: Range<StreamToken>,
        filter: Option<&'a Filter<'a>>,
    ) -> Result<PaginationResponse<StreamToken, (StreamToken, Event)>, StorageError> {
        let filter = StreamFilter {
            filter,
            room_id: None,
            source: crate::storage::StreamSource::All,
        };
        let query = Pagination::new(range, Some(filter));

        let response = self.stream_storage.stream_events(query).await?;
        let previous = response.previous().cloned();
        let next = response.next().cloned();
        let records = response.records();
        let mut events = Vec::with_capacity(records.len());

        // TODO: Add batch operation here
        for (stream_token, event_id) in records {
            let event = self
                .event_storage
                .get_event(&event_id)
                .await?
                .ok_or_else(|| StorageError::Custom("missing event id".to_string()))?;
            events.push((stream_token, event));
        }

        Ok(PaginationResponse::new(events, previous, next))
    }

    #[instrument(skip_all)]
    pub async fn stream_room_events<'a>(
        &self,
        pagination: Pagination<StreamToken, Option<StreamFilter<'a>>>,
    ) -> Result<PaginationResponse<StreamToken, (StreamToken, Event)>, StorageError> {
        let mut events = Vec::new();
        let target = pagination.limit().unwrap_or(100) as usize;
        let (mut previous, mut next) = match pagination.direction() {
            safareig_core::pagination::Direction::Forward => {
                (StreamToken::infinite(), StreamToken::horizon())
            }
            safareig_core::pagination::Direction::Backward => {
                (StreamToken::horizon(), StreamToken::infinite())
            }
        };
        let mut next_pagination = pagination.clone();

        loop {
            // Query events
            let current_pagination = next_pagination.clone();
            let direction = current_pagination.direction();
            let filter = current_pagination.context();
            let response = self
                .stream_storage
                .stream_events(next_pagination.clone())
                .await?;

            match direction {
                safareig_core::pagination::Direction::Forward => {
                    previous = previous.min(
                        response
                            .previous()
                            .cloned()
                            .unwrap_or_else(StreamToken::horizon),
                    );
                    next = next.max(
                        response
                            .next()
                            .cloned()
                            .unwrap_or_else(StreamToken::infinite),
                    );
                }
                safareig_core::pagination::Direction::Backward => {
                    previous = previous.max(
                        response
                            .previous()
                            .cloned()
                            .unwrap_or_else(StreamToken::infinite),
                    );
                    next = next.min(
                        response
                            .next()
                            .cloned()
                            .unwrap_or_else(StreamToken::horizon),
                    );
                }
            };

            let records = response.records();

            if records.is_empty() {
                break;
            }

            let event_ids: Vec<OwnedEventId> = records.iter().map(|(_, e)| e.to_owned()).collect();
            let current_events = self.event_storage.get_events(&event_ids).await?;
            let records = records
                .into_iter()
                .zip(current_events.into_iter())
                .collect::<Vec<_>>();

            for ((stream_token, _event_id), event) in records {
                match &filter {
                    Some(filter) => {
                        if let Some(filter) = filter.filter {
                            if filter.is_timeline_event_allowed(&event) {
                                events.push((stream_token, event));
                            }
                        }
                    }
                    None => {
                        events.push((stream_token, event));
                    }
                }
            }

            if events.len() >= target {
                break;
            }

            next_pagination = match direction {
                safareig_core::pagination::Direction::Forward => {
                    let limit = target.saturating_sub(events.len());
                    let range = (Boundary::Exclusive(next), pagination.range().to().clone());

                    Pagination::new(range.into(), filter.clone())
                        .with_limit(Limit::from(limit as u32))
                        .with_direction(pagination::Direction::Forward)
                }
                safareig_core::pagination::Direction::Backward => {
                    let limit = target.saturating_sub(events.len());
                    let range = (pagination.range().from().clone(), Boundary::Exclusive(next));

                    Pagination::new(range.into(), filter.clone())
                        .with_limit(Limit::from(limit as u32))
                        .with_direction(pagination::Direction::Backward)
                }
            };
        }

        let previous = if previous == StreamToken::horizon() || previous == StreamToken::infinite()
        {
            None
        } else {
            Some(previous)
        };
        let next = if next == StreamToken::horizon() || next == StreamToken::infinite() {
            None
        } else {
            Some(next)
        };

        Ok(PaginationResponse::new(events, previous, next))
    }

    pub async fn stream_room_state_events<'a>(
        &self,
        range: Range<StreamToken>,
        room_id: &'a RoomId,
    ) -> Result<PaginationResponse<StreamToken, (StreamToken, Event)>, StorageError> {
        let stream_filter = StreamFilter {
            filter: None,
            room_id: Some(room_id),
            source: crate::storage::StreamSource::State,
        };
        let query = Pagination::new(range, Some(stream_filter));

        let response = self.stream_storage.stream_events(query).await?;
        let previous = response.previous().cloned();
        let next = response.next().cloned();
        let records = response.records();
        let mut events = Vec::with_capacity(records.len());

        // TODO: Add batch operation here
        for (stream_token, event_id) in records {
            let event = self
                .event_storage
                .get_event(&event_id)
                .await?
                .ok_or_else(|| StorageError::Custom("missing event id".to_string()))?;

            if event.state_key.is_some() {
                events.push((stream_token, event));
            }
        }

        Ok(PaginationResponse::new(events, previous, next))
    }

    pub async fn current_stream_token(&self) -> Result<StreamToken, StorageError> {
        self.stream_storage.current_stream_token().await
    }
}

// TODO: Remove in favor of RoomLoader
#[derive(Clone)]
pub struct Storage {
    pub(crate) room: Arc<dyn RoomStorage>,
    pub(crate) room_stream_storage: Arc<dyn RoomStreamStorage>,
    pub(crate) events: Arc<dyn EventStorage>,
    pub(crate) topology: Arc<dyn RoomTopologyStorage>,
    pub(crate) state: Arc<dyn StateStorage>,
    pub(crate) server_name: OwnedServerName,
    pub(crate) bus: Bus,
    pub(crate) invite: Arc<dyn InviteStorage>,
    pub(crate) publisher: Arc<RoomEventPublisher>,
    pub(crate) hooks: Arc<EventHooks>,
    pub(crate) remote_state_fetcher: Arc<RemoteStateFetcher>,
}

pub struct Room {
    storage: Storage,
    room_id: OwnedRoomId,
    descriptor: RoomDescriptor,
    room_version: RoomVersion,
}

impl Room {
    pub fn load(
        storage: Storage,
        room_id: &RoomId,
        descriptor: RoomDescriptor,
        room_version: RoomVersion,
    ) -> Room {
        Room {
            storage,
            room_id: room_id.to_owned(),
            descriptor,
            room_version,
        }
    }

    pub fn id(&self) -> &RoomId {
        &self.room_id
    }

    pub async fn add_event(
        &self,
        builder: EventBuilder,
        mut options: EventOptions,
    ) -> Result<(OwnedEventId, StreamToken), RoomError> {
        if builder.room_id != self.room_id {
            return Err(RoomError::Custom("unexepcted room id".to_string()));
        }

        let leaves = self
            .storage
            .topology
            .forward_extremities(&self.room_id)
            .await?;
        let state = self.load_room_state(&leaves).await?;
        let event = self
            .fill_event_from_forward_extremities(builder, &state, &leaves)
            .await?;

        let id = event.event_ref.owned_event_id();

        options = options.set_previous_room_state_id(*state.id());
        self.storage
            .events
            .put_event(&event, options.clone())
            .await?;

        let stream_token = self
            .storage
            .room_stream_storage
            .insert_event_to_stream(&event)
            .await?;

        self.storage
            .topology
            .adjust_extremities(&self.room_id, &event, *state.id())
            .await?;

        EVENTS_PROCESSED.add(1, &[KeyValue::new("type", "no-state")]);
        self.storage
            .publisher
            .publish_room_event(&event, stream_token, &options)
            .await;

        self.storage.bus.send_message(BusMessage::RoomUpdate {
            room_id: self.room_id.to_owned(),
            kind: event.kind,
            is_state: false,
        });

        Ok((id, stream_token))
    }

    pub fn build_auth_chain(events: &[Event]) -> AuthChain {
        events
            .iter()
            .map(|event_ref| {
                let state_index =
                    StateIndex::state_key(event_ref.kind.clone(), event_ref.state_key.clone());
                (state_index, event_ref)
            })
            .collect()
    }

    async fn check_user_in_room(&self, sender: &UserId) -> Result<(), RoomError> {
        let history_visibility = self
            .history_visibility()
            .await
            .map(|event| event.content.history_visibility)
            .unwrap_or(HistoryVisibility::Invited);

        if matches!(history_visibility, HistoryVisibility::WorldReadable) {
            return Ok(());
        }

        let state = self.state_at_leaves().await?;
        let membership = self
            .membership(&state, sender)
            .await
            .map(|content| content.content.membership)
            .unwrap_or(MembershipState::Leave);

        if matches!(membership, MembershipState::Join) {
            return Ok(());
        }

        Err(RoomError::UserNotAllowed(sender.to_owned()))
    }

    pub async fn add_state(
        &self,
        builder: EventBuilder,
    ) -> Result<(OwnedEventId, StreamToken), RoomError> {
        if builder.room_id != self.room_id {
            return Err(RoomError::Custom("unexepcted room id".to_string()));
        }

        let leaves = self
            .storage
            .topology
            .forward_extremities(&self.room_id)
            .await?;
        let state = self.load_room_state(&leaves).await?;
        let event = self
            .fill_state_from_forward_extremities(builder, &state, &leaves)
            .await?;

        if let Some(hook) = self.storage.hooks.hook(&event.kind) {
            hook.before_insert(&event).await?;
        }

        let state_index = StateIndex::from(&event);
        let content = event.content.clone();
        let event_id = event.event_ref.owned_event_id();
        let prev_state_id = *state.id();
        let new_state = state
            .fork(state_index.clone(), event.event_ref.event_id())
            .await?;

        let options = EventOptions::default()
            .set_previous_room_state_id(prev_state_id)
            .set_room_state_id(*new_state.id());
        self.storage
            .events
            .put_event(&event, options.clone())
            .await?;

        let stream_token = self
            .storage
            .room_stream_storage
            .insert_event_to_stream(&event)
            .await?;

        self.storage
            .topology
            .adjust_extremities(&self.room_id, &event, *new_state.id())
            .await?;

        if state_index.kind == TimelineEventType::RoomMember {
            let room_member: RoomMemberEventContent = serde_json::from_value(content)?;
            let user_id: &UserId = state_index
                .state_key
                .as_ref()
                .map(|user| {
                    user.as_str()
                        .try_into()
                        .map_err(|_| RoomError::Custom("invalid state key".to_string()))
                })
                .transpose()?
                .ok_or_else(|| RoomError::Custom("missing user id".to_string()))?;

            self.handle_room_member(
                &event.sender,
                user_id,
                stream_token,
                &room_member,
                &event_id,
            )
            .await?;
        }

        EVENTS_PROCESSED.add(1, &[KeyValue::new("type", "state")]);

        self.storage
            .publisher
            .publish_room_event(&event, stream_token, &options)
            .await;

        self.storage.bus.send_message(BusMessage::RoomUpdate {
            room_id: self.room_id.to_owned(),
            kind: event.kind,
            is_state: true,
        });

        Ok((event_id, stream_token))
    }

    pub async fn leave_room(
        &self,
        sender: &UserId,
        profile_loader: &ProfileLoader,
    ) -> Result<(), RoomError> {
        let profile = profile_loader.load(sender).await?;
        let content = member_event_with_profile(MembershipState::Leave, profile.as_ref());
        let builder = EventBuilder::state(
            sender,
            content.into_content()?,
            &self.room_id,
            Some(sender.to_string()),
        );

        self.add_state(builder).await?;

        Ok(())
    }

    pub async fn make_join(
        &self,
        sender: &UserId,
        profile_loader: &ProfileLoader,
    ) -> Result<Event, RoomError> {
        let builder = self.join_builder(sender, profile_loader).await?;
        let leaves = self
            .storage
            .topology
            .forward_extremities(&self.room_id)
            .await?;
        let state = self.load_room_state(&leaves).await?;

        let event = self
            .fill_state_from_forward_extremities(builder, &state, &leaves)
            .await?;
        Ok(event)
    }

    pub async fn make_leave(
        &self,
        sender: &UserId,
        profile_loader: &ProfileLoader,
    ) -> Result<Event, RoomError> {
        let profile = profile_loader.load(sender).await?;
        let content = member_event_with_profile(MembershipState::Leave, profile.as_ref());
        let builder = EventBuilder::state(
            sender,
            content.into_content()?,
            &self.room_id,
            Some(sender.to_string()),
        );

        let leaves = self
            .storage
            .topology
            .forward_extremities(&self.room_id)
            .await?;
        let state = self.load_room_state(&leaves).await?;

        let event = self
            .fill_state_from_forward_extremities(builder, &state, &leaves)
            .await?;
        Ok(event)
    }

    pub async fn event(&self, event_id: &EventId) -> Result<Option<Event>, RoomError> {
        let event = self
            .storage
            .events
            .get_event(event_id)
            .await?
            .and_then(|e| {
                if e.room_id != self.room_id {
                    tracing::warn!(
                        event_id = e.event_ref.event_id().as_str(),
                        "Ignoring event id which does not belong to the current room"
                    );

                    return None;
                }

                Some(e)
            });

        Ok(event)
    }

    pub async fn events(&self, event_ids: &[OwnedEventId]) -> Result<Vec<Event>, RoomError> {
        let events = self
            .storage
            .events
            .get_events(event_ids)
            .await?
            .into_iter()
            .filter_map(|e| {
                if e.room_id != self.room_id {
                    tracing::warn!(
                        event_id = e.event_ref.event_id().as_str(),
                        "Ignoring event id which does not belong to the current room"
                    );

                    return None;
                }

                Some(e)
            })
            .collect();

        Ok(events)
    }

    pub async fn state_at(&self, event_id: &EventId) -> Result<Option<RoomState>, RoomError> {
        let state_id = self
            .storage
            .state
            .state_at(&self.room_id, event_id)
            .await?
            .map(|state_id| RoomState::load(state_id, self.storage.state.clone()));

        Ok(state_id)
    }

    pub async fn allows_federation(&self) -> Result<bool, RoomError> {
        let event_with_id = self.create_event().await?;

        Ok(event_with_id.content.federate)
    }

    pub async fn state_event(
        &self,
        index: &StateIndex,
        state: &RoomState,
    ) -> Result<Option<Event>, RoomError> {
        let event_id = state.event_id_for_state_key(index).await?;

        if let Some(event_id) = event_id {
            return self.event(&event_id).await;
        }

        Ok(None)
    }

    pub async fn depth_at_leaves(&self) -> u64 {
        let leaves = self
            .storage
            .topology
            .forward_extremities(&self.room_id)
            .await
            .unwrap_or_default();

        leaves
            .iter()
            .max_by_key(|leaf| leaf.depth)
            .map(|leaf| leaf.depth)
            .unwrap_or_default()
    }

    pub async fn forward_extremities(&self) -> Result<Vec<Leaf>, StorageError> {
        self.storage
            .topology
            .forward_extremities(&self.room_id)
            .await
    }

    pub async fn complete_event(&self, builder: EventBuilder) -> Result<Event, RoomError> {
        let leaves = self
            .storage
            .topology
            .forward_extremities(&self.room_id)
            .await?;
        let state = self.load_room_state(&leaves).await?;
        let event = self
            .fill_state_from_forward_extremities(builder, &state, &leaves)
            .await?;

        Ok(event)
    }

    pub fn room_version(&self) -> &RoomVersion {
        &self.room_version
    }

    pub async fn state_for_user(&self, user_id: &UserId) -> Result<Option<RoomState>, RoomError> {
        let current_state = self.state_at_leaves().await?;
        match self.membership(&current_state, user_id).await {
            Ok(membership) => match membership.content.membership {
                MembershipState::Join => Ok(Some(current_state)),
                MembershipState::Leave => {
                    let left_id = membership.event_id;
                    let state_id = self
                        .state_id_after_event(&left_id)
                        .await?
                        .ok_or_else(|| RoomError::Custom("missing state".to_string()))?;
                    let state = RoomState::load(state_id, self.storage.state.clone());

                    Ok(Some(state))
                }
                _ => Err(RoomError::UserNotAllowed(user_id.to_owned())),
            },
            Err(_e) => Err(RoomError::UserNotAllowed(user_id.to_owned())),
        }
    }

    pub async fn event_id_for_transaction(
        &self,
        room_id: &RoomId,
        txn_id: &TransactionId,
    ) -> Result<Option<OwnedEventId>, RoomError> {
        let event = self
            .storage
            .events
            .get_event_by_transaction_id(room_id, txn_id)
            .await?;

        Ok(event.map(|event| event.event_ref.owned_event_id()))
    }

    pub async fn can_redact_event(
        &self,
        redactor: &UserId,
        event: &Event,
    ) -> Result<bool, RoomError> {
        if redactor.server_name() == event.sender.server_name()
            && self.storage.server_name != redactor.server_name()
        {
            return Ok(true);
        }

        if redactor == event.sender {
            return Ok(true);
        }

        let state = self.state_at_leaves().await?;
        let power_levels = self.power_levels(&state).await?;
        let checker = PowerLevelChecker::new(power_levels);

        Ok(checker.can_redact(redactor))
    }

    pub async fn has_power_for_event(
        &self,
        user: &UserId,
        room_event_type: &TimelineEventType,
    ) -> Result<bool, RoomError> {
        let state = self.state_at_leaves().await?;
        let power_levels = self.power_levels(&state).await?;
        let checker = PowerLevelChecker::new(power_levels);

        Ok(checker.can_send(user, room_event_type))
    }

    async fn fill_event_from_forward_extremities(
        &self,
        builder: EventBuilder,
        state: &RoomState,
        forward_extremities: &[Leaf],
    ) -> Result<Event, RoomError> {
        let depth = self.calculate_depth(forward_extremities);
        let prev_events = self.get_prev_events(forward_extremities);
        let auth_events = state
            .auth_events(&builder.sender, None, self.storage.events.as_ref())
            .await?;
        let auth_ids = auth_events.iter().map(|e| e.event_ref.clone()).collect();

        let event = builder
            .prev_events(prev_events)
            .auth_events(auth_ids)
            .depth(depth)
            .build(self.room_version.event_id_calculator())?;

        let auth_chain = Self::build_auth_chain(&auth_events);
        self.room_version
            .authorizer()
            .authorize(&auth_chain, &event)?;

        Ok(event)
    }

    async fn fill_state_from_forward_extremities(
        &self,
        builder: EventBuilder,
        state: &RoomState,
        forward_extremities: &[Leaf],
    ) -> Result<Event, RoomError> {
        let depth = self.calculate_depth(forward_extremities);
        let prev_events = self.get_prev_events(forward_extremities);
        let state_info = StateInfo::try_from(&builder)
            .map_err(|_| RoomError::Custom("could not convert to state info".to_string()))?;
        let auth_events = state
            .auth_events(
                &builder.sender,
                Some(state_info),
                self.storage.events.as_ref(),
            )
            .await?;
        let state_index = StateIndex::state_key(builder.kind.clone(), builder.state_key.clone());
        let prev_content = self.current_content(state, &state_index).await?;
        let auth_ids = auth_events.iter().map(|e| e.event_ref.clone()).collect();

        let event = builder
            .prev_events(prev_events)
            .auth_events(auth_ids)
            .depth(depth)
            .prev_content(prev_content)
            .build(self.room_version.event_id_calculator())?;

        let auth_chain = Self::build_auth_chain(&auth_events);
        self.room_version
            .authorizer()
            .authorize(&auth_chain, &event)?;

        Ok(event)
    }

    async fn join_builder(
        &self,
        user_id: &UserId,
        profile_loader: &ProfileLoader,
    ) -> Result<EventBuilder, RoomError> {
        let profile = profile_loader.load(user_id).await?;
        let content = member_event_with_profile(MembershipState::Join, profile.as_ref());
        let builder = EventBuilder::state(
            user_id,
            content.into_content()?,
            &self.room_id,
            Some(user_id.to_string()),
        );

        Ok(builder)
    }

    async fn handle_room_member(
        &self,
        sender: &UserId,
        target: &UserId,
        stream_token: StreamToken,
        room_member: &RoomMemberEventContent,
        event_id: &EventId,
    ) -> Result<(), RoomError> {
        // TODO: This should be on a transaction with `add_event` method, probably
        if let MembershipState::Invite = room_member.membership {
            self.storage
                .invite
                .add_invite(
                    target,
                    Invite::Local(LocalInvite {
                        room: self.id().to_owned(),
                        event_id: event_id.to_owned(),
                    }),
                )
                .await?;
        }

        self.storage
            .room
            .update_membership(
                target,
                &self.room_id,
                Membership {
                    state: room_member.membership.clone(),
                    stream: stream_token,
                    event_id: event_id.to_owned(),
                },
            )
            .await?;

        if let MembershipState::Invite = room_member.membership {
            self.storage.bus.send_message(BusMessage::RoomInvited(
                self.room_id.clone(),
                sender.to_owned(),
                target.to_owned(),
            ))
        }

        Ok(())
    }

    pub async fn get_event(&self, event_id: &EventId) -> Result<Option<Event>, RoomError> {
        self.storage
            .events
            .get_event(event_id)
            .await
            .map_err(|e| e.into())
    }

    pub async fn event_content<E: EventContent + DeserializeOwned>(
        &self,
        key: &StateIndex,
        state: &RoomState,
    ) -> Result<Option<ContentWithId<E>>, RoomError> {
        let event_id = state.event_id_for_state_key(key).await?;

        if event_id.is_none() {
            return Ok(None);
        }

        let event_id = event_id.unwrap();
        self.storage
            .events
            .get_event(&event_id)
            .await?
            .map(|event| {
                serde_json::from_value::<E>(event.content).map(|content| ContentWithId {
                    content,
                    event_id: event_id.clone(),
                })
            })
            .transpose()
            .map_err(|_| RoomError::EventNotFound(event_id.clone()))
    }

    pub async fn state_at_leaves(&self) -> Result<RoomState, RoomError> {
        let leaves = self
            .storage
            .topology
            .forward_extremities(&self.room_id)
            .await?;

        self.load_room_state(&leaves).await
    }

    pub async fn has_event(&self, event_id: &EventId) -> Result<bool, RoomError> {
        let event = self.storage.events.get_event(event_id).await?;
        Ok(event.is_some())
    }

    pub async fn create_event(&self) -> Result<ContentWithId<RoomCreateEventContent>, RoomError> {
        let state_at_leaf = self.state_at_leaves().await?;
        let state_index = StateIndex::new(TimelineEventType::RoomCreate);
        let event = self
            .event_content(&state_index, &state_at_leaf)
            .await?
            .ok_or(RoomError::MissingStateKey(state_index))?;

        Ok(event)
    }

    pub async fn join_rules(&self) -> Result<ContentWithId<RoomJoinRulesEventContent>, RoomError> {
        let state_at_leaf = self.state_at_leaves().await?;
        let state_index = StateIndex::new(TimelineEventType::RoomJoinRules);
        let event = self
            .event_content(&state_index, &state_at_leaf)
            .await?
            .ok_or(RoomError::MissingStateKey(state_index))?;

        Ok(event)
    }

    pub async fn history_visibility(
        &self,
    ) -> Result<ContentWithId<RoomHistoryVisibilityEventContent>, RoomError> {
        let state_at_leaf = self.state_at_leaves().await?;
        let state_index = StateIndex::new(TimelineEventType::RoomHistoryVisibility);
        let event = self
            .event_content(&state_index, &state_at_leaf)
            .await?
            .ok_or(RoomError::MissingStateKey(state_index))?;

        Ok(event)
    }

    pub async fn encryption(&self) -> Result<ContentWithId<RoomEncryptionEventContent>, RoomError> {
        let state_at_leaf = self.state_at_leaves().await?;
        let state_index = StateIndex::new(TimelineEventType::RoomEncryption);
        let event = self
            .event_content(&state_index, &state_at_leaf)
            .await?
            .ok_or(RoomError::MissingStateKey(state_index))?;

        Ok(event)
    }

    pub async fn guest_access(
        &self,
    ) -> Result<ContentWithId<RoomGuestAccessEventContent>, RoomError> {
        let state_at_leaf = self.state_at_leaves().await?;
        let state_index = StateIndex::new(TimelineEventType::RoomGuestAccess);
        let event = self
            .event_content(&state_index, &state_at_leaf)
            .await?
            .ok_or(RoomError::MissingStateKey(state_index))?;

        Ok(event)
    }

    pub fn version(&self) -> &RoomVersionId {
        &self.descriptor.room_version
    }

    pub async fn create_extra(&self) -> Result<ContentWithId<CreateRoomExtra>, RoomError> {
        let state_at_leaf = self.state_at_leaves().await?;
        let state_index = StateIndex::new(TimelineEventType::from(CREATE_ROOM_EXTRA_TYPE));
        let event = self
            .event_content(&state_index, &state_at_leaf)
            .await?
            .ok_or(RoomError::MissingStateKey(state_index))?;

        Ok(event)
    }

    pub async fn room_name(&self) -> Result<ContentWithId<RoomNameEventContent>, RoomError> {
        let state_at_leaf = self.state_at_leaves().await?;
        let state_index = StateIndex::new(TimelineEventType::RoomName);
        let event = self
            .event_content(&state_index, &state_at_leaf)
            .await?
            .ok_or(RoomError::MissingStateKey(state_index))?;

        Ok(event)
    }

    pub async fn room_topic(&self) -> Result<ContentWithId<RoomTopicEventContent>, RoomError> {
        let state_at_leaf = self.state_at_leaves().await?;
        let state_index = StateIndex::new(TimelineEventType::RoomTopic);
        let event = self
            .event_content(&state_index, &state_at_leaf)
            .await?
            .ok_or(RoomError::MissingStateKey(state_index))?;

        Ok(event)
    }

    pub async fn canonical_alias(
        &self,
    ) -> Result<ContentWithId<RoomCanonicalAliasEventContent>, RoomError> {
        let state_at_leaf = self.state_at_leaves().await?;
        let state_index = StateIndex::new(TimelineEventType::RoomCanonicalAlias);
        let event = self
            .event_content(&state_index, &state_at_leaf)
            .await?
            .ok_or(RoomError::MissingStateKey(state_index))?;

        Ok(event)
    }

    pub async fn membership(
        &self,
        state: &RoomState,
        user_id: &UserId,
    ) -> Result<ContentWithId<RoomMemberEventContent>, RoomError> {
        let state_index =
            StateIndex::with_state_key(TimelineEventType::RoomMember, user_id.to_string());
        let event = self
            .event_content(&state_index, state)
            .await?
            .ok_or(RoomError::MissingStateKey(state_index))?;

        Ok(event)
    }

    pub async fn power_levels(
        &self,
        state: &RoomState,
    ) -> Result<RoomPowerLevelsEventContent, RoomError> {
        let state_index = StateIndex::new(TimelineEventType::RoomPowerLevels);
        let power_level = self
            .event_content(&state_index, state)
            .await?
            .map(|e| e.content)
            .unwrap_or_default();

        Ok(power_level)
    }

    pub async fn acl(
        &self,
        state: &RoomState,
    ) -> Result<Option<RoomServerAclEventContent>, RoomError> {
        let state_index = StateIndex::new(TimelineEventType::RoomServerAcl);
        let content = self
            .event_content(&state_index, state)
            .await?
            .map(|e| e.content);

        Ok(content)
    }

    /// Insert event and return the state id previous to the inserted event
    #[tracing::instrument(skip(self, event), fields(event = %event.event_ref.owned_event_id()))]
    pub async fn insert_event(
        &self,
        event: &Event,
        room_version: &RoomVersionId,
        add_leaf: bool,
        stream: bool, // TODO: Either add a new method or factory into Options bit flag
    ) -> Result<RoomStateId, RoomError> {
        // Get state at `prev_event` and authorize event
        // Get state at leaves and authorize event
        let mut new_state_id = None;
        let room_state = self.state_on_event(event).await?;
        let state_id_prev_events = *room_state.id();
        let state_info = event.state_key.as_ref().map(|_| StateInfo::from(event));

        // Calculate auth chain on state and authorize it.
        let auth_events = room_state
            .auth_events(&event.sender, state_info, self.storage.events.as_ref())
            .await?;

        let auth_chain = Self::build_auth_chain(&auth_events);
        self.room_version
            .authorizer()
            .authorize(&auth_chain, event)?;

        let mut leaf = Leaf {
            event_id: event.event_ref.clone(),
            depth: event.depth.saturating_add(1),
            state_at: state_id_prev_events,
        };

        if let Some(ref key) = event.state_key {
            // Is state event. Additional checks!
            let new_state = room_state
                .fork(
                    StateIndex::with_state_key(event.kind.clone(), key.clone()),
                    event.event_ref.event_id(),
                )
                .await?;
            leaf.state_at = *new_state.id();
            new_state_id = Some(*new_state.id());
        }

        let mut options = EventOptions::default().set_previous_room_state_id(state_id_prev_events);

        if let Some(state_id) = new_state_id.as_ref() {
            options = options.set_room_state_id(*state_id);
        }

        self.storage.events.put_event(event, options).await?;

        if stream {
            let stream_token = self
                .storage
                .room_stream_storage
                .insert_event_to_stream(event)
                .await?;

            if event.kind == TimelineEventType::RoomMember {
                let content: RoomMemberEventContent =
                    serde_json::from_value(event.content.clone())?;
                let target = event.state_key.as_ref().ok_or_else(|| {
                    RoomError::MissingStateKey(StateIndex::new(TimelineEventType::RoomMember))
                })?;
                let target_user = UserId::parse(target.as_str()).map_err(|_| {
                    RoomError::Custom("state key is not a valid user_id".to_string())
                })?;

                self.handle_room_member(
                    &event.sender,
                    &target_user,
                    stream_token,
                    &content,
                    event.event_ref.event_id(),
                )
                .await?;
            }

            self.storage.bus.send_message(BusMessage::RoomUpdate {
                room_id: self.room_id.to_owned(),
                kind: event.kind.clone(),
                is_state: false,
            });
        }

        // TODO: 2PC
        if add_leaf {
            self.storage
                .topology
                .adjust_extremities(
                    &self.room_id,
                    event,
                    new_state_id.unwrap_or(state_id_prev_events),
                )
                .await?;
        }

        Ok(state_id_prev_events)
    }

    /// Insert event skipping any validation. All autohrization rules should be applied before calling to this function.
    #[tracing::instrument(skip(self, event), fields(event = %event.event_ref.owned_event_id()))]
    pub async fn insert_backfill_event(
        &self,
        event: &Event,
        stream: bool,
        options: EventOptions,
    ) -> Result<(), RoomError> {
        let is_rejected = options.is_rejected();
        self.storage.events.put_event(event, options).await?;

        if stream && !is_rejected {
            self.storage
                .room_stream_storage
                .insert_event_to_stream(event)
                .await?;
        }

        Ok(())
    }

    /// Returns the room state on the given parent events. If there's any unknown state snapshot,
    /// it will be fetched from a remote, participant home server.
    /// After retrieving all snapshots, it will run a state resolution and generate a new snapshot.
    #[tracing::instrument(skip(self))]
    #[async_recursion::async_recursion]
    pub async fn state_on_event(&self, event: &Event) -> Result<RoomState, RoomError> {
        let state_at_event = self
            .storage
            .state
            .state_at(self.id(), event.event_ref.event_id())
            .await?;

        if let Some(room_state_id) = state_at_event {
            tracing::info!(?event, ?room_state_id, "Known state id for event");
            return Ok(RoomState::load(room_state_id, self.storage.state.clone()));
        }

        let mut states: HashSet<RoomStateId> = HashSet::new();
        let mut missing: HashSet<&EventId> = HashSet::new();

        for eid in &event.prev_events {
            let event_id = eid.event_id();
            let state_id_after = self.state_id_after_event(event_id).await?;
            tracing::info!(?eid, ?state_id_after, "State id after");

            match state_id_after {
                Some(state_id) => states.insert(state_id),
                None => missing.insert(event_id),
            };
        }

        tracing::debug!(
            "States at previous events. Missing {}/{}",
            missing.len(),
            event.prev_events.len()
        );

        if !missing.is_empty() {
            let policy = RemoteSelectorPolicy::Random(self.id().to_owned());

            for missing_event in missing {
                let state_id = self
                    .storage
                    .remote_state_fetcher
                    .fetch_state(self, missing_event, &policy)
                    .await?;
                states.insert(state_id);
                let extremity = BackwardExtremity::new(
                    event.event_ref.owned_event_id(),
                    // TODO: This is incorrect and I will need to check if we do really need to store the topological token
                    event.topological_token().prev(),
                );
                self.storage
                    .topology
                    .add_backward_extremities(self.id(), &[extremity])
                    .await?;
            }
        }

        let state: Result<_, RoomError> = match states.len() {
            0 => Ok(RoomState::new(self.storage.state.clone())),
            1 => {
                let state_id = *states.iter().next().unwrap();
                Ok(RoomState::load(state_id, self.storage.state.clone()))
            }
            _ => {
                tracing::info!("Resolving state with {} forward extremities", states.len());
                let params = StateResParameters {
                    state_ids: states,
                    room_id: self.room_id.to_owned(),
                    room_version: self.room_version.id().to_owned(),
                };

                Ok(self.room_version.state_resolution().resolve(params).await?)
            }
        };

        let state = state?;

        // We could resolve an state for this event. Assign the state to the event so it can be
        // used when looking up the state for this event in a follow-up iteration.
        self.storage
            .state
            .assign_state(&self.room_id, event.event_ref.event_id(), state.id())
            .await?;
        tracing::warn!(
            "Assigning state for {:?} at {}",
            event.event_ref.event_id(),
            state.id()
        );

        Ok(state)
    }

    pub async fn deepest_event(&self) -> Result<Event, RoomError> {
        let leaves = self.storage.topology.forward_extremities(self.id()).await?;
        let deepest_leaf = leaves
            .iter()
            .max_by(|left, right| left.depth.cmp(&right.depth))
            .ok_or_else(|| RoomError::Custom("room withour leaves".to_string()))?;

        self.storage
            .events
            .get_event(deepest_leaf.event_id.event_id())
            .await?
            .ok_or_else(|| {
                RoomError::Custom(format!(
                    "missing event id: {}",
                    deepest_leaf.event_id.event_id()
                ))
            })
    }

    pub async fn topological_events(
        &self,
        range: Range<TopologicalToken>,
        limit: u16,
        direction: Direction,
    ) -> Result<PaginationResponse<TopologicalToken, Event>, RoomError> {
        let query = Pagination::new(range, ())
            .with_direction(direction.into())
            .with_limit(pagination::Limit::from(limit as u32));

        let response = self.storage.room.room_events(&self.room_id, query).await?;

        // TODO: This will need to do visibility checks in the future. It would be smart to also
        // use iterators to avoid buffering all events.

        Ok(response)
    }

    pub async fn events_for_type(
        &self,
        kind: TimelineEventType,
        state: Option<&RoomState>,
    ) -> Result<Vec<Event>, RoomError> {
        let state = match state {
            Some(state) => state.clone(),
            None => self.state_at_leaves().await?,
        };

        let event_ids = state.event_ids_for_type(kind).await?;
        let events = self.storage.events.get_events(&event_ids).await?;

        Ok(events)
    }

    pub async fn event_content_for_keys(
        &self,
        keys: &[StateIndex],
        state: &RoomState,
    ) -> Result<PartialStateEvents, StorageError> {
        let contents = state
            .event_content_for_keys(keys, self.storage.events.as_ref())
            .await?;

        Ok(PartialStateEvents(contents))
    }

    #[tracing::instrument(skip(self))]
    pub async fn room_summary(&self) -> Result<RoomSummary, RoomError> {
        let state = self.state_at_leaves().await?;

        let state_keys = vec![
            StateIndex::canonical_alias(),
            StateIndex::name(),
            StateIndex::topic(),
            StateIndex::create(),
            StateIndex::history_visibility(),
            StateIndex::room_avatar(),
            StateIndex::join_rule(),
        ];

        let partial_state = self
            .event_content_for_keys(state_keys.as_ref(), &state)
            .await?;

        let alias = partial_state
            .content::<RoomCanonicalAliasEventContent>(StateEventType::RoomCanonicalAlias)
            .unwrap_or_default();

        let room_name = partial_state
            .content::<RoomNameEventContent>(StateEventType::RoomName)
            .map(|content| content.name);

        let room_topic = partial_state
            .content::<RoomTopicEventContent>(StateEventType::RoomTopic)
            .map(|content| content.topic);

        let room_create =
            partial_state.content::<RoomCreateEventContent>(StateEventType::RoomCreate);

        let room_type = room_create
            .as_ref()
            .and_then(|create| create.room_type.clone());
        let federable = room_create.map(|create| create.federate).unwrap_or(false);

        let world_readable = partial_state
            .content::<RoomHistoryVisibilityEventContent>(StateEventType::RoomHistoryVisibility)
            .map(|content| matches!(content.history_visibility, HistoryVisibility::WorldReadable))
            .unwrap_or_default();

        let guest_can_join = partial_state
            .content::<RoomGuestAccessEventContent>(StateEventType::RoomGuestAccess)
            .map(|content| matches!(content.guest_access, GuestAccess::CanJoin))
            .unwrap_or_default();

        let room_avatar = partial_state
            .content::<RoomAvatarEventContent>(StateEventType::RoomAvatar)
            .unwrap_or_default();

        let join_rule = partial_state
            .content::<RoomJoinRulesEventContent>(StateEventType::RoomJoinRules)
            .map(|content| content.join_rule)
            .unwrap_or(JoinRule::Private);

        // TODO: This does not count joined-only members
        let members = state.members().await?.len();

        Ok(RoomSummary {
            canoncical_alias: alias.alias,
            name: room_name,
            num_joined_members: members as u32,
            room_id: self.room_id.to_owned(),
            topic: room_topic,
            world_readable,
            guest_can_join,
            avatar_url: room_avatar.url,
            join_rule,
            room_type,
            federable,
        })
    }

    pub async fn is_joined(&self, state: &RoomState, user_id: &UserId) -> Result<bool, RoomError> {
        let state_index =
            StateIndex::with_state_key(TimelineEventType::RoomMember, user_id.to_string());
        let state = self
            .event_content::<RoomMemberEventContent>(&state_index, state)
            .await?
            .map(|cwid| cwid.content.membership)
            .unwrap_or(MembershipState::Leave);

        Ok(matches!(state, MembershipState::Join))
    }

    pub async fn forget(&self, user_id: &UserId) -> Result<(), RoomError> {
        let state = self.state_at_leaves().await?;
        let current_membership = self
            .membership(&state, user_id)
            .await
            .map(|c| c.content.membership)
            .unwrap_or(MembershipState::Leave);

        match current_membership {
            MembershipState::Leave => (),
            _ => {
                return Err(RoomError::UserNotAllowed(user_id.to_owned()));
            }
        }

        self.storage
            .room
            .remove_membership(user_id, &self.room_id)
            .await?;

        Ok(())
    }

    async fn state_id_after_event(&self, eid: &EventId) -> Result<Option<RoomStateId>, RoomError> {
        // This is the state previous to the state event. We need to figure out the state after
        // the event to obtain the state previous to our current event.
        let event = self.event(eid).await?;

        let event = match event {
            Some(event) => event,
            // If event does not exist, we won't have it's state id either
            None => return Ok(None),
        };

        let state_id = self.storage.state.state_at(&self.room_id, eid).await?;

        let maybe_state_id = if let Some(state_id) = state_id {
            match &event.state_key {
                Some(_) => {
                    // Event is a state event and we need to calculate the state after this event
                    let state = RoomState::load(state_id, self.storage.state.clone());
                    let state_index = StateIndex::from(&event);
                    let state = state.fork(state_index, eid).await?;

                    Some(*state.id())
                }
                None => {
                    // Event is not a state event, so we can safely assume that the state snapshot
                    // is the same for this event.

                    Some(state_id)
                }
            }
        } else {
            None
        };

        Ok(maybe_state_id)
    }

    async fn current_content(
        &self,
        state: &RoomState,
        key: &StateIndex,
    ) -> Result<Option<serde_json::Value>, RoomError> {
        let event_id = state.event_id_for_state_key(key).await?;
        let maybe_content = match event_id {
            Some(eid) => self
                .storage
                .events
                .get_event(&eid)
                .await?
                .map(|event| event.content),
            None => None,
        };

        Ok(maybe_content)
    }

    async fn load_room_state(&self, leaves: &[Leaf]) -> Result<RoomState, RoomError> {
        let states: HashSet<RoomStateId> = leaves.iter().map(|leaf| leaf.state_at).collect();

        let state = match states.len() {
            0 => RoomState::new(self.storage.state.clone()),
            1 => {
                let state_id = *states.iter().next().unwrap();
                RoomState::load(state_id, self.storage.state.clone())
            }
            _ => {
                tracing::info!("Resolving state with {} forward extremities", states.len());
                let params = StateResParameters {
                    state_ids: states,
                    room_id: self.room_id.to_owned(),
                    room_version: self.room_version.id().to_owned(),
                };

                self.room_version.state_resolution().resolve(params).await?
            }
        };

        Ok(state)
    }

    fn calculate_depth(&self, leaves: &[Leaf]) -> u64 {
        leaves
            .iter()
            .map(|leaf| leaf.depth)
            .max()
            .unwrap_or(0)
            .saturating_add(1)
    }

    fn get_prev_events(&self, leaves: &[Leaf]) -> Vec<EventRef> {
        leaves
            .iter()
            .map(|leaf| leaf.event_id.clone())
            .collect::<Vec<EventRef>>()
    }
}

pub struct RoomSummary {
    pub canoncical_alias: Option<OwnedRoomAliasId>,
    pub name: Option<String>,
    pub num_joined_members: u32,
    pub room_id: OwnedRoomId,
    pub topic: Option<String>,
    pub world_readable: bool,
    pub guest_can_join: bool,
    pub avatar_url: Option<OwnedMxcUri>,
    pub join_rule: JoinRule,
    pub room_type: Option<RoomType>,
    pub federable: bool,
}

pub struct PartialStateEvents(HashMap<StateEventType, serde_json::Value>);

impl PartialStateEvents {
    pub fn content<T: EventContent + DeserializeOwned>(
        &self,
        event_type: StateEventType,
    ) -> Option<T> {
        self.0
            .get(&event_type)
            .map(|value| serde_json::from_value::<T>(value.clone()))
            .transpose()
            .ok()
            .flatten()
    }
}

pub struct ContentWithId<E: EventContent + DeserializeOwned> {
    pub content: E,
    #[allow(unused)]
    pub event_id: OwnedEventId,
}

// Try convert the given payload depending on the event type.
// Since Ruma does not offer a deserialization layer anymore, we've created this new enum which will be used
// to ensure that the give content matches with the given types.
// This will cause some maintainability issues because we will need to keep a mapping of event types  to event contents.
pub enum SafareigMessageLikeEventContent {
    CallAnswer(Box<CallAnswerEventContent>),
    CallInvite(Box<CallInviteEventContent>),
    CallHangup(Box<CallHangupEventContent>),
    CallCandidates(Box<CallCandidatesEventContent>),
    KeyVerificationReady(Box<KeyVerificationReadyEventContent>),
    KeyVerificationStart(Box<KeyVerificationStartEventContent>),
    KeyVerificationCancel(Box<KeyVerificationCancelEventContent>),
    KeyVerificationAccept(Box<KeyVerificationAcceptEventContent>),
    KeyVerificationKey(Box<KeyVerificationKeyEventContent>),
    KeyVerificationMac(Box<KeyVerificationMacEventContent>),
    KeyVerificationDone(Box<KeyVerificationDoneEventContent>),
    Encrypted(Box<RoomEncryptedEventContent>),
    Message(Box<RoomMessageEventContent>),
    Redaction(Box<RoomRedactionEventContent>),
    Sticker(Box<StickerEventContent>),
    Custom(Content),
}

impl SafareigMessageLikeEventContent {
    pub fn from_parts(
        kind: &MessageLikeEventType,
        json: &RawValue,
    ) -> Result<Self, serde_json::Error> {
        let event_content = match kind {
            MessageLikeEventType::CallAnswer => SafareigMessageLikeEventContent::CallAnswer(
                Box::new(CallAnswerEventContent::from_parts(&kind.to_string(), json)?),
            ),
            MessageLikeEventType::CallInvite => SafareigMessageLikeEventContent::CallInvite(
                Box::new(CallInviteEventContent::from_parts(&kind.to_string(), json)?),
            ),
            MessageLikeEventType::CallHangup => SafareigMessageLikeEventContent::CallHangup(
                Box::new(CallHangupEventContent::from_parts(&kind.to_string(), json)?),
            ),
            MessageLikeEventType::CallCandidates => {
                SafareigMessageLikeEventContent::CallCandidates(Box::new(
                    CallCandidatesEventContent::from_parts(&kind.to_string(), json)?,
                ))
            }
            MessageLikeEventType::KeyVerificationReady => {
                SafareigMessageLikeEventContent::KeyVerificationReady(Box::new(
                    KeyVerificationReadyEventContent::from_parts(&kind.to_string(), json)?,
                ))
            }
            MessageLikeEventType::KeyVerificationStart => {
                SafareigMessageLikeEventContent::KeyVerificationStart(Box::new(
                    KeyVerificationStartEventContent::from_parts(&kind.to_string(), json)?,
                ))
            }
            MessageLikeEventType::KeyVerificationCancel => {
                SafareigMessageLikeEventContent::KeyVerificationCancel(Box::new(
                    KeyVerificationCancelEventContent::from_parts(&kind.to_string(), json)?,
                ))
            }
            MessageLikeEventType::KeyVerificationKey => {
                SafareigMessageLikeEventContent::KeyVerificationKey(Box::new(
                    KeyVerificationKeyEventContent::from_parts(&kind.to_string(), json)?,
                ))
            }
            MessageLikeEventType::KeyVerificationMac => {
                SafareigMessageLikeEventContent::KeyVerificationMac(Box::new(
                    KeyVerificationMacEventContent::from_parts(&kind.to_string(), json)?,
                ))
            }
            MessageLikeEventType::KeyVerificationDone => {
                SafareigMessageLikeEventContent::KeyVerificationDone(Box::new(
                    KeyVerificationDoneEventContent::from_parts(&kind.to_string(), json)?,
                ))
            }
            MessageLikeEventType::RoomEncrypted => {
                SafareigMessageLikeEventContent::Encrypted(Box::new(
                    RoomEncryptedEventContent::from_parts(&kind.to_string(), json)?,
                ))
            }
            MessageLikeEventType::RoomMessage => {
                SafareigMessageLikeEventContent::Message(Box::new(
                    RoomMessageEventContent::from_parts(&kind.to_string(), json)?,
                ))
            }
            MessageLikeEventType::RoomRedaction => {
                SafareigMessageLikeEventContent::Redaction(Box::new(
                    RoomRedactionEventContent::from_parts(&kind.to_string(), json)?,
                ))
            }
            MessageLikeEventType::Sticker => SafareigMessageLikeEventContent::Sticker(Box::new(
                StickerEventContent::from_parts(&kind.to_string(), json)?,
            )),
            _ => {
                let value = serde_json::Value::from_str(json.get())?;
                let content = Content::new(value, kind.to_string());

                tracing::debug!(?kind, "not validating content for event type");
                SafareigMessageLikeEventContent::Custom(content)
            }
        };

        Ok(event_content)
    }
}

// Try convert the given payload depending on the event type.
// Since Ruma does not offer a deserialization layer anymore, we've created this new enum which will be used
// to ensure that the give content matches with the given types.
// This will cause some maintainability issues because we will need to keep a mapping of event types  to event contents.
pub enum SafareigStateEventContent {
    PolicyRuleRoom(Box<PolicyRuleRoomEventContent>),
    PolicyRuleServer(Box<PolicyRuleServerEventContent>),
    PolicyRuleUser(Box<PolicyRuleUserEventContent>),
    RoomAliases(Box<RoomAliasesEventContent>),
    RoomAvatar(Box<RoomAvatarEventContent>),
    RoomCanonicalAlias(Box<RoomCanonicalAliasEventContent>),
    RoomCreate(Box<RoomCreateEventContent>),
    RoomEncryption(Box<RoomEncryptionEventContent>),
    RoomGuestAccess(Box<RoomGuestAccessEventContent>),
    RoomHistoryVisibility(Box<RoomHistoryVisibilityEventContent>),
    RoomJoinRules(Box<RoomJoinRulesEventContent>),
    RoomMember(Box<RoomMemberEventContent>),
    RoomName(Box<RoomNameEventContent>),
    RoomPinnedEvents(Box<RoomPinnedEventsEventContent>),
    RoomPowerLevels(Box<RoomPowerLevelsEventContent>),
    RoomServerAcl(Box<RoomServerAclEventContent>),
    RoomThirdPartyInvite(Box<RoomThirdPartyInviteEventContent>),
    RoomTombstone(Box<RoomTombstoneEventContent>),
    RoomTopic(Box<RoomTopicEventContent>),
    SpaceChild(Box<SpaceChildEventContent>),
    SpaceParent(Box<SpaceParentEventContent>),
    Custom(Content),
}

impl SafareigStateEventContent {
    pub fn from_parts(kind: &StateEventType, json: &RawValue) -> Result<Self, serde_json::Error> {
        let event_content = match kind {
            StateEventType::PolicyRuleRoom => SafareigStateEventContent::PolicyRuleRoom(Box::new(
                PolicyRuleRoomEventContent::from_parts(&kind.to_string(), json)?,
            )),
            StateEventType::PolicyRuleServer => {
                SafareigStateEventContent::PolicyRuleServer(Box::new(
                    PolicyRuleServerEventContent::from_parts(&kind.to_string(), json)?,
                ))
            }
            StateEventType::PolicyRuleUser => SafareigStateEventContent::PolicyRuleUser(Box::new(
                PolicyRuleUserEventContent::from_parts(&kind.to_string(), json)?,
            )),
            StateEventType::RoomAliases => SafareigStateEventContent::RoomAliases(Box::new(
                RoomAliasesEventContent::from_parts(&kind.to_string(), json)?,
            )),
            StateEventType::RoomAvatar => SafareigStateEventContent::RoomAvatar(Box::new(
                RoomAvatarEventContent::from_parts(&kind.to_string(), json)?,
            )),
            StateEventType::RoomCanonicalAlias => {
                SafareigStateEventContent::RoomCanonicalAlias(Box::new(
                    RoomCanonicalAliasEventContent::from_parts(&kind.to_string(), json)?,
                ))
            }
            StateEventType::RoomCreate => SafareigStateEventContent::RoomCreate(Box::new(
                RoomCreateEventContent::from_parts(&kind.to_string(), json)?,
            )),
            StateEventType::RoomEncryption => SafareigStateEventContent::RoomEncryption(Box::new(
                RoomEncryptionEventContent::from_parts(&kind.to_string(), json)?,
            )),
            StateEventType::RoomGuestAccess => {
                SafareigStateEventContent::RoomGuestAccess(Box::new(
                    RoomGuestAccessEventContent::from_parts(&kind.to_string(), json)?,
                ))
            }
            StateEventType::RoomHistoryVisibility => {
                SafareigStateEventContent::RoomHistoryVisibility(Box::new(
                    RoomHistoryVisibilityEventContent::from_parts(&kind.to_string(), json)?,
                ))
            }
            StateEventType::RoomJoinRules => SafareigStateEventContent::RoomJoinRules(Box::new(
                RoomJoinRulesEventContent::from_parts(&kind.to_string(), json)?,
            )),
            StateEventType::RoomMember => SafareigStateEventContent::RoomMember(Box::new(
                RoomMemberEventContent::from_parts(&kind.to_string(), json)?,
            )),
            StateEventType::RoomName => SafareigStateEventContent::RoomName(Box::new(
                RoomNameEventContent::from_parts(&kind.to_string(), json)?,
            )),
            StateEventType::RoomPinnedEvents => {
                SafareigStateEventContent::RoomPinnedEvents(Box::new(
                    RoomPinnedEventsEventContent::from_parts(&kind.to_string(), json)?,
                ))
            }
            StateEventType::RoomPowerLevels => {
                SafareigStateEventContent::RoomPowerLevels(Box::new(
                    RoomPowerLevelsEventContent::from_parts(&kind.to_string(), json)?,
                ))
            }
            StateEventType::RoomServerAcl => SafareigStateEventContent::RoomServerAcl(Box::new(
                RoomServerAclEventContent::from_parts(&kind.to_string(), json)?,
            )),
            StateEventType::RoomThirdPartyInvite => {
                SafareigStateEventContent::RoomThirdPartyInvite(Box::new(
                    RoomThirdPartyInviteEventContent::from_parts(&kind.to_string(), json)?,
                ))
            }
            StateEventType::RoomTombstone => SafareigStateEventContent::RoomTombstone(Box::new(
                RoomTombstoneEventContent::from_parts(&kind.to_string(), json)?,
            )),
            StateEventType::RoomTopic => SafareigStateEventContent::RoomTopic(Box::new(
                RoomTopicEventContent::from_parts(&kind.to_string(), json)?,
            )),
            StateEventType::SpaceChild => SafareigStateEventContent::SpaceChild(Box::new(
                SpaceChildEventContent::from_parts(&kind.to_string(), json)?,
            )),
            StateEventType::SpaceParent => SafareigStateEventContent::SpaceParent(Box::new(
                SpaceParentEventContent::from_parts(&kind.to_string(), json)?,
            )),
            _ => {
                let value = serde_json::Value::from_str(json.get())?;
                let content = Content::new(value, kind.to_string());

                tracing::debug!(?kind, "not validating content for event type");
                SafareigStateEventContent::Custom(content)
            }
        };

        Ok(event_content)
    }
}

#[cfg(test)]
mod tests {

    use std::sync::Arc;

    use safareig_core::ruma::{
        events::{
            room::{
                create::RoomCreateEventContent,
                member::{MembershipState, RoomMemberEventContent},
                name::RoomNameEventContent,
            },
            TimelineEventType,
        },
        OwnedServerName, RoomId,
    };
    use safareig_testing::CustomEventContent;

    use crate::{
        client::room::{loader::RoomLoader, state::RoomState},
        core::{
            events::{EventBuilder, EventContentExt},
            room::{EventEncoding, RoomDescriptor},
        },
        storage::{EventStorage, RoomStorage, StateStorage},
        testing::TestingDB,
    };

    #[tokio::test]
    async fn it_do_not_add_prev_content_if_there_was_not_prev_event() {
        let testing = TestingDB::default();
        let (id, room) = testing.public_room().await;
        let content = CustomEventContent {
            key: "a".to_string(),
        };

        let builder =
            EventBuilder::state(id.user(), content.into_content().unwrap(), room.id(), None);

        let (eid, _) = room.add_state(builder).await.unwrap();

        let events = testing.container.service::<Arc<dyn EventStorage>>();

        let event = events.get_event(&eid).await.unwrap().unwrap();
        assert!(event.prev_content.is_none());
    }

    #[tokio::test]
    async fn it_adds_prev_content_if_there_was_a_prev_event() {
        let testing = TestingDB::default();
        let (id, room) = testing.public_room().await;
        let room_name = "new name".parse().unwrap();

        let content = RoomNameEventContent::new(room_name);
        let builder =
            EventBuilder::state(id.user(), content.into_content().unwrap(), room.id(), None);

        let (eid, _) = room.add_state(builder).await.unwrap();

        let events = testing.container.service::<Arc<dyn EventStorage>>();
        let event = events.get_event(&eid).await.unwrap().unwrap();

        let prev_content = event.prev_content.unwrap();
        let prev_content_obj = prev_content.as_object().unwrap();
        assert_eq!(
            "Room name",
            prev_content_obj.get("name").unwrap().as_str().unwrap()
        );
    }

    /// Check that a state snapshot is stored previous to the stored event
    #[tokio::test]
    async fn it_stores_state_previous_to_event() {
        let testing = TestingDB::default();
        let user = testing.register().await;
        let server_name: OwnedServerName = "test.cat".parse().unwrap();
        let room_id = RoomId::new(&server_name);
        let descriptor = RoomDescriptor {
            room_id: room_id.clone(),
            encoding: EventEncoding::Json,
            room_version: testing.default_room_version(),
        };
        let rooms = testing.container.service::<Arc<dyn RoomStorage>>();

        rooms.update_room_descriptor(&descriptor).await.unwrap();

        let room = testing
            .container
            .service::<RoomLoader>()
            .get(&room_id)
            .await
            .unwrap()
            .unwrap();

        let mut content = RoomCreateEventContent::new_v1(user.user().to_owned());
        content.federate = false;
        content.predecessor = None;
        content.room_version = testing.default_room_version();

        let builder = EventBuilder::state(
            user.user(),
            content.into_content().unwrap(),
            room.id(),
            None,
        );
        let (_event_id, _) = room.add_state(builder).await.unwrap();

        let member_content = RoomMemberEventContent {
            avatar_url: None,
            displayname: None,
            is_direct: None,
            membership: MembershipState::Join,
            third_party_invite: None,
            reason: None,
            join_authorized_via_users_server: None,
        };

        let member_event = EventBuilder::state(
            user.user(),
            member_content.into_content().unwrap(),
            room.id(),
            Some(user.user().to_string()),
        );
        let (event_id, _) = room.add_state(member_event).await.unwrap();

        let state = testing.container.service::<Arc<dyn StateStorage>>();
        let state_id_at_create = state.state_at(&room_id, &event_id).await.unwrap().unwrap();
        let state = RoomState::load(state_id_at_create, state.clone());
        let events = state.state_events(None).await.unwrap();
        assert_eq!(1, events.len());
        assert_eq!(events[0].kind, TimelineEventType::RoomCreate);
    }
}
