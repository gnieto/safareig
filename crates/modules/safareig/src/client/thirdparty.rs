use std::collections::BTreeMap;

use safareig_core::{
    auth::Identity, command::Command, ruma::api::client::thirdparty::get_protocols, Inject,
};

#[derive(Inject)]
pub struct ThirdpartyProtocolsCommand;

#[async_trait::async_trait]
impl Command for ThirdpartyProtocolsCommand {
    type Input = get_protocols::v3::Request;
    type Output = get_protocols::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, _: Self::Input, _: Identity) -> Result<Self::Output, Self::Error> {
        Ok(get_protocols::v3::Response::new(BTreeMap::new()))
    }
}
