use reqwest::StatusCode;
use safareig::client::room::command::leave_room::LeaveRoomCommand;
use safareig_core::{
    command::Command,
    ruma::api::client::{error::ErrorKind, membership::leave_room},
};
use safareig_testing::{error::RumaErrorExtension, TestingApp};

#[tokio::test]
async fn user_can_leave_a_room() {
    let db = TestingApp::default().await;
    let (_, room) = db.public_room().await;
    let joiner = db.register().await;
    db.join_room(room.id().to_owned().to_owned(), joiner.clone())
        .await
        .unwrap();

    let cmd = db.command::<LeaveRoomCommand>();
    let input = leave_room::v3::Request {
        room_id: room.id().to_owned(),
        reason: None,
    };
    let result = cmd.execute(input, joiner).await;

    assert!(result.is_ok());
}

#[tokio::test]
async fn invited_user_can_reject_the_invite_with_the_leave_command() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let joiner = db.register().await;

    let _ = db.invite_room(room.id(), &creator, &joiner).await;

    let cmd = db.command::<LeaveRoomCommand>();
    let input = leave_room::v3::Request {
        room_id: room.id().to_owned(),
        reason: None,
    };
    cmd.execute(input, joiner).await.unwrap();
}

#[tokio::test]
async fn an_unknown_user_can_not_leave_a_room() {
    let db = TestingApp::default().await;
    let (_, room) = db.public_room().await;
    let joiner = db.register().await;

    let cmd = db.command::<LeaveRoomCommand>();
    let input = leave_room::v3::Request {
        room_id: room.id().to_owned(),
        reason: None,
    };
    let result = cmd.execute(input, joiner).await;

    assert!(result.is_err());
    let err = result.err().unwrap();
    assert_eq!(err.status_code, StatusCode::FORBIDDEN);
    assert_eq!(err.kind(), ErrorKind::forbidden());
}

#[tokio::test]
async fn user_can_not_leave_an_already_left_room() {
    let db = TestingApp::default().await;
    let (_, room) = db.public_room().await;
    let joiner = db.register().await;
    db.join_room(room.id().to_owned().to_owned(), joiner.clone())
        .await
        .unwrap();

    let cmd = db.command::<LeaveRoomCommand>();
    let input = leave_room::v3::Request {
        room_id: room.id().to_owned(),
        reason: None,
    };
    let result = cmd.execute(input, joiner.clone()).await;

    assert!(result.is_ok());

    let input = leave_room::v3::Request {
        room_id: room.id().to_owned(),
        reason: None,
    };
    let result = cmd.execute(input, joiner).await;

    assert!(result.is_err());
    let err = result.err().unwrap();
    assert_eq!(err.status_code, StatusCode::FORBIDDEN);
    assert_eq!(err.kind(), ErrorKind::forbidden());
}

#[tokio::test]
async fn leave_room_on_invited_room() {
    let db = TestingApp::default().await;
    let (inviter, room) = db.private_room().await;
    let joiner = db.register().await;
    db.invite_room(room.id(), &inviter, &joiner).await.unwrap();

    let cmd = db.command::<LeaveRoomCommand>();
    let input = leave_room::v3::Request {
        room_id: room.id().to_owned(),
        reason: None,
    };
    let result = cmd.execute(input, joiner).await;

    assert!(result.is_ok());
}
