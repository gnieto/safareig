use std::time::Duration;

use reqwest::StatusCode;
use safareig::{
    client::room::{command::create_room::CreateRoomCommand, loader::RoomLoader},
    storage::RoomStorage,
};
use safareig_core::{
    auth::Identity,
    bus::{Bus, BusMessage},
    command::Command,
    config::RoomVersionSelectorConfig,
    pagination::{self},
    ruma::{
        api::{
            client::{
                room::{create_room, Visibility},
                sync::sync_events,
            },
            Direction,
        },
        events::{
            room::{
                member::{MembershipState, RoomMemberEventContent},
                name::RoomNameEventContent,
                topic::RoomTopicEventContent,
            },
            AnyInitialStateEvent, EmptyStateKey, InitialStateEvent, TimelineEventType,
        },
        serde::Raw,
        OwnedRoomId, RoomAliasId, RoomVersionId,
    },
};
use safareig_sync::SyncCommand;
use safareig_testing::TestingApp;
use serde_json::{json, value::to_raw_value};

#[tokio::test]
async fn it_adds_an_invite_event_on_the_room() {
    let db = TestingApp::default().await;
    let creator = db.register().await;
    let invitee = db.register().await;
    let bus_recv = db.service::<Bus>().receiver();
    let sync = db.command::<SyncCommand>();

    // Intitial sync for invitee
    let sync_req = sync_events::v3::Request {
        filter: None,
        since: None,
        full_state: false,
        set_presence: safareig_core::ruma::presence::PresenceState::Offline,
        timeout: None,
    };
    let sync_response = sync.execute(sync_req, invitee.clone()).await.unwrap();
    let next_batch = sync_response.next_batch;

    let room_id = prepare_direct_message(&db, &creator, &invitee).await;

    let events = db.find_event(&room_id, TimelineEventType::RoomMember).await;
    assert_eq!(2, events.len());

    let creator_member = &events[0];
    let content: RoomMemberEventContent =
        serde_json::from_value(creator_member.content.clone()).unwrap();
    assert_eq!(
        creator.user().as_str(),
        creator_member.state_key.as_ref().unwrap()
    );
    assert_eq!(MembershipState::Join, content.membership);

    let invitee_member = &events[1];
    let content: RoomMemberEventContent =
        serde_json::from_value(invitee_member.content.clone()).unwrap();
    assert_eq!(
        invitee.user().as_str(),
        invitee_member.state_key.as_ref().unwrap()
    );
    assert_eq!(MembershipState::Invite, content.membership);

    // Intitial sync for invitee
    let sync_req = sync_events::v3::Request {
        filter: None,
        since: Some(next_batch),
        full_state: false,
        set_presence: safareig_core::ruma::presence::PresenceState::Offline,
        timeout: Some(Duration::from_secs(1)),
    };
    let sync_response = sync.execute(sync_req, invitee.clone()).await.unwrap();
    assert!(sync_response.rooms.invite.get(&room_id).is_some());

    // And check that an invite event has been pushed to the event bus, which means that sync will wake up a sync request
    let expected_message = BusMessage::RoomInvited(
        room_id.clone(),
        creator.user().to_owned(),
        invitee.user().to_owned(),
    );
    let bus_msg = TestingApp::find_bus_message_containing(bus_recv, |m| m == &expected_message);
    assert!(bus_msg.is_some());
}

#[tokio::test]
async fn it_creates_room_with_displayname_and_avatar_if_set() {
    let db = TestingApp::default().await;
    let creator = db
        .register_with_profile(Some("creator".to_string()), None)
        .await;
    let invitee = db
        .register_with_profile(Some("invitee".to_string()), None)
        .await;
    let room_id = prepare_direct_message(&db, &creator, &invitee).await;

    let events = db.find_event(&room_id, TimelineEventType::RoomMember).await;
    assert_eq!(2, events.len());

    let creator_member = &events[0];
    let content: RoomMemberEventContent =
        serde_json::from_value(creator_member.content.clone()).unwrap();
    assert_eq!("creator", content.displayname.unwrap().as_str());

    let invitee_member = &events[1];
    let content: RoomMemberEventContent =
        serde_json::from_value(invitee_member.content.clone()).unwrap();
    assert_eq!("invitee", content.displayname.unwrap().as_str());
}

#[tokio::test]
async fn it_creates_a_space_room() {
    let db = TestingApp::default().await;
    let creator = db
        .register_with_profile(Some("creator".to_string()), None)
        .await;
    let mut input = create_room::v3::Request {
        creation_content: Default::default(),
        initial_state: Default::default(),
        invite: Default::default(),
        invite_3pid: Default::default(),
        is_direct: Default::default(),
        name: Some("room name".parse().unwrap()),
        power_level_content_override: Default::default(),
        preset: Default::default(),
        room_alias_name: Default::default(),
        room_version: None,
        topic: Default::default(),
        visibility: Default::default(),
    };
    let content = serde_json::json!({
        "type": "m.space",
    });
    input.creation_content = Some(Raw::from_json(
        serde_json::value::to_raw_value(&content).unwrap(),
    ));

    let cmd = db.command::<CreateRoomCommand>();
    let response = cmd.execute(input, creator).await.unwrap();

    let loader = db.service::<RoomLoader>();
    let room = loader.get(&response.room_id).await.unwrap().unwrap();
    let events = room
        .topological_events(pagination::Range::all(), 1, Direction::Forward)
        .await
        .unwrap()
        .records();
    let event = &events[0];
    let content = event.content.as_object().unwrap();

    assert_eq!("m.space", content.get("type").unwrap());
}

#[tokio::test]
async fn it_creates_a_room_with_initial_state_overriding_name_and_topic() {
    let db = TestingApp::default().await;
    let creator = db
        .register_with_profile(Some("creator".to_string()), None)
        .await;

    let room_name: &str = "from initial name";
    let room_name_content = RoomNameEventContent::new(room_name.to_string());
    let state_event = InitialStateEvent {
        content: room_name_content,
        state_key: EmptyStateKey,
    };
    let room_name = state_event;

    let room_name_content = RoomTopicEventContent::new("initial topic".to_string());
    let state_event = InitialStateEvent {
        content: room_name_content,
        state_key: EmptyStateKey,
    };
    let room_topic = state_event;
    let cmd = db.command::<CreateRoomCommand>();
    let input = create_room::v3::Request {
        creation_content: None,
        initial_state: vec![
            Raw::new(&room_name).unwrap().cast(),
            Raw::new(&room_topic).unwrap().cast(),
        ],
        invite: vec![],
        invite_3pid: vec![],
        is_direct: true,
        name: None,
        power_level_content_override: None,
        preset: None,
        room_alias_name: None,
        room_version: None,
        topic: None,
        visibility: Visibility::Private,
    };
    let response = cmd.execute(input, creator.clone()).await.unwrap();

    let events = db
        .find_event(&response.room_id, TimelineEventType::RoomName)
        .await;
    assert_eq!(1, events.len());

    let room_name = &events[0];
    let content: RoomNameEventContent = serde_json::from_value(room_name.content.clone()).unwrap();
    assert_eq!("from initial name", content.name.as_str(),);

    let events = db
        .find_event(&response.room_id, TimelineEventType::RoomTopic)
        .await;
    assert_eq!(1, events.len());
    let room_name = &events[0];
    let content: RoomTopicEventContent = serde_json::from_value(room_name.content.clone()).unwrap();
    assert_eq!("initial topic", content.topic.as_str());
    assert_eq!("", events[0].state_key.as_ref().unwrap());
}

#[tokio::test]
async fn it_reject_an_invalid_room_member_initial_state_event() {
    let db = TestingApp::default().await;
    let creator = db
        .register_with_profile(Some("creator".to_string()), None)
        .await;

    let room_member = json!({
        "type": "m.room.member",
        "content": {
            "test": "invalid",
        },
        "state_key": creator.user().to_string(),
    });

    let cmd = db.command::<CreateRoomCommand>();
    let input = create_room::v3::Request {
        creation_content: None,
        initial_state: vec![Raw::<AnyInitialStateEvent>::from_json(
            to_raw_value(&room_member).unwrap(),
        )],
        invite: vec![],
        invite_3pid: vec![],
        is_direct: true,
        name: None,
        power_level_content_override: None,
        preset: None,
        room_alias_name: None,
        room_version: None,
        topic: None,
        visibility: Visibility::Private,
    };
    let response = cmd.execute(input, creator.clone()).await;

    assert!(response.is_err());
    assert_eq!(StatusCode::BAD_REQUEST, response.err().unwrap().status_code);
}

#[tokio::test]
async fn it_can_not_crate_room_with_an_already_used_alias() {
    let db = TestingApp::default().await;
    let creator = db
        .register_with_profile(Some("creator".to_string()), None)
        .await;
    let input = create_room::v3::Request {
        creation_content: Default::default(),
        initial_state: Default::default(),
        invite: Default::default(),
        invite_3pid: Default::default(),
        is_direct: Default::default(),
        name: Some("room name".parse().unwrap()),
        power_level_content_override: Default::default(),
        preset: Default::default(),
        room_alias_name: Some("same_alias".to_string()),
        room_version: None,
        topic: Default::default(),
        visibility: Default::default(),
    };
    let cmd = db.command::<CreateRoomCommand>();
    let response = cmd.execute(input, creator.clone()).await;
    assert!(response.is_ok());

    let input = create_room::v3::Request {
        creation_content: Default::default(),
        initial_state: Default::default(),
        invite: Default::default(),
        invite_3pid: Default::default(),
        is_direct: Default::default(),
        name: Some("room name".parse().unwrap()),
        power_level_content_override: Default::default(),
        preset: Default::default(),
        room_alias_name: Some("same_alias".to_string()),
        room_version: None,
        topic: Default::default(),
        visibility: Default::default(),
    };
    let response = cmd.execute(input, creator).await;

    assert!(response.is_err());
}

#[tokio::test]
async fn create_room_with_alias_should_geneate_canonical_alias_event() {
    let db = TestingApp::default().await;
    let creator = db
        .register_with_profile(Some("creator".to_string()), None)
        .await;
    let input = create_room::v3::Request {
        creation_content: Default::default(),
        initial_state: Default::default(),
        invite: Default::default(),
        invite_3pid: Default::default(),
        is_direct: Default::default(),
        name: Some("room name".parse().unwrap()),
        power_level_content_override: Default::default(),
        preset: Default::default(),
        room_alias_name: Some("alias".to_string()),
        room_version: None,
        topic: Default::default(),
        visibility: Default::default(),
    };
    let cmd = db.command::<CreateRoomCommand>();
    let response = cmd.execute(input, creator.clone()).await.unwrap();

    let loader = db.service::<RoomLoader>();
    let room = loader.get(&response.room_id).await.unwrap().unwrap();
    let events = room.canonical_alias().await.unwrap().content;
    let expected_alias = RoomAliasId::parse("#alias:test.cat").unwrap();
    assert_eq!(events.alias.unwrap(), expected_alias);
}

#[tokio::test]
async fn create_room_with_configured_room_version_for_alias() {
    let mut config = TestingApp::default_config();
    let target_room_version = RoomVersionId::try_from("s.v10").unwrap();
    config.rooms.versions.push(RoomVersionSelectorConfig {
        version: target_room_version.clone(),
        prefix: "safareig-".to_string(),
    });
    let db = TestingApp::with_config(config).await;

    let creator = db
        .register_with_profile(Some("creator".to_string()), None)
        .await;
    let input = create_room::v3::Request {
        creation_content: Default::default(),
        initial_state: Default::default(),
        invite: Default::default(),
        invite_3pid: Default::default(),
        is_direct: Default::default(),
        name: Some("room name".parse().unwrap()),
        power_level_content_override: Default::default(),
        preset: Default::default(),
        room_alias_name: Some("safareig-alias".to_string()),
        room_version: None,
        topic: Default::default(),
        visibility: Default::default(),
    };
    let cmd = db.command::<CreateRoomCommand>();

    let response = cmd.execute(input, creator.clone()).await.unwrap();

    let room_storage = db.service::<std::sync::Arc<dyn RoomStorage>>();
    let descriptor = room_storage
        .room_descriptor(&response.room_id)
        .await
        .unwrap()
        .unwrap();
    assert_eq!(descriptor.room_version, target_room_version);
}

#[tokio::test]
async fn create_v11_room_should_not_add_creator_in_create_room_event() {
    let config = TestingApp::default_config();
    let db = TestingApp::with_config(config).await;

    let creator = db
        .register_with_profile(Some("creator".to_string()), None)
        .await;
    let input = create_room::v3::Request {
        creation_content: Default::default(),
        initial_state: Default::default(),
        invite: Default::default(),
        invite_3pid: Default::default(),
        is_direct: Default::default(),
        name: Some("room name".parse().unwrap()),
        power_level_content_override: Default::default(),
        preset: Default::default(),
        room_alias_name: Some("safareig-alias".to_string()),
        room_version: Some(RoomVersionId::V11),
        topic: Default::default(),
        visibility: Default::default(),
    };
    let cmd = db.command::<CreateRoomCommand>();

    let response = cmd.execute(input, creator.clone()).await.unwrap();

    let room_loader = db.service::<RoomLoader>();
    let room = room_loader.get(&response.room_id).await.unwrap().unwrap();
    let create = room.create_event().await.unwrap().content;
    assert!(create.creator.is_none());
}

async fn prepare_direct_message(
    db: &TestingApp,
    creator: &Identity,
    invitee: &Identity,
) -> OwnedRoomId {
    let cmd = db.command::<CreateRoomCommand>();

    let input = create_room::v3::Request {
        creation_content: None,
        initial_state: vec![],
        invite: vec![invitee.user().to_owned()],
        invite_3pid: vec![],
        is_direct: true,
        name: None,
        power_level_content_override: None,
        preset: None,
        room_alias_name: None,
        room_version: None,
        topic: None,
        visibility: Visibility::Private,
    };

    let response = cmd.execute(input, creator.clone()).await.unwrap();

    response.room_id
}
