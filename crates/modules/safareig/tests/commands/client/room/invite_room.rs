use std::{str::FromStr, sync::Arc, time::Duration};

use reqwest::StatusCode;
use safareig::{
    client::room::{
        command::{
            invite_room::InviteRoomCommand, kick_room::KickRoomCommand,
            leave_room::LeaveRoomCommand,
        },
        state::StateIndex,
    },
    storage::RoomStorage,
};
use safareig_core::{
    bus::{Bus, BusMessage},
    command::Command,
    ruma::{
        api::{
            client::{
                error::ErrorKind,
                membership::{
                    invite_user, invite_user::v3::InvitationRecipient, kick_user, leave_room,
                },
                sync::sync_events,
            },
            federation::membership::create_invite::v2 as remote_invite,
            IncomingRequest, OutgoingResponse,
        },
        events::room::{
            member::{MembershipState, RoomMemberEventContent},
            power_levels::RoomPowerLevelsEventContent,
        },
        exports::http,
        user_id, Int,
    },
};
use safareig_sync::SyncCommand;
use safareig_testing::{error::RumaErrorExtension, path_extractor, TestingApp};
use tracing_test::traced_test;
use wiremock::{
    matchers::{method, path_regex},
    Mock, Respond, ResponseTemplate,
};

#[tokio::test]
async fn a_user_can_invite_another_one() {
    let test = TestingApp::default().await;
    let (creator, room) = test.private_room().await;
    let invitee = TestingApp::new_identity();
    let bus_recv = test.service::<Bus>().receiver();

    let cmd = test.command::<InviteRoomCommand>();
    let input = invite_user::v3::Request {
        room_id: room.id().to_owned(),
        recipient: InvitationRecipient::UserId {
            user_id: invitee.user().to_owned(),
        },
        reason: None,
    };
    let _ = cmd.execute(input, creator.clone()).await.unwrap();

    let state = room.state_at_leaves().await.unwrap();
    let membership = StateIndex::memberhip(invitee.user().to_string());
    let event = room
        .event_content::<RoomMemberEventContent>(&membership, &state)
        .await
        .unwrap()
        .unwrap();
    assert_eq!(MembershipState::Invite, event.content.membership);
    let rooms = test.service::<Arc<dyn RoomStorage>>();
    let memberships = rooms.membership(invitee.user()).await.unwrap();
    assert_eq!(
        MembershipState::Invite,
        memberships.get(room.id()).unwrap().state
    );

    // And check that an invite event has been pushed to the event bus, which means that sync will wake up a sync request
    let expected_message = BusMessage::RoomInvited(
        room.id().to_owned(),
        creator.user().to_owned(),
        invitee.user().to_owned(),
    );
    let bus_msg = TestingApp::find_bus_message_containing(bus_recv, |m| m == &expected_message);
    assert!(bus_msg.is_some());
}

#[tokio::test]
async fn a_user_which_does_not_belong_to_a_room_can_not_invite() {
    let test = TestingApp::default().await;
    let (_, room) = test.private_room().await;
    let inviter = TestingApp::new_identity();
    let invitee = TestingApp::new_identity();

    let cmd = test.command::<InviteRoomCommand>();
    let input = invite_user::v3::Request {
        room_id: room.id().to_owned(),
        recipient: InvitationRecipient::UserId {
            user_id: invitee.user().to_owned(),
        },
        reason: None,
    };
    let response = cmd.execute(input, inviter.clone()).await;

    assert!(response.is_err());
}

#[tokio::test]
async fn a_user_without_invite_power_can_not_invite_to_a_room() {
    let test = TestingApp::default().await;
    let (creator, room) = test.private_room().await;
    let inviter = TestingApp::new_identity();
    let invitee = TestingApp::new_identity();

    let cmd = test.command::<InviteRoomCommand>();
    let input = invite_user::v3::Request {
        room_id: room.id().to_owned(),
        recipient: InvitationRecipient::UserId {
            user_id: inviter.user().to_owned(),
        },
        reason: None,
    };
    cmd.execute(input, creator.clone()).await.unwrap();
    test.join_room(room.id().to_owned(), inviter.clone())
        .await
        .unwrap();

    test.mutate_power_levels(
        &creator,
        room.id(),
        |mut power_level: RoomPowerLevelsEventContent| {
            power_level.invite = Int::new_saturating(50);
            power_level
        },
    )
    .await;

    let cmd = test.command::<InviteRoomCommand>();
    let input = invite_user::v3::Request {
        room_id: room.id().to_owned(),
        recipient: InvitationRecipient::UserId {
            user_id: invitee.user().to_owned(),
        },
        reason: None,
    };
    let response = cmd.execute(input, inviter.clone()).await;

    assert!(response.is_err());
    let matrix_error = response.err().unwrap();
    assert_eq!(matrix_error.status_code, StatusCode::FORBIDDEN);
    assert_eq!(matrix_error.kind(), ErrorKind::forbidden());
}

#[tokio::test]
async fn invite_a_user_on_a_remote_server_fails_if_remote_invite_fails() {
    let (test, mock) = TestingApp::with_mocked_federation().await;
    let (creator, room) = test.private_room().await;

    Mock::given(method("PUT"))
        .and(path_regex("/_matrix/federation/v2/invite/.*/.*"))
        .respond_with(ResponseTemplate::new(StatusCode::INTERNAL_SERVER_ERROR))
        .mount(&mock)
        .await;
    let invitee = user_id!("@invitee:localhost:9999");
    let cmd = test.command::<InviteRoomCommand>();
    let input = invite_user::v3::Request {
        room_id: room.id().to_owned(),
        recipient: InvitationRecipient::UserId {
            user_id: invitee.to_owned(),
        },
        reason: None,
    };
    let response = cmd.execute(input, creator.clone()).await;

    assert!(response.is_err());
    let err = response.err().unwrap();
    assert_eq!(err.status_code, StatusCode::INTERNAL_SERVER_ERROR);
}

#[tokio::test]
async fn invite_a_user_on_a_remote_server_fails_if_remote_invite_is_forbbidden() {
    let (test, mock) = TestingApp::with_mocked_federation().await;
    let (creator, room) = test.private_room().await;

    Mock::given(method("PUT"))
        .and(path_regex("/_matrix/federation/v2/invite/.*/.*"))
        .respond_with(ResponseTemplate::new(StatusCode::FORBIDDEN))
        .mount(&mock)
        .await;
    let invitee = user_id!("@invitee:localhost:9999");
    let cmd = test.command::<InviteRoomCommand>();
    let input = invite_user::v3::Request {
        room_id: room.id().to_owned(),
        recipient: InvitationRecipient::UserId {
            user_id: invitee.to_owned(),
        },
        reason: None,
    };
    let response = cmd.execute(input, creator.clone()).await;

    assert!(response.is_err());
    let err = response.err().unwrap();
    assert_eq!(err.status_code, StatusCode::FORBIDDEN);
}

#[tokio::test]
async fn invite_a_user_on_a_remote_server_succeeds_on_successful_invite() {
    let (test, mock) = TestingApp::with_mocked_federation().await;
    let (creator, room) = test.private_room().await;

    let invitee = user_id!("@invitee:localhost:9999");
    let room_id = room.id().to_owned();

    Mock::given(method("PUT"))
        .and(path_regex("/_matrix/federation/v2/invite/.*/.*"))
        .respond_with(InviteResponder {})
        .mount(&mock)
        .await;
    let cmd = test.command::<InviteRoomCommand>();
    let input = invite_user::v3::Request {
        room_id: room.id().to_owned(),
        recipient: InvitationRecipient::UserId {
            user_id: invitee.to_owned(),
        },
        reason: None,
    };
    cmd.execute(input, creator.clone()).await.unwrap();

    let state = room.state_at_leaves().await.unwrap();
    let membership = StateIndex::memberhip(invitee.to_string());
    let event = room
        .event_content::<RoomMemberEventContent>(&membership, &state)
        .await
        .unwrap()
        .unwrap();
    assert_eq!(MembershipState::Invite, event.content.membership);
    let room_memberships = test
        .service::<Arc<dyn RoomStorage>>()
        .membership(invitee)
        .await
        .unwrap();
    let membership = room_memberships.get(&room_id).unwrap();
    assert_eq!(membership.state, MembershipState::Invite);
}

#[tokio::test]
async fn a_user_can_be_invited_multiple_times() {
    let test = TestingApp::default().await;
    let (creator, room) = test.private_room().await;
    let invitee = TestingApp::new_identity();

    let cmd = test.command::<InviteRoomCommand>();
    let input = invite_user::v3::Request {
        room_id: room.id().to_owned(),
        recipient: InvitationRecipient::UserId {
            user_id: invitee.user().to_owned(),
        },
        reason: None,
    };
    let _ = cmd.execute(input, creator.clone()).await.unwrap();

    let cmd = test.command::<InviteRoomCommand>();
    let input = invite_user::v3::Request {
        room_id: room.id().to_owned(),
        recipient: InvitationRecipient::UserId {
            user_id: invitee.user().to_owned(),
        },
        reason: None,
    };
    let _ = cmd.execute(input, creator.clone()).await.unwrap();
}

#[tokio::test]
async fn a_user_can_be_invited_after_being_kicked() {
    let test = TestingApp::default().await;
    let (creator, room) = test.private_room().await;
    let invitee = TestingApp::new_identity();

    let cmd = test.command::<InviteRoomCommand>();
    let input = invite_user::v3::Request {
        room_id: room.id().to_owned(),
        recipient: InvitationRecipient::UserId {
            user_id: invitee.user().to_owned(),
        },
        reason: None,
    };
    let _ = cmd.execute(input, creator.clone()).await.unwrap();

    let cmd = test.command::<KickRoomCommand>();
    let input = kick_user::v3::Request {
        room_id: room.id().to_owned(),
        user_id: invitee.user().to_owned(),
        reason: None,
    };
    let _ = cmd.execute(input, creator.clone()).await.unwrap();

    let cmd = test.command::<InviteRoomCommand>();
    let input = invite_user::v3::Request {
        room_id: room.id().to_owned(),
        recipient: InvitationRecipient::UserId {
            user_id: invitee.user().to_owned(),
        },
        reason: None,
    };
    let _ = cmd.execute(input, creator.clone()).await.unwrap();
}

#[tokio::test]
#[traced_test]
async fn invite_user_after_decline_should_appear_in_sync() {
    let test = TestingApp::default().await;
    let (creator, room) = test.private_room().await;
    let invitee = TestingApp::new_identity();

    let cmd = test.command::<InviteRoomCommand>();
    let input = invite_user::v3::Request {
        room_id: room.id().to_owned(),
        recipient: InvitationRecipient::UserId {
            user_id: invitee.user().to_owned(),
        },
        reason: None,
    };
    let _ = cmd.execute(input, creator.clone()).await.unwrap();

    let sync = test.command::<SyncCommand>();
    let mut sync_request = sync_events::v3::Request {
        filter: None,
        since: None,
        full_state: false,
        set_presence: safareig_core::ruma::presence::PresenceState::Offline,
        timeout: Some(Duration::from_secs(1)),
    };
    let response = sync
        .execute(sync_request.clone(), invitee.clone())
        .await
        .unwrap();
    assert_eq!(1, response.rooms.invite.len());
    sync_request.since = Some(response.next_batch);

    let cmd = test.command::<LeaveRoomCommand>();
    let input = leave_room::v3::Request {
        room_id: room.id().to_owned(),
        reason: None,
    };
    let _ = cmd.execute(input, invitee.clone()).await.unwrap();
    let response = sync
        .execute(sync_request.clone(), invitee.clone())
        .await
        .unwrap();
    assert_eq!(0, response.rooms.invite.len());
    sync_request.since = Some(response.next_batch);

    let cmd = test.command::<InviteRoomCommand>();
    let input = invite_user::v3::Request {
        room_id: room.id().to_owned(),
        recipient: InvitationRecipient::UserId {
            user_id: invitee.user().to_owned(),
        },
        reason: None,
    };
    let _ = cmd.execute(input, creator.clone()).await.unwrap();

    let response = sync
        .execute(sync_request.clone(), invitee.clone())
        .await
        .unwrap();
    assert_eq!(1, response.rooms.invite.len());
}

struct InviteResponder {}

impl Respond for InviteResponder {
    fn respond(&self, request: &wiremock::Request) -> ResponseTemplate {
        let uri = http::Uri::from_str(request.url.as_str()).unwrap();
        let http = http::Request::builder()
            .method(request.method.clone())
            .uri(uri.clone())
            .body(request.body.clone())
            .unwrap();

        let path =  <safareig_core::ruma::api::federation::membership::create_invite::v2::Request as IncomingRequest>::METADATA
            .history
            .stable_paths()
            .next()
            .unwrap()
            .1;

        let path_parts = path_extractor(path, uri.path().to_string().as_str());
        let request = remote_invite::Request::try_from_http_request(http, &path_parts).unwrap();

        let response = remote_invite::Response::new(request.event);
        let http_response: http::Response<Vec<u8>> = response.try_into_http_response().unwrap();

        ResponseTemplate::new(StatusCode::OK).set_body_bytes(http_response.into_body())
    }
}
