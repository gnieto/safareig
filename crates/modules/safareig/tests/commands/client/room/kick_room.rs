use safareig::client::room::command::kick_room::KickRoomCommand;
use safareig_core::{command::Command, ruma::api::client::membership::kick_user};
use safareig_testing::TestingApp;

#[tokio::test]
async fn unprivileged_user_can_not_kick_user() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let joiner = db.register().await;
    db.join_room(room.id().to_owned().to_owned(), joiner.clone())
        .await
        .unwrap();

    let cmd = db.command::<KickRoomCommand>();
    let input = kick_user::v3::Request {
        room_id: room.id().to_owned(),
        user_id: creator.user().to_owned(),
        reason: None,
    };
    let result = cmd.execute(input, joiner).await;

    assert!(result.is_err());
}

#[tokio::test]
async fn privileged_user_can_kick_people() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let joiner = db.register().await;
    db.join_room(room.id().to_owned().to_owned(), joiner.clone())
        .await
        .unwrap();

    let cmd = db.command::<KickRoomCommand>();

    let input = kick_user::v3::Request {
        room_id: room.id().to_owned(),
        user_id: joiner.user().to_owned(),
        reason: None,
    };
    let result = cmd.execute(input, creator).await;

    assert!(result.is_ok());
}

#[tokio::test]
async fn privileged_user_can_cancel_an_invitation() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let joiner = db.register().await;
    db.invite_room(room.id(), &creator, &joiner).await.unwrap();

    let cmd = db.command::<KickRoomCommand>();

    let input = kick_user::v3::Request {
        room_id: room.id().to_owned(),
        user_id: joiner.user().to_owned(),
        reason: None,
    };
    let result = cmd.execute(input, creator).await;

    assert!(result.is_ok());
}
