use std::{collections::HashSet, str::FromStr};

use itertools::Itertools;
use safareig::{
    client::{room::command::get_messages::GetMessagesCommand, sync::LazyLoadToken},
    storage::TopologicalToken,
};
use safareig_core::{
    auth::Identity,
    command::Command,
    pagination,
    ruma::{
        api::{
            client::{
                filter::{LazyLoadOptions, RoomEventFilter},
                message::get_message_events,
            },
            Direction,
        },
        events::{
            room::message::MessageType, AnyMessageLikeEvent, AnyStateEvent, AnyTimelineEvent,
            MessageLikeEvent,
        },
        serde::Raw,
        RoomId, UInt,
    },
};
use safareig_testing::TestingApp;

#[tokio::test]
async fn it_recovers_some_old_messages_backward() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let member = prepare_room_with_some_messages(&db, room.id(), &creator).await;
    let from_token = pagination::Boundary::Unlimited;

    let response = backward_messages(&db, room.id(), &member, from_token, 15).await;

    assert_eq!(response.chunk.len(), 15);
    assert_eq!("299", message_from_event(&response.chunk[0]).unwrap());
    assert_eq!("285", message_from_event(&response.chunk[14]).unwrap());
}

#[tokio::test]
async fn messages_backward_returns_same_from_and_to_at_the_end_of_the_stream() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let member = prepare_room_with_some_messages(&db, room.id(), &creator).await;
    let token = pagination::Boundary::Exclusive(TopologicalToken::root());

    let response = backward_messages(&db, room.id(), &member, token, 100).await;

    assert_eq!(response.chunk.len(), 0);
    assert_eq!("t0", &response.start);
    // assert_eq!("t0", &response.end.unwrap());
}

#[tokio::test]
async fn it_recovers_some_old_messages_forward() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    // Add two: We get the current depth and we want to skip the next event (join) and start on
    // the following one (from, while iterating forward, is inclusive)
    let token_after_creation = TopologicalToken::new(room.depth_at_leaves().await + 2);
    let member = prepare_room_with_some_messages(&db, room.id(), &creator).await;

    let response = forward_messages(
        &db,
        room.id(),
        &member,
        pagination::Boundary::Exclusive(token_after_creation),
        15,
    )
    .await;

    let start: TopologicalToken = response.start.parse().unwrap();
    assert_eq!(token_after_creation.depth(), start.depth());
    assert!(response.end.unwrap().starts_with("t26"));
    assert_eq!(response.chunk.len(), 15);
    assert_eq!("0", message_from_event(&response.chunk[0]).unwrap());
    assert_eq!("14", message_from_event(&response.chunk[14]).unwrap());
}

#[tokio::test]
async fn it_can_read_all_messages_backwards() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let member = prepare_room_with_some_messages(&db, room.id(), &creator).await;
    let mut from_token = Some(pagination::Boundary::Unlimited);

    let mut msg_set = HashSet::new();

    while let Some(token) = from_token {
        let response = backward_messages(&db, room.id(), &member, token, 15).await;
        from_token = response
            .end
            .map(|end| TopologicalToken::from_str(&end))
            .transpose()
            .unwrap()
            .map(pagination::Boundary::Exclusive);

        for e in response.chunk.into_iter() {
            if let Some(msg) = message_from_event(&e) {
                msg_set.insert(msg);
            }
        }
    }

    assert_eq!(300, msg_set.into_iter().sorted().count());
}

#[tokio::test]
async fn state_events_contains_state_keys() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let _ = prepare_room_with_some_messages(&db, room.id(), &creator).await;
    let token = pagination::Boundary::Exclusive(TopologicalToken::root());

    let messages: Vec<Raw<AnyTimelineEvent>> =
        forward_messages(&db, room.id(), &creator, token, 15)
            .await
            .chunk;

    assert!(matches!(
        messages[0].deserialize().unwrap(),
        AnyTimelineEvent::State(AnyStateEvent::RoomCreate(_))
    ));
    assert!(messages[0].json().get().contains("state_key"));
    assert!(matches!(
        messages[14].deserialize().unwrap(),
        AnyTimelineEvent::MessageLike(AnyMessageLikeEvent::RoomMessage(_))
    ));
    assert!(!messages[14].json().get().contains("state_key"));
}

#[tokio::test]
async fn user_who_has_left_can_not_read_messages_after_they_left() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let member = prepare_room_with_some_messages(&db, room.id(), &creator).await;
    let _ = db
        .leave_room(room.id().to_owned(), member.clone())
        .await
        .unwrap();

    for _ in 0..10 {
        let _ = db.talk(room.id(), &creator, "aaaa").await;
    }

    let from_token = pagination::Boundary::Unlimited;
    let response = backward_messages(&db, room.id(), &member, from_token.clone(), 15).await;

    assert_eq!(response.chunk.len(), 15);
    assert_eq!("299", message_from_event(&response.chunk[0]).unwrap());
    assert_eq!("285", message_from_event(&response.chunk[14]).unwrap());

    let response = forward_messages(&db, room.id(), &member, from_token, 15).await;
    assert_eq!(response.chunk.len(), 0);
}

async fn prepare_room_with_some_messages(
    db: &TestingApp,
    room: &RoomId,
    creator: &Identity,
) -> Identity {
    let new_member = db.register().await;
    db.join_room(room.to_owned(), new_member.to_owned())
        .await
        .unwrap();

    for i in 0..100 {
        db.talk(room, creator, i.to_string().as_str())
            .await
            .unwrap();
    }

    for i in 0..100 {
        db.talk(room, &new_member, (i + 100).to_string().as_str())
            .await
            .unwrap();
    }

    for i in 0..100 {
        db.talk(room, creator, (i + 200).to_string().as_str())
            .await
            .unwrap();
    }

    new_member
}

#[tokio::test]
async fn it_returns_empty_chunk_on_overlapping_window() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let member = prepare_room_with_some_messages(&db, room.id(), &creator).await;
    let from = TopologicalToken::infinite();
    let cmd = db.command::<GetMessagesCommand>();
    let incoming = get_message_events::v3::Request {
        room_id: room.id().to_owned(),
        from: Some(from.to_string()),
        to: Some(from.to_string()),
        dir: Direction::Backward,
        limit: UInt::new_wrapping(15),
        filter: RoomEventFilter::default(),
    };
    let response = cmd.execute(incoming, member.clone()).await.unwrap();

    assert_eq!(response.chunk.len(), 0);
}

#[tokio::test]
async fn it_returns_a_subset_of_messages() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let member = prepare_room_with_some_messages(&db, room.id(), &creator).await;
    let latest_event_id = db.talk(room.id(), &creator, "hi!").await.unwrap();
    let token_after_insert = TopologicalToken::new(room.depth_at_leaves().await);

    let to = TopologicalToken::new(token_after_insert.depth() - 5);
    let cmd = db.command::<GetMessagesCommand>();
    let incoming = get_message_events::v3::Request {
        room_id: room.id().to_owned(),
        from: Some(token_after_insert.to_string()),
        to: Some(to.to_string()),
        dir: Direction::Backward,
        limit: UInt::new_wrapping(15),
        filter: RoomEventFilter::default(),
    };
    let response = cmd.execute(incoming, member.clone()).await.unwrap();

    assert_eq!(response.chunk.len(), 6);
    // /messages when direction is backward, the returned events inclusively from the given token
    let event = response.chunk[0].deserialize().unwrap();
    assert_eq!(event.event_id(), latest_event_id);
}

#[tokio::test]
async fn it_returns_filtered_messages() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let member = prepare_room_with_some_messages(&db, room.id(), &creator).await;
    let token_after_insert = TopologicalToken::new(room.depth_at_leaves().await);

    let room_filter = RoomEventFilter {
        not_types: vec!["m.room.message".to_string()],
        ..Default::default()
    };

    let to = TopologicalToken::new(token_after_insert.depth() - 5);
    let cmd = db.command::<GetMessagesCommand>();
    let incoming = get_message_events::v3::Request {
        room_id: room.id().to_owned(),
        from: Some(token_after_insert.to_string()),
        to: Some(to.to_string()),
        dir: Direction::Backward,
        limit: UInt::new_wrapping(15),
        filter: room_filter,
    };
    let response = cmd.execute(incoming, member.clone()).await.unwrap();

    assert_eq!(response.chunk.len(), 0);
}

#[tokio::test]
async fn it_returns_lazy_load_members() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let second = db.register().await;
    db.join_room(room.id().to_owned(), second.clone())
        .await
        .unwrap();
    let third = db.register().await;
    db.join_room(room.id().to_owned(), third.clone())
        .await
        .unwrap();

    db.talk(room.id(), &creator, "0").await.unwrap();
    db.talk(room.id(), &second, "1").await.unwrap();
    db.talk(room.id(), &creator, "2").await.unwrap();
    db.talk(room.id(), &second, "3").await.unwrap();
    db.talk(room.id(), &creator, "4").await.unwrap();
    db.talk(room.id(), &third, "5").await.unwrap();
    db.talk(room.id(), &third, "6").await.unwrap();
    db.talk(room.id(), &third, "6").await.unwrap();

    let token_after_insert = TopologicalToken::new(room.depth_at_leaves().await)
        .with_lazy_load_token(LazyLoadToken::default());

    let room_filter = RoomEventFilter {
        lazy_load_options: LazyLoadOptions::Enabled {
            include_redundant_members: false,
        },
        ..Default::default()
    };

    let cmd = db.command::<GetMessagesCommand>();
    let incoming = get_message_events::v3::Request {
        room_id: room.id().to_owned(),
        from: Some(token_after_insert.to_string()),
        to: None,
        dir: Direction::Backward,
        limit: UInt::new_wrapping(2),
        filter: room_filter.clone(),
    };
    let response = cmd.execute(incoming, creator.clone()).await.unwrap();

    assert_eq!(response.state.len(), 1);
    let state_event = response.state[0].deserialize().unwrap();
    assert_eq!(third.user(), state_event.sender());

    // Next request should return lazy load members for `creator` ad `second`
    let mut next = response.end.unwrap();
    let incoming = get_message_events::v3::Request {
        room_id: room.id().to_owned(),
        from: Some(next),
        to: None,
        dir: Direction::Backward,
        limit: UInt::new_wrapping(3),
        filter: room_filter.clone(),
    };
    let response = cmd.execute(incoming, creator.clone()).await.unwrap();
    assert_eq!(response.state.len(), 2);
    next = response.end.unwrap();

    // Next request should not return any state event, because the state events has been
    // already sent in previous requests
    let incoming = get_message_events::v3::Request {
        room_id: room.id().to_owned(),
        from: Some(next),
        to: None,
        dir: Direction::Backward,
        limit: UInt::new_wrapping(2),
        filter: room_filter,
    };
    let response = cmd.execute(incoming, creator.clone()).await.unwrap();
    assert_eq!(response.state.len(), 0);
}

async fn backward_messages(
    db: &TestingApp,
    room_id: &RoomId,
    who: &Identity,
    from: pagination::Boundary<TopologicalToken>,
    limit: u16,
) -> get_message_events::v3::Response {
    let cmd = db.command::<GetMessagesCommand>();

    let from = match from {
        pagination::Boundary::Exclusive(f) | pagination::Boundary::Inclusive(f) => {
            Some(f.to_string())
        }
        pagination::Boundary::Unlimited => None,
    };

    let incoming = get_message_events::v3::Request {
        room_id: room_id.to_owned(),
        from,
        to: None,
        dir: Direction::Backward,
        limit: UInt::new_wrapping(limit as u64),
        filter: RoomEventFilter::default(),
    };

    cmd.execute(incoming, who.clone()).await.unwrap()
}

async fn forward_messages(
    db: &TestingApp,
    room_id: &RoomId,
    who: &Identity,
    from: pagination::Boundary<TopologicalToken>,
    limit: u16,
) -> get_message_events::v3::Response {
    let cmd = db.command::<GetMessagesCommand>();

    let from = match from {
        pagination::Boundary::Exclusive(f) | pagination::Boundary::Inclusive(f) => {
            Some(f.to_string())
        }
        pagination::Boundary::Unlimited => None,
    };

    let incoming = get_message_events::v3::Request {
        room_id: room_id.to_owned(),
        from,
        to: None,
        dir: Direction::Forward,
        limit: UInt::new_wrapping(limit as u64),
        filter: RoomEventFilter::default(),
    };

    cmd.execute(incoming, who.clone()).await.unwrap()
}

fn message_from_event(raw_event: &Raw<AnyTimelineEvent>) -> Option<String> {
    let any_room_event = raw_event.deserialize().unwrap();

    match any_room_event {
        AnyTimelineEvent::MessageLike(AnyMessageLikeEvent::RoomMessage(
            MessageLikeEvent::Original(me),
        )) => {
            if let MessageType::Text(tmec) = me.content.msgtype {
                Some(tmec.body)
            } else {
                None
            }
        }
        _ => None,
    }
}
