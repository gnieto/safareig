use std::sync::Arc;

use reqwest::StatusCode;
use safareig::{client::room::command::BanUserCommand, storage::InviteStorage};
use safareig_core::{
    command::Command,
    pagination::Range,
    ruma::api::client::{error::ErrorKind, membership::ban_user},
};
use safareig_testing::{error::RumaErrorExtension, TestingApp};

#[tokio::test]
async fn user_can_be_banned_from_room() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let joiner = db.register().await;
    let cmd = db.command::<BanUserCommand>();

    db.join_room(room.id().to_owned().to_owned(), joiner.clone())
        .await
        .unwrap();

    let input = ban_user::v3::Request {
        room_id: room.id().to_owned(),
        user_id: joiner.user().to_owned(),
        reason: Some("Telling unfunny jokes".to_string()),
    };
    let _ = cmd.execute(input, creator).await.unwrap();

    let talk_response = db.talk(room.id(), &joiner, "unfunny joke").await;
    assert!(talk_response.is_err());
}

#[tokio::test]
async fn an_unknown_user_can_be_banned() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let user_not_joined = db.register().await;

    let cmd = db.command::<BanUserCommand>();
    let input = ban_user::v3::Request {
        room_id: room.id().to_owned(),
        user_id: user_not_joined.user().to_owned(),
        reason: Some("Telling unfunny jokes".to_string()),
    };
    cmd.execute(input, creator).await.unwrap();
}

#[tokio::test]
async fn unprivileged_user_can_not_ban() {
    let db = TestingApp::default().await;
    let (_, room) = db.public_room().await;
    let joiner = db.register().await;
    db.join_room(room.id().to_owned().to_owned(), joiner.clone())
        .await
        .unwrap();

    let unprivileged = db.register().await;
    db.join_room(room.id().to_owned().to_owned(), unprivileged.clone())
        .await
        .unwrap();

    let cmd = db.command::<BanUserCommand>();
    let input = ban_user::v3::Request {
        room_id: room.id().to_owned(),
        user_id: joiner.user().to_owned(),
        reason: Some("Telling unfunny jokes".to_string()),
    };
    let result = cmd.execute(input, unprivileged).await;

    assert!(result.is_err());
    let err = result.err().unwrap();
    assert_eq!(err.status_code, StatusCode::FORBIDDEN);
    assert_eq!(err.kind(), ErrorKind::forbidden());
}

#[tokio::test]
async fn invited_user_can_be_banned_from_room() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let joiner = db.register().await;
    db.invite_room(room.id(), &creator, &joiner).await.unwrap();

    let cmd = db.command::<BanUserCommand>();
    let input = ban_user::v3::Request {
        room_id: room.id().to_owned(),
        user_id: joiner.user().to_owned(),
        reason: Some("Telling unfunny jokes".to_string()),
    };
    let _ = cmd.execute(input, creator).await.unwrap();

    let invite_storage = db.service::<Arc<dyn InviteStorage>>();
    let invites = invite_storage
        .query_invites(joiner.user(), Range::all())
        .await
        .unwrap();

    assert!(invites.records().is_empty());
}
