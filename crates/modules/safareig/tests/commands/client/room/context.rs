use safareig::{
    client::room::{command::context::ContextCommand, RoomStream},
    storage::TopologicalToken,
};
use safareig_core::{
    auth::Identity,
    command::Command,
    pagination,
    ruma::{
        api::{
            client::{context::get_context, filter::RoomEventFilter},
            Direction,
        },
        events::AnyTimelineEvent,
        serde::Raw,
        EventId, RoomId, UInt,
    },
};
use safareig_testing::{assert_not_found, TestingApp};

#[tokio::test]
async fn context_at_first_event() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;

    let messages = room
        .topological_events(pagination::Range::all(), 1, Direction::Forward)
        .await
        .unwrap()
        .records();
    let event_id = messages[0].event_ref.owned_event_id();
    let joiner = prepare_room_with_some_messages(&db, room.id(), &creator).await;

    let response = context_at(&db, room.id(), &event_id, &joiner).await;

    assert_eq!(response.events_before.len(), 0);
    assert_eq!(response.events_after.len(), 10);

    let current_event = event_to_json(&response.event.unwrap());
    let current_event_id = current_event.get("event_id").unwrap().as_str().unwrap();
    assert_eq!(event_id.as_str(), current_event_id);

    // Check that after events do not contain the context event
    let contains_event = contains_event_id(&response.events_after, &event_id);
    assert!(!contains_event);
}

#[tokio::test]
async fn context_at_last_event() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let joiner = prepare_room_with_some_messages(&db, room.id(), &creator).await;

    let room_stream = db.service::<RoomStream>();
    room_stream.current_stream_token().await.unwrap();
    let messages = room
        .topological_events(pagination::Range::all(), 1, Direction::Backward)
        .await
        .unwrap()
        .records();
    let event_id = messages[0].event_ref.owned_event_id();
    let response = context_at(&db, room.id(), &event_id, &joiner).await;

    assert_eq!(response.events_before.len(), 10);
    assert_eq!(response.events_after.len(), 0);
    let current_event = event_to_json(&response.event.unwrap());
    let current_event_id = current_event.get("event_id").unwrap().as_str().unwrap();
    assert_eq!(event_id.as_str(), current_event_id);

    // Check that before events do not contain the context event
    let contains_event = contains_event_id(&response.events_before, &event_id);
    assert!(!contains_event);
}

#[tokio::test]
async fn context_on_an_event_in_the_middle_of_the_stream() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let joiner = prepare_room_with_some_messages(&db, room.id(), &creator).await;

    let token_on_middle = TopologicalToken::new(150);
    let range = (
        pagination::Boundary::Unlimited,
        pagination::Boundary::Inclusive(token_on_middle),
    );
    let messages = room
        .topological_events(range.into(), 1, Direction::Backward)
        .await
        .unwrap()
        .records();
    let event_id = messages[0].event_ref.owned_event_id();

    let response = context_at(&db, room.id(), &event_id, &joiner).await;

    assert_eq!(response.events_before.len(), 10);
    assert_eq!(response.events_after.len(), 10);
    let current_event = event_to_json(&response.event.unwrap());
    let current_event_id = current_event.get("event_id").unwrap().as_str().unwrap();
    assert_eq!(event_id.as_str(), current_event_id);

    // Check that before events do not contain the context event
    let contains_event = contains_event_id(&response.events_before, &event_id);
    assert!(!contains_event);

    // Check that after events do not contain the context event
    let contains_event = contains_event_id(&response.events_after, &event_id);
    assert!(!contains_event);
}

#[tokio::test]
async fn context_on_an_event_in_the_middle_of_the_stream_with_filter() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let joiner = prepare_room_with_some_messages(&db, room.id(), &creator).await;

    let token_on_middle = TopologicalToken::new(150);
    let range = (
        pagination::Boundary::Unlimited,
        pagination::Boundary::Inclusive(token_on_middle),
    );
    let messages = room
        .topological_events(range.into(), 1, Direction::Backward)
        .await
        .unwrap()
        .records();
    let event_id = messages[0].event_ref.owned_event_id();

    let response = context_at_without_m_room_message(&db, room.id(), &event_id, &joiner).await;

    assert_eq!(response.events_before.len(), 0);
    assert_eq!(response.events_after.len(), 0);
    let current_event = event_to_json(&response.event.unwrap());
    let current_event_id = current_event.get("event_id").unwrap().as_str().unwrap();
    assert_eq!(event_id.as_str(), current_event_id);
}

#[tokio::test]
async fn context_on_a_non_existing_event() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let event_id = EventId::new(&db.server_name());

    let cmd = db.command::<ContextCommand>();
    let input = get_context::v3::Request {
        event_id: event_id.to_owned(),
        room_id: room.id().to_owned(),
        limit: UInt::new_wrapping(10),
        filter: RoomEventFilter::default(),
    };

    let response = cmd.execute(input, creator.clone()).await;

    assert_not_found(&response);
}

fn contains_event_id(events: &[Raw<AnyTimelineEvent>], event_id: &EventId) -> bool {
    events
        .iter()
        .map(event_to_json)
        .map(|value| value.get("event_id").unwrap().as_str().unwrap().to_string())
        .any(|eid| eid.as_str() == event_id.as_str())
}

fn event_to_json(event: &Raw<AnyTimelineEvent>) -> serde_json::Value {
    serde_json::from_str::<serde_json::Value>(event.json().get()).unwrap()
}

async fn context_at(
    db: &TestingApp,
    room: &RoomId,
    event_id: &EventId,
    user: &Identity,
) -> get_context::v3::Response {
    let cmd = db.command::<ContextCommand>();
    let input = get_context::v3::Request {
        event_id: event_id.to_owned(),
        room_id: room.to_owned(),
        limit: UInt::new_wrapping(10),
        filter: RoomEventFilter::default(),
    };

    cmd.execute(input, user.to_owned()).await.unwrap()
}

async fn context_at_without_m_room_message(
    db: &TestingApp,
    room: &RoomId,
    event_id: &EventId,
    user: &Identity,
) -> get_context::v3::Response {
    let room_filter = RoomEventFilter {
        not_types: vec!["m.room.message".to_string()],
        ..Default::default()
    };

    let cmd = db.command::<ContextCommand>();
    let input = get_context::v3::Request {
        event_id: event_id.to_owned(),
        room_id: room.to_owned(),
        limit: UInt::new_wrapping(10),
        filter: room_filter,
    };

    cmd.execute(input, user.to_owned()).await.unwrap()
}

async fn prepare_room_with_some_messages(
    db: &TestingApp,
    room: &RoomId,
    creator: &Identity,
) -> Identity {
    let new_member = db.register().await;
    db.join_room(room.to_owned(), new_member.to_owned())
        .await
        .unwrap();

    for i in 0..100 {
        db.talk(room, creator, i.to_string().as_str())
            .await
            .unwrap();
    }

    for i in 0..100 {
        db.talk(room, &new_member, (i + 100).to_string().as_str())
            .await
            .unwrap();
    }

    for i in 0..100 {
        db.talk(room, creator, (i + 200).to_string().as_str())
            .await
            .unwrap();
    }

    new_member
}
