use safareig::client::room::command::{unban::UnbanUserCommand, BanUserCommand};
use safareig_core::{
    command::Command,
    ruma::api::client::membership::{ban_user, unban_user},
};
use safareig_testing::TestingApp;

#[tokio::test]
async fn user_can_be_banned_and_unbanned() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let joiner = db.register().await;
    db.join_room(room.id().to_owned(), joiner.clone())
        .await
        .unwrap();

    let ban = db.command::<BanUserCommand>();
    let input = ban_user::v3::Request {
        room_id: room.id().to_owned(),
        user_id: joiner.user().to_owned(),
        reason: Some("Telling unfunny jokes".to_string()),
    };
    let _ = ban.execute(input, creator.clone()).await.unwrap();

    let talk_response = db.talk(room.id(), &joiner, "unfunny joke").await;
    assert!(talk_response.is_err());

    let unban = db.command::<UnbanUserCommand>();
    let input = unban_user::v3::Request {
        room_id: room.id().to_owned(),
        user_id: joiner.user().to_owned(),
        reason: Some("Jokes are better now".to_string()),
    };
    let _ = unban.execute(input, creator).await.unwrap();

    db.join_room(room.id().to_owned(), joiner.clone())
        .await
        .unwrap();

    let talk_response = db.talk(room.id(), &joiner, "better joke").await;
    assert!(talk_response.is_ok());
}

#[tokio::test]
async fn user_without_power_can_not_unban() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let joiner = db.register().await;
    let user_without_powers = db.register().await;
    db.join_room(room.id().to_owned(), joiner.clone())
        .await
        .unwrap();
    db.join_room(room.id().to_owned(), user_without_powers.clone())
        .await
        .unwrap();

    let ban = db.command::<BanUserCommand>();
    let input = ban_user::v3::Request {
        room_id: room.id().to_owned(),
        user_id: joiner.user().to_owned(),
        reason: Some("Telling unfunny jokes".to_string()),
    };
    let _ = ban.execute(input, creator).await.unwrap();

    let talk_response = db.talk(room.id(), &joiner, "unfunny joke").await;
    assert!(talk_response.is_err());

    let unban = db.command::<UnbanUserCommand>();
    let input = unban_user::v3::Request {
        room_id: room.id().to_owned(),
        user_id: joiner.user().to_owned(),
        reason: Some("Jokes are better now".to_string()),
    };

    let unban_result = unban.execute(input, user_without_powers).await;
    assert!(unban_result.is_err());
}
