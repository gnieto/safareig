use safareig::client::room::{command::send_state_event::SendStateEventCommand, RoomStream};
use safareig_core::{
    auth::Identity,
    command::Command,
    ruma::{
        api::client::{
            alias::{create_alias, delete_alias},
            state::send_state_event,
            sync::sync_events::v3::JoinedRoom,
        },
        events::{
            room::{
                canonical_alias::RoomCanonicalAliasEventContent,
                member::{MembershipState, RoomMemberEventContent},
                name::RoomNameEventContent,
            },
            AnyStateEventContent, TimelineEventType,
        },
        exports::http::StatusCode,
        presence::PresenceState,
        serde::Raw,
        RoomAliasId, RoomId,
    },
};
use safareig_rooms::alias::command::{CreateAliasCommand, RemoveAliasCommand};
use safareig_sync::SyncCommand;
use safareig_sync_api::SyncToken;
use safareig_testing::{event_content_to_raw, TestingApp};

#[tokio::test]
async fn it_stores_a_state_event_if_user_has_permissions() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let cmd = db.command::<SendStateEventCommand>();
    let room_name = "new room name".parse().unwrap();
    let room_name_content = RoomNameEventContent::new(room_name);
    let event = AnyStateEventContent::RoomName(room_name_content);
    let (event_type, body) = event_content_to_raw(event);

    let input = send_state_event::v3::Request {
        body,
        event_type: event_type.to_string().as_str().into(),
        state_key: "".to_string(),
        room_id: room.id().to_owned(),
        timestamp: None,
    };

    let _ = cmd.execute(input, creator).await.unwrap();
    let events = db.find_event(room.id(), TimelineEventType::RoomName).await;

    let event = events.last().unwrap();
    let content: RoomNameEventContent = serde_json::from_value(event.content.clone()).unwrap();
    assert_eq!(2, events.len());
    assert_eq!("new room name", content.name);
}

#[tokio::test]
async fn allows_store_extre_keys_on_state_events() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let cmd = db.command::<SendStateEventCommand>();
    let room_name = "new room name".parse().unwrap();
    let room_name_content = RoomNameEventContent::new(room_name);
    let event = AnyStateEventContent::RoomName(room_name_content);
    let (event_type, body) = event_content_to_raw(event);

    let mut new_body: serde_json::Value = serde_json::from_str(body.json().get()).unwrap();
    new_body.as_object_mut().unwrap().insert(
        "my_custom_field".to_string(),
        serde_json::Value::Number(24322i16.into()),
    );
    let body = Raw::from_json(serde_json::value::to_raw_value(&new_body).unwrap());

    let input = send_state_event::v3::Request {
        body,
        event_type: event_type.to_string().as_str().into(),
        state_key: "".to_string(),
        room_id: room.id().to_owned(),
        timestamp: None,
    };

    let _ = cmd.execute(input, creator).await.unwrap();
    let events = db.find_event(room.id(), TimelineEventType::RoomName).await;

    let event = events.last().unwrap();
    let content = event.content.as_object().unwrap();
    assert_eq!(
        content.get("my_custom_field").unwrap(),
        &serde_json::Value::Number(24322i64.into())
    );
    let content: RoomNameEventContent = serde_json::from_value(event.content.clone()).unwrap();
    assert_eq!(2, events.len());
    assert_eq!("new room name", content.name);
}

#[tokio::test]
async fn it_receives_only_one_on_sync_after_adding_a_state_event() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let cmd = db.command::<SendStateEventCommand>();
    let room_stream = db.service::<RoomStream>();
    let sync_token = room_stream.current_stream_token().await.unwrap();

    let room_member_content = RoomMemberEventContent {
        avatar_url: None,
        displayname: None,
        is_direct: None,
        membership: MembershipState::Join,
        third_party_invite: None,
        reason: None,
        join_authorized_via_users_server: None,
    };
    let event = AnyStateEventContent::RoomMember(room_member_content);
    let (event_type, body) = event_content_to_raw(event);

    let input = send_state_event::v3::Request {
        body,
        event_type: event_type.to_string().as_str().into(),
        state_key: creator.user().to_string(),
        room_id: room.id().to_owned(),
        timestamp: None,
    };

    let _ = cmd.execute(input, creator.clone()).await.unwrap();

    let sync_cmd = db.command::<SyncCommand>();
    let sync_token = SyncToken::new_event(sync_token);
    let sync_input = safareig_core::ruma::api::client::sync::sync_events::v3::Request {
        since: Some(sync_token.to_string()),
        filter: None,
        full_state: false,
        set_presence: PresenceState::Online,
        timeout: None,
    };
    let response = sync_cmd.execute(sync_input, creator).await.unwrap();
    let room_data: &JoinedRoom = response.rooms.join.get(room.id()).unwrap();

    assert_eq!(room_data.timeline.events.len(), 1);
    assert_eq!(room_data.state.events.len(), 0);
}

#[tokio::test]
async fn it_does_not_store_event_if_user_has_not_permission() {
    let db = TestingApp::default().await;
    let (_creator, room) = db.public_room().await;
    let new_user = db.register().await;
    db.join_room(room.id().to_owned(), new_user.clone())
        .await
        .unwrap();
    let cmd = db.command::<SendStateEventCommand>();
    let room_name = "new room name".parse().unwrap();
    let room_name_content = RoomNameEventContent::new(room_name);
    let event = AnyStateEventContent::RoomName(room_name_content);
    let (event_type, body) = event_content_to_raw(event);

    let input = send_state_event::v3::Request {
        body,
        event_type: event_type.to_string().as_str().into(),
        state_key: "".to_string(),
        room_id: room.id().to_owned(),
        timestamp: None,
    };

    let response = cmd.execute(input, new_user).await;

    assert!(response.is_err());
    let events = db.find_event(room.id(), TimelineEventType::RoomName).await;
    assert_eq!(1, events.len());
}

#[tokio::test]
async fn it_does_not_store_event_if_user_does_not_belong_to_room() {
    let db = TestingApp::default().await;
    let (_creator, room) = db.public_room().await;
    let not_joined = db.register().await;
    let cmd = db.command::<SendStateEventCommand>();
    let room_name = "new room name".parse().unwrap();
    let room_name_content = RoomNameEventContent::new(room_name);
    let event = AnyStateEventContent::RoomName(room_name_content);
    let (event_type, body) = event_content_to_raw(event);

    let input = send_state_event::v3::Request {
        body,
        event_type: event_type.to_string().as_str().into(),
        state_key: "".to_string(),
        room_id: room.id().to_owned(),
        timestamp: None,
    };

    let response = cmd.execute(input, not_joined).await;

    assert!(response.is_err());
    let events = db.find_event(room.id(), TimelineEventType::RoomName).await;
    assert_eq!(1, events.len());
}

#[tokio::test]
async fn it_does_not_allow_storing_canonical_alias_pointing_to_another_room() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let (second_creator, second_room) = db.public_room().await;
    let alias = RoomAliasId::parse("#alias:test.cat").unwrap();

    let create_alias_cmd = db.command::<CreateAliasCommand>();
    let input = create_alias::v3::Request {
        room_alias: alias.clone(),
        room_id: room.id().to_owned(),
    };
    create_alias_cmd.execute(input, creator).await.unwrap();

    let cmd = db.command::<SendStateEventCommand>();
    let event = AnyStateEventContent::RoomCanonicalAlias(RoomCanonicalAliasEventContent {
        alias: Some(alias),
        alt_aliases: vec![],
    });
    let (event_type, body) = event_content_to_raw(event);

    let input = send_state_event::v3::Request {
        body,
        event_type: event_type.to_string().as_str().into(),
        state_key: "".to_string(),
        room_id: second_room.id().to_owned(),
        timestamp: None,
    };

    let response = cmd.execute(input, second_creator).await;

    assert!(response.is_err());
    let err = response.err().unwrap();
    assert_eq!(err.status_code, StatusCode::BAD_REQUEST);
}

#[tokio::test]
async fn it_does_not_validate_previously_added_aliases() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let alias = RoomAliasId::parse("#alias:test.cat").unwrap();
    let alias2 = RoomAliasId::parse("#alias2:test.cat").unwrap();
    add_alias(&db, &creator, &alias, room.id()).await;
    let canonical_alias = RoomCanonicalAliasEventContent {
        alias: Some(alias.clone()),
        alt_aliases: vec![],
    };
    update_canonical_alias(&db, &creator, room.id(), canonical_alias)
        .await
        .unwrap();
    delete_alias(&db, &creator, &alias).await;
    add_alias(&db, &creator, &alias2, room.id()).await;
    let canonical_alias = RoomCanonicalAliasEventContent {
        alias: Some(alias),
        alt_aliases: vec![alias2],
    };
    // This should not validate "alias". If it would be verified, the insertion of this event
    // would fail, as it does not exist anymore.
    let response = update_canonical_alias(&db, &creator, room.id(), canonical_alias).await;

    assert!(response.is_ok());
}

#[tokio::test]
async fn it_does_not_allow_storing_canonical_alias_with_non_valid_alias() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;

    let cmd = db.command::<SendStateEventCommand>();

    let event_type = TimelineEventType::RoomCanonicalAlias;
    let body = serde_json::json!({
        "alias": "invalid",
        "alt_alias": []
    });
    let body = Raw::new(&body).unwrap().cast();
    let input = send_state_event::v3::Request {
        body,
        event_type: event_type.to_string().as_str().into(),
        state_key: "".to_string(),
        room_id: room.id().to_owned(),
        timestamp: None,
    };

    let response = cmd.execute(input, creator).await;

    assert!(response.is_err());
    let err = response.err().unwrap();
    assert_eq!(err.status_code, StatusCode::BAD_REQUEST);
}

async fn add_alias(app: &TestingApp, id: &Identity, alias: &RoomAliasId, room_id: &RoomId) {
    let create_alias_cmd = app.command::<CreateAliasCommand>();
    let input = create_alias::v3::Request {
        room_alias: alias.to_owned(),
        room_id: room_id.to_owned(),
    };
    create_alias_cmd
        .execute(input, id.to_owned())
        .await
        .unwrap();
}

async fn delete_alias(app: &TestingApp, id: &Identity, alias: &RoomAliasId) {
    let cmd = app.command::<RemoveAliasCommand>();
    let input = delete_alias::v3::Request {
        room_alias: alias.to_owned(),
    };
    cmd.execute(input, id.to_owned()).await.unwrap();
}

async fn update_canonical_alias(
    app: &TestingApp,
    id: &Identity,
    room_id: &RoomId,
    content: RoomCanonicalAliasEventContent,
) -> Result<send_state_event::v3::Response, safareig_core::ruma::api::client::Error> {
    let cmd = app.command::<SendStateEventCommand>();
    let event = AnyStateEventContent::RoomCanonicalAlias(content);
    let (event_type, body) = event_content_to_raw(event);

    let input = send_state_event::v3::Request {
        body,
        event_type: event_type.to_string().as_str().into(),
        state_key: "".to_string(),
        room_id: room_id.to_owned(),
        timestamp: None,
    };

    cmd.execute(input, id.to_owned()).await
}
