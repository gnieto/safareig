use safareig::client::room::state::StateIndex;
use safareig_core::ruma::events::room::member::{MembershipState, RoomMemberEventContent};
use safareig_testing::TestingApp;

#[tokio::test]
async fn a_user_can_join_to_a_public_room() {
    let test = TestingApp::default().await;
    let room = test.public_room().await.1;
    let new_user = TestingApp::new_identity();

    let response = test.join_room(room.id().to_owned(), new_user.clone()).await;

    let state = room.state_at_leaves().await.unwrap();
    let membership = StateIndex::memberhip(new_user.user().to_string());
    let event = room
        .event_content::<RoomMemberEventContent>(&membership, &state)
        .await
        .unwrap()
        .unwrap();
    assert!(response.is_ok());
    assert_eq!(response.unwrap().room_id, room.id());
    assert_eq!(MembershipState::Join, event.content.membership);
}

#[tokio::test]
async fn a_user_can_not_join_a_private_room() {
    let test = TestingApp::default().await;
    let (_, room) = test.private_room().await;
    let new_user = TestingApp::new_identity();

    let response = test.join_room(room.id().to_owned(), new_user.clone()).await;

    assert!(response.is_err());
}
