use std::{collections::BTreeMap, sync::Arc};

use safareig::{
    client::room::{Room, RoomStream},
    core::{
        events::{formatter::EventFormatterV2, Event, EventBuilder, EventContentExt},
        room::EventFormat,
    },
    federation::room::command::{send::SendCommand, state::StateCommand},
    storage::{EventStorage, TopologicalToken},
};
use safareig_core::{
    auth::Identity,
    command::Command,
    pagination::{self, Range},
    ruma::{
        api::{
            federation::{
                event::{get_missing_events, get_room_state},
                transactions::{
                    edu::{Edu, ReceiptContent, ReceiptData, ReceiptMap},
                    send_transaction_message::v1 as send_message,
                },
            },
            Direction,
        },
        events::{
            pdu::Pdu,
            receipt::{Receipt, ReceiptThread},
            room::{
                member::{MembershipState, RoomMemberEventContent},
                message::RoomMessageEventContent,
            },
        },
        serde::Raw,
        server_name,
        signatures::Ed25519KeyPair,
        user_id, DeviceId, MilliSecondsSinceUnixEpoch, OwnedEventId, ServerName, TransactionId,
        UserId,
    },
};
use safareig_ephemeral::services::EphemeralEvent;
use safareig_federation::{keys::KeyProvider, server::storage::FederationStorage};
use safareig_receipts::module::ReceiptsModule;
use safareig_testing::{federation::response_template_from, generate_event_id, TestingApp};
use safareig_users::profile::ProfileLoader;
use serde_json::value::RawValue;
use tracing_test::traced_test;
use wiremock::{
    matchers::{method, path},
    Mock, ResponseTemplate,
};

#[tokio::test]
async fn accepts_incoming_events() {
    let db = TestingApp::default().await;
    let (_, room) = db.public_room().await;
    let remote = ServerName::parse("localhost:8888").unwrap();
    let key = db.generate_remote_key_for(&remote).await;

    let remote_join = user_id!("@usuari:localhost:8888");
    let device = DeviceId::new();
    let id = Identity::Device(device, remote_join.to_owned());
    let _ = db.join_room(room.id().to_owned(), id).await.unwrap();
    let (eid1, pdu1) = generate_event(&key, remote_join, &room, &db).await;

    let cmd = db.command::<SendCommand>();
    let input = send_message::Request {
        transaction_id: TransactionId::new(),
        origin: remote.clone(),
        origin_server_ts: MilliSecondsSinceUnixEpoch::now(),
        pdus: vec![pdu1.into_json()],
        edus: vec![],
    };
    let results: BTreeMap<OwnedEventId, Result<(), String>> = cmd
        .execute(input, TestingApp::federation_identity())
        .await
        .unwrap()
        .pdus;

    assert!(results.get(&eid1).unwrap().is_ok());
}

#[tokio::test]
async fn incoming_event_with_missing_previous_events_should_retrieve_missing_events() {
    let (test, mock) = TestingApp::with_mocked_federation().await;
    let remote = ServerName::parse(mock.address().to_string()).unwrap();
    let key = test.generate_remote_key_for(&remote).await;

    let (_, room) = test.public_room().await;
    let remote_joiner = UserId::new(&remote);
    let device = DeviceId::new();
    let id = Identity::Device(device, remote_joiner.to_owned());
    let _ = test.join_room(room.id().to_owned(), id).await.unwrap();

    // TODO: Hack - Adding the server to the tracked server so it tries to fetch missing events
    // from the mocked server
    let federation_storage = test.service::<Arc<dyn FederationStorage>>();
    federation_storage
        .track_server(room.id(), &remote)
        .await
        .unwrap();

    let latest_event_ids: Vec<_> = room
        .forward_extremities()
        .await
        .unwrap()
        .into_iter()
        .map(|l| l.event_id)
        .collect();

    let missing_content = RoomMessageEventContent::text_plain("missing event content");
    let mut missing_event = EventBuilder::builder(
        &remote_joiner,
        missing_content.into_content().unwrap(),
        room.id(),
    )
    .prev_events(latest_event_ids);
    missing_event = set_auth_events(&test, &room, missing_event).await;
    let (missing_event, pdu) = build_and_sign(&test, &key, &room, missing_event);

    Mock::given(method("POST"))
        .and(path(format!(
            "/_matrix/federation/v1/get_missing_events/{}",
            room.id()
        )))
        .respond_with(missing_event_response(&[pdu]))
        .mount(&mock)
        .await;

    let next_event_content = RoomMessageEventContent::text_plain("next event content");
    let mut next_event = EventBuilder::builder(
        &remote_joiner,
        next_event_content.into_content().unwrap(),
        room.id(),
    )
    .prev_events(vec![missing_event.event_ref.clone()]);
    next_event = set_auth_events(&test, &room, next_event).await;
    let (next_event, next_pdu) = build_and_sign(&test, &key, &room, next_event);

    let cmd = test.command::<SendCommand>();
    let input = send_message::Request {
        transaction_id: TransactionId::new(),
        origin: remote.to_owned(),
        origin_server_ts: MilliSecondsSinceUnixEpoch::now(),
        pdus: vec![next_pdu.into_json()],
        edus: vec![],
    };

    let results: BTreeMap<OwnedEventId, Result<(), String>> = cmd
        .execute(input, TestingApp::federation_identity())
        .await
        .unwrap()
        .pdus;

    assert!(results
        .get(next_event.event_ref.event_id())
        .unwrap()
        .is_ok());

    let stream = test.service::<RoomStream>();
    let events = stream
        .stream_events(Range::all(), None)
        .await
        .unwrap()
        .records();

    // Check that latest sent message was added to the event stream
    let latest = &events.last().unwrap().1;
    assert_eq!(next_event.event_ref.event_id(), latest.event_ref.event_id());

    // Check that missing event is in the event stream before the missing event
    let latest_before = &events.get(events.len() - 2).unwrap().1;
    assert_eq!(
        missing_event.event_ref.event_id(),
        latest_before.event_ref.event_id()
    );
}

#[tokio::test]
#[traced_test]
async fn incoming_event_with_incomplete_missing_events() {
    let (test, mock) = TestingApp::with_mocked_federation().await;
    let remote = ServerName::parse(mock.address().to_string()).unwrap();
    let key = test.generate_remote_key_for(&remote).await;

    let (_, room): (_, Room) = test.public_room().await;
    let remote_joiner = UserId::new(&remote);
    let device = DeviceId::new();
    let id = Identity::Device(device, remote_joiner.to_owned());
    let pdu = join_event(&test, &key, &room, &remote_joiner).await;
    let join_event = room
        .room_version()
        .event_format()
        .pdu_to_event(&pdu)
        .await
        .unwrap();
    room.insert_event(&join_event, room.version(), true, true)
        .await
        .unwrap();

    // Add a new regular event to the room so we can easily calculate the response for /state. The latest
    // event in the room is the join event. If we return the state at that event, we will return the state *before*
    // applying the join event. This will lead to events rejected because of mismatching auth events, because it will miss
    // the m.room.member.
    test.talk(room.id(), &id, "hi!").await.unwrap();

    // TODO: Hack - Adding the server to the tracked server so it tries to fetch missing events
    // from the mocked server
    let federation_storage = test.service::<Arc<dyn FederationStorage>>();
    federation_storage
        .track_server(room.id(), &remote)
        .await
        .unwrap();

    let chain = chain_of_events(10, &test, &key, &remote_joiner, &room).await;
    let partial_missing_events: Vec<Raw<Pdu>> = chain[6..9].iter().map(|t| t.1.clone()).collect();

    Mock::given(method("POST"))
        .and(path(format!(
            "/_matrix/federation/v1/get_missing_events/{}",
            room.id()
        )))
        .respond_with(missing_event_response(&partial_missing_events))
        .mount(&mock)
        .await;

    let state = calculate_state_at_forward_extermities(&test, &room).await;
    Mock::given(method("GET"))
        .and(path(format!("/_matrix/federation/v1/state/{}", room.id())))
        .respond_with(response_template_from(state))
        .mount(&mock)
        .await;

    let cmd = test.command::<SendCommand>();
    let input = send_message::Request {
        transaction_id: TransactionId::new(),
        origin: remote.to_owned(),
        origin_server_ts: MilliSecondsSinceUnixEpoch::now(),
        pdus: vec![chain.last().unwrap().1.clone().into_json()],
        edus: vec![],
    };

    let results: BTreeMap<OwnedEventId, Result<(), String>> = cmd
        .execute(input, TestingApp::federation_identity())
        .await
        .unwrap()
        .pdus;

    let sent_event_id = chain.last().unwrap().0.event_ref.owned_event_id();
    assert!(results.get(&sent_event_id).unwrap().is_ok());
}

#[tokio::test]
async fn accepts_incoming_receipts() {
    let db = TestingApp::with_modules(vec![Box::<ReceiptsModule>::default()]).await;
    let (_creator, room) = db.public_room().await;
    let remote = server_name!("localhost:8888");
    let remote_join = user_id!("@usuari:localhost:8888");
    let event_id = generate_event_id();
    let mut receipts = BTreeMap::new();
    let mut read_map = BTreeMap::new();
    let receipt = Receipt {
        ts: Some(MilliSecondsSinceUnixEpoch::now()),
        thread: ReceiptThread::Unthreaded,
    };
    read_map.insert(
        remote_join.to_owned(),
        ReceiptData::new(receipt, vec![event_id.to_owned()]),
    );
    let receipt_map = ReceiptMap { read: read_map };
    receipts.insert(room.id().to_owned(), receipt_map);

    let read_markers = Edu::Receipt(ReceiptContent { receipts });

    let cmd = db.command::<SendCommand>();
    let input = send_message::Request {
        transaction_id: TransactionId::new(),
        origin: remote.to_owned(),
        origin_server_ts: MilliSecondsSinceUnixEpoch::now(),
        pdus: vec![],
        edus: vec![Raw::new(&read_markers).unwrap()],
    };

    cmd.execute(input, TestingApp::federation_identity())
        .await
        .unwrap();
    let first_event = db.latest_ephemeral().unwrap();
    match first_event {
        EphemeralEvent::Marker(room_id, user, event, _) => {
            assert_eq!(&room_id, room.id());
            assert_eq!(user, remote_join);
            assert_eq!(event, event_id);
        }
        _ => panic!("expected a read marker"),
    }
}

#[tokio::test]
async fn reject_incoming_events_if_they_are_malformed() {
    let (db, _mocked_server) = TestingApp::with_mocked_federation().await;
    let (local_user, room) = db.public_room().await;

    let local_key_provider = db.service::<Arc<KeyProvider>>();
    let key = local_key_provider.current_key().await.unwrap();
    let remote_join = user_id!("@usuari:localhost:8888");
    let device = DeviceId::new();
    let id = Identity::Device(device, remote_join.to_owned());
    let _ = db.join_room(room.id().to_owned(), id).await.unwrap();

    let (_, pdu2) = generate_malformed_event(key.as_ref(), remote_join, &room).await;
    let (_, pdu_ok) = generate_event(key.as_ref(), local_user.user(), &room, &db).await;

    let cmd = db.command::<SendCommand>();

    let input = send_message::Request {
        transaction_id: TransactionId::new(),
        origin: db.server_name(),
        origin_server_ts: MilliSecondsSinceUnixEpoch::now(),
        pdus: vec![pdu2.into_json(), pdu_ok.into_json()],
        edus: vec![],
    };
    let result = cmd
        .execute(input, TestingApp::federation_identity())
        .await
        .unwrap();

    assert_eq!(1, result.pdus.len());
}

#[tokio::test]
async fn rejects_soft_failed_event() {
    let db = TestingApp::default().await;
    let (_, room) = db.public_room().await;
    let joined_user = db.register().await;
    db.join_room(room.id().to_owned().to_owned(), joined_user.to_owned())
        .await
        .unwrap();
    db.talk(room.id(), &joined_user, "Leaving!").await.unwrap();
    let remote = ServerName::parse("localhost:8888").unwrap();
    let key = db.generate_remote_key_for(&remote).await;

    let remote_join = user_id!("@usuari:localhost:8888");
    let device = DeviceId::new();
    let id = Identity::Device(device, remote_join.to_owned());
    let _ = db.join_room(room.id().to_owned(), id).await.unwrap();

    // This event is acceptable at this point, but we will leave before sending it. This means
    // that event is soft-failed: Correct in the topological order, but incorrect on the
    // state at forward extremities (or leaves).
    let (eid1, pdu1) = generate_event(&key, remote_join, &room, &db).await;
    let profile_loader = db.service::<ProfileLoader>();
    room.leave_room(remote_join, &profile_loader).await.unwrap();
    let room_stream = db.service::<RoomStream>();
    let token_before_send = room_stream.current_stream_token().await.unwrap();

    let cmd = db.command::<SendCommand>();
    let input = send_message::Request {
        transaction_id: TransactionId::new(),
        origin: remote.clone(),
        origin_server_ts: MilliSecondsSinceUnixEpoch::now(),
        pdus: vec![pdu1.into_json()],
        edus: vec![],
    };
    let results: BTreeMap<OwnedEventId, Result<(), String>> = cmd
        .execute(input, TestingApp::federation_identity())
        .await
        .unwrap()
        .pdus;

    assert!(results.get(&eid1).unwrap().is_ok());
    let token_after_send = room_stream.current_stream_token().await.unwrap();
    assert_eq!(token_after_send, token_before_send);
}

async fn generate_event(
    key: &Ed25519KeyPair,
    id: &UserId,
    room: &Room,
    db: &TestingApp,
) -> (OwnedEventId, Raw<Pdu>) {
    let content = RoomMessageEventContent::text_plain("hi!");
    let state = room.state_at_leaves().await.unwrap();
    let depth = room.depth_at_leaves().await;
    let topo_start = TopologicalToken::new(depth);

    let range = (
        pagination::Boundary::Inclusive(topo_start),
        pagination::Boundary::Unlimited,
    );

    let last_event = room
        .topological_events(range.into(), 1, Direction::Forward)
        .await
        .unwrap()
        .records()
        .first()
        .cloned()
        .unwrap();

    let events = db.service::<Arc<dyn EventStorage>>();
    let auth_events = state.auth_events(id, None, events.as_ref()).await.unwrap();
    let auth_events_ids = auth_events.into_iter().map(|e| e.event_ref).collect();
    let version = room.room_version();

    let event = EventBuilder::builder(id, content.into_content().unwrap(), room.id())
        .prev_events(vec![last_event.event_ref])
        .auth_events(auth_events_ids)
        .build(version.event_id_calculator())
        .unwrap();
    let eid = event.event_ref.owned_event_id();
    let key_provider = db.service::<Arc<KeyProvider>>();
    let event_format = EventFormatterV2::new(key_provider, id.server_name().to_owned());
    let pdu = event_format.event_to_pdu(&event, key).unwrap();

    (eid, pdu)
}

async fn join_event(
    test: &TestingApp,
    key: &Ed25519KeyPair,
    room: &Room,
    user: &UserId,
) -> Raw<Pdu> {
    let latest_event_ids: Vec<_> = room
        .forward_extremities()
        .await
        .unwrap()
        .into_iter()
        .map(|l| l.event_id)
        .collect();

    let content = RoomMemberEventContent::new(MembershipState::Join);
    let mut event = EventBuilder::state(
        user,
        content.into_content().unwrap(),
        room.id(),
        Some(user.to_string()),
    )
    .prev_events(latest_event_ids);
    event = set_auth_events(test, room, event).await;

    build_and_sign(test, key, room, event).1
}

async fn set_auth_events(test: &TestingApp, room: &Room, builder: EventBuilder) -> EventBuilder {
    let state = room.state_at_leaves().await.unwrap();
    let events = test.service::<Arc<dyn EventStorage>>();
    let auth_events = state
        .auth_events(&builder.sender, None, events.as_ref())
        .await
        .unwrap();
    let auth_events_ids = auth_events.into_iter().map(|e| e.event_ref).collect();

    builder.auth_events(auth_events_ids)
}

fn build_and_sign(
    test: &TestingApp,
    key: &Ed25519KeyPair,
    room: &Room,
    builder: EventBuilder,
) -> (Event, Raw<Pdu>) {
    let version = room.room_version();
    let sender_server = builder.sender.server_name().to_owned();
    let event = builder.build(version.event_id_calculator()).unwrap();

    let key_provider = test.service::<Arc<KeyProvider>>();
    let event_format = EventFormatterV2::new(key_provider, sender_server);
    let pdu = event_format.event_to_pdu(&event, key).unwrap();

    (event, pdu)
}

fn missing_event_response(pdu: &[Raw<Pdu>]) -> ResponseTemplate {
    let response = get_missing_events::v1::Response {
        events: pdu.iter().map(|p| p.clone().into_json()).collect(),
    };

    response_template_from(response)
}

async fn calculate_state_at_forward_extermities(
    test: &TestingApp,
    room: &Room,
) -> get_room_state::v1::Response {
    let forward_extremities = room.forward_extremities().await.unwrap();
    let forward_extremity = forward_extremities.first().unwrap();

    let command = test.command::<StateCommand>();
    let request = get_room_state::v1::Request {
        room_id: room.id().to_owned(),
        event_id: forward_extremity.event_id.owned_event_id(),
    };

    command
        .execute(request, TestingApp::federation_identity())
        .await
        .unwrap()
}

async fn chain_of_events(
    events: u32,
    test: &TestingApp,
    key: &Ed25519KeyPair,
    remote_joiner: &UserId,
    room: &Room,
) -> Vec<(Event, Raw<Pdu>)> {
    let mut chain = Vec::new();
    let mut latest_event_ids: Vec<_> = room
        .forward_extremities()
        .await
        .unwrap()
        .into_iter()
        .map(|l| l.event_id)
        .collect();

    for i in 0..events {
        let content = RoomMessageEventContent::text_plain(format!("msg {i}"));
        let mut event =
            EventBuilder::builder(remote_joiner, content.into_content().unwrap(), room.id())
                .prev_events(latest_event_ids);
        event = set_auth_events(test, room, event).await;
        let (event, pdu) = build_and_sign(test, key, room, event);

        latest_event_ids = vec![event.event_ref.clone()];
        chain.push((event, pdu));
    }

    chain
}

async fn generate_malformed_event(
    key: &Ed25519KeyPair,
    id: &UserId,
    room: &Room,
) -> (OwnedEventId, Raw<Pdu>) {
    let content = RoomMessageEventContent::text_plain("hi!");
    let version = room.room_version();

    let event = EventBuilder::builder(id, content.into_content().unwrap(), room.id())
        .build(version.event_id_calculator())
        .unwrap();
    let eid = event.event_ref.owned_event_id();

    let raw_event = version.event_format().event_to_pdu(&event, key).unwrap();

    // Add an `extra_key` key to force an invalid hash and signature
    let mut json: serde_json::Value = serde_json::from_str(raw_event.json().get()).unwrap();
    let _ = json
        .as_object_mut()
        .unwrap()
        .insert("extra_key".to_string(), serde_json::Value::Null);

    let raw = RawValue::from_string(serde_json::to_string(&json).unwrap()).unwrap();

    (eid, Raw::<Pdu>::from_json(raw))
}
