use std::collections::HashSet;

use safareig::{
    client::room::RoomStream,
    core::filter::Filter,
    storage::{StreamFilter, StreamSource},
};
use safareig_core::{
    pagination::{Boundary, Direction, Limit, Pagination},
    ruma::api::client::filter::FilterDefinition,
    StreamToken,
};
use safareig_testing::TestingApp;

/*
#[tokio::test]
async fn sync_with_event_full_strategy_should_return_limited_when_filtering_is_used() {
    let test = TestingApp::default().await;
    let (creator, room) = test.public_room().await;
    let filtered_user_id = test.register().await;
    test.join_room(room.id().to_owned(), filtered_user_id.clone()).await;

    // Stream contains messages from creator interleaved with "id"
    for i in 0..5 {
        test.talk(room.id(), &creator, &i.to_string()).await;
    }
    test.talk(room.id(), &filtered_user_id, "a").await;
    for i in 0..5 {
        test.talk(room.id(), &creator, &i.to_string()).await;
    }
    test.talk(room.id(), &filtered_user_id, "b").await;
    for i in 0..5 {
        test.talk(room.id(), &creator, &i.to_string()).await;
    }

    // Prepare a room filter which contains only messages from `filtered_user_id` and limit 1.
    // We should find a single event and timeline should be marked as limited

}
*/
#[tokio::test]
async fn room_stream_should_return_the_desired_amount_of_events_when_using_filter() {
    let test = TestingApp::default().await;
    let (creator, room) = test.public_room().await;
    let filtered_user_id = test.register().await;
    test.join_room(room.id().to_owned(), filtered_user_id.clone())
        .await
        .unwrap();

    // Stream contains messages from creator interleaved with "id"
    for i in 0..5 {
        test.talk(room.id(), &creator, &i.to_string())
            .await
            .unwrap();
    }
    test.talk(room.id(), &filtered_user_id, "a").await.unwrap();
    for i in 0..5 {
        test.talk(room.id(), &creator, &i.to_string())
            .await
            .unwrap();
    }
    test.talk(room.id(), &filtered_user_id, "b").await.unwrap();
    for i in 0..5 {
        test.talk(room.id(), &creator, &i.to_string())
            .await
            .unwrap();
    }

    let room_stream = test.service::<RoomStream>();
    let mut definition = FilterDefinition::default();
    definition.room.timeline.senders = Some(vec![filtered_user_id.user().to_owned()]);
    let ignored = HashSet::new();
    let filter = Filter::new_single_room(&definition, room.id(), &ignored);

    let stream_filter = StreamFilter {
        filter: Some(&filter),
        room_id: Some(room.id()),
        source: StreamSource::Timeline,
    };
    let range = (
        Boundary::Inclusive(StreamToken::from(3)),
        Boundary::Unlimited,
    );
    let pagination = Pagination::new(range.into(), Some(stream_filter))
        .with_limit(Limit::from(1))
        .with_direction(Direction::Backward);

    let result = room_stream.stream_room_events(pagination).await.unwrap();

    let next = result.next().cloned().unwrap();
    let prev = result.previous().cloned().unwrap();
    assert_eq!(23, *next);
    assert_eq!(28, *prev);
    let records = result.records();
    assert_eq!(1, records.len());
}

#[tokio::test]
async fn room_stream_should_return_the_desired_amount_of_events_when_using_filter_forward() {
    let test = TestingApp::default().await;
    let (creator, room) = test.public_room().await;
    let filtered_user_id = test.register().await;
    test.join_room(room.id().to_owned(), filtered_user_id.clone())
        .await
        .unwrap();

    // Stream contains messages from creator interleaved with "id"
    for i in 0..5 {
        test.talk(room.id(), &creator, &i.to_string())
            .await
            .unwrap();
    }
    test.talk(room.id(), &filtered_user_id, "a").await.unwrap();
    for i in 0..5 {
        test.talk(room.id(), &creator, &i.to_string())
            .await
            .unwrap();
    }
    test.talk(room.id(), &filtered_user_id, "b").await.unwrap();
    for i in 0..5 {
        test.talk(room.id(), &creator, &i.to_string())
            .await
            .unwrap();
    }

    let room_stream = test.service::<RoomStream>();
    let mut definition = FilterDefinition::default();
    definition.room.timeline.senders = Some(vec![filtered_user_id.user().to_owned()]);
    let ignored = HashSet::new();
    let filter = Filter::new_single_room(&definition, room.id(), &ignored);

    let stream_filter = StreamFilter {
        filter: Some(&filter),
        room_id: Some(room.id()),
        source: StreamSource::Timeline,
    };
    let range = (
        Boundary::Inclusive(StreamToken::from(3)),
        Boundary::Unlimited,
    );
    let pagination = Pagination::new(range.into(), Some(stream_filter))
        .with_limit(Limit::from(1))
        .with_direction(Direction::Forward);

    let result = room_stream.stream_room_events(pagination).await.unwrap();

    let next = result.next().cloned().unwrap();
    let prev = result.previous().cloned().unwrap();
    assert_eq!(11, *next);
    assert_eq!(3, *prev);
    let records = result.records();
    assert_eq!(1, records.len());
}

#[tokio::test]
async fn room_stream_should_read_the_full_stream_if_filter_filters_all_events_in_the_stream() {
    let test = TestingApp::default().await;
    let (creator, room) = test.public_room().await;
    let filtered_user_id = test.register().await;
    let non_participating_user = test.register().await;
    test.join_room(room.id().to_owned(), filtered_user_id.clone())
        .await
        .unwrap();

    // Stream contains messages from creator interleaved with "id"
    for i in 0..5 {
        test.talk(room.id(), &creator, &i.to_string())
            .await
            .unwrap();
    }
    test.talk(room.id(), &filtered_user_id, "a").await.unwrap();
    for i in 0..5 {
        test.talk(room.id(), &creator, &i.to_string())
            .await
            .unwrap();
    }
    test.talk(room.id(), &filtered_user_id, "b").await.unwrap();
    for i in 0..5 {
        test.talk(room.id(), &creator, &i.to_string())
            .await
            .unwrap();
    }

    let room_stream = test.service::<RoomStream>();
    let mut definition = FilterDefinition::default();
    definition.room.timeline.senders = Some(vec![non_participating_user.user().to_owned()]);
    let ignored = HashSet::new();
    let filter = Filter::new_single_room(&definition, room.id(), &ignored);

    let stream_filter = StreamFilter {
        filter: Some(&filter),
        room_id: Some(room.id()),
        source: StreamSource::Timeline,
    };
    let range = (
        Boundary::Inclusive(StreamToken::from(3)),
        Boundary::Unlimited,
    );
    let pagination = Pagination::new(range.into(), Some(stream_filter))
        .with_limit(Limit::from(1))
        .with_direction(Direction::Backward);

    let result = room_stream.stream_room_events(pagination).await.unwrap();

    assert!(result.next().is_none());
    assert!(result.previous().is_none());
    let records = result.records();
    assert_eq!(0, records.len());
}
