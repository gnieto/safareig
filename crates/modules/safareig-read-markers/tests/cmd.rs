use safareig_core::{
    command::{Command, Module},
    ruma::{api::client::read_marker::set_read_marker, EventId, RoomId},
};
use safareig_ephemeral::services::EphemeralEvent;
use safareig_push_notifications::PushRulesModule;
use safareig_read_markers::{command::SetReadMarkerCommand, module::ReadMarkersModule};
use safareig_receipts::module::ReceiptsModule;
use safareig_testing::TestingApp;

#[tokio::test]
async fn it_does_not_set_marker_if_user_not_in_room() {
    let modules: Vec<Box<dyn Module<_>>> = vec![
        Box::new(ReadMarkersModule::default()),
        Box::new(ReceiptsModule::default()),
        Box::new(PushRulesModule),
    ];

    let app = TestingApp::with_modules(modules).await;
    let marker_cmd = app.command::<SetReadMarkerCommand>();
    let input = set_read_marker::v3::Request {
        fully_read: Some(EventId::new(&app.server_name())),
        read_receipt: None,
        room_id: RoomId::new(&app.server_name()),
        private_read_receipt: None,
    };

    let cmd_result = marker_cmd.execute(input, TestingApp::new_identity()).await;

    assert!(cmd_result.is_err());
}

#[tokio::test]
async fn it_does_not_set_marker_if_event_id_not_exists() {
    let modules: Vec<Box<dyn Module<_>>> = vec![
        Box::new(ReadMarkersModule::default()),
        Box::new(ReceiptsModule::default()),
        Box::new(PushRulesModule),
    ];
    let app = TestingApp::with_modules(modules).await;
    let room = app.public_room().await.1;
    let marker_cmd = app.command::<SetReadMarkerCommand>();

    let input = set_read_marker::v3::Request {
        fully_read: Some(EventId::new(&app.server_name())),
        read_receipt: None,
        room_id: room.id().to_owned(),
        private_read_receipt: None,
    };

    let cmd_result = marker_cmd.execute(input, TestingApp::new_identity()).await;

    assert!(cmd_result.is_err());
}

#[tokio::test]
async fn it_sets_marker() {
    let modules: Vec<Box<dyn Module<_>>> = vec![
        Box::new(ReadMarkersModule::default()),
        Box::new(ReceiptsModule::default()),
        Box::new(PushRulesModule),
    ];
    let app = TestingApp::with_modules(modules).await;
    let (identity, room) = app.public_room().await;

    let event_id = app.talk(room.id(), &identity, "hi!").await.unwrap();

    let input = set_read_marker::v3::Request {
        fully_read: Some(event_id.clone()),
        read_receipt: None,
        room_id: room.id().to_owned(),
        private_read_receipt: None,
    };

    let marker_cmd = app.command::<SetReadMarkerCommand>();
    let cmd_result = marker_cmd.execute(input, identity.clone()).await;

    assert!(cmd_result.is_ok());
}

#[tokio::test]
async fn it_sets_fully_read_marker_and_read_marker() {
    let modules: Vec<Box<dyn Module<_>>> = vec![
        Box::new(ReadMarkersModule::default()),
        Box::new(ReceiptsModule::default()),
        Box::new(PushRulesModule),
    ];
    let app = TestingApp::with_modules(modules).await;
    let (identity, room) = app.public_room().await;

    let event_id = app.talk(room.id(), &identity, "hi!").await.unwrap();
    let second_event_id = app.talk(room.id(), &identity, "hi again!").await.unwrap();

    let input = set_read_marker::v3::Request {
        fully_read: Some(event_id.clone()),
        read_receipt: Some(second_event_id.clone()),
        room_id: room.id().to_owned(),
        private_read_receipt: None,
    };

    let marker_cmd = app.command::<SetReadMarkerCommand>();
    let cmd_result = marker_cmd.execute(input, identity.clone()).await;

    assert!(cmd_result.is_ok());
    match app.latest_ephemeral().unwrap() {
        EphemeralEvent::Marker(room_id, user, event, _) => {
            assert_eq!(room.id(), &room_id);
            assert_eq!(identity.user(), &user);
            assert_eq!(event, second_event_id);
        }
        _ => {
            panic!("expected marker event")
        }
    }
}
