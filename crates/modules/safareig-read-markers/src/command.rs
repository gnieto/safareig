use async_trait::async_trait;
use safareig::client::room::loader::RoomLoader;
use safareig_core::{
    auth::Identity, command::Command, ruma::api::client::read_marker::set_read_marker, Inject,
};
use safareig_receipts::ReceiptManager;

#[derive(Inject)]
pub struct SetReadMarkerCommand {
    room_loader: RoomLoader,
    receipt_manager: ReceiptManager,
}

#[async_trait]
impl Command for SetReadMarkerCommand {
    type Input = set_read_marker::v3::Request;
    type Output = set_read_marker::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let room = self
            .room_loader
            .get_for_user(&input.room_id, id.user())
            .await?;

        if let Some(fully_read) = input.fully_read {
            if let Err(error) = self
                .receipt_manager
                .handle_fully_read_receipt(&room, id.user(), &fully_read)
                .await
            {
                tracing::warn!(?error, "Could not handle fully read receipt");
            }
        }

        if let Some(event_id) = &input.read_receipt {
            if let Err(error) = self
                .receipt_manager
                .handle_read_receipt(room.id(), id.user(), event_id)
                .await
            {
                tracing::warn!(?error, "Could not handle read receipt");
            }
        }

        if let Some(event_id) = &input.private_read_receipt {
            if let Err(error) = self
                .receipt_manager
                .handle_private_read_receipt(room.id(), id.user(), event_id)
                .await
            {
                tracing::warn!(?error, "Could not handle private read receipt");
            }
        }

        Ok(set_read_marker::v3::Response::new())
    }
}
