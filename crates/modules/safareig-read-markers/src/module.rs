use safareig_core::command::{Container, Module, Router};

use crate::command::SetReadMarkerCommand;

#[derive(Default)]
pub struct ReadMarkersModule {}

#[async_trait::async_trait]
impl<R: Router> Module<R> for ReadMarkersModule {
    fn register(&self, container: &mut Container, router: &mut R) {
        container.inject_endpoint::<_, SetReadMarkerCommand, _>(router);
    }
}
