use std::{
    collections::{BTreeSet, HashMap, HashSet},
    ops::Deref,
};

use safareig_core::{
    auth::Identity,
    ruma::{
        api::client::{filter::FilterDefinition, sync::sync_events},
        events::room::member::MembershipState,
        DeviceId, OwnedEventId, OwnedRoomId, OwnedUserId, RoomId, UserId,
    },
    StreamToken,
};

use crate::SyncToken;

#[derive(Clone, Copy, Debug)]
pub enum SyncMode {
    /// Incremental sync, from `since` token.
    Incremental,

    /// Request contains `full_state` flag
    FullState,

    /// Initial sync. No `since` token is provided.
    InitialSync,
}

impl SyncMode {
    pub fn new(request: &sync_events::v3::Request, token: Option<SyncToken>) -> Self {
        if request.full_state {
            SyncMode::FullState
        } else if token.is_none() {
            SyncMode::InitialSync
        } else {
            SyncMode::Incremental
        }
    }
}

pub struct Context<'a> {
    pub request: &'a sync_events::v3::Request,
    pub from_token: SyncToken,
    pub to_event_token: StreamToken,
    pub id: &'a Identity,
    pub filter: FilterDefinition,
    pub sync_mode: SyncMode,
    pub user_rooms: UserRoomsMemberhip,
    pub ignored: HashSet<OwnedUserId>,
}

impl<'a> Context<'a> {
    #[allow(clippy::wrong_self_convention)]
    pub fn from_token(&self) -> &SyncToken {
        &self.from_token
    }

    pub fn to_event_token(&self) -> StreamToken {
        self.to_event_token
    }

    pub fn needs_full_state(&self) -> bool {
        matches!(self.sync_mode, SyncMode::FullState)
    }

    pub fn sync_mode(&self) -> SyncMode {
        self.sync_mode
    }

    pub fn user(&self) -> &UserId {
        self.id.user()
    }

    pub fn device(&self) -> Option<&DeviceId> {
        self.id.device()
    }

    pub fn id(&self) -> &Identity {
        self.id
    }

    pub fn user_memberships(&self) -> &UserRoomsMemberhip {
        &self.user_rooms
    }

    pub fn user_join_since_last_sync(&self, room: &RoomId) -> bool {
        self.user_memberships()
            .joined_since_last_sync
            .contains_key(room)
    }

    pub fn is_large_gap(&self) -> bool {
        if self.from_token().event == StreamToken::horizon() {
            return true;
        }

        let from_event = *self.from_token().event.deref();
        let to_event = *self.to_event_token;

        (to_event - from_event) > 100
    }

    pub fn is_initial_sync(&self) -> bool {
        matches!(self.sync_mode, SyncMode::InitialSync)
    }
}

#[derive(Debug)]
pub struct SyncMembership {
    pub state: MembershipState,
    pub stream: StreamToken, // TODO: Evalute using depth (or add depth)
    pub event_id: OwnedEventId,
}

#[derive(Debug, Default)]
pub struct UserRoomsMemberhip {
    pub joined: HashMap<OwnedRoomId, SyncMembership>,
    pub joined_since_last_sync: HashMap<OwnedRoomId, SyncMembership>,
    pub invited: HashMap<OwnedRoomId, SyncMembership>,
    pub leave: HashMap<OwnedRoomId, SyncMembership>,
}

impl UserRoomsMemberhip {
    pub fn rooms(&self) -> BTreeSet<&RoomId> {
        self.joined
            .keys()
            .chain(self.joined_since_last_sync.keys())
            .chain(self.invited.keys())
            .chain(self.leave.keys())
            .map(|room| room.as_ref())
            .collect()
    }
}
