use safareig_core::{
    error::{error_chain, RumaExt},
    ruma::{
        api::client::error::{ErrorBody, ErrorKind},
        exports::http::StatusCode,
    },
    storage::StorageError,
};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum SyncError {
    #[error("Storage error")]
    Storage(#[from] StorageError),
    #[error("Invalid token")]
    InvalidToken,
    #[error("{0}")]
    Custom(String),
    #[error("Missing extension: {0}")]
    MissingExtension(String),
}

impl From<SyncError> for safareig_core::ruma::api::client::Error {
    fn from(error: SyncError) -> Self {
        tracing::error!(err = error_chain(&error), "Sync error");

        match error {
            SyncError::Storage(_)
            | SyncError::MissingExtension(_)
            | SyncError::InvalidToken
            | SyncError::Custom(_) => safareig_core::ruma::api::client::Error {
                body: ErrorBody::Standard {
                    kind: ErrorKind::Unknown,
                    message: error.to_string(),
                },
                status_code: StatusCode::INTERNAL_SERVER_ERROR,
            },
        }
    }
}

impl From<SyncError> for safareig_core::ruma::api::error::MatrixError {
    fn from(error: SyncError) -> Self {
        let client = safareig_core::ruma::api::client::Error::from(error);
        client.to_generic()
    }
}
