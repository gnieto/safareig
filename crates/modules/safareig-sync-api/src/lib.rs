use std::{collections::HashMap, str::FromStr};

use context::Context;
use error::SyncError;
use safareig_core::{ruma::api::client::sync::sync_events, StreamToken};

pub mod context;
pub mod error;

#[derive(Default, Clone)]
pub struct SyncToken {
    event: StreamToken,
    extensions: HashMap<String, String>,
}

impl SyncToken {
    pub fn new(event: StreamToken, extensions: HashMap<String, String>) -> Self {
        Self { event, extensions }
    }

    pub fn new_event(event: StreamToken) -> Self {
        Self {
            event,
            extensions: Default::default(),
        }
    }

    pub fn event(&self) -> StreamToken {
        self.event
    }

    pub fn update_event(&mut self, token: StreamToken) {
        self.event = token;
    }

    pub fn extension_token<T: SyncTokenPart>(&self) -> Result<Option<T>, SyncError> {
        self.extensions
            .get(T::id())
            .map(|raw_token| T::from_str(raw_token.as_str()))
            .transpose()
            .map_err(|_| SyncError::MissingExtension(T::id().to_string()))
    }

    pub fn update<T: SyncTokenPart>(&mut self, part: T) {
        self.extensions
            .insert(T::id().to_string(), part.to_string());
    }
}

impl ToString for SyncToken {
    fn to_string(&self) -> String {
        let mut sync_token = format!("{}", self.event,);

        for (id, token) in &self.extensions {
            sync_token.push_str(&format!("#{id}:{token}"));
        }

        sync_token
    }
}

impl TryFrom<&str> for SyncToken {
    type Error = ();

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let mut parts = value.split('#');

        let event = parts.next().ok_or(())?;
        let event = StreamToken::from_str(event).map_err(|_| ())?;

        let mut extensions = HashMap::new();

        for part in parts {
            let mut delimiter_split = part.split(':');

            match (delimiter_split.next(), delimiter_split.next()) {
                (Some(id), Some(value)) => {
                    extensions.insert(id.to_string(), value.to_string());
                }
                _ => {
                    tracing::warn!(token = part, "Found invalid sync token")
                }
            }
        }

        Ok(SyncToken { event, extensions })
    }
}

pub trait SyncTokenPart: ToString + FromStr + Default {
    fn id() -> &'static str;
}

#[async_trait::async_trait]
pub trait SyncExtension: Send + Sync {
    async fn fill_sync<'a>(
        &self,
        ctx: &'a Context<'a>,
        response: &mut sync_events::v3::Response,
        token: &mut SyncToken,
    ) -> Result<(), SyncError>;

    fn priority(&self) -> u8 {
        128
    }
}
