use std::sync::Arc;

use safareig_client_config::{
    account_data_stream, command::SetAccountRoomDataCommand, storage::AccountDataStorage,
};
use safareig_core::{
    command::Command,
    pagination::{Pagination, Range},
    ruma::{
        api::client::config::set_room_account_data,
        events::{RoomAccountDataEventType, TimelineEventType},
        serde::Raw,
    },
};
use safareig_testing::{error::RumaErrorExtension, TestingApp};
use serde_json::{json, value::RawValue};

#[tokio::test]
async fn it_does_not_store_account_data_if_event_type_is_banned() {
    let app = TestingApp::default().await;
    let (user, room) = app.public_room().await;
    let cmd = app.command::<SetAccountRoomDataCommand>();
    let input = set_room_account_data::v3::Request {
        user_id: user.user().to_owned(),
        event_type: RoomAccountDataEventType::from("m.fully_read"),
        data: Raw::from_json(RawValue::from_string("{}".to_string()).unwrap()),
        room_id: room.id().to_owned(),
    };

    let response = cmd.execute(input, user).await;

    assert!(response.is_err());
    assert!(response
        .err()
        .unwrap()
        .message()
        .contains("target type can not"));
}

#[tokio::test]
async fn it_stores_global_account_data() {
    let app = TestingApp::default().await;
    let (user, room) = app.public_room().await;
    let cmd = app.command::<SetAccountRoomDataCommand>();
    let input = set_room_account_data::v3::Request {
        user_id: user.user().to_owned(),
        event_type: RoomAccountDataEventType::from("custom_event"),
        data: Raw::from_json(RawValue::from_string("{}".to_string()).unwrap()),
        room_id: room.id().to_owned(),
    };

    let _ = cmd.execute(input, user.clone()).await.unwrap();

    let storage = app.service::<Arc<dyn AccountDataStorage>>();
    let pagination = Pagination::new(Range::all(), account_data_stream(user.user()));
    let mut stream = storage
        .account_data_stream(user.user(), &pagination)
        .await
        .unwrap()
        .records();
    let event = stream.remove(0);
    assert_eq!(
        event.content().event_type(),
        TimelineEventType::from("custom_event")
    );
}

#[tokio::test]
async fn store_same_event_type_multiple_times_retrieves_only_one_event() {
    let app = TestingApp::default().await;
    let (user, room) = app.public_room().await;
    let cmd = app.command::<SetAccountRoomDataCommand>();

    for i in 0..3 {
        let data = json!({
            "id": i,
        });

        let input = set_room_account_data::v3::Request {
            user_id: user.user().to_owned(),
            event_type: RoomAccountDataEventType::from("custom_event"),
            data: Raw::from_json(serde_json::value::to_raw_value(&data).unwrap()),
            room_id: room.id().to_owned(),
        };

        let _ = cmd.execute(input, user.clone()).await.unwrap();
    }

    let storage = app.service::<Arc<dyn AccountDataStorage>>();
    let pagination = Pagination::new(Range::all(), account_data_stream(user.user()));

    let stream_response = storage
        .account_data_stream(user.user(), &pagination)
        .await
        .unwrap();
    assert_eq!(1, stream_response.records().len());
}
