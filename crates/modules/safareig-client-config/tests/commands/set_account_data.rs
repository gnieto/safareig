use std::sync::Arc;

use safareig_client_config::{
    account_data_stream, command::SetAccountDataCommand, storage::AccountDataStorage,
};
use safareig_core::{
    auth::Identity,
    command::Command,
    pagination::{Pagination, Range},
    ruma::{
        api::client::config::set_global_account_data,
        events::{GlobalAccountDataEventType, TimelineEventType},
        serde::Raw,
    },
};
use safareig_testing::{error::RumaErrorExtension, TestingApp};
use serde_json::{json, value::RawValue};

#[tokio::test]
async fn it_does_not_store_account_data_if_user_does_not_match() {
    let app = TestingApp::default().await;
    let user_a = app.register().await;
    let user_b = app.register().await;
    let cmd = app.command::<SetAccountDataCommand>();
    let input = set_global_account_data::v3::Request {
        user_id: user_b.user().to_owned(),
        event_type: GlobalAccountDataEventType::from("any_event"),
        data: Raw::from_json(RawValue::from_string("{}".to_string()).unwrap()),
    };

    let response = cmd.execute(input, user_a).await;

    assert!(response.is_err());
    assert!(response.err().unwrap().message().contains("user mismatch"));
}

#[tokio::test]
async fn it_does_not_store_account_data_if_event_type_is_banned() {
    let app = TestingApp::default().await;
    let user = app.register().await;
    let cmd = app.command::<SetAccountDataCommand>();
    let input = set_global_account_data::v3::Request {
        user_id: user.user().to_owned(),
        event_type: GlobalAccountDataEventType::from("m.fully_read"),
        data: Raw::from_json(RawValue::from_string("{}".to_string()).unwrap()),
    };

    let response = cmd.execute(input, user).await;

    assert!(response.is_err());
    assert!(response
        .err()
        .unwrap()
        .message()
        .contains("target type can not"));
}

#[tokio::test]
async fn it_does_not_allow_to_set_many_properties() {
    let mut cfg = TestingApp::default_config();
    cfg.account.account_data_max_size = 5;
    let app = TestingApp::with_config(cfg).await;
    let user = app.register().await;
    add_properties(&app, &user, 5).await;

    let cmd = app.command::<SetAccountDataCommand>();
    let input = set_global_account_data::v3::Request {
        user_id: user.user().to_owned(),
        event_type: GlobalAccountDataEventType::from("custom_event"),
        data: Raw::from_json(RawValue::from_string("{}".to_string()).unwrap()),
    };
    let response = cmd.execute(input, user).await;

    assert!(response.is_err());
    assert!(response
        .err()
        .unwrap()
        .message()
        .contains("too many properties on account data"));
}

#[tokio::test]
async fn it_stores_global_account_data() {
    let app = TestingApp::default().await;
    let user = app.register().await;
    let cmd = app.command::<SetAccountDataCommand>();
    let input = set_global_account_data::v3::Request {
        user_id: user.user().to_owned(),
        event_type: GlobalAccountDataEventType::from("custom_event"),
        data: Raw::from_json(RawValue::from_string("{}".to_string()).unwrap()),
    };

    let _ = cmd.execute(input, user.clone()).await.unwrap();

    let storage = app.service::<Arc<dyn AccountDataStorage>>();
    let pagination = Pagination::new(Range::all(), account_data_stream(user.user()));
    let mut stream = storage
        .account_data_stream(user.user(), &pagination)
        .await
        .unwrap()
        .records();
    assert_eq!(1, stream.len());
    let event = stream.remove(0);
    assert_eq!(
        event.content().event_type(),
        TimelineEventType::from("custom_event")
    );
}

#[tokio::test]
async fn store_same_event_type_multiple_times_retrieves_only_one_event() {
    let app = TestingApp::default().await;
    let user = app.register().await;
    let cmd = app.command::<SetAccountDataCommand>();

    for i in 0..3 {
        let data = json!({
            "id": i,
        });

        let input = set_global_account_data::v3::Request {
            user_id: user.user().to_owned(),
            event_type: GlobalAccountDataEventType::from("custom_event"),
            data: Raw::from_json(serde_json::value::to_raw_value(&data).unwrap()),
        };

        let _ = cmd.execute(input, user.clone()).await.unwrap();
    }

    let storage = app.service::<Arc<dyn AccountDataStorage>>();
    let pagination = Pagination::new(Range::all(), account_data_stream(user.user()));

    let stream_response = storage
        .account_data_stream(user.user(), &pagination)
        .await
        .unwrap();
    assert_eq!(1, stream_response.records().len());
}

#[tokio::test]
#[tracing_test::traced_test]
async fn store_secret_storage() {
    let app = TestingApp::default().await;
    let user = app.register().await;
    let cmd = app.command::<SetAccountDataCommand>();

    let raw_json = r#"
    {
        "algorithm": "m.secret_storage.v1.aes-hmac-sha2",
        "passphrase": {
            "algorithm": "m.pbkdf2",
            "iterations": 500000,
            "salt": "Nd69alzbATTROw3STtTypW2jxhKCHRDn"
        },
        "iv": "BrfXyUPgHLIGG/sJSCbR8w==",
        "mac": "pXnqGbZaPR8hJSubb7kA3+HVSF5FXckTV+hblAC9J80="
    }
    "#;

    let input = set_global_account_data::v3::Request {
        user_id: user.user().to_owned(),
        event_type: GlobalAccountDataEventType::from(
            "m.secret_storage.key.qrzBeOaOD3TanET4jLupihg36je1gQS3",
        ),
        data: Raw::from_json_string(raw_json.to_string()).unwrap(),
    };

    let _ = cmd.execute(input, user.clone()).await.unwrap();
}

async fn add_properties(db: &TestingApp, id: &Identity, amount: u32) {
    let data = RawValue::from_string("{}".to_string()).unwrap();
    for i in 0..amount {
        db.store_global_account_data(id, &format!("event_{i}"), data.clone())
            .await
            .unwrap();
    }
}
