use std::collections::BTreeMap;

use safareig_client_config::command::GetAccountRoomDataCommand;
use safareig_core::{
    command::Command,
    ruma::{
        api::client::config::get_room_account_data, events::AnyRoomAccountDataEventContent,
        serde::Raw,
    },
};
use safareig_testing::{assert_not_found, TestingApp};
use serde_json::{json, value::to_raw_value, Value};

#[tokio::test]
async fn it_returns_resource_not_found_if_key_was_not_set() {
    let app = TestingApp::default().await;
    let (user, room) = app.public_room().await;
    let cmd = app.command::<GetAccountRoomDataCommand>();
    let input = get_room_account_data::v3::Request {
        event_type: "custom_event".into(),
        user_id: user.user().to_owned(),
        room_id: room.id().to_owned(),
    };

    let response = cmd.execute(input, user.clone()).await;

    assert_not_found(&response);
}

#[tokio::test]
async fn it_returns_event_data_if_key_was_set() {
    let app = TestingApp::default().await;
    let (user, room) = app.public_room().await;
    let cmd = app.command::<GetAccountRoomDataCommand>();
    app.store_room_account_data(
        &user,
        "m.direct",
        to_raw_value(&json!({
            "@user:localhost": [],
        }))
        .unwrap(),
        &room,
    )
    .await
    .unwrap();

    let input = get_room_account_data::v3::Request {
        event_type: "m.direct".into(),
        user_id: user.user().to_owned(),
        room_id: room.id().to_owned(),
    };

    let response = cmd.execute(input, user.clone()).await.unwrap();
    let raw = content(response.account_data);

    assert_eq!(1, raw.len());
    assert!(raw.contains_key("@user:localhost"));
}

#[tokio::test]
async fn it_returns_only_last_key_if_set_multiple_times() {
    let app = TestingApp::default().await;
    let (user, room) = app.public_room().await;
    let cmd = app.command::<GetAccountRoomDataCommand>();
    app.store_room_account_data(
        &user,
        "m.direct",
        to_raw_value(&json!({
            "@user:localhost": [],
        }))
        .unwrap(),
        &room,
    )
    .await
    .unwrap();

    app.store_room_account_data(
        &user,
        "m.direct",
        to_raw_value(&json!({
            "@user:localhost": ["!asdf:localhost", "!qwerty:localhost"],
        }))
        .unwrap(),
        &room,
    )
    .await
    .unwrap();
    let input = get_room_account_data::v3::Request {
        event_type: "m.direct".into(),
        user_id: user.user().to_owned(),
        room_id: room.id().to_owned(),
    };

    let response = cmd.execute(input, user.clone()).await.unwrap();
    let raw = content(response.account_data);

    assert_eq!(1, raw.len());
    assert!(raw.contains_key("@user:localhost"));
    assert_eq!(
        2,
        raw.get("@user:localhost")
            .unwrap()
            .as_array()
            .unwrap()
            .len()
    );
}

fn content(event: Raw<AnyRoomAccountDataEventContent>) -> BTreeMap<String, Value> {
    let any_event = event.json().to_string();

    serde_json::from_str::<BTreeMap<String, Value>>(&any_event).unwrap()
}
