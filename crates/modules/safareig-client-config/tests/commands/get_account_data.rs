use std::collections::BTreeMap;

use safareig_client_config::command::GetAccountDataCommand;
use safareig_core::{
    command::Command,
    ruma::{
        api::client::config::get_global_account_data, events::AnyGlobalAccountDataEventContent,
        serde::Raw,
    },
};
use safareig_testing::{assert_not_found, TestingApp};
use serde_json::{json, value::to_raw_value, Value};

#[tokio::test]
async fn it_returns_resource_not_found_if_key_was_not_set() {
    let app = TestingApp::default().await;
    let user = app.register().await;
    let cmd = app.command::<GetAccountDataCommand>();
    let input = get_global_account_data::v3::Request {
        event_type: "custom_event".into(),
        user_id: user.user().to_owned(),
    };

    let response = cmd.execute(input, user.clone()).await;

    assert_not_found(&response);
}

#[tokio::test]
async fn it_returns_event_data_if_key_was_set() {
    let app = TestingApp::default().await;
    let user = app.register().await;
    let cmd = app.command::<GetAccountDataCommand>();
    app.store_global_account_data(
        &user,
        "m.direct",
        to_raw_value(&json!({
            "@user:localhost": [],
        }))
        .unwrap(),
    )
    .await
    .unwrap();

    let input = get_global_account_data::v3::Request {
        event_type: "m.direct".into(),
        user_id: user.user().to_owned(),
    };

    let response = cmd.execute(input, user.clone()).await.unwrap();
    let raw = content(response.account_data);

    assert_eq!(1, raw.len());
    assert!(raw.contains_key("@user:localhost"));
}

#[tokio::test]
async fn it_returns_only_last_key_if_set_multiple_times() {
    let app = TestingApp::default().await;
    let user = app.register().await;
    let cmd = app.command::<GetAccountDataCommand>();
    app.store_global_account_data(
        &user,
        "m.direct",
        to_raw_value(&json!({
            "@user:localhost": [],
        }))
        .unwrap(),
    )
    .await
    .unwrap();

    app.store_global_account_data(
        &user,
        "m.direct",
        to_raw_value(&json!({
            "@user:localhost": ["!asdf:localhost", "!qwerty:localhost"],
        }))
        .unwrap(),
    )
    .await
    .unwrap();
    let input = get_global_account_data::v3::Request {
        event_type: "m.direct".into(),
        user_id: user.user().to_owned(),
    };

    let response = cmd.execute(input, user.clone()).await.unwrap();
    let raw = content(response.account_data);

    assert_eq!(1, raw.len());
    assert!(raw.contains_key("@user:localhost"));
    assert_eq!(
        2,
        raw.get("@user:localhost")
            .unwrap()
            .as_array()
            .unwrap()
            .len()
    );
}

fn content(event: Raw<AnyGlobalAccountDataEventContent>) -> BTreeMap<String, Value> {
    let any_event = event.json().to_string();

    serde_json::from_str::<BTreeMap<String, Value>>(&any_event).unwrap()
}
