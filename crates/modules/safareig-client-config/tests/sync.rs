use std::{collections::HashSet, sync::Arc, time::Duration};

use safareig_client_config::{
    storage::{AccountDataEvent, AccountDataStorage},
    sync::{AccountSync, ClientConfigToken},
};
use safareig_core::{
    auth::Identity,
    events::Content,
    ruma::{
        api::client::{filter::FilterDefinition, sync::sync_events},
        events::{
            room::member::MembershipState,
            tag::{TagEventContent, Tags},
            AnyGlobalAccountDataEvent,
        },
        presence::PresenceState,
        EventId, OwnedUserId, RoomId,
    },
    StreamToken,
};
use safareig_sync_api::{
    context::{Context, SyncMembership, SyncMode, UserRoomsMemberhip},
    SyncExtension, SyncToken,
};
use safareig_testing::TestingApp;
use serde_json::{json, value::to_raw_value};

#[tokio::test]
pub async fn account_data_retrieves_global_and_room_account_data_events() {
    let test = TestingApp::default().await;
    let id = test.register().await;
    let (_, public_room) = test.public_room().await;
    test.join_room(public_room.id().to_owned(), id.clone())
        .await
        .unwrap();
    let sync = AccountSync::new(test.service::<Arc<dyn AccountDataStorage>>());
    let request = sync_events::v3::Request {
        filter: None,
        full_state: false,
        set_presence: PresenceState::Unavailable,
        timeout: Some(Duration::from_secs(1)),
        since: None,
    };

    add_direct_to_global_account_data(&test, &id, &[]).await;
    add_direct_to_global_account_data(&test, &id, &["!asdf:localhost", "!qwerty:localhost"]).await;
    add_room_account_data(&test, &id, public_room.id()).await;

    let mut membership = UserRoomsMemberhip::default();
    membership.joined.insert(
        public_room.id().to_owned(),
        SyncMembership {
            state: MembershipState::Join,
            stream: StreamToken::default(),
            event_id: EventId::new(&test.server_name()),
        },
    );

    let context = Context {
        request: &request,
        from_token: SyncToken::default(),
        to_event_token: StreamToken::default(),
        id: &id,
        user_rooms: membership,
        filter: FilterDefinition::default(),
        sync_mode: SyncMode::FullState,
        ignored: HashSet::new(),
    };
    let mut sync_token = SyncToken::default();

    // Execute sync
    let mut response = sync_events::v3::Response::new("".to_string());
    sync.fill_sync(&context, &mut response, &mut sync_token)
        .await
        .unwrap();

    assert_eq!(1, response.account_data.events.len());
    assert_eq!(
        3,
        *sync_token
            .extension_token::<ClientConfigToken>()
            .unwrap()
            .unwrap()
            .token()
    );

    let event = response.account_data.events[0].deserialize().unwrap();
    match event {
        AnyGlobalAccountDataEvent::Direct(direct) => {
            let user: OwnedUserId = "@user:localhost".parse().unwrap();
            let rooms = direct.content.0.get(&user).unwrap();
            assert_eq!(2, rooms.len());
        }
        _ => panic!("expected direct event type"),
    }

    let room = response.rooms.join.get(public_room.id()).unwrap();
    assert_eq!(1, room.account_data.events.len());
}

async fn add_direct_to_global_account_data(test: &TestingApp, id: &Identity, rooms: &[&str]) {
    test.store_global_account_data(
        id,
        "m.direct",
        to_raw_value(&json!({
            "@user:localhost": rooms,
        }))
        .unwrap(),
    )
    .await
    .unwrap();
}

async fn add_room_account_data(test: &TestingApp, id: &Identity, room_id: &RoomId) {
    let account = test.service::<Arc<dyn AccountDataStorage>>();
    let tag_event = TagEventContent::new(Tags::default());
    let event = AccountDataEvent::Room(
        room_id.to_owned(),
        Content::from_event_content(&tag_event).unwrap(),
    );

    account.update_account_data(id.user(), event).await.unwrap();
}
