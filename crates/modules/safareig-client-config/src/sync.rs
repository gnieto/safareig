use std::{str::FromStr, sync::Arc};

use safareig_core::{
    pagination::{self, Pagination},
    ruma::api::client::sync::sync_events::{self},
    StreamToken,
};
use safareig_sync_api::{
    context::Context, error::SyncError, SyncExtension, SyncToken, SyncTokenPart,
};

use crate::{
    account_data_stream,
    storage::{AccountDataEvent, AccountDataStorage},
};

pub struct AccountSync {
    account: Arc<dyn AccountDataStorage>,
}

impl AccountSync {
    pub fn new(account: Arc<dyn AccountDataStorage>) -> Self {
        AccountSync { account }
    }
}

#[async_trait::async_trait]
impl SyncExtension for AccountSync {
    #[tracing::instrument(name = "sync-account-data", skip_all)]
    async fn fill_sync<'a>(
        &self,
        ctx: &'a Context<'a>,
        response: &mut sync_events::v3::Response,
        token: &mut SyncToken,
    ) -> Result<(), SyncError> {
        let from = ctx
            .from_token()
            .extension_token::<ClientConfigToken>()
            .unwrap()
            .unwrap_or_default();
        let range = (
            pagination::Boundary::Exclusive(from.0),
            pagination::Boundary::Unlimited,
        );
        let query = Pagination::new(range.into(), account_data_stream(ctx.user()));

        let stream_response = self.account.account_data_stream(ctx.user(), &query).await?;

        // Update sync token
        if let Some(new_token) = stream_response.next() {
            token.update(ClientConfigToken(*new_token));
        }
        let events = stream_response.records();

        // Fill response with account data events
        for e in events {
            match e {
                AccountDataEvent::Room(room, event) => {
                    let joined_room = response.rooms.join.entry(room).or_default();

                    let event_type = event.event_type().clone();
                    match event.into_raw() {
                        Ok(raw_event) => joined_room.account_data.events.push(raw_event),
                        Err(e) => {
                            tracing::error!(
                                err = e.to_string().as_str(),
                                event_type = event_type.to_string().as_str(),
                                "could not serialize account data event",
                            )
                        }
                    }
                }
                AccountDataEvent::Global(event) => {
                    let event_type = event.event_type().clone();
                    match event.into_raw() {
                        Ok(raw_event) => response.account_data.events.push(raw_event),
                        Err(e) => {
                            tracing::error!(
                                err = e.to_string().as_str(),
                                event_type = event_type.to_string().as_str(),
                                "could not serialize account data event",
                            )
                        }
                    }
                }
            }
        }

        Ok(())
    }
}

#[derive(Default)]
pub struct ClientConfigToken(StreamToken);

impl ClientConfigToken {
    pub fn token(&self) -> StreamToken {
        self.0
    }
}

impl SyncTokenPart for ClientConfigToken {
    fn id() -> &'static str {
        "c"
    }
}

impl ToString for ClientConfigToken {
    fn to_string(&self) -> String {
        self.0.to_string()
    }
}

impl FromStr for ClientConfigToken {
    type Err = std::num::ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(ClientConfigToken(StreamToken::from_str(s)?))
    }
}
