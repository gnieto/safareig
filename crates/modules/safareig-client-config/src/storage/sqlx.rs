use safareig_core::{
    events::Content,
    pagination::{Pagination, PaginationResponse},
    ruma::{
        events::{GlobalAccountDataEventType, RoomAccountDataEventType},
        RoomId, UserId,
    },
    storage::StorageError,
    StreamToken,
};
use safareig_sqlx::{decode_i64_to_u64, DirectionExt, PaginatedIterator, RangeWrapper};
use sea_query::{Alias, Condition, Expr, Iden, Query, SqliteQueryBuilder};
use sea_query_binder::SqlxBinder;
use sqlx::{Row, Sqlite};

use super::{AccountDataEvent, AccountDataStorage};

pub struct SqlxStorage {
    pool: sqlx::Pool<Sqlite>,
}

impl SqlxStorage {
    pub fn new(pool: sqlx::Pool<Sqlite>) -> Self {
        Self { pool }
    }
}

#[async_trait::async_trait]
impl AccountDataStorage for SqlxStorage {
    async fn update_account_data(
        &self,
        user: &UserId,
        event: AccountDataEvent,
    ) -> Result<StreamToken, StorageError> {
        let (content, maybe_room): (_, Option<String>) = match event {
            AccountDataEvent::Global(content) => (content, None),
            AccountDataEvent::Room(room_id, content) => (content, Some(room_id.to_string())),
        };
        let event_type = content.event_type();
        let event_content = content.content();

        let mut tx = self.pool.begin().await?;

        sqlx::query(
            r#"
            INSERT INTO account_data (user_id, event_type, room_id, event)
            VALUES (?1, ?2, ?3, ?4)
            ON CONFLICT DO UPDATE SET event = ?4
            "#,
        )
        .bind(user.as_str())
        .bind(event_type.to_string().as_str())
        .bind(maybe_room.as_ref())
        .bind(event_content)
        .execute(&mut *tx)
        .await?;

        let token = sqlx::query(
            r#"
            INSERT INTO account_data_stream (user_id, event_type, room_id, stream_token)
            VALUES (?1, ?2, ?3, (SELECT COALESCE(MAX(stream_token), 0) + 1 FROM  account_data_stream WHERE user_id = ?1))
            RETURNING stream_token
            "#,
        )
        .bind(user.as_str())
        .bind(event_type.to_string().as_str())
        .bind(maybe_room.as_ref())
        .bind(event_content)
        .fetch_optional(&mut *tx)
        .await?
        .map(|row| {
            let token = decode_i64_to_u64(row.try_get::<i64, _>("stream_token")?)?;

            Ok(StreamToken::from(token)) as Result<_, StorageError>
        })
        .transpose()?
        .unwrap_or_default();

        tx.commit().await?;

        Ok(token)
    }

    async fn get_account_event(
        &self,
        user: &UserId,
        event_type: &GlobalAccountDataEventType,
    ) -> Result<Option<Content>, StorageError> {
        let account_data = sqlx::query(
            r#"
            SELECT event FROM account_data
            WHERE user_id = ?1 AND event_type = ?2 AND room_id IS NULL
            "#,
        )
        .bind(user.as_str())
        .bind(event_type.to_string().as_str())
        .fetch_optional(&self.pool)
        .await?
        .map(|row| {
            let event = row.try_get::<serde_json::Value, _>("event")?;
            let content = Content::new(event, event_type.to_string());

            Ok(content) as Result<_, StorageError>
        })
        .transpose()?;

        Ok(account_data)
    }

    async fn get_room_account_event(
        &self,
        user: &UserId,
        room: &RoomId,
        event_type: &RoomAccountDataEventType,
    ) -> Result<Option<Content>, StorageError> {
        let account_data = sqlx::query(
            r#"
            SELECT event FROM account_data
            WHERE user_id = ?1 AND event_type = ?2 AND room_id = ?3
            "#,
        )
        .bind(user.as_str())
        .bind(event_type.to_string().as_str())
        .bind(room.as_str())
        .fetch_optional(&self.pool)
        .await?
        .map(|row| {
            let event = row.try_get::<serde_json::Value, _>("event")?;
            let content = Content::new(event, event_type.to_string());

            Ok(content) as Result<_, StorageError>
        })
        .transpose()?;

        Ok(account_data)
    }

    async fn account_data_properties(
        &self,
        user: &UserId,
        room: Option<&RoomId>,
    ) -> Result<u32, StorageError> {
        let query = match room {
            Some(room) => sqlx::query(
                r#"
                    SELECT COUNT(*) as properties_count FROM account_data
                    WHERE user_id = ?1 AND room_id = ?2
                    "#,
            )
            .bind(user.as_str())
            .bind(room.as_str()),
            None => sqlx::query(
                r#"
                    SELECT COUNT(*) as properties_count FROM account_data
                    WHERE user_id = ?1 AND room_id IS NULL
                    "#,
            )
            .bind(user.as_str()),
        };

        let rows = query
            .fetch_optional(&self.pool)
            .await?
            .map(|row| {
                let count = row.try_get::<u32, _>("properties_count")?;

                Ok(count) as Result<_, StorageError>
            })
            .transpose()?
            .unwrap_or(0u32);

        Ok(rows)
    }

    async fn account_data_stream(
        &self,
        user: &UserId,
        query: &Pagination<StreamToken, String>,
    ) -> Result<PaginationResponse<StreamToken, AccountDataEvent>, StorageError> {
        let join_cond = Condition::all()
            .add(Expr::col((AccountData::Table, AccountData::UserId)).eq(Expr::cust("ads.user_id")))
            .add(
                Condition::all()
                    .add(
                        Expr::col((AccountData::Table, AccountData::EventType))
                            .eq(Expr::cust("ads.event_type")),
                    )
                    .add(
                        Condition::any()
                            .add(
                                Expr::col((AccountData::Table, AccountData::RoomId))
                                    .eq(Expr::cust("ads.room_id")),
                            )
                            .add(
                                Condition::any()
                                    .add(
                                        Expr::col((AccountData::Table, AccountData::RoomId))
                                            .is_null(),
                                    )
                                    .add(Expr::cust("ads.room_id IS NULL")),
                            ),
                    ),
            );

        let wrapper = RangeWrapper(query.range().map(|t| t.to_string()));
        let mut subquery = Query::select();
        subquery
            .column((AccountDataStream::Table, AccountDataStream::UserId))
            .column((AccountDataStream::Table, AccountDataStream::EventType))
            .column((AccountDataStream::Table, AccountDataStream::RoomId))
            .expr_as(
                Expr::col(AccountDataStream::StreamToken).max(),
                Alias::new("stream_token"),
            )
            .from(AccountDataStream::Table)
            .and_where(Expr::col(AccountDataStream::UserId).eq(user.as_str()))
            .cond_where(wrapper.condition(Expr::col(AccountDataStream::StreamToken)))
            .group_by_columns([
                (AccountDataStream::Table, AccountDataStream::UserId),
                (AccountDataStream::Table, AccountDataStream::EventType),
                (AccountDataStream::Table, AccountDataStream::RoomId),
            ])
            .order_by(AccountDataStream::StreamToken, query.direction().order())
            .limit(query.limit().unwrap_or(20) as u64);

        let (query, values) = Query::select()
            .expr(Expr::table_asterisk(AccountData::Table))
            .expr(Expr::cust("ads.stream_token"))
            .from_subquery(subquery, Alias::new("ads"))
            .inner_join(AccountData::Table, join_cond)
            .build_sqlx(SqliteQueryBuilder);

        let account_data_events = sqlx::query_with(&query, values)
            .fetch_all(&self.pool)
            .await?;

        let iterator = PaginatedIterator::new(account_data_events.into_iter(), |row| {
            let token =
                StreamToken::from(decode_i64_to_u64(row.try_get::<i64, _>("stream_token")?)?);
            let room = row.try_get::<Option<&str>, _>("room_id")?;
            let event_type = row.try_get::<&str, _>("event_type")?;
            let event = row.try_get::<serde_json::Value, _>("event")?;
            let content = Content::new(event, event_type.to_string());

            let account_data_event = match room {
                Some(room) => {
                    let room_id = room.parse()?;
                    AccountDataEvent::Room(room_id, content)
                }
                None => AccountDataEvent::Global(content),
            };

            Ok((token, account_data_event)) as Result<_, StorageError>
        });

        iterator.into_pagination_response()
    }

    async fn account_data_head(&self, user_id: &UserId) -> Result<StreamToken, StorageError> {
        let token = sqlx::query(
            r#"
            SELECT MAX(stream_token) as stream_token FROM account_data
            WHERE user_id = ?1
            "#,
        )
        .bind(user_id.as_str())
        .fetch_optional(&self.pool)
        .await?
        .map(|row| {
            let count = decode_i64_to_u64(row.try_get::<i64, _>("stream_token")?)?;

            Ok(StreamToken::from(count)) as Result<_, StorageError>
        })
        .transpose()?
        .unwrap_or_default();

        Ok(token)
    }
}

#[derive(Iden)]
enum AccountDataStream {
    Table,
    StreamToken,
    UserId,
    RoomId,
    EventType,
}

#[derive(Iden)]
enum AccountData {
    Table,
    UserId,
    EventType,
    RoomId,
}
