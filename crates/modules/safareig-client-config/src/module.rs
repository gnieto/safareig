use std::sync::Arc;

use safareig_core::{
    command::{Container, Module, ServiceCollectionExtension},
    database::DatabaseConnection,
    ddi::{ServiceCollectionExt, ServiceProvider, ServiceResolverExt},
    storage::StorageError,
};
use safareig_sqlx::run_migration;
use safareig_sync_api::SyncExtension;

use crate::{
    command::{
        GetAccountDataCommand, GetAccountRoomDataCommand, SetAccountDataCommand,
        SetAccountRoomDataCommand,
    },
    storage::AccountDataStorage,
    sync::AccountSync,
    AccountDataManager,
};

#[derive(Clone, Default)]
pub struct ClientConfigModule;

impl ClientConfigModule {
    fn create_account_data_storage(db: DatabaseConnection) -> Arc<dyn AccountDataStorage> {
        match db {
            #[cfg(feature = "backend-sqlx")]
            DatabaseConnection::Sqlx(pool) => {
                Arc::new(crate::storage::sqlx::SqlxStorage::new(pool))
            }
            _ => unimplemented!(),
        }
    }
}

#[async_trait::async_trait]
impl<R: safareig_core::command::Router> Module<R> for ClientConfigModule {
    fn register(&self, container: &mut Container, router: &mut R) {
        container
            .service_collection
            .service_factory(|connection: &DatabaseConnection| {
                Ok(ClientConfigModule::create_account_data_storage(
                    connection.clone(),
                ))
            });
        container
            .service_collection
            .register::<AccountDataManager>();

        container.inject_endpoint::<_, GetAccountDataCommand, _>(router);
        container.inject_endpoint::<_, SetAccountDataCommand, _>(router);
        container.inject_endpoint::<_, GetAccountRoomDataCommand, _>(router);
        container.inject_endpoint::<_, SetAccountRoomDataCommand, _>(router);

        container.service_collection.service_factory_var(
            "client-config",
            |storage: &Arc<dyn AccountDataStorage>| {
                let sync: Arc<dyn SyncExtension> = Arc::new(AccountSync::new(storage.clone()));

                Ok(sync)
            },
        );
    }

    async fn run_migrations(&self, container: &ServiceProvider) -> Result<(), StorageError> {
        #[cfg(feature = "backend-sqlx")]
        {
            let db = container.get::<DatabaseConnection>().unwrap();
            run_migration(db, "sqlite", "safareig-client-config").await?;
        }

        Ok(())
    }
}
