use safareig_core::{
    events::Content,
    pagination::{Pagination, PaginationResponse},
    ruma::{
        events::{GlobalAccountDataEventType, RoomAccountDataEventType},
        OwnedRoomId, RoomId, UserId,
    },
    storage::StorageError,
    StreamToken,
};
use serde::{Deserialize, Serialize};

pub mod sqlx;

#[derive(Serialize, Deserialize, Debug)]
pub enum AccountDataEvent {
    Room(OwnedRoomId, Content),
    Global(Content),
}

impl AccountDataEvent {
    pub fn content(self) -> Content {
        match self {
            AccountDataEvent::Room(_, content) => content,
            AccountDataEvent::Global(content) => content,
        }
    }
}

#[async_trait::async_trait]
pub trait AccountDataStorage: Send + Sync {
    async fn update_account_data(
        &self,
        user: &UserId,
        event: AccountDataEvent,
    ) -> Result<StreamToken, StorageError>;

    async fn get_account_event(
        &self,
        user: &UserId,
        event_type: &GlobalAccountDataEventType,
    ) -> Result<Option<Content>, StorageError>;

    async fn get_room_account_event(
        &self,
        user: &UserId,
        room: &RoomId,
        event_type: &RoomAccountDataEventType,
    ) -> Result<Option<Content>, StorageError>;

    async fn account_data_properties(
        &self,
        user: &UserId,
        room: Option<&RoomId>,
    ) -> Result<u32, StorageError>;

    // TODO: user <-> query
    async fn account_data_stream(
        &self,
        user: &UserId,
        query: &Pagination<StreamToken, String>,
    ) -> Result<PaginationResponse<StreamToken, AccountDataEvent>, StorageError>;
    async fn account_data_head(&self, user_id: &UserId) -> Result<StreamToken, StorageError>;
}
