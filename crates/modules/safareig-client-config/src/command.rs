use safareig_core::{
    ruma::{
        api::client::error::{ErrorBody, ErrorKind},
        exports::http::StatusCode,
    },
    storage::StorageError,
};
use thiserror::Error;

mod get_account_data;
mod get_account_room_data;
mod set_account_data;
mod set_account_room_data;

pub use get_account_data::GetAccountDataCommand;
pub use get_account_room_data::GetAccountRoomDataCommand;
pub use set_account_data::SetAccountDataCommand;
pub use set_account_room_data::SetAccountRoomDataCommand;

const BANNED_ACCOUNT_DATA_TYPES: &[&str] = &["m.fully_read"];

#[derive(Error, Debug)]
pub enum AccountDataError {
    #[error("target type can not be updated from client side")]
    BannedEventType,
    #[error("given event could not be converted to basic event")]
    InvalidEvent,
    #[error("target key {0} missing on account data")]
    MissingKey(String),
    #[error("too many properties on account data")]
    TooManyProperties,
    #[error("storage error: {0}")]
    StorageError(#[from] StorageError),
    #[error("serialization error: {0}")]
    Serde(#[from] serde_json::Error),
}

impl From<AccountDataError> for safareig_core::ruma::api::client::Error {
    fn from(account_error: AccountDataError) -> Self {
        match account_error {
            AccountDataError::BannedEventType => safareig_core::ruma::api::client::Error {
                status_code: StatusCode::FORBIDDEN,
                body: ErrorBody::Standard {
                    kind: ErrorKind::forbidden(),
                    message: account_error.to_string(),
                },
            },
            AccountDataError::InvalidEvent => safareig_core::ruma::api::client::Error {
                status_code: StatusCode::BAD_REQUEST,
                body: ErrorBody::Standard {
                    kind: ErrorKind::BadJson,
                    message: account_error.to_string(),
                },
            },
            AccountDataError::MissingKey(_) => safareig_core::ruma::api::client::Error {
                status_code: StatusCode::NOT_FOUND,
                body: ErrorBody::Standard {
                    kind: ErrorKind::NotFound,
                    message: account_error.to_string(),
                },
            },
            AccountDataError::TooManyProperties => safareig_core::ruma::api::client::Error {
                status_code: StatusCode::BAD_REQUEST,
                body: ErrorBody::Standard {
                    kind: ErrorKind::forbidden(),
                    message: account_error.to_string(),
                },
            },
            AccountDataError::StorageError(_) | AccountDataError::Serde(_) => {
                safareig_core::ruma::api::client::Error {
                    status_code: StatusCode::INTERNAL_SERVER_ERROR,
                    body: ErrorBody::Standard {
                        kind: ErrorKind::Unknown,
                        message: account_error.to_string(),
                    },
                }
            }
        }
    }
}
