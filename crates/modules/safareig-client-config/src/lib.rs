use std::{str::FromStr, sync::Arc};

use command::AccountDataError;
use safareig_core::{
    bus::{Bus, BusMessage},
    config::HomeserverConfig,
    events::Content,
    ruma::{
        events::{
            direct::DirectEventContent, fully_read::FullyReadEventContent,
            identity_server::IdentityServerEventContent,
            ignored_user_list::IgnoredUserListEventContent, push_rules::PushRulesEventContent,
            secret_storage::default_key::SecretStorageDefaultKeyEventContent, tag::TagEventContent,
            AnyGlobalAccountDataEvent, EventContentFromType, GlobalAccountDataEventType,
            RoomAccountDataEventType, StaticEventContent,
        },
        UserId,
    },
    Inject,
};
use serde::de::DeserializeOwned;
use serde_json::value::RawValue;
use storage::{AccountDataEvent, AccountDataStorage};

pub mod command;
mod module;
pub mod storage;
pub mod sync;

pub use module::ClientConfigModule;

pub fn build_global_account_data_event(
    event_type: &str,
    data: Box<RawValue>,
) -> Result<AnyGlobalAccountDataEvent, AccountDataError> {
    let json = serde_json::json!({
        "type": event_type,
        "content": data,
    });
    let event: AnyGlobalAccountDataEvent =
        serde_json::from_value(json).map_err(|_| AccountDataError::InvalidEvent)?;

    Ok(event)
}

pub fn account_data_stream(user: &UserId) -> String {
    format!("account:{user}")
}

#[derive(Clone, Inject)]
pub struct AccountDataManager {
    account_data: Arc<dyn AccountDataStorage>,
    bus: Bus,
    config: Arc<HomeserverConfig>,
}

impl AccountDataManager {
    pub async fn update_account_data(
        &self,
        user: &UserId,
        account_data: AccountDataEvent,
    ) -> Result<(), AccountDataError> {
        let current_amount = self
            .account_data
            .account_data_properties(user, None)
            .await?;

        if current_amount >= self.config.account.account_data_max_size {
            return Err(AccountDataError::TooManyProperties);
        }

        let _ = self
            .account_data
            .update_account_data(user, account_data)
            .await?;
        self.bus
            .send_message(BusMessage::AccountData(user.to_owned()));

        Ok(())
    }

    pub async fn get_account_event(
        &self,
        user: &UserId,
        event_type: &GlobalAccountDataEventType,
    ) -> Result<Option<Content>, AccountDataError> {
        Ok(self
            .account_data
            .get_account_event(user, event_type)
            .await?)
    }

    pub async fn get_account_event_content<E: StaticEventContent + DeserializeOwned>(
        &self,
        user: &UserId,
    ) -> Result<Option<E>, AccountDataError> {
        let content = self
            .account_data
            .get_account_event(user, &GlobalAccountDataEventType::from(E::TYPE))
            .await?;

        let event_content = content.map(|c| c.to_event_content()).transpose()?;

        Ok(event_content)
    }
}

// Try convert the given payload depending on the event type.
// Since Ruma does not offer a deserialization layer anymore, we've created this new enum which will be used
// to ensure that the give content matches with the given types.
// This will cause some maintainability issues because we will need to keep a mapping of event types  to event contents.
pub enum SafareigGlobalEventContent {
    Direct(Box<DirectEventContent>),
    IdentityServer(Box<IdentityServerEventContent>),
    IgnoredUserList(Box<IgnoredUserListEventContent>),
    PushRules(Box<PushRulesEventContent>),
    SecretStorageDefaultKey(Box<SecretStorageDefaultKeyEventContent>),
    Custom(Content),
}

impl SafareigGlobalEventContent {
    pub fn from_parts(
        kind: &GlobalAccountDataEventType,
        json: &RawValue,
    ) -> Result<Self, serde_json::Error> {
        let event_content = match kind {
            GlobalAccountDataEventType::Direct => SafareigGlobalEventContent::Direct(Box::new(
                DirectEventContent::from_parts(&kind.to_string(), json)?,
            )),
            GlobalAccountDataEventType::IdentityServer => {
                SafareigGlobalEventContent::IdentityServer(Box::new(
                    IdentityServerEventContent::from_parts(&kind.to_string(), json)?,
                ))
            }
            GlobalAccountDataEventType::IgnoredUserList => {
                SafareigGlobalEventContent::IgnoredUserList(Box::new(
                    IgnoredUserListEventContent::from_parts(&kind.to_string(), json)?,
                ))
            }
            GlobalAccountDataEventType::PushRules => SafareigGlobalEventContent::PushRules(
                Box::new(PushRulesEventContent::from_parts(&kind.to_string(), json)?),
            ),
            GlobalAccountDataEventType::SecretStorageDefaultKey => {
                SafareigGlobalEventContent::SecretStorageDefaultKey(Box::new(
                    SecretStorageDefaultKeyEventContent::from_parts(&kind.to_string(), json)?,
                ))
            }
            _ => {
                // TODO: GlobalAccountDataEventType::SecretStorageKey is unvalidated right now. See below:
                // TODO: This is needed because element web sends padded base64 for IV and MAC fields
                // The spec does not mention these fields needs to be unpadded, so I'm not sure if this is correct or not
                // TODO: Clarify if we should go back and use the official ruma types or it needs to be fixed there to allow accepting
                // the padded base64 strings.

                let value = serde_json::Value::from_str(json.get())?;
                let content = Content::new(value, kind.to_string());

                tracing::debug!(?kind, "not validating content for event type");
                SafareigGlobalEventContent::Custom(content)
            }
        };

        Ok(event_content)
    }
}

pub enum SafareigRoomEventContent {
    FullyRead(Box<FullyReadEventContent>),
    Tag(Box<TagEventContent>),
    Custom(Content),
}

impl SafareigRoomEventContent {
    pub fn from_parts(
        kind: &RoomAccountDataEventType,
        json: &RawValue,
    ) -> Result<Self, serde_json::Error> {
        let event_content = match kind {
            RoomAccountDataEventType::FullyRead => SafareigRoomEventContent::FullyRead(Box::new(
                FullyReadEventContent::from_parts(&kind.to_string(), json)?,
            )),
            RoomAccountDataEventType::Tag => SafareigRoomEventContent::Tag(Box::new(
                TagEventContent::from_parts(&kind.to_string(), json)?,
            )),
            _ => {
                let value = serde_json::Value::from_str(json.get())?;
                let content = Content::new(value, kind.to_string());

                tracing::debug!(?kind, "not validating content for event type");
                SafareigRoomEventContent::Custom(content)
            }
        };

        Ok(event_content)
    }
}
