use async_trait::async_trait;
use safareig_core::{
    auth::Identity, command::Command, events::Content,
    ruma::api::client::config::set_room_account_data, Inject,
};

use super::{AccountDataError, BANNED_ACCOUNT_DATA_TYPES};
use crate::{storage::AccountDataEvent, AccountDataManager, SafareigRoomEventContent};

#[derive(Inject)]
pub struct SetAccountRoomDataCommand {
    account_data: AccountDataManager,
}

#[async_trait]
impl Command for SetAccountRoomDataCommand {
    type Input = set_room_account_data::v3::Request;
    type Output = set_room_account_data::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        id.check_user(&input.user_id)?;
        // TODO: Verify that user has visibility to target room. This is not possible right now because
        // it depends on RoomLoader/Room which are not decoupled yet from safareig. Introducing this
        // dependency right now, leads to a crate circular dependency.
        let event_type = input.event_type.to_string();

        if BANNED_ACCOUNT_DATA_TYPES.contains(&event_type.to_string().as_str()) {
            return Err(AccountDataError::BannedEventType.into());
        }

        let _ = SafareigRoomEventContent::from_parts(&input.event_type, input.data.json())
            .map_err(AccountDataError::Serde)?;

        let content = Content::new(
            serde_json::from_str(input.data.json().get()).map_err(AccountDataError::Serde)?,
            input.event_type.to_string(),
        );

        let account_data = AccountDataEvent::Room(input.room_id, content);
        self.account_data
            .update_account_data(id.user(), account_data)
            .await?;

        Ok(set_room_account_data::v3::Response {})
    }
}
