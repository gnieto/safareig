use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    command::Command,
    ruma::{
        api::client::{
            config::get_global_account_data,
            error::{ErrorBody, ErrorKind},
        },
        exports::http::StatusCode,
        serde::Raw,
    },
    Inject,
};

use super::AccountDataError;
use crate::storage::AccountDataStorage;

#[derive(Inject)]
pub struct GetAccountDataCommand {
    account_data: Arc<dyn AccountDataStorage>,
}

#[async_trait]
impl Command for GetAccountDataCommand {
    type Input = get_global_account_data::v3::Request;
    type Output = get_global_account_data::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        id.check_user(&input.user_id)?;

        let event = self
            .account_data
            .get_account_event(id.user(), &input.event_type)
            .await?
            .map(|content| content.content().clone())
            .ok_or_else(|| AccountDataError::MissingKey(input.event_type.to_string()))?;

        let event = Raw::new(&event).map_err(|e| safareig_core::ruma::api::client::Error {
            status_code: StatusCode::INTERNAL_SERVER_ERROR,
            body: ErrorBody::Standard {
                kind: ErrorKind::BadJson,
                message: e.to_string(),
            },
        })?;

        Ok(get_global_account_data::v3::Response::new(event.cast()))
    }
}
