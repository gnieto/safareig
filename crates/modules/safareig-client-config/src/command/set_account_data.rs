use async_trait::async_trait;
use safareig_core::{
    auth::Identity, command::Command, error::error_chain, events::Content,
    ruma::api::client::config::set_global_account_data, Inject,
};

use super::{AccountDataError, BANNED_ACCOUNT_DATA_TYPES};
use crate::{storage::AccountDataEvent, AccountDataManager, SafareigGlobalEventContent};

#[derive(Inject)]
pub struct SetAccountDataCommand {
    account_data: AccountDataManager,
}

#[async_trait]
impl Command for SetAccountDataCommand {
    type Input = set_global_account_data::v3::Request;
    type Output = set_global_account_data::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        id.check_user(&input.user_id)?;
        let event_type = input.event_type.to_string();

        if BANNED_ACCOUNT_DATA_TYPES.contains(&event_type.to_string().as_str()) {
            return Err(AccountDataError::BannedEventType.into());
        }

        let _ = SafareigGlobalEventContent::from_parts(&input.event_type, input.data.json())
            .map_err(|e| {
                tracing::error!(
                    trace = &error_chain(&e),
                    "Received invalid event type and input data",
                );

                AccountDataError::Serde(e)
            })?;

        let content = Content::new(
            serde_json::from_str(input.data.json().get()).map_err(AccountDataError::Serde)?,
            input.event_type.to_string(),
        );

        let account_data = AccountDataEvent::Global(content);
        self.account_data
            .update_account_data(id.user(), account_data)
            .await?;

        Ok(set_global_account_data::v3::Response::new())
    }
}
