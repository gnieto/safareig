use safareig_core::{command::Command, ruma::api::client::device::get_devices};
use safareig_devices::command::ListAllDevicesCommand;
use safareig_testing::TestingApp;

#[tokio::test]
async fn get_all_user_devices() {
    let cfg = TestingApp::default_config();
    let db = TestingApp::with_config(cfg).await;
    let user = db.register().await;
    let _ = db.login(user.user(), "pwd").await.unwrap();
    let _ = db.login(user.user(), "pwd").await.unwrap();

    let cmd = db.command::<ListAllDevicesCommand>();

    let result = cmd
        .execute(get_devices::v3::Request::new(), user.clone())
        .await
        .unwrap();

    assert_eq!(3, result.devices.len());
}
