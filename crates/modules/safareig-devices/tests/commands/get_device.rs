use http::StatusCode;
use safareig_core::{command::Command, ruma::api::client::device::get_device};
use safareig_devices::command::GetDeviceCommand;
use safareig_testing::TestingApp;

#[tokio::test]
async fn get_device_user() {
    let db = TestingApp::default().await;
    let user = db.register().await;
    let login_response = db.login(user.user(), "pwd").await.unwrap();
    let cmd = db.command::<GetDeviceCommand>();

    let request = get_device::v3::Request {
        device_id: login_response.device_id.clone(),
    };
    let result = cmd.execute(request, user.clone()).await.unwrap();

    assert_eq!(login_response.device_id, result.device.device_id);
}

#[tokio::test]
async fn get_device_from_another_user_returns_not_found() {
    let db = TestingApp::default().await;
    let user = db.register().await;
    let another_user = TestingApp::new_identity();
    let cmd = db.command::<GetDeviceCommand>();

    let request = get_device::v3::Request {
        device_id: user.device().unwrap().to_owned(),
    };
    let result = cmd.execute(request, another_user.clone()).await;

    assert!(result.is_err());
    let err = result.err().unwrap();
    assert_eq!(err.status_code, StatusCode::NOT_FOUND);
}
