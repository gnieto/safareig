use http::StatusCode;
use safareig_core::{
    command::Command,
    ruma::api::client::device::{get_device, update_device},
};
use safareig_devices::command::{GetDeviceCommand, UpdateDeviceCommand};
use safareig_testing::TestingApp;

#[tokio::test]
async fn update_a_user_device() {
    let db = TestingApp::default().await;
    let user = db.register().await;
    let device_id = user.device().unwrap();

    let cmd = db.command::<UpdateDeviceCommand>();

    let request = update_device::v3::Request {
        device_id: device_id.to_owned(),
        display_name: Some("new display name".to_string()),
    };
    let _ = cmd.execute(request, user.clone()).await.unwrap();

    let cmd = db.command::<GetDeviceCommand>();
    let device = get_device::v3::Request {
        device_id: device_id.to_owned(),
    };

    let result = cmd.execute(device, user.clone()).await.unwrap();
    assert_eq!("new display name", result.device.display_name.unwrap());
}

#[tokio::test]
async fn update_device_not_owned_by_the_user() {
    let db = TestingApp::default().await;
    let user = db.register().await;
    let new_user = TestingApp::new_identity();
    let device_id = user.device().unwrap();

    let cmd = db.command::<UpdateDeviceCommand>();

    let request = update_device::v3::Request {
        device_id: device_id.to_owned(),
        display_name: Some("new display name".to_string()),
    };
    let response = cmd.execute(request, new_user).await;

    assert!(response.is_err());
    let err = response.err().unwrap();
    assert_eq!(err.status_code, StatusCode::NOT_FOUND);
}

#[tokio::test]
async fn update_an_unkown_device() {
    let db = TestingApp::default().await;
    let user = db.register().await;

    let cmd = db.command::<UpdateDeviceCommand>();

    let request = update_device::v3::Request {
        device_id: "non existing device".into(),
        display_name: Some("new display name".to_string()),
    };
    let response = cmd.execute(request, user).await;

    assert!(response.is_err());
    let err = response.err().unwrap();
    assert_eq!(err.status_code, StatusCode::NOT_FOUND);
}
