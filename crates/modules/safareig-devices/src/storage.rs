use async_trait::async_trait;
use safareig_core::{
    ruma::{DeviceId, OwnedDeviceId, OwnedUserId, UserId},
    storage::StorageError,
    StreamToken,
};
use serde::{Deserialize, Serialize};

#[cfg(feature = "backend-sqlx")]
pub mod sqlx;

#[async_trait]
pub trait DeviceStorage: Send + Sync {
    async fn get_device(
        &self,
        user_id: &UserId,
        device_id: &DeviceId,
    ) -> Result<Option<Device>, StorageError>;
    async fn devices_by_user(&self, user_id: &UserId) -> Result<Vec<Device>, StorageError>;
    async fn update_device(&self, device: &Device) -> Result<(), StorageError>;
    async fn delete_device(&self, device: &Device) -> Result<(), StorageError>;
    async fn assign(
        &self,
        auth: String,
        user_id: &UserId,
        device_id: &DeviceId,
    ) -> Result<(), StorageError>;
    async fn get_auth(
        &self,
        auth: String,
    ) -> Result<Option<(OwnedUserId, OwnedDeviceId)>, StorageError>;
    async fn revoke_auth(&self, auth: String) -> Result<(), StorageError>;
}

#[derive(Serialize, Deserialize)]
pub struct Device {
    pub ruma_device: safareig_core::ruma::api::client::device::Device,
    pub current_auth: Option<String>,
    pub refresh_token: Option<String>,
    pub user: OwnedUserId,
}

#[async_trait]
pub trait DeviceListStorage: Send + Sync {
    async fn update_device_list(
        &self,
        user: &UserId,
        state: DeviceListState,
    ) -> Result<(), StorageError>;
    async fn increase_device_list_id(&self, user: &UserId)
        -> Result<DeviceListState, StorageError>;
    async fn device_list_state(&self, user: &UserId) -> Result<DeviceListState, StorageError>;
}

#[derive(Serialize, Deserialize)]
pub struct DeviceListState {
    pub stream_id: StreamToken,
    pub stale: bool,
}

impl Default for DeviceListState {
    fn default() -> Self {
        Self {
            stream_id: StreamToken::horizon(),
            stale: false,
        }
    }
}

impl From<StreamToken> for DeviceListState {
    fn from(stream_id: StreamToken) -> Self {
        DeviceListState {
            stream_id,
            stale: false,
        }
    }
}
