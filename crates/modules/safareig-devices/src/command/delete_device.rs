use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    ruma::api::client::{device::delete_device, uiaa::UiaaResponse},
    Inject,
};
use safareig_uiaa::{SessionState, UiaaCommand};

use crate::service::DeviceManager;

#[derive(Inject)]
pub struct DeleteDeviceCommand {
    devices: Arc<DeviceManager>,
}

#[async_trait]
impl UiaaCommand for DeleteDeviceCommand {
    type Input = delete_device::v3::Request;
    type Output = delete_device::v3::Response;
    type Error = safareig_core::ruma::api::client::uiaa::UiaaResponse;

    async fn execute(
        &self,
        input: Self::Input,
        id: Identity,
        _: Option<SessionState>,
    ) -> Result<Self::Output, Self::Error> {
        self.devices
            .delete_device(id.user(), &input.device_id)
            .await
            .map_err(|e| UiaaResponse::MatrixError(e.into()))?;

        Ok(delete_device::v3::Response {})
    }
}
