use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity, command::Command, ruma::api::client::device::get_devices, Inject,
};

use crate::storage::DeviceStorage;

#[derive(Inject)]
pub struct ListAllDevicesCommand {
    devices: Arc<dyn DeviceStorage>,
}

#[async_trait]
impl Command for ListAllDevicesCommand {
    type Input = get_devices::v3::Request;
    type Output = get_devices::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, _: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let devices = self
            .devices
            .devices_by_user(id.user())
            .await?
            .into_iter()
            .map(|d| d.ruma_device)
            .collect();

        Ok(get_devices::v3::Response { devices })
    }
}
