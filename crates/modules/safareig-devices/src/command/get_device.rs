use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity, command::Command, error::ResourceNotFound,
    ruma::api::client::device::get_device, Inject,
};

use crate::service::DeviceManager;

#[derive(Inject)]
pub struct GetDeviceCommand {
    devices: Arc<DeviceManager>,
}

#[async_trait]
impl Command for GetDeviceCommand {
    type Input = get_device::v3::Request;
    type Output = get_device::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let device = self
            .devices
            .get_device(id.user(), &input.device_id)
            .await?
            .map(|d| d.ruma_device)
            .ok_or(ResourceNotFound)?;

        Ok(get_device::v3::Response { device })
    }
}
