use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity, command::Command, error::ResourceNotFound,
    ruma::api::client::device::update_device, Inject,
};

use crate::service::DeviceManager;

#[derive(Inject)]
pub struct UpdateDeviceCommand {
    device_manager: Arc<DeviceManager>,
}

#[async_trait]
impl Command for UpdateDeviceCommand {
    type Input = update_device::v3::Request;
    type Output = update_device::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let mut device = self
            .device_manager
            .get_device(id.user(), &input.device_id)
            .await?
            .ok_or(ResourceNotFound)?;

        id.check_user(&device.user)?;
        device.ruma_device.display_name = input.display_name.clone();
        self.device_manager
            .update_device(id.user(), &device)
            .await?;

        Ok(update_device::v3::Response {})
    }
}
