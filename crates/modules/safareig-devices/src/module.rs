use std::sync::Arc;

use safareig_core::{
    command::{Container, Module, Router, ServiceCollectionExtension},
    database::DatabaseConnection,
    ddi::{ServiceCollectionExt, ServiceProvider, ServiceResolverExt},
    storage::StorageError,
};
use safareig_uiaa::module::ContainerUIAAExt;

use crate::{
    command::{
        get_device::GetDeviceCommand, list_devices::ListAllDevicesCommand,
        update_device::UpdateDeviceCommand, DeleteDeviceCommand, DeleteDevicesCommand,
    },
    service::DeviceManager,
    storage::{DeviceListStorage, DeviceStorage},
};

#[derive(Default)]
pub struct DeviceModule {}

impl DeviceModule {
    fn create_device_storage(db: DatabaseConnection) -> Arc<dyn DeviceStorage> {
        match db {
            #[cfg(feature = "backend-sqlx")]
            DatabaseConnection::Sqlx(pool) => {
                Arc::new(crate::storage::sqlx::DeviceSqliteStorage { pool })
            }
            _ => unimplemented!(""),
        }
    }

    fn create_device_list_storage(db: DatabaseConnection) -> Arc<dyn DeviceListStorage> {
        match db {
            #[cfg(feature = "backend-sqlx")]
            DatabaseConnection::Sqlx(pool) => {
                Arc::new(crate::storage::sqlx::DeviceSqliteStorage { pool })
            }
            _ => unimplemented!(""),
        }
    }
}

#[async_trait::async_trait]
impl<R: Router> Module<R> for DeviceModule {
    fn register(&self, container: &mut Container, router: &mut R) {
        container
            .service_collection
            .service_factory(|db: &DatabaseConnection| Ok(Self::create_device_storage(db.clone())));
        container
            .service_collection
            .service_factory(|db: &DatabaseConnection| {
                Ok(Self::create_device_list_storage(db.clone()))
            });

        container.service_collection.register_arc::<DeviceManager>();
        container.inject_endpoint::<_, GetDeviceCommand, _>(router);
        container.inject_endpoint::<_, ListAllDevicesCommand, _>(router);
        container.inject_endpoint::<_, UpdateDeviceCommand, _>(router);
        container.uiaa_endpoint::<_, DeleteDeviceCommand, _>("device", router);
        container.uiaa_endpoint::<_, DeleteDevicesCommand, _>("device", router);
    }

    async fn run_migrations(&self, container: &ServiceProvider) -> Result<(), StorageError> {
        #[cfg(feature = "backend-sqlx")]
        {
            let db = container.get::<DatabaseConnection>()?;
            safareig_sqlx::run_migration(db, "sqlite", "safareig-devices").await?;
        }

        Ok(())
    }
}
