use std::{
    str::FromStr,
    time::{Duration, UNIX_EPOCH},
};

use async_trait::async_trait;
use safareig_core::{
    ruma::{api::client::device, DeviceId, OwnedDeviceId, OwnedUserId, UserId},
    storage::{Result, StorageError},
    time::system_time_to_millis,
    StreamToken,
};
use sqlx::{sqlite::SqliteRow, Row, Sqlite};

use super::{Device, DeviceListState, DeviceListStorage, DeviceStorage};

pub struct DeviceSqliteStorage {
    pub pool: sqlx::Pool<Sqlite>,
}

#[async_trait]
impl DeviceStorage for DeviceSqliteStorage {
    async fn get_device(&self, user_id: &UserId, device_id: &DeviceId) -> Result<Option<Device>> {
        sqlx::query("SELECT * FROM devices WHERE user_id = ? AND id = ?")
            .bind(user_id.as_str())
            .bind(device_id.as_str())
            .fetch_optional(&self.pool)
            .await?
            .map(TryFrom::try_from)
            .transpose()
    }

    async fn devices_by_user(&self, user_id: &UserId) -> Result<Vec<Device>> {
        let results = sqlx::query("SELECT * FROM devices WHERE user_id = ?")
            .bind(user_id.as_str())
            .fetch_all(&self.pool)
            .await?;

        let devices = results
            .into_iter()
            .map(TryFrom::try_from)
            .collect::<Result<Vec<Device>>>()?;

        Ok(devices)
    }

    async fn update_device(&self, device: &Device) -> Result<()> {
        let device_id = device.ruma_device.device_id.as_str();
        let user_id = device.user.as_str();
        let ts_as_secs = device
            .ruma_device
            .last_seen_ts
            .map(|time| time.as_secs())
            .map(u32::try_from)
            .transpose()
            .map_err(|_| StorageError::Custom("invalid time".to_string()))?;

        sqlx::query(
            r#"
        INSERT INTO devices (id, user_id, token, display_name, last_seen_ip, last_seen_ts)
          VALUES(?1, ?6, ?2, ?3, ?4, ?5)
         ON CONFLICT(id, user_id)
         DO UPDATE SET token=?2, display_name=?3, last_seen_ip=?4, last_seen_ts=?5;
        "#,
        )
        .bind(device_id)
        .bind(device.current_auth.as_ref())
        .bind(device.ruma_device.display_name.as_ref())
        .bind(device.ruma_device.last_seen_ip.as_ref())
        .bind(ts_as_secs)
        .bind(user_id)
        .execute(&self.pool)
        .await?;

        Ok(())
    }

    async fn delete_device(&self, device: &Device) -> Result<()> {
        let device_id = device.ruma_device.device_id.as_str();

        sqlx::query(
            r#"
            DELETE FROM devices
            WHERE id = ?1
        "#,
        )
        .bind(device_id)
        .execute(&self.pool)
        .await?;

        Ok(())
    }

    async fn assign(&self, auth: String, user_id: &UserId, device_id: &DeviceId) -> Result<()> {
        let device_id = device_id.as_str();
        let user_id = user_id.as_str();

        let query = sqlx::query(
            r#"
            UPDATE devices
                SET token = ?3
            WHERE id = ?1 AND user_id = ?2
        "#,
        )
        .bind(device_id)
        .bind(user_id)
        .bind(auth);
        query.execute(&self.pool).await?;

        Ok(())
    }

    async fn get_auth(&self, auth: String) -> Result<Option<(OwnedUserId, OwnedDeviceId)>> {
        sqlx::query("SELECT id, user_id FROM devices WHERE token = ?")
            .bind(auth)
            .fetch_optional(&self.pool)
            .await?
            .map(row_to_auth_tuple)
            .transpose()
    }

    async fn revoke_auth(&self, auth: String) -> Result<()> {
        let query = sqlx::query(
            r#"
            UPDATE devices
                SET token = NULL
            WHERE token = ?
        "#,
        )
        .bind(auth);
        query.execute(&self.pool).await?;

        Ok(())
    }
}

fn row_to_auth_tuple(row: SqliteRow) -> Result<(OwnedUserId, OwnedDeviceId)> {
    let user_id = OwnedUserId::from_str(row.try_get::<&str, _>("user_id")?).unwrap();
    let device_id = OwnedDeviceId::from(row.try_get::<&str, _>("id")?);

    Ok((user_id, device_id))
}

impl TryFrom<SqliteRow> for Device {
    type Error = safareig_core::storage::StorageError;

    fn try_from(row: SqliteRow) -> Result<Self> {
        let user_id = OwnedUserId::from_str(row.try_get::<&str, _>("user_id")?).unwrap();
        let device_id = OwnedDeviceId::from(row.try_get::<&str, _>("id")?);
        let token = row.try_get::<Option<String>, _>("token")?;
        let refresh_token = row.try_get::<Option<String>, _>("refresh_token")?;
        let display = row.try_get::<Option<String>, _>("display_name")?;
        let last_ip = row.try_get::<Option<String>, _>("last_seen_ip")?;
        let last_ts = row
            .try_get::<Option<u32>, _>("last_seen_ts")?
            .map(|secs| UNIX_EPOCH + Duration::from_secs(secs as u64))
            .map(system_time_to_millis);

        Ok(Device {
            ruma_device: device::Device {
                device_id,
                display_name: display,
                last_seen_ip: last_ip,
                last_seen_ts: last_ts,
            },
            current_auth: token,
            refresh_token,
            user: user_id,
        })
    }
}

#[async_trait]
impl DeviceListStorage for DeviceSqliteStorage {
    async fn update_device_list(&self, user: &UserId, state: super::DeviceListState) -> Result<()> {
        let user = user.as_str().to_string();

        let query = sqlx::query(
            r#"
        INSERT INTO device_list (user_id, stream_id, stale)
          VALUES(?1, ?2, ?3)
         ON CONFLICT(user_id)
         DO UPDATE SET stream_id=?2, stale=?3;
        "#,
        )
        .bind(user)
        .bind(state.stream_id)
        .bind(state.stale);
        query.execute(&self.pool).await?;

        Ok(())
    }

    async fn increase_device_list_id(&self, user: &UserId) -> Result<super::DeviceListState> {
        let user = user.as_str().to_string();

        let device_list = sqlx::query(
            r#"
        UPDATE device_list
        SET stream_id = stream_id + 1
        WHERE user_id = ?
        RETURNING stream_id, stale
        "#,
        )
        .bind(user)
        .fetch_optional(&self.pool)
        .await?
        .map(TryFrom::try_from)
        .transpose()?
        .unwrap_or_default();

        Ok(device_list)
    }

    async fn device_list_state(&self, user: &UserId) -> Result<super::DeviceListState> {
        let user = user.as_str().to_string();

        let device_list = sqlx::query(
            r#"
            SELECT stream_id, stale FROM device_list
            WHERE user_id = ?
        "#,
        )
        .bind(user)
        .fetch_optional(&self.pool)
        .await?
        .map(TryFrom::try_from)
        .transpose()?
        .unwrap_or_default();

        Ok(device_list)
    }
}

impl TryFrom<SqliteRow> for super::DeviceListState {
    type Error = StorageError;

    fn try_from(row: SqliteRow) -> std::result::Result<Self, Self::Error> {
        let stream_id = row.try_get::<i64, _>("stream_id")?;
        let stale = row.try_get("stale")?;

        Ok(DeviceListState {
            stream_id: StreamToken::from(stream_id.try_into().unwrap_or(0)),
            stale,
        })
    }
}
