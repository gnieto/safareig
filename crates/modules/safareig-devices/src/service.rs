use std::sync::Arc;

use safareig_core::{
    config::HomeserverConfig,
    ruma::{DeviceId, UserId},
    storage::StorageError,
    Inject,
};

use crate::storage::{Device, DeviceListStorage, DeviceStorage};

#[derive(Inject)]
pub struct DeviceManager {
    config: Arc<HomeserverConfig>,
    devices: Arc<dyn DeviceStorage>,
    device_list: Arc<dyn DeviceListStorage>,
    // ephemeral: EphemeralStream,
}

impl DeviceManager {
    pub async fn get_device(
        &self,
        user: &UserId,
        device_id: &DeviceId,
    ) -> Result<Option<Device>, StorageError> {
        self.devices.get_device(user, device_id).await
    }

    pub async fn update_device(&self, user: &UserId, device: &Device) -> Result<(), StorageError> {
        self.devices.update_device(device).await?;

        // TODO: Consider converting DeviceManager into a trait and put the following code into a Federation-specific struct
        if self.config.federation.enabled {
            let device_list_state = self.device_list.increase_device_list_id(user).await?;

            if !device_list_state.stale {
                /*let event = EphemeralEvent::DeviceUpdate(
                    user.to_owned(),
                    device.ruma_device.device_id.to_owned(),
                    device_list_state.stream_id,
                );*/
                // self.ephemeral.push_event(event);
            }
        }

        Ok(())
    }

    pub async fn delete_device(
        &self,
        user: &UserId,
        device: &DeviceId,
    ) -> Result<(), StorageError> {
        let device = match self.get_device(user, device).await? {
            Some(device) => device,
            None => return Ok(()),
        };

        if let Some(ref auth) = device.current_auth {
            self.devices.revoke_auth(auth.to_string()).await?;
        };

        self.devices.delete_device(&device).await?;

        // TODO: Consider converting DeviceManager into a trait and put the following code into a Federation-specific struct
        if self.config.federation.enabled {
            let device_list_state = self.device_list.increase_device_list_id(user).await?;

            if !device_list_state.stale {
                /*let event = EphemeralEvent::DeviceUpdate(
                    user.to_owned(),
                    device.ruma_device.device_id.to_owned(),
                    device_list_state.stream_id,
                );*/
                // self.ephemeral.push_event(event);
            }
        }

        Ok(())
    }

    pub async fn invalidate_sessions(
        &self,
        user_id: &UserId,
        filter: Option<&DeviceId>,
    ) -> Result<(), StorageError> {
        let all_devices = self.devices.devices_by_user(user_id).await?;

        for mut device in all_devices.into_iter() {
            if Some(device.ruma_device.device_id.as_ref()) != filter {
                self.invalidate_session(&mut device).await?;
            }
        }

        Ok(())
    }

    async fn invalidate_session(&self, device: &mut Device) -> Result<(), StorageError> {
        if let Some(current_auth) = device.current_auth.clone() {
            self.devices.revoke_auth(current_auth).await?;
        }

        device.current_auth = None;

        self.devices.update_device(device).await?;

        Ok(())
    }
}
