use std::sync::Arc;

use ddi::ServiceCollectionExt;

use crate::{
    bus::Bus,
    command::{Container, Module, Router},
    config::HomeserverConfig,
    database::DatabaseConnection,
    ServerInfo,
};

pub struct CoreModule {
    pub config: Arc<HomeserverConfig>,
    pub db: DatabaseConnection,
    pub server_info: ServerInfo,
    pub bus: Bus,
}

impl CoreModule {
    pub fn new(config: Arc<HomeserverConfig>) -> Self {
        let db = DatabaseConnection::from(config.as_ref());

        CoreModule {
            config: config.clone(),
            db,
            server_info: ServerInfo::new(config.server_name.clone()),
            bus: Bus::default(),
        }
    }

    pub fn database_connection(&self) -> DatabaseConnection {
        self.db.clone()
    }

    pub fn config(&self) -> Arc<HomeserverConfig> {
        self.config.clone()
    }

    pub fn bus(&self) -> Bus {
        self.bus.clone()
    }

    pub fn server_info(&self) -> ServerInfo {
        self.server_info.clone()
    }
}

impl<R: Router> Module<R> for CoreModule {
    fn register(&self, container: &mut Container, _router: &mut R) {
        container.service_collection.service(self.config.clone());
        container
            .service_collection
            .service(self.database_connection());
        container.service_collection.service(self.bus());
        container.service_collection.service(self.server_info());
        container
            .service_collection
            .service(self.config.server_name.clone());
    }
}
