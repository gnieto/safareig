use std::error::Error;

use ruma::{
    api::{
        client::error::{ErrorBody, ErrorKind, StandardErrorBody},
        error::{MatrixError, MatrixErrorBody},
    },
    exports::http::StatusCode,
};

pub struct ResourceNotFound;

impl From<ResourceNotFound> for ruma::api::client::Error {
    fn from(_: ResourceNotFound) -> Self {
        ruma::api::client::Error {
            body: ErrorBody::Standard {
                kind: ErrorKind::NotFound,
                message: "resource not found".to_string(),
            },
            status_code: StatusCode::NOT_FOUND,
        }
    }
}

impl From<ResourceNotFound> for ruma::api::error::MatrixError {
    fn from(error: ResourceNotFound) -> Self {
        let client = ruma::api::client::Error::from(error);
        RumaExt::to_generic(client)
    }
}

pub trait RumaExt {
    fn to_generic(self) -> ruma::api::error::MatrixError;
}

impl<F> RumaExt for F
where
    ruma::api::client::Error: From<F>,
{
    fn to_generic(self) -> ruma::api::error::MatrixError {
        let e = ruma::api::client::Error::from(self);
        let status_code = e.status_code;

        let body = match e.body {
            ErrorBody::Standard { kind, message } => {
                let standard = StandardErrorBody { kind, message };

                MatrixErrorBody::Json(serde_json::to_value(&standard).unwrap())
            }
            ErrorBody::Json(json) => MatrixErrorBody::Json(json),
            ErrorBody::NotJson { bytes, .. } => MatrixErrorBody::from_bytes(bytes.as_ref()),
        };

        MatrixError { status_code, body }
    }
}

pub fn error_chain(error: &impl Error) -> String {
    let mut error_msg = error.to_string();
    let mut next = error.source();

    while let Some(source) = next {
        error_msg.push_str(&format!("\n\t - {source}"));
        next = source.source();
    }

    error_msg
}
