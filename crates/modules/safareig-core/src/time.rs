use std::time::{Duration, SystemTime, UNIX_EPOCH};

use ruma::MilliSecondsSinceUnixEpoch;

pub fn system_time_to_u64(time: SystemTime) -> u64 {
    let duration = time.duration_since(UNIX_EPOCH).unwrap_or_default();

    let seconds = duration.as_secs();
    let millis = duration.subsec_millis() as u64;

    (seconds * 1000) + millis
}

pub fn u64_to_system_time(time: u64) -> SystemTime {
    UNIX_EPOCH
        .checked_add(Duration::from_millis(time))
        .unwrap_or(UNIX_EPOCH)
}

pub fn uint_to_system_time(time: &MilliSecondsSinceUnixEpoch) -> SystemTime {
    time.to_system_time()
        .or_else(|| {
            let secs = u64::from(time.as_secs());
            UNIX_EPOCH.checked_add(Duration::from_secs(secs))
        })
        .unwrap_or(UNIX_EPOCH)
}

pub fn system_time_to_millis(time: SystemTime) -> MilliSecondsSinceUnixEpoch {
    MilliSecondsSinceUnixEpoch::from_system_time(time)
        .unwrap_or_else(MilliSecondsSinceUnixEpoch::now)
}
