use ruma::ServerName;

use crate::config::HomeserverConfig;

#[derive(Clone)]
pub struct ServerInfo {
    server_name: ruma::OwnedServerName,
}

impl ServerInfo {
    pub fn new(server_name: ruma::OwnedServerName) -> Self {
        Self { server_name }
    }

    pub fn is_local_server(&self, server_name: &ServerName) -> bool {
        self.server_name() == server_name
    }

    pub fn server_name(&self) -> &ServerName {
        self.server_name.as_ref()
    }
}

impl From<&HomeserverConfig> for ServerInfo {
    fn from(config: &HomeserverConfig) -> Self {
        ServerInfo {
            server_name: config.server_name.clone(),
        }
    }
}
