use ruma::{
    api::client::error::{ErrorBody, ErrorKind},
    exports::http::StatusCode,
    RoomId, ServerName,
};
use thiserror::Error;

pub trait RoomIdExt {
    fn try_server_name(&self) -> Result<&ServerName, IdError>;
}

impl RoomIdExt for RoomId {
    fn try_server_name(&self) -> Result<&ServerName, IdError> {
        self.server_name().ok_or(IdError::MissingServerNameFromRoom)
    }
}

#[derive(Error, Debug)]
pub enum IdError {
    #[error("Missing error name from room")]
    MissingServerNameFromRoom,
}

impl From<IdError> for ruma::api::client::Error {
    fn from(_: IdError) -> Self {
        ruma::api::client::Error {
            body: ErrorBody::Standard {
                kind: ErrorKind::Unknown,
                message: "id error".to_string(),
            },
            status_code: StatusCode::INTERNAL_SERVER_ERROR,
        }
    }
}
