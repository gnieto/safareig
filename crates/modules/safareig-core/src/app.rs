use std::sync::Arc;

use graceful::BackgroundHandler;

use crate::{
    command::{Container, FrozenContainer, Module, Router},
    config::HomeserverConfig,
};

pub struct App<R: Router> {
    container: Container,
    router: R,
    options: AppOptions,
    modules_v2: Vec<Box<dyn Module<R>>>,
}

#[derive(Default)]
pub struct AppOptions {
    pub spawn_worker: Option<BackgroundHandler>,
    pub run_migrations: bool,
}

impl<R: Router> App<R> {
    pub fn new(router: R, opts: AppOptions, config: Arc<HomeserverConfig>) -> Self {
        Self {
            container: Container::new(config),
            router,
            options: opts,
            modules_v2: Vec::new(),
        }
    }

    #[must_use]
    pub fn with_module(mut self, component: Box<dyn Module<R>>) -> Self {
        self.modules_v2.push(component);
        self
    }

    pub async fn take(mut self) -> (FrozenContainer, R) {
        for module in &self.modules_v2 {
            module.register(&mut self.container, &mut self.router);
        }

        let container = self.container.freeze();

        for module in &self.modules_v2 {
            if self.options.run_migrations {
                let result = module.run_migrations(&container.service_provider).await;
                if let Err(e) = result {
                    tracing::error!(err = e.to_string().as_str(), "Error running migration",)
                }
            }

            if let Some(background) = &self.options.spawn_worker {
                module
                    .spawn_workers(&container.service_provider, background)
                    .await
            }
        }

        (container, self.router)
    }
}
