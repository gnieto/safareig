use ruma::{events::TimelineEventType, OwnedDeviceId, OwnedRoomId, OwnedServerName, OwnedUserId};
use tokio::sync::{
    broadcast,
    broadcast::{Receiver, Sender},
};

use crate::StreamToken;

#[derive(Clone)]
pub struct Bus {
    tx: broadcast::Sender<BusMessage>,
}

impl Default for Bus {
    fn default() -> Self {
        Self::new()
    }
}

impl Bus {
    fn new() -> Bus {
        let (tx, _) = broadcast::channel(3_000);

        Bus { tx }
    }

    pub fn receiver(&self) -> Receiver<BusMessage> {
        self.tx.subscribe()
    }

    pub fn send_message(&self, msg: BusMessage) {
        msg.trace();
        let send_result = self.sender().send(msg);

        if let Err(e) = send_result {
            tracing::error!(
                err = e.to_string().as_str(),
                "Could not send a new message trough the bus"
            );
        }
    }

    fn sender(&self) -> Sender<BusMessage> {
        self.tx.clone()
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum BusMessage {
    RoomCreated(OwnedRoomId),
    RoomJoined(OwnedRoomId, OwnedUserId),
    RoomInvited(OwnedRoomId, OwnedUserId, OwnedUserId),
    RoomUpdate {
        room_id: OwnedRoomId,
        kind: TimelineEventType,
        is_state: bool,
    },
    Presence(OwnedUserId),
    StreamUpdate(StreamToken),
    FullyReadMarker(OwnedUserId, OwnedRoomId),
    Marker(OwnedRoomId),
    ServerTracked(OwnedServerName, OwnedRoomId),
    UserRegistered(OwnedUserId),
    Typing(OwnedUserId, OwnedRoomId),
    ToDevice(OwnedUserId, OwnedDeviceId),
    ToDeviceFederation(OwnedServerName),
    DeviceUpdate(OwnedDeviceId),
    AccountData(OwnedUserId),
    E2eeUpdate(OwnedUserId),
    ServerServiceRestored(OwnedServerName),
    UpdatePushers(OwnedUserId),
    Shutdown,
}

impl BusMessage {
    fn trace(&self) {
        match self {
            Self::RoomCreated(room_id) => {
                tracing::debug!(event = "room_created", room_id = room_id.as_str(), "Event")
            }
            Self::RoomJoined(room, user) => {
                tracing::debug!(
                    event = "room_joined",
                    room_id = room.as_str(),
                    user_id = user.as_str(),
                    "Event"
                )
            }
            Self::RoomInvited(room, _, user) => {
                tracing::debug!(
                    event = "room_invited",
                    room_id = room.as_str(),
                    user_id = user.as_str(),
                    "Event"
                )
            }
            Self::AccountData(user) => {
                tracing::debug!(event = "account_data", user_id = user.as_str(), "Event")
            }
            Self::RoomUpdate {
                room_id,
                kind,
                is_state,
            } => {
                tracing::debug!(
                    event = "room_update",
                    room_id = room_id.as_str(),
                    kind = kind.to_string().as_str(),
                    is_state = is_state,
                    "Event"
                )
            }
            Self::Presence(user) => {
                tracing::debug!(event = "presence_update", user_id = user.as_str(), "Event")
            }
            Self::Typing(user, room) => {
                tracing::debug!(
                    event = "typing",
                    user_id = user.as_str(),
                    room = room.as_str(),
                    "Event"
                )
            }
            Self::ServerTracked(server, room) => {
                tracing::debug!(
                    event = "server_tracked",
                    server = server.as_str(),
                    room = room.as_str(),
                    "Event"
                )
            }
            Self::UserRegistered(user) => {
                tracing::debug!(event = "user_registered", user_id = user.as_str(), "Event")
            }
            Self::DeviceUpdate(device) => {
                tracing::debug!(event = "device_update", device = device.as_str(), "Event")
            }
            Self::Shutdown => {
                tracing::debug!(event = "shutdown", "Event")
            }
            Self::UpdatePushers(user) => {
                tracing::debug!(event = "update_pushers", user_id = user.as_str(), "Event")
            }
            _ => {}
        }
    }
}
