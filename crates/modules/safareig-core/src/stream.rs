use std::{fmt::Display, ops::Deref, str::FromStr};

use ruma::UInt;
use serde::{Deserialize, Serialize};

use crate::pagination::Token;

#[derive(Ord, PartialOrd, PartialEq, Eq, Clone, Copy, Serialize, Deserialize, Debug)]
pub struct StreamToken(u64);

impl Token for StreamToken {}
impl Token for rusty_ulid::Ulid {}

impl Display for StreamToken {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl FromStr for StreamToken {
    type Err = std::num::ParseIntError;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        Ok(StreamToken(u64::from_str(s)?))
    }
}

impl From<u64> for StreamToken {
    fn from(token: u64) -> Self {
        StreamToken(token)
    }
}

impl Deref for StreamToken {
    type Target = u64;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl Default for StreamToken {
    fn default() -> Self {
        StreamToken::horizon()
    }
}

impl StreamToken {
    /// Returns a marker value which designates the start of the stream
    #[must_use]
    pub fn horizon() -> Self {
        StreamToken(0)
    }

    /// Returns a marker value which designates the end of the stream
    #[must_use]
    pub fn infinite() -> Self {
        StreamToken(u64::MAX)
    }

    #[must_use]
    pub fn next(&self) -> Self {
        StreamToken(self.0 + 1)
    }

    #[must_use]
    pub fn prev(&self) -> Self {
        StreamToken(self.0.saturating_sub(1))
    }
}

impl From<StreamToken> for UInt {
    fn from(stream_token: StreamToken) -> Self {
        UInt::new_wrapping(stream_token.0)
    }
}

#[cfg(feature = "backend-sqlx")]
impl sqlx::Type<sqlx::Sqlite> for StreamToken {
    fn type_info() -> <sqlx::Sqlite as sqlx::Database>::TypeInfo {
        <i64 as sqlx::Type<sqlx::Sqlite>>::type_info()
    }
}

#[cfg(feature = "backend-sqlx")]
impl<'q> sqlx::Encode<'q, sqlx::Sqlite> for StreamToken {
    fn encode_by_ref(
        &self,
        args: &mut <sqlx::Sqlite as sqlx::database::HasArguments<'q>>::ArgumentBuffer,
    ) -> sqlx::encode::IsNull {
        args.push(sqlx::sqlite::SqliteArgumentValue::Int64(self.0 as i64));

        sqlx::encode::IsNull::No
    }
}
