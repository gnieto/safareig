use std::{
    collections::{HashMap, HashSet},
    path::{Path, PathBuf},
    time::Duration,
};

use ::config::{Config, Environment, File};
use ruma::{OwnedServerName, OwnedUserId, RoomVersionId, UserId};
use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct HomeserverConfig {
    pub server_name: OwnedServerName,
    pub bind: String,
    pub ssl_bind: String,
    pub account: AccountConfig,
    pub federation: FederationConfig,
    pub ssl: SslConfig,
    pub appservices: HashMap<String, PathBuf>,
    pub database: DatabaseConfig,
    pub uiaa: UiaaConfig,
    pub media: MediaConfig,
    #[serde(default)]
    pub monitoring: MonitoringConfig,
    #[serde(default)]
    pub content_reporting: ContentReportingConfig,
    #[serde(default)]
    pub admin: AdminConfig,
    #[serde(default)]
    pub auth: AuthConfig,
    #[serde(default)]
    pub server_side_search: ServerSideSearchConfig,
    #[serde(default)]
    pub rooms: RoomsConfig,
    #[cfg(feature = "msc2965")]
    pub oidc: OidcConfig,
}

impl HomeserverConfig {
    pub fn default_room_version(&self) -> RoomVersionId {
        self.rooms.default_version.to_owned()
    }

    pub fn room_version_for_alias(&self, alias: &str) -> RoomVersionId {
        self.rooms
            .versions
            .iter()
            .find(|config| alias.starts_with(&config.prefix))
            .map(|config| config.version.to_owned())
            .unwrap_or_else(|| self.default_room_version())
    }

    pub fn is_admin(&self, user: &UserId) -> bool {
        self.admin.admins.contains(user)
    }
}

#[cfg(feature = "msc2965")]
#[derive(Debug, Deserialize)]
pub struct OidcConfig {
    pub issuer: String,
}

#[derive(Debug, Deserialize)]
pub struct AccountConfig {
    pub account_data_max_size: u32,
}

#[derive(Debug, Deserialize)]
pub struct UiaaConfig {
    pub register_stages: Vec<String>,
    pub login_stages: Vec<String>,
    #[serde(default = "default_device_stage")]
    pub device_stages: Vec<String>,
    #[serde(default = "default_password_stage")]
    pub password_stages: Vec<String>,
    pub shared_token: Option<String>,
}

fn default_device_stage() -> Vec<String> {
    vec!["m.login.password".to_string()]
}

fn default_password_stage() -> Vec<String> {
    vec!["m.login.password".to_string()]
}

#[derive(Debug, Deserialize)]
pub struct FederationConfig {
    pub skip_insecure: bool,
    pub enabled: bool,
    #[serde(default)]
    pub notary_servers: Vec<OwnedServerName>,
    #[serde(default = "federation_default_rate_limit")]
    pub rate_limit_quota_minute: u32,
    #[serde(default = "federation_default_circuit_breaker_failures")]
    pub circuit_breaker_consecutive_failures: u32,
}

fn federation_default_rate_limit() -> u32 {
    300
}

fn federation_default_circuit_breaker_failures() -> u32 {
    10
}

#[derive(Debug, Deserialize)]
pub struct SslConfig {
    pub enabled: bool,
    pub cert_path: Option<PathBuf>,
    pub key_path: Option<PathBuf>,
}

#[derive(Debug, Deserialize)]
#[serde(tag = "storage")]
#[non_exhaustive]
pub enum DatabaseConfig {
    Sqlite(SqliteOptions),
}

#[derive(Debug, Deserialize)]
#[serde(tag = "reporter")]
#[non_exhaustive]
pub enum ContentReportingConfig {
    None,
    ServerNotice,
}

impl Default for ContentReportingConfig {
    fn default() -> Self {
        Self::None
    }
}

#[derive(Debug, Deserialize, Default)]
pub struct SqliteOptions {
    #[serde(default)]
    pub in_memory: bool,
    #[serde(default)]
    pub path: Option<String>,
    #[serde(default)]
    pub log_statements: bool,
    #[serde(default)]
    pub log_slow: bool,
}

impl SqliteOptions {
    pub fn testing() -> Self {
        SqliteOptions {
            in_memory: true,
            path: None,
            log_statements: false,
            log_slow: false,
        }
    }
}

#[derive(Debug, Deserialize)]
pub struct MediaConfig {
    #[serde(
        with = "serde_humanize_rs",
        default = "MediaConfig::default_max_upload_size"
    )]
    pub max_upload_size: usize,
    #[serde(flatten)]
    pub media_driver: MediaDriver,
}

impl Default for MediaConfig {
    fn default() -> Self {
        MediaConfig {
            max_upload_size: MediaConfig::default_max_upload_size(),
            media_driver: MediaDriver::default(),
        }
    }
}

#[derive(Debug, Deserialize)]
#[serde(tag = "driver")]
#[derive(Default)]
pub enum MediaDriver {
    Filesystem(FilesystemConfig),
    #[default]
    Noop,
}

#[derive(Debug, Deserialize)]
pub struct FilesystemConfig {
    pub path: Option<String>,
}

impl MediaConfig {
    pub fn default_max_upload_size() -> usize {
        10_000_000
    }
}

#[derive(Debug, Deserialize)]
pub struct MonitoringConfig {
    #[serde(default)]
    pub opentelemetry_enabled: bool,
    #[serde(default = "default_svc_name")]
    pub service_name: String,
}

impl Default for MonitoringConfig {
    fn default() -> Self {
        Self {
            opentelemetry_enabled: Default::default(),
            service_name: default_svc_name(),
        }
    }
}

fn default_svc_name() -> String {
    "safareig".to_string()
}

#[derive(Debug, Deserialize, Default)]
pub struct AdminConfig {
    pub enabled: bool,
    #[serde(default)]
    pub admins: HashSet<OwnedUserId>,
}

#[derive(Debug, Deserialize)]
pub struct AuthConfig {
    #[serde(
        with = "serde_humanize_rs",
        default = "AuthConfig::default_expiry_time"
    )]
    pub expiry_time: Duration,
}

impl Default for AuthConfig {
    fn default() -> Self {
        Self {
            expiry_time: AuthConfig::default_expiry_time(),
        }
    }
}

impl AuthConfig {
    fn default_expiry_time() -> Duration {
        Duration::from_secs(3600 * 24 * 7)
    }
}

#[derive(Debug, Deserialize)]
pub struct RoomsConfig {
    #[serde(default = "default_room_version_id")]
    pub default_version: RoomVersionId,

    pub versions: Vec<RoomVersionSelectorConfig>,
}

impl Default for RoomsConfig {
    fn default() -> Self {
        Self {
            default_version: default_room_version_id(),
            versions: Default::default(),
        }
    }
}

fn default_room_version_id() -> RoomVersionId {
    RoomVersionId::V10
}

#[derive(Debug, Deserialize)]
pub struct RoomVersionSelectorConfig {
    pub prefix: String,
    pub version: RoomVersionId,
}

#[derive(Debug, Deserialize)]
#[serde(tag = "driver")]
#[non_exhaustive]
pub enum ServerSideSearchConfig {
    Sqlite,
    Tantivy(TantivyOptions),
}

impl Default for ServerSideSearchConfig {
    fn default() -> Self {
        Self::Tantivy(TantivyOptions::default())
    }
}

#[derive(Debug, Deserialize, Clone, Default)]
pub struct TantivyOptions {
    pub index_path: Option<PathBuf>,
}

impl TantivyOptions {
    pub fn path(&self, config: &HomeserverConfig) -> PathBuf {
        let dir = match &self.index_path {
            Some(path) => path.to_path_buf(),
            None => {
                let mut dir = dirs::data_dir().unwrap();
                dir.push("safareig");
                dir.push(config.server_name.to_string());
                dir.push("tantivy");

                dir
            }
        };

        std::fs::create_dir_all(&dir).unwrap();

        dir
    }
}

impl HomeserverConfig {
    pub fn from_file(path: &Path) -> HomeserverConfig {
        let mut config = Config::builder();

        if std::path::Path::new(path).exists() {
            config = config.add_source(File::from(path));
        } else {
            tracing::error!(
                path = path.to_str().unwrap_or_default(),
                "Could not find any configuration file in the target directory. Server will use default confiuration."
            );
        }

        let config = config.add_source(
            Environment::with_prefix("SAFAREIG")
                .separator("__")
                .try_parsing(true),
        );

        let homeserver: HomeserverConfig = config
            .build()
            .unwrap()
            .try_deserialize()
            .expect("config is deserializable");

        if homeserver.federation.skip_insecure {
            tracing::warn!("Federation HTTPS clients won't check SSL CA certificates, which can lead to MITM attacks. Please consider switching this flag unless the application runs in dev mode");
        }

        if !homeserver.ssl.enabled {
            tracing::warn!("Please enable and configure SSL if you want to be able to federate this homeserver");
        }

        homeserver
    }
}
