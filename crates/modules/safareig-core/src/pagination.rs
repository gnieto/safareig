use std::fmt::{Debug, Display};

use ruma::UInt;

pub trait Token: Display + Ord + PartialOrd + Eq + PartialEq + Clone {}

impl Token for String {}

#[derive(Debug, Clone)]
pub enum Boundary<T> {
    Exclusive(T),
    Inclusive(T),
    Unlimited,
}

impl<T: Clone> Boundary<T> {
    pub fn is_exclusive(&self) -> bool {
        matches!(self, Boundary::Exclusive(_))
    }

    pub fn exclusive(&self) -> Option<Boundary<T>> {
        match self {
            Boundary::Exclusive(t) | Boundary::Inclusive(t) => Some(Boundary::Exclusive(t.clone())),
            Boundary::Unlimited => None,
        }
    }

    pub fn inclusive(&self) -> Boundary<T> {
        match self {
            Boundary::Exclusive(t) | Boundary::Inclusive(t) => Boundary::Inclusive(t.clone()),
            Boundary::Unlimited => Boundary::Unlimited,
        }
    }

    pub fn token(self) -> Option<T> {
        match self {
            Boundary::Exclusive(t) | Boundary::Inclusive(t) => Some(t),
            Boundary::Unlimited => None,
        }
    }
}

impl<T> Boundary<T> {
    pub fn as_ops_ref(&self) -> std::ops::Bound<&T> {
        match self {
            Boundary::Exclusive(t) => std::ops::Bound::Excluded(t),
            Boundary::Inclusive(t) => std::ops::Bound::Included(t),
            Boundary::Unlimited => std::ops::Bound::Unbounded,
        }
    }
}

impl<T: Token> From<Boundary<T>> for std::ops::Bound<T> {
    fn from(boundary: Boundary<T>) -> Self {
        match boundary {
            Boundary::Exclusive(t) => std::ops::Bound::Excluded(t),
            Boundary::Inclusive(t) => std::ops::Bound::Included(t),
            Boundary::Unlimited => std::ops::Bound::Unbounded,
        }
    }
}

#[derive(Clone, Copy, Debug)]
pub enum Direction {
    Backward,
    Forward,
}

impl From<ruma::api::Direction> for Direction {
    fn from(ruma_direction: ruma::api::Direction) -> Self {
        match ruma_direction {
            ruma::api::Direction::Backward => Self::Backward,
            ruma::api::Direction::Forward => Self::Forward,
        }
    }
}

#[derive(Debug, Clone)]
pub struct Limit(u32);

impl Limit {
    pub fn limit_or_default(limit: Option<UInt>, default: u32) -> Self {
        match limit {
            Some(limit) => {
                let mut limit = Self::from(limit);
                limit.0 = u32::min(limit.0, default);
                limit
            }
            None => Limit(default),
        }
    }
}

impl From<u32> for Limit {
    fn from(limit: u32) -> Self {
        Limit(limit)
    }
}

impl From<UInt> for Limit {
    fn from(limit: UInt) -> Self {
        Limit(u32::try_from(limit).unwrap())
    }
}

#[derive(Debug, Clone)]
pub struct Range<T>(Boundary<T>, Boundary<T>);

impl<T> From<(Boundary<T>, Boundary<T>)> for Range<T> {
    fn from(tuple: (Boundary<T>, Boundary<T>)) -> Self {
        Range(tuple.0, tuple.1)
    }
}

impl<T: Clone> Range<T> {
    pub fn from(&self) -> &Boundary<T> {
        &self.0
    }

    pub fn to(&self) -> &Boundary<T> {
        &self.1
    }

    pub fn set_to(&mut self, new_to: Boundary<T>) {
        self.1 = new_to;
    }

    pub fn tuple(&self) -> (Boundary<T>, Boundary<T>) {
        (self.0.clone(), self.1.clone())
    }

    pub fn to_token(&self) -> Option<T> {
        match &self.1 {
            Boundary::Exclusive(t) | Boundary::Inclusive(t) => Some(t.clone()),
            Boundary::Unlimited => None,
        }
    }

    pub fn from_token(&self) -> Option<T> {
        match &self.0 {
            Boundary::Exclusive(t) | Boundary::Inclusive(t) => Some(t.clone()),
            Boundary::Unlimited => None,
        }
    }

    pub fn all() -> Self {
        (Boundary::Unlimited, Boundary::Unlimited).into()
    }
}

impl<T: Clone> Range<T> {
    pub fn map<O, F: Fn(&T) -> O>(&self, f: F) -> Range<O> {
        let from = match &self.0 {
            Boundary::Exclusive(from) => Boundary::Exclusive(f(from)),
            Boundary::Inclusive(from) => Boundary::Inclusive(f(from)),
            Boundary::Unlimited => Boundary::Unlimited,
        };

        let to = match &self.1 {
            Boundary::Exclusive(to) => Boundary::Exclusive(f(to)),
            Boundary::Inclusive(to) => Boundary::Inclusive(f(to)),
            Boundary::Unlimited => Boundary::Unlimited,
        };

        (from, to).into()
    }
}

impl<T: Token> Range<T> {
    pub fn is_included(&self, element: T) -> bool {
        let from_allowed = match self.0.clone() {
            Boundary::Exclusive(from) => element > from,
            Boundary::Inclusive(from) => element >= from,
            Boundary::Unlimited => true,
        };

        let to_allowed = match self.1.clone() {
            Boundary::Exclusive(to) => element < to,
            Boundary::Inclusive(to) => element <= to,
            Boundary::Unlimited => true,
        };

        from_allowed && to_allowed
    }

    pub fn limit_to_upper_bound(self, token: T) -> Self {
        let new_from = match self.0 {
            Boundary::Exclusive(from) => Boundary::Exclusive(std::cmp::min(from, token.clone())),
            Boundary::Inclusive(from) => Boundary::Inclusive(std::cmp::min(from, token.clone())),
            Boundary::Unlimited => Boundary::Unlimited,
        };

        let new_to = match self.1.clone() {
            Boundary::Exclusive(to) => Boundary::Exclusive(std::cmp::min(to, token)),
            Boundary::Inclusive(to) => Boundary::Inclusive(std::cmp::min(to, token)),
            Boundary::Unlimited => Boundary::Exclusive(token),
        };

        (new_from, new_to).into()
    }
}

impl<T> std::ops::RangeBounds<T> for Range<T> {
    fn start_bound(&self) -> std::ops::Bound<&T> {
        self.0.as_ops_ref()
    }

    fn end_bound(&self) -> std::ops::Bound<&T> {
        self.1.as_ops_ref()
    }
}

#[derive(Debug, Clone)]
pub struct Pagination<T: Token, C = ()> {
    range: Range<T>,
    direction: Direction,
    limit: Option<Limit>,
    context: C,
}

impl<T: Token, C> Pagination<T, C> {
    pub fn new(range: Range<T>, context: C) -> Self {
        Self {
            range,
            direction: Direction::Forward,
            limit: None,
            context,
        }
    }

    pub fn with_limit(mut self, limit: Limit) -> Self {
        self.limit = Some(limit);
        self
    }

    pub fn with_direction(mut self, direction: Direction) -> Self {
        self.direction = direction;
        self
    }

    pub fn from(&self) -> &Boundary<T> {
        &self.range.0
    }

    pub fn range(&self) -> &Range<T> {
        &self.range
    }

    pub fn mut_range(&mut self) -> &mut Range<T> {
        &mut self.range
    }

    pub fn limit(&self) -> Option<u32> {
        self.limit.as_ref().map(|limit| limit.0)
    }

    pub fn direction(&self) -> Direction {
        self.direction
    }

    pub fn context(&self) -> &C {
        &self.context
    }
}

pub struct PaginationResponse<T: Token, I> {
    records: Vec<I>,
    previous: Option<T>,
    next: Option<T>,
}

impl<T: Token, I> PaginationResponse<T, I> {
    pub fn new(records: Vec<I>, previous: Option<T>, next: Option<T>) -> Self {
        PaginationResponse {
            records,
            previous,
            next,
        }
    }

    pub fn empty() -> Self {
        Self {
            records: vec![],
            previous: None,
            next: None,
        }
    }

    pub fn records(self) -> Vec<I> {
        self.records
    }

    pub fn is_empty(&self) -> bool {
        self.records.is_empty()
    }

    pub fn previous(&self) -> Option<&T> {
        self.previous.as_ref()
    }

    pub fn next(&self) -> Option<&T> {
        self.next.as_ref()
    }
}
