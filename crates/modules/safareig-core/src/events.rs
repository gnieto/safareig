use ruma::{
    api::client::error::{ErrorBody, ErrorKind},
    events::{EventContent, EventContentFromType, GlobalAccountDataEventType, TimelineEventType},
    exports::http::StatusCode,
    serde::Raw,
};
use serde::{Deserialize, Serialize};
use serde_json::value::to_raw_value;
use thiserror::Error;

use crate::error::RumaExt;

#[derive(Error, Debug)]
pub enum EventError {
    #[error("Mismatching hashes")]
    MismatchingHashes,
    #[error("Invalid signature")]
    InvalidSignature,
    #[error("Signing error")]
    SigningError,
    #[error("Conversion to canonical JSON failed")]
    CanonicalJsonConversion,
    #[error("Missing expected field {0}")]
    MissingField(String),
    #[error("Serde JSON error: {0}")]
    SerdeJson(#[from] serde_json::Error),
    #[error("Event content has unexpeted types or values")]
    UnexpectedContent(serde_json::Error),
    #[error("{0}")]
    Custom(String),
}

impl From<EventError> for ruma::api::client::Error {
    fn from(error: EventError) -> Self {
        match error {
            EventError::UnexpectedContent(_) => ruma::api::client::Error {
                body: ErrorBody::Standard {
                    kind: ErrorKind::InvalidParam,
                    message: error.to_string(),
                },
                status_code: StatusCode::BAD_REQUEST,
            },
            _ => ruma::api::client::Error {
                body: ErrorBody::Standard {
                    kind: ErrorKind::Unknown,
                    message: error.to_string(),
                },
                status_code: StatusCode::INTERNAL_SERVER_ERROR,
            },
        }
    }
}

impl From<EventError> for ruma::api::error::MatrixError {
    fn from(error: EventError) -> Self {
        let client = ruma::api::client::Error::from(error);
        client.to_generic()
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Content {
    content: serde_json::Value,
    event_type: String,
}

impl Content {
    pub fn new(content: serde_json::Value, event_type: String) -> Self {
        Content {
            content,
            event_type,
        }
    }

    pub fn from_event_content<E>(event: &E) -> Result<Content, EventError>
    where
        E: EventContent,
        E::EventType: ToString,
    {
        Ok(Content {
            content: serde_json::to_value(event).map_err(EventError::UnexpectedContent)?,
            event_type: event.event_type().to_string(),
        })
    }

    pub fn from_raw_event_content<E: EventContent + EventContentFromType, K: ToString>(
        event_type: K,
        content: &Raw<E>,
    ) -> Result<Content, EventError> {
        // Delegate to Ruma the parsing and verification of the incoming event. We ignore the result
        // because we are only interested on the verification phase.
        let _ = E::from_parts(&event_type.to_string(), content.json())
            .map_err(EventError::UnexpectedContent)?;

        // After validating, we build the final event content.
        let json = serde_json::from_str(content.json().get())?;

        Ok(Content {
            content: json,
            event_type: event_type.to_string(),
        })
    }

    pub fn event_type(&self) -> TimelineEventType {
        TimelineEventType::from(self.event_type.as_ref())
    }

    pub fn global_account_data_type(&self) -> GlobalAccountDataEventType {
        GlobalAccountDataEventType::from(self.event_type.as_ref())
    }

    pub fn content(&self) -> &serde_json::Value {
        &self.content
    }

    pub fn to_parts(self) -> (TimelineEventType, serde_json::Value) {
        (self.event_type(), self.content)
    }

    pub fn into_raw<T>(self) -> Result<Raw<T>, serde_json::Error> {
        let content = serde_json::json!({
            "type": self.event_type,
            "content": self.content,
        });
        let raw = to_raw_value(&content)?;

        Ok(Raw::from_json(raw))
    }

    pub fn to_event_content<T: EventContent + EventContentFromType>(
        self,
    ) -> Result<T, serde_json::Error> {
        let raw = to_raw_value(&self.content)?;
        T::from_parts(self.event_type.as_str(), &raw)
    }
}
