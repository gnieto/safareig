use std::sync::Arc;

#[async_trait::async_trait]
pub trait Listener<T>: Send + Sync {
    async fn on_event(&self, event: &T);
}

pub struct Dispatcher<T> {
    listeners: Vec<Arc<dyn Listener<T>>>,
}

impl<T> Dispatcher<T> {
    pub fn new(listeners: Vec<Arc<dyn Listener<T>>>) -> Self {
        Self { listeners }
    }

    pub async fn dispatch_event(&self, event: T) {
        for l in &self.listeners {
            l.on_event(&event).await;
        }
    }
}
