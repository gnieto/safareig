use std::{
    any::{type_name, Any},
    marker::PhantomData,
    sync::Arc,
};

use async_trait::async_trait;
use ddi::{
    DDIError, DDIResult, ServiceCollection, ServiceCollectionExt, ServiceFnOnce, ServiceProvider,
    ServiceResolverExt,
};
use graceful::BackgroundHandler;
use ruma::api::IncomingRequest;

use crate::{auth::Identity, config::HomeserverConfig, storage::StorageError};

#[async_trait]
pub trait Command {
    type Input;
    type Output;
    type Error;
    const OPT_PARAMS: u8 = 0;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error>;
}

pub struct FrozenContainer {
    pub service_provider: ddi::ServiceProvider,
}

pub struct Container {
    pub service_collection: ddi::ServiceCollection,
    pub config: Arc<HomeserverConfig>,
}

impl Container {
    pub fn new(config: Arc<HomeserverConfig>) -> Self {
        Self {
            service_collection: ddi::ServiceCollection::new(),
            config,
        }
    }

    pub fn register_lazy_endpoint<Param, Req, Factory, C, R>(
        &mut self,
        factory: Factory,
        router: &mut R,
    ) where
        Factory: ServiceFnOnce<Param, DDIResult<C>> + Send + Sync + 'static,
        Req: IncomingRequest + Send + Sync + 'static,
        R: Router,
        C: 'static
            + Any
            + Send
            + Sync
            + Command<Input = Req, Output = Req::OutgoingResponse, Error = Req::EndpointError>,
    {
        router.add_route::<Req, C>(PhantomData);
        self.service_collection
            .service_factory::<Param, C, Factory>(factory);
    }

    pub fn inject_endpoint<Req, C, R>(&mut self, router: &mut R)
    where
        Req: IncomingRequest + Send + Sync + 'static,
        R: Router,
        C: 'static
            + Any
            + Send
            + Sync
            + InjectServices
            + Command<Input = Req, Output = Req::OutgoingResponse, Error = Req::EndpointError>,
    {
        router.add_route::<Req, C>(PhantomData);
        self.service_collection.register::<C>();
    }

    pub fn freeze(self) -> FrozenContainer {
        FrozenContainer {
            service_provider: self.service_collection.provider(),
        }
    }
}

impl FrozenContainer {
    pub fn command<C: Any + Send + Sync>(&self) -> Option<&C> {
        self.service_provider
            .get::<C>()
            .map_err(|error| {
                tracing::error!(?error, "DDI error");
                error
            })
            .ok()
    }

    pub fn service<S: Any + Clone>(&self) -> S {
        self.service_provider
            .get::<S>()
            .unwrap_or_else(|error| {
                panic!(
                    "[Service Provider] Expected service registered, but it isn't: {}\n\nError: {}",
                    type_name::<S>(),
                    error,
                )
            })
            .clone()
    }

    pub fn service_opt<S: Any + Clone>(&self) -> Option<S> {
        self.service_provider.get::<S>().ok().cloned()
    }
}

#[async_trait::async_trait]
pub trait Module<R: Router>: Send + Sync {
    fn register(&self, container: &mut Container, router: &mut R);
    async fn spawn_workers(&self, _container: &ServiceProvider, _: &BackgroundHandler) {}
    async fn run_migrations(&self, _container: &ServiceProvider) -> Result<(), StorageError> {
        Ok(())
    }
}

pub struct DummyRouter;

impl Router for DummyRouter {
    fn add_route<R, C>(&mut self, _command: PhantomData<C>)
    where
        R: IncomingRequest + Send + Sync + 'static,
        C: 'static
            + Send
            + Sync
            + Command<Input = R, Output = R::OutgoingResponse, Error = R::EndpointError>,
    {
    }
}

pub trait Router {
    fn add_route<R, C>(&mut self, command: PhantomData<C>)
    where
        R: IncomingRequest + Send + Sync + 'static,
        C: 'static
            + Send
            + Sync
            + Command<Input = R, Output = R::OutgoingResponse, Error = R::EndpointError>;
}

pub trait InjectServices: Sized {
    fn inject(sp: &ServiceProvider) -> Result<Self, DDIError>;
}

pub trait ServiceCollectionExtension: ServiceCollectionExt {
    fn register<T: InjectServices + Send + Sync + 'static>(&mut self);
    fn register_arc<T: InjectServices + Send + Sync + 'static>(&mut self);
    fn register_var<T: InjectServices + Send + Sync + 'static>(&mut self, name: &'static str);
}

impl ServiceCollectionExtension for ServiceCollection {
    fn register<T: InjectServices + Send + Sync + 'static>(&mut self) {
        self.service_factory::<_, T, _>(|sp: &ServiceProvider| T::inject(sp))
    }

    fn register_arc<T: InjectServices + Send + Sync + 'static>(&mut self) {
        self.service_factory::<_, Arc<T>, _>(|sp: &ServiceProvider| T::inject(sp).map(Arc::new))
    }

    fn register_var<T: InjectServices + Send + Sync + 'static>(&mut self, name: &'static str) {
        self.service_factory_var::<_, T, _>(name, |sp: &ServiceProvider| T::inject(sp))
    }
}
