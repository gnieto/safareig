use std::{path::PathBuf, time::Duration};

#[cfg(feature = "backend-sqlx")]
use sqlx::ConnectOptions;

use crate::config::{DatabaseConfig, HomeserverConfig};

#[derive(Clone)]
#[non_exhaustive]
pub enum DatabaseConnection {
    #[cfg(feature = "backend-sqlx")]
    Sqlx(::sqlx::Pool<::sqlx::Sqlite>),
}

impl DatabaseConnection {
    #[cfg(feature = "backend-sqlx")]
    pub fn sqlx(self) -> Option<::sqlx::Pool<::sqlx::Sqlite>> {
        match self {
            DatabaseConnection::Sqlx(pool) => Some(pool),
            _ => None,
        }
    }
}

impl From<&HomeserverConfig> for DatabaseConnection {
    fn from(config: &HomeserverConfig) -> Self {
        match config.database {
            DatabaseConfig::Sqlite(ref options) => {
                #[cfg(not(feature = "backend-sqlx"))]
                panic!("Compiled without SQLX support. Change databas configuration");
                #[cfg(feature = "backend-sqlx")]
                {
                    use std::str::FromStr;

                    use sqlx::sqlite::SqliteJournalMode;

                    let sqlite_options = if options.in_memory {
                        ::sqlx::sqlite::SqliteConnectOptions::from_str("sqlite::memory:").unwrap()
                    } else {
                        let mut dir = match &options.path {
                            Some(path) => {
                                let mut dir = PathBuf::from(path);
                                dir.push("safareig");
                                dir.push(config.server_name.to_string());
                                dir.push("db");

                                std::fs::create_dir_all(dir.clone()).unwrap();

                                dir
                            }
                            None => {
                                let mut dir = dirs::data_dir().unwrap();
                                dir.push("safareig");
                                dir.push(config.server_name.to_string());
                                dir.push("db");

                                std::fs::create_dir_all(dir.clone()).unwrap();

                                dir
                            }
                        };

                        dir.push("safareig.db");

                        tracing::info!("Using Sqlite DB on path {:?}", dir);

                        ::sqlx::sqlite::SqliteConnectOptions::new().filename(dir)
                    };

                    let mut sqlite_options = sqlite_options
                        .synchronous(::sqlx::sqlite::SqliteSynchronous::Normal)
                        .serialized(true)
                        .busy_timeout(Duration::from_secs(1))
                        .journal_mode(SqliteJournalMode::Wal)
                        .create_if_missing(true);

                    if options.log_statements {
                        sqlite_options = sqlite_options.log_statements(::log::LevelFilter::Info);
                    } else {
                        sqlite_options = sqlite_options.log_statements(::log::LevelFilter::Debug);
                    }

                    if options.log_slow {
                        sqlite_options = sqlite_options
                            .log_slow_statements(::log::LevelFilter::Info, Duration::from_secs(5));
                    }

                    let pool = ::sqlx::pool::PoolOptions::new();

                    DatabaseConnection::Sqlx(pool.connect_lazy_with(sqlite_options))
                }
            }
            _ => unimplemented!(),
        }
    }
}
