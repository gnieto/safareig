use std::{collections::HashMap, convert::TryFrom};

use ruma::{
    api::client::error::{ErrorBody, ErrorKind},
    exports::http::StatusCode,
    DeviceId, OwnedDeviceId, OwnedServerName, OwnedServerSigningKeyId, OwnedUserId, UserId,
};

#[derive(Clone, Debug)]
pub enum Identity {
    None,
    Device(OwnedDeviceId, OwnedUserId),
    AppService(Option<OwnedUserId>, String),
    Federation(FederationAuth),
}

impl Identity {
    pub fn user(&self) -> &UserId {
        match self {
            Identity::Device(_, u) => u,
            Identity::AppService(Some(u), _) => u,
            Identity::Federation(_) | Identity::None | Identity::AppService(None, _) => {
                unreachable!("expected user for appservice token")
            }
        }
    }

    pub fn maybe_user(&self) -> Option<&UserId> {
        match self {
            Identity::Device(_, u) => Some(u),
            Identity::AppService(Some(u), _) => Some(u),
            Identity::Federation(_) | Identity::None | Identity::AppService(None, _) => None,
        }
    }

    pub fn device(&self) -> Option<&DeviceId> {
        match self {
            Identity::Device(d, _) => Some(d),
            Identity::Federation(_) | Identity::None | Identity::AppService(_, _) => None,
        }
    }

    pub fn check_user(&self, compare_to: &UserId) -> Result<(), ruma::api::client::Error> {
        if self.matches_expected_user(compare_to) {
            return Ok(());
        }

        Err(ruma::api::client::Error {
            body: ErrorBody::Standard {
                kind: ErrorKind::forbidden(),
                message: "user mismatch".to_string(),
            },
            status_code: StatusCode::UNAUTHORIZED,
        })
    }

    pub fn check_device_user(
        &self,
        expected_user: &UserId,
        expected_device: &DeviceId,
    ) -> Result<(), ruma::api::client::Error> {
        if self.matches_expected_user(expected_user) && self.device() == Some(expected_device) {
            return Ok(());
        }

        Err(ruma::api::client::Error {
            body: ErrorBody::Standard {
                kind: ErrorKind::forbidden(),
                message: "user mismatch".to_string(),
            },
            status_code: StatusCode::UNAUTHORIZED,
        })
    }

    pub fn to_federation_auth(self) -> Option<FederationAuth> {
        match self {
            Identity::Federation(auth) => Some(auth),
            _ => None,
        }
    }

    pub fn federation_auth(&self) -> &FederationAuth {
        match self {
            Identity::Federation(ref auth) => auth,
            _ => unreachable!(),
        }
    }

    fn matches_expected_user(&self, compare_to: &UserId) -> bool {
        self.user() == compare_to
    }
}

#[derive(Clone, Debug)]
pub struct FederationAuth {
    pub server: OwnedServerName,
    pub key: OwnedServerSigningKeyId,
    pub signature: String,
}

impl TryFrom<&[u8]> for FederationAuth {
    type Error = &'static str;

    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        if !value.starts_with(b"X-Matrix ") {
            return Err("X-Matrix not found");
        }

        let value = String::from_utf8_lossy(&value[9..]);
        let header_map: HashMap<&str, &str> = value
            .split(',')
            .map(|expr| {
                let mut expr_parts = expr.split('=');
                let name = expr_parts.next().unwrap_or_default();
                let value = expr_parts.next().unwrap_or_default();

                (name, value.trim_end_matches('\"').trim_start_matches('\"'))
            })
            .collect();

        let server_name = header_map.get("origin").ok_or("origin not found")?;
        let server_name: OwnedServerName = server_name.parse().map_err(|_| "invalid origin")?;

        let key = header_map.get("key").ok_or("key not found")?;
        let key: OwnedServerSigningKeyId = (*key).try_into().map_err(|_| "invalid key")?;

        let signature = header_map.get("sig").ok_or("signature not found")?;

        Ok(FederationAuth {
            server: server_name,
            key,
            signature: signature.to_string(),
        })
    }
}

#[cfg(test)]
mod test {
    use std::convert::TryFrom;

    use crate::auth::FederationAuth;

    #[test]
    fn parse_matrix_header() {
        let bytes = b"X-Matrix origin=\"test.cat\",key=\"ed25519:key1\",sig=\"ABCD\"";
        let h = bytes as &[u8];
        let header = FederationAuth::try_from(h).unwrap();

        assert_eq!(header.server.as_str(), "test.cat");
        assert_eq!(header.key.as_str(), "ed25519:key1");
        assert_eq!(header.signature.as_str(), "ABCD");
    }

    #[test]
    fn parse_matrix_header_unquoted() {
        let bytes = b"X-Matrix origin=test.cat,key=ed25519:key1,sig=ABCD";
        let h = bytes as &[u8];
        let header = FederationAuth::try_from(h).unwrap();

        assert_eq!(header.server.as_str(), "test.cat");
        assert_eq!(header.key.as_str(), "ed25519:key1");
        assert_eq!(header.signature.as_str(), "ABCD");
    }

    #[test]
    fn parse_invalid_matrix_header_errors() {
        let bytes = b"X-Matrix";
        let h = bytes as &[u8];
        let header = FederationAuth::try_from(h);
        assert!(header.is_err());

        let bytes = b"X-Matrix ";
        let h = bytes as &[u8];
        let header = FederationAuth::try_from(h);
        assert!(header.is_err());

        let bytes = b"X-Matrix origin=test.cat,key=ed25519:key1";
        let h = bytes as &[u8];
        let header = FederationAuth::try_from(h);
        assert!(header.is_err());
    }
}
