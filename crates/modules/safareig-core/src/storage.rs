use std::str::Utf8Error;

use ddi::DDIError;
use ruma::{
    api::client::{
        error::{ErrorBody, ErrorKind},
        Error,
    },
    exports::http::StatusCode,
};
use thiserror::Error;

use crate::error::RumaExt;

#[derive(Error, Debug)]
pub enum StorageError {
    #[error("I/O error")]
    Io,
    #[error("Ser/Deserialization error")]
    Serialization(#[from] ::serde_json::Error),
    #[error("Transaction error")]
    Transaction,
    #[error("Utf8 error")]
    Utf8(#[from] Utf8Error),
    #[error("Ruma identifier error")]
    Ruma(#[from] ruma::IdParseError),
    #[error("{0}")]
    Custom(String),
    #[error("Malformed data on the underlying storage")]
    CorruptedData,
    #[cfg(feature = "backend-sqlx")]
    #[error("SQL error: {0}")]
    Sqlx(#[from] ::sqlx::Error),

    // TODO: Remove this variant. Rename error type from modules.
    #[error("Dependency injection: {0}")]
    DDI(#[from] DDIError),
}

pub type Result<T> = std::result::Result<T, StorageError>;

impl From<StorageError> for Error {
    fn from(error: StorageError) -> Self {
        #[cfg(feature = "backend-sqlx")]
        {
            if let StorageError::Sqlx(ref sql_error) = error {
                let code = sql_error.as_database_error().and_then(|error| error.code());

                if let Some(code) = code {
                    if code == "1555" {
                        if let Some(msg) =
                            sql_error.as_database_error().map(|error| error.message())
                        {
                            tracing::error!(msg);
                        }

                        return Error {
                            body: ErrorBody::Standard {
                                kind: ErrorKind::Unknown,
                                message: "database conflict".to_string(),
                            },
                            status_code: StatusCode::CONFLICT,
                        };
                    }
                }
            }
        }

        Error {
            body: ErrorBody::Standard {
                kind: ErrorKind::Unknown,
                message: error.to_string(),
            },
            status_code: StatusCode::INTERNAL_SERVER_ERROR,
        }
    }
}

impl From<StorageError> for ruma::api::error::MatrixError {
    fn from(e: StorageError) -> Self {
        RumaExt::to_generic(Error::from(e))
    }
}
