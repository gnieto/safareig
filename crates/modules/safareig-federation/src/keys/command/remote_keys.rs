use std::{
    collections::{BTreeMap, HashSet},
    sync::Arc,
    time::SystemTime,
};

use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    command::Command,
    config::HomeserverConfig,
    ruma::{
        api::federation::discovery::{
            get_remote_server_keys::v2 as single_server_keys,
            get_remote_server_keys_batch::v2::{self as remote_keys, QueryCriteria},
            ServerSigningKeys,
        },
        serde::Raw,
        OwnedServerSigningKeyId,
    },
    time::uint_to_system_time,
    Inject,
};

use crate::keys::{
    sign_ruma_object, split_keys, storage::Key, KeyFetcher, LocalKeyProvider, RequestedKeys,
    ServerKeyStorage,
};

#[derive(Inject, Clone)]
pub struct RemoteKeysCommand {
    local_key_provider: Arc<LocalKeyProvider>,
    config: Arc<HomeserverConfig>,
    key_storage: Arc<dyn ServerKeyStorage>,
    key_fetcher: Arc<dyn KeyFetcher>,
}

#[async_trait]
impl Command for RemoteKeysCommand {
    type Input = remote_keys::Request;
    type Output = remote_keys::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;

    async fn execute(&self, input: Self::Input, _: Identity) -> Result<Self::Output, Self::Error> {
        let mut server_keys = Vec::new();
        let current_key = self.local_key_provider.current_key().await?;

        for (server, queried_keys) in &input.server_keys {
            let keys_for_server = self.key_storage.get_server_keys(server).await?;
            let (mut usable_keys, non_fresh_keys) =
                Self::separate_usable_keys(keys_for_server, queried_keys);

            if !non_fresh_keys.is_empty() {
                let mut requested_keys = RequestedKeys::new();
                requested_keys.insert(server.to_owned(), non_fresh_keys);
                let maybe_new_keys = self.key_fetcher.fetch_keys(&requested_keys).await;

                match maybe_new_keys {
                    Err(e) => {
                        tracing::error!(
                            server = server.to_string().as_str(),
                            err = e.to_string().as_str(),
                            "returning partial or non-fresh keys for target server",
                        )
                    }
                    Ok(mut keys) => {
                        if let Some(keys) = keys.remove(server) {
                            for (_, k) in keys.into_iter() {
                                usable_keys.push(k);
                            }
                        }
                    }
                }
            }

            let signing_keys = split_keys(server, usable_keys)?;

            let signed_object: ServerSigningKeys = sign_ruma_object(
                self.config.server_name.as_str(),
                &signing_keys,
                &[current_key.as_ref()],
            )?;

            // TODO: unwrap
            server_keys.push(Raw::new(&signed_object).unwrap());
        }

        Ok(remote_keys::Response { server_keys })
    }
}

impl RemoteKeysCommand {
    /// `split_keys` splits the keys in two chunks: One with usable keys and one with missing or already expired
    /// keys.
    fn separate_usable_keys(
        keys: Vec<Key>,
        queried_keys: &BTreeMap<OwnedServerSigningKeyId, QueryCriteria>,
    ) -> (Vec<Key>, HashSet<OwnedServerSigningKeyId>) {
        let now = SystemTime::now();
        let mut keys_to_fetch = HashSet::new();
        let mut usable_keys = Vec::new();

        for key in keys {
            let minimum_valid = queried_keys
                .get(&key.id)
                .and_then(|criteria| criteria.minimum_valid_until_ts)
                .map(|millis| uint_to_system_time(&millis))
                .unwrap_or(now);

            if let Some(valid_until_ts) = key.valid_until_ts {
                if valid_until_ts <= minimum_valid {
                    keys_to_fetch.insert(key.id);
                } else {
                    usable_keys.push(key);
                }
            } else {
                usable_keys.push(key);
            }
        }

        (usable_keys, keys_to_fetch)
    }
}

#[derive(Inject)]
pub struct RemoteKeysSingleCommand {
    remote_command: RemoteKeysCommand,
}

#[async_trait]
impl Command for RemoteKeysSingleCommand {
    type Input = single_server_keys::Request;
    type Output = single_server_keys::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let mut server_keys = BTreeMap::new();
        server_keys.insert(input.server_name.clone(), BTreeMap::new());

        let request = remote_keys::Request { server_keys };

        let response = self.remote_command.execute(request, id).await?;

        Ok(single_server_keys::Response {
            server_keys: response.server_keys,
        })
    }
}
