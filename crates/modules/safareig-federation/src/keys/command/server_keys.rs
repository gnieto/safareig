use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{auth::Identity, command::Command, config::HomeserverConfig, Inject};

use crate::keys::{api::v2 as server_keys, sign_ruma_object, split_keys, LocalKeyProvider};

#[derive(Inject)]
pub struct ServerKeysCommand {
    server_keys: Arc<LocalKeyProvider>,
    config: Arc<HomeserverConfig>,
}

#[async_trait]
impl Command for ServerKeysCommand {
    type Input = server_keys::Request;
    type Output = server_keys::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;
    const OPT_PARAMS: u8 = 1;

    async fn execute(&self, _: Self::Input, _: Identity) -> Result<Self::Output, Self::Error> {
        let current_key = self.server_keys.current_key().await?;
        let homeserver = &self.config.server_name;
        let current_server_keys = self.server_keys.all_keys().await?;

        let signing_keys = split_keys(homeserver, current_server_keys)?;
        let signing_keys =
            sign_ruma_object(homeserver.as_str(), &signing_keys, &[current_key.as_ref()])?;

        Ok(server_keys::Response {
            server_key: signing_keys,
        })
    }
}
