use async_trait::async_trait;
use safareig_core::{ruma::ServerName, storage::StorageError};
use safareig_sqlx::decode_from_str;
use sqlx::{sqlite::SqliteRow, types::chrono, Pool, Row, Sqlite};

use crate::keys::{Key, ServerKeyStorage};

pub struct SqlxServerKeyStorage {
    pool: Pool<Sqlite>,
}

impl SqlxServerKeyStorage {
    // TODO: Generic or dyn dispatch
    pub fn new(pool: Pool<Sqlite>) -> Self {
        Self { pool }
    }
}

#[async_trait]
impl ServerKeyStorage for SqlxServerKeyStorage {
    async fn get_server_keys(&self, server_name: &ServerName) -> Result<Vec<Key>, StorageError> {
        sqlx::query(
            r#"
            SELECT * FROM server_keys
            WHERE server_name = ?1;
            "#,
        )
        .bind(server_name.as_str())
        .fetch_all(&self.pool)
        .await?
        .into_iter()
        .map(TryFrom::try_from)
        .collect::<Result<Vec<Key>, StorageError>>()
    }

    async fn add_server_key(
        &self,
        server_name: &ServerName,
        key: &Key,
    ) -> Result<(), StorageError> {
        let key_type = match key.key_type {
            super::KeyType::KeyPair => 0,
            super::KeyType::PublicKey => 1,
        };
        let expire = key.expire.map(chrono::DateTime::<chrono::Utc>::from);
        let valid_until = key
            .valid_until_ts
            .map(chrono::DateTime::<chrono::Utc>::from);

        sqlx::query(
            r#"
            INSERT INTO server_keys (server_name, key_id, key_content, key_type, expire, valid_until)
            VALUES (?1, ?2, ?3, ?4, ?5, ?6)
            ON CONFLICT DO UPDATE SET expire = ?5, valid_until = ?6
            "#,
        )
        .bind(server_name.as_str())
        .bind(key.id.as_str())
        .bind(&key.content)
        .bind(key_type)
        .bind(expire)
        .bind(valid_until)
        .execute(&self.pool)
        .await?;

        Ok(())
    }
}

impl TryFrom<SqliteRow> for Key {
    type Error = StorageError;

    fn try_from(row: SqliteRow) -> Result<Self, Self::Error> {
        let key_id = decode_from_str(row.get::<&str, _>("key_id"))?;
        let content = row.get::<Vec<u8>, _>("key_content");
        let key_type = row.get::<u32, _>("key_type");
        let expire = row.try_get::<Option<chrono::DateTime<chrono::Utc>>, _>("expire")?;
        let valid_until = row.try_get::<Option<chrono::DateTime<chrono::Utc>>, _>("valid_until")?;

        let key_type = match key_type {
            0 => super::KeyType::KeyPair,
            1 => super::KeyType::PublicKey,
            _ => return Err(StorageError::Custom("unrecognized key type".to_string())),
        };

        Ok(Key {
            content,
            key_type,
            id: key_id,
            expire: expire.map(|expire| expire.into()),
            valid_until_ts: valid_until.map(|valid_until| valid_until.into()),
        })
    }
}
