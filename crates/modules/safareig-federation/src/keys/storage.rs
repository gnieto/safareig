#[cfg(feature = "backend-sqlx")]
pub(crate) mod sqlx;

use std::time::SystemTime;

use async_trait::async_trait;
use safareig_core::{
    ruma::{signatures::Ed25519KeyPair, OwnedServerSigningKeyId, ServerName},
    storage::StorageError,
};
use serde::{Deserialize, Serialize};

use super::Ed25519PublicKey;

#[async_trait]
pub trait ServerKeyStorage: Send + Sync {
    async fn get_server_keys(&self, server_name: &ServerName) -> Result<Vec<Key>, StorageError>;
    async fn add_server_key(&self, server_name: &ServerName, key: &Key)
        -> Result<(), StorageError>;
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct Key {
    pub content: Vec<u8>,
    pub key_type: KeyType,
    pub id: OwnedServerSigningKeyId,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub expire: Option<SystemTime>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub valid_until_ts: Option<SystemTime>,
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub enum KeyType {
    KeyPair,
    PublicKey,
}

impl std::convert::TryFrom<&Key> for Ed25519PublicKey {
    type Error = StorageError;

    fn try_from(key: &Key) -> std::result::Result<Self, Self::Error> {
        match key.key_type {
            KeyType::PublicKey => Ed25519PublicKey::new(key.content.to_vec(), key.id.to_string())
                .map_err(|_| StorageError::Custom("invalid public key".to_string())),
            KeyType::KeyPair => {
                let key_pair =
                    Ed25519KeyPair::from_der(&key.content, key.id.key_name().to_string())
                        .map_err(|_| StorageError::Custom("invalid key pair".to_string()));

                key_pair.and_then(|pair| {
                    let content = pair.public_key();
                    let version = key.id.to_string();

                    Ed25519PublicKey::new(content.to_vec(), version)
                        .map_err(|_| StorageError::Custom("invalid public key".to_string()))
                })
            }
        }
    }
}
