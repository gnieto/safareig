pub mod v2 {
    use safareig_core::ruma::{
        api::{federation::discovery::ServerSigningKeys, metadata, request, response, Metadata},
        serde::Raw,
    };

    const METADATA: Metadata = metadata! {
        method: GET,
        rate_limited: false,
        authentication: None,
        history: {
            1.0 => "/_matrix/key/v2/server/:key_id",
        }
    };

    #[request(error = safareig_core::ruma::api::error::MatrixError)]
    #[derive(Default)]
    pub struct Request {
        #[ruma_api(path)]
        pub key_id: String,
    }

    #[response(error = safareig_core::ruma::api::error::MatrixError)]
    pub struct Response {
        /// Queried server key, signed by the notary server.
        #[ruma_api(body)]
        pub server_key: Raw<ServerSigningKeys>,
    }

    impl Response {
        /// Creates a new `Response` with the given server key.
        pub fn new(server_key: Raw<ServerSigningKeys>) -> Self {
            Self { server_key }
        }
    }

    impl From<Raw<ServerSigningKeys>> for Response {
        fn from(server_key: Raw<ServerSigningKeys>) -> Self {
            Self::new(server_key)
        }
    }
}
