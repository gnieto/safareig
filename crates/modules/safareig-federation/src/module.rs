use std::sync::Arc;

use ruma_common::OwnedServerName;
use safareig_core::{
    command::{Container, Module, Router},
    config::HomeserverConfig,
    database::DatabaseConnection,
    ddi::{ServiceCollectionExt, ServiceProvider, ServiceResolverExt},
    storage::StorageError,
    ServerInfo,
};
use trust_dns_resolver::config::{ResolverConfig, ResolverOpts};

use crate::{
    client::{
        resolution::{FederationServerDiscovery, RumaClientWellKnown, ServerDiscovery},
        ClientBuilder,
    },
    keys::{
        command::{RemoteKeysCommand, RemoteKeysSingleCommand, ServerKeysCommand},
        storage::ServerKeyStorage,
        ChainedKeyFetcher, KeyFetcher, KeyProvider, LocalKeyProvider, NotaryKeyFetcher,
        OriginKeyFetcher, RemoteKeyProvider,
    },
    server::storage::{FederationStorage, ServerStorage},
};

#[derive(Default)]
pub struct ServerKeysModule {}

impl ServerKeysModule {
    fn server_discovery(server_storage: Arc<dyn ServerStorage>) -> Arc<dyn ServerDiscovery> {
        let srv_resolver = trust_dns_resolver::TokioAsyncResolver::tokio(
            ResolverConfig::default(),
            ResolverOpts::default(),
        )
        .unwrap();

        let discovery = FederationServerDiscovery::new(
            server_storage,
            Arc::new(RumaClientWellKnown {}),
            Arc::new(srv_resolver),
        );

        Arc::new(discovery)
    }

    fn create_server_storage(connection: DatabaseConnection) -> Arc<dyn ServerStorage> {
        match connection {
            #[cfg(feature = "backend-sqlx")]
            DatabaseConnection::Sqlx(pool) => {
                Arc::new(crate::server::storage::sqlx::SqlxServerStorage::new(pool))
            }
            _ => unimplemented!(""),
        }
    }

    fn create_server_key_storage(connection: DatabaseConnection) -> Arc<dyn ServerKeyStorage> {
        match connection {
            #[cfg(feature = "backend-sqlx")]
            DatabaseConnection::Sqlx(pool) => {
                Arc::new(crate::keys::storage::sqlx::SqlxServerKeyStorage::new(pool))
            }
            _ => unimplemented!(""),
        }
    }

    fn create_federation_storage(
        connection: DatabaseConnection,
        server_name: OwnedServerName,
    ) -> Arc<dyn FederationStorage> {
        match connection {
            #[cfg(feature = "backend-sqlx")]
            DatabaseConnection::Sqlx(pool) => Arc::new(
                crate::server::storage::sqlx::SqlxFederationStorage::new(pool, server_name),
            ),
            _ => unimplemented!(""),
        }
    }

    fn create_key_fetcher(
        config: &HomeserverConfig,
        client_builder: Arc<ClientBuilder>,
        key_storage: Arc<dyn ServerKeyStorage>,
        federation_storage: Arc<dyn FederationStorage>,
    ) -> Arc<dyn KeyFetcher> {
        let origin = OriginKeyFetcher::new(client_builder.clone(), key_storage.clone());
        let notary = NotaryKeyFetcher::new(
            config.federation.notary_servers.clone(),
            client_builder,
            key_storage,
            federation_storage,
        );

        let key_fetcher = ChainedKeyFetcher::new(vec![Arc::new(origin), Arc::new(notary)]);

        Arc::new(key_fetcher)
    }
}

#[async_trait::async_trait]
impl<R: Router> Module<R> for ServerKeysModule {
    fn register(&self, container: &mut Container, router: &mut R) {
        container
            .service_collection
            .service_factory(|db: &DatabaseConnection| {
                Ok(Self::create_server_key_storage(db.clone()))
            });
        container
            .service_collection
            .service_factory(|db: &DatabaseConnection| Ok(Self::create_server_storage(db.clone())));
        container.service_collection.service_factory(
            |db: &DatabaseConnection, cfg: &Arc<HomeserverConfig>| {
                Ok(Self::create_federation_storage(
                    db.clone(),
                    cfg.server_name.clone(),
                ))
            },
        );
        container
            .service_collection
            .service_factory(|ss: &Arc<dyn ServerStorage>| Ok(Self::server_discovery(ss.clone())));
        container.service_collection.service_factory(
            |ks: &Arc<dyn ServerKeyStorage>, si: &ServerInfo| {
                Ok(Arc::new(LocalKeyProvider::new(ks.clone(), si.clone())))
            },
        );
        container.service_collection.service_factory(
            |server_keys: &Arc<dyn ServerKeyStorage>, fetcher: &Arc<dyn KeyFetcher>| {
                Ok(Arc::new(RemoteKeyProvider::new(
                    server_keys.clone(),
                    fetcher.clone(),
                )))
            },
        );
        container.service_collection.service_factory(
            |lk: &Arc<LocalKeyProvider>,
             cfg: &Arc<HomeserverConfig>,
             sd: &Arc<dyn ServerDiscovery>| {
                Ok(Arc::new(ClientBuilder::new(
                    lk.clone(),
                    cfg.clone(),
                    sd.clone(),
                )))
            },
        );
        container.service_collection.service_factory(
            |si: &ServerInfo, lk: &Arc<LocalKeyProvider>, rk: &Arc<RemoteKeyProvider>| {
                Ok(Arc::new(KeyProvider::new(
                    si.clone(),
                    lk.clone(),
                    rk.clone(),
                )))
            },
        );
        container.service_collection.service_factory(
            |cfg: &Arc<HomeserverConfig>,
             cb: &Arc<ClientBuilder>,
             ks: &Arc<dyn ServerKeyStorage>,
             fs: &Arc<dyn FederationStorage>| {
                Ok(Self::create_key_fetcher(
                    cfg.as_ref(),
                    cb.clone(),
                    ks.clone(),
                    fs.clone(),
                ))
            },
        );

        container.inject_endpoint::<_, RemoteKeysCommand, _>(router);
        container.inject_endpoint::<_, RemoteKeysSingleCommand, _>(router);
        container.inject_endpoint::<_, ServerKeysCommand, _>(router);
    }

    async fn run_migrations(&self, container: &ServiceProvider) -> Result<(), StorageError> {
        #[cfg(feature = "backend-sqlx")]
        {
            let db = container.get::<DatabaseConnection>()?;
            safareig_sqlx::run_migration(db, "sqlite", "safareig-federation").await?;
        }

        Ok(())
    }
}
