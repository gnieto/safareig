#[cfg(feature = "backend-sqlx")]
pub(crate) mod sqlx;

use std::{
    collections::HashSet,
    time::{Duration, SystemTime},
};

use async_trait::async_trait;
use ruma_common::{OwnedRoomId, RoomId};
use safareig_core::{
    ruma::{OwnedServerName, ServerName},
    storage::StorageError,
};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, Default)]
pub enum ServerState {
    /// Server marked as permanently offline. We won't send outgoing requests to that server unless an
    /// administrator lifts the permanent status or the server receives a request.
    PermanentlyOffline,

    /// Server accumulates outgoing errors.
    TransientError(u8),

    /// Server operates normally.
    #[default]
    Online,
}

impl ServerState {
    pub fn is_online(&self) -> bool {
        #[cfg(feature = "sytest")]
        {
            return true;
        }

        match self {
            ServerState::PermanentlyOffline => false,
            ServerState::TransientError(_) => true,
            ServerState::Online => true,
        }
    }

    /// Increase error count for this server and return if there's a change of state. This allows the caller
    /// to take action, like stopping the federation worker.
    pub fn error_received(&mut self) -> &ServerState {
        let new_state = match self {
            ServerState::Online => ServerState::TransientError(1),
            ServerState::TransientError(n) => {
                if *n == 15 {
                    ServerState::PermanentlyOffline
                } else {
                    ServerState::TransientError(n.saturating_add_signed(1))
                }
            }
            ServerState::PermanentlyOffline => ServerState::PermanentlyOffline,
        };

        *self = new_state;

        self
    }

    /// Returns the delay given the amount of previous (persisted) errors.
    pub fn delay(&self) -> Option<std::time::Duration> {
        #[cfg(feature = "sytest")]
        {
            return Some(std::time::Duration::from_millis(200));
        }

        match self {
            ServerState::Online => Some(std::time::Duration::from_secs(5)),
            ServerState::TransientError(n) => {
                Some(std::time::Duration::from_secs(2u64.pow(*n as u32)))
            }
            ServerState::PermanentlyOffline => None,
        }
    }

    /// Change the status of the server to online. A federation worker will be woken up (if it wasn't) and outgoing
    /// errors count will be resetted.
    pub fn set_online(&mut self) {
        *self = ServerState::Online;
    }
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct Server {
    cached_well_known: Option<WellKnown>,
    latest_outgoing_request: SystemTime,
    latest_incoming_request: SystemTime,
    #[serde(default)]
    state: ServerState,
}

impl Default for Server {
    fn default() -> Self {
        Server {
            cached_well_known: None,
            latest_incoming_request: SystemTime::now(),
            latest_outgoing_request: SystemTime::now(),
            state: ServerState::Online,
        }
    }
}

impl Server {
    pub fn well_known(&self, now: SystemTime) -> Option<WellKnown> {
        match &self.cached_well_known {
            Some(WellKnown::Errored(time)) => {
                // Recommended time from the spec
                let error_cache_time = Duration::from_secs(3600);
                let time_since_error = now.duration_since(*time).unwrap_or(error_cache_time);

                if time_since_error >= error_cache_time {
                    None
                } else {
                    self.cached_well_known.clone()
                }
            }
            Some(WellKnown::Resolved(time, duration, _)) => {
                // Specs recommendation: Respect cache time, but impose a maximum of 48 hours.
                let cache_time = (*duration).min(Duration::from_secs(48 * 3600));
                let time_since_error = now.duration_since(*time).unwrap_or(cache_time);

                if time_since_error >= cache_time {
                    None
                } else {
                    self.cached_well_known.clone()
                }
            }
            None => None,
        }
    }

    pub fn set_well_known(&mut self, well_known: WellKnown) {
        self.cached_well_known = Some(well_known);
    }

    pub fn state(&self) -> &ServerState {
        &self.state
    }

    pub fn state_mut(&mut self) -> &mut ServerState {
        &mut self.state
    }

    pub fn received_incoming_request(&mut self) {
        self.latest_incoming_request = SystemTime::now();
        self.state.set_online();
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum WellKnown {
    Errored(SystemTime),
    Resolved(SystemTime, Duration, OwnedServerName),
}

#[async_trait]
pub trait ServerStorage: Send + Sync {
    async fn store_server(
        &self,
        server_name: &ServerName,
        server: &Server,
    ) -> Result<(), StorageError>;
    async fn load_server(&self, server_name: &ServerName) -> Result<Server, StorageError>;
}

#[async_trait]
pub trait FederationStorage: Send + Sync {
    async fn track_server(
        &self,
        room_id: &RoomId,
        server_name: &ServerName,
    ) -> Result<(), StorageError>;
    async fn tracked_servers(&self) -> Result<Vec<OwnedServerName>, StorageError>;
    async fn server_rooms(
        &self,
        server_name: &ServerName,
    ) -> Result<HashSet<OwnedRoomId>, StorageError>;
    async fn participating_servers(
        &self,
        room_id: &RoomId,
    ) -> Result<Vec<OwnedServerName>, StorageError>;
}
