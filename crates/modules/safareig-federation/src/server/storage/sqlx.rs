use std::collections::HashSet;

use async_trait::async_trait;
use ruma_common::{OwnedServerName, RoomId, ServerName};
use safareig_core::storage::StorageError;
use sqlx::{Pool, Row, Sqlite};

use super::{FederationStorage, ServerStorage};

pub struct SqlxServerStorage {
    pool: Pool<Sqlite>,
}

impl SqlxServerStorage {
    // TODO: Generic or dyn dispatch
    pub fn new(pool: Pool<Sqlite>) -> SqlxServerStorage {
        Self { pool }
    }
}

#[async_trait]
impl ServerStorage for SqlxServerStorage {
    async fn store_server(
        &self,
        server_name: &safareig_core::ruma::ServerName,
        server: &super::Server,
    ) -> Result<(), StorageError> {
        let value = serde_json::to_value(server)?;

        let sql = "INSERT INTO servers (server_name, server_data)
        VALUES (?1, ?2)
        ON CONFLICT DO UPDATE SET server_data = ?2";

        sqlx::query(sql)
            .bind(server_name.as_str())
            .bind(value)
            .execute(&self.pool)
            .await?;

        Ok(())
    }

    async fn load_server(
        &self,
        server_name: &safareig_core::ruma::ServerName,
    ) -> Result<super::Server, StorageError> {
        let sql = "SELECT * FROM servers WHERE server_name = ?1";

        let row = sqlx::query(sql)
            .bind(server_name.as_str())
            .fetch_optional(&self.pool)
            .await?;

        let server = match row {
            Some(row) => {
                let data = row.try_get::<serde_json::Value, _>("server_data")?;
                serde_json::from_value(data)?
            }
            None => super::Server::default(),
        };

        Ok(server)
    }
}

pub struct SqlxFederationStorage {
    pool: Pool<Sqlite>,
    server_name: OwnedServerName,
}

impl SqlxFederationStorage {
    // TODO: Generic or dyn dispatch
    pub fn new(pool: Pool<Sqlite>, server_name: OwnedServerName) -> Self {
        Self { pool, server_name }
    }
}

#[async_trait]
impl FederationStorage for SqlxFederationStorage {
    async fn track_server(
        &self,
        room_id: &safareig_core::ruma::RoomId,
        server_name: &safareig_core::ruma::ServerName,
    ) -> Result<(), StorageError> {
        use std::ops::Deref;

        if self.server_name.deref() == server_name {
            tracing::debug!("Preventing to track current homeserver as a server in federation");
            return Ok(());
        }

        let server_name = server_name.to_string();
        let room_id = room_id.to_string();

        sqlx::query(
            r#"
            INSERT OR IGNORE INTO servers_rooms (server_name, room_id)
            VALUES (?1, ?2);
            "#,
        )
        .bind(server_name)
        .bind(room_id)
        .execute(&self.pool)
        .await?;

        Ok(())
    }

    async fn tracked_servers(
        &self,
    ) -> Result<Vec<safareig_core::ruma::OwnedServerName>, StorageError> {
        let rows = sqlx::query(
            r#"
            SELECT DISTINCT server_name AS server_name FROM servers_rooms
            "#,
        )
        .fetch_all(&self.pool)
        .await?;

        let servers = rows
            .into_iter()
            .map(|row| row.get::<String, _>("server_name"))
            .map(|server| ServerName::parse(server).map_err(|_| StorageError::CorruptedData))
            .collect::<Result<Vec<_>, StorageError>>()?;

        Ok(servers)
    }

    async fn server_rooms(
        &self,
        server_name: &safareig_core::ruma::ServerName,
    ) -> Result<HashSet<safareig_core::ruma::OwnedRoomId>, StorageError> {
        let rows = sqlx::query(
            r#"
            SELECT room_id FROM servers_rooms
            WHERE server_name = ?
            "#,
        )
        .bind(server_name.to_string())
        .fetch_all(&self.pool)
        .await?;

        let servers = rows
            .into_iter()
            .map(|row| row.get::<String, _>("room_id"))
            .map(|room_id| RoomId::parse(room_id).map_err(|_| StorageError::CorruptedData))
            .collect::<Result<HashSet<_>, StorageError>>()?;

        Ok(servers)
    }

    async fn participating_servers(
        &self,
        room_id: &RoomId,
    ) -> Result<Vec<OwnedServerName>, StorageError> {
        let rows = sqlx::query(
            r#"
            SELECT server_name FROM servers_rooms
            WHERE room_id = ?
            "#,
        )
        .bind(room_id.as_str())
        .fetch_all(&self.pool)
        .await?;

        let servers = rows
            .into_iter()
            .map(|row| row.get::<String, _>("server_name"))
            .map(|server| ServerName::parse(server).map_err(|_| StorageError::CorruptedData))
            .collect::<Result<Vec<_>, StorageError>>()?;

        Ok(servers)
    }
}
