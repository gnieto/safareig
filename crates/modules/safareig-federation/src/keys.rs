pub mod api;
pub mod command;
pub mod storage;

use std::{
    borrow::Borrow,
    collections::{BTreeMap, HashSet},
    convert::TryFrom,
    num::NonZeroUsize,
    ops::DerefMut,
    sync::Arc,
    time::SystemTime,
};

use async_trait::async_trait;
use lru::LruCache;
use rand::prelude::SliceRandom;
use rusty_ulid::Ulid;
use safareig_core::{
    error::error_chain,
    ruma::{
        api::federation::discovery::{
            get_remote_server_keys_batch::v2::{self as notary_keys, QueryCriteria},
            get_server_keys::v2 as server_keys,
            OldVerifyKey, ServerSigningKeys, VerifyKey,
        },
        signatures::{sign_json, verify_json, Ed25519KeyPair, PublicKeyMap, PublicKeySet},
        CanonicalJsonValue, KeyName, OwnedServerName, OwnedServerSigningKeyId, ServerName,
        ServerSigningKeyId, SigningKeyAlgorithm,
    },
    storage::StorageError,
    time::{system_time_to_millis, uint_to_system_time},
    ServerInfo,
};
use serde::{de::DeserializeOwned, Serialize};
use tokio::sync::Mutex;

use crate::{
    client::ClientBuilder,
    keys::storage::{Key, KeyType, ServerKeyStorage},
    server::storage::FederationStorage,
    FederationError,
};

/// `KeyProvider` retrieves the requested public keys, either if they are from the local homeserver
/// or if the keys are from a remote
pub struct KeyProvider {
    server_info: ServerInfo,
    local_provider: Arc<LocalKeyProvider>,
    remote_provider: Arc<RemoteKeyProvider>,
}

impl KeyProvider {
    pub fn new(
        server_info: ServerInfo,
        local_provider: Arc<LocalKeyProvider>,
        remote: Arc<RemoteKeyProvider>,
    ) -> Self {
        Self {
            server_info,
            local_provider,
            remote_provider: remote,
        }
    }

    /// `load` will recover the public key of the target key id and server name. If `None` is returned
    /// it means that it could not be recovered from in-memory, main storage or the remote homeserver.
    pub async fn load(
        &self,
        server_name: &ServerName,
        id: &ServerSigningKeyId,
    ) -> Option<Arc<Ed25519PublicKey>> {
        if self.server_info.is_local_server(server_name) {
            self.local_provider.load(id).await
        } else {
            self.remote_provider.load(server_name, id).await
        }
    }

    pub async fn current_key(&self) -> Result<Arc<Ed25519KeyPair>, FederationError> {
        self.local_provider.current_key().await
    }
}

/// `LocalKeyProvider` retrieves (or generates a new) current key for the local homeserver. This key
/// key pair can then be used to sign outgoing request for federation. This can also return all the
/// exiting key pairs to return current and old pairs on the corresponding endpoints.
///
/// Note that this is kept separated from the `RemoteKeyProvider` to prevent a circular
/// dependency.
pub struct LocalKeyProvider {
    storage: Arc<dyn ServerKeyStorage>,
    server_info: ServerInfo,
    mutex: Mutex<Option<(Arc<Ed25519KeyPair>, Option<SystemTime>)>>,
    time_provider: Box<dyn Sync + Send + Fn() -> SystemTime>,
}

impl LocalKeyProvider {
    pub fn new(storage: Arc<dyn ServerKeyStorage>, server_info: ServerInfo) -> Self {
        LocalKeyProvider {
            storage,
            server_info,
            mutex: Default::default(),
            time_provider: Box::new(SystemTime::now),
        }
    }

    pub async fn current_key(&self) -> Result<Arc<Ed25519KeyPair>, FederationError> {
        // Protect this method with a mutex to avoid that more than one key gets generated
        // due to concurrent calls to this method.
        let mut guard = self.mutex.lock().await;
        if let Some(key) = guard.as_ref() {
            return Ok(key.0.clone());
        }

        // We don't have any key cached. Let's find the latest one and generate one if we don't
        // currently have one or if it's outdated.
        let latest_key = self.latest_key().await?;
        if let Some(latest_key) = latest_key {
            match latest_key.expire {
                None => {
                    let key_pair = Arc::new(Ed25519KeyPair::from_der(
                        &latest_key.content,
                        latest_key.id.key_name().to_string(),
                    )?);
                    *guard = Some((key_pair.clone(), None));

                    return Ok(key_pair);
                }
                Some(expire) => {
                    if (self.time_provider)() < expire {
                        let key_pair = Arc::new(Ed25519KeyPair::from_der(
                            &latest_key.content,
                            latest_key.id.key_name().to_string(),
                        )?);
                        *guard = Some((key_pair.clone(), latest_key.expire));

                        return Ok(key_pair);
                    }
                }
            }
        }

        let new_key = Ed25519KeyPair::generate()?;
        let version = Ulid::generate().to_string();
        // TODO: Check this after Ruma upgrade
        let name: Box<KeyName> = version.clone().into();
        let key_id = ServerSigningKeyId::from_parts(SigningKeyAlgorithm::Ed25519, &name);

        let key = Key {
            content: new_key.to_vec(),
            key_type: KeyType::KeyPair,
            id: key_id,
            expire: None,
            valid_until_ts: None,
        };
        self.storage
            .add_server_key(self.server_info.server_name(), &key)
            .await?;

        let key_pair = Arc::new(Ed25519KeyPair::from_der(&new_key, version)?);
        *guard = Some((key_pair.clone(), key.expire));

        Ok(key_pair)
    }

    // TODO: Probably this deserves some caching layer to avoid recreating the public keys every
    // time.
    pub async fn load(&self, id: &ServerSigningKeyId) -> Option<Arc<Ed25519PublicKey>> {
        self.all_keys()
            .await
            .unwrap_or_default()
            .into_iter()
            .find(|key| key.id == id)
            .and_then(|key| {
                let key_pair = Ed25519KeyPair::from_der(&key.content, id.to_string());

                key_pair.ok().and_then(|pair| {
                    Ed25519PublicKey::new(pair.public_key().to_vec(), key.id.to_string())
                        .ok()
                        .map(Arc::new)
                })
            })
    }

    pub async fn all_keys(&self) -> Result<Vec<Key>, FederationError> {
        Ok(self
            .storage
            .get_server_keys(self.server_info.server_name())
            .await?)
    }

    async fn latest_key(&self) -> Result<Option<Key>, FederationError> {
        Ok(self
            .all_keys()
            .await?
            .into_iter()
            .find(|key| key.expire.is_none()))
    }
}

/// `RemoteKeyProvider` handles how keys are fetched from remote servers. When a missing key is
/// requested, it will request to the remote server all the keys and they will be stored on the main
/// corresponding storage.
///
/// It will maintain the last used keys in a LRU cache to prevent hitting either the DB or
/// the remote server.
pub struct RemoteKeyProvider {
    storage: Arc<dyn ServerKeyStorage>,
    key_fetcher: Arc<dyn KeyFetcher>,
    lru: Mutex<LruCache<RemoteKeyId, Arc<Ed25519PublicKey>>>,
}

#[derive(Hash, Eq, PartialEq, Clone)]
struct RemoteKeyId {
    server_name: OwnedServerName,
    version: OwnedServerSigningKeyId,
}

impl From<&RemoteKeyId> for RequestedKeys {
    fn from(remote_key_id: &RemoteKeyId) -> Self {
        let mut requested = RequestedKeys::new();
        let mut keys = HashSet::new();
        keys.insert(remote_key_id.version.clone());
        requested.insert(remote_key_id.server_name.clone(), keys);

        requested
    }
}

impl RemoteKeyProvider {
    pub fn new(server_keys: Arc<dyn ServerKeyStorage>, fetcher: Arc<dyn KeyFetcher>) -> Self {
        RemoteKeyProvider {
            storage: server_keys,
            key_fetcher: fetcher,
            lru: Mutex::new(LruCache::new(NonZeroUsize::new(10).unwrap())),
        }
    }

    /// `load` will recover the public key of the target key id and server name. If `None` is returned
    /// it means that it could not be recovered from in-memory, main storage or the remote homeserver.
    pub async fn load(
        &self,
        server_name: &ServerName,
        id: &ServerSigningKeyId,
    ) -> Option<Arc<Ed25519PublicKey>> {
        let key_id = RemoteKeyId {
            server_name: server_name.to_owned(),
            version: id.to_owned(),
        };

        // The scope of this lock is only to check if we've hit the LRU. In case
        // that the key is missing, we will release the lock and try find the key on
        // the DB or the remote homeserver, but while we are doing this, we don't want to
        // maintain the lock.
        // On a follow-up version, ideally, we would like to prevent that more than one
        // execution flow tries to fetch the remote keys concurrently.
        {
            let mut lock = self.lru.lock().await;
            let lru = lock.deref_mut();
            if let Some(key) = lru.get(&key_id) {
                crate::metrics::KEYS_FROM_CACHE.add(1, &[]);
                return Some(key.clone());
            }
        }

        let mut from_storage = self
            .load_from_storage(&key_id)
            .await
            .map_err(|e| {
                tracing::error!("Could not load key from storage: {}", e);
                e
            })
            .ok()
            .flatten();

        if from_storage.is_none() {
            tracing::info!(
                "Requesting key {} to remote homserver {}",
                key_id.version,
                server_name
            );

            let remote_key = self
                .load_from_remote_homeserver(&key_id)
                .await
                .map_err(|e| {
                    tracing::error!(
                        key = key_id.version.as_str(),
                        remote_server = server_name.as_str(),
                        "Could not load key from remote homeserver: {}",
                        e
                    );
                    e
                })
                .ok()
                .flatten();
            from_storage = remote_key;
            crate::metrics::KEYS_FROM_REMOTE.add(1, &[]);
        }

        let key = from_storage?;
        let mut lock = self.lru.lock().await;
        let lru = lock.deref_mut();
        lru.put(key_id, key.clone());

        Some(key)
    }

    async fn load_from_remote_homeserver(
        &self,
        key_id: &RemoteKeyId,
    ) -> Result<Option<Arc<Ed25519PublicKey>>, FederationError> {
        let requested_keys = RequestedKeys::from(key_id);
        let keys = self.key_fetcher.fetch_keys(&requested_keys).await?;
        let target_key = keys
            .get(&key_id.server_name)
            .and_then(|server_keys| server_keys.get(&key_id.version))
            .map(|key| Ed25519PublicKey::try_from(key).map(Arc::new))
            .transpose()?;

        Ok(target_key)
    }

    async fn load_from_storage(
        &self,
        key_id: &RemoteKeyId,
    ) -> Result<Option<Arc<Ed25519PublicKey>>, FederationError> {
        self.storage
            .get_server_keys(&key_id.server_name)
            .await?
            .into_iter()
            .find(|key| key.id == key_id.version)
            .map(|key| {
                crate::metrics::KEYS_FROM_CACHE.add(1, &[]);
                Ed25519PublicKey::new(key.content.clone(), key.id.key_name().to_string())
                    .map(Arc::new)
            })
            .transpose()
            .map_err(|e| {
                tracing::error!("Error decoding stored key: {}", e);
                StorageError::Custom("Could not decode stored key".to_string()).into()
            })
    }
}

// TODO: Move this to ruma-signatures
pub struct Ed25519PublicKey {
    public_key: Vec<u8>,
    version: String,
}

impl Ed25519PublicKey {
    pub fn new(public_key: Vec<u8>, version: String) -> Result<Self, FederationError> {
        // TODO: Check that public key is a valid

        Ok(Ed25519PublicKey {
            public_key,
            version,
        })
    }

    pub fn version(&self) -> &str {
        self.version.as_ref()
    }

    pub fn public_key(&self) -> &[u8] {
        self.public_key.as_slice()
    }

    pub fn base64(&self) -> safareig_core::ruma::serde::Base64 {
        safareig_core::ruma::serde::Base64::new(self.public_key().to_vec())
    }
}

pub fn sign_ruma_object<T: Serialize, U: DeserializeOwned, K: Borrow<Ed25519KeyPair>>(
    entity: &str,
    object: &T,
    keys: &[K],
) -> Result<U, FederationError> {
    let json_value = serde_json::to_value(object)?;
    let mut canonical = CanonicalJsonValue::try_from(json_value)?
        .into_object()
        .ok_or(FederationError::Unknown)?; // TODO: Change Error tag

    for key in keys {
        sign_json(entity, key.borrow(), &mut canonical)?;
    }

    let serde_value = serde_json::to_value(&canonical).map_err(FederationError::from)?;
    let server_signing_keys: U =
        serde_json::from_value(serde_value).map_err(FederationError::from)?;

    Ok(server_signing_keys)
}

pub fn verify_ruma_object<T: Serialize>(
    object: T,
    keys: &PublicKeyMap,
) -> Result<T, FederationError> {
    let json_value = serde_json::to_value(&object)?;
    let canonical = CanonicalJsonValue::try_from(json_value)?
        .into_object()
        .ok_or(FederationError::Unknown)?; // TODO: Change Error tag

    verify_json(keys, &canonical).map_err(|_| FederationError::InvalidSignature)?;

    Ok(object)
}

pub trait CanonicalJsonValueExt {
    fn as_object(&self) -> Option<&BTreeMap<String, CanonicalJsonValue>>;
    fn into_object(self) -> Option<BTreeMap<String, CanonicalJsonValue>>;
    fn as_string(&self) -> Option<&String>;
}

impl CanonicalJsonValueExt for CanonicalJsonValue {
    fn as_object(&self) -> Option<&BTreeMap<String, CanonicalJsonValue>> {
        match self {
            CanonicalJsonValue::Object(obj) => Some(obj),
            _ => None,
        }
    }

    fn into_object(self) -> Option<BTreeMap<String, CanonicalJsonValue>> {
        match self {
            CanonicalJsonValue::Object(obj) => Some(obj),
            _ => None,
        }
    }

    fn as_string(&self) -> Option<&String> {
        match self {
            CanonicalJsonValue::String(s) => Some(s),
            _ => None,
        }
    }
}

pub fn split_keys(
    server_name: &ServerName,
    keys: Vec<Key>,
) -> Result<ServerSigningKeys, FederationError> {
    let now = SystemTime::now();
    let mut active = BTreeMap::new();
    let mut non_active = BTreeMap::new();
    let mut valid_until_ts = None;

    for key in keys {
        let public_key = Ed25519PublicKey::try_from(&key)?;
        let base64_public_key = public_key.base64();

        match key.expire {
            Some(expire) => {
                if expire < now {
                    let millis_key = system_time_to_millis(expire);
                    let old_verify = OldVerifyKey::new(millis_key, base64_public_key);
                    non_active.insert(key.id.clone(), old_verify);
                } else {
                    let active_key = VerifyKey::new(base64_public_key);
                    active.insert(key.id.clone(), active_key);
                }
            }
            None => {
                let active_key = VerifyKey::new(base64_public_key);
                active.insert(key.id.clone(), active_key);
            }
        }

        valid_until_ts = valid_until_ts.max(key.valid_until_ts);
    }

    let max_valid_time = SystemTime::now() + std::time::Duration::from_secs(3600 * 24);
    let valid_until_ts = valid_until_ts
        .min(Some(max_valid_time))
        .unwrap_or(max_valid_time);

    Ok(ServerSigningKeys {
        server_name: server_name.to_owned(),
        verify_keys: active,
        old_verify_keys: non_active,
        signatures: Default::default(),
        valid_until_ts: system_time_to_millis(valid_until_ts),
    })
}

pub type RequestedKeys = BTreeMap<OwnedServerName, HashSet<OwnedServerSigningKeyId>>;
pub type FetchedKeys = BTreeMap<OwnedServerName, BTreeMap<OwnedServerSigningKeyId, Key>>;

#[async_trait]
pub trait KeyFetcher: Sync + Send {
    async fn fetch_keys(&self, keys: &RequestedKeys) -> Result<FetchedKeys, FederationError>;
}

pub struct ChainedKeyFetcher {
    fetchers: Vec<Arc<dyn KeyFetcher>>,
}

impl ChainedKeyFetcher {
    pub fn new(fetchers: Vec<Arc<dyn KeyFetcher>>) -> Self {
        Self { fetchers }
    }
}

#[async_trait]
impl KeyFetcher for ChainedKeyFetcher {
    async fn fetch_keys(
        &self,
        requested_keys: &RequestedKeys,
    ) -> Result<FetchedKeys, FederationError> {
        for (i, fetcher) in self.fetchers.iter().enumerate() {
            match fetcher.fetch_keys(requested_keys).await {
                Ok(keys) => return Ok(keys),
                Err(e) => {
                    tracing::info!(
                        index = i.to_string(),
                        err = error_chain(&e),
                        "Could not fetch keys with the given fetcher",
                    )
                }
            }
        }

        Err(FederationError::Custom(
            "could not get keys from any KeyFetcher".to_string(),
        ))
    }
}

/// Fetches keys from origin
#[derive(Clone)]
pub struct OriginKeyFetcher {
    client_builder: Arc<ClientBuilder>,
    key_storage: Arc<dyn ServerKeyStorage>,
}

impl OriginKeyFetcher {
    pub fn new(
        client_builder: Arc<ClientBuilder>,
        key_storage: Arc<dyn ServerKeyStorage>,
    ) -> OriginKeyFetcher {
        Self {
            client_builder,
            key_storage,
        }
    }
}

#[async_trait]
impl KeyFetcher for OriginKeyFetcher {
    #[tracing::instrument(skip(self, requested_keys), fields(fetcher = "origin"))]
    async fn fetch_keys(
        &self,
        requested_keys: &RequestedKeys,
    ) -> Result<FetchedKeys, FederationError> {
        let mut fetched_keys = FetchedKeys::new();

        // TODO: This could be dispatched in parallel
        for (server, keys) in requested_keys.iter() {
            // Load from remote server
            let request = server_keys::Request {};
            let response = self
                .client_builder
                .client_for(server)
                .await?
                .request(request)
                .await?;

            handle_incoming_server_keys(
                self.key_storage.as_ref(),
                response.server_key.deserialize()?,
                keys,
                &mut fetched_keys,
            )
            .await;
        }

        Ok(fetched_keys)
    }
}

/// Fetches keys from a notary server
pub struct NotaryKeyFetcher {
    notaries: Vec<OwnedServerName>,
    client_builder: Arc<ClientBuilder>,
    key_storage: Arc<dyn ServerKeyStorage>,
    federation: Arc<dyn FederationStorage>,
}

impl NotaryKeyFetcher {
    pub fn new(
        notaries: Vec<OwnedServerName>,
        client_builder: Arc<ClientBuilder>,
        key_storage: Arc<dyn ServerKeyStorage>,
        federation: Arc<dyn FederationStorage>,
    ) -> Self {
        Self {
            notaries,
            client_builder,
            key_storage,
            federation,
        }
    }

    async fn select_notary(&self) -> Option<OwnedServerName> {
        if !self.notaries.is_empty() {
            Some(
                self.notaries
                    .choose(&mut rand::thread_rng())
                    .cloned()
                    .expect("notaries has at least one element"),
            )
        } else {
            // If no notaries have been configured, select one tracked server at random
            let servers = self.federation.tracked_servers().await.unwrap_or_default();

            servers.choose(&mut rand::thread_rng()).cloned()
        }
    }

    async fn handle_notary_response(
        &self,
        notary: &ServerName,
        mut requested: RequestedKeys,
        response: notary_keys::Response,
    ) -> Result<(RequestedKeys, FetchedKeys), FederationError> {
        let known_keys = self.key_storage.get_server_keys(notary).await?;
        let mut key_map = PublicKeyMap::new();
        let mut key_set = PublicKeySet::new();

        for key in known_keys.into_iter() {
            key_set.insert(
                key.id.to_string(),
                Ed25519PublicKey::try_from(&key)?.base64(),
            );
        }

        key_map.insert(notary.to_string(), key_set);
        let mut fetched_keys = FetchedKeys::new();

        for keys in response.server_keys.into_iter() {
            // Add returned public keys for target server to  current key map. This will allow us to verify
            // the signatures from the notary server and the target server.
            // Responses from notaries, usually are signed by both the notary server and the origin server.
            Self::add_target_keys_to_key_map(&keys, &mut key_map)?;

            let expected_keys = requested
                .remove(&keys.deserialize()?.server_name)
                .unwrap_or_default();
            let verified = verify_ruma_object(keys, &key_map)?.deserialize()?;
            handle_incoming_server_keys(
                self.key_storage.as_ref(),
                verified,
                &expected_keys,
                &mut fetched_keys,
            )
            .await;
        }

        Ok((requested, fetched_keys))
    }

    fn add_target_keys_to_key_map(
        keys: &ruma_common::serde::Raw<ServerSigningKeys>,
        key_map: &mut BTreeMap<String, BTreeMap<String, ruma_common::serde::Base64>>,
    ) -> Result<(), FederationError> {
        let server_signing_keys = keys.deserialize()?;
        let target_server = key_map
            .entry(server_signing_keys.server_name.to_string())
            .or_default();
        for (server_name, public_keys) in server_signing_keys.verify_keys {
            target_server.insert(server_name.to_string(), public_keys.key);
        }
        Ok(())
    }
}

#[async_trait]
impl KeyFetcher for NotaryKeyFetcher {
    #[tracing::instrument(skip(self, requested_keys), fields(fetcher = "notary"))]
    async fn fetch_keys(
        &self,
        requested_keys: &RequestedKeys,
    ) -> Result<FetchedKeys, FederationError> {
        let notary = self
            .select_notary()
            .await
            .ok_or_else(|| FederationError::Custom("could not find any notary".to_string()))?;

        let server_keys = requested_keys
            .iter()
            .map(|(server, keys)| {
                let criteria = keys
                    .iter()
                    .map(|key_id| (key_id.clone(), QueryCriteria::default()))
                    .collect();

                (server.to_owned(), criteria)
            })
            .collect();

        let request = notary_keys::Request { server_keys };

        let response = self
            .client_builder
            .client_for(&notary)
            .await?
            .request(request)
            .await?;

        let (requested, fetched_from_notary) = self
            .handle_notary_response(&notary, requested_keys.clone(), response)
            .await?;

        if !requested.is_empty() {
            Err(FederationError::Custom(
                "could not get keys from notary server".to_string(),
            ))
        } else {
            Ok(fetched_from_notary)
        }
    }
}

async fn handle_incoming_server_keys(
    keys_storage: &dyn ServerKeyStorage,
    server_keys: ServerSigningKeys,
    requested_keys: &HashSet<OwnedServerSigningKeyId>,
    fetched_keys: &mut FetchedKeys,
) {
    let current_server_keys = fetched_keys
        .entry(server_keys.server_name.clone())
        .or_default();

    let valid_until_ts = server_keys.valid_until_ts;
    let valid_until_ts = uint_to_system_time(&valid_until_ts);

    for (current_key_id, verify_key) in server_keys.verify_keys.into_iter() {
        let target_key = handle_incoming_key(
            keys_storage,
            &server_keys.server_name,
            &current_key_id,
            verify_key.key,
            None,
            Some(valid_until_ts),
        )
        .await;

        match target_key {
            Err(e) => {
                tracing::warn!(
                    server = server_keys.server_name.to_string().as_str(),
                    key_id = current_key_id.key_name().as_str(),
                    err = e.to_string().as_str(),
                    "Not storing received key with invalid format",
                );
            }
            Ok(key) => {
                if requested_keys.contains(&current_key_id) {
                    current_server_keys.insert(current_key_id.to_owned(), key);
                }
            }
        }
    }

    for (current_key_id, old_verify_key) in server_keys.old_verify_keys.into_iter() {
        let target_key = handle_incoming_key(
            keys_storage,
            &server_keys.server_name,
            &current_key_id,
            old_verify_key.key,
            Some(uint_to_system_time(&old_verify_key.expired_ts)),
            None,
        )
        .await;

        match target_key {
            Err(e) => {
                tracing::warn!(
                    server = server_keys.server_name.to_string().as_str(),
                    key_id = current_key_id.key_name().as_str(),
                    err = e.to_string().as_str(),
                    "Not storing received key with invalid format",
                );
            }
            Ok(key) => {
                if requested_keys.contains(&current_key_id) {
                    current_server_keys.insert(current_key_id.to_owned(), key);
                }
            }
        }
    }
}

async fn handle_incoming_key(
    key_storage: &dyn ServerKeyStorage,
    server_name: &ServerName,
    current_key_id: &ServerSigningKeyId,
    key: safareig_core::ruma::serde::Base64,
    expire: Option<SystemTime>,
    valid_until: Option<SystemTime>,
) -> Result<Key, FederationError> {
    if expire.is_none() && valid_until.is_none() {
        return Err(FederationError::Custom(
            "can not store a key without expiration and non valid_until".to_string(),
        ));
    }

    let public_key = key
        .as_bytes()
        .try_into()
        .map_err(|_| FederationError::Custom("Invalid lentgh for public key".to_string()))?;

    let _ = ed25519_dalek::VerifyingKey::from_bytes(public_key)
        .map_err(|_| FederationError::Custom("Invalid public key".to_string()))?;

    let key = Key {
        content: key.as_bytes().to_vec(),
        key_type: KeyType::PublicKey,
        id: current_key_id.to_owned(),
        expire,
        valid_until_ts: valid_until,
    };

    key_storage.add_server_key(server_name, &key).await?;

    Ok(key)
}

#[cfg(test)]
mod tests {
    use std::{
        collections::BTreeMap,
        ops::{Add, Deref},
        sync::Arc,
        time::{Duration, SystemTime},
    };

    use rusty_ulid::Ulid;
    use safareig_core::{
        ruma::{
            serde::Base64, signatures::Ed25519KeyPair, KeyName, OwnedServerName,
            OwnedServerSigningKeyId, ServerName, ServerSigningKeyId, SigningKeyAlgorithm,
        },
        ServerInfo,
    };
    use safareig_testing::TestingApp;

    use super::{handle_incoming_key, storage::ServerKeyStorage, FetchedKeys, RequestedKeys};
    use crate::{
        keys::{
            storage::{Key, KeyType},
            KeyFetcher, LocalKeyProvider, RemoteKeyProvider,
        },
        testing::InMemoryServerStorage,
        FederationError,
    };

    #[tokio::test]
    async fn local_keys_are_the_same_on_successive_keys() {
        let config = TestingApp::default_config();
        let in_memory = Arc::new(InMemoryServerStorage::default());
        let local = Arc::new(LocalKeyProvider::new(in_memory, ServerInfo::from(&config)));

        let first_key = local.current_key().await.unwrap();
        let second_key = local.current_key().await.unwrap();

        assert_eq!(first_key.deref().version(), second_key.deref().version());
    }

    #[tokio::test]
    async fn local_keys_are_regenerated_if_keys_gets_expired() {
        let config = TestingApp::default_config();
        let server_info = ServerInfo::from(&config);
        let in_memory = Arc::new(InMemoryServerStorage::default());
        let local = Arc::new(LocalKeyProvider::new(
            in_memory.clone(),
            server_info.clone(),
        ));
        let first_key = local.current_key().await.unwrap();

        let mut key_to_expire = local.all_keys().await.unwrap().pop().unwrap();
        key_to_expire.expire = Some(SystemTime::now());
        in_memory
            .add_server_key(server_info.server_name(), &key_to_expire)
            .await
            .unwrap();

        let local = LocalKeyProvider {
            storage: in_memory.clone(),
            server_info,
            mutex: Default::default(),
            time_provider: Box::new(|| SystemTime::now().add(Duration::from_secs(65 * 24 * 3600))),
        };
        let second_key = local.current_key().await.unwrap();

        assert_ne!(first_key.deref().version(), second_key.deref().version());
    }

    #[tokio::test]
    async fn remote_keys_are_fetched_from_remote_homeserver() {
        let in_memory = Arc::new(InMemoryServerStorage::default());

        let remote_server = ServerName::parse("remoteserver.cat").unwrap();
        let remote_key_provider =
            RemoteKeyProvider::new(in_memory.clone(), Arc::new(HardcodedKeyFetcher {}));
        let target_id = safareig_core::ruma::server_signing_key_id!("ed25519:test");

        let key = remote_key_provider.load(&remote_server, target_id).await;

        assert!(key.is_some());
        assert_eq!(key.unwrap().version(), target_id.as_str());
    }

    #[tokio::test]
    async fn invalid_keys_are_not_stored() {
        let in_memory = Arc::new(InMemoryServerStorage::default());
        let server_name = safareig_core::ruma::server_name!("localhost:8448");
        let key_id = safareig_core::ruma::server_signing_key_id!("ed25519:test_key");
        let content = Base64::new("invalid".as_bytes().to_owned());

        let result = handle_incoming_key(
            in_memory.as_ref(),
            server_name,
            key_id,
            content,
            None,
            Some(SystemTime::now().add(Duration::from_secs(3600))),
        )
        .await;

        assert!(result.is_err());
    }

    #[tokio::test]
    async fn remote_keys_with_failing_homserver_does_not_return_the_requested_key() {
        let in_memory = Arc::new(InMemoryServerStorage::default());

        let remote_key_provider =
            RemoteKeyProvider::new(in_memory.clone(), Arc::new(FailingKeyFetcher {}));
        let remote_server = ServerName::parse("remoteserver.cat").unwrap();
        let version = Ulid::generate().to_string();
        let name: Box<KeyName> = version.clone().into();
        let target_id = ServerSigningKeyId::from_parts(SigningKeyAlgorithm::Ed25519, &name);

        let key = remote_key_provider.load(&remote_server, &target_id).await;

        assert!(key.is_none());
    }

    #[tokio::test]
    async fn remote_keys_are_fetched_from_database_if_exists() {
        let in_memory = Arc::new(InMemoryServerStorage::default());
        let key_fetcher = Arc::new(NonCalledKeyFetcher);

        let remote_key_provider = RemoteKeyProvider::new(in_memory.clone(), key_fetcher);

        let (remote_server, key_id) = add_key_to_storage(in_memory.as_ref()).await;
        let key = remote_key_provider.load(&remote_server, &key_id).await;

        assert!(key.is_some());
    }

    async fn add_key_to_storage(
        keys: &dyn ServerKeyStorage,
    ) -> (OwnedServerName, OwnedServerSigningKeyId) {
        // Manually store a public key on the DB
        let remote_server = ServerName::parse("remoteserver.cat").unwrap();
        let version = Ulid::generate().to_string();
        let name: Box<KeyName> = version.clone().into();

        let raw_key_pair = Ed25519KeyPair::generate().unwrap();
        let key_pair = Ed25519KeyPair::from_der(&raw_key_pair, version.clone()).unwrap();
        let key_id = ServerSigningKeyId::from_parts(SigningKeyAlgorithm::Ed25519, &name);
        let safareig_key = Key {
            content: key_pair.public_key().to_vec(),
            id: key_id.clone(),
            expire: None,
            valid_until_ts: Some(SystemTime::now().add(Duration::from_secs(3600))),
            key_type: KeyType::KeyPair,
        };
        keys.add_server_key(&remote_server, &safareig_key)
            .await
            .unwrap();

        (remote_server, key_id)
    }

    struct NonCalledKeyFetcher;

    #[async_trait::async_trait]
    impl KeyFetcher for NonCalledKeyFetcher {
        async fn fetch_keys(&self, _: &RequestedKeys) -> Result<FetchedKeys, FederationError> {
            panic!("should not be called")
        }
    }

    struct FailingKeyFetcher {}

    #[async_trait::async_trait]
    impl KeyFetcher for FailingKeyFetcher {
        async fn fetch_keys(&self, _: &RequestedKeys) -> Result<FetchedKeys, FederationError> {
            Err(FederationError::Custom(
                "could not fetch keys from remote server".to_string(),
            ))
        }
    }

    pub struct HardcodedKeyFetcher {}

    #[async_trait::async_trait]
    impl KeyFetcher for HardcodedKeyFetcher {
        async fn fetch_keys(
            &self,
            requested_keys: &RequestedKeys,
        ) -> Result<FetchedKeys, FederationError> {
            let first_tuple = requested_keys.iter().next().unwrap();

            let first_key = first_tuple.1.iter().next().unwrap();

            let mut fetched_keys = FetchedKeys::new();
            let mut keys = BTreeMap::new();
            keys.insert(
                first_key.clone(),
                Key {
                    content: vec![],
                    key_type: KeyType::PublicKey,
                    id: first_key.clone(),
                    expire: None,
                    valid_until_ts: Some(SystemTime::now().add(Duration::from_secs(3600))),
                },
            );
            fetched_keys.insert(first_tuple.0.clone(), keys);

            Ok(fetched_keys)
        }
    }
}
