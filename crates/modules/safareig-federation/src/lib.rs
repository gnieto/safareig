pub mod client;
mod error;
pub mod keys;
pub(crate) mod metrics;
mod module;
pub mod server;

#[cfg(test)]
pub mod testing;

pub use error::FederationError;
pub use module::ServerKeysModule;
