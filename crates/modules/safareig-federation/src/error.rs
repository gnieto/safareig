use std::time::Duration;

use reqwest::StatusCode;
use safareig_core::{
    error::RumaExt,
    ruma::{
        api::client::error::{ErrorBody, ErrorKind, RetryAfter},
        OwnedServerName, OwnedServerSigningKeyId,
    },
    storage::StorageError,
};
use thiserror::Error;

use crate::client::ClientError;

#[derive(Error, Debug)]
pub enum FederationError {
    #[error("Unknown error")]
    Unknown,
    #[error("Signature error")]
    Signature(#[from] ::safareig_core::ruma::signatures::Error),
    #[error("Storage error: {0}")]
    Storage(#[from] StorageError),
    #[error("Serde error: {0}")]
    Serde(#[from] serde_json::Error),
    #[error("Canonical serde error: {0}")]
    CanonicalSerde(#[from] safareig_core::ruma::CanonicalJsonError),
    #[error("Error on HTTP client request: {0}")]
    Client(#[from] ClientError),
    #[error("Error on HTTP client request: {0}")]
    Reqwest(#[from] reqwest::Error),
    #[error("Found invalid key {1} on {0} server")]
    InvalidKey(OwnedServerName, OwnedServerSigningKeyId),
    #[error("Invalid signature")]
    InvalidSignature,
    #[error("Request not sent due to circuit-breaker")]
    CircuitBreaker,
    #[error("Request not sent due to rate-limit hit")]
    RateLimited,
    #[error("Federation error: {0}")]
    Custom(String),
}

impl FederationError {
    pub fn matches_status_code(&self, code: StatusCode) -> bool {
        match self {
            Self::Client(ClientError::RumaError(ruma)) => ruma.status_code == code,
            Self::Client(ClientError::HttpError(http_error)) => match http_error.status() {
                Some(req_code) => req_code == code,
                _ => false,
            },
            _ => false,
        }
    }
}

impl From<FederationError> for safareig_core::ruma::api::client::Error {
    fn from(federation_error: FederationError) -> Self {
        #[allow(clippy::match_single_binding)]
        match federation_error {
            FederationError::Client(e) => safareig_core::ruma::api::client::Error::from(e),
            FederationError::RateLimited | FederationError::CircuitBreaker => {
                safareig_core::ruma::api::client::Error {
                    body: ErrorBody::Standard {
                        kind: ErrorKind::LimitExceeded {
                            retry_after: Some(RetryAfter::Delay(Duration::from_secs(10))),
                        },
                        message: federation_error.to_string(),
                    },
                    status_code: StatusCode::TOO_MANY_REQUESTS,
                }
            }
            e => safareig_core::ruma::api::client::Error {
                body: ErrorBody::Standard {
                    kind: ErrorKind::Unknown,
                    message: e.to_string(),
                },
                status_code: StatusCode::INTERNAL_SERVER_ERROR,
            },
        }
    }
}

impl From<FederationError> for safareig_core::ruma::api::error::MatrixError {
    fn from(federation_error: FederationError) -> Self {
        RumaExt::to_generic(safareig_core::ruma::api::client::Error::from(
            federation_error,
        ))
    }
}
