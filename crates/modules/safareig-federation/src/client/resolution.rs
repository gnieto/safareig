use std::{
    net::{IpAddr, SocketAddr},
    str::FromStr,
    sync::Arc,
    time::{Duration, SystemTime},
};

use async_trait::async_trait;
use reqwest::Url;
use safareig_core::ruma::{
    api::{IncomingResponse, MatrixVersion, OutgoingRequest, SendAccessToken},
    exports::http,
    OwnedServerName, ServerName,
};
use tracing::instrument;
use trust_dns_resolver::{proto::rr::rdata::SRV, TokioAsyncResolver};

use crate::{
    server::storage::{Server, ServerStorage, WellKnown},
    FederationError,
};

pub const CONSIDERING_VERSIONS: &[MatrixVersion] = &[
    MatrixVersion::V1_0,
    MatrixVersion::V1_1,
    MatrixVersion::V1_2,
    MatrixVersion::V1_3,
    MatrixVersion::V1_4,
    MatrixVersion::V1_5,
    MatrixVersion::V1_6,
    MatrixVersion::V1_7,
    MatrixVersion::V1_8,
];

/// ServerNameResolution contains the results of applying the Server Discovery algorithm described
/// on the federation spec.
pub struct ServerNameResolution {
    /// Final host to connect to when issuing federation requests
    pub host: String,
    /// Depending on the used discovery rule, a specific host name should be used
    pub host_header: String,
    /// Depending on the used discovery rule, a specific TLS server name should be used.
    pub tls_server_name: String,

    http: bool,
}

impl ServerNameResolution {
    pub fn http(host: String) -> Self {
        ServerNameResolution {
            host: host.clone(),
            host_header: host.clone(),
            tls_server_name: host,
            http: true,
        }
    }

    pub fn endpoint(&self) -> String {
        if self.http {
            format!("http://{}/", self.host)
        } else {
            format!("https://{}/", self.host)
        }
    }
}

impl From<ServerNameDetails> for ServerNameResolution {
    fn from(details: ServerNameDetails) -> Self {
        let host_header = match details.port {
            Some(_) => details.hostname(),
            None => details.host.to_string(),
        };

        ServerNameResolution {
            host: details.hostname(),
            host_header,
            tls_server_name: details.host.to_string(),
            http: false,
        }
    }
}

#[async_trait]
pub trait ServerDiscovery: Send + Sync {
    async fn resolve(&self, remote: &ServerName) -> Result<ServerNameResolution, FederationError>;
}

pub(crate) struct NoServerNameResolver;

#[async_trait]
impl ServerDiscovery for NoServerNameResolver {
    async fn resolve(&self, remote: &ServerName) -> Result<ServerNameResolution, FederationError> {
        Ok(ServerNameResolution {
            host: remote.to_string(),
            host_header: remote.to_string(),
            tls_server_name: remote.to_string(),
            http: true,
        })
    }
}

#[async_trait]
pub trait SrvLookup: Send + Sync {
    async fn lookup(&self, query: &str) -> Result<Option<SRV>, FederationError>;
}

#[async_trait]
impl SrvLookup for TokioAsyncResolver {
    async fn lookup(&self, query: &str) -> Result<Option<SRV>, FederationError> {
        let srv = self
            .srv_lookup(query)
            .await
            .map_err(|e| {
                tracing::debug!(error = e.to_string(), "Could not lookup via SRV");

                e
            })
            .ok()
            .and_then(|lookup| lookup.into_iter().next());

        Ok(srv)
    }
}

#[async_trait]
pub trait WellKnownResolver: Send + Sync {
    async fn well_known(&self, server: &ServerName) -> Result<OwnedServerName, FederationError>;
}

pub struct RumaClientWellKnown;

#[async_trait]
impl WellKnownResolver for RumaClientWellKnown {
    async fn well_known(&self, server: &ServerName) -> Result<OwnedServerName, FederationError> {
        let uri = format!("https://{server}/");
        let client = WellKnownClient {
            client: reqwest::Client::new(),
            remote: uri.clone(),
        };

        let request =
            safareig_core::ruma::api::federation::discovery::discover_homeserver::Request::new();
        let response = client.request(request).await.map_err(|e| {
            tracing::error!("{}", e.to_string());
            FederationError::Custom("Well known request failed".to_string())
        })?;

        Ok(response.server)
    }
}

// TODO: Use ruma client if send request methods are Send+Sync
struct WellKnownClient {
    client: reqwest::Client,
    remote: String,
}

impl WellKnownClient {
    pub async fn request<R: OutgoingRequest>(
        &self,
        request: R,
    ) -> Result<R::IncomingResponse, FederationError> {
        let http_r: http::Request<Vec<u8>> = request
            .try_into_http_request(
                self.remote.as_str(),
                SendAccessToken::None,
                CONSIDERING_VERSIONS,
            )
            .unwrap();
        let (parts, body) = http_r.into_parts();

        let request = self
            .client
            .request(parts.method, Url::from_str(&parts.uri.to_string()).unwrap())
            .body(body)
            .build()
            .unwrap();

        let result = self.client.execute(request).await?.error_for_status()?;

        let bytes = result.bytes().await?.to_vec();

        let http_resp = http::Response::new(bytes);
        let response: R::IncomingResponse = R::IncomingResponse::try_from_http_response(http_resp)
            .map_err(|e| FederationError::Custom(e.to_string()))?;

        Ok(response)
    }
}

pub(crate) struct FederationServerDiscovery {
    server_storage: Arc<dyn ServerStorage>,
    well_known: Arc<dyn WellKnownResolver>,
    resolver: Arc<dyn SrvLookup>,
}

#[async_trait]
impl ServerDiscovery for FederationServerDiscovery {
    #[instrument(skip(self))]
    async fn resolve(
        &self,
        server_name: &ServerName,
    ) -> Result<ServerNameResolution, FederationError> {
        let mut details = ServerNameDetails::from(server_name);

        if details.is_hostname_without_port() {
            if let Some(delegated_name) = self.well_known_delegation(server_name).await? {
                details = ServerNameDetails::from(delegated_name.as_ref());
            }
        } else {
            return Ok(details.into());
        }

        if details.is_hostname_without_port() {
            self.srv_resolution(details.server_name.as_ref()).await
        } else {
            Ok(details.into())
        }
    }
}

impl FederationServerDiscovery {
    pub fn new(
        server_storage: Arc<dyn ServerStorage>,
        well_known: Arc<dyn WellKnownResolver>,
        resolver: Arc<dyn SrvLookup>,
    ) -> Self {
        Self {
            server_storage,
            well_known,
            resolver,
        }
    }

    async fn well_known_delegation(
        &self,
        server_name: &ServerName,
    ) -> Result<Option<OwnedServerName>, FederationError> {
        let mut server = self.server_storage.load_server(server_name).await?;
        let well_known = self.well_known_resolution(server_name, &mut server).await?;
        let maybe_resolution = match &well_known {
            WellKnown::Resolved(_, _, resolved) => Ok(Some(resolved.clone())),
            WellKnown::Errored(_) => Ok(None),
        };

        server.set_well_known(well_known);
        self.server_storage
            .store_server(server_name, &server)
            .await?;

        maybe_resolution
    }

    async fn well_known_resolution(
        &self,
        server_name: &ServerName,
        server: &mut Server,
    ) -> Result<WellKnown, FederationError> {
        let well_known = server.well_known(SystemTime::now());

        let well_known = match well_known {
            None => {
                let response = self.well_known.well_known(server_name).await;

                match response {
                    Err(error) => {
                        tracing::error!("Well-known error: {}", error);
                        WellKnown::Errored(SystemTime::now())
                    }
                    Ok(response) => WellKnown::Resolved(
                        SystemTime::now(),
                        Duration::from_secs(8 * 3600),
                        response,
                    ),
                }
            }
            Some(well_known) => well_known,
        };

        Ok(well_known)
    }

    async fn srv_resolution(
        &self,
        server_name: &ServerName,
    ) -> Result<ServerNameResolution, FederationError> {
        let matrix_fed = self
            .srv_resolution_for_name(server_name, "matrix-fed")
            .await?;
        if let Some(resolution) = matrix_fed {
            return Ok(resolution);
        }

        let matrix = self.srv_resolution_for_name(server_name, "matrix").await?;
        if let Some(resolution) = matrix {
            return Ok(resolution);
        }

        Ok(ServerNameResolution {
            host: format!("{}:{}", server_name, 8448),
            host_header: server_name.to_string(),
            tls_server_name: server_name.to_string(),
            http: false,
        })
    }

    async fn srv_resolution_for_name(
        &self,
        server_name: &ServerName,
        iana: &str,
    ) -> Result<Option<ServerNameResolution>, FederationError> {
        let srv = self
            .resolver
            .lookup(&format!("_{iana}._tcp.{server_name}"))
            .await?;

        match srv {
            Some(srv) => {
                let srv_server_name = ServerName::parse(format!(
                    "{}:{}",
                    srv.target().to_string().trim_matches('.'),
                    srv.port()
                ))
                .map_err(|_e| {
                    tracing::error!("invalid SRV found: {:?}", srv);
                    FederationError::Unknown
                })?;

                Ok(Some(ServerNameResolution {
                    host: srv_server_name.to_string(),
                    host_header: server_name.to_string(),
                    tls_server_name: server_name.to_string(),
                    http: false,
                }))
            }
            None => Ok(None),
        }
    }
}

struct ServerNameDetails {
    host: ServerNameHost,
    port: Option<u16>,
    server_name: OwnedServerName,
}

impl ServerNameDetails {
    fn is_hostname_without_port(&self) -> bool {
        matches!(self.host, ServerNameHost::Hostname(_)) && self.port.is_none()
    }

    fn hostname(&self) -> String {
        format!("{}:{}", self.host.to_string(), self.port.unwrap_or(8448))
    }
}

impl From<&ServerName> for ServerNameDetails {
    fn from(server_name: &ServerName) -> Self {
        if let Ok(details) = server_name.as_str().parse::<SocketAddr>() {
            return ServerNameDetails {
                host: ServerNameHost::Ip(details.ip()),
                port: Some(details.port()),
                server_name: server_name.to_owned(),
            };
        }

        if let Ok(ip_address) = server_name.as_str().parse::<IpAddr>() {
            return ServerNameDetails {
                host: ServerNameHost::Ip(ip_address),
                port: None,
                server_name: server_name.to_owned(),
            };
        }

        if server_name.as_str().contains(':') {
            // There is no need to check that port is a number: If server name was an IP, it was
            // detected by the previous rule.
            // On the other hand, Ruma is validating itself that a non-ip server name with port, has
            // a valid port number. For example, server_name!("server.cat:qqq") is not a valid
            // server name.

            let mut server_name_split = server_name.as_str().split(':');
            let hostname = server_name_split.next().unwrap().to_string();
            let port = server_name_split.next().unwrap().parse::<u16>().unwrap();

            ServerNameDetails {
                host: ServerNameHost::Hostname(hostname),
                port: Some(port),
                server_name: server_name.to_owned(),
            }
        } else {
            ServerNameDetails {
                host: ServerNameHost::Hostname(server_name.to_string()),
                port: None,
                server_name: server_name.to_owned(),
            }
        }
    }
}

#[derive(Debug)]
enum ServerNameHost {
    Ip(IpAddr),
    Hostname(String),
}

impl ToString for ServerNameHost {
    fn to_string(&self) -> String {
        match self {
            ServerNameHost::Ip(ip_address) => ip_address.to_string(),
            ServerNameHost::Hostname(hostname) => hostname.to_string(),
        }
    }
}

#[cfg(test)]
mod tests {
    use std::{
        str::FromStr,
        sync::Arc,
        time::{Duration, SystemTime},
    };

    use async_trait::async_trait;
    use safareig_core::ruma::{server_name, OwnedServerName, ServerName};
    use trust_dns_resolver::{proto::rr::rdata::SRV, Name};

    use super::{
        FederationServerDiscovery, ServerDiscovery, SrvLookup, WellKnown, WellKnownResolver,
    };
    use crate::{
        server::storage::{Server, ServerStorage},
        testing::InMemoryServerStorage,
        FederationError,
    };

    struct HardcodedSrvLookup;

    #[async_trait]
    impl SrvLookup for HardcodedSrvLookup {
        async fn lookup(&self, query: &str) -> Result<Option<SRV>, FederationError> {
            match query {
                "_matrix-fed._tcp.iana" => Ok(Some(SRV::new(
                    0,
                    1,
                    8448,
                    Name::from_str("srv-iana").unwrap(),
                ))),
                "_matrix._tcp.another.cat" => Ok(Some(SRV::new(
                    0,
                    1,
                    8448,
                    Name::from_str("srv-another.cat").unwrap(),
                ))),
                "_matrix._tcp.server.cat" => Ok(Some(SRV::new(
                    0,
                    1,
                    8448,
                    Name::from_str("srv-server.cat").unwrap(),
                ))),
                _ => Ok(None),
            }
        }
    }

    struct MockedWellKnown {
        well_known: OwnedServerName,
    }

    #[async_trait]
    impl WellKnownResolver for MockedWellKnown {
        async fn well_known(
            &self,
            _server: &ServerName,
        ) -> Result<OwnedServerName, FederationError> {
            Ok(self.well_known.clone())
        }
    }

    #[tokio::test]
    async fn literal_ip() {
        let in_memory = Arc::new(InMemoryServerStorage::default());
        let ip_with_port = server_name!("127.0.0.1:8998");
        let well_known = server_name!("another.cat");
        let server_resolution = prepare_test(well_known, in_memory).await;

        let server_name = server_resolution.resolve(ip_with_port).await.unwrap();
        assert_eq!("127.0.0.1:8998", server_name.host.as_str());
        assert_eq!("127.0.0.1:8998", server_name.host_header.as_str());
        assert_eq!("127.0.0.1", server_name.tls_server_name.as_str());

        let ip_without_port = server_name!("127.0.0.1");
        let server_name = server_resolution.resolve(ip_without_port).await.unwrap();
        assert_eq!("127.0.0.1:8448", server_name.host.as_str());
        assert_eq!("127.0.0.1", server_name.host_header.as_str());
        assert_eq!("127.0.0.1", server_name.tls_server_name.as_str());
    }

    #[tokio::test]
    async fn hostname_with_port() {
        let in_memory = Arc::new(InMemoryServerStorage::default());
        let hostname_with_port = server_name!("server.cat:8998");
        let well_known = server_name!("another.cat");
        let server_resolution = prepare_test(well_known, in_memory).await;

        let server_name = server_resolution.resolve(hostname_with_port).await.unwrap();

        assert_eq!("server.cat:8998", server_name.host.as_str());
        assert_eq!("server.cat:8998", server_name.host_header.as_str());
        assert_eq!("server.cat", server_name.tls_server_name.as_str());
    }

    #[tokio::test]
    async fn cached_well_known_resolution() {
        let in_memory = Arc::new(InMemoryServerStorage::default());
        let remote = server_name!("server.cat");
        let well_known = server_name!("another.cat");
        let server_resolution = prepare_test(well_known, in_memory.clone()).await;

        let mut server = Server::default();
        let well_known = WellKnown::Resolved(
            SystemTime::now(),
            Duration::from_secs(300),
            ServerName::parse("localhost:8448").unwrap(),
        );
        server.set_well_known(well_known);
        in_memory.store_server(remote, &server).await.unwrap();

        let resolution = server_resolution.resolve(remote).await.unwrap();

        // If `another.cat` is returned means that well known was hit
        assert_eq!(resolution.host, "localhost:8448");
    }

    #[tokio::test]
    async fn cached_recently_errored_well_known_resolution() {
        let in_memory = Arc::new(InMemoryServerStorage::default());
        let remote = server_name!("server.cat");
        let well_known = server_name!("another.cat");
        let server_resolution = prepare_test(well_known, in_memory.clone()).await;

        let mut server = Server::default();
        let well_known = WellKnown::Errored(SystemTime::now());
        server.set_well_known(well_known);
        in_memory.store_server(remote, &server).await.unwrap();

        let resolution = server_resolution.resolve(remote).await.unwrap();

        assert_eq!(resolution.host, "srv-server.cat:8448");
        assert_eq!(resolution.host_header, "server.cat");
        assert_eq!(resolution.tls_server_name, "server.cat");
    }

    #[tokio::test]
    async fn cached_long_time_ago_errored_well_known_resolution() {
        let in_memory = Arc::new(InMemoryServerStorage::default());
        let remote = server_name!("server.cat");
        let well_known = server_name!("127.0.0.1");
        let server_resolution = prepare_test(well_known, in_memory.clone()).await;

        let mut server = Server::default();
        let well_known = WellKnown::Errored(
            SystemTime::now()
                .checked_sub(Duration::from_secs(50 * 3600))
                .unwrap(),
        );
        server.set_well_known(well_known);
        in_memory.store_server(remote, &server).await.unwrap();

        let resolution = server_resolution.resolve(remote).await.unwrap();

        assert_eq!(resolution.host, "127.0.0.1:8448");
        assert_eq!(resolution.host_header, "127.0.0.1");
        assert_eq!(resolution.tls_server_name, "127.0.0.1");
    }

    #[tokio::test]
    async fn non_cached_well_known_resolution() {
        let in_memory = Arc::new(InMemoryServerStorage::default());
        let remote = server_name!("server.cat");
        let well_known = server_name!("127.0.0.1");
        let server_resolution = prepare_test(well_known, in_memory).await;

        let resolution = server_resolution.resolve(remote).await.unwrap();

        assert_eq!(resolution.host, "127.0.0.1:8448");
        assert_eq!(resolution.host_header, "127.0.0.1");
        assert_eq!(resolution.tls_server_name, "127.0.0.1");
    }

    #[tokio::test]
    async fn srv_correct_resolution() {
        let in_memory = Arc::new(InMemoryServerStorage::default());
        let remote = server_name!("server.cat");
        let server_resolution = prepare_non_well_known_test(remote, in_memory.clone()).await;

        let resolution = server_resolution.resolve(remote).await.unwrap();

        assert_eq!(resolution.host, "srv-server.cat:8448");
        assert_eq!(resolution.host_header, "server.cat");
        assert_eq!(resolution.tls_server_name, "server.cat");
    }

    #[tokio::test]
    async fn srv_correct_resolution_matrix_fed() {
        let in_memory = Arc::new(InMemoryServerStorage::default());
        let remote = server_name!("iana");
        let server_resolution = prepare_non_well_known_test(remote, in_memory.clone()).await;

        let resolution = server_resolution.resolve(remote).await.unwrap();

        assert_eq!(resolution.host, "srv-iana:8448");
        assert_eq!(resolution.host_header, "iana");
        assert_eq!(resolution.tls_server_name, "iana");
    }

    #[tokio::test]
    async fn default_resolution_to_sever_name_on_8448() {
        let in_memory = Arc::new(InMemoryServerStorage::default());
        let remote = server_name!("no-srv-server.cat");
        let server_resolution = prepare_non_well_known_test(remote, in_memory.clone()).await;

        let resolution = server_resolution.resolve(remote).await.unwrap();

        assert_eq!(resolution.host, "no-srv-server.cat:8448");
        assert_eq!(resolution.host_header, "no-srv-server.cat");
        assert_eq!(resolution.tls_server_name, "no-srv-server.cat");
    }

    #[tokio::test]
    async fn well_known_resolution_with_srv_resolution() {
        let in_memory = Arc::new(InMemoryServerStorage::default());
        let remote = server_name!("server.cat");
        let well_known = server_name!("another.cat");
        let server_resolution = prepare_test(well_known, in_memory).await;

        let resolution = server_resolution.resolve(remote).await.unwrap();

        assert_eq!(resolution.host, "srv-another.cat:8448");
        assert_eq!(resolution.host_header, "another.cat");
        assert_eq!(resolution.tls_server_name, "another.cat");
    }

    async fn prepare_test(
        delegated: &ServerName,
        server_storage: Arc<dyn ServerStorage>,
    ) -> FederationServerDiscovery {
        FederationServerDiscovery {
            server_storage,
            well_known: Arc::new(MockedWellKnown {
                well_known: delegated.to_owned(),
            }),
            resolver: Arc::new(HardcodedSrvLookup),
        }
    }

    async fn prepare_non_well_known_test(
        target: &ServerName,
        server_storage: Arc<dyn ServerStorage>,
    ) -> FederationServerDiscovery {
        FederationServerDiscovery {
            server_storage,
            well_known: Arc::new(MockedWellKnown {
                well_known: target.to_owned(),
            }),
            resolver: Arc::new(HardcodedSrvLookup),
        }
    }
}
