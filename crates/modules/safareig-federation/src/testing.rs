use std::{
    collections::HashMap,
    sync::{Arc, Mutex},
};

use safareig_core::{ruma::ServerName, storage::StorageError};

use crate::{
    keys::storage::{Key, ServerKeyStorage},
    server::storage::{Server, ServerStorage},
};

#[derive(Default)]
pub struct InMemoryServerStorage {
    servers: Arc<Mutex<HashMap<String, Server>>>,
    keys: Arc<Mutex<HashMap<String, HashMap<String, Key>>>>,
}

#[async_trait::async_trait]
impl ServerStorage for InMemoryServerStorage {
    async fn store_server(
        &self,
        server_name: &safareig_core::ruma::ServerName,
        server: &Server,
    ) -> Result<(), StorageError> {
        let mut lock = self.servers.lock().unwrap();
        (*lock).insert(server_name.to_string(), server.clone());

        Ok(())
    }

    async fn load_server(
        &self,
        server_name: &safareig_core::ruma::ServerName,
    ) -> Result<Server, StorageError> {
        let server_name = server_name.to_string();
        let lock = self.servers.lock().unwrap();

        let server = lock.get(&server_name).cloned().unwrap_or_default();

        Ok(server)
    }
}

#[async_trait::async_trait]
impl ServerKeyStorage for InMemoryServerStorage {
    async fn get_server_keys(&self, server_name: &ServerName) -> Result<Vec<Key>, StorageError> {
        let mut lock = self.keys.lock().unwrap();
        let keys = lock.entry(server_name.to_string()).or_default();

        Ok(keys.values().cloned().collect())
    }

    async fn add_server_key(
        &self,
        server_name: &ServerName,
        key: &Key,
    ) -> Result<(), StorageError> {
        let mut lock = self.keys.lock().unwrap();
        let keys = lock.entry(server_name.to_string()).or_default();
        let _key_entry = keys.insert(key.id.to_string(), key.clone());

        Ok(())
    }
}
