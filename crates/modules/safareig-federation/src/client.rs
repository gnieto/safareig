pub mod resolution;

use std::{
    collections::HashMap, convert::TryFrom, num::NonZeroU32, ops::Deref, str::FromStr, sync::Arc,
    time::Duration,
};

use failsafe::futures::CircuitBreaker;
use futures::{Future, FutureExt, TryFutureExt};
use governor::RateLimiter;
use opentelemetry::{global, propagation::Injector, KeyValue};
use reqwest::{
    header::{HeaderName, HeaderValue},
    Request, StatusCode, Url,
};
use safareig_core::{
    config::{FederationConfig, HomeserverConfig},
    error::error_chain,
    ruma::{
        api::{
            client::error::{ErrorBody, ErrorKind},
            EndpointError, IncomingResponse, MatrixVersion, OutgoingRequest, SendAccessToken,
        },
        exports::http,
        signatures::sign_json,
        CanonicalJsonObject, CanonicalJsonValue, OwnedServerName, ServerName,
    },
};
use serde::de::DeserializeOwned;
use thiserror::Error;
use tokio::sync::RwLock;
use tracing::instrument;
use tracing_opentelemetry::OpenTelemetrySpanExt;

use crate::{
    client::resolution::ServerDiscovery,
    keys::LocalKeyProvider,
    metrics::{RATE_LIMITED_CALLS, REJECTED_CALLS},
    FederationError,
};

pub const CONSIDERING_VERSIONS: &[MatrixVersion] = &[
    MatrixVersion::V1_0,
    MatrixVersion::V1_1,
    MatrixVersion::V1_2,
    MatrixVersion::V1_3,
    MatrixVersion::V1_4,
    MatrixVersion::V1_5,
    MatrixVersion::V1_6,
    MatrixVersion::V1_7,
    MatrixVersion::V1_8,
];

pub struct ClientBuilder {
    keys: Arc<LocalKeyProvider>,
    config: Arc<HomeserverConfig>,
    clients: Arc<RwLock<HashMap<OwnedServerName, FederationClient>>>,
    server_discovery: Arc<dyn ServerDiscovery>,
}

impl ClientBuilder {
    pub fn new(
        keys: Arc<LocalKeyProvider>,
        config: Arc<HomeserverConfig>,
        server_discovery: Arc<dyn ServerDiscovery>,
    ) -> Self {
        ClientBuilder {
            keys,
            config,
            clients: Default::default(),
            server_discovery,
        }
    }

    pub async fn client_for(
        &self,
        remote: &ServerName,
    ) -> Result<FederationClient, FederationError> {
        if remote == self.config.server_name.deref() {
            tracing::error!(
                remote = remote.as_str(),
                me = self.config.server_name.as_str(),
                "Can not build a client for local server name",
            );

            panic!("can not build a client for local server name")
        }

        // TODO: Avoid load the same remote twice
        let mut clients = self.clients.write().await;

        if !clients.contains_key(remote) {
            #[cfg(not(feature = "sytest"))]
            let http_client = reqwest::ClientBuilder::new()
                .danger_accept_invalid_certs(self.config.federation.skip_insecure)
                .connect_timeout(Duration::from_secs(30))
                .timeout(Duration::from_secs(120));

            #[cfg(feature = "sytest")]
            // I've needed to add this special configuration which forces http1 during sytest.
            // I was experiencing a 40ms delay for each federation request between safareig servers during sytest
            // executions.
            // After some research, I found this article which talks about a similar issue: https://vorner.github.io/2020/11/06/40-ms-bug.html, but it
            // does not match exactly with what I'm experiencing.
            // After a lot of tweaking, apparently, using http1 make the problem go away, but I was not experiencing the same in a
            // live server.
            // By now, this solves the issue, but this is something I should spend some time to fully understand why the issue
            // appears with http2 (client and server) + tls enabled (server).
            let http_client = reqwest::ClientBuilder::new()
                .danger_accept_invalid_certs(self.config.federation.skip_insecure)
                .connect_timeout(Duration::from_secs(30))
                .timeout(Duration::from_secs(120))
                .http1_only();

            let server_resolution = self.server_discovery.resolve(remote).await?;

            clients.insert(
                remote.to_owned(),
                FederationClient::new(
                    http_client.build()?,
                    &self.config.server_name,
                    remote,
                    server_resolution.endpoint(),
                    self.keys.clone(),
                    &self.config.federation,
                ),
            );
        }

        Ok(clients.get(remote).unwrap().clone())
    }
}

#[derive(Error, Debug)]
pub enum ClientError {
    #[error("Unknown error")]
    Unknown,
    #[error("No key available")]
    NoKeyAvailable,
    #[error("Sign error")]
    SignError(#[from] ::safareig_core::ruma::signatures::Error),
    #[error("HTTP error: {0}")]
    HttpError(#[from] ::reqwest::Error),
    #[error("Ruma error: {0}")]
    RumaError(#[from] safareig_core::ruma::api::client::Error),
}

impl From<ClientError> for safareig_core::ruma::api::client::Error {
    fn from(ce: ClientError) -> Self {
        match ce {
            ClientError::RumaError(e) => e,
            e => safareig_core::ruma::api::client::Error {
                body: ErrorBody::Standard {
                    kind: ErrorKind::Unknown,
                    message: e.to_string(),
                },
                status_code: StatusCode::INTERNAL_SERVER_ERROR,
            },
        }
    }
}

type CircuitBreakerType = failsafe::StateMachine<
    failsafe::failure_policy::ConsecutiveFailures<failsafe::backoff::Exponential>,
    MetricsInstrumentator,
>;
type RateLimitType = governor::RateLimiter<
    governor::state::NotKeyed,
    governor::state::InMemoryState,
    governor::clock::MonotonicClock,
>;

#[derive(Clone)]
pub struct FederationClient {
    client: reqwest::Client,
    local_server: OwnedServerName,
    remote_server: OwnedServerName,
    remote_endpoint: String,
    key_provider: Arc<LocalKeyProvider>,
    circuit_breaker: CircuitBreakerType,
    rate_limit: Arc<RateLimitType>,
}

impl FederationClient {
    pub fn new(
        client: reqwest::Client,
        local_server: &ServerName,
        remote_server: &ServerName,
        remote_endpoint: String,
        key_provider: Arc<LocalKeyProvider>,
        config: &FederationConfig,
    ) -> FederationClient {
        // TODO: Resolve well known for remote_server
        // Create an exponential growth backoff which starts from 10s and ends with 60s.
        let backoff =
            failsafe::backoff::exponential(Duration::from_secs(1), Duration::from_secs(60));
        let cb = failsafe::Config::new()
            .failure_policy(failsafe::failure_policy::consecutive_failures(
                config.circuit_breaker_consecutive_failures,
                backoff,
            ))
            .instrument(MetricsInstrumentator {
                remote_server: remote_server.to_owned(),
            })
            .build();

        let quota =
            governor::Quota::per_minute(NonZeroU32::new(config.rate_limit_quota_minute).unwrap());
        let rate_limit = RateLimiter::direct(quota);

        FederationClient {
            client,
            local_server: local_server.to_owned(),
            remote_server: remote_server.to_owned(),
            remote_endpoint,
            key_provider,
            circuit_breaker: cb,
            rate_limit: Arc::new(rate_limit),
        }
    }

    #[instrument(skip(self, request), fields(remote = self.remote_server.as_str(), path = R::METADATA.history.all_paths().next(), remote = %self.remote_server))]
    pub async fn auth_request<'a, R: OutgoingRequest>(
        &self,
        request: R,
    ) -> Result<R::IncomingResponse, FederationError> {
        let http_request = self.convert_to_http_request(request)?;
        let body = self
            .safe_and_limited_future(self.internal_auth_request(http_request))
            .await?;

        let http_resp = http::Response::new(body);
        let response: R::IncomingResponse = R::IncomingResponse::try_from_http_response(http_resp)
            .map_err(|e| {
                tracing::error!(err = e.to_string().as_str(), "Serialization error",);
                ClientError::Unknown
            })?;

        Ok(response)
    }

    #[instrument(skip(self, request), fields(remote = self.remote_server.as_str(), path = R::METADATA.history.all_paths().next()))]
    pub async fn auth_request_custom<R: OutgoingRequest, O: DeserializeOwned>(
        &self,
        request: R,
    ) -> Result<O, FederationError> {
        let http_request = self.convert_to_http_request(request)?;
        let body = self
            .safe_and_limited_future(self.internal_auth_request(http_request))
            .await?;

        let response: O = serde_json::from_slice(&body).map_err(|e| {
            tracing::error!(err = e.to_string().as_str(), "Serialization error",);

            ClientError::Unknown
        })?;

        Ok(response)
    }

    #[instrument(skip(self, request), fields(remote = self.remote_server.as_str(), path = R::METADATA.history.all_paths().next()))]
    pub async fn request<R: OutgoingRequest>(
        &self,
        request: R,
    ) -> Result<R::IncomingResponse, FederationError> {
        let http_r: http::Request<Vec<u8>> = request
            .try_into_http_request(
                self.remote_endpoint.as_str(),
                SendAccessToken::None,
                CONSIDERING_VERSIONS,
            )
            .unwrap();
        let (parts, body) = http_r.into_parts();
        let uri_str = parts.uri.to_string();
        let request = self
            .client
            .request(parts.method, Url::from_str(&uri_str).unwrap())
            .header(http::header::CONTENT_TYPE, "application/json")
            .body(body)
            .build()
            .unwrap();

        let result = self
            .safe_and_limited_future(
                self.client
                    .execute(request)
                    .map(|r| r.and_then(|response| response.error_for_status()))
                    .map_err(ClientError::from),
            )
            .await?;

        let bytes = result.bytes().await.map_err(ClientError::from)?.to_vec();

        let http_resp = http::Response::new(bytes);
        let response: R::IncomingResponse = R::IncomingResponse::try_from_http_response(http_resp)
            .map_err(|_| ClientError::Unknown)?;

        Ok(response)
    }

    fn convert_to_http_request<R: OutgoingRequest>(
        &self,
        request: R,
    ) -> Result<http::Request<Vec<u8>>, ClientError> {
        request
            .try_into_http_request(
                self.remote_endpoint.as_str(),
                SendAccessToken::None,
                CONSIDERING_VERSIONS,
            )
            .map_err(|error| {
                tracing::error!(
                    error = error_chain(&error),
                    "error converting request to http request",
                );
                ClientError::Unknown
            })
    }

    async fn internal_auth_request(
        &self,
        http_r: http::Request<Vec<u8>>,
    ) -> Result<Vec<u8>, ClientError> {
        let (parts, body) = http_r.into_parts();
        let uri_str = parts.uri.to_string();

        let key = self.key_provider.current_key().await.map_err(|e| {
            tracing::error!(
                err = e.to_string().as_str(),
                "could not generate a local key"
            );

            ClientError::NoKeyAvailable
        })?;

        let mut content = CanonicalJsonObject::new();
        content.insert(
            "method".to_string(),
            CanonicalJsonValue::String(parts.method.to_string()),
        );
        content.insert(
            "uri".to_string(),
            CanonicalJsonValue::String(
                parts
                    .uri
                    .path_and_query()
                    .map(|pq| pq.to_string())
                    .unwrap_or_default(),
            ),
        );
        content.insert(
            "origin".to_string(),
            CanonicalJsonValue::String(self.local_server.to_string()),
        );
        content.insert(
            "destination".to_string(),
            CanonicalJsonValue::String(self.remote_server.to_string()),
        );

        if !body.is_empty() {
            let body_value: serde_json::Value = serde_json::from_slice(body.as_slice()).unwrap();
            let canonical_body = CanonicalJsonValue::try_from(body_value).unwrap();

            content.insert("content".to_string(), canonical_body);
        }

        sign_json(self.local_server.as_str(), key.as_ref(), &mut content)?;

        let mut builder = self
            .client
            .request(parts.method.clone(), Url::from_str(&uri_str).unwrap())
            .header(http::header::CONTENT_TYPE, "application/json")
            .body(body);

        let signatures = content
            .get("signatures")
            .and_then(|s| s.as_object())
            .and_then(|signatures| signatures.get(self.local_server.as_str()))
            .and_then(|server_name| server_name.as_object())
            .ok_or(ClientError::Unknown)?;

        for (k, v) in signatures {
            if let CanonicalJsonValue::String(sig) = v {
                builder = builder.header(
                    "Authorization",
                    format!(
                        "X-Matrix origin=\"{}\",key=\"{}\",sig=\"{}\"",
                        self.local_server.as_str(),
                        k,
                        sig
                    ),
                );
            }
        }

        let request = builder.build()?;
        let request = inject_context_to_request(request);
        let result = self.client.execute(request).await?;

        if result.status().is_server_error() || result.status().is_client_error() {
            let status = result.status();
            let bytes = result.bytes().await?.to_vec();

            let response = http::Response::builder()
                .status(status)
                .body(bytes)
                .map_err(|_| ClientError::Unknown)?;

            let error = safareig_core::ruma::api::client::Error::from_http_response(response);

            return Err(ClientError::RumaError(error));
        }

        let bytes = result.bytes().await?.to_vec();

        Ok(bytes)
    }

    async fn safe_and_limited_future<T, E: Into<FederationError>>(
        &self,
        f: impl Future<Output = Result<T, E>>,
    ) -> Result<T, FederationError> {
        self.rate_limit.check().map_err(|_| {
            RATE_LIMITED_CALLS.add(
                1,
                &[KeyValue::new("remote", self.remote_server.to_string())],
            );

            FederationError::RateLimited
        })?;

        self.circuit_breaker.call(f).await.map_err(|e| match e {
            failsafe::Error::Rejected => FederationError::CircuitBreaker,
            failsafe::Error::Inner(e) => e.into(),
        })
    }
}

pub fn inject_context_to_request(mut request: Request) -> Request {
    let context = tracing::Span::current().context();

    global::get_text_map_propagator(|injector| {
        injector.inject_context(
            &context,
            &mut RequestInjector {
                request: &mut request,
            },
        )
    });

    request
}

struct RequestInjector<'a> {
    request: &'a mut Request,
}

impl<'a> Injector for RequestInjector<'a> {
    fn set(&mut self, key: &str, value: String) {
        let header_name = HeaderName::from_str(key).unwrap();
        let header_value = HeaderValue::from_str(&value).unwrap();
        self.request.headers_mut().insert(header_name, header_value);
    }
}

struct MetricsInstrumentator {
    remote_server: OwnedServerName,
}

impl failsafe::Instrument for MetricsInstrumentator {
    fn on_call_rejected(&self) {
        REJECTED_CALLS.add(
            1,
            &[KeyValue::new("remote", self.remote_server.to_string())],
        );
        tracing::error!(
            remote = self.remote_server.as_str(),
            "could not perform request to remote server because circuit breaker is closed",
        );
    }

    fn on_open(&self) {
        tracing::debug!(
            remote = self.remote_server.as_str(),
            "Circuit breaker is open target remote",
        )
    }

    fn on_half_open(&self) {
        tracing::debug!(
            remote = self.remote_server.as_str(),
            "Circuit breaker is half-open for target remote",
        )
    }

    fn on_closed(&self) {
        tracing::warn!(
            remote = self.remote_server.as_str(),
            "Circuit breaker has closed for target remote",
        )
    }
}
