use lazy_static::lazy_static;
use opentelemetry::metrics::Counter;

lazy_static! {
    pub static ref KEYS_FROM_CACHE: Counter<u64> =
        opentelemetry::global::meter("safareig/federation")
            .u64_counter("keys_from_cache")
            .with_description("Amount of remote keys fetched from memory or storage")
            .init();
    pub static ref KEYS_FROM_REMOTE: Counter<u64> =
        opentelemetry::global::meter("safareig/federation")
            .u64_counter("keys_from_remote")
            .with_description("Amount of times a key needs to be fetched from a remote homeserver")
            .init();
    pub static ref REJECTED_CALLS: Counter<u64> =
        opentelemetry::global::meter("safareig/federation")
            .u64_counter("rejected_federation_calls")
            .with_description("Amount of rejected requests due to CircuitBreaker is closed")
            .init();
    pub static ref RATE_LIMITED_CALLS: Counter<u64> =
        opentelemetry::global::meter("safareig/federation")
            .u64_counter("rate_limited_federation_calls")
            .with_description("Amount of rejected requests due to rate-limit hit")
            .init();
}
