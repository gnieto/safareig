use std::sync::Arc;

use safareig_core::{auth::Identity, command::Command, ruma::CanonicalJsonValue};
use safareig_federation::keys::{
    api as get_server_keys, command::ServerKeysCommand, CanonicalJsonValueExt, LocalKeyProvider,
};
use safareig_testing::TestingApp;

#[tokio::test]
async fn fetch_keys_return_keys_and_are_signed_with_active_keys() {
    let app = TestingApp::default().await;
    let local = app.service::<Arc<LocalKeyProvider>>();
    let local_key = local.current_key().await.unwrap();

    let cmd = app.command::<ServerKeysCommand>();

    let request = get_server_keys::v2::Request::default();
    let response = cmd.execute(request, Identity::None).await.unwrap();

    let value = serde_json::to_value(response.server_key).unwrap();
    let json: CanonicalJsonValue = serde_json::from_value(value).unwrap();
    let map = json.into_object().unwrap();

    let signatures = map.get("signatures").cloned().unwrap();
    let homeserver_signatures = signatures
        .into_object()
        .unwrap()
        .get(app.server_name().as_str())
        .cloned()
        .unwrap()
        .into_object()
        .unwrap();

    assert_eq!(1, homeserver_signatures.len());
    let (key_name, _signature) = homeserver_signatures.iter().next().unwrap();
    assert_eq!(key_name, &format!("ed25519:{}", local_key.version()));
}
