use std::{
    collections::BTreeMap,
    ops::{Add, Sub},
    sync::Arc,
    time::{Duration, SystemTime, UNIX_EPOCH},
};

use safareig_core::{
    auth::Identity,
    command::Command,
    config::HomeserverConfig,
    ruma::api::federation::discovery::get_remote_server_keys_batch::v2::{
        self as remote_keys, QueryCriteria,
    },
    time::uint_to_system_time,
};
use safareig_federation::keys::{
    command::RemoteKeysCommand,
    storage::{Key, KeyType, ServerKeyStorage},
    KeyFetcher, LocalKeyProvider,
};
use safareig_testing::{
    federation::{FailingKeyFetcher, HardcodedKeyFetcher},
    TestingApp,
};

#[tokio::test]
async fn returns_cached_keys_if_all_keys_are_still_usable() {
    let test = TestingApp::default().await;
    let key_storage = test.service::<Arc<dyn ServerKeyStorage>>();

    let server_name = safareig_core::ruma::server_name!("localhost:8448");
    let expired_key_id = safareig_core::ruma::server_signing_key_id!("ed25519:expired");
    let expired_key = Key {
        content: vec![],
        key_type: KeyType::PublicKey,
        id: expired_key_id.to_owned(),
        expire: Some(SystemTime::now().sub(Duration::from_secs(600))),
        valid_until_ts: None,
    };
    key_storage
        .add_server_key(server_name, &expired_key)
        .await
        .unwrap();
    let almost_valid_until_ts = SystemTime::now().add(Duration::from_secs(300));
    let almost_expired_key_id =
        safareig_core::ruma::server_signing_key_id!("ed25519:almost_expired");
    let still_valid = Key {
        content: vec![],
        key_type: KeyType::PublicKey,
        id: almost_expired_key_id.to_owned(),
        expire: None,
        valid_until_ts: Some(almost_valid_until_ts),
    };
    key_storage
        .add_server_key(server_name, &still_valid)
        .await
        .unwrap();

    let mut servers = BTreeMap::new();
    let mut keys = BTreeMap::new();
    keys.insert(expired_key_id.to_owned(), QueryCriteria::default());
    keys.insert(almost_expired_key_id.to_owned(), QueryCriteria::default());
    servers.insert(server_name.to_owned(), keys);

    let request = remote_keys::Request {
        server_keys: servers,
    };

    let cmd = remote_key_command(&test, Arc::new(FailingKeyFetcher {}));

    let response = cmd
        .execute(request, Identity::None)
        .await
        .expect("command executes successfully");
    let remote_server_keys = &response.server_keys[0].deserialize().unwrap();
    assert_eq!(server_name, remote_server_keys.server_name);
    assert_eq!(1, remote_server_keys.verify_keys.len());
    assert_eq!(1, remote_server_keys.old_verify_keys.len());
    assert_eq!(
        almost_valid_until_ts
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_secs(),
        uint_to_system_time(&remote_server_keys.valid_until_ts)
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_secs(),
    );
}

#[tokio::test]
async fn successfully_fetches_keys_which_are_not_valid_anymore() {
    let test = TestingApp::default().await;
    let key_storage = test.service::<Arc<dyn ServerKeyStorage>>();

    let server_name = safareig_core::ruma::server_name!("localhost:8448");
    let expired_key_id = safareig_core::ruma::server_signing_key_id!("ed25519:expired");
    let expired_key = Key {
        content: vec![],
        key_type: KeyType::PublicKey,
        id: expired_key_id.to_owned(),
        expire: Some(SystemTime::now().sub(Duration::from_secs(600))),
        valid_until_ts: None,
    };
    key_storage
        .add_server_key(server_name, &expired_key)
        .await
        .unwrap();
    let almost_expired_key_id =
        safareig_core::ruma::server_signing_key_id!("ed25519:almost_expired");
    let almost_valid_until_ts = SystemTime::now().sub(Duration::from_secs(300));
    let still_valid = Key {
        content: vec![],
        key_type: KeyType::PublicKey,
        id: almost_expired_key_id.to_owned(),
        expire: None,
        valid_until_ts: Some(almost_valid_until_ts),
    };
    key_storage
        .add_server_key(server_name, &still_valid)
        .await
        .unwrap();

    let mut servers = BTreeMap::new();
    let mut keys = BTreeMap::new();
    keys.insert(expired_key_id.to_owned(), QueryCriteria::default());
    keys.insert(almost_expired_key_id.to_owned(), QueryCriteria::default());
    servers.insert(server_name.to_owned(), keys);

    let request = remote_keys::Request {
        server_keys: servers,
    };

    let cmd = remote_key_command(&test, Arc::new(HardcodedKeyFetcher {}));

    let response = cmd
        .execute(request, Identity::None)
        .await
        .expect("command executes successfully");
    let remote_server_keys = &response.server_keys[0].deserialize().unwrap();
    assert_eq!(server_name, remote_server_keys.server_name);
    assert_eq!(1, remote_server_keys.verify_keys.len());
    assert_eq!(1, remote_server_keys.old_verify_keys.len());
    let new_valid_until = uint_to_system_time(&remote_server_keys.valid_until_ts);
    assert!(new_valid_until > SystemTime::now());
}

#[tokio::test]
async fn does_not_return_an_outdated_key_when_remote_key_fetcher_fails() {
    let test = TestingApp::default().await;
    let key_storage = test.service::<Arc<dyn ServerKeyStorage>>();

    let server_name = safareig_core::ruma::server_name!("localhost:8448");
    let expired_key_id = safareig_core::ruma::server_signing_key_id!("ed25519:expired");
    let expired_key = Key {
        content: vec![],
        key_type: KeyType::PublicKey,
        id: expired_key_id.to_owned(),
        expire: Some(SystemTime::now().sub(Duration::from_secs(600))),
        valid_until_ts: None,
    };
    key_storage
        .add_server_key(server_name, &expired_key)
        .await
        .unwrap();
    let invalid_key_id = safareig_core::ruma::server_signing_key_id!("ed25519:invalid");
    let invalid_key = Key {
        content: vec![],
        key_type: KeyType::PublicKey,
        id: invalid_key_id.to_owned(),
        expire: None,
        valid_until_ts: Some(SystemTime::now().sub(Duration::from_secs(300))),
    };
    key_storage
        .add_server_key(server_name, &invalid_key)
        .await
        .unwrap();

    let mut servers = BTreeMap::new();
    let mut keys = BTreeMap::new();
    keys.insert(expired_key_id.to_owned(), QueryCriteria::default());
    keys.insert(invalid_key_id.to_owned(), QueryCriteria::default());
    servers.insert(server_name.to_owned(), keys);

    let request = remote_keys::Request {
        server_keys: servers,
    };

    let cmd = remote_key_command(&test, Arc::new(FailingKeyFetcher {}));

    let response = cmd
        .execute(request, Identity::None)
        .await
        .expect("command executes successfully");
    let remote_server_keys = &response.server_keys[0].deserialize().unwrap();
    assert_eq!(server_name, remote_server_keys.server_name);
    assert_eq!(0, remote_server_keys.verify_keys.len());
    assert_eq!(1, remote_server_keys.old_verify_keys.len());
    let new_valid_until = uint_to_system_time(&remote_server_keys.valid_until_ts);
    assert!(new_valid_until > SystemTime::now());
}

fn remote_key_command(test: &TestingApp, key_fetcher: Arc<dyn KeyFetcher>) -> RemoteKeysCommand {
    let local_key_provider = test.service::<Arc<LocalKeyProvider>>();
    let config = test.service::<Arc<HomeserverConfig>>();
    let key_storage = test.service::<Arc<dyn ServerKeyStorage>>();

    RemoteKeysCommand::new(local_key_provider, config, key_storage, key_fetcher)
}
