use std::{collections::HashMap, sync::Arc};

use ruma_common::ServerName;
use safareig_federation::client::resolution::ServerDiscovery;
use safareig_testing::TestingApp;

#[tokio::test]
#[tracing_test::traced_test]
#[ignore]
async fn it_resolves_server_name() {
    let app = TestingApp::default().await;
    let discovery = app.service::<Arc<dyn ServerDiscovery>>();

    // https://t2bot.io/docs/resolvematrix/
    let mut tests = HashMap::new();
    tests.insert("2.s.resolvematrix.dev:7652", "2.s.resolvematrix.dev:7652");
    tests.insert("3b.s.resolvematrix.dev", "wk.3b.s.resolvematrix.dev:7753");
    tests.insert(
        "3c.s.resolvematrix.dev",
        "srv.wk.3c.s.resolvematrix.dev:7754",
    );
    tests.insert("3d.s.resolvematrix.dev", "wk.3d.s.resolvematrix.dev:8448");
    tests.insert("4.s.resolvematrix.dev", "srv.4.s.resolvematrix.dev:7855");
    tests.insert("5.s.resolvematrix.dev", "5.s.resolvematrix.dev:8448");

    for (test, expected) in tests {
        let test = ServerName::parse(test).unwrap();
        let r = discovery.resolve(&test).await.unwrap();
        assert_eq!(r.host, expected);
    }
}
