use std::sync::Arc;

use safareig::client::room::loader::RoomLoader;
use safareig_core::{
    command::{Container, Module},
    config::{ContentReportingConfig, HomeserverConfig},
    ddi::{ServiceCollectionExt, ServiceProvider, ServiceResolverExt},
};
use safareig_server_notices::ServerNotices;

use crate::{command::ReportContentCommand, ContentReporter, NoopReporter, ServerNoticeReporter};

#[derive(Default)]
pub struct ReportingContentModule;

#[async_trait::async_trait]
impl<R: safareig_core::command::Router> Module<R> for ReportingContentModule {
    fn register(&self, container: &mut Container, router: &mut R) {
        container.service_collection.service_factory(
            |config: &Arc<HomeserverConfig>, sp: &ServiceProvider| {
                let content_reporting: Arc<dyn ContentReporter> = match &config.content_reporting {
                    ContentReportingConfig::ServerNotice => {
                        let reporter = ServerNoticeReporter::new(
                            sp.get::<Arc<ServerNotices>>().unwrap().clone(),
                            config.admin.admins.clone(),
                            sp.get::<RoomLoader>().unwrap().clone(),
                        );
                        Arc::new(reporter)
                    }
                    _ => Arc::new(NoopReporter),
                };

                Ok(content_reporting)
            },
        );

        container.inject_endpoint::<_, ReportContentCommand, _>(router);
    }
}
