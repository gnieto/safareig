use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity, command::Command, ruma::api::client::room::report_content, Inject,
};

use crate::{ContentReporter, Report, ReportScore};

#[derive(Inject)]
pub struct ReportContentCommand {
    reporter: Arc<dyn ContentReporter>,
}

#[async_trait]
impl Command for ReportContentCommand {
    type Input = report_content::v3::Request;
    type Output = report_content::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let mut reason = input.reason.unwrap_or_default();
        reason.truncate(1024);

        let report = Report {
            reporter: id.user().to_owned(),
            event_id: input.event_id,
            room_id: input.room_id,
            reason,
            score: ReportScore::try_from(input.score.unwrap_or_default())?,
        };

        self.reporter.report(report).await?;

        Ok(report_content::v3::Response {})
    }
}
