mod command;
mod module;

use std::{collections::HashSet, convert::TryFrom, sync::Arc};

pub use command::ReportContentCommand;
pub use module::ReportingContentModule;
use ruma_common::{exports::http::StatusCode, OwnedEventId, OwnedRoomId, OwnedUserId};
use safareig::client::room::{loader::RoomLoader, RoomError};
use safareig_core::{
    error::{error_chain, RumaExt},
    events::Content,
    ruma::{api::client::error::ErrorBody, events::room::message::RoomMessageEventContent, Int},
    storage::StorageError,
    Inject,
};
use safareig_server_notices::{ServerNoticeError, ServerNotices};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum ReportContentError {
    #[error("Server notice error")]
    ServerNotice(#[from] ServerNoticeError),
    #[error("Score is not in the expected range")]
    InvalidScore,
    #[error("Unable to report event: it does not exist or you aren't able to see it")]
    NonVisibleEventId,
    #[error("Room error")]
    RoomError(#[from] RoomError),
    #[error("Serialization error")]
    Serde(#[from] serde_json::Error),
    #[error("Storage error")]
    StorageError(#[from] StorageError),
}

impl From<ReportContentError> for safareig_core::ruma::api::client::Error {
    fn from(room_error: ReportContentError) -> Self {
        match room_error {
            ReportContentError::ServerNotice(e) => e.into(),
            ReportContentError::InvalidScore => safareig_core::ruma::api::client::Error {
                status_code: StatusCode::BAD_REQUEST,
                body: ErrorBody::Standard {
                    kind: safareig_core::ruma::api::client::error::ErrorKind::InvalidParam,
                    message: room_error.to_string(),
                },
            },
            ReportContentError::NonVisibleEventId => safareig_core::ruma::api::client::Error {
                status_code: StatusCode::NOT_FOUND,
                body: ErrorBody::Standard {
                    kind: safareig_core::ruma::api::client::error::ErrorKind::NotFound,
                    message: room_error.to_string(),
                },
            },
            _ => safareig_core::ruma::api::client::Error {
                status_code: StatusCode::INTERNAL_SERVER_ERROR,
                body: ErrorBody::Standard {
                    kind: safareig_core::ruma::api::client::error::ErrorKind::Unknown,
                    message: room_error.to_string(),
                },
            },
        }
    }
}

impl From<ReportContentError> for safareig_core::ruma::api::error::MatrixError {
    fn from(error: ReportContentError) -> Self {
        let client = safareig_core::ruma::api::client::Error::from(error);
        client.to_generic()
    }
}

#[async_trait::async_trait]
pub trait ContentReporter: Send + Sync {
    async fn report(&self, report: Report) -> Result<(), ReportContentError>;
}

pub struct NoopReporter;

#[async_trait::async_trait]
impl ContentReporter for NoopReporter {
    async fn report(&self, report: Report) -> Result<(), ReportContentError> {
        if report.score.is_offensive() {
            tracing::warn!(
                reporter = report.reporter.as_str(),
                event_id = report.event_id.as_str(),
                room_id = report.room_id.as_str(),
                reason = report.reason.as_str(),
                score = report.score.0.to_string().as_str(),
                "Content reported"
            );
        } else {
            tracing::warn!(
                reporter = report.reporter.as_str(),
                event_id = report.event_id.as_str(),
                room_id = report.room_id.as_str(),
                reason = report.reason.as_str(),
                score = report.score.0.to_string().as_str(),
                "Content reported"
            );
        }

        Ok(())
    }
}

#[derive(Inject)]
pub struct ServerNoticeReporter {
    server_notices: Arc<ServerNotices>,
    admins: HashSet<OwnedUserId>,
    room_loader: RoomLoader,
}

#[async_trait::async_trait]
impl ContentReporter for ServerNoticeReporter {
    async fn report(&self, report: Report) -> Result<(), ReportContentError> {
        let room = self
            .room_loader
            .get_for_user(&report.room_id, &report.reporter)
            .await
            .map_err(|e| match e {
                RoomError::RoomNotFound(_) => ReportContentError::NonVisibleEventId,
                RoomError::UserNotAllowed(_) => ReportContentError::NonVisibleEventId,
                _ => ReportContentError::RoomError(e),
            })?;

        let content_message = format!(
            "User {} reported the content of event {} (room: {}). Reason: {}",
            report.reporter, report.event_id, report.room_id, report.reason
        );

        let event = room
            .event(&report.event_id)
            .await?
            .ok_or(ReportContentError::NonVisibleEventId)?;

        let json_event = serde_json::to_string_pretty(&event)?;

        for admin in &self.admins {
            let message_content = RoomMessageEventContent::text_plain(content_message.clone());
            let maybe_content = Content::from_event_content(&message_content);
            if maybe_content.is_err() {
                continue;
            }

            if let Err(e) = self
                .server_notices
                .send_notice(admin, maybe_content.unwrap())
                .await
            {
                tracing::error!(
                    err = error_chain(&e),
                    "could not send server notice to admin user",
                );
            }

            let reported_event_content = RoomMessageEventContent::notice_plain(json_event.clone());
            let maybe_content = Content::from_event_content(&reported_event_content);
            if maybe_content.is_err() {
                continue;
            }

            if let Err(e) = self
                .server_notices
                .send_notice(admin, maybe_content.unwrap())
                .await
            {
                tracing::error!(
                    err = error_chain(&e),
                    "could not send server notice to admin user",
                );
            }
        }

        Ok(())
    }
}

pub struct Report {
    pub reporter: OwnedUserId,
    pub event_id: OwnedEventId,
    pub room_id: OwnedRoomId,
    // TODO: Make this optional
    pub reason: String,
    pub score: ReportScore,
}

pub struct ReportScore(i32);

impl ReportScore {
    pub fn is_offensive(&self) -> bool {
        self.0 > -100 && self.0 < -50
    }
}

impl TryFrom<Int> for ReportScore {
    type Error = ReportContentError;

    fn try_from(value: Int) -> Result<Self, Self::Error> {
        let value = i32::try_from(value).map_err(|_| ReportContentError::InvalidScore)?;

        if value < -100 {
            return Err(ReportContentError::InvalidScore);
        }

        if value > 0 {
            return Err(ReportContentError::InvalidScore);
        }

        Ok(ReportScore(value))
    }
}
