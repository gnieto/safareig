use http::StatusCode;
use ruma_common::RoomId;
use safareig::client::room::Room;
use safareig_core::{
    command::Command,
    config::ContentReportingConfig,
    ruma::{api::client::room::report_content, EventId},
};
use safareig_reporting_content::{ReportContentCommand, ReportingContentModule};
use safareig_server_notices::ServerNoticesModule;
use safareig_testing::TestingApp;

#[tokio::test]
async fn it_does_not_allow_to_report_an_event_on_a_room_if_a_user_does_not_belong_to_the_room() {
    let mut config = TestingApp::default_config();
    config.content_reporting = ContentReportingConfig::ServerNotice;

    let app = TestingApp::with_config_and_modules(
        config,
        vec![
            Box::new(ReportingContentModule),
            Box::new(ServerNoticesModule),
        ],
    )
    .await;
    let cmd = app.command::<ReportContentCommand>();
    let non_joined_user_id = app.register().await;
    let (_, room): (_, Room) = app.public_room().await;
    let event = room.create_event().await.unwrap();

    let input = report_content::v3::Request {
        room_id: room.id().to_owned(),
        event_id: event.event_id,
        score: None,
        reason: None,
    };

    let response = cmd.execute(input, non_joined_user_id).await;
    let error = response.err().unwrap();

    assert_eq!(error.status_code, StatusCode::NOT_FOUND);
}

#[tokio::test]
async fn it_does_not_allow_to_report_an_event_on_a_non_existring_room() {
    let mut config = TestingApp::default_config();
    config.content_reporting = ContentReportingConfig::ServerNotice;
    let server_name = config.server_name.clone();

    let app = TestingApp::with_config_and_modules(
        config,
        vec![
            Box::new(ReportingContentModule),
            Box::new(ServerNoticesModule),
        ],
    )
    .await;
    let cmd = app.command::<ReportContentCommand>();
    let non_joined_user_id = app.register().await;

    let input = report_content::v3::Request {
        room_id: RoomId::new(&server_name),
        event_id: EventId::new(&server_name),
        score: None,
        reason: None,
    };

    let response = cmd.execute(input, non_joined_user_id).await;
    let error = response.err().unwrap();

    assert_eq!(error.status_code, StatusCode::NOT_FOUND);
}
