use std::sync::Arc;

use async_trait::async_trait;
use safareig::client::room::{RoomError, SafareigMessageLikeEventContent};
use safareig_core::{
    auth::Identity, command::Command, events::Content, ruma::events::MessageLikeEventType,
    storage::StorageError, Inject,
};
use safareig_server_notices::ServerNotices;

use crate::api::server_notices::v1 as server_notices;

#[derive(Inject)]
pub struct AdminSendServerNoticeCommand {
    server_notices: Arc<ServerNotices>,
}

#[async_trait]
impl Command for AdminSendServerNoticeCommand {
    type Input = server_notices::Request;
    type Output = server_notices::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;

    async fn execute(
        &self,
        input: Self::Input,
        _id: Identity,
    ) -> Result<Self::Output, Self::Error> {
        let event_type = input
            .kind
            .map(|kind| MessageLikeEventType::from(kind.to_string()))
            .unwrap_or(MessageLikeEventType::RoomMessage);

        SafareigMessageLikeEventContent::from_parts(&event_type, input.content.json())
            .map_err(RoomError::Serde)?;
        let content = Content::new(
            input
                .content
                .deserialize_as::<serde_json::Value>()
                .map_err(RoomError::Serde)?,
            event_type.to_string(),
        );

        let event_id = self
            .server_notices
            .send_notice(&input.user_id, content)
            .await?
            .ok_or_else(|| StorageError::Custom("could not send notice".to_string()))?;

        Ok(server_notices::Response { event_id })
    }
}
