use std::sync::Arc;

use safareig_core::{auth::Identity, command::Command, ruma::UInt, Inject};
use safareig_users::storage::UserStorage;
use synapse_admin_api::users::list_users::v2::{self as list_users, UserMinorDetails};

#[derive(Inject)]
pub struct AdminListUsersCommand {
    users: Arc<dyn UserStorage>,
}

#[async_trait::async_trait]
impl Command for AdminListUsersCommand {
    type Input = list_users::Request;
    type Output = list_users::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;

    async fn execute(
        &self,
        input: Self::Input,
        _id: Identity,
    ) -> Result<Self::Output, Self::Error> {
        let all_users = self.users.all_users().await.unwrap();
        let limit = input
            .limit
            .map(|l| u16::try_from(l).unwrap())
            .unwrap_or(100);
        let from = u16::try_from(input.from).unwrap();
        let user_count = all_users.len();
        let mut next_token = all_users.len();

        let users = all_users
            .into_iter()
            .enumerate()
            .skip(from as usize)
            .filter(|(_, user)| match &input.name {
                Some(name) => {
                    user.user_id.to_string().contains(name.as_str())
                        || user
                            .display
                            .as_ref()
                            .map(|d| d.to_string())
                            .unwrap_or_default()
                            .contains(name.as_str())
                }
                None => true,
            })
            .take(limit as usize)
            .map(|(i, u)| {
                let details = UserMinorDetails {
                    name: u.user_id.to_string(),
                    user_type: None,
                    is_guest: false,
                    admin: false,
                    deactivated: !u.is_active(),
                    displayname: u.display.unwrap_or_else(|| u.user_id.to_string()),
                    avatar_url: u.avatar.map(|a| a.to_string()),
                };
                next_token = i;

                details
            })
            .collect();

        Ok(list_users::Response {
            users,
            next_token: Some(next_token.to_string()),
            total: UInt::new_wrapping(user_count as u64),
        })
    }
}
