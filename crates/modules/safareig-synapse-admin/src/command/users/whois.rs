use safareig_core::{auth::Identity, command::Command, Inject};
use safareig_server_admin::Whois;

use crate::api::users::whois::v1 as whois;

#[derive(Inject)]
pub struct AdminWhoisCommand {
    whois: Whois,
}

#[async_trait::async_trait]
impl Command for AdminWhoisCommand {
    type Input = whois::Request;
    type Output = whois::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;

    async fn execute(
        &self,
        input: Self::Input,
        _id: Identity,
    ) -> Result<Self::Output, Self::Error> {
        let whois_data = self.whois.whois(&input.user_id).await?;

        Ok(whois::Response {
            user_id: Some(input.user_id.to_owned()),
            devices: whois_data.devices,
        })
    }
}
