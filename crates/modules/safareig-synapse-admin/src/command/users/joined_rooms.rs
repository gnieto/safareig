use std::sync::Arc;

use ruma_common::OwnedRoomId;
use safareig::storage::RoomStorage;
use safareig_core::{
    auth::Identity,
    command::Command,
    ruma::{events::room::member::MembershipState, UInt},
    Inject,
};
use synapse_admin_api::users::list_joined_rooms::v1 as list_joined_rooms;

#[derive(Inject)]
pub struct AdminJoinedRoomsCommand {
    rooms: Arc<dyn RoomStorage>,
}

#[async_trait::async_trait]
impl Command for AdminJoinedRoomsCommand {
    type Input = list_joined_rooms::Request;
    type Output = list_joined_rooms::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;

    async fn execute(
        &self,
        input: Self::Input,
        _id: Identity,
    ) -> Result<Self::Output, Self::Error> {
        let joined_rooms: Vec<OwnedRoomId> = self
            .rooms
            .membership(&input.user_id)
            .await?
            .into_iter()
            .filter(|(_, membership)| membership.state == MembershipState::Join)
            .map(|tuple| tuple.0)
            .collect();

        Ok(list_joined_rooms::Response {
            total: UInt::new_saturating(joined_rooms.len() as u64),
            joined_rooms,
        })
    }
}
