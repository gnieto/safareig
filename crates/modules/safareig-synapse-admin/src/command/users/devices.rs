use std::sync::Arc;

use safareig_core::{auth::Identity, command::Command, ruma::UInt, Inject};
use safareig_devices::storage::DeviceStorage;

use crate::api::users::devices::v2 as list_devices;

#[derive(Inject)]
pub struct AdminListDevicesCommand {
    devices: Arc<dyn DeviceStorage>,
}

#[async_trait::async_trait]
impl Command for AdminListDevicesCommand {
    type Input = list_devices::Request;
    type Output = list_devices::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;

    async fn execute(
        &self,
        input: Self::Input,
        _id: Identity,
    ) -> Result<Self::Output, Self::Error> {
        let user_devices = self.devices.devices_by_user(&input.user_id).await.unwrap();

        Ok(list_devices::Response {
            total: UInt::new_saturating(user_devices.len() as u64),
            devices: user_devices.into_iter().map(|d| d.ruma_device).collect(),
        })
    }
}
