use std::sync::Arc;

use safareig_core::{auth::Identity, command::Command, error::ResourceNotFound, Inject};
use safareig_users::storage::UserStorage;
use synapse_admin_api::users::{get_details::v2 as get_details, UserDetails};

#[derive(Inject)]
pub struct AdminUserDetailsCommand {
    users: Arc<dyn UserStorage>,
}

#[async_trait::async_trait]
impl Command for AdminUserDetailsCommand {
    type Input = get_details::Request;
    type Output = get_details::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;

    async fn execute(
        &self,
        input: Self::Input,
        _id: Identity,
    ) -> Result<Self::Output, Self::Error> {
        let user = self
            .users
            .user_by_id(&input.user_id)
            .await?
            .ok_or(ResourceNotFound)?;

        let deactivated = !user.is_active();
        let details = UserDetails {
            name: user.user_id.to_string(),
            password_hash: user.password,
            is_guest: false,
            admin: false,
            consent_version: None,
            consent_server_notice_sent: None,
            appservice_id: None,
            creation_ts: None,
            user_type: None,
            deactivated,
            displayname: user.display.unwrap_or_else(|| user.user_id.to_string()),
            avatar_url: user.avatar.map(|a| a.to_string()),
            threepids: vec![],
            external_ids: vec![],
        };

        Ok(get_details::Response { details })
    }
}
