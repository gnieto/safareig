use safareig_core::{auth::Identity, command::Command, ruma::UInt, Inject};

use crate::api::users::media::v1 as list_media;

#[derive(Default, Inject)]
pub struct AdminListMediaCommand {}

#[async_trait::async_trait]
impl Command for AdminListMediaCommand {
    type Input = list_media::Request;
    type Output = list_media::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;

    async fn execute(
        &self,
        _input: Self::Input,
        _id: Identity,
    ) -> Result<Self::Output, Self::Error> {
        Ok(list_media::Response {
            total: UInt::new_saturating(0u64),
            media: vec![],
            next_token: None,
        })
    }
}
