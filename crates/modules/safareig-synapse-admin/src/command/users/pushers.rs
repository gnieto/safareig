use std::sync::Arc;

use safareig_core::{auth::Identity, command::Command, ruma::UInt, Inject};
use safareig_push_notifications::storage::PusherStorage;

use crate::api::users::pushers::v1 as list_pushers;

#[derive(Inject)]
pub struct AdminListPushersCommand {
    push_storage: Arc<dyn PusherStorage>,
}

#[async_trait::async_trait]
impl Command for AdminListPushersCommand {
    type Input = list_pushers::Request;
    type Output = list_pushers::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;

    async fn execute(
        &self,
        input: Self::Input,
        _id: Identity,
    ) -> Result<Self::Output, Self::Error> {
        let pushers = self.push_storage.all_pushers(&input.user_id).await.unwrap();

        Ok(list_pushers::Response {
            total: UInt::new_saturating(pushers.len() as u64),
            pushers,
        })
    }
}
