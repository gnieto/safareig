use ruma_common::RoomId;
use safareig::client::room::loader::RoomLoader;
use safareig_core::{
    ruma::{
        api::client::room::Visibility,
        events::room::{guest_access::GuestAccess, history_visibility::HistoryVisibility},
        UInt,
    },
    storage::StorageError,
    Inject, ServerInfo,
};
use synapse_admin_api::rooms::list_rooms::v1::RoomDetails;

pub mod forward_extremities;
pub mod members;
pub mod room_details;
pub mod room_list;
pub mod state;

#[derive(Clone, Inject)]
pub struct RoomDetailsLoader {
    room_loader: RoomLoader,
    server_info: ServerInfo,
}

impl RoomDetailsLoader {
    pub async fn room_details(&self, room_id: &RoomId) -> Result<RoomDetails, StorageError> {
        let room = self.room_loader.get(room_id).await.unwrap().unwrap();

        let state = room.state_at_leaves().await.unwrap();
        let summary = room.room_summary().await.unwrap();

        let local_members = state
            .members()
            .await
            .unwrap()
            .into_iter()
            .filter(|user| self.server_info.is_local_server(user.server_name()))
            .count();

        let creation_content = room.create_event().await.unwrap().content;

        let visibility = room
            .create_extra()
            .await
            .map(|event| event.content.visibility)
            .unwrap_or_default();

        let history_visibility = room
            .history_visibility()
            .await
            .map(|event| event.content.history_visibility)
            .unwrap_or(HistoryVisibility::Invited);

        let guest_access = room
            .guest_access()
            .await
            .map(|event| event.content.guest_access)
            .unwrap_or(GuestAccess::Forbidden);

        let encryption = room
            .encryption()
            .await
            .map(|event| event.content.algorithm)
            .map(|algorithm| algorithm.to_string())
            .ok();

        let num_state_events = state.state_events(None).await.unwrap().len();

        let room_details = RoomDetails {
            room_id: room.id().to_owned(),
            name: summary.name,
            canonical_alias: summary.canoncical_alias,
            joined_members: UInt::new_saturating(summary.num_joined_members as u64),
            joined_local_members: UInt::new_saturating(local_members as u64),
            version: room.version().to_string(),
            creator: creation_content.creator.to_owned(),
            encryption,
            federatable: creation_content.federate,
            public: matches!(visibility, Visibility::Public),
            join_rules: summary.join_rule,
            guest_access,
            history_visibility,
            state_events: UInt::new_saturating(num_state_events as u64),
        };

        Ok(room_details)
    }
}
