use safareig_core::{auth::Identity, command::Command, Inject};

use super::RoomDetailsLoader;
use crate::api::rooms::room_details::v1 as room_details;

#[derive(Inject)]
pub struct AdminRoomDetailsCommand {
    room_details: RoomDetailsLoader,
}

#[async_trait::async_trait]
impl Command for AdminRoomDetailsCommand {
    type Input = room_details::Request;
    type Output = room_details::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;

    async fn execute(
        &self,
        input: Self::Input,
        _id: Identity,
    ) -> Result<Self::Output, Self::Error> {
        let details = self
            .room_details
            .room_details(&input.room_id)
            .await
            .unwrap();

        Ok(room_details::Response { details })
    }
}
