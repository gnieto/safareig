use safareig::client::room::loader::RoomLoader;
use safareig_core::{
    auth::Identity, command::Command, error::ResourceNotFound, ruma::UInt, Inject,
};

use crate::api::rooms::members::v1 as members;

#[derive(Inject)]
pub struct AdminRoomMembersCommand {
    room_loader: RoomLoader,
}

#[async_trait::async_trait]
impl Command for AdminRoomMembersCommand {
    type Input = members::Request;
    type Output = members::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;

    async fn execute(
        &self,
        input: Self::Input,
        _id: Identity,
    ) -> Result<Self::Output, Self::Error> {
        let room = self
            .room_loader
            .get(&input.room_id)
            .await?
            .ok_or(ResourceNotFound)?;
        let state = room.state_at_leaves().await?;
        let members = state.members().await?;

        Ok(members::Response {
            total: UInt::new_wrapping(members.len() as u64),
            members,
        })
    }
}
