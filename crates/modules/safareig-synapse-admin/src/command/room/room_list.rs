use std::sync::Arc;

use safareig::storage::RoomStorage;
use safareig_core::{auth::Identity, command::Command, ruma::UInt, Inject};
use synapse_admin_api::rooms::list_rooms::v1::{self as list_rooms};
use tokio::runtime::Handle;

use super::RoomDetailsLoader;

#[derive(Inject)]
pub struct AdminListRoomsCommand {
    rooms: Arc<dyn RoomStorage>,
    room_details: RoomDetailsLoader,
}

#[async_trait::async_trait]
impl Command for AdminListRoomsCommand {
    type Input = list_rooms::Request;
    type Output = list_rooms::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;

    async fn execute(
        &self,
        input: Self::Input,
        _id: Identity,
    ) -> Result<Self::Output, Self::Error> {
        let rooms = self.rooms.find_all_rooms().await.unwrap();
        let from = input
            .from
            .map(|from| u16::try_from(from).unwrap())
            .unwrap_or_default();
        let limit = input
            .limit
            .map(|l| u16::try_from(l).unwrap())
            .unwrap_or(100);

        let room_count = rooms.len();
        let mut next_token = from as usize;

        let rooms = rooms
            .into_iter()
            .enumerate()
            .skip(from as usize)
            .map(|(i, room)| {
                let room_id = room.room_id;
                let details = tokio::task::block_in_place(|| {
                    Handle::current()
                        .block_on(async move { self.room_details.room_details(&room_id).await })
                })
                .unwrap();

                (i, details)
            })
            .filter(|(_, room)| match &input.search_term {
                Some(search_term) => {
                    let name = room.name.clone().unwrap_or_default();

                    name.contains(search_term)
                }
                None => true,
            })
            .take(limit as usize)
            .map(|(i, room_details)| {
                next_token = i;

                room_details
            })
            .collect();

        Ok(list_rooms::Response {
            rooms,
            offset: UInt::new_saturating(next_token as u64),
            total_rooms: UInt::new_saturating(room_count as u64),
            next_batch: Some(UInt::new_saturating(next_token as u64)),
            prev_batch: if from == 0 {
                None
            } else {
                Some(UInt::new_wrapping(from as u64))
            },
        })
    }
}
