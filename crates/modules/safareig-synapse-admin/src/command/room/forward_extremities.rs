use std::sync::Arc;

use ruma_common::{MilliSecondsSinceUnixEpoch, OwnedRoomAliasId};
use safareig::storage::RoomTopologyStorage;
use safareig_core::{auth::Identity, command::Command, ruma::UInt, Inject};
use safareig_rooms::alias::AliasResolver;

use crate::api::rooms::forward_extremities::v1::{self as forward_extremities, ForwardExtremity};

#[derive(Inject)]
pub struct AdminRoomForwardExtremitiesCommand {
    topology: Arc<dyn RoomTopologyStorage>,
    alias_resolver: Arc<dyn AliasResolver>,
}

#[async_trait::async_trait]
impl Command for AdminRoomForwardExtremitiesCommand {
    type Input = forward_extremities::Request;
    type Output = forward_extremities::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;

    async fn execute(
        &self,
        input: Self::Input,
        _id: Identity,
    ) -> Result<Self::Output, Self::Error> {
        let room_id = if input.room_id.is_room_id() {
            input.room_id.try_into().unwrap()
        } else {
            let alias: OwnedRoomAliasId = input.room_id.try_into().unwrap();
            self.alias_resolver.resolve_alias(&alias).await?.room_id
        };

        let fwd_extremities = self.topology.forward_extremities(&room_id).await.unwrap();

        let extremities: Vec<_> = fwd_extremities
            .into_iter()
            .map(|extremity| ForwardExtremity {
                event_id: extremity.event_id.event_id().to_owned(),
                state_group: extremity.state_at.to_string(),
                depth: UInt::new_saturating(extremity.depth),
                received_time: MilliSecondsSinceUnixEpoch::now(),
            })
            .collect();

        Ok(forward_extremities::Response {
            count: UInt::new_saturating(extremities.len() as u64),
            results: extremities,
        })
    }
}
