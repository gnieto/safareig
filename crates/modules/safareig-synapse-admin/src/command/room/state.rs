use safareig::client::room::loader::RoomLoader;
use safareig_core::{auth::Identity, command::Command, events::EventError, Inject};

use crate::api::rooms::state::v1::{self as state};

#[derive(Inject)]
pub struct AdminRoomStateCommand {
    room_loader: RoomLoader,
}

#[async_trait::async_trait]
impl Command for AdminRoomStateCommand {
    type Input = state::Request;
    type Output = state::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;

    async fn execute(
        &self,
        input: Self::Input,
        _id: Identity,
    ) -> Result<Self::Output, Self::Error> {
        let room = self.room_loader.get(&input.room_id).await.unwrap().unwrap();

        let state = room.state_at_leaves().await.unwrap();
        let state_events = state
            .state_events(None)
            .await
            .unwrap()
            .into_iter()
            .map(TryInto::try_into)
            .collect::<Result<Vec<_>, EventError>>()
            .unwrap();

        Ok(state::Response {
            state: state_events,
        })
    }
}
