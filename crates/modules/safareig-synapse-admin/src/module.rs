use safareig_core::command::{Container, Module, Router, ServiceCollectionExtension};

use crate::command::{
    room::{
        forward_extremities::AdminRoomForwardExtremitiesCommand, members::AdminRoomMembersCommand,
        room_details::AdminRoomDetailsCommand, room_list::AdminListRoomsCommand,
        state::AdminRoomStateCommand, RoomDetailsLoader,
    },
    server_notices::AdminSendServerNoticeCommand,
    users::{
        AdminJoinedRoomsCommand, AdminListDevicesCommand, AdminListMediaCommand,
        AdminListPushersCommand, AdminListUsersCommand, AdminUserDetailsCommand, AdminWhoisCommand,
    },
};

#[derive(Default)]
pub struct SynapseAdminModule {}

#[async_trait::async_trait]
impl<R: Router> Module<R> for SynapseAdminModule {
    fn register(&self, container: &mut Container, router: &mut R) {
        // User
        container.inject_endpoint::<_, AdminListUsersCommand, _>(router);
        container.inject_endpoint::<_, AdminListDevicesCommand, _>(router);
        container.inject_endpoint::<_, AdminListMediaCommand, _>(router);
        container.inject_endpoint::<_, AdminJoinedRoomsCommand, _>(router);
        container.inject_endpoint::<_, AdminListPushersCommand, _>(router);
        container.inject_endpoint::<_, AdminUserDetailsCommand, _>(router);
        container.inject_endpoint::<_, AdminWhoisCommand, _>(router);

        // Rooms
        container.inject_endpoint::<_, AdminRoomDetailsCommand, _>(router);
        container.inject_endpoint::<_, AdminListRoomsCommand, _>(router);
        container.inject_endpoint::<_, AdminRoomForwardExtremitiesCommand, _>(router);
        container.inject_endpoint::<_, AdminRoomStateCommand, _>(router);
        container.inject_endpoint::<_, AdminRoomMembersCommand, _>(router);
        container.service_collection.register::<RoomDetailsLoader>();

        // Server notices
        container.inject_endpoint::<_, AdminSendServerNoticeCommand, _>(router);
    }
}
