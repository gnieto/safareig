//! [GET /_synapse/admin/v2/users/<user_id>/devices](https://github.com/matrix-org/synapse/blob/develop/docs/admin_api/user_admin_api.md#list-all-devices)

use ruma_common::OwnedUserId;
use safareig_core::ruma::{
    api::{client::device::Device, metadata, request, response, Metadata},
    UInt,
};

const METADATA: Metadata = metadata! {
    method: GET,
    rate_limited: false,
    authentication: AccessToken,
    history: {
        unstable => "/_synapse/admin/v2/users/:user_id/devices",
    }
};

#[request(error = safareig_core::ruma::api::error::MatrixError)]
pub struct Request {
    /// user ID
    #[ruma_api(path)]
    pub user_id: OwnedUserId,
}

#[response(error = safareig_core::ruma::api::error::MatrixError)]
pub struct Response {
    /// User's device count
    pub total: UInt,

    /// User's device information
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub devices: Vec<Device>,
}

impl Request {
    /// Creates an `Request` with the given user ID.
    pub fn new(user_id: OwnedUserId) -> Self {
        Self { user_id }
    }
}

impl Response {
    /// Creates a new `Response` with all parameters defaulted.
    pub fn new(devices: Vec<Device>) -> Self {
        Self {
            total: UInt::new_wrapping(devices.len() as u64),
            devices,
        }
    }
}
