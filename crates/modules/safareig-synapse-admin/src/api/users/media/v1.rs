//! [GET /_synapse/admin/v1/users/<user_id>/media](https://github.com/matrix-org/synapse/blob/develop/docs/admin_api/user_admin_api.md#list-media-uploaded-by-a-user)

use ruma_common::{MilliSecondsSinceUnixEpoch, OwnedMxcUri, OwnedUserId};
use safareig_core::ruma::{
    api::{metadata, request, response, Direction, Metadata},
    UInt,
};
use serde::{Deserialize, Serialize};

const METADATA: Metadata = metadata! {
    method: GET,
    rate_limited: false,
    authentication: AccessToken,
    history: {
        unstable => "/_synapse/admin/v1/users/:user_id/media",
    }
};

#[request(error = safareig_core::ruma::api::error::MatrixError)]
pub struct Request {
    /// user ID
    #[ruma_api(path)]
    pub user_id: OwnedUserId,

    /// Maximum amount of users to return. Defaults to 100.
    #[serde(skip_serializing_if = "Option::is_none", default = "default_limit")]
    #[ruma_api(query)]
    pub limit: UInt,

    /// Offset in the returned list.
    ///
    /// Defaults to 0.
    #[serde(default, skip_serializing_if = "ruma::serde::is_default")]
    #[ruma_api(query)]
    pub from: UInt,

    /// Search direction.
    #[serde(default, skip_serializing_if = "ruma::serde::is_default")]
    #[ruma_api(query)]
    #[serde(rename = "dir")]
    pub direction: Direction,

    /// Search criteria.
    #[serde(default, skip_serializing_if = "ruma::serde::is_default")]
    #[ruma_api(query)]
    pub order_by: OrderCriteria,
}

#[response(error = safareig_core::ruma::api::error::MatrixError)]
pub struct Response {
    /// User's device count
    pub total: UInt,

    /// User's device information
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub media: Vec<MediaDetail>,

    /// Pagination token
    pub next_token: Option<String>,
}

impl Request {
    /// Creates an `Request` with the given user ID.
    pub fn new(user_id: OwnedUserId) -> Self {
        Self {
            user_id,
            limit: default_limit(),
            from: Default::default(),
            direction: Direction::Forward,
            order_by: OrderCriteria::CreationTimestamp,
        }
    }
}

impl Response {
    /// Creates a new `Response` with all parameters defaulted.
    pub fn new(media: Vec<MediaDetail>, total: UInt, next_token: Option<String>) -> Self {
        Self {
            total,
            media,
            next_token,
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct MediaDetail {
    #[serde(rename = "created_ts")]
    /// Timestamp when the content was uploaded in ms.
    pub created: MilliSecondsSinceUnixEpoch,

    #[serde(rename = "last_access_ts")]
    /// Timestamp when the content was last accessed in ms.
    pub last_access: MilliSecondsSinceUnixEpoch,

    /// The id used to refer to the media. Details about the format are documented under media repository.
    pub media_id: OwnedMxcUri,

    /// Length of the media in bytes.
    pub media_length: UInt,

    /// The MIME-type of the media.
    pub media_type: String,

    /// The user ID that initiated the quarantine request for this media.
    pub quarantined_by: Option<OwnedUserId>,

    /// Status if this media is safe from quarantining.
    pub safe_from_quarantine: bool,

    /// The name the media was uploaded with.
    pub upload_name: String,
}

fn default_limit() -> UInt {
    UInt::new_saturating(100)
}

#[derive(Serialize, Deserialize, Default, Debug, Clone)]
#[serde(rename_all = "snake_case")]
pub enum OrderCriteria {
    /// Media are ordered alphabetically by media_id.
    MediaId,

    /// Media are ordered alphabetically by name the media was uploaded with.
    UploadName,

    #[serde(rename = "created_ts")]
    #[default]
    /// Media are ordered by when the content was uploaded in ms. Smallest to largest. This is the default.
    CreationTimestamp,

    #[serde(rename = "last_access_ts")]
    /// Media are ordered by when the content was last accessed in ms. Smallest to largest.
    LastAccessTimestamp,

    /// Media are ordered by length of the media in bytes. Smallest to largest.
    MediaLength,

    /// Media are ordered alphabetically by MIME-type.
    MediaType,

    /// Media are ordered alphabetically by the user ID that initiated the quarantine request for this media.
    QuarantinedBy,

    /// Media are ordered by the status if this media is safe from quarantining.
    SafeFromQuarantine,
}
