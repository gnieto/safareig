//! [GET /_synapse/admin/v1/whois/<user_id>](https://github.com/matrix-org/synapse/blob/develop/docs/admin_api/user_admin_api.md#query-current-sessions-for-a-user)

use std::collections::BTreeMap;

use ruma_common::OwnedUserId;
use safareig_core::ruma::api::{
    client::server::get_user_info::v3::DeviceInfo, metadata, request, response, Metadata,
};

const METADATA: Metadata = metadata! {
    method: GET,
    rate_limited: false,
    authentication: AccessToken,
    history: {
        unstable => "/_synapse/admin/v1/whois/:user_id",
    }
};

#[request(error = safareig_core::ruma::api::error::MatrixError)]
pub struct Request {
    /// user ID
    #[ruma_api(path)]
    pub user_id: OwnedUserId,
}

#[response(error = safareig_core::ruma::api::error::MatrixError)]
pub struct Response {
    /// The Matrix user ID of the user.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub user_id: Option<OwnedUserId>,

    /// A map of the user's device identifiers to information about that device.
    #[serde(default, skip_serializing_if = "BTreeMap::is_empty")]
    pub devices: BTreeMap<String, DeviceInfo>,
}

impl Request {
    /// Creates an `Request` with the given user ID.
    pub fn new(user_id: OwnedUserId) -> Self {
        Self { user_id }
    }
}

impl Response {
    /// Creates an empty `Response`.
    pub fn new(devices: BTreeMap<String, DeviceInfo>) -> Self {
        Self {
            user_id: None,
            devices,
        }
    }
}
