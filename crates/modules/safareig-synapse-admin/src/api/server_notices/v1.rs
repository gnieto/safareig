//! [POST /_synapse/admin/v1/send_server_notice](https://github.com/matrix-org/synapse/blob/master/docs/admin_api/server_notices.md#server-notices)

use ruma_common::{
    api::{request, response, Metadata},
    metadata,
    serde::Raw,
    OwnedEventId, OwnedUserId,
};
use safareig_core::ruma::events::{AnyMessageLikeEventContent, TimelineEventType};

const METADATA: Metadata = metadata! {
    method: POST,
    rate_limited: false,
    authentication: AccessToken,
    history: {
        unstable => "/_synapse/admin/v1/send_server_notice",
    }
};

#[request(error = safareig_core::ruma::api::error::MatrixError)]
pub struct Request {
    /// Target user id
    pub user_id: OwnedUserId,

    /// Server notice content
    pub content: Raw<AnyMessageLikeEventContent>,

    /// Optionally sets the room event type
    pub kind: Option<TimelineEventType>,

    /// State key
    pub state_key: Option<String>,
}

#[response(error = safareig_core::ruma::api::error::MatrixError)]
pub struct Response {
    /// Id of the event generated in the server notice room
    pub event_id: OwnedEventId,
}

impl Request {
    pub fn new(user_id: OwnedUserId, content: Raw<AnyMessageLikeEventContent>) -> Self {
        Self {
            user_id,
            content,
            kind: None,
            state_key: None,
        }
    }
}

impl Response {
    /// Creates an empty `Response`
    pub fn new(event_id: OwnedEventId) -> Self {
        Self { event_id }
    }
}
