//! [GET /_synapse/admin/v1/rooms/<room_id>/state](https://github.com/matrix-org/synapse/blob/master/docs/admin_api/rooms.md#room-state-api)

use ruma_common::{
    api::{request, response, Metadata},
    metadata,
    serde::Raw,
    OwnedRoomId,
};
use safareig_core::ruma::events::AnyStateEvent;

const METADATA: Metadata = metadata! {
    method: GET,
    rate_limited: false,
    authentication: AccessToken,
    history: {
        unstable => "/_synapse/admin/v1/rooms/:room_id/state",
    }
};

#[request(error = safareig_core::ruma::api::error::MatrixError)]
pub struct Request {
    /// user ID
    #[ruma_api(path)]
    pub room_id: OwnedRoomId,
}

#[response(error = safareig_core::ruma::api::error::MatrixError)]
pub struct Response {
    /// Current state events
    pub state: Vec<Raw<AnyStateEvent>>,
}

impl Request {
    /// Creates an `Request` with the given user ID.
    pub fn new(room_id: OwnedRoomId) -> Self {
        Self { room_id }
    }
}

impl Response {
    /// Creates a `Response` from current state
    pub fn new(state: Vec<Raw<AnyStateEvent>>) -> Self {
        Self { state }
    }
}
