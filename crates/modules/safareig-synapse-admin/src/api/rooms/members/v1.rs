//! [GET /_synapse/admin/v1/rooms/<room_id>/members](https://github.com/matrix-org/synapse/blob/master/docs/admin_api/rooms.md#room-members-api)

use ruma_common::{
    api::{request, response, Metadata},
    metadata, OwnedRoomId, OwnedUserId,
};
use safareig_core::ruma::UInt;

const METADATA: Metadata = metadata! {
    method: GET,
    rate_limited: false,
    authentication: AccessToken,
    history: {
        unstable => "/_synapse/admin/v1/rooms/:room_id/members",
    }
};

#[request(error = safareig_core::ruma::api::error::MatrixError)]
pub struct Request {
    /// user ID
    #[ruma_api(path)]
    pub room_id: OwnedRoomId,
}

#[response(error = safareig_core::ruma::api::error::MatrixError)]
pub struct Response {
    /// Total room members
    pub total: UInt,

    /// Room members
    pub members: Vec<OwnedUserId>,
}

impl Request {
    /// Creates an `Request` with the given user ID.
    pub fn new(room_id: OwnedRoomId) -> Self {
        Self { room_id }
    }
}

impl Response {
    /// Creates an empty `Response`.
    pub fn new(members: Vec<OwnedUserId>) -> Self {
        Self {
            total: UInt::new_saturating(members.len() as u64),
            members,
        }
    }
}
