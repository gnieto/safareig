//! [GET /_synapse/admin/v1/rooms/<room_id_or_alias>/forward_extremities](https://github.com/matrix-org/synapse/blob/master/docs/admin_api/rooms.md#check-for-forward-extremities)

use ruma_common::{
    api::{request, response, Metadata},
    metadata, MilliSecondsSinceUnixEpoch, OwnedEventId, OwnedRoomOrAliasId,
};
use safareig_core::ruma::UInt;
use serde::{Deserialize, Serialize};

const METADATA: Metadata = metadata! {
    method: GET,
    rate_limited: false,
    authentication: AccessToken,
    history: {
        unstable => "/_synapse/admin/v1/rooms/:room_id/forward_extremities",
    }
};

#[request(error = safareig_core::ruma::api::error::MatrixError)]
pub struct Request {
    /// user ID
    #[ruma_api(path)]
    pub room_id: OwnedRoomOrAliasId,
}

#[response(error = safareig_core::ruma::api::error::MatrixError)]
pub struct Response {
    /// Amount of forward extremities
    pub count: UInt,

    /// Forward extremities
    pub results: Vec<ForwardExtremity>,
}

impl Request {
    /// Creates an `Request` with the given user ID.
    pub fn new(room_id_or_alias: OwnedRoomOrAliasId) -> Self {
        Self {
            room_id: room_id_or_alias,
        }
    }
}

impl Response {
    /// Creates a `Response` from a vector of forward extremities
    pub fn new(forward_extremities: Vec<ForwardExtremity>) -> Self {
        Self {
            count: UInt::new_saturating(forward_extremities.len() as u64),
            results: forward_extremities,
        }
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ForwardExtremity {
    /// Event Id of the forward extremity
    pub event_id: OwnedEventId,

    /// Extremity state group
    pub state_group: String,

    /// Depth
    pub depth: UInt,

    /// Exteremity's timestamp
    #[serde(rename = "received_ts")]
    pub received_time: MilliSecondsSinceUnixEpoch,
}
