//! [GET /_synapse/admin/v1/rooms/<room_id>](https://github.com/matrix-org/synapse/blob/develop/docs/admin_api/rooms.md)

use ruma_common::{
    api::{request, response, Metadata},
    metadata, OwnedRoomId,
};
use synapse_admin_api::rooms::list_rooms::v1::RoomDetails;

const METADATA: Metadata = metadata! {
    method: GET,
    rate_limited: false,
    authentication: AccessToken,
    history: {
        unstable => "/_synapse/admin/v1/rooms/:room_id",
    }
};

#[request(error = safareig_core::ruma::api::error::MatrixError)]
pub struct Request {
    /// user ID
    #[ruma_api(path)]
    pub room_id: OwnedRoomId,
}

#[response(error = safareig_core::ruma::api::error::MatrixError)]
pub struct Response {
    #[serde(flatten)]
    pub details: RoomDetails,
}

impl Request {
    /// Creates an `Request` with the given user ID.
    pub fn new(room_id: OwnedRoomId) -> Self {
        Self { room_id }
    }
}

impl Response {
    /// Creates an empty `Response`.
    pub fn new(details: RoomDetails) -> Self {
        Self { details }
    }
}
