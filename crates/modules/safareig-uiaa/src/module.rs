use std::{any::Any, sync::Arc};

use ruma_common::api::IncomingRequest;
use safareig_core::{
    command::{Container, InjectServices, Module, Router, ServiceCollectionExtension},
    config::HomeserverConfig,
    database::DatabaseConnection,
    ddi::{ServiceCollectionExt, ServiceProvider, ServiceResolverExt},
    ruma::api::client::uiaa::UiaaResponse,
    storage::StorageError,
};

use crate::{
    command::{FallbackCommand, FallbackTemplateCommand, UiaaInput},
    service::AuthStages,
    storage::SessionStorage,
    CommandWrapper, UiaaCommand, UiaaStage, UiaaWrapper,
};

#[derive(Default)]
pub struct UiaaModule {}

impl UiaaModule {
    fn create_session_storage(db: DatabaseConnection) -> Arc<dyn SessionStorage> {
        match db {
            #[cfg(feature = "backend-sqlx")]
            DatabaseConnection::Sqlx(pool) => {
                Arc::new(crate::storage::sqlx::SessionSqlxStorage::new(pool))
            }
            _ => unimplemented!("Unkown database connection"),
        }
    }
}

#[async_trait::async_trait]
impl<R: Router> Module<R> for UiaaModule {
    fn register(&self, container: &mut Container, router: &mut R) {
        container
            .service_collection
            .service_factory(|db: &DatabaseConnection| {
                Ok(Self::create_session_storage(db.clone()))
            });
        container.service_collection.service_factory(
            |cfg: &Arc<HomeserverConfig>, sp: &ServiceProvider| {
                let stages = sp
                    .get_all::<Arc<dyn UiaaStage>>()?
                    .into_iter()
                    .map(|(stage_name, stage)| (stage_name.to_string(), stage.clone()))
                    .collect();

                Ok(Arc::new(AuthStages::new(cfg.clone(), stages)))
            },
        );

        container.service_collection.register::<CommandWrapper>();

        container.inject_endpoint::<_, FallbackTemplateCommand, _>(router);
        container.inject_endpoint::<_, FallbackCommand, _>(router);
    }

    async fn run_migrations(&self, container: &ServiceProvider) -> Result<(), StorageError> {
        #[cfg(feature = "backend-sqlx")]
        {
            let db = container.get::<DatabaseConnection>()?;
            safareig_sqlx::run_migration(db, "sqlite", "safareig-uiaa").await?;
        }

        Ok(())
    }
}

pub trait ContainerUIAAExt {
    fn uiaa_endpoint<Req, C, R>(&mut self, stage: &'static str, router: &mut R)
    where
        Req: IncomingRequest<EndpointError = UiaaResponse> + Send + Sync + UiaaInput + 'static,
        R: Router,
        C: 'static
            + Any
            + Send
            + Sync
            + InjectServices
            + UiaaCommand<Input = Req, Output = Req::OutgoingResponse, Error = Req::EndpointError>;
}

impl ContainerUIAAExt for Container {
    fn uiaa_endpoint<Req, C, R>(&mut self, stage: &'static str, router: &mut R)
    where
        Req: IncomingRequest<EndpointError = UiaaResponse> + Send + Sync + UiaaInput + 'static,
        R: Router,
        C: 'static
            + Any
            + Send
            + Sync
            + InjectServices
            + UiaaCommand<Input = Req, Output = Req::OutgoingResponse, Error = Req::EndpointError>,
    {
        self.register_lazy_endpoint::<_, Req, _, UiaaWrapper<C>, R>(
            move |sp: &ServiceProvider| {
                let wrapper = sp.get::<CommandWrapper>()?;
                let command = C::inject(sp)?;

                Ok(wrapper.wrap(command, stage))
            },
            router,
        );
    }
}
