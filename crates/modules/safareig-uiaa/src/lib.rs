pub(crate) mod auth;
mod command;
pub mod error;
pub mod module;
mod service;
mod storage;

use std::sync::Arc;

pub use command::{UiaaCommand, UiaaWrapper};
use safareig_core::{config::HomeserverConfig, Inject};
pub use service::{AuthStages, StageData, UiaaStage};
pub use storage::SessionState;
use storage::SessionStorage;

#[derive(Clone, Inject)]
pub struct CommandWrapper {
    config: Arc<HomeserverConfig>,
    session: Arc<dyn SessionStorage>,
    stages: Arc<AuthStages>,
}

impl CommandWrapper {
    pub fn wrap<C: UiaaCommand + Send + Sync>(
        &self,
        command: C,
        flow_id: &'static str,
    ) -> UiaaWrapper<C> {
        UiaaWrapper {
            inner: command,
            flow_id,
            config: self.config.clone(),
            session_storage: self.session.clone(),
            stages: self.stages.clone(),
        }
    }
}
