use safareig_core::ruma::{
    api::client::{
        error::{ErrorBody, ErrorKind},
        uiaa::UiaaResponse,
    },
    exports::http,
};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum UiaaError {
    #[error("Not current stage")]
    NotCurrentStage,
    #[error("Forbidden")]
    Forbidden,
    #[error("Forbidden")]
    NonRetriableForbidden,
    #[error("Missing session")]
    MissingSession,
    #[error("{0}")]
    Custom(String),
}

impl From<UiaaError> for UiaaResponse {
    fn from(error: UiaaError) -> Self {
        UiaaResponse::MatrixError(error.into())
    }
}

impl From<UiaaError> for safareig_core::ruma::api::client::Error {
    fn from(error: UiaaError) -> Self {
        match error {
            UiaaError::Forbidden => safareig_core::ruma::api::client::Error {
                status_code: http::StatusCode::UNAUTHORIZED,
                body: ErrorBody::Standard {
                    kind: ErrorKind::forbidden(),
                    message: error.to_string(),
                },
            },
            UiaaError::NonRetriableForbidden => safareig_core::ruma::api::client::Error {
                status_code: http::StatusCode::FORBIDDEN,
                body: ErrorBody::Standard {
                    kind: ErrorKind::forbidden(),
                    message: error.to_string(),
                },
            },
            _ => safareig_core::ruma::api::client::Error {
                status_code: http::StatusCode::UNAUTHORIZED,
                body: ErrorBody::Standard {
                    kind: ErrorKind::Unknown,
                    message: error.to_string(),
                },
            },
        }
    }
}
