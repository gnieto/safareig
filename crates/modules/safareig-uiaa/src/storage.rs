use std::collections::BTreeMap;

use async_trait::async_trait;
use rusty_ulid::Ulid;
use safareig_core::storage::StorageError;
use serde::{Deserialize, Serialize};
use serde_json::Value;

use crate::error::UiaaError;

#[cfg(feature = "backend-sqlx")]
pub mod sqlx;

#[async_trait]
pub trait SessionStorage: Send + Sync {
    async fn get_session(&self, id: &str) -> Result<Option<SessionState>, StorageError>;
    async fn put_session(&self, session: &SessionState) -> Result<(), StorageError>;
}

#[derive(Serialize, Deserialize, Debug)]
pub struct SessionState {
    // TODO: Type alias
    /// session identifier
    pub(crate) session: String,

    /// list of completed stages
    pub(crate) completed: Vec<String>,

    /// state contains any temporary data which is required by any auth stage. It can be used to
    /// store some state between requests for this concrete session.
    pub(crate) state: BTreeMap<String, Value>,
}

impl SessionState {
    pub fn start() -> SessionState {
        SessionState {
            session: Ulid::generate().to_string(),
            completed: Vec::new(),
            state: BTreeMap::new(),
        }
    }

    pub fn id(&self) -> &str {
        self.session.as_str()
    }

    pub fn missing_stages(&self, stages: &[String]) -> Vec<String> {
        let equals = stages
            .iter()
            .zip(self.completed.iter())
            .take_while(|(left, right)| left == right)
            .count();

        if equals < stages.len() {
            stages[equals..].to_vec()
        } else {
            Vec::new()
        }
    }

    pub fn complete(&mut self, stage: &str, required_stages: &[String]) -> Result<(), UiaaError> {
        let missing_states = self.missing_stages(required_stages);

        if let Some(next_stage) = missing_states.first() {
            return if next_stage.as_str() == stage {
                self.completed.push(stage.to_string());
                Ok(())
            } else {
                Err(UiaaError::NotCurrentStage)
            };
        }

        Err(UiaaError::NotCurrentStage)
    }

    pub fn completed(&self) -> Vec<String> {
        self.completed.clone()
    }

    pub fn state_mut(&mut self) -> &mut BTreeMap<String, Value> {
        &mut self.state
    }

    pub fn state(&self) -> &BTreeMap<String, Value> {
        &self.state
    }
}
