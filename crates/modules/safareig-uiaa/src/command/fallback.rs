use std::{collections::BTreeMap, str::FromStr, sync::Arc};

use async_trait::async_trait;
use ruma_common::exports::http::{header::*, StatusCode};
use safareig_core::{
    auth::Identity,
    command::Command,
    config::HomeserverConfig,
    ruma::api::{
        client::{
            error::{ErrorBody, ErrorKind},
            uiaa::UiaaResponse,
        },
        metadata, request, response, Metadata,
    },
    Inject,
};

use crate::{
    error::UiaaError,
    service::{FallbackData, FallbackInput},
    storage::SessionStorage,
    AuthStages, StageData,
};

#[derive(Inject)]
pub struct FallbackCommand {
    session: Arc<dyn SessionStorage>,
    config: Arc<HomeserverConfig>,
    stages: Arc<AuthStages>,
}

#[async_trait]
impl Command for FallbackCommand {
    type Input = Request;
    type Output = Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let session_id = input.session.clone();
        let mut session = self
            .session
            .get_session(session_id.to_string().as_str())
            .await?
            .ok_or(UiaaError::MissingSession)?;

        let data: BTreeMap<String, String> = form_urlencoded::parse(&input.content)
            .into_iter()
            .map(|(k, v)| (k.to_string(), v.to_string()))
            .collect();

        let fallback_data = FallbackData {
            session: rusty_ulid::Ulid::from_str(&input.session).unwrap(),
            extra: data,
        };
        let fallback_input = FallbackInput::new(input.auth_type.to_string(), fallback_data);

        let stages = self.config.uiaa.register_stages.as_ref();
        let _ = self
            .stages
            .advance(
                &id,
                &mut session,
                StageData::Fallback(&fallback_input),
                stages,
            )
            .map_err(Self::adapt_uiaa_error)?;
        self.session.put_session(&session).await?;

        Ok(Response {
            redirect_url: None,
            content_type: "text/html; content=UTF-8".to_string(),
            body: Self::load_success_content().await.as_bytes().to_vec(),
        })
    }
}

impl FallbackCommand {
    async fn load_success_content() -> String {
        let template = "resource/template/success.html".to_string();

        tokio::fs::read_to_string(template).await.unwrap()
    }

    fn adapt_uiaa_error(error: UiaaResponse) -> safareig_core::ruma::api::client::Error {
        match error {
            UiaaResponse::MatrixError(e) => e,
            UiaaResponse::AuthResponse(_) => safareig_core::ruma::api::client::Error {
                body: ErrorBody::Standard {
                    kind: ErrorKind::forbidden(),
                    message: "uiaa repsponse".to_string(),
                },
                status_code: StatusCode::UNAUTHORIZED,
            },
        }
    }
}

const METADATA: Metadata = metadata! {
    method: POST,
    rate_limited: false,
    authentication: None,
    history: {
        1.0 => "/_matrix/client/r0/auth/:auth_type/fallback/web",
        1.1 => "/_matrix/client/v3/auth/:auth_type/fallback/web",
    }
};

#[request(error = safareig_core::ruma::api::client::Error)]
pub struct Request {
    /// The type name ("m.login.dummy", etc.) of the uiaa stage to get a fallback page for.
    #[ruma_api(path)]
    pub auth_type: String,

    /// The ID of the session given by the homeserver.
    #[ruma_api(query)]
    pub session: String,

    /// Raw content of the request. This will usually be a form-encoded request.
    #[ruma_api(raw_body)]
    pub content: Vec<u8>,
}

#[response(error = safareig_core::ruma::api::client::Error)]
#[derive(Default)]
pub struct Response {
    /// Optional URI to redirect to.
    #[ruma_api(header = LOCATION)]
    pub redirect_url: Option<String>,

    /// Optional URI to redirect to.
    #[ruma_api(header = CONTENT_TYPE)]
    pub content_type: String,

    /// HTML to return to client.
    #[ruma_api(raw_body)]
    pub body: Vec<u8>,
}
