use std::str::FromStr;

use async_trait::async_trait;
use ruma_common::exports::http::header::*;
use safareig_core::{auth::Identity, command::Command, ruma::api::*, Inject};
use tinytemplate::TinyTemplate;

#[derive(Inject)]
pub struct FallbackTemplateCommand {}

#[async_trait]
impl Command for FallbackTemplateCommand {
    type Input = Request;
    type Output = Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, _: Identity) -> Result<Self::Output, Self::Error> {
        let sanitized = input.auth_type.replace('.', "_");
        let template =
            std::path::PathBuf::from_str(&format!("resource/template/{sanitized}.html")).unwrap();
        let content = tokio::fs::read_to_string(template).await.unwrap();

        let mut tt = TinyTemplate::new();
        tt.add_template(&sanitized, &content).unwrap();

        // Convert incoming string to ulid and convert back to String. We do that to prevent
        // html injections.
        let ulid = rusty_ulid::Ulid::from_str(&input.session).unwrap();
        let context = serde_json::json!({
            "session": ulid.to_string(),
        });

        let rendered = tt.render(&sanitized, &context).unwrap();

        Ok(Response {
            redirect_url: None,
            body: rendered.as_bytes().to_vec(),
            content_type: "text/html; charset=UTF-8".to_string(),
        })
    }
}

const METADATA: Metadata = metadata! {
    method: GET,
    rate_limited: false,
    authentication: None,
    history: {
        1.0 => "/_matrix/client/r0/auth/:auth_type/fallback/web",
        1.1 => "/_matrix/client/v3/auth/:auth_type/fallback/web",
    }
};

#[request(error = safareig_core::ruma::api::client::Error)]
pub struct Request {
    /// The type name ("m.login.dummy", etc.) of the uiaa stage to get a fallback page for.
    #[ruma_api(path)]
    pub auth_type: String,

    /// The ID of the session given by the homeserver.
    #[ruma_api(query)]
    pub session: String,
}

#[response(error = safareig_core::ruma::api::client::Error)]
#[derive(Default)]
pub struct Response {
    /// Optional URI to redirect to.
    #[ruma_api(header = LOCATION)]
    pub redirect_url: Option<String>,

    /// Optional URI to redirect to.
    #[ruma_api(header = CONTENT_TYPE)]
    pub content_type: String,

    /// HTML to return to client.
    #[ruma_api(raw_body)]
    pub body: Vec<u8>,
}
