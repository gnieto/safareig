use std::collections::BTreeMap;

use async_trait::async_trait;
use safareig_core::storage::StorageError;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use sqlx::Row;

use super::{SessionState, SessionStorage};

pub struct SessionSqlxStorage {
    pool: sqlx::Pool<sqlx::Sqlite>,
}

#[async_trait]
impl SessionStorage for SessionSqlxStorage {
    async fn get_session(&self, id: &str) -> Result<Option<SessionState>, StorageError> {
        let session = sqlx::query(
            r#"
            SELECT s.data FROM sessions s
            WHERE session_id = ?
            "#,
        )
        .bind(id.to_string())
        .fetch_optional(&self.pool)
        .await?;

        match session {
            Some(row) => {
                let row_data = row.get::<String, _>("data");
                let data: SessionData = serde_json::from_str(&row_data)?;

                Ok(Some(SessionState {
                    session: id.to_string(),
                    completed: data.completed,
                    state: data.state,
                }))
            }
            None => Ok(None),
        }
    }

    async fn put_session(&self, session: &SessionState) -> Result<(), StorageError> {
        let session_id = session.id();
        let data = serde_json::to_string(&SessionData {
            completed: session.completed(),
            state: session.state().clone(),
        })?;

        let now = sqlx::types::chrono::Utc::now();

        sqlx::query(
            r#"
            INSERT INTO sessions (session_id, data, last_update)
            VALUES (?1, ?2, ?3)
            ON CONFLICT(session_id)
            DO UPDATE SET data = ?2, last_update = ?3
            "#,
        )
        .bind(session_id)
        .bind(data)
        .bind(now)
        .execute(&self.pool)
        .await?;

        Ok(())
    }
}

impl SessionSqlxStorage {
    pub fn new(pool: sqlx::Pool<sqlx::Sqlite>) -> Self {
        Self { pool }
    }
}

#[derive(Serialize, Deserialize)]
struct SessionData {
    /// list of completed stages
    completed: Vec<String>,
    /// state contains any temporary data which is required by any auth stage. It can be used to
    /// store some state between requests for this concrete session.
    state: BTreeMap<String, Value>,
}
