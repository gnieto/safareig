use std::{
    collections::{BTreeMap, HashMap},
    sync::Arc,
};

use rusty_ulid::Ulid;
use safareig_core::{
    auth::Identity,
    config::HomeserverConfig,
    ruma::{
        api::client::{
            error::{ErrorBody, ErrorKind, StandardErrorBody},
            uiaa::{AuthData, AuthFlow, AuthType, UiaaInfo, UiaaResponse},
        },
        exports::http::StatusCode,
    },
};
use serde::Deserialize;
use serde_json::Value;

use crate::{
    auth::{Dummy, RegistrationToken, SafareigForbidden},
    error::UiaaError,
    storage::SessionState,
};

#[derive(Debug)]
pub struct FallbackInput {
    #[allow(unused)]
    auth_type: String,
    params: FallbackData,
}

#[derive(Deserialize, Debug)]
pub struct FallbackData {
    #[allow(unused)]
    pub(crate) session: Ulid,
    #[serde(flatten)]
    #[allow(unused)]
    pub(crate) extra: BTreeMap<String, String>,
}

impl FallbackData {
    pub fn raw_data(&self) -> &BTreeMap<String, String> {
        &self.extra
    }
}

impl FallbackInput {
    pub fn new(auth_type: String, params: FallbackData) -> Self {
        FallbackInput { auth_type, params }
    }

    pub fn fallback_data(&self) -> &FallbackData {
        &self.params
    }
}

/// AuthorizationData contains any required data for this stage, which can come via the fallback
/// endpoint or from a UIAA request. This enum is used to transport the data and to be able to
/// distinguish the origin of the data.
pub enum StageData<'a> {
    Fallback(&'a FallbackInput),
    UiaaRequest(&'a AuthData),
}

impl<'a> StageData<'a> {
    pub fn auth_data(&self) -> Option<&'a AuthData> {
        match self {
            StageData::Fallback(_) => None,
            StageData::UiaaRequest(auth_data) => Some(auth_data),
        }
    }
}

// TODO: Maybe execute should have an associated type for auth error for extensibility
pub trait UiaaStage: Send + Sync {
    /// Returns the stage name
    fn stage_name(&self) -> &str;

    /// Params is used to build the UiaaInfo and can be used to forward specific information of the
    /// stage to the client
    fn params(&self, session: &mut SessionState) -> Option<BTreeMap<String, Value>>;

    /// Execute is called on the UIAA or fallback endpoints to advance to the next stage of the UIAA
    /// flows. If it returns successfully, it means that the current stage has finished.
    fn execute(
        &self,
        identity: &Identity,
        session: &mut SessionState,
        data: StageData,
    ) -> Result<(), UiaaError>;
}

pub struct AuthStages {
    stages: HashMap<String, Arc<dyn UiaaStage>>,
}

impl AuthStages {
    pub fn new(
        config: Arc<HomeserverConfig>,
        mut stages: HashMap<String, Arc<dyn UiaaStage>>,
    ) -> Self {
        let registration_token = RegistrationToken { config };
        stages.insert(
            registration_token.stage_name().to_string(),
            Arc::new(registration_token),
        );

        let safareig_forbidden = SafareigForbidden;
        stages.insert(
            safareig_forbidden.stage_name().to_string(),
            Arc::new(safareig_forbidden),
        );

        let dummy = Dummy;
        stages.insert(dummy.stage_name().to_string(), Arc::new(dummy));

        AuthStages { stages }
    }

    pub fn advance(
        &self,
        identity: &Identity,
        session: &mut SessionState,
        data: StageData<'_>,
        stages: &[String],
    ) -> Result<(), UiaaResponse> {
        let missing = session.missing_stages(stages);

        if missing.is_empty() {
            return Ok(());
        }

        let next_stage = missing[0].as_str();
        let mut auth_error = None;
        let mut error_kind = ErrorKind::Unauthorized;

        match self.stage(next_stage) {
            Some(auth) => match auth.execute(identity, session, data) {
                Err(e) => {
                    let client_error = safareig_core::ruma::api::client::Error::from(e);
                    if client_error.status_code != StatusCode::UNAUTHORIZED {
                        return Err(UiaaResponse::MatrixError(client_error));
                    }

                    if let ErrorBody::Standard { message, kind } = client_error.body {
                        auth_error = Some(message);
                        error_kind = kind;
                    }
                }
                Ok(_) => session.complete(next_stage, stages)?,
            },
            None => auth_error = Some("unknown stage".to_string()),
        }

        match auth_error {
            Some(auth_error) => Err(UiaaResponse::AuthResponse(UiaaInfo {
                flows: vec![AuthFlow {
                    stages: stages
                        .iter()
                        .map(|auth| AuthType::from(auth.as_ref()))
                        .collect(),
                }],
                completed: session
                    .completed()
                    .iter()
                    .map(|auth| AuthType::from(auth.as_ref()))
                    .collect(),
                params: Default::default(),
                session: Some(session.id().to_string()),
                auth_error: Some(StandardErrorBody {
                    kind: error_kind,
                    message: auth_error,
                }),
            })),
            None => Ok(()),
        }
    }

    fn stage(&self, name: &str) -> Option<Arc<dyn UiaaStage>> {
        let stage = self.stages.get(name).cloned();

        stage
    }
}

#[cfg(test)]
mod tests {
    use std::{
        collections::{BTreeMap, HashMap},
        sync::Arc,
    };

    use safareig_core::{
        auth::Identity,
        ruma::api::client::uiaa::{AuthData, UiaaResponse},
    };
    use serde_json::Value;

    use crate::{error::UiaaError, AuthStages, SessionState, StageData, UiaaStage};

    #[test]
    fn should_return_ok_on_empty_missing_stages() {
        let id = Identity::None;
        let auth_stages = AuthStages {
            stages: Default::default(),
        };
        let mut session = SessionState::start();

        let auth_data = serde_json::json!({
            "type": "test",
            "session": session.id().to_string(),
            "auth_parameters": BTreeMap::<String, serde_json::Value>::new(),
        });
        let incoming: AuthData = serde_json::from_value(auth_data).unwrap();
        let result = auth_stages.advance(&id, &mut session, StageData::UiaaRequest(&incoming), &[]);

        assert!(result.is_ok());
    }

    #[test]
    fn should_return_error_if_next_stage_is_not_registered() {
        let id = Identity::None;
        let auth_stages = AuthStages {
            stages: Default::default(),
        };
        let mut session = SessionState::start();
        let auth_data = serde_json::json!({
            "type": "test",
            "session": session.id().to_string(),
            "auth_parameters": BTreeMap::<String, serde_json::Value>::new(),
        });
        let incoming: AuthData = serde_json::from_value(auth_data).unwrap();

        let result = auth_stages.advance(
            &id,
            &mut session,
            StageData::UiaaRequest(&incoming),
            &["safareig.non-existing".to_string()],
        );

        assert!(result.is_err());
        let error = match result {
            Err(UiaaResponse::AuthResponse(e)) => e,
            _ => unreachable!(),
        };
        assert_eq!(error.auth_error.unwrap().message, "unknown stage");
    }

    #[test]
    fn should_return_error_on_failing_stage_execution() {
        let id = Identity::None;
        let mut stages: HashMap<String, Arc<dyn UiaaStage>> = HashMap::new();

        stages.insert(
            "safareig.failing".to_string(),
            Arc::new(FailingAuthStage {}) as Arc<dyn UiaaStage>,
        );
        let auth_stages = AuthStages { stages };
        let mut session = SessionState::start();
        let auth_data = serde_json::json!({
            "type": "test",
            "session": session.id().to_string(),
            "auth_parameters": BTreeMap::<String, serde_json::Value>::new(),
        });
        let incoming: AuthData = serde_json::from_value(auth_data).unwrap();

        let result = auth_stages.advance(
            &id,
            &mut session,
            StageData::UiaaRequest(&incoming),
            &["safareig.failing".to_string()],
        );

        assert!(result.is_err());
        let error = match result {
            Err(UiaaResponse::AuthResponse(e)) => e,
            _ => unreachable!(),
        };
        assert_eq!(error.auth_error.unwrap().message, "failing stage");
    }

    #[test]
    fn should_complete_a_stage_on_successful_stage_execution() {
        let id = Identity::None;
        let mut stages: HashMap<String, Arc<dyn UiaaStage>> = HashMap::new();
        stages.insert(
            "safareig.successful".to_string(),
            Arc::new(SuccessfulAuthStage {}) as Arc<dyn UiaaStage>,
        );
        let auth_stages = AuthStages { stages };
        let mut session = SessionState::start();
        let auth_data = serde_json::json!({
            "type": "test",
            "session": session.id().to_string(),
            "auth_parameters": BTreeMap::<String, serde_json::Value>::new(),
        });
        let incoming: AuthData = serde_json::from_value(auth_data).unwrap();

        let result = auth_stages.advance(
            &id,
            &mut session,
            StageData::UiaaRequest(&incoming),
            &["safareig.successful".to_string()],
        );

        assert!(result.is_ok());
    }

    #[test]
    fn should_success_on_multi_stage_uiaa() {
        let id = Identity::None;
        let mut stages: HashMap<String, Arc<dyn UiaaStage>> = HashMap::new();
        stages.insert(
            "safareig.successful".to_string(),
            Arc::new(SuccessfulAuthStage {}) as Arc<dyn UiaaStage>,
        );
        let auth_stages = AuthStages { stages };
        let mut session = SessionState::start();
        let auth_data = serde_json::json!({
            "type": "test",
            "session": session.id().to_string(),
            "auth_parameters": BTreeMap::<String, serde_json::Value>::new(),
        });
        let incoming: AuthData = serde_json::from_value(auth_data).unwrap();
        let flow = [
            "safareig.successful".to_string(),
            "safareig.successful".to_string(),
            "safareig.successful".to_string(),
        ];

        let result =
            auth_stages.advance(&id, &mut session, StageData::UiaaRequest(&incoming), &flow);
        assert!(result.is_ok());
        let result =
            auth_stages.advance(&id, &mut session, StageData::UiaaRequest(&incoming), &flow);
        assert!(result.is_ok());
        assert_eq!(session.completed().len(), 2);
        let result =
            auth_stages.advance(&id, &mut session, StageData::UiaaRequest(&incoming), &flow);
        assert!(result.is_ok());
        assert_eq!(session.missing_stages(&flow).len(), 0);
    }

    struct FailingAuthStage;

    impl UiaaStage for FailingAuthStage {
        fn stage_name(&self) -> &str {
            "safareig.uiaa_failing"
        }

        fn params(&self, _: &mut SessionState) -> Option<BTreeMap<String, Value>> {
            unimplemented!()
        }

        fn execute(
            &self,
            _identity: &Identity,
            _: &mut SessionState,
            _: StageData,
        ) -> Result<(), UiaaError> {
            Err(UiaaError::Custom("failing stage".to_string()))
        }
    }

    struct SuccessfulAuthStage;

    impl UiaaStage for SuccessfulAuthStage {
        fn stage_name(&self) -> &str {
            "safareig.successful"
        }

        fn params(&self, _: &mut SessionState) -> Option<BTreeMap<String, Value>> {
            unimplemented!()
        }

        fn execute(
            &self,
            _identity: &Identity,
            _: &mut SessionState,
            _: StageData,
        ) -> Result<(), UiaaError> {
            Ok(())
        }
    }
}
