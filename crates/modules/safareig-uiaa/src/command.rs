use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    command::Command,
    config::HomeserverConfig,
    ruma::api::client::{
        keys::upload_signing_keys,
        uiaa::{self, AuthData, AuthFlow, AuthType, UiaaInfo, UiaaResponse},
    },
};

mod fallback;
mod fallback_template;

pub use fallback::FallbackCommand;
pub use fallback_template::FallbackTemplateCommand;

use crate::{
    service::{AuthStages, StageData},
    storage::{SessionState, SessionStorage},
};

#[async_trait]
pub trait UiaaCommand {
    type Input;
    type Output;
    type Error;

    async fn execute(
        &self,
        input: Self::Input,
        id: Identity,
        session: Option<SessionState>,
    ) -> Result<Self::Output, Self::Error>;
    fn extract_data(&self, _input: &Self::Input, _session_state: &mut SessionState) {}
}

pub struct UiaaWrapper<C: UiaaCommand + Send + Sync> {
    pub(crate) inner: C,
    pub(crate) flow_id: &'static str,
    pub(crate) config: Arc<HomeserverConfig>,
    pub(crate) session_storage: Arc<dyn SessionStorage>,
    pub(crate) stages: Arc<AuthStages>,
}

#[async_trait]
impl<C> Command for UiaaWrapper<C>
where
    C: UiaaCommand + Send + Sync,
    C::Input: UiaaInput + Send + Sync,
    UiaaResponse: From<C::Error>,
{
    type Input = C::Input;
    type Output = C::Output;
    type Error = UiaaResponse;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        if let Identity::AppService(_, _) = &id {
            Ok(self.inner.execute(input, id, None).await?)
        } else {
            let stages = self.stages();
            let auth_data = input.auth();
            let mut session = self.obtain_session(auth_data).await?;
            self.inner.extract_data(&input, &mut session);

            let auth_data = match auth_data {
                Some(auth_data) => auth_data,
                None => {
                    let uiaa = UiaaInfo {
                        flows: vec![AuthFlow {
                            stages: stages
                                .iter()
                                .map(|auth| AuthType::from(auth.as_ref()))
                                .collect(),
                        }],
                        completed: Default::default(),
                        params: Default::default(),
                        session: Some(session.id().to_string()),
                        auth_error: None,
                    };

                    self.session_storage
                        .put_session(&session)
                        .await
                        .map_err(|e| UiaaResponse::MatrixError(e.into()))?;

                    return Err(UiaaResponse::AuthResponse(uiaa));
                }
            };

            self.stages.advance(
                &id,
                &mut session,
                StageData::UiaaRequest(auth_data),
                &stages,
            )?;
            self.session_storage
                .put_session(&session)
                .await
                .map_err(|e| UiaaResponse::MatrixError(e.into()))?;

            tracing::warn!("Session [execute]: {:?}", session);
            Ok(self.inner.execute(input, id, Some(session)).await?)
        }
    }
}

impl<C: UiaaCommand + Sync + Send> UiaaWrapper<C> {
    fn stages(&self) -> Vec<String> {
        match self.flow_id {
            "register" => self.config.uiaa.register_stages.to_owned(),
            "login" => self.config.uiaa.login_stages.to_owned(),
            "device" => self.config.uiaa.device_stages.to_owned(),
            "password" => self.config.uiaa.password_stages.to_owned(),
            _ => vec![],
        }
    }

    async fn obtain_session(
        &self,
        auth_data: Option<&AuthData>,
    ) -> Result<SessionState, uiaa::UiaaResponse> {
        let session_id = self.get_session(auth_data);

        let session = match session_id {
            Some(session_id) => self
                .session_storage
                .get_session(session_id)
                .await
                .unwrap()
                .unwrap(),
            None => {
                let session_state = SessionState::start();
                self.session_storage
                    .put_session(&session_state)
                    .await
                    .unwrap();

                session_state
            }
        };

        Ok(session)
    }

    fn get_session<'a>(&self, auth_data: Option<&'a AuthData>) -> Option<&'a str> {
        auth_data.as_ref()?.session()
    }
}

pub trait UiaaInput {
    fn auth(&self) -> Option<&AuthData>;
}

impl UiaaInput for safareig_core::ruma::api::client::account::register::v3::Request {
    fn auth(&self) -> Option<&AuthData> {
        self.auth.as_ref()
    }
}

impl UiaaInput for safareig_core::ruma::api::client::device::delete_device::v3::Request {
    fn auth(&self) -> Option<&AuthData> {
        self.auth.as_ref()
    }
}

impl UiaaInput for safareig_core::ruma::api::client::device::delete_devices::v3::Request {
    fn auth(&self) -> Option<&AuthData> {
        self.auth.as_ref()
    }
}

impl UiaaInput for safareig_core::ruma::api::client::account::change_password::v3::Request {
    fn auth(&self) -> Option<&AuthData> {
        self.auth.as_ref()
    }
}

impl UiaaInput for safareig_core::ruma::api::client::account::deactivate::v3::Request {
    fn auth(&self) -> Option<&AuthData> {
        self.auth.as_ref()
    }
}

impl UiaaInput for upload_signing_keys::v3::Request {
    fn auth(&self) -> Option<&AuthData> {
        self.auth.as_ref()
    }
}
