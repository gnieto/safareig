use std::collections::BTreeMap;

use safareig_core::auth::Identity;
use serde_json::Value;

use crate::{
    error::UiaaError,
    service::{StageData, UiaaStage},
    storage::SessionState,
};

/// Fail always UIAA stage. It can be used as a registration UIAA stage to prevent users
/// to register to the homeserver
pub struct SafareigForbidden;

impl UiaaStage for SafareigForbidden {
    fn stage_name(&self) -> &str {
        "safareig.forbidden"
    }

    fn params(&self, _: &mut SessionState) -> Option<BTreeMap<String, Value>> {
        None
    }

    fn execute(&self, _: &Identity, _: &mut SessionState, _: StageData) -> Result<(), UiaaError> {
        Err(UiaaError::Custom(
            "homserver do not allow any login/regsitration".to_string(),
        ))
    }
}
