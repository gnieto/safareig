use std::collections::BTreeMap;

use safareig_core::auth::Identity;
use serde_json::Value;

use crate::{
    error::UiaaError,
    service::{StageData, UiaaStage},
    storage::SessionState,
};

/// Dummy authentication for stage `m.login.dummy` which just advances the stage regardless of the
/// session's state.
pub struct Dummy;

impl UiaaStage for Dummy {
    fn stage_name(&self) -> &str {
        "m.login.dummy"
    }

    fn params(&self, _: &mut SessionState) -> Option<BTreeMap<String, Value>> {
        None
    }

    fn execute(&self, _: &Identity, _: &mut SessionState, _: StageData) -> Result<(), UiaaError> {
        tracing::info!("Executing dummy login");
        Ok(())
    }
}
