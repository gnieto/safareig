use std::{collections::BTreeMap, sync::Arc};

use safareig_core::{auth::Identity, config::HomeserverConfig, ruma::api::client::uiaa::AuthData};
use serde_json::Value;

use crate::{
    error::UiaaError,
    service::{FallbackInput, StageData, UiaaStage},
    storage::SessionState,
};

/// `RegistrationToken` implements the `m.login.registration_token` as defined in [MSC3231](https://github.com/govynnus/matrix-doc/blob/token-registration/proposals/3231-token-authenticated-registration.md).
/// Additionaly, it also implements a fallback flow for those client implementations which do not natively support this stage.
pub struct RegistrationToken {
    pub(crate) config: Arc<HomeserverConfig>,
}

impl UiaaStage for RegistrationToken {
    fn stage_name(&self) -> &str {
        "m.login.registration_token"
    }

    fn params(&self, _: &mut SessionState) -> Option<BTreeMap<String, Value>> {
        None
    }

    fn execute(
        &self,
        _: &Identity,
        session: &mut SessionState,
        data: StageData,
    ) -> Result<(), UiaaError> {
        match data {
            StageData::Fallback(fallback) => self.fallback(session, fallback),
            StageData::UiaaRequest(auth_data) => self.uiaa_request(session, auth_data),
        }
    }
}

impl RegistrationToken {
    fn uiaa_request(&self, session: &SessionState, auth_data: &AuthData) -> Result<(), UiaaError> {
        // Is finished will be true if the token was set via the fallback endpoint.
        let is_finished = session
            .state()
            .get(self.stage_name())
            .map(|value| match value {
                Value::Bool(b) => *b,
                _ => false,
            })
            .unwrap_or(false);

        // Check if the token matches with the one in auth_data
        let uiaa_token_matches = match auth_data {
            AuthData::RegistrationToken(token) => self.is_valid_token(&token.token),
            _ => false,
        };

        if is_finished || uiaa_token_matches {
            Ok(())
        } else {
            Err(UiaaError::Custom("stage not finished yet".to_string()))
        }
    }

    fn fallback(&self, session: &mut SessionState, data: &FallbackInput) -> Result<(), UiaaError> {
        let given_token = data
            .fallback_data()
            .raw_data()
            .get("token")
            .cloned()
            .unwrap_or_default();

        if self.is_valid_token(&given_token) {
            session
                .state_mut()
                .insert(self.stage_name().to_string(), Value::Bool(true));

            Ok(())
        } else {
            Err(UiaaError::Custom("invalid token".to_string()))
        }
    }

    fn is_valid_token(&self, token: &str) -> bool {
        let config_token = self.config.uiaa.shared_token.clone().unwrap_or_default();

        config_token == token
    }
}
