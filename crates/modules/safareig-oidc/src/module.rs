use std::sync::Arc;

use safareig_core::{
    command::{Container, Module},
    ddi::{ServiceCollectionExt, ServiceProvider, ServiceResolverExt},
};

use crate::command::AuthIssuerCommand;

#[derive(Default)]
pub struct OidcModule {}

impl<R: safareig_core::command::Router> Module<R> for OidcModule {
    fn register(&self, container: &mut Container, router: &mut R) {
        container.inject_endpoint::<_, AuthIssuerCommand, _>(router);
    }
}
