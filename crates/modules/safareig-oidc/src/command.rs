use std::{collections::BTreeMap, sync::Arc};

use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    command::Command,
    config::HomeserverConfig,
    ruma::api::client::{discovery::get_authentication_issuer, keys::claim_keys},
    Inject,
};
use serde_json::Value;

#[derive(Inject)]
pub struct AuthIssuerCommand {
    config: Arc<HomeserverConfig>,
}

#[async_trait]
impl Command for AuthIssuerCommand {
    type Input = get_authentication_issuer::msc2965::Request;
    type Output = get_authentication_issuer::msc2965::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, _: Identity) -> Result<Self::Output, Self::Error> {
        Ok(get_authentication_issuer::msc2965::Response {
            issuer: self.config.oidc.issuer.clone(),
        })
    }
}
