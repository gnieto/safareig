use std::collections::HashSet;

use safareig_client_config::AccountDataManager;
use safareig_core::{
    ruma::{events::ignored_user_list::IgnoredUserListEventContent, OwnedUserId, UserId},
    storage::StorageError,
    Inject,
};

mod module;

pub use module::IgnoredUsersModule;

#[async_trait::async_trait]
pub trait IgnoreUsersRepository: Send + Sync {
    async fn ignored_users(&self, user: &UserId) -> Result<HashSet<OwnedUserId>, StorageError>;
}

#[derive(Inject)]
pub struct ClientConfigIgnoreUsers {
    client_config: AccountDataManager,
}

#[async_trait::async_trait]
impl IgnoreUsersRepository for ClientConfigIgnoreUsers {
    async fn ignored_users(&self, user: &UserId) -> Result<HashSet<OwnedUserId>, StorageError> {
        let content = self
            .client_config
            .get_account_event_content::<IgnoredUserListEventContent>(user)
            .await
            .map_err(|error| {
                tracing::error!(?error, "could not get account data content");
                StorageError::Custom(error.to_string())
            })?
            .map(|content| content.ignored_users.keys().cloned().collect())
            .unwrap_or_else(HashSet::new);

        Ok(content)
    }
}
