use std::sync::Arc;

use safareig_core::{
    command::{Container, InjectServices, Module, Router},
    ddi::{ServiceCollectionExt, ServiceProvider},
};

use crate::{ClientConfigIgnoreUsers, IgnoreUsersRepository};

#[derive(Default)]
pub struct IgnoredUsersModule;

impl<R: Router> Module<R> for IgnoredUsersModule {
    fn register(&self, container: &mut Container, _router: &mut R) {
        container
            .service_collection
            .service_factory(|sp: &ServiceProvider| {
                let repository: Arc<dyn IgnoreUsersRepository> =
                    Arc::new(ClientConfigIgnoreUsers::inject(sp)?);
                Ok(repository)
            });
    }
}
