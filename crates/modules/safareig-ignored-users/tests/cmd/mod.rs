use std::{collections::HashSet, time::Duration};

use safareig_client_config::command::SetAccountDataCommand;
use safareig_core::{
    auth::Identity,
    command::Command,
    ruma::{
        api::client::{config::set_global_account_data, sync::sync_events},
        events::{
            ignored_user_list::IgnoredUserListEventContent, AnySyncMessageLikeEvent,
            AnySyncTimelineEvent,
        },
        presence::PresenceState,
        serde::Raw,
        OwnedUserId,
    },
};
use safareig_ignored_users::IgnoredUsersModule;
use safareig_sync::SyncCommand;
use safareig_testing::TestingApp;

#[tokio::test]
async fn it_ignores_invites_from_ignored_users() {
    let app = TestingApp::with_modules(vec![Box::<IgnoredUsersModule>::default()]).await;
    let (creator, room) = app.public_room().await;
    let (creator2, room2) = app.public_room().await;

    let invited = app.register().await;

    let _ = app
        .invite_room(room.id(), &creator, &invited)
        .await
        .unwrap();

    let _ = app
        .invite_room(room2.id(), &creator2, &invited)
        .await
        .unwrap();

    set_ignored_users(&app, &invited, vec![creator.user().to_owned()]).await;

    let cmd = app.command::<SyncCommand>();
    let input = sync_events::v3::Request {
        filter: None,
        full_state: false,
        set_presence: PresenceState::Unavailable,
        timeout: Some(Duration::from_secs(1)),
        since: None,
    };
    let result = cmd.execute(input, invited.to_owned()).await.unwrap();

    assert_eq!(1, result.rooms.invite.len());
    assert_eq!(
        vec![room2.id().to_owned()],
        result.rooms.invite.keys().cloned().collect::<Vec<_>>()
    );
}

#[tokio::test]
async fn it_does_not_ignore_users_if_ignore_module_is_not_registered() {
    let app = TestingApp::default().await;
    let (creator, room) = app.public_room().await;
    let (creator2, room2) = app.public_room().await;

    let invited = app.register().await;

    let _ = app
        .invite_room(room.id(), &creator, &invited)
        .await
        .unwrap();

    let _ = app
        .invite_room(room2.id(), &creator2, &invited)
        .await
        .unwrap();

    set_ignored_users(&app, &invited, vec![creator.user().to_owned()]).await;

    let cmd = app.command::<SyncCommand>();
    let input = sync_events::v3::Request {
        filter: None,
        full_state: false,
        set_presence: PresenceState::Unavailable,
        timeout: Some(Duration::from_secs(1)),
        since: None,
    };
    let result = cmd.execute(input, invited.to_owned()).await.unwrap();

    assert_eq!(2, result.rooms.invite.len());
    let mut rooms = HashSet::new();
    rooms.insert(room.id().to_owned());
    rooms.insert(room2.id().to_owned());
    assert_eq!(
        rooms,
        result.rooms.invite.keys().cloned().collect::<HashSet<_>>()
    );
}

#[tokio::test]
async fn it_ignores_messages_from_ignored_users() {
    let app = TestingApp::with_modules(vec![Box::<IgnoredUsersModule>::default()]).await;
    let (creator, room) = app.public_room().await;
    let user1 = app.register().await;

    let _ = app
        .join_room(room.id().to_owned(), user1.clone())
        .await
        .unwrap();

    set_ignored_users(&app, &creator, vec![user1.user().to_owned()]).await;

    app.talk(room.id(), &user1, "missatge 1").await.unwrap();
    app.talk(room.id(), &user1, "missatge 2").await.unwrap();
    app.talk(room.id(), &user1, "missatge 3").await.unwrap();
    app.talk(room.id(), &user1, "missatge 4").await.unwrap();
    app.talk(room.id(), &user1, "missatge 5").await.unwrap();

    let cmd = app.command::<SyncCommand>();
    let input = sync_events::v3::Request {
        filter: None,
        full_state: false,
        set_presence: PresenceState::Unavailable,
        timeout: Some(Duration::from_secs(1)),
        since: None,
    };
    let result = cmd.execute(input, creator.to_owned()).await.unwrap();

    let timeline = &result.rooms.join.get(room.id()).unwrap().timeline;

    for event in &timeline.events {
        let event = event.deserialize().unwrap();

        if let AnySyncTimelineEvent::MessageLike(AnySyncMessageLikeEvent::RoomMessage(msg)) = event
        {
            if msg
                .as_original()
                .unwrap()
                .content
                .body()
                .contains("missatge")
                && msg.sender() == user1.user()
            {
                panic!("Received an unexpected message from ignored user");
            }
        }
    }
}

async fn set_ignored_users(app: &TestingApp, identity: &Identity, ignored: Vec<OwnedUserId>) {
    let content = IgnoredUserListEventContent::users(ignored);
    let cmd = app.command::<SetAccountDataCommand>();
    let input = set_global_account_data::v3::Request {
        user_id: identity.user().to_owned(),
        event_type: safareig_core::ruma::events::GlobalAccountDataEventType::IgnoredUserList,
        data: Raw::new(&content).unwrap().cast(),
    };

    cmd.execute(input, identity.clone()).await.unwrap();
}
