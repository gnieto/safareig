use async_trait::async_trait;
use safareig_core::{
    ruma::{OwnedMxcUri, OwnedUserId, UserId},
    storage::StorageError,
};
use serde::{Deserialize, Serialize};

#[cfg(feature = "backend-sqlx")]
pub mod sqlx;

#[async_trait]
pub trait UserStorage: Send + Sync {
    async fn user_by_id(&self, user_id: &UserId) -> Result<Option<User>, StorageError>;
    async fn users(&self, users: &[OwnedUserId]) -> Result<Vec<User>, StorageError>;
    async fn all_users(&self) -> Result<Vec<User>, StorageError>;
    async fn update_user(&self, user: &User) -> Result<(), StorageError>;
    async fn create_user(&self, user: &User) -> Result<(), StorageError>;
}

#[derive(Serialize, Deserialize, Debug, Clone, Default)]
pub enum AccountStatus {
    #[default]
    Active,
    Deactivated,
}

#[derive(Serialize, Deserialize, Debug, Clone, Default)]
pub enum AccountType {
    #[default]
    User,
    Appservice,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct User {
    pub user_id: OwnedUserId,
    pub password: Option<String>,
    pub avatar: Option<OwnedMxcUri>,
    pub display: Option<String>,
    #[serde(default)]
    pub status: AccountStatus,
    #[serde(default)]
    pub kind: AccountType,
}

impl User {
    pub fn new(id: &UserId, password: String) -> User {
        User {
            user_id: id.to_owned(),
            password: Some(password),
            avatar: None,
            display: None,
            status: AccountStatus::Active,
            kind: AccountType::User,
        }
    }

    pub fn appservice(id: &UserId) -> User {
        User {
            user_id: id.to_owned(),
            password: None,
            avatar: None,
            display: None,
            status: AccountStatus::Deactivated,
            kind: AccountType::Appservice,
        }
    }

    pub fn is_active(&self) -> bool {
        matches!(self.status, AccountStatus::Active)
    }

    pub fn deactivate(&mut self) {
        self.status = AccountStatus::Deactivated;
    }
}
