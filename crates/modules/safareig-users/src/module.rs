use std::sync::Arc;

use safareig_core::{
    command::{Container, Module, ServiceCollectionExtension},
    database::DatabaseConnection,
    ddi::{ServiceCollectionExt, ServiceProvider, ServiceResolverExt},
    listener::{Dispatcher, Listener},
};

use crate::{
    command::UserDirectorySearchCommand,
    profile::{
        command::{
            client::{
                GetAvatarCommand, GetDisplaynameCommand, GetProfileCommand, SetAvatarCommand,
                SetDisplaynameCommand,
            },
            federation::ProfileQueryCommand,
        },
        ProfileLoader, ProfileUpdated,
    },
    storage::UserStorage,
};

#[derive(Default)]
pub struct UsersModule {}

impl UsersModule {
    fn create_storage(db: DatabaseConnection) -> Arc<dyn UserStorage> {
        match db {
            #[cfg(feature = "backend-sqlx")]
            DatabaseConnection::Sqlx(pool) => {
                Arc::new(crate::storage::sqlx::SqlxStorage::new(pool))
            }
            _ => unimplemented!(),
        }
    }
}

#[async_trait::async_trait]
impl<R: safareig_core::command::Router> Module<R> for UsersModule {
    fn register(&self, container: &mut Container, router: &mut R) {
        container
            .service_collection
            .service_factory(|db: &DatabaseConnection| Ok(Self::create_storage(db.clone())));
        container.service_collection.register::<ProfileLoader>();
        container
            .service_collection
            .service_factory(|sp: &ServiceProvider| {
                let listeners: Vec<_> = sp
                    .get_all::<Arc<dyn Listener<ProfileUpdated>>>()?
                    .into_iter()
                    .map(|t| t.1.clone())
                    .collect();

                Ok(Arc::new(Dispatcher::new(listeners)))
            });

        // Profile - Client
        container.inject_endpoint::<_, GetAvatarCommand, _>(router);
        container.inject_endpoint::<_, GetDisplaynameCommand, _>(router);
        container.inject_endpoint::<_, GetProfileCommand, _>(router);
        container.inject_endpoint::<_, SetAvatarCommand, _>(router);
        container.inject_endpoint::<_, SetDisplaynameCommand, _>(router);

        // Profile - Federation
        container.inject_endpoint::<_, ProfileQueryCommand, _>(router);

        // User directory
        container.inject_endpoint::<_, UserDirectorySearchCommand, _>(router);
    }
}
