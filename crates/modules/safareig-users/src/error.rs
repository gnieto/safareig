use safareig_core::ruma::{
    api::client::error::{ErrorBody, ErrorKind},
    exports::http::StatusCode,
};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum ProfileError {
    #[error("Given display name has a length of {0}, but the limit is {1}")]
    DisplaynameTooLarge(u32, u32),
}

impl From<ProfileError> for safareig_core::ruma::api::client::Error {
    fn from(e: ProfileError) -> Self {
        match e {
            ProfileError::DisplaynameTooLarge(_, _) => safareig_core::ruma::api::client::Error {
                status_code: StatusCode::BAD_REQUEST,
                body: ErrorBody::Standard {
                    kind: ErrorKind::TooLarge,
                    message: e.to_string(),
                },
            },
        }
    }
}
