use std::{collections::HashMap, sync::Arc};

use safareig_core::{
    error::error_chain,
    ruma::{
        api::federation::query::get_profile_information,
        events::room::member::{MembershipState, RoomMemberEventContent},
        MxcUri, OwnedMxcUri, OwnedUserId, UserId,
    },
    storage::StorageError,
    Inject, ServerInfo,
};
use safareig_federation::client::ClientBuilder;

use crate::storage::{User, UserStorage};

pub mod command;

pub struct ProfileUpdated {
    user: OwnedUserId,
    displayname: Option<String>,
    avatar: Option<OwnedMxcUri>,
}

impl ProfileUpdated {
    pub fn user(&self) -> &UserId {
        &self.user
    }

    pub fn displayname(&self) -> Option<&str> {
        self.displayname.as_ref().map(|display| display.as_ref())
    }

    pub fn avatar(&self) -> Option<&MxcUri> {
        self.avatar.as_ref().map(|uri| uri.as_ref())
    }
}

impl From<User> for ProfileUpdated {
    fn from(user: User) -> Self {
        ProfileUpdated {
            user: user.user_id,
            displayname: user.display,
            avatar: user.avatar,
        }
    }
}

pub struct Profile {
    pub display_name: Option<String>,
    pub avatar_url: Option<OwnedMxcUri>,
}

#[derive(Clone, Inject)]
pub struct ProfileLoader {
    user_storage: Arc<dyn UserStorage>,
    client_builder: Arc<ClientBuilder>,
    server_info: ServerInfo,
}

impl ProfileLoader {
    pub async fn load(&self, user: &UserId) -> Result<Option<Profile>, StorageError> {
        if !self.server_info.is_local_server(user.server_name()) {
            return Ok(self.load_remote_user(user).await);
        }

        Ok(self.user_storage.user_by_id(user).await?.map(|u| Profile {
            display_name: u.display,
            avatar_url: u.avatar,
        }))
    }

    pub async fn load_multiple(
        &self,
        users: &[OwnedUserId],
    ) -> Result<HashMap<OwnedUserId, Profile>, StorageError> {
        let (local, remote) = self.split_local_users(users);
        let local: Vec<OwnedUserId> = local.into_iter().map(|user| user.to_owned()).collect();

        let mut profiles: HashMap<OwnedUserId, Profile> = self
            .user_storage
            .users(local.as_ref())
            .await?
            .into_iter()
            .map(|u| {
                let profile = Profile {
                    display_name: u.display,
                    avatar_url: u.avatar,
                };

                (u.user_id, profile)
            })
            .collect();

        for remote_user in remote {
            if let Some(profile) = self.load_remote_user(remote_user.as_ref()).await {
                profiles.insert(remote_user.to_owned(), profile);
            }
        }

        Ok(profiles)
    }

    fn split_local_users<'a>(
        &self,
        users: &'a [OwnedUserId],
    ) -> (Vec<&'a OwnedUserId>, Vec<&'a OwnedUserId>) {
        users
            .iter()
            .partition(|u| self.server_info.is_local_server(u.server_name()))
    }

    async fn load_remote_user(&self, user: &UserId) -> Option<Profile> {
        let profile_query = get_profile_information::v1::Request {
            user_id: user.to_owned(),
            field: None,
        };
        let client = self
            .client_builder
            .client_for(user.server_name())
            .await
            .ok()?;

        match client.auth_request(profile_query).await {
            Err(e) => {
                tracing::error!(
                    err = error_chain(&e),
                    user = user.as_str(),
                    "could not obtain profile for target user"
                );

                None
            }
            Ok(response) => Some(Profile {
                display_name: response.displayname,
                avatar_url: response.avatar_url,
            }),
        }
    }
}

pub fn member_event_with_profile(
    state: MembershipState,
    profile: Option<&Profile>,
) -> RoomMemberEventContent {
    let displayname = profile.map(|p| p.display_name.clone()).unwrap_or_default();
    let avatar_url = profile.and_then(|p| p.avatar_url.clone());

    RoomMemberEventContent {
        avatar_url,
        displayname,
        is_direct: None,
        membership: state,
        third_party_invite: None,
        reason: None,
        join_authorized_via_users_server: None,
    }
}
