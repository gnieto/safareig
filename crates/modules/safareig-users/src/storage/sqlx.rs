use async_trait::async_trait;
use safareig_core::{
    ruma::{OwnedUserId, UserId},
    storage::StorageError,
};
use safareig_sqlx::decode_from_deserialize;
use sqlx::{sqlite::SqliteRow, Row, Sqlite};

use crate::storage::{User, UserStorage};

pub struct SqlxStorage {
    pool: sqlx::Pool<Sqlite>,
}

impl SqlxStorage {
    pub fn new(pool: sqlx::Pool<Sqlite>) -> Self {
        Self { pool }
    }
}

#[async_trait]
impl UserStorage for SqlxStorage {
    async fn user_by_id(&self, user_id: &UserId) -> Result<Option<User>, StorageError> {
        let user = sqlx::query("SELECT * FROM users WHERE id = ?")
            .bind(user_id.as_str())
            .fetch_optional(&self.pool)
            .await?
            .map(row_to_user);

        Ok(user)
    }

    async fn users(&self, users: &[OwnedUserId]) -> Result<Vec<User>, StorageError> {
        let params = format!("?{}", ", ?".repeat(users.len().saturating_sub(1)));
        let query_str = format!("SELECT * FROM users WHERE id IN ( {params} )");

        let mut query = sqlx::query(&query_str);
        for uid in users {
            query = query.bind(uid.as_str());
        }

        let users = query
            .fetch_all(&self.pool)
            .await?
            .into_iter()
            .map(row_to_user)
            .collect();

        Ok(users)
    }

    async fn all_users(&self) -> Result<Vec<User>, StorageError> {
        let users = sqlx::query("SELECT * FROM users")
            .fetch_all(&self.pool)
            .await?
            .into_iter()
            .map(row_to_user)
            .collect();

        Ok(users)
    }

    async fn update_user(&self, user: &User) -> Result<(), StorageError> {
        let user_id = user.user_id.as_str();
        let avatar = user.avatar.as_ref().map(|d| d.as_str());
        let status = serde_json::to_string(&user.status)?;
        let kind = serde_json::to_string(&user.kind)?;

        sqlx::query(
            r#"
            INSERT INTO users (id, password, display, avatar, status, user_type)
            VALUES (?1, ?2, ?3, ?4, ?5, ?6)
            ON CONFLICT DO UPDATE SET password = ?2, display = ?3, avatar = ?4, status = ?5, user_type = ?6
            "#,
        )
        .bind(user_id)
        .bind(user.password.as_ref())
        .bind(user.display.as_ref())
        .bind(avatar)
        .bind(&status)
        .bind(&kind)
        .execute(&self.pool)
        .await?;

        Ok(())
    }

    async fn create_user(&self, user: &User) -> Result<(), StorageError> {
        let user_id = user.user_id.as_str();
        let avatar = user.avatar.as_ref().map(|d| d.as_str());
        let status = serde_json::to_string(&user.status)?;
        let kind = serde_json::to_string(&user.kind)?;

        sqlx::query(
            r#"
            INSERT INTO users (id, password, display, avatar, status, user_type)
            VALUES( ?1, ?2, ?3, ?4, ?5, ?6)
            "#,
        )
        .bind(user_id)
        .bind(user.password.as_ref())
        .bind(user.display.as_ref())
        .bind(avatar)
        .bind(&status)
        .bind(&kind)
        .execute(&self.pool)
        .await?;

        Ok(())
    }
}

pub(crate) fn row_to_user(r: SqliteRow) -> User {
    let avatar = r
        .get::<Option<String>, _>("avatar")
        .map(|avatar| avatar.into());

    User {
        user_id: UserId::parse(r.get::<&str, _>("id")).unwrap(),
        password: r.get::<Option<String>, _>("password"),
        avatar,
        display: r.get::<Option<String>, _>("display"),
        status: r
            .get::<Option<&str>, _>("status")
            .map(|s| decode_from_deserialize(s).unwrap_or_default())
            .unwrap_or_default(),
        kind: r
            .get::<Option<&str>, _>("user_type")
            .map(|s| decode_from_deserialize(s).unwrap_or_default())
            .unwrap_or_default(),
    }
}
