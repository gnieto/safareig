use async_trait::async_trait;
use safareig_core::{
    auth::Identity, command::Command, error::ResourceNotFound,
    ruma::api::client::profile::get_display_name, Inject,
};

use crate::profile::ProfileLoader;

#[derive(Inject)]
pub struct GetDisplaynameCommand {
    profile_loader: ProfileLoader,
}

#[async_trait]
impl Command for GetDisplaynameCommand {
    type Input = get_display_name::v3::Request;
    type Output = get_display_name::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, _: Identity) -> Result<Self::Output, Self::Error> {
        let profile = self
            .profile_loader
            .load(&input.user_id)
            .await?
            .ok_or(ResourceNotFound)?;

        Ok(get_display_name::v3::Response::new(profile.display_name))
    }
}
