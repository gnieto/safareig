use async_trait::async_trait;
use safareig_core::{
    auth::Identity, command::Command, error::ResourceNotFound,
    ruma::api::client::profile::get_profile, Inject,
};

use crate::profile::ProfileLoader;

#[derive(Inject)]
pub struct GetProfileCommand {
    profile_loader: ProfileLoader,
}

#[async_trait]
impl Command for GetProfileCommand {
    type Input = get_profile::v3::Request;
    type Output = get_profile::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, _: Identity) -> Result<Self::Output, Self::Error> {
        let profile = self
            .profile_loader
            .load(&input.user_id)
            .await?
            .ok_or(ResourceNotFound)?;

        Ok(get_profile::v3::Response::new(
            profile.avatar_url,
            profile.display_name,
        ))
    }
}
