use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity, command::Command, error::ResourceNotFound, listener::Dispatcher,
    ruma::api::client::profile::set_display_name, Inject,
};

use crate::{error::ProfileError, profile::ProfileUpdated, storage::UserStorage};

pub const MAX_DISPLAYNAME_LENGTH: u32 = 255;

#[derive(Inject)]
pub struct SetDisplaynameCommand {
    users: Arc<dyn UserStorage>,
    dispatcher: Arc<Dispatcher<ProfileUpdated>>,
}

#[async_trait]
impl Command for SetDisplaynameCommand {
    type Input = set_display_name::v3::Request;
    type Output = set_display_name::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let _ = id.check_user(&input.user_id)?;

        let displayname_length = input
            .displayname
            .as_ref()
            .map(|name| name.len() as u32)
            .unwrap_or(0);
        if displayname_length > MAX_DISPLAYNAME_LENGTH {
            return Err(ProfileError::DisplaynameTooLarge(
                displayname_length,
                MAX_DISPLAYNAME_LENGTH,
            )
            .into());
        }

        let mut user = self
            .users
            .user_by_id(id.user())
            .await?
            .ok_or(ResourceNotFound)?;

        user.display = input.displayname.clone();
        self.users.update_user(&user).await?;
        let profile_updated = ProfileUpdated::from(user);
        self.dispatcher.dispatch_event(profile_updated).await;

        Ok(set_display_name::v3::Response::new())
    }
}
