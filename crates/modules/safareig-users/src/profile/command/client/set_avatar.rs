use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity, command::Command, error::ResourceNotFound, listener::Dispatcher,
    ruma::api::client::profile::set_avatar_url, Inject,
};

use crate::{profile::ProfileUpdated, storage::UserStorage};

#[derive(Inject)]
pub struct SetAvatarCommand {
    users: Arc<dyn UserStorage>,
    dispatcher: Arc<Dispatcher<ProfileUpdated>>,
}

#[async_trait]
impl Command for SetAvatarCommand {
    type Input = set_avatar_url::v3::Request;
    type Output = set_avatar_url::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let _ = id.check_user(&input.user_id)?;

        let mut user = self
            .users
            .user_by_id(id.user())
            .await?
            .ok_or(ResourceNotFound)?;

        user.avatar = input.avatar_url.clone();
        self.users.update_user(&user).await?;
        let profile_updated = ProfileUpdated::from(user);
        self.dispatcher.dispatch_event(profile_updated).await;

        Ok(set_avatar_url::v3::Response::new())
    }
}
