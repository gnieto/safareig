use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    command::Command,
    error::ResourceNotFound,
    ruma::api::federation::query::get_profile_information::{v1 as profile, v1::ProfileField},
    Inject,
};

use crate::profile::ProfileLoader;

#[derive(Inject)]
pub struct ProfileQueryCommand {
    profile_loader: ProfileLoader,
}

#[async_trait]
impl Command for ProfileQueryCommand {
    type Input = profile::Request;
    type Output = profile::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;

    async fn execute(&self, input: Self::Input, _: Identity) -> Result<Self::Output, Self::Error> {
        let user_id = &input.user_id;

        let profile = self
            .profile_loader
            .load(user_id)
            .await?
            .ok_or(ResourceNotFound)?;

        let mut response = profile::Response::default();

        match &input.field {
            None => {
                response.displayname = profile.display_name;
                response.avatar_url = profile.avatar_url;
            }
            Some(ProfileField::DisplayName) => {
                response.displayname = profile.display_name;
            }
            Some(ProfileField::AvatarUrl) => {
                response.avatar_url = profile.avatar_url;
            }
            Some(f) => {
                tracing::info!(
                    field = f.to_string().as_str(),
                    "Requested unknown profile field",
                );
            }
        }

        Ok(response)
    }
}
