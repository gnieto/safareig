use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity, command::Command, ruma::api::client::user_directory::search_users, Inject,
};

use crate::storage::UserStorage;

#[derive(Inject)]
pub struct UserDirectorySearchCommand {
    user_storage: Arc<dyn UserStorage>,
}

#[async_trait]
impl Command for UserDirectorySearchCommand {
    type Input = search_users::v3::Request;
    type Output = search_users::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, _: Identity) -> Result<Self::Output, Self::Error> {
        let term = input.search_term.to_lowercase();
        let limit = u64::from(input.limit) as usize;

        let matching_users: Vec<search_users::v3::User> = self
            .user_storage
            .all_users()
            .await?
            .into_iter()
            .filter(|u| {
                let lower_user_id = u.user_id.as_str().to_lowercase();
                let lower_display = u.display.as_ref().map(|display| display.to_lowercase());

                lower_user_id.contains(&term)
                    || lower_display
                        .map(|display_name| display_name.contains(&term))
                        .unwrap_or(false)
            })
            .map(|u| search_users::v3::User {
                user_id: u.user_id,
                display_name: u.display,
                avatar_url: u.avatar,
            })
            .take(limit)
            .collect();

        let is_limited = matching_users.len() == limit;

        Ok(search_users::v3::Response {
            results: matching_users,
            limited: is_limited,
        })
    }
}
