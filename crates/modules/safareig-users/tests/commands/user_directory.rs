use safareig_core::{
    command::Command,
    ruma::{api::client::user_directory::search_users, UInt},
};
use safareig_testing::TestingApp;
use safareig_users::command::UserDirectorySearchCommand;

#[tokio::test]
async fn it_searches_and_find_users_by_id() {
    let db = TestingApp::default().await;
    let id = TestingApp::new_identity();
    prepare_some_users(&db).await;

    let cmd = db.command::<UserDirectorySearchCommand>();
    let request = search_users::v3::Request {
        search_term: "TEST.cat".to_string(),
        limit: UInt::new_wrapping(2),
        language: None,
    };

    let response = cmd.execute(request, id).await.unwrap();

    assert!(response.limited);
    assert_eq!(response.results.len(), 2);
}

#[tokio::test]
async fn it_searches_and_find_users_by_display_name() {
    let db = TestingApp::default().await;
    let id = TestingApp::new_identity();
    prepare_some_users(&db).await;
    let cmd = db.command::<UserDirectorySearchCommand>();
    let request = search_users::v3::Request {
        search_term: "abc".to_string(),
        limit: UInt::new_wrapping(10),
        language: None,
    };

    let response = cmd.execute(request, id).await.unwrap();

    assert!(!response.limited);
    assert_eq!(response.results.len(), 3);
}

#[tokio::test]
async fn it_searches_and_does_not_find_any_matching_user() {
    let db = TestingApp::default().await;
    let id = TestingApp::new_identity();
    prepare_some_users(&db).await;
    let cmd = db.command::<UserDirectorySearchCommand>();

    let request = search_users::v3::Request {
        search_term: "non matching".to_string(),
        limit: UInt::new_wrapping(10),
        language: None,
    };

    let response = cmd.execute(request, id).await.unwrap();

    assert!(!response.limited);
    assert_eq!(response.results.len(), 0);
}

async fn prepare_some_users(db: &TestingApp) {
    db.register_with_profile(Some("abcdef".to_string()), None)
        .await;
    db.register_with_profile(Some("abcghi".to_string()), None)
        .await;
    db.register_with_profile(Some("jklabc".to_string()), None)
        .await;
}
