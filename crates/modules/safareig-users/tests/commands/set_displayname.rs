use std::sync::Arc;

use safareig_core::{
    command::Command,
    ruma::{
        api::client::profile::set_display_name,
        events::{room::member::RoomMemberEventContent, TimelineEventType},
    },
};
use safareig_testing::{error::RumaErrorExtension, TestingApp};
use safareig_users::{profile::command::client::SetDisplaynameCommand, storage::UserStorage};

#[tokio::test]
async fn it_updates_displayname_and_is_broadcasts_it_to_joined_rooms() {
    let test = TestingApp::default().await;
    let (public_creator, room) = test.public_room().await;
    let (_, private) = test.private_room().await;
    let cmd = test.command::<SetDisplaynameCommand>();

    let input = set_display_name::v3::Request {
        user_id: public_creator.user().to_owned(),
        displayname: Some("display name".to_string()),
    };

    let _ = cmd.execute(input, public_creator.clone()).await.unwrap();

    let users = test.service::<Arc<dyn UserStorage>>();
    let user = users
        .user_by_id(public_creator.user())
        .await
        .unwrap()
        .unwrap();

    assert_eq!("display name", user.display.unwrap().as_str());
    let event = test.event_at_leaf(room.id()).await;
    let content: RoomMemberEventContent = serde_json::from_value(event.content).unwrap();
    // Check broadcast to the public room (user belongs to it)
    assert_eq!(event.kind, TimelineEventType::RoomMember);
    assert_eq!("display name", content.displayname.unwrap(),);

    // Check not broadcasted to private room (user does not belong to)
    let event = test.event_at_leaf(private.id()).await;
    assert_ne!(event.kind, TimelineEventType::RoomMember);
}

#[tokio::test]
async fn it_unsets_displayname() {
    let test = TestingApp::default().await;
    let id = test.register().await;
    let cmd = test.command::<SetDisplaynameCommand>();

    let input = set_display_name::v3::Request {
        user_id: id.user().to_owned(),
        displayname: Some("display name".to_string()),
    };
    let _ = cmd.execute(input, id.clone()).await.unwrap();

    let input = set_display_name::v3::Request {
        user_id: id.user().to_owned(),
        displayname: None,
    };
    let _ = cmd.execute(input, id.clone()).await.unwrap();

    let users = test.service::<Arc<dyn UserStorage>>();
    let user = users.user_by_id(id.user()).await.unwrap().unwrap();

    assert!(user.display.is_none());
}

#[tokio::test]
async fn it_does_not_allow_setting_a_large_display_name() {
    let test = TestingApp::default().await;
    let id = test.register().await;
    let cmd = test.command::<SetDisplaynameCommand>();

    let input = set_display_name::v3::Request {
        user_id: id.user().to_owned(),
        displayname: Some("d".repeat(256)),
    };
    let response = cmd.execute(input, id.clone()).await;

    assert!(response.is_err());
    assert_eq!(
        response.err().unwrap().message(),
        "Given display name has a length of 256, but the limit is 255"
    );
}
