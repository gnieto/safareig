use std::sync::Arc;

use safareig_core::{
    command::Command,
    ruma::{
        api::client::profile::set_avatar_url,
        events::{room::member::RoomMemberEventContent, TimelineEventType},
        OwnedMxcUri,
    },
};
use safareig_testing::TestingApp;
use safareig_users::{profile::command::client::SetAvatarCommand, storage::UserStorage};

#[tokio::test]
async fn it_updates_avatar_and_is_broadcasts_it_to_joined_rooms() {
    let test = TestingApp::default().await;
    let (public_creator, room) = test.public_room().await;
    let cmd = test.command::<SetAvatarCommand>();
    let uri: OwnedMxcUri = "mxc://localhost/id".into();

    let input = set_avatar_url::v3::Request {
        user_id: public_creator.user().to_owned(),
        avatar_url: Some(uri.clone()),
    };

    let _ = cmd.execute(input, public_creator.clone()).await.unwrap();

    let user = test
        .service::<Arc<dyn UserStorage>>()
        .user_by_id(public_creator.user())
        .await
        .unwrap()
        .unwrap();

    assert_eq!(uri.to_string(), user.avatar.unwrap().as_str());
    let event = test.event_at_leaf(room.id()).await;
    let content: RoomMemberEventContent = serde_json::from_value(event.content.clone()).unwrap();
    // Check broadcast to the public room (user belongs to it)
    assert_eq!(event.kind, TimelineEventType::RoomMember);
    assert_eq!(uri, content.avatar_url.unwrap());
}

#[tokio::test]
async fn it_unsets_avatar() {
    let test = TestingApp::default().await;
    let id = test.register().await;
    let cmd = test.command::<SetAvatarCommand>();
    let uri = "mxc://localhost/id".into();

    let input = set_avatar_url::v3::Request {
        user_id: id.user().to_owned(),
        avatar_url: Some(uri),
    };
    let _ = cmd.execute(input, id.clone()).await.unwrap();

    let input = set_avatar_url::v3::Request {
        user_id: id.user().to_owned(),
        avatar_url: None,
    };
    let _ = cmd.execute(input, id.clone()).await.unwrap();

    let user = test
        .service::<Arc<dyn UserStorage>>()
        .user_by_id(id.user())
        .await
        .unwrap()
        .unwrap();

    assert!(user.display.is_none());
}
