use std::sync::Arc;

use safareig::client::room::{command::create_room::CreateRoomCommand, loader::RoomLoader, Room};
use safareig_core::{
    auth::Identity,
    command::Command,
    ruma::{
        api::client::room::{
            create_room::{self},
            upgrade_room, Visibility,
        },
        events::{
            room::{
                power_levels::RoomPowerLevelsEventContent, tombstone::RoomTombstoneEventContent,
            },
            TimelineEventType,
        },
        room_alias_id, RoomVersionId,
    },
};
use safareig_room_upgrades::{command::UpgradeRoomCommand, module::RoomUpgradesModule};
use safareig_rooms::alias::storage::AliasStorage;
use safareig_testing::TestingApp;

#[tokio::test]
pub async fn it_can_upgrade_a_room() {
    let app = TestingApp::with_modules(vec![Box::<RoomUpgradesModule>::default()]).await;
    let (creator, room) = create_room_with_version(&app, RoomVersionId::V6).await;

    let input = upgrade_room::v3::Request {
        room_id: room.id().to_owned(),
        new_version: RoomVersionId::V10,
    };
    let cmd = app.command::<UpgradeRoomCommand>();
    let result = cmd.execute(input, creator.clone()).await.unwrap();

    // Tombstone in the previous room has been set
    let tombstones = app
        .find_event(room.id(), TimelineEventType::RoomTombstone)
        .await;
    let tombstone = tombstones.last().unwrap();
    let tombstone = tombstone.content_as::<RoomTombstoneEventContent>().unwrap();
    assert_eq!(tombstone.replacement_room, result.replacement_room);

    // Check power level events in the previous room has been changed
    let power_level_event = app.event_at_leaf(room.id()).await;
    let power_levels = power_level_event
        .content_as::<RoomPowerLevelsEventContent>()
        .unwrap();
    assert_eq!(50, i64::from(power_levels.invite));
    assert_eq!(50, i64::from(power_levels.events_default));

    // A new user can join
    let new_user = app.register().await;
    let _ = app
        .join_room(result.replacement_room.clone(), new_user)
        .await
        .unwrap();

    // New room version is V10
    let new_room = app
        .service::<RoomLoader>()
        .get(&result.replacement_room)
        .await
        .unwrap()
        .unwrap();
    let create = new_room.create_event().await.unwrap();
    assert_eq!(RoomVersionId::V10, create.content.room_version);
    assert_eq!(room.id(), create.content.predecessor.unwrap().room_id);

    // Check alias points to the new version
    let aliases = app.service::<Arc<dyn AliasStorage>>();
    let room_alias_id = room_alias_id!("#room_alias:test.cat");
    let _current_alias = aliases.get_alias(room_alias_id).await.unwrap();
}

async fn create_room_with_version(
    app: &TestingApp,
    room_version: RoomVersionId,
) -> (Identity, Room) {
    let cmd = app.command::<CreateRoomCommand>();
    let id = app.register().await;
    let room_name = "Room name".parse().unwrap();

    let input = create_room::v3::Request {
        creation_content: None,
        power_level_content_override: None,
        initial_state: vec![],
        invite: vec![],
        invite_3pid: vec![],
        is_direct: false,
        name: Some(room_name),
        preset: None,
        room_alias_name: Some("room_alias".to_string()),
        room_version: Some(room_version),
        topic: Some("Description".to_string()),
        visibility: Visibility::Public,
    };
    let output = cmd.execute(input, id.clone()).await.unwrap();

    let room_loader = app.service::<RoomLoader>();
    let room = room_loader.get(&output.room_id).await.unwrap().unwrap();

    (id, room)
}
