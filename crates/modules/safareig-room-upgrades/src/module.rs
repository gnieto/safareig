use safareig_core::command::{Container, Module};

use crate::command::UpgradeRoomCommand;

#[derive(Default)]
pub struct RoomUpgradesModule;

#[async_trait::async_trait]
impl<R: safareig_core::command::Router> Module<R> for RoomUpgradesModule {
    fn register(&self, container: &mut Container, router: &mut R) {
        container.inject_endpoint::<_, UpgradeRoomCommand, _>(router);
    }
}
