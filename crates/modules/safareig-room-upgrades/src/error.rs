use safareig::client::room::RoomError;
use safareig_core::ruma::{
    api::client::error::{ErrorBody, ErrorKind},
    exports::http::StatusCode,
};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum RoomUpgradeError {
    #[error("Room error")]
    Room(RoomError),
    #[error("User can not send tombstone event")]
    UserNotAllowed,
}

impl From<RoomUpgradeError> for safareig_core::ruma::api::client::Error {
    fn from(e: RoomUpgradeError) -> Self {
        match e {
            RoomUpgradeError::Room(e) => e.into(),
            RoomUpgradeError::UserNotAllowed => safareig_core::ruma::api::client::Error {
                status_code: StatusCode::FORBIDDEN,
                body: ErrorBody::Standard {
                    kind: ErrorKind::InvalidParam,
                    message: e.to_string(),
                },
            },
        }
    }
}
