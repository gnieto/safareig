use std::sync::Arc;

use async_trait::async_trait;
use safareig::{
    client::room::{loader::RoomLoader, state::StateIndex, Room, RoomError},
    core::{
        events::EventBuilder,
        room::{CreateRoomParameters, RoomCreator, RoomVersion, RoomVersionRegistry},
    },
};
use safareig_core::{
    auth::Identity,
    command::Command,
    events::Content,
    ruma::{
        api::client::room::{create_room::v3::CreationContent, upgrade_room},
        events::{
            room::{create::PreviousRoom, tombstone::RoomTombstoneEventContent},
            TimelineEventType,
        },
        serde::Raw,
        Int, OwnedEventId, RoomId, UserId,
    },
    Inject,
};
use safareig_rooms::alias::{error::AliasError, storage::AliasStorage};

use crate::error::RoomUpgradeError;

#[derive(Inject)]
pub struct UpgradeRoomCommand {
    room_loader: RoomLoader,
    room_versions: Arc<RoomVersionRegistry>,
    room_creator: RoomCreator,
    room_aliases: Arc<dyn AliasStorage>,
}

#[async_trait]
impl Command for UpgradeRoomCommand {
    type Input = upgrade_room::v3::Request;
    type Output = upgrade_room::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let new_room_version = self
            .room_versions
            .get(&input.new_version)
            .ok_or_else(|| RoomError::UnsupportedRoomVersion(input.new_version.to_owned()))?
            .to_owned();

        let previous_room = self
            .room_loader
            .get_for_user(&input.room_id, id.user())
            .await?;
        if !previous_room
            .has_power_for_event(id.user(), &TimelineEventType::RoomTombstone)
            .await?
        {
            return Err(RoomUpgradeError::UserNotAllowed.into());
        }

        let new_room_id = RoomId::new(id.user().server_name());
        let tombstone_id = Self::send_tombstone(&previous_room, id.user(), &new_room_id).await?;
        self.transfer_state_events(
            &previous_room,
            id.user(),
            &new_room_id,
            new_room_version,
            tombstone_id,
        )
        .await?;
        self.transfer_aliases(&input.room_id, &new_room_id).await?;
        Self::adjust_power_levels(&previous_room, id.user()).await?;

        Ok(upgrade_room::v3::Response {
            replacement_room: new_room_id,
        })
    }
}

impl UpgradeRoomCommand {
    async fn send_tombstone(
        previous_room: &Room,
        user: &UserId,
        new_room_id: &RoomId,
    ) -> Result<OwnedEventId, RoomError> {
        let tombstone = RoomTombstoneEventContent {
            body: "".to_string(),
            replacement_room: new_room_id.to_owned(),
        };

        let content = Content::from_event_content(&tombstone)?;
        let tombstone_builder = EventBuilder::state(user, content, previous_room.id(), None);
        let (tombstone_id, _) = previous_room.add_state(tombstone_builder).await?;

        Ok(tombstone_id)
    }

    async fn transfer_state_events(
        &self,
        previous_room: &Room,
        user: &UserId,
        new_room_id: &RoomId,
        room_version: RoomVersion,
        tombstone_id: OwnedEventId,
    ) -> Result<(), RoomError> {
        let state = previous_room.state_at_leaves().await?;

        let previous_create = previous_room.create_event().await?.content;

        let creation_content = CreationContent {
            federate: previous_create.federate,
            predecessor: Some(PreviousRoom {
                event_id: tombstone_id,
                room_id: previous_room.id().to_owned(),
            }),
            room_type: previous_create.room_type,
        };
        let mut params =
            CreateRoomParameters::new(user.to_owned(), room_version, new_room_id.to_owned())
                .with_creation_content(Raw::new(&creation_content)?);

        let suggested_event_types = [
            TimelineEventType::RoomServerAcl,
            TimelineEventType::RoomEncryption,
            TimelineEventType::RoomName,
            TimelineEventType::RoomAvatar,
            TimelineEventType::RoomTopic,
            TimelineEventType::RoomGuestAccess,
            TimelineEventType::RoomHistoryVisibility,
            TimelineEventType::RoomJoinRules,
            TimelineEventType::RoomPowerLevels,
        ];

        let custom_event_types = [TimelineEventType::from("safareig.create_extra")];

        for event_type in suggested_event_types.into_iter().chain(custom_event_types) {
            let index = StateIndex::state_key(event_type, None);
            let event = previous_room.state_event(&index, &state).await?;

            if let Some(event) = event {
                match event.kind {
                    TimelineEventType::RoomPowerLevels => {
                        params = params.with_power_levels(event.content_as::<_>()?);
                    }
                    _ => {
                        let state_key = event.state_key.clone();
                        let content = Content::from(event);
                        let event_builder =
                            EventBuilder::state(user, content, new_room_id, state_key);
                        params = params.add_initial_event(event_builder);
                    }
                }
            }
        }

        self.room_creator.create_room(params).await?;

        Ok(())
    }

    async fn transfer_aliases(
        &self,
        old_room: &RoomId,
        new_room: &RoomId,
    ) -> Result<(), AliasError> {
        let current_aliases = self.room_aliases.room_aliases(old_room).await?;

        for mut alias in current_aliases {
            alias.room_id = new_room.to_owned();
            self.room_aliases.set_alias(alias, true).await?;
        }

        Ok(())
    }

    async fn adjust_power_levels(previous_room: &Room, user: &UserId) -> Result<(), RoomError> {
        let state = previous_room.state_at_leaves().await?;
        let mut power_level = previous_room.power_levels(&state).await?;
        let new_power = Int::max(
            Int::new(50).unwrap(),
            power_level
                .users_default
                .saturating_add(Int::new(1).unwrap()),
        );
        power_level.events_default = new_power;
        power_level.invite = new_power;

        let content = Content::from_event_content(&power_level)?;
        let event_builder = EventBuilder::state(user, content, previous_room.id(), None);
        previous_room.add_state(event_builder).await?;

        Ok(())
    }
}
