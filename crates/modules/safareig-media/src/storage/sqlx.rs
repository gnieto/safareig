use async_trait::async_trait;
use safareig_core::storage::StorageError;
use sqlx::{sqlite::SqliteRow, Pool, Row, Sqlite};

use super::{MediaContent, MediaStorage};

pub(crate) struct SqlxMediaStorage {
    #[allow(unused)]
    pub(crate) pool: Pool<Sqlite>,
}

#[async_trait]
impl MediaStorage for SqlxMediaStorage {
    async fn create_media(
        &self,
        media_id: &safareig_core::ruma::MxcUri,
        content: &super::MediaContent,
    ) -> safareig_core::storage::Result<()> {
        sqlx::query(
            r#"
            INSERT INTO media (media_id, filename, content_type)
            VALUES (?1, ?2, ?3)
            "#,
        )
        .bind(media_id.as_str())
        .bind(content.filename.as_deref())
        .bind(content.content_type.as_deref())
        .execute(&self.pool)
        .await?;

        Ok(())
    }

    async fn get_media(
        &self,
        media_id: &safareig_core::ruma::MxcUri,
    ) -> safareig_core::storage::Result<Option<super::MediaContent>> {
        let media_content = sqlx::query(
            r#"
            SELECT filename, content_type FROM media 
            WHERE media_id = ?
            "#,
        )
        .bind(media_id.as_str())
        .fetch_optional(&self.pool)
        .await?
        .map(TryFrom::try_from)
        .transpose()?;

        Ok(media_content)
    }
}

impl TryFrom<SqliteRow> for MediaContent {
    type Error = StorageError;

    fn try_from(row: SqliteRow) -> Result<Self, StorageError> {
        let filename = row.try_get::<Option<String>, _>("filename")?;
        let content_type = row.try_get::<Option<String>, _>("content_type")?;

        Ok(MediaContent {
            filename,
            content_type,
        })
    }
}
