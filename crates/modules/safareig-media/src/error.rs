use image::ImageError;
use safareig_core::ruma::{
    api::client::error::{ErrorBody, ErrorKind},
    exports::http::StatusCode,
};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum MediaError {
    #[error("Federation not supported")]
    FederationError,
    #[error("Federation requests not allowed on this server")]
    FederationNotEnabled,
    #[error("Invalid Mxc URI")]
    InvalidMxcUri,
    #[error("IO error")]
    IoError(#[from] std::io::Error),
    #[error("Cannot upload files larger than {0} bytes")]
    MaxBytesExceeded(usize),
    #[error("Resource not found")]
    ResourceNotFound,
    #[error("Image processing error: {0}")]
    Image(#[from] ImageError),
    #[error("Unknown transformation")]
    UnknownTransformation,
    #[error("Invalid mime type")]
    InvalidMimeType,
}

impl From<MediaError> for safareig_core::ruma::api::client::Error {
    fn from(error: MediaError) -> Self {
        match error {
            MediaError::MaxBytesExceeded(_) => safareig_core::ruma::api::client::Error {
                status_code: StatusCode::PAYLOAD_TOO_LARGE,
                body: ErrorBody::Standard {
                    kind: ErrorKind::TooLarge,
                    message: error.to_string(),
                },
            },
            MediaError::ResourceNotFound => safareig_core::ruma::api::client::Error {
                status_code: StatusCode::NOT_FOUND,
                body: ErrorBody::Standard {
                    kind: ErrorKind::NotFound,
                    message: error.to_string(),
                },
            },
            MediaError::FederationNotEnabled => safareig_core::ruma::api::client::Error {
                status_code: StatusCode::FORBIDDEN,
                body: ErrorBody::Standard {
                    kind: ErrorKind::forbidden(),
                    message: error.to_string(),
                },
            },
            e => safareig_core::ruma::api::client::Error {
                status_code: StatusCode::INTERNAL_SERVER_ERROR,
                body: ErrorBody::Standard {
                    kind: ErrorKind::Unknown,
                    message: e.to_string(),
                },
            },
        }
    }
}
