use std::{sync::Arc, task::Poll};

use async_trait::async_trait;
use pin_project::pin_project;
use rand::{distributions::Alphanumeric, thread_rng, Rng};
use safareig_core::{
    auth::Identity,
    command::Command,
    config::HomeserverConfig,
    ruma::{api::client::media::create_content, OwnedMxcUri},
    Inject,
};
use tokio::io::AsyncRead;

use crate::{
    driver::Driver,
    error::MediaError,
    storage::{MediaContent, MediaStorage},
};

pub struct CreateMediaCommandRequest {
    pub filename: Option<String>,
    pub content_type: Option<String>,
    pub stream: Box<dyn AsyncRead + Send + Unpin>,
}

#[derive(Inject)]
#[inject]
pub struct CreateMediaCommand {
    config: Arc<HomeserverConfig>,
    media_storage: Arc<dyn MediaStorage>,
    pub(crate) media_driver: Arc<dyn Driver>,
}

#[async_trait]
impl Command for CreateMediaCommand {
    type Input = CreateMediaCommandRequest;
    type Output = create_content::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, _: Identity) -> Result<Self::Output, Self::Error> {
        let max_bytes = self.config.media.max_upload_size;
        let media_id = self.generate_media_id();
        let limited_reader = ByteLimitedReader::new(max_bytes, input.stream);
        let mut boxed = Box::new(limited_reader) as Box<dyn AsyncRead + Send + Unpin>;

        self.media_driver
            .store_media(&media_id, &mut boxed)
            .await
            .map_err(move |error| {
                if let MediaError::IoError(io_error) = &error {
                    // TODO: Checking the to_string version of the error seems fragile. We are limited by AsyncRead returning a
                    // std::io::Error.
                    if io_error.to_string() == "max byte exceeded" {
                        return MediaError::MaxBytesExceeded(max_bytes);
                    }
                }

                error
            })?;

        let media_content = MediaContent {
            filename: input.filename.clone(),
            content_type: input.content_type.clone(),
        };

        self.media_storage
            .create_media(&media_id, &media_content)
            .await?;

        Ok(create_content::v3::Response {
            content_uri: media_id,
        })
    }
}

impl CreateMediaCommand {
    pub fn generate_media_id(&self) -> OwnedMxcUri {
        let rand_string: String = thread_rng()
            .sample_iter(&Alphanumeric)
            .take(45)
            .map(char::from)
            .collect();

        let content_uri = format!("mxc://{}/{}", self.config.server_name, rand_string);

        content_uri.into()
    }
}

#[pin_project]
struct ByteLimitedReader<T: AsyncRead + Send + Unpin> {
    byte_limit: usize,
    current: usize,
    #[pin]
    inner: T,
}

impl<T: AsyncRead + Send + Unpin> ByteLimitedReader<T> {
    pub fn new(limit: usize, inner: T) -> Self {
        ByteLimitedReader {
            byte_limit: limit,
            current: 0,
            inner,
        }
    }
}

impl<T: AsyncRead + Send + Unpin> AsyncRead for ByteLimitedReader<T> {
    fn poll_read(
        self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
        buf: &mut tokio::io::ReadBuf<'_>,
    ) -> std::task::Poll<std::io::Result<()>> {
        if self.current >= self.byte_limit {
            return Poll::Ready(Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                "max byte exceeded",
            )));
        }

        let me = self.project();
        let buf_remaining = buf.remaining();
        let poll_result = me.inner.poll_read(cx, buf);

        if let Poll::Ready(Ok(_)) = poll_result {
            let buf_remaining_after = buf.remaining();
            let written = buf_remaining.saturating_sub(buf_remaining_after);

            (*me.current) = (*me.current).saturating_add(written);
        }

        poll_result
    }
}
