use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    command::Command,
    config::HomeserverConfig,
    error::ResourceNotFound,
    ruma::{
        api::client::media::{get_content, get_content_as_filename},
        OwnedMxcUri,
    },
    Inject,
};
use tokio::io::AsyncRead;

use crate::{driver::Driver, error::MediaError, storage::MediaStorage};

pub struct DownloadResponse {
    pub stream: Box<dyn AsyncRead + Unpin + Send>,
    pub content_type: Option<String>,
    pub filename: Option<String>,
}

impl DownloadResponse {
    pub fn content_type(&self) -> String {
        self.content_type
            .clone()
            .unwrap_or_else(|| "application/octet-stream".to_string())
    }

    pub fn content_disposition(&self) -> String {
        self.filename
            .clone()
            .map(|filename| format!("inline; filename={filename}"))
            .unwrap_or_else(|| "inline".to_string())
    }

    pub fn content_security_policy(&self) -> &'static str {
        "sandbox; default-src 'none'; script-src 'none'; plugin-types application/pdf; style-src 'unsafe-inline'; object-src 'self';"
    }

    pub fn stream(self) -> Box<dyn AsyncRead + Unpin + Send> {
        self.stream
    }
}

#[derive(Inject)]
pub struct DownloadCommand {
    config: Arc<HomeserverConfig>,
    media_storage: Arc<dyn MediaStorage>,
    media_driver: Arc<dyn Driver>,
}

#[async_trait]
impl Command for DownloadCommand {
    type Input = get_content::v3::Request;
    type Output = DownloadResponse;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, _: Identity) -> Result<Self::Output, Self::Error> {
        if input.server_name != self.config.server_name {
            return Err(MediaError::FederationNotEnabled.into());
        }

        let uri = format!("mxc://{}/{}", self.config.server_name, input.media_id);
        let media_id: OwnedMxcUri = uri.into();

        let media = self
            .media_storage
            .get_media(&media_id)
            .await?
            .ok_or(ResourceNotFound)?;

        let stream = self.media_driver.load_media(&media_id).await?;

        Ok(DownloadResponse {
            stream,
            content_type: media.content_type,
            filename: media.filename,
        })
    }
}

#[derive(Inject)]
pub struct DownloadFilenameCommand {
    config: Arc<HomeserverConfig>,
    media_storage: Arc<dyn MediaStorage>,
    media_driver: Arc<dyn Driver>,
}

#[async_trait]
impl Command for DownloadFilenameCommand {
    type Input = get_content_as_filename::v3::Request;
    type Output = DownloadResponse;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let cmd = DownloadCommand {
            config: self.config.clone(),
            media_storage: self.media_storage.clone(),
            media_driver: self.media_driver.clone(),
        };

        let download_input = get_content::v3::Request {
            media_id: input.media_id.to_owned(),
            server_name: input.server_name.to_owned(),
            allow_remote: input.allow_remote.to_owned(),
            timeout_ms: input.timeout_ms.to_owned(),
            allow_redirect: input.allow_redirect,
        };

        let mut response = cmd.execute(download_input, id).await?;
        response.filename = Some(input.filename.to_owned());

        Ok(response)
    }
}
