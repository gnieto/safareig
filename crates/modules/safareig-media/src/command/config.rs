use std::{convert::TryFrom, sync::Arc};

use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    command::Command,
    config::{HomeserverConfig, MediaConfig},
    ruma::{api::client::media::get_media_config, UInt},
    Inject,
};

#[derive(Inject)]
pub struct ConfigCommand {
    config: Arc<HomeserverConfig>,
}

#[async_trait]
impl Command for ConfigCommand {
    type Input = get_media_config::v3::Request;
    type Output = get_media_config::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, _: Self::Input, _: Identity) -> Result<Self::Output, Self::Error> {
        let bytes = u64::try_from(self.config.media.max_upload_size)
            .unwrap_or_else(|_| u64::try_from(MediaConfig::default_max_upload_size()).unwrap_or(0));

        Ok(get_media_config::v3::Response {
            upload_size: UInt::new_wrapping(bytes),
        })
    }
}
