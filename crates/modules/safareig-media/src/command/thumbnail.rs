use std::{io::Cursor, sync::Arc};

use async_trait::async_trait;
use image::{io::Reader as ImageReader, ImageFormat, ImageOutputFormat};
use safareig_core::{
    auth::Identity,
    command::Command,
    config::HomeserverConfig,
    error::ResourceNotFound,
    ruma::{
        api::client::media::get_content_thumbnail::{self, v3::Method},
        OwnedMxcUri,
    },
    Inject,
};
use tokio::io::AsyncRead;

use super::DownloadResponse;
use crate::{driver::Driver, error::MediaError, storage::MediaStorage};

#[derive(Inject)]
pub struct ThumbnailCommand {
    config: Arc<HomeserverConfig>,
    media_storage: Arc<dyn MediaStorage>,
    media_driver: Arc<dyn Driver>,
}

#[async_trait]
impl Command for ThumbnailCommand {
    type Input = get_content_thumbnail::v3::Request;
    type Output = DownloadResponse;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, _: Identity) -> Result<Self::Output, Self::Error> {
        if input.server_name != self.config.server_name {
            return Err(MediaError::FederationNotEnabled.into());
        }

        let uri = format!("mxc://{}/{}", self.config.server_name, input.media_id);
        let media_id: OwnedMxcUri = uri.into();

        let media = self
            .media_storage
            .get_media(&media_id)
            .await?
            .ok_or(ResourceNotFound)?;

        let mut stream = self.media_driver.load_media(&media_id).await?;
        let mut content = Vec::new();
        let _ = tokio::io::copy(&mut stream, &mut content).await;

        let stream = match self.generate_thumbnail(&content, media.content_type.clone(), &input) {
            Ok(stream) => stream,
            Err(e) => {
                tracing::error!(err = e.to_string().as_str(), "Could not generate thumbnail",);

                // Serve the original content if thumbnail generation failed
                Box::new(Cursor::new(content))
            }
        };

        Ok(DownloadResponse {
            stream,
            content_type: media.content_type,
            filename: media.filename,
        })
    }
}

impl ThumbnailCommand {
    fn generate_thumbnail(
        &self,
        content: &[u8],
        media_format: Option<String>,
        input: &get_content_thumbnail::v3::Request,
    ) -> Result<Box<dyn AsyncRead + Unpin + Send>, MediaError> {
        let img = ImageReader::new(Cursor::new(content))
            .with_guessed_format()?
            .decode()?;

        let width = img.width();
        let height = img.height();

        // Servers MUST NOT upscale thumbnails under any circumstance.
        let requested_width = u32::try_from(input.width).unwrap().min(width);
        let requested_height = u32::try_from(input.height).unwrap().min(height);

        let mut output_format = media_format.unwrap_or_else(|| "image/png".to_string());
        if output_format.is_empty() {
            output_format = "image/png".to_string();
        }

        let format =
            ImageFormat::from_mime_type(output_format).ok_or(MediaError::InvalidMimeType)?;

        match &input.method {
            Some(Method::Scale) => {
                let mut thumbnail_content = Cursor::new(Vec::new());
                let thumbnail = img.thumbnail(requested_width, requested_height);
                thumbnail.write_to(&mut thumbnail_content, ImageOutputFormat::from(format))?;
                thumbnail_content.set_position(0);

                Ok(Box::new(thumbnail_content))
            }
            Some(Method::Crop) => {
                let mut thumbnail_content = Cursor::new(Vec::new());
                let thumbnail = img.thumbnail_exact(requested_width, requested_height);
                thumbnail.write_to(&mut thumbnail_content, ImageOutputFormat::from(format))?;
                thumbnail_content.set_position(0);

                Ok(Box::new(thumbnail_content))
            }
            Some(method) => {
                tracing::error!(
                    method = method.to_string().as_str(),
                    "Ignoring unknown transformation",
                );

                Err(MediaError::UnknownTransformation)
            }
            _ => {
                tracing::error!("Ignoring undefined transformation",);

                Err(MediaError::UnknownTransformation)
            }
        }
    }
}
