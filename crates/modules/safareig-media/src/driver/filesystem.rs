use std::path::PathBuf;

use async_trait::async_trait;
use safareig_core::ruma::MxcUri;
use tokio::io::AsyncRead;

use super::Driver;
use crate::error::MediaError;

pub struct FileSystemDriver {
    pub(crate) path: PathBuf,
}

#[async_trait]
impl Driver for FileSystemDriver {
    async fn store_media(
        &self,
        mxc_uri: &MxcUri,
        content_stream: &mut Box<dyn AsyncRead + Send + Unpin>,
    ) -> Result<(), MediaError> {
        // TODO: This implementation is not optimal. Probably we should be splitting the files
        // among several folders.

        let path_to_file = self.path_for_uri(mxc_uri)?;

        let mut file = tokio::fs::File::create(path_to_file.clone()).await?;
        let result = tokio::io::copy(content_stream, &mut file).await;

        if result.is_err() {
            let _ = tokio::fs::remove_file(path_to_file.clone()).await?;
        }

        result.map(|_| ()).map_err(|error| error.into())
    }

    async fn load_media(
        &self,
        handle: &MxcUri,
    ) -> Result<Box<dyn AsyncRead + Send + Unpin>, MediaError> {
        let path_to_file = self.path_for_uri(handle)?;
        let file = tokio::fs::File::open(path_to_file.clone()).await?;

        Ok(Box::new(file))
    }
}

impl FileSystemDriver {
    fn path_for_uri(&self, mxc_uri: &MxcUri) -> Result<PathBuf, MediaError> {
        let name = mxc_uri.media_id().map_err(|_| MediaError::InvalidMxcUri)?;
        let mut path_to_file = self.path.clone();
        path_to_file.push(name.replace("..", ""));

        Ok(path_to_file)
    }
}
