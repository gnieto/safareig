use async_trait::async_trait;
use safareig_core::ruma::MxcUri;
use tokio::io::AsyncRead;

use super::Driver;
use crate::error::MediaError;

pub struct NoopDriver {}

#[async_trait]
impl Driver for NoopDriver {
    async fn store_media(
        &self,
        _mxc_uri: &MxcUri,
        _content_stream: &mut Box<dyn AsyncRead + Send + Unpin>,
    ) -> Result<(), MediaError> {
        Ok(())
    }

    async fn load_media(
        &self,
        _handle: &MxcUri,
    ) -> Result<Box<dyn AsyncRead + Send + Unpin>, MediaError> {
        unimplemented!("");
    }
}
