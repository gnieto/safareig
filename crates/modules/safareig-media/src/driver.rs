pub(crate) mod filesystem;
pub(crate) mod noop;

use async_trait::async_trait;
use safareig_core::ruma::MxcUri;
use tokio::io::AsyncRead;

use crate::error::MediaError;

#[async_trait]
pub trait Driver: Send + Sync {
    async fn store_media(
        &self,
        mxc_uri: &MxcUri,
        content_stream: &mut Box<dyn AsyncRead + Send + Unpin>,
    ) -> Result<(), MediaError>;

    async fn load_media(
        &self,
        handle: &MxcUri,
    ) -> Result<Box<dyn AsyncRead + Send + Unpin>, MediaError>;
}
