use std::{path::PathBuf, sync::Arc};

use safareig_core::{
    command::{Container, Module, Router, ServiceCollectionExtension},
    config::{HomeserverConfig, MediaDriver},
    database::DatabaseConnection,
    ddi::{ServiceCollectionExt, ServiceProvider, ServiceResolverExt},
    storage::StorageError,
};

use crate::{
    command::{
        ConfigCommand, CreateMediaCommand, DownloadCommand, DownloadFilenameCommand,
        ThumbnailCommand,
    },
    driver::{filesystem::FileSystemDriver, noop::NoopDriver, Driver},
    storage::MediaStorage,
};

#[derive(Default)]
pub struct MediaModule {}

impl MediaModule {
    pub fn driver(config: &HomeserverConfig) -> Arc<dyn Driver> {
        match &config.media.media_driver {
            MediaDriver::Filesystem(fs) => {
                let path = fs
                    .path
                    .as_ref()
                    .map(PathBuf::from)
                    .unwrap_or_else(|| Self::filesystem_default_path(config));

                Arc::new(FileSystemDriver {
                    path: PathBuf::from(&path),
                })
            }
            MediaDriver::Noop => Arc::new(NoopDriver {}),
        }
    }

    fn storage(connection: DatabaseConnection) -> Arc<dyn MediaStorage> {
        match connection {
            #[cfg(feature = "backend-sqlx")]
            DatabaseConnection::Sqlx(pool) => {
                Arc::new(crate::storage::sqlx::SqlxMediaStorage { pool })
            }
            _ => unimplemented!(""),
        }
    }

    fn create_media_directory(config: Arc<HomeserverConfig>) {
        if let MediaDriver::Filesystem(fs) = &config.media.media_driver {
            let path = fs
                .path
                .as_ref()
                .map(PathBuf::from)
                .unwrap_or_else(|| Self::filesystem_default_path(config.as_ref()));

            if let Err(e) = std::fs::create_dir_all(path) {
                tracing::error!("Could not create FS directory: {}", e);
                panic!("Filesystem driver not properly configured");
            }
        }
    }

    fn filesystem_default_path(config: &HomeserverConfig) -> PathBuf {
        let mut dir = dirs::data_dir().unwrap();
        dir.push("safareig");
        dir.push(config.server_name.to_string());
        dir.push("media");

        dir
    }
}

#[async_trait::async_trait]
impl<R: Router> Module<R> for MediaModule {
    fn register(&self, container: &mut Container, router: &mut R) {
        container.inject_endpoint::<_, ConfigCommand, _>(router);
        container
            .service_collection
            .register::<CreateMediaCommand>();
        container.service_collection.register::<DownloadCommand>();
        container
            .service_collection
            .register::<DownloadFilenameCommand>();
        container.service_collection.register::<ThumbnailCommand>();

        container
            .service_collection
            .service_factory(|db: &DatabaseConnection| Ok(Self::storage(db.clone())));

        container
            .service_collection
            .service_factory(|config: &Arc<HomeserverConfig>| Ok(Self::driver(config.as_ref())));
    }

    async fn run_migrations(&self, container: &ServiceProvider) -> Result<(), StorageError> {
        #[cfg(feature = "backend-sqlx")]
        {
            let db = container.get::<DatabaseConnection>()?;
            safareig_sqlx::run_migration(db, "sqlite", "safareig-media").await?;
        }

        let config = container.get::<Arc<HomeserverConfig>>()?;
        Self::create_media_directory(config.clone());

        Ok(())
    }
}
