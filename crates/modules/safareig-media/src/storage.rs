#[cfg(feature = "backend-sqlx")]
pub mod sqlx;

use async_trait::async_trait;
use safareig_core::{ruma::MxcUri, storage::Result};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct MediaContent {
    /// filename of this media, if specificed on creation
    pub filename: Option<String>,

    /// content type of the file, if specified on creation
    pub content_type: Option<String>,
}

#[async_trait]
pub trait MediaStorage: Send + Sync {
    async fn create_media(&self, media_id: &MxcUri, content: &MediaContent) -> Result<()>;
    async fn get_media(&self, media_id: &MxcUri) -> Result<Option<MediaContent>>;
}
