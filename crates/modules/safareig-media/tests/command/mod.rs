mod config;
mod create_media;
mod download;

use std::{
    collections::HashMap,
    iter::FromIterator,
    sync::{Arc, Mutex},
};

use async_trait::async_trait;
use safareig_core::{
    command::{Container, Module, Router},
    ddi::{ServiceCollectionExt, ServiceProvider},
    ruma::{exports::bytes::Bytes, OwnedMxcUri},
    storage::StorageError,
};
use safareig_media::{driver::Driver, error::MediaError, module::MediaModule};
use tokio::io::{AsyncRead, Result as TokioResult};
use tokio_util::io::StreamReader;

pub struct FailingDriver;

#[async_trait]
impl Driver for FailingDriver {
    async fn store_media(
        &self,
        _: &safareig_core::ruma::MxcUri,
        _: &mut Box<dyn tokio::io::AsyncRead + Send + Unpin>,
    ) -> Result<(), MediaError> {
        Err(MediaError::IoError(std::io::Error::new(
            std::io::ErrorKind::Other,
            "test error",
        )))
    }

    async fn load_media(
        &self,
        _: &safareig_core::ruma::MxcUri,
    ) -> Result<Box<dyn tokio::io::AsyncRead + Send + Unpin>, MediaError> {
        Err(MediaError::IoError(std::io::Error::new(
            std::io::ErrorKind::Other,
            "load test error",
        )))
    }
}
#[derive(Default)]
pub struct InMemoryDriver {
    files: Arc<Mutex<HashMap<OwnedMxcUri, Vec<u8>>>>,
}

impl InMemoryDriver {
    pub fn file_content(&self, media_id: &safareig_core::ruma::MxcUri) -> Vec<u8> {
        let files = self.files.lock().unwrap();

        files.get(media_id).unwrap().clone()
    }
}

#[async_trait]
impl Driver for InMemoryDriver {
    async fn store_media(
        &self,
        mxc_uri: &safareig_core::ruma::MxcUri,
        content_stream: &mut Box<dyn tokio::io::AsyncRead + Send + Unpin>,
    ) -> Result<(), MediaError> {
        let mut writer: Vec<u8> = vec![];

        let _written = tokio::io::copy(content_stream, &mut writer).await? as usize;
        let mut files = self.files.lock().unwrap();
        files.insert(mxc_uri.to_owned(), writer);

        Ok(())
    }

    async fn load_media(
        &self,
        handle: &safareig_core::ruma::MxcUri,
    ) -> Result<Box<dyn tokio::io::AsyncRead + Send + Unpin>, MediaError> {
        let files = self.files.lock().unwrap();
        let bytes = files
            .get(handle)
            .ok_or(MediaError::ResourceNotFound)?
            .clone();

        let stream = tokio_stream::iter(vec![TokioResult::Ok(Bytes::from_iter(bytes))]);

        let read = StreamReader::new(stream);

        Ok(Box::new(read))
    }
}

pub fn hardcoded_input_stream() -> Box<dyn AsyncRead + Send + Unpin> {
    let data = tokio_stream::iter(vec![
        TokioResult::Ok(Bytes::from_static(&[0, 1, 2, 3])),
        TokioResult::Ok(Bytes::from_static(&[4, 5, 6, 7])),
        TokioResult::Ok(Bytes::from_static(&[8, 9, 10, 11])),
    ]);
    let stream = StreamReader::new(data);

    Box::new(stream)
}

pub struct TestingMediaModule {
    driver: Option<Arc<dyn Driver>>,
    media: MediaModule,
}

impl TestingMediaModule {
    pub fn new(media_module: MediaModule) -> Self {
        Self {
            media: media_module,
            driver: None,
        }
    }

    pub fn with_driver(mut self, driver: Arc<dyn Driver>) -> Self {
        self.driver = Some(driver);
        self
    }
}

#[async_trait::async_trait]
impl<R: Router> Module<R> for TestingMediaModule {
    fn register(&self, container: &mut Container, router: &mut R) {
        Module::<R>::register(&self.media, container, router);
        if let Some(driver) = &self.driver {
            container.service_collection.service(driver.clone());
        }
    }

    async fn run_migrations(&self, container: &ServiceProvider) -> Result<(), StorageError> {
        Module::<R>::run_migrations(&self.media, container).await
    }
}
