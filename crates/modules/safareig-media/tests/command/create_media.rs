use std::sync::Arc;

use safareig_core::{
    command::Command,
    ruma::{
        api::client::error::ErrorKind,
        exports::{bytes::Bytes, http::StatusCode},
    },
};
use safareig_media::{
    command::{CreateMediaCommand, CreateMediaCommandRequest},
    module::MediaModule,
    storage::MediaStorage,
};
use safareig_testing::{error::RumaErrorExtension, TestingApp};
use tokio::io::Result as TokioResult;
use tokio_util::io::StreamReader;

use crate::command::{hardcoded_input_stream, FailingDriver, InMemoryDriver, TestingMediaModule};

#[tokio::test]
async fn it_can_create_a_new_media_file() {
    let driver = Arc::new(InMemoryDriver::default());
    let module = TestingMediaModule::new(MediaModule {}).with_driver(driver.clone());
    let app = TestingApp::with_modules(vec![Box::new(module)]).await;

    let cmd = app.command::<CreateMediaCommand>();
    let id: safareig_core::auth::Identity = TestingApp::new_identity();

    let input = CreateMediaCommandRequest {
        filename: Some("filename".to_string()),
        content_type: Some("image/jpeg".to_string()),
        stream: hardcoded_input_stream(),
    };

    let response = cmd.execute(input, id).await.unwrap();
    let media_id = response.content_uri;

    let raw_media = driver.file_content(&media_id);
    assert_eq!(12, raw_media.len());

    let storage = app.service::<Arc<dyn MediaStorage>>();

    let media_content = storage.get_media(&media_id).await.unwrap().unwrap();
    assert_eq!("filename", media_content.filename.unwrap());
    assert_eq!("image/jpeg", media_content.content_type.unwrap());
}

#[tokio::test]
async fn it_does_not_store_the_media_file_if_driver_fails() {
    let driver = Arc::new(FailingDriver {});
    let module = TestingMediaModule::new(MediaModule {}).with_driver(driver.clone());
    let app = TestingApp::with_modules(vec![Box::new(module)]).await;

    let data = tokio_stream::iter(vec![
        TokioResult::Ok(Bytes::from_static(&[0, 1, 2, 3])),
        TokioResult::Ok(Bytes::from_static(&[4, 5, 6, 7])),
        TokioResult::Ok(Bytes::from_static(&[8, 9, 10, 11])),
    ]);
    let stream = StreamReader::new(data);
    let cmd = app.command::<CreateMediaCommand>();
    let id: safareig_core::auth::Identity = TestingApp::new_identity();
    let input = CreateMediaCommandRequest {
        filename: Some("filename".to_string()),
        content_type: Some("image/jpeg".to_string()),
        stream: Box::new(stream),
    };

    let response = cmd.execute(input, id).await;
    assert!(response.is_err());
    let matrix_error = response.err().unwrap();
    assert_eq!(StatusCode::INTERNAL_SERVER_ERROR, matrix_error.status_code);
    assert_eq!(matrix_error.kind(), ErrorKind::Unknown);
    assert_eq!(matrix_error.message(), "IO error");
}

#[tokio::test]
async fn it_does_not_store_large_files() {
    let mut cfg = TestingApp::default_config();
    cfg.media.max_upload_size = 8;
    let driver = Arc::new(InMemoryDriver::default());
    let module = TestingMediaModule::new(MediaModule {}).with_driver(driver.clone());
    let app = TestingApp::with_config_and_modules(cfg, vec![Box::new(module)]).await;

    let data = tokio_stream::iter(vec![
        TokioResult::Ok(Bytes::from_static(&[0, 1, 2, 3])),
        TokioResult::Ok(Bytes::from_static(&[4, 5, 6, 7])),
        TokioResult::Ok(Bytes::from_static(&[8, 9, 10, 11])),
    ]);
    let stream = StreamReader::new(data);
    let cmd = app.command::<CreateMediaCommand>();
    let id: safareig_core::auth::Identity = TestingApp::new_identity();
    let input = CreateMediaCommandRequest {
        filename: Some("filename".to_string()),
        content_type: Some("image/jpeg".to_string()),
        stream: Box::new(stream),
    };

    let response = cmd.execute(input, id).await;
    assert!(response.is_err());
    let matrix_error = response.err().unwrap();
    assert_eq!(StatusCode::PAYLOAD_TOO_LARGE, matrix_error.status_code);
    assert_eq!(matrix_error.kind(), ErrorKind::TooLarge);
    assert_eq!(
        matrix_error.message(),
        "Cannot upload files larger than 8 bytes"
    );
}
