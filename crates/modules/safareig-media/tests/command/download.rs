use std::{sync::Arc, time::Duration};

use safareig_core::{
    auth::Identity,
    command::Command,
    config::HomeserverConfig,
    ruma::{
        api::client::{
            error::ErrorKind,
            media::{get_content, get_content_as_filename},
        },
        exports::http::StatusCode,
        OwnedMxcUri,
    },
};
use safareig_media::{
    command::{
        CreateMediaCommand, CreateMediaCommandRequest, DownloadCommand, DownloadFilenameCommand,
    },
    driver::Driver,
    module::MediaModule,
    storage::MediaStorage,
};
use safareig_testing::{error::RumaErrorExtension, TestingApp};

use super::hardcoded_input_stream;
use crate::command::{FailingDriver, InMemoryDriver, TestingMediaModule};

#[tokio::test]
async fn download_filename_it_downloads_a_file_with_the_filename_assigned() {
    let driver = Arc::new(InMemoryDriver::default());
    let module = TestingMediaModule::new(MediaModule::default()).with_driver(driver.clone());
    let app = TestingApp::with_modules(vec![Box::new(module)]).await;
    let cmd = app.command::<DownloadFilenameCommand>();

    let content_uri = create_media(
        &app,
        driver.clone(),
        Some("filename".to_string()),
        Some("image/jpeg".to_string()),
    )
    .await;
    let request = get_content_as_filename::v3::Request {
        media_id: content_uri.media_id().unwrap().to_string(),
        server_name: content_uri.server_name().unwrap().to_owned(),
        allow_remote: false,
        filename: "test_filename".to_string(),
        allow_redirect: true,
        timeout_ms: Duration::from_secs(1),
    };

    let response = cmd.execute(request, Identity::None).await.unwrap();
    let filename = response.filename.clone();
    let content_disposition = response.content_disposition();
    let content_type = response.content_type();

    let mut content = Vec::new();
    let _ = tokio::io::copy(&mut response.stream(), &mut content).await;

    assert_eq!(12, content.len());
    assert_eq!("test_filename", filename.unwrap());
    assert_eq!("inline; filename=test_filename", content_disposition);
    assert_eq!("image/jpeg", content_type);
}

#[tokio::test]
async fn download_cmd_downloads_a_file_with_the_filename_assigned() {
    let driver = Arc::new(InMemoryDriver::default());
    let module = TestingMediaModule::new(MediaModule::default()).with_driver(driver.clone());
    let app = TestingApp::with_modules(vec![Box::new(module)]).await;

    let cmd = app.command::<DownloadCommand>();
    let content_uri = create_media(
        &app,
        driver.clone(),
        Some("filename".to_string()),
        Some("image/jpeg".to_string()),
    )
    .await;
    let request = get_content::v3::Request {
        media_id: content_uri.media_id().unwrap().to_string(),
        server_name: content_uri.server_name().unwrap().to_owned(),
        allow_remote: false,
        allow_redirect: true,
        timeout_ms: Duration::from_secs(1),
    };

    let response = cmd.execute(request, Identity::None).await.unwrap();
    let filename = response.filename.clone();
    let content_disposition = response.content_disposition();
    let content_type = response.content_type();

    let mut content = Vec::new();
    let _ = tokio::io::copy(&mut response.stream(), &mut content).await;

    assert_eq!(12, content.len());
    assert_eq!("filename", filename.unwrap());
    assert_eq!("inline; filename=filename", content_disposition);
    assert_eq!("image/jpeg", content_type);
}

#[tokio::test]
async fn download_cmd_downloads_a_file_without_filename_or_content_type() {
    let driver = Arc::new(InMemoryDriver::default());
    let module = TestingMediaModule::new(MediaModule::default()).with_driver(driver.clone());
    let app = TestingApp::with_modules(vec![Box::new(module)]).await;
    let cmd = app.command::<DownloadCommand>();

    let content_uri = create_media(&app, driver.clone(), None, None).await;
    let request = get_content::v3::Request {
        media_id: content_uri.media_id().unwrap().to_string(),
        server_name: content_uri.server_name().unwrap().to_owned(),
        allow_remote: false,
        allow_redirect: true,
        timeout_ms: Duration::from_secs(1),
    };

    let response = cmd.execute(request, Identity::None).await.unwrap();
    let content_disposition = response.content_disposition();
    let content_type = response.content_type();

    let mut content = Vec::new();
    let _ = tokio::io::copy(&mut response.stream(), &mut content).await;

    assert_eq!(12, content.len());
    assert_eq!("inline", content_disposition);
    assert_eq!("application/octet-stream", content_type);
}

#[tokio::test]
async fn donwload_cmd_returns_404_in_a_non_existing_media_url() {
    let driver = Arc::new(InMemoryDriver::default());
    let module = TestingMediaModule::new(MediaModule::default()).with_driver(driver.clone());
    let app = TestingApp::with_modules(vec![Box::new(module)]).await;
    let cmd = app.command::<DownloadCommand>();

    let content_uri: OwnedMxcUri = "mxc://test.cat/non-existing".into();
    let request = get_content::v3::Request {
        media_id: content_uri.media_id().unwrap().to_string(),
        server_name: content_uri.server_name().unwrap().to_owned(),
        allow_remote: false,
        allow_redirect: true,
        timeout_ms: Duration::from_secs(1),
    };

    let response = cmd.execute(request, Identity::None).await;

    assert!(response.is_err());
    let matrix_error = response.err().unwrap();
    assert_eq!(StatusCode::NOT_FOUND, matrix_error.status_code);
    assert_eq!(ErrorKind::NotFound, matrix_error.kind());
    assert_eq!("resource not found", matrix_error.message());
}

#[tokio::test]
async fn donwload_cmd_fails_with_federated_request() {
    let driver = Arc::new(InMemoryDriver::default());
    let module = TestingMediaModule::new(MediaModule::default()).with_driver(driver.clone());
    let app = TestingApp::with_modules(vec![Box::new(module)]).await;
    let cmd = app.command::<DownloadCommand>();

    let content_uri: OwnedMxcUri = "mxc://non-exising/non-existing".into();
    let request = get_content::v3::Request {
        media_id: content_uri.media_id().unwrap().to_string(),
        server_name: content_uri.server_name().unwrap().to_owned(),
        allow_remote: false,
        allow_redirect: true,
        timeout_ms: Duration::from_secs(1),
    };

    let response = cmd.execute(request, Identity::None).await;

    assert!(response.is_err());
    let matrix_error = response.err().unwrap();
    assert_eq!(StatusCode::FORBIDDEN, matrix_error.status_code);
    assert_eq!(ErrorKind::forbidden(), matrix_error.kind());
    assert_eq!(
        "Federation requests not allowed on this server",
        matrix_error.message()
    );
}

#[tokio::test]
async fn donwload_cmd_fails_on_a_failing_driver() {
    let driver = Arc::new(FailingDriver {});
    let module = TestingMediaModule::new(MediaModule::default()).with_driver(driver.clone());
    let app = TestingApp::with_modules(vec![Box::new(module)]).await;
    let cmd = app.command::<DownloadCommand>();

    // Create media with a non-erroring driver
    let in_memory_driver = Arc::new(InMemoryDriver::default());
    let content_uri = create_media(&app, in_memory_driver, None, None).await;
    let request = get_content::v3::Request {
        media_id: content_uri.media_id().unwrap().to_string(),
        server_name: content_uri.server_name().unwrap().to_owned(),
        allow_remote: false,
        allow_redirect: true,
        timeout_ms: Duration::from_secs(1),
    };

    let response = cmd.execute(request, Identity::None).await;

    assert!(response.is_err());
    let matrix_error = response.err().unwrap();
    assert_eq!(StatusCode::INTERNAL_SERVER_ERROR, matrix_error.status_code);
    assert_eq!(ErrorKind::Unknown, matrix_error.kind());
    assert_eq!("IO error", matrix_error.message());
}

async fn create_media(
    db: &TestingApp,
    driver: Arc<dyn Driver>,
    filename: Option<String>,
    content_type: Option<String>,
) -> OwnedMxcUri {
    let cmd = CreateMediaCommand::new(
        db.service::<Arc<HomeserverConfig>>(),
        db.service::<Arc<dyn MediaStorage>>(),
        driver.clone(),
    );

    let id = TestingApp::new_identity();
    let input = CreateMediaCommandRequest {
        filename,
        content_type,
        stream: hardcoded_input_stream(),
    };

    let response = cmd.execute(input, id).await.unwrap();

    response.content_uri
}
