use std::convert::From;

use safareig_core::{command::Command, ruma::api::client::media::get_media_config};
use safareig_media::{command::ConfigCommand, module::MediaModule};
use safareig_testing::TestingApp;

#[tokio::test]
async fn returns_config() {
    let mut config = TestingApp::default_config();
    config.media.max_upload_size = 12_345_678;
    let id = TestingApp::new_identity();
    let db = TestingApp::with_config_and_modules(config, vec![Box::<MediaModule>::default()]).await;

    let cmd = db.command::<ConfigCommand>();
    let req = get_media_config::v3::Request {};

    let response = cmd.execute(req, id).await.unwrap();

    assert_eq!(12_345_678u64, u64::from(response.upload_size));
}
