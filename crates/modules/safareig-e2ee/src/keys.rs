use std::{collections::BTreeMap, sync::Arc};

use itertools::Itertools;
use safareig_core::{
    error::error_chain,
    ruma::{
        api::{
            client::{
                error::ErrorBody,
                keys::{
                    claim_keys::v3::OneTimeKeys,
                    upload_signatures::v3::{Failure, SignedKeys},
                },
            },
            federation::keys::claim_keys::v1::OneTimeKeyClaims,
        },
        encryption::{CrossSigningKey, DeviceKeys},
        serde::{Base64, Raw},
        signatures::PublicKeyMap,
        OwnedDeviceId, OwnedDeviceKeyId, OwnedServerName, OwnedUserId, ServerName, UserId,
    },
    Inject, ServerInfo,
};
use safareig_devices::storage::DeviceStorage;
use safareig_federation::{client::ClientBuilder, keys::verify_ruma_object};
use serde::{Deserialize, Serialize};
use serde_json::value::{to_raw_value, RawValue};

use crate::{
    storage::{KeyStorage, Signature, SignatureStorage},
    E2eeError,
};

pub type UserOneTimeKey = BTreeMap<OwnedUserId, OneTimeKeys>;

#[derive(Debug)]
pub struct CrossSigningSignatures(Vec<Signature>);

impl CrossSigningSignatures {
    pub fn signature<'a>(&'a self, key_name: &'a str) -> impl Iterator<Item = &'a Signature> {
        self.0.iter().filter(move |s| s.key_name == key_name)
    }
}

pub struct UserKeys {
    pub device_keys: BTreeMap<OwnedDeviceId, DeviceKeys>,
    pub cross_signing: CrossSigningKeys,
}

impl UserKeys {
    pub fn raw_device_keys(&self) -> BTreeMap<OwnedDeviceId, Raw<DeviceKeys>> {
        self.device_keys
            .iter()
            .map(|(device, keys)| (device.to_owned(), Raw::new(keys).unwrap()))
            .collect()
    }

    pub fn key_map(&self, user: &UserId) -> KeyMap {
        let mut key_map = KeyMap::default();
        for device_key in self.device_keys.values() {
            key_map.insert(&user, &device_key.keys);
        }

        if let Some(master) = self.cross_signing.master() {
            if let Ok(master) = master.deserialize() {
                key_map.insert(&user, &master.keys);
            }
        }

        if let Some(signing) = self.cross_signing.self_signing() {
            if let Ok(signing) = signing.deserialize() {
                key_map.insert(&user, &signing.keys);
            }
        }

        if let Some(user_signing) = self.cross_signing.user_signing() {
            if let Ok(user_signing) = user_signing.deserialize() {
                key_map.insert(&user, &user_signing.keys);
            }
        }

        key_map
    }

    pub fn find_key(&self, key_name: &str) -> Option<Raw<GenericKey>> {
        let device_key = self
            .device_keys
            .iter()
            .find(|(k, _v)| AsRef::<str>::as_ref(k) == key_name)
            .map(|(_, v)| {
                let value = to_raw_value(v).unwrap();

                Raw::<GenericKey>::from_json(value)
            });

        if device_key.is_some() {
            return device_key;
        }

        if let Some(cross) = self.cross_signing.master() {
            if let Ok(deserialized) = cross.deserialize() {
                if deserialized
                    .keys
                    .iter()
                    .any(|k| k.0.as_str().contains(key_name))
                {
                    return Some(cross.clone().cast());
                }
            }
        }

        if let Some(cross) = self.cross_signing.self_signing() {
            if let Ok(deserialized) = cross.deserialize() {
                if deserialized
                    .keys
                    .iter()
                    .any(|k| k.0.as_str().contains(key_name))
                {
                    return Some(cross.clone().cast());
                }
            }
        }

        if let Some(cross) = self.cross_signing.user_signing() {
            if let Ok(deserialized) = cross.deserialize() {
                if deserialized
                    .keys
                    .iter()
                    .any(|k| k.0.as_str().contains(key_name))
                {
                    return Some(cross.clone().cast());
                }
            }
        }

        None
    }

    pub fn add_signatures(&mut self, signatures: CrossSigningSignatures) {
        for (device_id, device_key) in self.device_keys.iter_mut() {
            for signature in signatures.signature(device_id.as_str()) {
                let user_entry = device_key
                    .signatures
                    .entry(signature.signing_user.clone())
                    .or_default();
                user_entry.insert(signature.key_id.clone(), signature.signature.clone());
            }
        }

        if let Err(e) = self.cross_signing.add_signatures(signatures) {
            tracing::error!(
                err = error_chain(&e),
                "could not add some signature to some user's key",
            )
        }
    }
}

#[async_trait::async_trait]
pub trait E2EEKeyHandler: Send + Sync {
    /// Claim the given keys from the target `server_name`. The caller should verify that `key_claim` users belong to the given
    /// `server_name`.
    async fn claim_keys(
        &self,
        server_name: &ServerName,
        key_claim: &OneTimeKeyClaims,
    ) -> Result<UserOneTimeKey, E2eeError>;

    async fn query_keys(
        &self,
        server_name: &ServerName,
        requester: Option<&UserId>,
        users: &BTreeMap<OwnedUserId, Vec<OwnedDeviceId>>,
    ) -> Result<BTreeMap<OwnedUserId, UserKeys>, E2eeError>;
}

#[derive(Inject, Clone)]
pub struct LocalE2EEKeyHandler {
    keys: Arc<dyn KeyStorage>,
    devices: Arc<dyn DeviceStorage>,
    signatures: Arc<dyn SignatureStorage>,
}

#[async_trait::async_trait]
impl E2EEKeyHandler for LocalE2EEKeyHandler {
    async fn claim_keys(
        &self,
        _server_name: &ServerName,
        key_claims: &OneTimeKeyClaims,
    ) -> Result<UserOneTimeKey, E2eeError> {
        let mut user_one_time_keys = UserOneTimeKey::new();

        for (user_id, devices) in key_claims {
            for (device_id, algorithm) in devices {
                let claimed_key = self.keys.remove_one_time_key(device_id, algorithm).await?;

                if let Some((key_id, otk)) = claimed_key {
                    let one_time_keys = user_one_time_keys.entry(user_id.to_owned()).or_default();
                    let device_entry = one_time_keys.entry(device_id.to_owned()).or_default();

                    let raw_otk = Raw::new(&otk).map_err(E2eeError::from)?;
                    device_entry.insert(key_id, raw_otk);

                    break;
                }

                // If no otk could be found, a fallback key, if exists, should be used
                let fallback_key = self.keys.claim_fallback_key(device_id, algorithm).await?;

                if let Some((key_id, raw_otk)) = fallback_key {
                    tracing::warn!(
                        ?user_id,
                        ?device_id,
                        "Using fallback key. Target user/device do not have more one time keys",
                    );
                    let one_time_keys = user_one_time_keys.entry(user_id.to_owned()).or_default();
                    let device_entry = one_time_keys.entry(device_id.to_owned()).or_default();

                    device_entry.insert(key_id, raw_otk);
                }
            }
        }

        Ok(user_one_time_keys)
    }

    async fn query_keys(
        &self,
        _server_name: &ServerName,
        requester: Option<&UserId>,
        users: &BTreeMap<OwnedUserId, Vec<OwnedDeviceId>>,
    ) -> Result<BTreeMap<OwnedUserId, UserKeys>, E2eeError> {
        let mut keys = BTreeMap::new();

        for (user, key_ids) in users.iter() {
            let signatures =
                CrossSigningSignatures(self.signatures.load_signatures(user, requester).await?);
            let device_ids = if key_ids.is_empty() {
                Some(
                    self.devices
                        .devices_by_user(user)
                        .await?
                        .into_iter()
                        .map(|d| d.ruma_device.device_id)
                        .collect(),
                )
            } else {
                Some(key_ids.clone())
            };

            let user_device_keys = self
                .keys
                .get_keys(user, device_ids.as_deref())
                .await?
                .into_iter()
                .map(|device_keys| (device_keys.device_id.clone(), device_keys))
                .collect();

            let cross_signing = self.keys.user_cross_signing_keys(user).await?;

            let mut user_keys = UserKeys {
                device_keys: user_device_keys,
                cross_signing,
            };
            user_keys.add_signatures(signatures);

            keys.insert(user.clone(), user_keys);
        }

        Ok(keys)
    }
}

#[derive(Inject)]
pub struct FederatedE2EEKeyHandler {
    local: LocalE2EEKeyHandler,
    client_builder: Arc<ClientBuilder>,
    server_info: ServerInfo,
    keys: Arc<dyn KeyStorage>,
    signatures: Arc<dyn SignatureStorage>,
}

#[async_trait::async_trait]
impl E2EEKeyHandler for FederatedE2EEKeyHandler {
    async fn claim_keys(
        &self,
        server_name: &ServerName,
        key_claim: &OneTimeKeyClaims,
    ) -> Result<UserOneTimeKey, E2eeError> {
        if self.server_info.is_local_server(server_name) {
            return self.local.claim_keys(server_name, key_claim).await;
        }

        let client = self.client_builder.client_for(server_name).await?;

        let request = safareig_core::ruma::api::federation::keys::claim_keys::v1::Request {
            one_time_keys: key_claim.clone(),
        };
        let response = client.auth_request(request).await?;

        Ok(response.one_time_keys)
    }

    async fn query_keys(
        &self,
        server_name: &ServerName,
        requester: Option<&UserId>,
        users: &BTreeMap<OwnedUserId, Vec<OwnedDeviceId>>,
    ) -> Result<BTreeMap<OwnedUserId, UserKeys>, E2eeError> {
        if self.server_info.is_local_server(server_name) {
            return self.local.query_keys(server_name, requester, users).await;
        }

        let client = self.client_builder.client_for(server_name).await?;
        let request = safareig_core::ruma::api::federation::keys::get_keys::v1::Request {
            device_keys: users.to_owned(),
        };
        let response = client.auth_request(request).await?;

        let mut user_keys = BTreeMap::new();

        for (user, keys) in response.device_keys.into_iter() {
            let user_id = user.to_owned();
            let user_entry: &mut UserKeys = user_keys.entry(user).or_insert_with(move || {
                let cross_signing = CrossSigningKeys::new(user_id);
                UserKeys {
                    device_keys: Default::default(),
                    cross_signing,
                }
            });

            for (device, key) in keys {
                let current_device_key: DeviceKeys = key.deserialize()?;
                self.keys.update_keys(&current_device_key).await?;
                user_entry.device_keys.insert(device, current_device_key);
            }
        }

        for (user, master_key) in response.master_keys.into_iter() {
            let user_id = user.to_owned();
            let user_entry: &mut UserKeys = user_keys.entry(user).or_insert_with(move || {
                let cross_signing = CrossSigningKeys::new(user_id);
                UserKeys {
                    device_keys: Default::default(),
                    cross_signing,
                }
            });

            if let Err(e) = user_entry.cross_signing.update_master(master_key) {
                tracing::error!(
                    err = error_chain(&e),
                    "could not update master key for target user",
                )
            }
        }

        for (user, self_signing) in response.self_signing_keys.into_iter() {
            let user_id = user.to_owned();
            let user_entry: &mut UserKeys = user_keys.entry(user).or_insert_with(move || {
                let cross_signing = CrossSigningKeys::new(user_id);
                UserKeys {
                    device_keys: Default::default(),
                    cross_signing,
                }
            });

            let key_map = user_entry.cross_signing.key_map();
            if let Err(e) = user_entry
                .cross_signing
                .update_self_signing(self_signing, &key_map)
            {
                tracing::error!(
                    err = error_chain(&e),
                    "could not update user signing key for target user",
                )
            }
        }

        for (user_id, user) in &mut user_keys {
            let signatures =
                CrossSigningSignatures(self.signatures.load_signatures(user_id, None).await?);

            if let Err(e) = self
                .keys
                .update_cross_signing_keys(&user.cross_signing)
                .await
            {
                tracing::error!(
                    err = error_chain(&e),
                    user = user_id.as_str(),
                    "could not update cross signing keys for user",
                );
            }

            user.add_signatures(signatures);
        }

        Ok(user_keys)
    }
}

pub fn group_map_by_server_name<V>(
    keys: BTreeMap<OwnedUserId, V>,
) -> BTreeMap<OwnedServerName, BTreeMap<OwnedUserId, V>> {
    let keys_by_server: BTreeMap<OwnedServerName, BTreeMap<OwnedUserId, V>> = keys
        .into_iter()
        .group_by(|(user, _)| user.server_name().to_owned())
        .into_iter()
        .map(|(server_name, claims)| (server_name, claims.collect()))
        .collect();

    keys_by_server
}

#[derive(Default)]
pub struct KeyMap {
    public_key_map: PublicKeyMap,
}

impl KeyMap {
    pub fn public_key_map(&self) -> &PublicKeyMap {
        &self.public_key_map
    }

    pub fn insert<E: ToString, K: AsRef<str>, V: AsRef<[u8]>>(
        &mut self,
        entity: &E,
        key_map: &BTreeMap<K, V>,
    ) {
        let key_map_iter = key_map.iter().filter_map(|(k, v)| {
            Base64::parse(v.as_ref())
                .ok()
                .map(|b64| (k.as_ref().to_string(), b64))
        });

        let user_entry = self.public_key_map.entry(entity.to_string()).or_default();
        user_entry.extend(key_map_iter);
    }

    pub fn merge(mut self, key_map: Self) -> Self {
        for (user, keys) in key_map.public_key_map {
            let entry = self.public_key_map.entry(user).or_default();
            entry.extend(keys);
        }

        self
    }
}

pub struct CrossSigningKeys {
    user_id: OwnedUserId,
    master: Option<Raw<CrossSigningKey>>,
    user_signing: Option<Raw<CrossSigningKey>>,
    self_signing: Option<Raw<CrossSigningKey>>,
}

impl CrossSigningKeys {
    pub fn new(user_id: OwnedUserId) -> Self {
        Self {
            user_id,
            master: None,
            user_signing: None,
            self_signing: None,
        }
    }

    pub fn unverified(
        user_id: OwnedUserId,
        master: Option<Raw<CrossSigningKey>>,
        user_signing: Option<Raw<CrossSigningKey>>,
        self_signing: Option<Raw<CrossSigningKey>>,
    ) -> Self {
        Self {
            user_id,
            master,
            user_signing,
            self_signing,
        }
    }

    pub fn user_id(&self) -> &UserId {
        self.user_id.as_ref()
    }

    pub fn master(&self) -> Option<&Raw<CrossSigningKey>> {
        self.master.as_ref()
    }

    pub fn user_signing(&self) -> Option<&Raw<CrossSigningKey>> {
        self.user_signing.as_ref()
    }

    pub fn self_signing(&self) -> Option<&Raw<CrossSigningKey>> {
        self.self_signing.as_ref()
    }

    pub fn add_signatures(&mut self, signatures: CrossSigningSignatures) -> Result<(), E2eeError> {
        if let Some(master) = &self.master {
            let mut generic = master.clone().cast::<GenericKey>().deserialize()?;
            let key_id = generic.keys.iter().next().map(|t| t.0).cloned();
            if let Some(key_id) = key_id {
                let key_name = key_id.device_id().as_str();
                for s in signatures.signature(key_name) {
                    generic.add_signature(s.clone());
                }
            }

            let cross_key = to_raw_value(&generic)?;
            self.master = Some(Raw::from_json(cross_key));
        }

        if let Some(user_signing) = &self.user_signing {
            let mut generic = user_signing.clone().cast::<GenericKey>().deserialize()?;
            let key_id = generic.keys.iter().next().map(|t| t.0).cloned();
            if let Some(key_id) = key_id {
                let key_name = key_id.device_id().as_str();
                for s in signatures.signature(key_name) {
                    generic.add_signature(s.clone());
                }
            }

            let cross_key = to_raw_value(&generic)?;
            self.user_signing = Some(Raw::from_json(cross_key));
        }

        if let Some(self_signing) = &self.self_signing {
            let mut generic = self_signing.clone().cast::<GenericKey>().deserialize()?;
            let key_id = generic.keys.iter().next().map(|t| t.0).cloned();
            if let Some(key_id) = key_id {
                let key_name = key_id.device_id().as_str();
                for s in signatures.signature(key_name) {
                    generic.add_signature(s.clone());
                }
            }

            let cross_key = to_raw_value(&generic)?;
            self.self_signing = Some(Raw::from_json(cross_key));
        }

        Ok(())
    }

    pub fn update_master(&mut self, master: Raw<CrossSigningKey>) -> Result<KeyMap, E2eeError> {
        let decoded = master.deserialize()?;
        let mut key_map = KeyMap::default();
        key_map.insert(&self.user_id, &decoded.keys);
        self.validate_key(&decoded)?;

        if !decoded.signatures.is_empty() {
            let is_signed_by_master_key = decoded
                .signatures
                .get(self.user_id())
                .map(|signatures| signatures.keys().any(|k| decoded.keys.contains_key(k)))
                .unwrap_or(false);

            if is_signed_by_master_key {
                verify_ruma_object(&master, key_map.public_key_map())
                    .map_err(|_| E2eeError::KeySignatureValidation)?;
            }
        }

        self.master = Some(master);
        self.user_signing = None;
        self.self_signing = None;

        Ok(key_map)
    }

    pub fn update_user_signing(
        &mut self,
        user_signing: Raw<CrossSigningKey>,
        key_map: &KeyMap,
    ) -> Result<(), E2eeError> {
        let cross_key = user_signing.deserialize()?;
        self.validate_key(&cross_key)?;
        verify_ruma_object(&user_signing, key_map.public_key_map())
            .map_err(|_| E2eeError::KeySignatureValidation)?;
        self.user_signing = Some(user_signing);

        Ok(())
    }

    pub fn update_self_signing(
        &mut self,
        self_signing: Raw<CrossSigningKey>,
        key_map: &KeyMap,
    ) -> Result<(), E2eeError> {
        let cross_key = self_signing.deserialize()?;
        self.validate_key(&cross_key)?;
        verify_ruma_object(&self_signing, key_map.public_key_map())
            .map_err(|_| E2eeError::KeySignatureValidation)?;
        self.self_signing = Some(self_signing);

        Ok(())
    }

    pub fn key_map(&self) -> KeyMap {
        let mut key_map = KeyMap::default();

        if let Some(master) = &self.master {
            if let Ok(key) = master.deserialize() {
                key_map.insert(&self.user_id, &key.keys);
            }
        }

        if let Some(master) = &self.self_signing {
            if let Ok(key) = master.deserialize() {
                key_map.insert(&self.user_id, &key.keys);
            }
        }

        if let Some(master) = &self.user_signing {
            if let Ok(key) = master.deserialize() {
                key_map.insert(&self.user_id, &key.keys);
            }
        }

        key_map
    }

    fn validate_key(&self, cross_key: &CrossSigningKey) -> Result<(), E2eeError> {
        if cross_key.keys.len() != 1 {
            return Err(E2eeError::CrossKeyMultipleKeys);
        }

        if let Some(master_key) = &self.master {
            let master_deserialized = master_key.deserialize()?;

            let conflicting_key = cross_key.keys.iter().any(|(id, public)| {
                master_deserialized.keys.contains_key(id)
                    || master_deserialized
                        .keys
                        .iter()
                        .any(|(_, master_public)| master_public == public)
            });

            if conflicting_key {
                return Err(E2eeError::ConflictingMasterKey);
            }
        }

        Ok(())
    }
}

#[derive(Default)]
pub struct SignatureFailures(BTreeMap<OwnedUserId, BTreeMap<String, E2eeError>>);

impl SignatureFailures {
    pub fn record_failure(&mut self, user_id: &UserId, key: &str, error: E2eeError) {
        tracing::error!(
            err = error_chain(&error),
            key_name = key,
            user = user_id.as_str(),
            "failed to collect key"
        );

        let user_entry = self.0.entry(user_id.to_owned()).or_default();
        user_entry.insert(key.to_string(), error);
    }
}

impl From<SignatureFailures> for BTreeMap<OwnedUserId, BTreeMap<String, Failure>> {
    fn from(failures: SignatureFailures) -> Self {
        failures
            .0
            .into_iter()
            .map(|(user, user_failures)| {
                let user_failures = user_failures
                    .into_iter()
                    .map(|(key, error)| {
                        let matrix_error = safareig_core::ruma::api::client::Error::from(error);
                        let failure = match matrix_error.body {
                            ErrorBody::Standard { message, kind } => {
                                serde_json::json!({
                                    "errcode": kind,
                                    "error": message,
                                })
                            }
                            _ => {
                                serde_json::json!({
                                    "errcode": "M_UNKNOWN",
                                    "error": "unknown failure",
                                })
                            }
                        };
                        let failure = serde_json::from_value(failure).unwrap();

                        (key, failure)
                    })
                    .collect();

                (user, user_failures)
            })
            .collect()
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct GenericKey {
    #[serde(default, skip_serializing_if = "BTreeMap::is_empty")]
    pub signatures: BTreeMap<OwnedUserId, BTreeMap<OwnedDeviceKeyId, String>>,
    #[serde(default, skip_serializing_if = "BTreeMap::is_empty")]
    pub keys: BTreeMap<OwnedDeviceKeyId, String>,
    #[serde(flatten)]
    pub extra: serde_json::Map<String, serde_json::Value>,
}

impl GenericKey {
    pub fn add_signatures(
        &mut self,
        signatures: BTreeMap<OwnedUserId, BTreeMap<OwnedDeviceKeyId, String>>,
    ) {
        for (user, signatures) in signatures {
            let user_entry = self.signatures.entry(user).or_default();

            for (key_id, signature) in signatures {
                user_entry.insert(key_id, signature);
            }
        }
    }

    pub fn add_signature(&mut self, signature: Signature) {
        let user_entry = self.signatures.entry(signature.signing_user).or_default();
        user_entry.insert(signature.key_id, signature.signature);
    }
}

#[derive(Inject, Clone)]
pub struct SignatureUploader {
    storage: Arc<dyn KeyStorage>,
    key_handler: Arc<dyn E2EEKeyHandler>,
    signature_storage: Arc<dyn SignatureStorage>,
}

impl SignatureUploader {
    pub async fn upload_signatures(
        &self,
        signer: &UserId,
        keys: BTreeMap<OwnedUserId, SignedKeys>,
    ) -> Result<SignatureFailures, E2eeError> {
        let mut signatures_to_store = Vec::new();
        let mut failures = SignatureFailures::default();
        let mut key_map = self.load_signer_key_map(signer).await?;

        for (user, signed_keys) in keys {
            if user != signer {
                key_map = self.add_user_public_keys(key_map, &user).await?;
            }

            for (key_name, key) in signed_keys.iter() {
                if let Err(e) = self.collect_key_signatures(
                    &mut signatures_to_store,
                    key,
                    key_name,
                    &key_map,
                    signer,
                ) {
                    failures.record_failure(&user, key_name, e);
                }
            }
        }

        self.signature_storage
            .insert_signatures(&signatures_to_store)
            .await?;

        Ok(failures)
    }

    fn collect_key_signatures(
        &self,
        signatures_to_store: &mut Vec<Signature>,
        key: &RawValue,
        key_name: &str,
        key_map: &KeyMap,
        signer: &UserId,
    ) -> Result<(), E2eeError> {
        let raw = Raw::<GenericKey>::from_json_string(key.get().to_string())?;
        let generic_key = raw.deserialize()?;

        verify_ruma_object(&generic_key, key_map.public_key_map())?;

        let current_signatures = raw.deserialize()?.signatures;
        for (target_user, signatures) in current_signatures {
            for (key_id, signature) in signatures {
                let s = Signature {
                    signing_user: signer.to_owned(),
                    target_user: target_user.to_owned(),
                    key_id: key_id.to_owned(),
                    signature: signature.to_string(),
                    key_name: key_name.to_owned(),
                };

                signatures_to_store.push(s);
            }
        }

        Ok(())
    }

    async fn load_signer_key_map(&self, signer: &UserId) -> Result<KeyMap, E2eeError> {
        let cross_keys = self.storage.user_cross_signing_keys(signer).await?;
        let mut key_map = cross_keys.key_map();
        let device_keys = self.storage.get_keys(signer, None).await?;

        for device_key in device_keys {
            key_map.insert(&signer, &device_key.keys);
        }

        Ok(key_map)
    }

    async fn add_user_public_keys(
        &self,
        mut key_map: KeyMap,
        user: &UserId,
    ) -> Result<KeyMap, E2eeError> {
        let mut users = BTreeMap::new();
        users.insert(user.to_owned(), Default::default());
        let mut public_keys = self
            .key_handler
            .query_keys(user.server_name(), None, &users)
            .await?;

        if let Some(keys) = public_keys.remove(user) {
            key_map = key_map.merge(keys.key_map(user));
        }

        Ok(key_map)
    }
}
