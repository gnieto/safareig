use std::{collections::HashSet, ops::Deref, sync::Arc};

use async_trait::async_trait;
use safareig_core::{
    auth::FederationAuth,
    error::error_chain,
    ruma::{
        api::{
            client::device::Device as RumaDevice,
            federation::{
                device::get_devices::v1 as get_devices,
                transactions::edu::{DeviceListUpdateContent, Edu, SigningKeyUpdateContent},
            },
        },
        OwnedUserId, UInt,
    },
    storage::StorageError,
    Inject, ServerInfo, StreamToken,
};
use safareig_devices::storage::{Device, DeviceListState, DeviceListStorage, DeviceStorage};
use safareig_ephemeral::services::{EphemeralAggregator, EphemeralEvent, EphemeralHandler};
use safareig_federation::client::ClientBuilder;

use crate::{keys::CrossSigningKeys, storage::KeyStorage, stream::E2eeStream};

#[derive(Inject)]
pub struct EphemeralCrossSigningHandler {
    key_storage: Arc<dyn KeyStorage>,
    key_stream: E2eeStream,
    server_info: ServerInfo,
}

#[async_trait]
impl EphemeralHandler for EphemeralCrossSigningHandler {
    async fn on_federation(&self, edu: Edu, _auth: &FederationAuth) {
        let key_signing_update = match edu {
            Edu::SigningKeyUpdate(update) => update,
            _ => return,
        };

        let user_id = key_signing_update.user_id.clone();

        // TODO: Validate incoming signatures
        let user_cross = CrossSigningKeys::unverified(
            key_signing_update.user_id,
            key_signing_update.master_key,
            None,
            key_signing_update.self_signing_key,
        );

        if let Err(e) = self
            .key_storage
            .update_cross_signing_keys(&user_cross)
            .await
        {
            tracing::error!(
                user_id = user_id.as_str(),
                err = error_chain(&e),
                "error storing incoming e2ee cross signing keys",
            );

            return;
        }

        if let Err(e) = self
            .key_stream
            .send_cross_signing_key_update(&user_id)
            .await
        {
            tracing::error!(
                user_id = user_id.as_str(),
                err = error_chain(&e),
                "error storing to stream e2ee cross signing key update",
            );
        }
    }

    fn edu_aggregator(&self) -> Box<dyn EphemeralAggregator> {
        Box::new(EphemeralCrossSigningAggregator::new(
            self.key_storage.clone(),
            self.server_info.clone(),
        ))
    }
}

pub struct EphemeralCrossSigningAggregator {
    edus: Vec<Edu>,
    users: HashSet<OwnedUserId>,
    key_storage: Arc<dyn KeyStorage>,
    server_info: ServerInfo,
}

impl EphemeralCrossSigningAggregator {
    pub fn new(key_storage: Arc<dyn KeyStorage>, server_info: ServerInfo) -> Self {
        EphemeralCrossSigningAggregator {
            edus: Vec::new(),
            users: HashSet::new(),
            key_storage,
            server_info,
        }
    }
}

#[async_trait]
impl EphemeralAggregator for EphemeralCrossSigningAggregator {
    async fn aggregate_event(&mut self, event: EphemeralEvent) {
        let user_id = match event {
            EphemeralEvent::CrossSigning(user_id) => user_id,
            _ => return,
        };

        if !self.server_info.is_local_server(user_id.server_name()) {
            return;
        }

        if self.users.contains(&user_id) {
            return;
        }

        if let Ok(user_keys) = self.key_storage.user_cross_signing_keys(&user_id).await {
            let key_update = SigningKeyUpdateContent {
                user_id: user_id.clone(),
                master_key: user_keys.master().cloned(),
                self_signing_key: user_keys.self_signing().cloned(),
            };
            let edu = Edu::SigningKeyUpdate(key_update);
            self.edus.push(edu);
        }

        self.users.insert(user_id);
    }

    fn edus(self: Box<Self>) -> Vec<Edu> {
        self.edus.clone()
    }
}

#[derive(Inject)]
pub struct EphemeralDeviceListHandler {
    device: Arc<dyn DeviceStorage>,
    keys: Arc<dyn KeyStorage>,
    device_list: Arc<dyn DeviceListStorage>,
    client_builder: Arc<ClientBuilder>,
}

#[async_trait]
impl EphemeralHandler for EphemeralDeviceListHandler {
    async fn on_federation(&self, edu: Edu, auth: &FederationAuth) {
        let update = match edu {
            Edu::DeviceListUpdate(update) => update,
            _ => return,
        };

        if let Err(e) = self.handle_update(update).await {
            tracing::error!(
                server = auth.server.as_str(),
                err = e.to_string().as_str(),
                "errored while updating a device list update",
            );
        }
    }

    fn edu_aggregator(&self) -> Box<dyn EphemeralAggregator> {
        Box::new(EphemeralDeviceAggregator::new(self.device.clone()))
    }
}

impl EphemeralDeviceListHandler {
    async fn handle_update(&self, update: DeviceListUpdateContent) -> Result<(), StorageError> {
        let user = &update.user_id;
        let state = self.device_list.device_list_state(user).await?;
        let prev_update = update.prev_id.first();

        if Self::needs_full_sync(state.stream_id, prev_update) {
            // TODO: Consider tokio::spawn
            // TODO: Handle concurrency issues w/ delta update or other key operations that may happen concurrently
            self.full_device_sync(&update).await?;
        } else {
            self.incremental_device_update(update).await?;
        }

        Ok(())
    }

    async fn incremental_device_update(
        &self,
        update: DeviceListUpdateContent,
    ) -> Result<(), StorageError> {
        let device = Device {
            ruma_device: RumaDevice {
                device_id: update.device_id,
                display_name: update.device_display_name,
                last_seen_ip: None,
                last_seen_ts: None,
            },
            current_auth: None,
            refresh_token: None,
            user: update.user_id.clone(),
        };

        self.device.update_device(&device).await?;
        if let Some(keys) = update.keys {
            let keys = keys.deserialize().unwrap();
            self.keys.update_keys(&keys).await?;
        }

        let stream_id: u64 = TryFrom::try_from(update.stream_id).unwrap_or(0);
        let device_list_state = DeviceListState {
            stream_id: StreamToken::from(stream_id),
            stale: false,
        };

        self.device_list
            .update_device_list(&update.user_id, device_list_state)
            .await?;

        Ok(())
    }

    async fn full_device_sync(&self, update: &DeviceListUpdateContent) -> Result<(), StorageError> {
        let target_server = update.user_id.server_name();
        let client = self
            .client_builder
            .client_for(target_server)
            .await
            .map_err(|e| StorageError::Custom(e.to_string()))?;

        let request = get_devices::Request {
            user_id: update.user_id.to_owned(),
        };

        let response = client.auth_request(request).await;

        match response {
            Err(e) => {
                tracing::error!(
                    server = target_server.as_str(),
                    err = e.to_string().as_str(),
                    "could not fetch full device list from remote server",
                );

                let device_list_state = DeviceListState {
                    stream_id: StreamToken::horizon(),
                    stale: true,
                };

                self.device_list
                    .update_device_list(&update.user_id, device_list_state)
                    .await?;
            }
            Ok(response) => {
                let stream_id: u64 = TryFrom::try_from(response.stream_id).unwrap_or(0);

                for new_device in response.devices {
                    let device = Device {
                        ruma_device: RumaDevice {
                            device_id: new_device.device_id,
                            display_name: new_device.device_display_name,
                            last_seen_ip: None,
                            last_seen_ts: None,
                        },
                        current_auth: None,
                        user: response.user_id.clone(),
                        refresh_token: None,
                    };

                    self.device.update_device(&device).await?;
                    let keys = new_device.keys.deserialize().unwrap();
                    self.keys.update_keys(&keys).await?;
                }

                let device_list_state = DeviceListState {
                    stream_id: StreamToken::from(stream_id),
                    stale: false,
                };

                self.device_list
                    .update_device_list(&update.user_id, device_list_state)
                    .await?;
            }
        }

        Ok(())
    }

    fn needs_full_sync(current: StreamToken, prev_update: Option<&UInt>) -> bool {
        if let Some(prev_id) = prev_update {
            let prev_id = u64::try_from(*prev_id).unwrap_or(0);
            let current = *current.deref();

            prev_id != current
        } else {
            true
        }
    }
}

pub struct EphemeralDeviceAggregator {
    edus: Vec<Edu>,
    devices: Arc<dyn DeviceStorage>,
}

impl EphemeralDeviceAggregator {
    pub fn new(devices: Arc<dyn DeviceStorage>) -> Self {
        EphemeralDeviceAggregator {
            edus: Vec::new(),
            devices,
        }
    }
}

#[async_trait]
impl EphemeralAggregator for EphemeralDeviceAggregator {
    async fn aggregate_event(&mut self, event: EphemeralEvent) {
        if let EphemeralEvent::DeviceUpdate(user_id, device_id, stream_id) = event {
            let is_server_interested_in_user = true;

            if is_server_interested_in_user {
                let maybe_edu = match self.devices.get_device(&user_id, &device_id).await {
                    Ok(None) => {
                        let content = DeviceListUpdateContent {
                            user_id,
                            device_id,
                            device_display_name: None,
                            stream_id: stream_id.into(),
                            prev_id: vec![stream_id.prev().into()],
                            deleted: Some(true),
                            keys: Default::default(),
                        };

                        Some(Edu::DeviceListUpdate(content))
                    }
                    Ok(Some(device)) => {
                        let content = DeviceListUpdateContent {
                            user_id,
                            device_id,
                            device_display_name: device.ruma_device.display_name,
                            stream_id: stream_id.into(),
                            prev_id: vec![stream_id.prev().into()],
                            deleted: None,
                            keys: Default::default(),
                        };

                        Some(Edu::DeviceListUpdate(content))
                    }
                    Err(e) => {
                        tracing::error!(
                            err = e.to_string().as_str(),
                            device = device_id.as_str(),
                            user = user_id.as_str(),
                            "could not load device information from the database. EDU will be skipped"
                        );

                        None
                    }
                };

                if let Some(edu) = maybe_edu {
                    self.edus.push(edu);
                }
            }
        }
    }

    fn edus(self: Box<Self>) -> Vec<Edu> {
        self.edus.clone()
    }
}
