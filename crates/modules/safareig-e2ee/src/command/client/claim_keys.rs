use std::{collections::BTreeMap, sync::Arc};

use async_trait::async_trait;
use safareig_core::{
    auth::Identity, command::Command, ruma::api::client::keys::claim_keys, Inject,
};
use serde_json::Value;

use crate::keys::{group_map_by_server_name, E2EEKeyHandler, UserOneTimeKey};

#[derive(Inject)]
pub struct ClaimKeysCommand {
    key_handler: Arc<dyn E2EEKeyHandler>,
}

#[async_trait]
impl Command for ClaimKeysCommand {
    type Input = claim_keys::v3::Request;
    type Output = claim_keys::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, _: Identity) -> Result<Self::Output, Self::Error> {
        // TODO: Rate-limit the amount of claims per unit of time
        let mut claimed_keys = UserOneTimeKey::new();
        let mut failures: BTreeMap<String, Value> = Default::default();

        let keys_by_server = group_map_by_server_name(input.one_time_keys);

        for (server, otk) in keys_by_server.into_iter() {
            match self.key_handler.claim_keys(&server, &otk).await {
                Ok(mut otk) => {
                    claimed_keys.append(&mut otk);
                }
                Err(e) => {
                    let server_name = server.to_string();

                    tracing::error!(
                        err = e.to_string().as_str(),
                        server_name = server_name.as_str(),
                        "could not claim one time keys",
                    );
                    failures.insert(
                        server_name,
                        Value::String("could not claim one time keys".to_string()),
                    );
                }
            }
        }

        Ok(claim_keys::v3::Response {
            failures,
            one_time_keys: claimed_keys,
        })
    }
}
