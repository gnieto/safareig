use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{auth::Identity, command::Command, ruma::api::client::keys::get_keys, Inject};

use crate::keys::{group_map_by_server_name, E2EEKeyHandler};

#[derive(Inject)]
pub struct QueryKeysCommand {
    key_handler: Arc<dyn E2EEKeyHandler>,
}

#[async_trait]
impl Command for QueryKeysCommand {
    type Input = get_keys::v3::Request;
    type Output = get_keys::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let mut response = get_keys::v3::Response {
            failures: Default::default(),
            device_keys: Default::default(),
            master_keys: Default::default(),
            self_signing_keys: Default::default(),
            user_signing_keys: Default::default(),
        };
        let device_keys_by_server = group_map_by_server_name(input.device_keys);

        for (server, users) in device_keys_by_server {
            let users_keys = self
                .key_handler
                .query_keys(&server, Some(id.user()), &users)
                .await?;

            for (user_id, user_keys) in users_keys {
                response
                    .device_keys
                    .insert(user_id.to_owned(), user_keys.raw_device_keys());

                if let Some(master) = user_keys.cross_signing.master() {
                    response
                        .master_keys
                        .insert(user_id.to_owned(), master.clone());
                }

                if let Some(self_signing) = user_keys.cross_signing.self_signing() {
                    response
                        .self_signing_keys
                        .insert(user_id.to_owned(), self_signing.clone());
                }

                // Return user signing key only if user's request matches with queried user
                if id.user() == user_id {
                    if let Some(user_signing) = user_keys.cross_signing.user_signing() {
                        response
                            .user_signing_keys
                            .insert(user_id.to_owned(), user_signing.clone());
                    }
                }
            }
        }

        Ok(response)
    }
}
