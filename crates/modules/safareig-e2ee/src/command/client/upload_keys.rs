use std::{collections::BTreeMap, sync::Arc};

use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    command::Command,
    ruma::{
        api::client::keys::upload_keys,
        encryption::{DeviceKeys, OneTimeKey},
        serde::Raw,
        DeviceKeyAlgorithm, OwnedDeviceKeyId, UInt,
    },
    Inject,
};

use crate::{storage::KeyStorage, stream::E2eeStream, E2eeError};

#[derive(Inject)]
pub struct UploadKeysCommand {
    keys: Arc<dyn KeyStorage>,
    stream: E2eeStream,
}

#[async_trait]
impl Command for UploadKeysCommand {
    type Input = upload_keys::v3::Request;
    type Output = upload_keys::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        if let Some(device_keys) = &input.device_keys {
            let device_keys: DeviceKeys = device_keys.deserialize().map_err(E2eeError::from)?;
            id.check_device_user(&device_keys.user_id, &device_keys.device_id)?;
            // TODO: validate signatures!
            self.keys.update_keys(&device_keys).await?;
        }

        if !input.one_time_keys.is_empty() {
            let otks: &BTreeMap<OwnedDeviceKeyId, Raw<OneTimeKey>> = &input.one_time_keys;

            for (key_id, otk) in otks.iter() {
                let otk = otk.deserialize().map_err(E2eeError::from)?;
                self.keys
                    .add_one_time_key(id.device().unwrap(), key_id, &otk)
                    .await?;
            }
        }

        if !input.fallback_keys.is_empty() {
            for (key_id, otk) in input.fallback_keys.iter() {
                // Verify that OTK can be deserialized to a fallback key, but send the raw value to storage
                _ = otk.deserialize().map_err(E2eeError::from)?;
                self.keys
                    .add_fallback_key(id.device().unwrap(), key_id, otk)
                    .await?;
            }
        }

        let counts = self
            .keys
            .pending_keys(id.device().unwrap())
            .await?
            .into_iter()
            .map(|(k, counter)| (k, UInt::from(counter)))
            .collect::<BTreeMap<DeviceKeyAlgorithm, UInt>>();

        let response = upload_keys::v3::Response::new(counts);

        if input.device_keys.is_some() {
            self.stream.send_device_key_update(id.user()).await?;
        }

        Ok(response)
    }
}
