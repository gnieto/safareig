use safareig_core::{
    auth::Identity, command::Command, ruma::api::client::keys::upload_signatures, Inject,
};

use crate::{keys::SignatureUploader, stream::E2eeStream};

#[derive(Inject)]
pub struct UploadSignaturesCommand {
    signatures: SignatureUploader,
    stream: E2eeStream,
}

#[async_trait::async_trait]
impl Command for UploadSignaturesCommand {
    type Input = upload_signatures::v3::Request;
    type Output = upload_signatures::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let users = input.signed_keys.keys().cloned().collect();
        let failures = self
            .signatures
            .upload_signatures(id.user(), input.signed_keys)
            .await?;

        self.stream.send_signatures_update(id.user(), users).await?;

        Ok(upload_signatures::v3::Response {
            failures: failures.into(),
        })
    }
}
