use std::sync::Arc;

use safareig_core::{
    auth::Identity,
    ruma::api::client::{keys::upload_signing_keys, uiaa::UiaaResponse},
    Inject,
};
use safareig_uiaa::{SessionState, UiaaCommand};

use crate::{storage::KeyStorage, stream::E2eeStream};

#[derive(Inject)]
pub struct UploadUserKeysCommand {
    keys: Arc<dyn KeyStorage>,
    stream: E2eeStream,
}

#[async_trait::async_trait]
impl UiaaCommand for UploadUserKeysCommand {
    type Input = upload_signing_keys::v3::Request;
    type Output = upload_signing_keys::v3::Response;
    type Error = safareig_core::ruma::api::client::uiaa::UiaaResponse;

    async fn execute(
        &self,
        input: Self::Input,
        id: Identity,
        _: Option<SessionState>,
    ) -> Result<Self::Output, Self::Error> {
        let mut keys = self
            .keys
            .user_cross_signing_keys(id.user())
            .await
            .map_err(|e| UiaaResponse::MatrixError(e.into()))?;
        let mut key_map = keys.key_map();
        let mut user_signing_update = false;
        let mut cross_signing_update = false;

        if let Some(master) = input.master_key {
            key_map = keys
                .update_master(master)
                .map_err(|e| UiaaResponse::MatrixError(e.into()))?;
            cross_signing_update = true;
        }

        if let Some(user_signing) = input.user_signing_key {
            keys.update_user_signing(user_signing, &key_map)
                .map_err(|e| UiaaResponse::MatrixError(e.into()))?;
            user_signing_update = true;
        }

        if let Some(self_signing) = input.self_signing_key {
            keys.update_self_signing(self_signing, &key_map)
                .map_err(|e| UiaaResponse::MatrixError(e.into()))?;
            cross_signing_update = true;
        }

        self.keys
            .update_cross_signing_keys(&keys)
            .await
            .map_err(|e| UiaaResponse::MatrixError(e.into()))?;

        if cross_signing_update {
            self.stream
                .send_cross_signing_key_update(id.user())
                .await
                .map_err(|e| UiaaResponse::MatrixError(e.into()))?;
        } else if user_signing_update {
            self.stream
                .send_user_signing(id.user())
                .await
                .map_err(|e| UiaaResponse::MatrixError(e.into()))?;
        }

        Ok(upload_signing_keys::v3::Response {})
    }
}
