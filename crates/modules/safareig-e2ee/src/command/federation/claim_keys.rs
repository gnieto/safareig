use std::sync::Arc;

use async_trait::async_trait;
use http::StatusCode;
use safareig_core::{
    auth::Identity,
    command::Command,
    error::RumaExt,
    ruma::api::{
        client::error::{ErrorBody, ErrorKind},
        federation::keys::claim_keys::v1 as claim_keys,
    },
    Inject, ServerInfo,
};

use crate::keys::{group_map_by_server_name, E2EEKeyHandler};

#[derive(Inject)]
pub struct ClaimKeysCommand {
    server_info: ServerInfo,
    local_key_handler: Arc<dyn E2EEKeyHandler>,
}

#[async_trait]
impl Command for ClaimKeysCommand {
    type Input = claim_keys::Request;
    type Output = claim_keys::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;

    async fn execute(&self, input: Self::Input, _: Identity) -> Result<Self::Output, Self::Error> {
        // TODO: Rate-limit the amount of claims per unit of time
        let mut keys_by_server = group_map_by_server_name(input.one_time_keys);
        let keys = keys_by_server
            .remove(self.server_info.server_name())
            .unwrap_or_default();

        if !keys_by_server.is_empty() {
            return Err(safareig_core::ruma::api::client::Error {
                body: ErrorBody::Standard {
                    kind: ErrorKind::Unauthorized,
                    message:
                        "claim through federation should contain only devices from local server"
                            .to_string(),
                },
                status_code: StatusCode::UNAUTHORIZED,
            }
            .to_generic());
        }

        let claimed_keys = self
            .local_key_handler
            .claim_keys(self.server_info.server_name(), &keys)
            .await?;

        Ok(claim_keys::Response {
            one_time_keys: claimed_keys,
        })
    }
}

#[cfg(test)]
mod tests {
    use std::{collections::BTreeMap, sync::Arc};

    use safareig_core::{
        auth::Identity,
        command::Command,
        ruma::{
            api::federation::keys::claim_keys::{v1 as claim_keys, v1::OneTimeKeyClaims},
            encryption::OneTimeKey,
            serde::Raw,
            OwnedDeviceId, OwnedUserId, ServerName, UserId,
        },
        ServerInfo,
    };
    use safareig_testing::TestingApp;

    use super::ClaimKeysCommand;
    use crate::{
        keys::{E2EEKeyHandler, UserKeys, UserOneTimeKey},
        E2eeError,
    };

    #[tokio::test]
    async fn it_errors_on_non_local_users_present() {
        let cmd = ClaimKeysCommand {
            server_info: ServerInfo::from(&TestingApp::default_config()),
            local_key_handler: Arc::new(DummyClaimer {}),
        };

        let mut users = BTreeMap::new();
        users.insert(
            TestingApp::new_identity().user().to_owned(),
            Default::default(),
        );
        users.insert(
            safareig_core::ruma::user_id!("@u:localhost:999").to_owned(),
            Default::default(),
        );

        let request = claim_keys::Request {
            one_time_keys: users,
        };

        let result = cmd.execute(request, Identity::None).await;

        assert!(result.is_err());
    }

    #[tokio::test]
    async fn it_claims_keys_from_remote() {
        let cmd = ClaimKeysCommand {
            server_info: ServerInfo::from(&TestingApp::default_config()),
            local_key_handler: Arc::new(DummyClaimer {}),
        };

        let mut users = BTreeMap::new();
        users.insert(
            TestingApp::new_identity().user().to_owned(),
            Default::default(),
        );

        let request = claim_keys::Request {
            one_time_keys: users,
        };

        let result = cmd.execute(request, Identity::None).await.unwrap();

        assert_eq!(1, result.one_time_keys.len());
    }

    struct DummyClaimer;

    #[async_trait::async_trait]
    impl E2EEKeyHandler for DummyClaimer {
        async fn claim_keys(
            &self,
            _server_name: &ServerName,
            key_claims: &OneTimeKeyClaims,
        ) -> Result<UserOneTimeKey, E2eeError> {
            let mut user_one_time_keys = UserOneTimeKey::new();

            for (user, devices) in key_claims {
                let one_time_keys = user_one_time_keys.entry(user.to_owned()).or_default();

                for device_id in devices.keys() {
                    let claimed_key = Some((
                        safareig_core::ruma::device_key_id!("a:b").to_owned(),
                        OneTimeKey::Key("test".to_string()),
                    ));

                    if let Some((key_id, otk)) = claimed_key {
                        let device_entry = one_time_keys.entry(device_id.to_owned()).or_default();

                        let raw_otk = Raw::new(&otk).map_err(E2eeError::from)?;
                        device_entry.insert(key_id, raw_otk);
                    }
                }
            }

            Ok(user_one_time_keys)
        }

        async fn query_keys(
            &self,
            _: &ServerName,
            _: Option<&UserId>,
            _: &BTreeMap<OwnedUserId, Vec<OwnedDeviceId>>,
        ) -> Result<BTreeMap<OwnedUserId, UserKeys>, E2eeError> {
            unimplemented!("")
        }
    }
}
