use std::{collections::HashMap, sync::Arc};

use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    command::Command,
    ruma::{
        api::federation::device::get_devices::v1 as get_devices, encryption::DeviceKeys,
        serde::Raw, OwnedDeviceId,
    },
    Inject,
};
use safareig_devices::storage::{DeviceListStorage, DeviceStorage};

use crate::storage::KeyStorage;

#[derive(Inject)]
pub struct UserDeviceCommand {
    devices: Arc<dyn DeviceStorage>,
    user_keys: Arc<dyn KeyStorage>,
    user_device_list: Arc<dyn DeviceListStorage>,
}

#[async_trait]
impl Command for UserDeviceCommand {
    type Input = get_devices::Request;
    type Output = get_devices::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;

    async fn execute(&self, input: Self::Input, _: Identity) -> Result<Self::Output, Self::Error> {
        let user_id = &input.user_id;
        let keys = self.user_keys.get_keys(user_id, None).await?;

        let keys_by_device: HashMap<&OwnedDeviceId, &DeviceKeys> = keys
            .iter()
            .map(|device_keys| (&device_keys.device_id, device_keys))
            .collect();

        let devices: Vec<get_devices::UserDevice> = self
            .devices
            .devices_by_user(user_id)
            .await?
            .into_iter()
            .filter_map(|device| {
                let device_keys = keys_by_device.get(&device.ruma_device.device_id);

                device_keys.map(|keys| {
                    let _ = &device;
                    let raw_keys = Raw::new(*keys).unwrap();
                    get_devices::UserDevice {
                        device_id: device.ruma_device.device_id,
                        keys: raw_keys,
                        device_display_name: device.ruma_device.display_name,
                    }
                })
            })
            .collect();

        let cross_keys = self
            .user_keys
            .user_cross_signing_keys(user_id.as_ref())
            .await?;

        // TODO: 2PC - Consistency
        let current_state = self.user_device_list.device_list_state(user_id).await?;

        Ok(get_devices::Response {
            user_id: user_id.to_owned(),
            stream_id: current_state.stream_id.into(),
            devices,
            master_key: cross_keys.master().cloned(),
            self_signing_key: cross_keys.self_signing().cloned(),
        })
    }
}
