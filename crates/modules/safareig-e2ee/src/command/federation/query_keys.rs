use std::sync::Arc;

use async_trait::async_trait;
use http::StatusCode;
use safareig_core::{
    auth::Identity,
    command::Command,
    error::RumaExt,
    ruma::api::{
        client::error::{ErrorBody, ErrorKind},
        federation::keys::get_keys::v1 as query_keys,
    },
    Inject, ServerInfo,
};

use crate::keys::{group_map_by_server_name, E2EEKeyHandler};

#[derive(Inject)]
pub struct QueryKeysCommand {
    server_info: ServerInfo,
    local_key_handler: Arc<dyn E2EEKeyHandler>,
}

#[async_trait]
impl Command for QueryKeysCommand {
    type Input = query_keys::Request;
    type Output = query_keys::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;

    async fn execute(&self, input: Self::Input, _: Identity) -> Result<Self::Output, Self::Error> {
        let mut device_keys = group_map_by_server_name(input.device_keys);
        let keys = device_keys
            .remove(self.server_info.server_name())
            .unwrap_or_default();

        if !device_keys.is_empty() {
            return Err(safareig_core::ruma::api::client::Error {
                body: ErrorBody::Standard {
                    kind: ErrorKind::Unauthorized,
                    message:
                        "query through federation should contain only devices from local server"
                            .to_string(),
                },
                status_code: StatusCode::UNAUTHORIZED,
            }
            .to_generic());
        }

        let users_keys = self
            .local_key_handler
            .query_keys(self.server_info.server_name(), None, &keys)
            .await?;

        let mut response = query_keys::Response {
            device_keys: Default::default(),
            self_signing_keys: Default::default(),
            master_keys: Default::default(),
        };

        for (user_id, user_keys) in users_keys {
            response
                .device_keys
                .insert(user_id.to_owned(), user_keys.raw_device_keys());

            if let Some(master) = user_keys.cross_signing.master() {
                response
                    .master_keys
                    .insert(user_id.to_owned(), master.clone());
            }

            if let Some(self_signing) = user_keys.cross_signing.self_signing() {
                response
                    .self_signing_keys
                    .insert(user_id.to_owned(), self_signing.clone());
            }
        }

        Ok(response)
    }
}
