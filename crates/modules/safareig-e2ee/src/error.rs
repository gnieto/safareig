use http::StatusCode;
use safareig_core::{
    error::RumaExt,
    ruma::api::client::error::{ErrorBody, ErrorKind},
    storage::StorageError,
};
use safareig_federation::FederationError;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum E2eeError {
    #[error("Serde error: {0}")]
    Serde(#[from] serde_json::Error),
    #[error("Storage error: {0}")]
    Storage(#[from] StorageError),
    #[error("Federation error: {0}")]
    Federation(#[from] FederationError),
    #[error("Cross key contains multiple public signatures")]
    CrossKeyMultipleKeys,
    #[error("Master key reused for another user key")]
    ConflictingMasterKey,
    #[error("Key signature is not properly signed")]
    KeySignatureValidation,
    #[error("Missing target key")]
    MissingTargetKey,
}

impl From<E2eeError> for safareig_core::ruma::api::client::Error {
    fn from(e: E2eeError) -> Self {
        match e {
            E2eeError::Serde(e) => safareig_core::ruma::api::client::Error {
                status_code: StatusCode::BAD_REQUEST,
                body: ErrorBody::Standard {
                    kind: ErrorKind::BadJson,
                    message: e.to_string(),
                },
            },
            E2eeError::Federation(e) => e.into(),
            E2eeError::Storage(e) => e.into(),
            E2eeError::CrossKeyMultipleKeys => safareig_core::ruma::api::client::Error {
                status_code: StatusCode::BAD_REQUEST,
                body: ErrorBody::Standard {
                    kind: ErrorKind::Unknown,
                    message: e.to_string(),
                },
            },
            E2eeError::ConflictingMasterKey => safareig_core::ruma::api::client::Error {
                status_code: StatusCode::FORBIDDEN,
                body: ErrorBody::Standard {
                    kind: ErrorKind::Unknown,
                    message: e.to_string(),
                },
            },
            E2eeError::KeySignatureValidation | E2eeError::MissingTargetKey => {
                safareig_core::ruma::api::client::Error {
                    // TODO: Add Invalid signature to ruma
                    status_code: StatusCode::BAD_REQUEST,
                    body: ErrorBody::Standard {
                        kind: ErrorKind::InvalidParam,
                        message: e.to_string(),
                    },
                }
            }
        }
    }
}

impl From<E2eeError> for safareig_core::ruma::api::error::MatrixError {
    fn from(error: E2eeError) -> Self {
        let client = safareig_core::ruma::api::client::Error::from(error);
        client.to_generic()
    }
}
