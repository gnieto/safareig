use std::collections::BTreeMap;

use async_trait::async_trait;
use safareig_core::{
    pagination::{Pagination, PaginationResponse},
    ruma::{
        encryption::{DeviceKeys, OneTimeKey},
        serde::Raw,
        DeviceId, DeviceKeyAlgorithm, DeviceKeyId, OwnedDeviceId, OwnedDeviceKeyId, OwnedUserId,
        UserId,
    },
    storage::StorageError,
    StreamToken,
};
use serde::{Deserialize, Serialize};

use crate::keys::CrossSigningKeys;

#[cfg(feature = "backend-sqlx")]
pub mod sqlx;

#[async_trait]
pub trait KeyStorage: Send + Sync {
    async fn get_keys(
        &self,
        user: &UserId,
        devices: Option<&[OwnedDeviceId]>,
    ) -> Result<Vec<DeviceKeys>, StorageError>;
    async fn update_keys(&self, device_keys: &DeviceKeys) -> Result<(), StorageError>;
    async fn add_one_time_key(
        &self,
        device_id: &DeviceId,
        key_id: &DeviceKeyId,
        otk: &OneTimeKey,
    ) -> Result<(), StorageError>;
    async fn remove_one_time_key(
        &self,
        device_id: &DeviceId,
        algorithm: &DeviceKeyAlgorithm,
    ) -> Result<Option<(OwnedDeviceKeyId, OneTimeKey)>, StorageError>;
    async fn pending_keys(
        &self,
        device_id: &DeviceId,
    ) -> Result<BTreeMap<DeviceKeyAlgorithm, u32>, StorageError>;

    async fn user_cross_signing_keys(
        &self,
        user: &UserId,
    ) -> Result<CrossSigningKeys, StorageError>;
    async fn update_cross_signing_keys(&self, keys: &CrossSigningKeys) -> Result<(), StorageError>;

    async fn add_fallback_key(
        &self,
        device_id: &DeviceId,
        key_id: &DeviceKeyId,
        otk: &Raw<OneTimeKey>,
    ) -> Result<(), StorageError>;

    async fn claim_fallback_key(
        &self,
        device_id: &DeviceId,
        algorithm: &DeviceKeyAlgorithm,
    ) -> Result<Option<(OwnedDeviceKeyId, Raw<OneTimeKey>)>, StorageError>;

    async fn unclaimed_fallback_algorithms(
        &self,
        device_id: &DeviceId,
    ) -> Result<Vec<DeviceKeyAlgorithm>, StorageError>;
}

#[derive(Serialize, Deserialize, Debug)]
pub enum E2eeUpdateType {
    DeviceList,
    CrossSigningKeys,
    UserSigningKey,
    DeviceKey,
    Signatures(Vec<OwnedUserId>),
}

#[derive(Serialize, Deserialize, Debug)]
pub struct E2eeUpdate {
    pub user: OwnedUserId,
    pub kind: E2eeUpdateType,
}

#[async_trait::async_trait]
pub trait E2eeStreamStorage: Send + Sync {
    async fn push_e2ee_update(&self, update: &E2eeUpdate) -> Result<StreamToken, StorageError>;
    async fn e2ee_updates(
        &self,
        query: Pagination<StreamToken, ()>,
    ) -> Result<PaginationResponse<StreamToken, E2eeUpdate>, StorageError>;
}

#[derive(Debug, Clone)]
pub struct Signature {
    pub signing_user: OwnedUserId,
    pub target_user: OwnedUserId,
    pub key_id: OwnedDeviceKeyId,
    pub signature: String,
    pub key_name: String,
}

#[async_trait::async_trait]
pub trait SignatureStorage: Send + Sync {
    async fn insert_signatures(&self, signatures: &[Signature]) -> Result<(), StorageError>;
    async fn load_signatures(
        &self,
        user: &UserId,
        requester: Option<&UserId>,
    ) -> Result<Vec<Signature>, StorageError>;
}
