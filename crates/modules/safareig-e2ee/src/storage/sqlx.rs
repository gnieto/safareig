use std::{
    collections::{BTreeMap, HashMap},
    ops::DerefMut,
};

use safareig_core::{
    pagination::{Pagination, PaginationResponse},
    ruma::{
        encryption::{CrossSigningKey, DeviceKeys, OneTimeKey},
        serde::Raw,
        DeviceId, DeviceKeyAlgorithm, DeviceKeyId, OwnedDeviceId, OwnedDeviceKeyId, UserId,
    },
    storage::StorageError,
    StreamToken,
};
use safareig_sqlx::{
    decode_from_deserialize, decode_from_str, decode_i64_to_u64, DirectionExt, PaginatedIterator,
    RangeWrapper,
};
use sea_query::{Alias, Expr, Iden, Query, SqliteQueryBuilder};
use sea_query_binder::SqlxBinder;
use sqlx::{database::HasValueRef, error::BoxDynError, QueryBuilder, Row, Sqlite, Transaction};

use super::{E2eeStreamStorage, E2eeUpdate, KeyStorage, Signature, SignatureStorage};
use crate::keys::CrossSigningKeys;

pub struct SqliteE2EEStorage {
    pool: sqlx::Pool<sqlx::Sqlite>,
}

impl SqliteE2EEStorage {
    pub fn new(pool: sqlx::Pool<sqlx::Sqlite>) -> Self {
        Self { pool }
    }
}

#[async_trait::async_trait]
impl KeyStorage for SqliteE2EEStorage {
    async fn get_keys(
        &self,
        user: &UserId,
        devices: Option<&[OwnedDeviceId]>,
    ) -> Result<Vec<DeviceKeys>, StorageError> {
        match devices {
            Some(devices) => {
                let mut out_devices = Vec::with_capacity(devices.len());

                // TODO: These queries could be converted into a single query
                for device in devices {
                    let maybe_device = sqlx::query(
                        r#"
                        SELECT key_blob FROM device_keys
                        WHERE user_id = ?1 AND device_id = ?2
                        "#,
                    )
                    .bind(user.as_str())
                    .bind(device.as_str())
                    .fetch_optional(&self.pool)
                    .await?
                    .map(|row| {
                        let blob = row.try_get::<serde_json::Value, _>("key_blob")?;
                        let device_key: DeviceKeys = serde_json::from_value(blob)?;

                        Ok(device_key) as Result<_, StorageError>
                    })
                    .transpose()?;

                    if let Some(device) = maybe_device {
                        out_devices.push(device);
                    }
                }

                Ok(out_devices)
            }
            None => {
                let devices = sqlx::query(
                    r#"
                    SELECT key_blob FROM device_keys
                    WHERE user_id = ?1
                    "#,
                )
                .bind(user.as_str())
                .fetch_all(&self.pool)
                .await?
                .into_iter()
                .map(|row| {
                    let blob = row.try_get::<serde_json::Value, _>("key_blob")?;
                    let device_key: DeviceKeys = serde_json::from_value(blob)?;

                    Ok(device_key) as Result<_, StorageError>
                })
                .collect::<Result<_, StorageError>>()?;

                Ok(devices)
            }
        }
    }

    async fn update_keys(&self, device_keys: &DeviceKeys) -> Result<(), StorageError> {
        let key_blob = serde_json::to_value(device_keys)?;

        sqlx::query(
            r#"
            INSERT INTO device_keys (user_id, device_id, key_blob)
            VALUES (?1, ?2, ?3)
            ON CONFLICT(user_id, device_id)
            DO UPDATE SET key_blob = ?3
            "#,
        )
        .bind(device_keys.user_id.as_str())
        .bind(device_keys.device_id.as_str())
        .bind(&key_blob)
        .execute(&self.pool)
        .await?;

        Ok(())
    }

    async fn add_one_time_key(
        &self,
        device_id: &DeviceId,
        key_id: &DeviceKeyId,
        otk: &OneTimeKey,
    ) -> Result<(), StorageError> {
        let otk = serde_json::to_value(otk)?;

        sqlx::query(
            r#"
            INSERT INTO device_otk (device_id, algorithm, key_name, fallback, otk_blob)
            VALUES (?1, ?2, ?3, ?4, ?5)
            "#,
        )
        .bind(device_id.as_str())
        .bind(key_id.algorithm().as_str())
        .bind(key_id.device_id().as_str())
        .bind(0)
        .bind(&otk)
        .execute(&self.pool)
        .await?;

        Ok(())
    }

    async fn remove_one_time_key(
        &self,
        device_id: &DeviceId,
        algorithm: &DeviceKeyAlgorithm,
    ) -> Result<Option<(OwnedDeviceKeyId, OneTimeKey)>, StorageError> {
        let otk = sqlx::query(
            r#"
            DELETE FROM device_otk 
            WHERE device_id = ?1 AND algorithm = ?2 AND fallback = 0
            RETURNING * 
            "#,
        )
        .bind(device_id.as_str())
        .bind(algorithm.as_ref())
        .fetch_optional(&self.pool)
        .await?
        .map(|row| {
            let algorithm: DeviceKeyAlgorithm = row.get::<&str, _>("algorithm").into();
            let key_name: OwnedDeviceId = row.get::<&str, _>("key_name").into();

            let otk: serde_json::Value = row.try_get::<serde_json::Value, _>("otk_blob")?;
            let otk: OneTimeKey = serde_json::from_value(otk)?;
            let device_key_id = DeviceKeyId::from_parts(algorithm, &key_name);

            Ok((device_key_id, otk)) as Result<_, StorageError>
        })
        .transpose()?;

        Ok(otk)
    }

    async fn pending_keys(
        &self,
        device_id: &DeviceId,
    ) -> Result<BTreeMap<DeviceKeyAlgorithm, u32>, StorageError> {
        let counts = sqlx::query(
            r#"
            SELECT algorithm, COUNT(*) as amount FROM device_otk 
            WHERE device_id = ?1
            GROUP BY algorithm
            "#,
        )
        .bind(device_id.as_str())
        .fetch_all(&self.pool)
        .await?
        .into_iter()
        .map(|row| {
            let algorithm: DeviceKeyAlgorithm = row.get::<&str, _>("algorithm").into();
            let count = row.try_get::<u32, _>("amount")?;

            Ok((algorithm, count))
        })
        .collect::<Result<_, StorageError>>()?;

        Ok(counts)
    }

    async fn user_cross_signing_keys(
        &self,
        user: &UserId,
    ) -> Result<CrossSigningKeys, StorageError> {
        let mut keys: HashMap<UserKeyId, Raw<CrossSigningKey>> = sqlx::query(
            r#"
            SELECT key_type, key_blob FROM user_keys
            WHERE user_id = ?1
            "#,
        )
        .bind(user.as_str())
        .fetch_all(&self.pool)
        .await?
        .into_iter()
        .map(|row| {
            let blob = row.try_get::<serde_json::Value, _>("key_blob")?;
            let key_type = row.try_get::<i64, _>("key_type")? as u32;
            let key_type = UserKeyId::try_from(key_type).unwrap();

            let user_key: Raw<CrossSigningKey> = Raw::new(&blob)?.cast();

            Ok((key_type, user_key)) as Result<_, StorageError>
        })
        .collect::<Result<_, StorageError>>()?;

        let master = keys.remove(&UserKeyId::MasterKey);
        let user_signing = keys.remove(&UserKeyId::UserSigning);
        let self_signing = keys.remove(&UserKeyId::SelfSigning);

        Ok(CrossSigningKeys::unverified(
            user.to_owned(),
            master,
            user_signing,
            self_signing,
        ))
    }

    async fn update_cross_signing_keys(&self, keys: &CrossSigningKeys) -> Result<(), StorageError> {
        let mut tx = self.pool.begin().await?;

        self.update_key(keys.user_id(), keys.master(), UserKeyId::MasterKey, &mut tx)
            .await?;
        self.update_key(
            keys.user_id(),
            keys.user_signing(),
            UserKeyId::UserSigning,
            &mut tx,
        )
        .await?;
        self.update_key(
            keys.user_id(),
            keys.self_signing(),
            UserKeyId::SelfSigning,
            &mut tx,
        )
        .await?;

        tx.commit().await?;

        Ok(())
    }

    async fn add_fallback_key(
        &self,
        device_id: &DeviceId,
        key_id: &DeviceKeyId,
        otk: &Raw<OneTimeKey>,
    ) -> Result<(), StorageError> {
        let otk = serde_json::to_value(otk)?;

        sqlx::query(
            r#"
            INSERT INTO fallback_keys (device_id, algorithm, key_name, otk_blob)
            VALUES (?1, ?2, ?3, ?4)
            ON CONFLICT DO UPDATE SET key_name = ?3, otk_blob = ?4
            "#,
        )
        .bind(device_id.as_str())
        .bind(key_id.algorithm().as_str())
        .bind(key_id.device_id().as_str())
        .bind(&otk)
        .execute(&self.pool)
        .await?;

        Ok(())
    }

    async fn claim_fallback_key(
        &self,
        device_id: &DeviceId,
        algorithm: &DeviceKeyAlgorithm,
    ) -> Result<Option<(OwnedDeviceKeyId, Raw<OneTimeKey>)>, StorageError> {
        let otk = sqlx::query(
            r#"
            UPDATE fallback_keys 
            SET used = 1
            WHERE device_id = ?1 AND algorithm = ?2
            RETURNING *
            "#,
        )
        .bind(device_id.as_str())
        .bind(algorithm.as_ref())
        .fetch_optional(&self.pool)
        .await?
        .map(|row| {
            let algorithm: DeviceKeyAlgorithm = row.get::<&str, _>("algorithm").into();
            let key_name: OwnedDeviceId = row.get::<&str, _>("key_name").into();

            let otk: serde_json::Value = row.try_get::<serde_json::Value, _>("otk_blob")?;
            let otk = Raw::new(&otk)?;
            let device_key_id = DeviceKeyId::from_parts(algorithm, &key_name);

            Ok((device_key_id, otk.cast())) as Result<_, StorageError>
        })
        .transpose()?;

        Ok(otk)
    }

    async fn unclaimed_fallback_algorithms(
        &self,
        device_id: &DeviceId,
    ) -> Result<Vec<DeviceKeyAlgorithm>, StorageError> {
        let counts = sqlx::query(
            r#"
            SELECT algorithm FROM fallback_keys 
            WHERE device_id = ?1 AND used = 0
            "#,
        )
        .bind(device_id.as_str())
        .fetch_all(&self.pool)
        .await?
        .into_iter()
        .map(|row| {
            let algorithm: DeviceKeyAlgorithm = row.get::<&str, _>("algorithm").into();

            Ok(algorithm)
        })
        .collect::<Result<_, StorageError>>()?;

        Ok(counts)
    }
}

#[async_trait::async_trait]
impl SignatureStorage for SqliteE2EEStorage {
    async fn insert_signatures(&self, signatures: &[Signature]) -> Result<(), StorageError> {
        if signatures.is_empty() {
            return Ok(());
        }

        let mut builder = QueryBuilder::new(
            "INSERT INTO key_signatures (signing_user, target_user, signature, key_id, key_name) ",
        );

        builder.push_values(signatures.iter(), |mut b, signature: &Signature| {
            b.push_bind(signature.signing_user.as_str())
                .push_bind(signature.target_user.as_str())
                .push_bind(signature.signature.as_str())
                .push_bind(signature.key_id.as_str())
                .push_bind(signature.key_name.as_str());
        });

        let query = builder.build();

        query.execute(&self.pool).await?;

        Ok(())
    }

    async fn load_signatures(
        &self,
        user: &UserId,
        requester: Option<&UserId>,
    ) -> Result<Vec<Signature>, StorageError> {
        let query = match requester {
            Some(requester) => {
                let sql = r#"
                SELECT * FROM key_signatures
                WHERE target_user = ?1 OR signing_user IN (?1, ?2)
                "#;

                sqlx::query(sql)
                    .bind(requester.as_str())
                    .bind(user.as_str())
            }
            None => {
                let sql = r#"
                SELECT * FROM key_signatures
                WHERE signing_user = ?1
                "#;

                sqlx::query(sql).bind(user.as_str())
            }
        };

        let signatures = query
            .fetch_all(&self.pool)
            .await?
            .into_iter()
            .map(|row| {
                let signing = decode_from_str(row.try_get::<&str, _>("signing_user")?)?;
                let target = decode_from_str(row.try_get::<&str, _>("target_user")?)?;
                let key_id = decode_from_str(row.try_get::<&str, _>("key_id")?)?;
                let signature = row.try_get::<String, _>("signature")?;
                let key_name = row.try_get::<String, _>("key_name")?;

                let signature = Signature {
                    signing_user: signing,
                    target_user: target,
                    key_id,
                    signature,
                    key_name,
                };

                Ok(signature) as Result<_, StorageError>
            })
            .collect::<Result<_, StorageError>>()?;

        Ok(signatures)
    }
}

impl SqliteE2EEStorage {
    async fn update_key<'c>(
        &self,
        user_id: &UserId,
        user_key: Option<&Raw<CrossSigningKey>>,
        key_id: UserKeyId,
        tx: &mut Transaction<'c, Sqlite>,
    ) -> Result<(), StorageError> {
        match user_key {
            Some(key) => {
                let json = serde_json::to_value(key)?;
                sqlx::query(
                    r#"
                    INSERT INTO user_keys (user_id, key_type, key_blob)
                    VALUES (?1, ?2, ?3)
                    ON CONFLICT DO UPDATE SET key_type = ?2, key_blob = ?3
                    "#,
                )
                .bind(user_id.as_str())
                .bind(key_id)
                .bind(json)
                .execute(tx.deref_mut())
                .await?;
            }
            None => {
                sqlx::query(
                    r#"
                    DELETE FROM user_keys
                    WHERE user_id = ?1 AND key_type = ?2
                    "#,
                )
                .bind(user_id.as_str())
                .bind(key_id)
                .execute(tx.deref_mut())
                .await?;
            }
        };

        Ok(())
    }
}

#[async_trait::async_trait]
impl E2eeStreamStorage for SqliteE2EEStorage {
    async fn push_e2ee_update(&self, update: &E2eeUpdate) -> Result<StreamToken, StorageError> {
        let serialized_kind = serde_json::to_string(&update.kind)?;
        let stream_token = sqlx::query(
            r#"
            INSERT INTO e2ee_update_stream (user_id, event_type, insertion_time)
            VALUES (?1, ?2, CURRENT_TIME)
            RETURNING stream_token;
            "#,
        )
        .bind(update.user.as_str())
        .bind(serialized_kind)
        .fetch_optional(&self.pool)
        .await?
        .map(|row| decode_i64_to_u64(row.get::<i64, _>("stream_token")))
        .transpose()?
        .map(StreamToken::from)
        .ok_or_else(|| StorageError::Custom("could not insert event to stream".to_string()))?;

        Ok(stream_token)
    }

    async fn e2ee_updates(
        &self,
        query: Pagination<safareig_core::StreamToken, ()>,
    ) -> Result<PaginationResponse<StreamToken, E2eeUpdate>, StorageError> {
        let wrapper = RangeWrapper(query.range().map(|t| t.to_string()));
        let (query, values) = Query::select()
            .column(E2eeUpdateStream::StreamToken)
            .column(E2eeUpdateStream::UserId)
            .column(E2eeUpdateStream::EventType)
            .from(E2eeUpdateStream::Table)
            .cond_where(wrapper.condition(Expr::col(E2eeUpdateStream::StreamToken)))
            .order_by(Alias::new("stream_token"), query.direction().order())
            .limit(query.limit().unwrap_or(100) as u64)
            .build_sqlx(SqliteQueryBuilder);

        let events = sqlx::query_with(&query, values)
            .fetch_all(&self.pool)
            .await?;

        let iterator = PaginatedIterator::new(events.into_iter(), |row| {
            let token = decode_i64_to_u64(row.try_get::<i64, _>("stream_token")?)?;
            let user_id = decode_from_str(row.get::<&str, _>("user_id"))?;
            let kind = decode_from_deserialize(row.get::<&str, _>("event_type"))?;
            let stream_token = StreamToken::from(token);
            let update = E2eeUpdate {
                user: user_id,
                kind,
            };

            Ok((stream_token, update))
        });

        iterator.into_pagination_response()
    }
}

#[derive(Iden)]
enum E2eeUpdateStream {
    Table,
    StreamToken,
    UserId,
    EventType,
}

#[derive(Hash, Eq, PartialEq)]
enum UserKeyId {
    MasterKey,
    UserSigning,
    SelfSigning,
}

impl From<&UserKeyId> for u32 {
    fn from(key_id: &UserKeyId) -> Self {
        match key_id {
            UserKeyId::MasterKey => 1,
            UserKeyId::UserSigning => 2,
            UserKeyId::SelfSigning => 3,
        }
    }
}

impl TryFrom<u32> for UserKeyId {
    type Error = ();

    fn try_from(value: u32) -> Result<Self, Self::Error> {
        let id = match value {
            1 => UserKeyId::MasterKey,
            2 => UserKeyId::UserSigning,
            3 => UserKeyId::SelfSigning,
            _ => return Err(()),
        };

        Ok(id)
    }
}

impl sqlx::Type<sqlx::Sqlite> for UserKeyId {
    fn type_info() -> <sqlx::Sqlite as sqlx::Database>::TypeInfo {
        <i64 as sqlx::Type<sqlx::Sqlite>>::type_info()
    }
}

impl<'q> sqlx::Encode<'q, sqlx::Sqlite> for UserKeyId {
    fn encode_by_ref(
        &self,
        args: &mut <sqlx::Sqlite as sqlx::database::HasArguments<'q>>::ArgumentBuffer,
    ) -> sqlx::encode::IsNull {
        let key_id = u32::from(self);
        args.push(sqlx::sqlite::SqliteArgumentValue::Int64(key_id as i64));

        sqlx::encode::IsNull::No
    }
}

impl<'q> sqlx::Decode<'q, sqlx::Sqlite> for UserKeyId {
    fn decode(value: <sqlx::Sqlite as HasValueRef<'q>>::ValueRef) -> Result<Self, BoxDynError> {
        let value = <i64 as sqlx::Decode<sqlx::Sqlite>>::decode(value)?;

        Ok(UserKeyId::try_from(value as u32).unwrap())
    }
}
