use std::collections::BTreeMap;

use rusty_ulid::Ulid;
use safareig_core::{
    ruma::{
        api::client::backup::{BackupAlgorithm, KeyBackupData},
        serde::Raw,
        OwnedRoomId, RoomId, UserId,
    },
    storage::StorageError,
};
use serde::{Deserialize, Serialize};

use super::error::E2eeBackupError;

#[cfg(feature = "backend-sqlx")]
pub(crate) mod sqlx;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Backup {
    algorithm: Raw<BackupAlgorithm>,
    version: String,
    etag: String,
}

impl Backup {
    pub fn new(algorithm: Raw<BackupAlgorithm>) -> Self {
        Self {
            algorithm,
            version: Ulid::generate().to_string(),
            etag: Ulid::generate().to_string(),
        }
    }

    pub fn version(&self) -> &str {
        self.version.as_str()
    }

    pub fn algorithm(&self) -> &Raw<BackupAlgorithm> {
        &self.algorithm
    }

    pub async fn count(
        &self,
        user_id: &UserId,
        storage: &dyn BackupStorage,
    ) -> Result<u32, StorageError> {
        storage.count_session_keys(user_id, &self.version).await
    }

    pub fn etag(&self) -> &str {
        self.etag.as_str()
    }

    pub fn update_algorithm(
        &mut self,
        algorithm: Raw<BackupAlgorithm>,
    ) -> Result<(), E2eeBackupError> {
        let old_algorithm = self.algorithm.get_field::<&str>("algorithm")?;
        let new_algorithm = algorithm.get_field::<&str>("algorithm")?;

        if old_algorithm != new_algorithm {
            return Err(E2eeBackupError::AlgorithmMismatch);
        }

        self.algorithm = algorithm;

        Ok(())
    }

    pub async fn update_etag(
        &mut self,
        user_id: &UserId,
        storage: &dyn BackupStorage,
    ) -> Result<(), StorageError> {
        self.etag = rusty_ulid::Ulid::generate().to_string();
        storage.update_backup(user_id, self).await?;

        Ok(())
    }

    pub async fn update_key(
        &mut self,
        user_id: &UserId,
        room_id: &RoomId,
        session: &str,
        new_key: &Raw<KeyBackupData>,
        storage: &dyn BackupStorage,
    ) -> Result<(), E2eeBackupError> {
        let key = storage
            .get_session_key(user_id, &self.version, room_id, session)
            .await?;

        match key {
            None => (),
            Some(old_key) => {
                let old_key = old_key.deserialize()?;
                let new_key = new_key.deserialize()?;

                if old_key.is_verified != new_key.is_verified && old_key.is_verified {
                    // Old key is verified, but new one isn't. Keep old key.
                    return Ok(());
                }

                if old_key.first_message_index < new_key.first_message_index {
                    // if they have the same values for is_verified, then it will keep the key with a lower first_message_index;
                    return Ok(());
                }

                if old_key.forwarded_count < new_key.forwarded_count {
                    // and finally, is is_verified and first_message_index are equal, then it will keep the key with a lower forwarded_count.
                    return Ok(());
                }
            }
        }

        storage
            .update_session_key(user_id, &self.version, room_id, session, new_key)
            .await?;

        Ok(())
    }
}

pub type SessionKeys = BTreeMap<OwnedRoomId, BTreeMap<String, Raw<KeyBackupData>>>;

#[async_trait::async_trait]
pub trait BackupStorage: Send + Sync {
    async fn load_backup_version(&self, user_id: &UserId) -> Result<Option<Backup>, StorageError>;
    async fn update_backup(&self, user_id: &UserId, backup: &Backup) -> Result<(), StorageError>;
    async fn delete_backup_version(
        &self,
        user_id: &UserId,
        version: &str,
    ) -> Result<(), StorageError>;

    async fn get_session_key(
        &self,
        user_id: &UserId,
        version: &str,
        room_id: &RoomId,
        session_id: &str,
    ) -> Result<Option<Raw<KeyBackupData>>, StorageError>;
    async fn get_session_keys(
        &self,
        user_id: &UserId,
        version: &str,
        room_id: Option<&RoomId>,
        session_id: Option<&str>,
    ) -> Result<SessionKeys, StorageError>;
    async fn update_session_key(
        &self,
        user_id: &UserId,
        version: &str,
        room_id: &RoomId,
        session_id: &str,
        key_backup: &Raw<KeyBackupData>,
    ) -> Result<(), StorageError>;
    async fn delete_session_key(
        &self,
        user_id: &UserId,
        version: &str,
        room_id: Option<&RoomId>,
        session_id: Option<&str>,
    ) -> Result<(), StorageError>;
    async fn count_session_keys(
        &self,
        user_id: &UserId,
        version: &str,
    ) -> Result<u32, StorageError>;
}
