use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity, command::Command, ruma::api::client::backup::create_backup_version, Inject,
};

use crate::backup::storage::{Backup, BackupStorage};

#[derive(Inject)]
pub struct CreateBackupVersionCommand {
    backups: Arc<dyn BackupStorage>,
}

#[async_trait]
impl Command for CreateBackupVersionCommand {
    type Input = create_backup_version::v3::Request;
    type Output = create_backup_version::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let backup = Backup::new(input.algorithm);
        self.backups.update_backup(id.user(), &backup).await?;

        Ok(create_backup_version::v3::Response {
            version: backup.version().to_string(),
        })
    }
}
