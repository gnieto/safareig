use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity, command::Command, error::ResourceNotFound,
    ruma::api::client::backup::update_backup_version, Inject,
};

use crate::backup::{error::E2eeBackupError, storage::BackupStorage};

#[derive(Inject)]
pub struct UpdateBackupVersionCommand {
    backups: Arc<dyn BackupStorage>,
}

#[async_trait]
impl Command for UpdateBackupVersionCommand {
    type Input = update_backup_version::v3::Request;
    type Output = update_backup_version::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let mut backup = self
            .backups
            .load_backup_version(id.user())
            .await?
            .ok_or(ResourceNotFound)?;

        if backup.version() != input.version {
            return Err(E2eeBackupError::BackupVersionMismatch(input.version).into());
        }

        backup.update_algorithm(input.algorithm)?;
        self.backups.update_backup(id.user(), &backup).await?;

        Ok(update_backup_version::v3::Response {})
    }
}
