use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    command::Command,
    error::ResourceNotFound,
    ruma::{
        api::client::backup::{
            delete_backup_keys, delete_backup_keys_for_room, delete_backup_keys_for_session,
        },
        UInt,
    },
    Inject,
};

use crate::backup::{error::E2eeBackupError, storage::BackupStorage};

#[derive(Inject)]
pub struct DeleteBackupKeysCommand {
    backups: Arc<dyn BackupStorage>,
}

#[async_trait]
impl Command for DeleteBackupKeysCommand {
    type Input = delete_backup_keys::v3::Request;
    type Output = delete_backup_keys::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let mut backup = self
            .backups
            .load_backup_version(id.user())
            .await?
            .ok_or(ResourceNotFound)?;

        if backup.version() != input.version {
            return Err(E2eeBackupError::BackupVersionMismatch(input.version).into());
        }

        self.backups
            .delete_session_key(id.user(), backup.version(), None, None)
            .await?;
        backup.update_etag(id.user(), self.backups.as_ref()).await?;

        Ok(delete_backup_keys::v3::Response {
            etag: backup.etag().to_string(),
            count: UInt::new_saturating(
                backup.count(id.user(), self.backups.as_ref()).await?.into(),
            ),
        })
    }
}

#[derive(Inject)]
pub struct DeleteBackupRoomKeysCommand {
    backups: Arc<dyn BackupStorage>,
}

#[async_trait]
impl Command for DeleteBackupRoomKeysCommand {
    type Input = delete_backup_keys_for_room::v3::Request;
    type Output = delete_backup_keys_for_room::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let mut backup = self
            .backups
            .load_backup_version(id.user())
            .await?
            .ok_or(ResourceNotFound)?;

        if backup.version() != input.version {
            return Err(E2eeBackupError::BackupVersionMismatch(input.version).into());
        }

        self.backups
            .delete_session_key(id.user(), backup.version(), Some(&input.room_id), None)
            .await?;
        backup.update_etag(id.user(), self.backups.as_ref()).await?;

        Ok(delete_backup_keys_for_room::v3::Response {
            etag: backup.etag().to_string(),
            count: UInt::new_saturating(
                backup.count(id.user(), self.backups.as_ref()).await?.into(),
            ),
        })
    }
}

#[derive(Inject)]
pub struct DeleteBackupSessionKeysCommand {
    backups: Arc<dyn BackupStorage>,
}

#[async_trait]
impl Command for DeleteBackupSessionKeysCommand {
    type Input = delete_backup_keys_for_session::v3::Request;
    type Output = delete_backup_keys_for_session::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let mut backup = self
            .backups
            .load_backup_version(id.user())
            .await?
            .ok_or(ResourceNotFound)?;

        if backup.version() != input.version {
            return Err(E2eeBackupError::BackupVersionMismatch(input.version).into());
        }

        self.backups
            .delete_session_key(
                id.user(),
                backup.version(),
                Some(&input.room_id),
                Some(input.session_id.as_str()),
            )
            .await?;
        backup.update_etag(id.user(), self.backups.as_ref()).await?;

        Ok(delete_backup_keys_for_session::v3::Response {
            etag: backup.etag().to_string(),
            count: UInt::new_saturating(
                backup.count(id.user(), self.backups.as_ref()).await?.into(),
            ),
        })
    }
}
