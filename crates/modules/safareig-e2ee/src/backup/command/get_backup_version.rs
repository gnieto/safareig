use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    command::Command,
    error::ResourceNotFound,
    ruma::{
        api::client::backup::{get_backup_info, get_latest_backup_info},
        UInt,
    },
    Inject,
};

use crate::backup::{error::E2eeBackupError, storage::BackupStorage};

#[derive(Inject)]
pub struct GetLatestBackupVersionCommand {
    backups: Arc<dyn BackupStorage>,
}

#[async_trait]
impl Command for GetLatestBackupVersionCommand {
    type Input = get_latest_backup_info::v3::Request;
    type Output = get_latest_backup_info::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(
        &self,
        _input: Self::Input,
        id: Identity,
    ) -> Result<Self::Output, Self::Error> {
        let backup = self
            .backups
            .load_backup_version(id.user())
            .await?
            .ok_or(ResourceNotFound)?;

        Ok(get_latest_backup_info::v3::Response {
            algorithm: backup.algorithm().to_owned(),
            count: UInt::from(backup.count(id.user(), self.backups.as_ref()).await?),
            etag: backup.etag().to_string(),
            version: backup.version().to_string(),
        })
    }
}

#[derive(Inject)]
pub struct GetBackupVersionCommand {
    backups: Arc<dyn BackupStorage>,
}

#[async_trait]
impl Command for GetBackupVersionCommand {
    type Input = get_backup_info::v3::Request;
    type Output = get_backup_info::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let backup = self
            .backups
            .load_backup_version(id.user())
            .await?
            .ok_or(ResourceNotFound)?;

        if backup.version() != input.version {
            return Err(E2eeBackupError::BackupVersionMismatch(input.version).into());
        }

        Ok(get_backup_info::v3::Response {
            algorithm: backup.algorithm().to_owned(),
            count: UInt::from(backup.count(id.user(), self.backups.as_ref()).await?),
            etag: backup.etag().to_string(),
            version: backup.version().to_string(),
        })
    }
}
