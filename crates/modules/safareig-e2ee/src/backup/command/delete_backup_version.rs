use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity, command::Command, ruma::api::client::backup::delete_backup_version, Inject,
};

use crate::backup::storage::BackupStorage;

#[derive(Inject)]
pub struct DeleteBackupVersionCommand {
    backups: Arc<dyn BackupStorage>,
}

#[async_trait]
impl Command for DeleteBackupVersionCommand {
    type Input = delete_backup_version::v3::Request;
    type Output = delete_backup_version::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        self.backups
            .delete_session_key(id.user(), &input.version, None, None)
            .await?;

        self.backups
            .delete_backup_version(id.user(), input.version.as_str())
            .await?;

        Ok(delete_backup_version::v3::Response {})
    }
}
