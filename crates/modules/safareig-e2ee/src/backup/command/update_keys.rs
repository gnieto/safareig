use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    command::Command,
    error::ResourceNotFound,
    ruma::{
        api::client::backup::{
            add_backup_keys, add_backup_keys_for_room, add_backup_keys_for_session,
        },
        UInt,
    },
    Inject,
};

use crate::backup::{error::E2eeBackupError, storage::BackupStorage};

#[derive(Inject)]
pub struct UpdateBackupKeysCommand {
    backups: Arc<dyn BackupStorage>,
}

#[async_trait]
impl Command for UpdateBackupKeysCommand {
    type Input = add_backup_keys::v3::Request;
    type Output = add_backup_keys::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let mut backup = self
            .backups
            .load_backup_version(id.user())
            .await?
            .ok_or(ResourceNotFound)?;

        if backup.version() != input.version {
            return Err(E2eeBackupError::BackupVersionMismatch(input.version).into());
        }

        for (room_id, room) in input.rooms {
            for (session_id, key) in room.sessions {
                backup
                    .update_key(
                        id.user(),
                        &room_id,
                        &session_id,
                        &key,
                        self.backups.as_ref(),
                    )
                    .await?;
            }
        }

        Ok(add_backup_keys::v3::Response {
            etag: backup.etag().to_string(),
            count: UInt::new_saturating(
                backup.count(id.user(), self.backups.as_ref()).await?.into(),
            ),
        })
    }
}

#[derive(Inject)]
pub struct UpdateBackupRoomKeysCommand {
    backups: Arc<dyn BackupStorage>,
}

#[async_trait]
impl Command for UpdateBackupRoomKeysCommand {
    type Input = add_backup_keys_for_room::v3::Request;
    type Output = add_backup_keys_for_room::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let mut backup = self
            .backups
            .load_backup_version(id.user())
            .await?
            .ok_or(ResourceNotFound)?;

        if backup.version() != input.version {
            return Err(E2eeBackupError::BackupVersionMismatch(input.version).into());
        }

        let room_id = input.room_id;
        for (session_id, key) in input.sessions {
            backup
                .update_key(
                    id.user(),
                    &room_id,
                    &session_id,
                    &key,
                    self.backups.as_ref(),
                )
                .await?;
        }

        Ok(add_backup_keys_for_room::v3::Response {
            etag: backup.etag().to_string(),
            count: UInt::new_saturating(
                backup.count(id.user(), self.backups.as_ref()).await?.into(),
            ),
        })
    }
}

#[derive(Inject)]
pub struct UpdateBackupSessionKeysCommand {
    backups: Arc<dyn BackupStorage>,
}

#[async_trait]
impl Command for UpdateBackupSessionKeysCommand {
    type Input = add_backup_keys_for_session::v3::Request;
    type Output = add_backup_keys_for_session::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let mut backup = self
            .backups
            .load_backup_version(id.user())
            .await?
            .ok_or(ResourceNotFound)?;

        if backup.version() != input.version {
            return Err(E2eeBackupError::BackupVersionMismatch(input.version).into());
        }

        let room_id = input.room_id;
        let session_id = input.session_id;
        let key = input.session_data;
        backup
            .update_key(
                id.user(),
                &room_id,
                &session_id,
                &key,
                self.backups.as_ref(),
            )
            .await?;

        Ok(add_backup_keys_for_session::v3::Response {
            etag: backup.etag().to_string(),
            count: UInt::new_saturating(
                backup.count(id.user(), self.backups.as_ref()).await?.into(),
            ),
        })
    }
}
