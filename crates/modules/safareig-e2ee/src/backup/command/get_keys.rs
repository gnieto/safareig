use std::{collections::BTreeMap, sync::Arc};

use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    command::Command,
    error::ResourceNotFound,
    ruma::{
        api::client::backup::{
            get_backup_keys, get_backup_keys_for_room, get_backup_keys_for_session, RoomKeyBackup,
        },
        OwnedRoomId,
    },
    Inject,
};

use crate::backup::{error::E2eeBackupError, storage::BackupStorage};

#[derive(Inject)]
pub struct GetBackupKeysCommand {
    backups: Arc<dyn BackupStorage>,
}

#[async_trait]
impl Command for GetBackupKeysCommand {
    type Input = get_backup_keys::v3::Request;
    type Output = get_backup_keys::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let backup = self
            .backups
            .load_backup_version(id.user())
            .await?
            .ok_or(ResourceNotFound)?;

        if backup.version() != input.version {
            return Err(E2eeBackupError::BackupVersionMismatch(input.version).into());
        }

        let keys = self
            .backups
            .get_session_keys(id.user(), backup.version(), None, None)
            .await?;

        let ruma_keys = keys
            .into_iter()
            .map(|(room_id, room_keys)| {
                let key_backup = RoomKeyBackup::new(room_keys);

                (room_id, key_backup)
            })
            .collect();

        Ok(get_backup_keys::v3::Response { rooms: ruma_keys })
    }
}

#[derive(Inject)]
pub struct GetBackupRoomKeysCommand {
    backups: Arc<dyn BackupStorage>,
}

#[async_trait]
impl Command for GetBackupRoomKeysCommand {
    type Input = get_backup_keys_for_room::v3::Request;
    type Output = get_backup_keys_for_room::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let backup = self
            .backups
            .load_backup_version(id.user())
            .await?
            .ok_or(ResourceNotFound)?;

        if backup.version() != input.version {
            return Err(E2eeBackupError::BackupVersionMismatch(input.version).into());
        }

        let keys = self
            .backups
            .get_session_keys(id.user(), backup.version(), Some(&input.room_id), None)
            .await?;

        let mut ruma_keys: BTreeMap<OwnedRoomId, RoomKeyBackup> = keys
            .into_iter()
            .map(|(room_id, room_keys)| {
                let key_backup = RoomKeyBackup::new(room_keys);

                (room_id, key_backup)
            })
            .collect();

        Ok(get_backup_keys_for_room::v3::Response {
            sessions: ruma_keys
                .remove(&input.room_id)
                .ok_or(ResourceNotFound)?
                .sessions,
        })
    }
}

#[derive(Inject)]
pub struct GetBackupSessionKeysCommand {
    backups: Arc<dyn BackupStorage>,
}

#[async_trait]
impl Command for GetBackupSessionKeysCommand {
    type Input = get_backup_keys_for_session::v3::Request;
    type Output = get_backup_keys_for_session::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let backup = self
            .backups
            .load_backup_version(id.user())
            .await?
            .ok_or(ResourceNotFound)?;

        if backup.version() != input.version {
            return Err(E2eeBackupError::BackupVersionMismatch(input.version).into());
        }

        let keys = self
            .backups
            .get_session_keys(id.user(), backup.version(), Some(&input.room_id), None)
            .await?;

        let mut ruma_keys: BTreeMap<OwnedRoomId, RoomKeyBackup> = keys
            .into_iter()
            .map(|(room_id, room_keys)| {
                let key_backup = RoomKeyBackup::new(room_keys);

                (room_id, key_backup)
            })
            .collect();

        Ok(get_backup_keys_for_session::v3::Response {
            key_data: ruma_keys
                .remove(&input.room_id)
                .ok_or(ResourceNotFound)?
                .sessions
                .remove(&input.session_id)
                .ok_or(ResourceNotFound)?,
        })
    }
}
