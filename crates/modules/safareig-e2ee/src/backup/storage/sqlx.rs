use safareig_core::{
    ruma::{api::client::backup::KeyBackupData, serde::Raw, RoomId, UserId},
    storage::StorageError,
};
use safareig_sqlx::{decode_from_deserialize, decode_from_str, decode_i64_to_u64};
use serde_json::value::RawValue;
use sqlx::{sqlite::SqliteRow, Row};

use super::{Backup, BackupStorage, SessionKeys};

pub struct SqliteE2eeBackupStorage {
    pool: sqlx::Pool<sqlx::Sqlite>,
}

impl SqliteE2eeBackupStorage {
    pub fn new(pool: sqlx::Pool<sqlx::Sqlite>) -> Self {
        Self { pool }
    }
}

#[async_trait::async_trait]
impl BackupStorage for SqliteE2eeBackupStorage {
    async fn load_backup_version(&self, user_id: &UserId) -> Result<Option<Backup>, StorageError> {
        let latest = sqlx::query(
            r#"
            SELECT * FROM server_side_backup
            WHERE user_id = ?1
            ORDER BY version DESC
            LIMIT 1
            "#,
        )
        .bind(user_id.as_str())
        .fetch_optional(&self.pool)
        .await?
        .map(Backup::try_from)
        .transpose()?;

        Ok(latest)
    }

    async fn update_backup(&self, user_id: &UserId, backup: &Backup) -> Result<(), StorageError> {
        sqlx::query(
            r#"
            INSERT INTO server_side_backup (user_id, version, auth_data, etag)
            VALUES (?1, ?2, ?3, ?4)
            ON CONFLICT DO UPDATE SET auth_data = ?3, etag = ?4
            "#,
        )
        .bind(user_id.as_str())
        .bind(backup.version())
        .bind(backup.algorithm.json().get())
        .bind(backup.etag())
        .execute(&self.pool)
        .await?;

        Ok(())
    }

    async fn delete_backup_version(
        &self,
        user_id: &UserId,
        version: &str,
    ) -> Result<(), StorageError> {
        sqlx::query(
            r#"
            DELETE FROM server_side_backup
            WHERE user_id = ?1 AND version = ?2
            "#,
        )
        .bind(user_id.as_str())
        .bind(version)
        .execute(&self.pool)
        .await?;

        Ok(())
    }

    async fn get_session_key(
        &self,
        user_id: &UserId,
        version: &str,
        room_id: &RoomId,
        session_id: &str,
    ) -> Result<Option<Raw<KeyBackupData>>, StorageError> {
        let sql = r#"
            SELECT key_data FROM server_side_backup_key
            WHERE user_id = ?1 AND version = ?2 AND room_id = ?3 AND session_id = ?4;
        "#
        .to_string();

        let key = sqlx::query(&sql)
            .bind(user_id.as_str())
            .bind(version)
            .bind(room_id.as_str())
            .bind(session_id)
            .fetch_optional(&self.pool)
            .await?
            .map(|row| {
                let key = row.get::<&str, _>("key_data");
                Raw::from_json(RawValue::from_string(key.to_string()).unwrap())
            });

        Ok(key)
    }

    async fn get_session_keys(
        &self,
        user_id: &UserId,
        version: &str,
        room_id: Option<&RoomId>,
        session_id: Option<&str>,
    ) -> Result<SessionKeys, StorageError> {
        let mut sql = r#"
            SELECT version, user_id, room_id, key_data, CAST(session_id as TEXT) as session_id  FROM server_side_backup_key
            WHERE user_id = ?1 AND version = ?2
        "#.to_string();

        if let Some(_room_id) = room_id {
            sql.push_str(" AND room_id = ?3");
        }

        if let Some(_session) = session_id {
            sql.push_str(" AND session_id = ?4");
        }

        let mut query = sqlx::query(&sql).bind(user_id.as_str()).bind(version);

        if let Some(room_id) = room_id {
            query = query.bind(room_id.as_str());
        }

        if let Some(session) = session_id {
            query = query.bind(session);
        }

        let mut session_keys = SessionKeys::default();

        let rows = query.fetch_all(&self.pool).await?;

        for r in rows {
            let room_id = decode_from_str(r.get::<&str, _>("room_id"))?;
            let session_id = r.get::<String, _>("session_id");
            let key = r.get::<&str, _>("key_data");
            let key = Raw::from_json(RawValue::from_string(key.to_string()).unwrap());

            let room_entry = session_keys.entry(room_id).or_default();
            room_entry.entry(session_id).or_insert(key);
        }

        Ok(session_keys)
    }

    async fn update_session_key(
        &self,
        user_id: &UserId,
        version: &str,
        room_id: &RoomId,
        session_id: &str,
        key_backup: &Raw<KeyBackupData>,
    ) -> Result<(), StorageError> {
        let sql = r#"
            INSERT INTO server_side_backup_key (version, user_id, room_id, session_id, key_data)
            VALUES(?1, ?2, ?3, ?4, ?5)
            ON CONFLICT DO UPDATE SET key_data = ?5
        "#;

        let _key = sqlx::query(sql)
            .bind(version)
            .bind(user_id.as_str())
            .bind(room_id.as_str())
            .bind(session_id)
            .bind(key_backup.json().get())
            .execute(&self.pool)
            .await?;

        Ok(())
    }

    async fn delete_session_key(
        &self,
        user_id: &UserId,
        version: &str,
        room_id: Option<&RoomId>,
        session_id: Option<&str>,
    ) -> Result<(), StorageError> {
        let mut sql = r#"
            DELETE FROM server_side_backup_key
            WHERE user_id = ?1 AND version = ?2
        "#
        .to_string();

        if let Some(_room_id) = room_id {
            sql.push_str(" AND room_id = ?");
        }

        if let Some(_session) = session_id {
            sql.push_str(" AND session_id = ?");
        }

        let mut query = sqlx::query(&sql).bind(user_id.as_str()).bind(version);

        if let Some(room_id) = room_id {
            query = query.bind(room_id.as_str());
        }

        if let Some(session) = session_id {
            query = query.bind(session);
        }

        query.execute(&self.pool).await?;

        Ok(())
    }

    async fn count_session_keys(
        &self,
        user_id: &UserId,
        version: &str,
    ) -> Result<u32, StorageError> {
        let sql = r#"
            SELECT COUNT(*) as key_amount FROM server_side_backup_key
            WHERE user_id = ?1 AND version = ?2
        "#;

        let key_count = sqlx::query(sql)
            .bind(user_id.as_str())
            .bind(version)
            .fetch_optional(&self.pool)
            .await?
            .map(|row| {
                let count = decode_i64_to_u64(row.get::<i64, _>("key_amount")).unwrap_or_default();

                u32::try_from(count).unwrap_or_default()
            })
            .unwrap_or_default();

        Ok(key_count)
    }
}

impl TryFrom<SqliteRow> for Backup {
    type Error = StorageError;

    fn try_from(row: SqliteRow) -> Result<Self, StorageError> {
        let version = row.get::<String, _>("version");
        let auth_data = decode_from_deserialize(row.get::<&str, _>("auth_data"))?;
        let etag = row.get::<&str, _>("etag");

        let backup = Backup {
            algorithm: auth_data,
            version,
            etag: etag.to_string(),
        };

        Ok(backup)
    }
}
