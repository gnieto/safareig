use http::StatusCode;
use safareig_core::{
    error::RumaExt,
    ruma::api::client::error::{ErrorBody, ErrorKind},
    storage::StorageError,
};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum E2eeBackupError {
    #[error("Current version does not match with the provided one {0}")]
    BackupVersionMismatch(String),
    #[error("New algorithm does not match with the old one")]
    AlgorithmMismatch,
    #[error("Storage error {0}")]
    StorageError(#[from] StorageError),
    #[error("Serde error {0}")]
    SerdeError(#[from] serde_json::Error),
}

impl From<E2eeBackupError> for safareig_core::ruma::api::client::Error {
    fn from(e: E2eeBackupError) -> Self {
        match e {
            E2eeBackupError::BackupVersionMismatch(_) => safareig_core::ruma::api::client::Error {
                body: ErrorBody::Standard {
                    // TODO: Add M_WRONG_ROOM_KEYS_VERSION
                    kind: ErrorKind::Unknown,
                    message: e.to_string(),
                },
                status_code: StatusCode::FORBIDDEN,
            },
            E2eeBackupError::AlgorithmMismatch => safareig_core::ruma::api::client::Error {
                body: ErrorBody::Standard {
                    kind: ErrorKind::Unknown,
                    message: e.to_string(),
                },
                status_code: StatusCode::BAD_REQUEST,
            },
            E2eeBackupError::SerdeError(_) => safareig_core::ruma::api::client::Error {
                body: ErrorBody::Standard {
                    kind: ErrorKind::Unknown,
                    message: e.to_string(),
                },
                status_code: StatusCode::INTERNAL_SERVER_ERROR,
            },
            E2eeBackupError::StorageError(e) => e.into(),
        }
    }
}

impl From<E2eeBackupError> for safareig_core::ruma::api::error::MatrixError {
    fn from(error: E2eeBackupError) -> Self {
        let client = safareig_core::ruma::api::client::Error::from(error);
        client.to_generic()
    }
}
