use std::sync::Arc;

use safareig_core::{
    ruma::{OwnedUserId, UserId},
    storage::StorageError,
};
use safareig_ephemeral::services::{EphemeralEvent, EphemeralStream};

use crate::storage::{E2eeStreamStorage, E2eeUpdate, E2eeUpdateType};

#[derive(Clone)]
pub struct E2eeStream {
    storage: Arc<dyn E2eeStreamStorage>,
    ephemeral: EphemeralStream,
}

impl E2eeStream {
    pub fn new(storage: Arc<dyn E2eeStreamStorage>, ephemeral: EphemeralStream) -> Self {
        Self { storage, ephemeral }
    }

    pub async fn send_cross_signing_key_update(
        &self,
        user_id: &UserId,
    ) -> Result<(), StorageError> {
        let update = E2eeUpdate {
            user: user_id.to_owned(),
            kind: E2eeUpdateType::CrossSigningKeys,
        };

        self.storage.push_e2ee_update(&update).await?;
        self.ephemeral
            .push_event(EphemeralEvent::CrossSigning(user_id.to_owned()));

        Ok(())
    }

    pub async fn send_device_key_update(&self, user_id: &UserId) -> Result<(), StorageError> {
        let update = E2eeUpdate {
            user: user_id.to_owned(),
            kind: E2eeUpdateType::DeviceKey,
        };

        self.storage.push_e2ee_update(&update).await?;

        Ok(())
    }

    pub async fn send_signatures_update(
        &self,
        user_id: &UserId,
        target_users: Vec<OwnedUserId>,
    ) -> Result<(), StorageError> {
        let update = E2eeUpdate {
            user: user_id.to_owned(),
            kind: E2eeUpdateType::Signatures(target_users),
        };

        self.storage.push_e2ee_update(&update).await?;

        Ok(())
    }

    pub async fn send_user_signing(&self, user_id: &UserId) -> Result<(), StorageError> {
        let update = E2eeUpdate {
            user: user_id.to_owned(),
            kind: E2eeUpdateType::UserSigningKey,
        };

        self.storage.push_e2ee_update(&update).await?;

        Ok(())
    }
}
