use std::{collections::HashSet, str::FromStr, sync::Arc};

use safareig_core::{
    pagination::{Boundary, Direction, Limit, Pagination, Range},
    ruma::{
        api::client::sync::sync_events::{self, DeviceLists},
        events::{
            room::member::{MembershipState, RoomMemberEventContent},
            StateEventType,
        },
        serde::Raw,
        OwnedUserId, UInt,
    },
    storage::StorageError,
    StreamToken,
};
use safareig_sync_api::{
    context::{Context, SyncMode},
    error::SyncError,
    SyncExtension, SyncToken, SyncTokenPart,
};

use crate::storage::{E2eeStreamStorage, E2eeUpdateType, KeyStorage};

pub struct E2eeSyncExtension {
    stream: Arc<dyn E2eeStreamStorage>,
    keys: Arc<dyn KeyStorage>,
}

impl E2eeSyncExtension {
    pub fn new(stream: Arc<dyn E2eeStreamStorage>, keys: Arc<dyn KeyStorage>) -> Self {
        Self { stream, keys }
    }
}

#[async_trait::async_trait]
impl SyncExtension for E2eeSyncExtension {
    #[tracing::instrument(name = "sync-e2ee", skip_all)]
    async fn fill_sync<'a>(
        &self,
        ctx: &'a Context<'a>,
        response: &mut sync_events::v3::Response,
        token: &mut SyncToken,
    ) -> Result<(), SyncError> {
        let current_token = token.extension_token::<E2eeListToken>()?;
        let new_token = match current_token {
            Some(e2ee_token) => {
                let range = (Boundary::Exclusive(e2ee_token.0), Boundary::Unlimited);

                let query = Pagination::new(range.into(), ());
                let updates = self
                    .stream
                    .e2ee_updates(query)
                    .await
                    .map_err(|e| StorageError::Custom(e.to_string()))?;

                let next = updates.next().cloned().map(E2eeListToken);

                // TODO: Check user's visibility
                let mut changed_users = HashSet::new();

                let records = updates.records();
                for update in records {
                    match update.kind {
                        E2eeUpdateType::UserSigningKey => {
                            if ctx.user() == update.user {
                                changed_users.insert(update.user);
                            };
                        }
                        E2eeUpdateType::Signatures(users) => {
                            changed_users.extend(users.into_iter());
                        }
                        _ => {
                            changed_users.insert(update.user);
                        }
                    };
                }

                response.device_lists = DeviceLists {
                    changed: changed_users.into_iter().collect(),
                    left: Default::default(),
                };

                next
            }
            None => self
                .load_latest_token()
                .await
                .map_err(|err| SyncError::Custom(err.to_string()))?,
        };

        if let Some(e2ee_token) = new_token {
            token.update(e2ee_token);
        }

        if let SyncMode::Incremental = ctx.sync_mode() {
            let mut joined_or_invited = HashSet::new();
            let mut left = HashSet::new();

            for joined in response.rooms.join.values() {
                self.collect_members(
                    &mut joined_or_invited,
                    &mut left,
                    joined.state.events.as_ref(),
                );
                self.collect_members(
                    &mut joined_or_invited,
                    &mut left,
                    joined.timeline.events.as_ref(),
                );
            }

            for invite in response.rooms.invite.values() {
                self.collect_members(
                    &mut joined_or_invited,
                    &mut left,
                    invite.invite_state.events.as_ref(),
                );
            }

            response
                .device_lists
                .changed
                .extend(joined_or_invited.into_iter());
            response.device_lists.left.extend(left.into_iter());
        }

        // TODO: Consider merge this into a single query.
        self.add_one_time_token_count(ctx, response).await?;
        self.add_unused_fallback_keys(ctx, response).await?;

        Ok(())
    }
}

impl E2eeSyncExtension {
    async fn add_one_time_token_count<'a>(
        &self,
        ctx: &'a Context<'a>,
        response: &mut sync_events::v3::Response,
    ) -> Result<(), SyncError> {
        if let Some(device_id) = ctx.id().device() {
            response.device_one_time_keys_count = self
                .keys
                .pending_keys(device_id)
                .await?
                .into_iter()
                .map(|tuple| (tuple.0, UInt::from(tuple.1)))
                .collect();
        }

        Ok(())
    }

    async fn add_unused_fallback_keys<'a>(
        &self,
        ctx: &'a Context<'a>,
        response: &mut sync_events::v3::Response,
    ) -> Result<(), SyncError> {
        if let Some(device_id) = ctx.id().device() {
            response.device_unused_fallback_key_types =
                Some(self.keys.unclaimed_fallback_algorithms(device_id).await?);
        }

        Ok(())
    }

    fn collect_members<T>(
        &self,
        joined: &mut HashSet<OwnedUserId>,
        left: &mut HashSet<OwnedUserId>,
        timeline: &[Raw<T>],
    ) {
        timeline.iter().for_each(|event| {
            if let Some((content, state_key)) = event.membership() {
                match content.membership {
                    MembershipState::Join | MembershipState::Invite => {
                        joined.insert(state_key);
                    }
                    MembershipState::Leave | MembershipState::Ban => {
                        left.insert(state_key);
                    }
                    _ => (),
                }
            }
        })
    }

    async fn load_latest_token(&self) -> Result<Option<E2eeListToken>, StorageError> {
        // Read latest update, without boundaries and backward direction. Set the token with the returned
        // update (if any).
        let query = Pagination::new(Range::all(), ())
            .with_direction(Direction::Backward)
            .with_limit(Limit::from(1u32));
        let result = self.stream.e2ee_updates(query).await?;

        Ok(result
            .previous()
            .or_else(|| result.next())
            .cloned()
            .map(E2eeListToken))
    }
}

trait RawMembershipEventExt {
    fn membership(&self) -> Option<(RoomMemberEventContent, OwnedUserId)>;
}

// This implementation is a hack in order to prevent a big bloat of several megabytes pulled from Ruma and the deserialization from
// Any*Event structures. Since we are only interested in membership events, this allows us to deserialize the event only in the case
// that the event is a membership event.
// A better implementation for this would be to move the event structure into a new crate and have a custom representation of a timeline
// in our side, instead of re-use the external representation from Ruma.
impl<T> RawMembershipEventExt for &Raw<T> {
    fn membership(&self) -> Option<(RoomMemberEventContent, OwnedUserId)> {
        let event_type = self.get_field::<StateEventType>("type").ok().flatten()?;

        if let StateEventType::RoomMember = event_type {
            let content = self
                .get_field::<RoomMemberEventContent>("content")
                .ok()
                .flatten()?;
            let state_key = self.get_field::<OwnedUserId>("state_key").ok().flatten()?;

            return Some((content, state_key));
        }

        None
    }
}

#[derive(Default)]
pub struct E2eeListToken(StreamToken);

impl SyncTokenPart for E2eeListToken {
    fn id() -> &'static str {
        "el"
    }
}

impl ToString for E2eeListToken {
    fn to_string(&self) -> String {
        self.0.to_string()
    }
}

impl FromStr for E2eeListToken {
    type Err = std::num::ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Self(StreamToken::from_str(s)?))
    }
}

#[cfg(test)]
mod tests {
    use safareig_core::ruma::{
        events::{room::member::MembershipState, AnySyncTimelineEvent},
        owned_user_id,
        serde::Raw,
    };
    use serde_json::json;

    use super::RawMembershipEventExt;

    #[test]
    fn it_extract_membership_events_state() {
        let json = json!({
            "type": "m.room.member",
            "state_key": "@test:localhost:8448",
            "content": {
                "membership": "join"
            }
        });
        let raw: Raw<AnySyncTimelineEvent> =
            Raw::from_json_string(serde_json::to_string(&json).unwrap()).unwrap();

        let (content, state_key) = (&raw).membership().unwrap();
        assert_eq!(MembershipState::Join, content.membership);
        assert_eq!(owned_user_id!("@test:localhost:8448"), state_key);
    }

    #[test]
    fn it_does_not_extract_membership_with_invalid_state_key() {
        let json = json!({
            "type": "m.room.member",
            "state_key": "<invalid>",
            "content": {
                "membership": "join"
            }
        });
        let raw: Raw<AnySyncTimelineEvent> =
            Raw::from_json_string(serde_json::to_string(&json).unwrap()).unwrap();

        assert!((&raw).membership().is_none());
    }

    #[test]
    fn it_does_not_extract_membership_with_invalid_content() {
        let json = json!({
            "type": "m.room.member",
            "state_key": "@test:localhost:8448",
            "content": {}
        });
        let raw: Raw<AnySyncTimelineEvent> =
            Raw::from_json_string(serde_json::to_string(&json).unwrap()).unwrap();

        assert!((&raw).membership().is_none());
    }
}
