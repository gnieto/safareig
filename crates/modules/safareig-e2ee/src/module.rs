use std::sync::Arc;

use safareig_core::{
    command::{Container, InjectServices, Module, Router, ServiceCollectionExtension},
    database::DatabaseConnection,
    ddi::{ServiceCollectionExt, ServiceProvider, ServiceResolverExt},
    storage::StorageError,
};
use safareig_ephemeral::services::{EphemeralHandler, EphemeralStream};
use safareig_sync_api::SyncExtension;
use safareig_uiaa::module::ContainerUIAAExt;

use crate::{
    backup::{
        command::{
            CreateBackupVersionCommand, DeleteBackupKeysCommand, DeleteBackupRoomKeysCommand,
            DeleteBackupSessionKeysCommand, DeleteBackupVersionCommand, GetBackupKeysCommand,
            GetBackupRoomKeysCommand, GetBackupSessionKeysCommand, GetBackupVersionCommand,
            GetLatestBackupVersionCommand, UpdateBackupKeysCommand, UpdateBackupRoomKeysCommand,
            UpdateBackupSessionKeysCommand, UpdateBackupVersionCommand,
        },
        storage::BackupStorage,
    },
    command::{
        client::{
            claim_keys::ClaimKeysCommand, query_keys::QueryKeysCommand,
            upload_keys::UploadKeysCommand, UploadSignaturesCommand, UploadUserKeysCommand,
        },
        federation::{
            claim_keys::ClaimKeysCommand as FederationClaimCommand,
            query_keys::QueryKeysCommand as FederationQueryCommand, UserDeviceCommand,
        },
    },
    ephemeral::{EphemeralCrossSigningHandler, EphemeralDeviceListHandler},
    keys::{E2EEKeyHandler, FederatedE2EEKeyHandler, LocalE2EEKeyHandler, SignatureUploader},
    storage::{E2eeStreamStorage, KeyStorage, SignatureStorage},
    stream::E2eeStream,
    sync::E2eeSyncExtension,
};

#[derive(Default)]
pub struct E2eeModule {}

impl E2eeModule {
    fn storage(connection: DatabaseConnection) -> Arc<dyn KeyStorage> {
        match connection {
            #[cfg(feature = "backend-sqlx")]
            DatabaseConnection::Sqlx(pool) => {
                Arc::new(crate::storage::sqlx::SqliteE2EEStorage::new(pool))
            }
            _ => unimplemented!(""),
        }
    }

    fn stream_storage(connection: DatabaseConnection) -> Arc<dyn E2eeStreamStorage> {
        match connection {
            #[cfg(feature = "backend-sqlx")]
            DatabaseConnection::Sqlx(pool) => {
                Arc::new(crate::storage::sqlx::SqliteE2EEStorage::new(pool))
            }
            _ => unimplemented!(""),
        }
    }

    fn backup_storage(connection: DatabaseConnection) -> Arc<dyn BackupStorage> {
        match connection {
            #[cfg(feature = "backend-sqlx")]
            DatabaseConnection::Sqlx(pool) => Arc::new(
                crate::backup::storage::sqlx::SqliteE2eeBackupStorage::new(pool),
            ),
            _ => unimplemented!(""),
        }
    }

    fn signature_storage(connection: DatabaseConnection) -> Arc<dyn SignatureStorage> {
        match connection {
            #[cfg(feature = "backend-sqlx")]
            DatabaseConnection::Sqlx(pool) => {
                Arc::new(crate::storage::sqlx::SqliteE2EEStorage::new(pool))
            }
            _ => unimplemented!(""),
        }
    }
}

#[async_trait::async_trait]
impl<R: Router> Module<R> for E2eeModule {
    fn register(&self, container: &mut Container, router: &mut R) {
        // Services
        container
            .service_collection
            .service_factory(|db: &DatabaseConnection| Ok(Self::storage(db.clone())));
        container
            .service_collection
            .service_factory(|db: &DatabaseConnection| Ok(Self::stream_storage(db.clone())));
        container
            .service_collection
            .service_factory(|db: &DatabaseConnection| Ok(Self::signature_storage(db.clone())));
        container
            .service_collection
            .service_factory(|db: &DatabaseConnection| Ok(Self::backup_storage(db.clone())));
        container.service_collection.service_factory(
            |stream: &Arc<dyn E2eeStreamStorage>, es: &EphemeralStream| {
                Ok(E2eeStream::new(stream.clone(), es.clone()))
            },
        );

        container
            .service_collection
            .register::<LocalE2EEKeyHandler>();
        container
            .service_collection
            .service_factory(|sp: &ServiceProvider| {
                let federated = FederatedE2EEKeyHandler::inject(sp)?;
                let handler: Arc<dyn E2EEKeyHandler> = Arc::new(federated);

                Ok(handler)
            });
        container.service_collection.register::<SignatureUploader>();
        container.service_collection.service_factory_var(
            "e2ee",
            |ss: &Arc<dyn E2eeStreamStorage>, ks: &Arc<dyn KeyStorage>| {
                let sync_extension: Arc<dyn SyncExtension> =
                    Arc::new(E2eeSyncExtension::new(ss.clone(), ks.clone()));
                Ok(sync_extension)
            },
        );

        // Endpoints
        container.inject_endpoint::<_, ClaimKeysCommand, _>(router);
        container.inject_endpoint::<_, UploadKeysCommand, _>(router);
        container.inject_endpoint::<_, QueryKeysCommand, _>(router);
        container.inject_endpoint::<_, UploadSignaturesCommand, _>(router);

        // Backups
        container.inject_endpoint::<_, CreateBackupVersionCommand, _>(router);
        container.inject_endpoint::<_, GetLatestBackupVersionCommand, _>(router);
        container.inject_endpoint::<_, DeleteBackupVersionCommand, _>(router);
        container.inject_endpoint::<_, UpdateBackupVersionCommand, _>(router);
        container.inject_endpoint::<_, GetBackupVersionCommand, _>(router);
        container.inject_endpoint::<_, UpdateBackupKeysCommand, _>(router);
        container.inject_endpoint::<_, UpdateBackupRoomKeysCommand, _>(router);
        container.inject_endpoint::<_, UpdateBackupSessionKeysCommand, _>(router);
        container.inject_endpoint::<_, GetBackupKeysCommand, _>(router);
        container.inject_endpoint::<_, GetBackupRoomKeysCommand, _>(router);
        container.inject_endpoint::<_, GetBackupSessionKeysCommand, _>(router);
        container.inject_endpoint::<_, DeleteBackupKeysCommand, _>(router);
        container.inject_endpoint::<_, DeleteBackupRoomKeysCommand, _>(router);
        container.inject_endpoint::<_, DeleteBackupSessionKeysCommand, _>(router);
        container.uiaa_endpoint::<_, UploadUserKeysCommand, _>("password", router);

        if container.config.federation.enabled {
            container.inject_endpoint::<_, FederationClaimCommand, _>(router);
            container.inject_endpoint::<_, FederationQueryCommand, _>(router);
            container.inject_endpoint::<_, UserDeviceCommand, _>(router);
        }

        container.service_collection.service_factory_var(
            "m.signing_key_update",
            |sp: &ServiceProvider| {
                let handler: Arc<dyn EphemeralHandler> =
                    Arc::new(EphemeralCrossSigningHandler::inject(sp)?);
                Ok(handler)
            },
        );
        container.service_collection.service_factory_var(
            "m.device_list_update",
            |sp: &ServiceProvider| {
                let handler: Arc<dyn EphemeralHandler> =
                    Arc::new(EphemeralDeviceListHandler::inject(sp)?);
                Ok(handler)
            },
        );
    }

    async fn run_migrations(&self, container: &ServiceProvider) -> Result<(), StorageError> {
        #[cfg(feature = "backend-sqlx")]
        {
            let db = container.get::<DatabaseConnection>()?;
            safareig_sqlx::run_migration(db, "sqlite", "safareig-e2ee").await?;
        }

        Ok(())
    }
}
