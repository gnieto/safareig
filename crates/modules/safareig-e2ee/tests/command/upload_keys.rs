use std::sync::Arc;

use http::StatusCode;
use safareig_core::{
    command::Command,
    ruma::{
        api::client::{error::ErrorKind, keys::upload_signing_keys, uiaa::UiaaResponse},
        encryption::{CrossSigningKey, KeyUsage},
        serde::Raw,
    },
};
use safareig_e2ee::{
    command::client::UploadUserKeysCommand, module::E2eeModule, storage::KeyStorage, E2eeError,
};
use safareig_federation::keys::sign_ruma_object;
use safareig_testing::{error::RumaErrorExtension, TestingApp};
use safareig_uiaa::UiaaWrapper;

use crate::command::{set_user_cross_keys, EncryptionKey};

#[tokio::test]
async fn store_cross_signing_keys() {
    let app = TestingApp::with_modules(vec![Box::new(E2eeModule {})]).await;
    let id = app.register().await;

    set_user_cross_keys(&app, &id).await;

    let storage = app.service::<Arc<dyn KeyStorage>>();
    let keys = storage.user_cross_signing_keys(id.user()).await.unwrap();

    assert!(keys.master().is_some());
}

#[tokio::test]
async fn allow_master_key_not_being_signed() {
    let app = TestingApp::with_modules(vec![Box::new(E2eeModule {})]).await;
    let id = app.register().await;
    let master = EncryptionKey::generate("master_key");
    let cross_key = master.cross_signing_key(id.user(), KeyUsage::Master);

    let cmd = app.command::<UiaaWrapper<UploadUserKeysCommand>>();
    let input = upload_signing_keys::v3::Request {
        auth: Some(TestingApp::uiaa_password(id.user())),
        master_key: Some(Raw::new(&cross_key).unwrap()),
        self_signing_key: None,
        user_signing_key: None,
    };
    cmd.execute(input, id.clone()).await.unwrap();

    let storage = app.service::<Arc<dyn KeyStorage>>();
    let keys = storage.user_cross_signing_keys(id.user()).await.unwrap();

    assert!(keys.master().is_some());
}

#[tokio::test]
async fn cross_keys_with_more_than_one_key_should_fail() {
    let app = TestingApp::with_modules(vec![Box::new(E2eeModule {})]).await;
    let id = app.register().await;
    let master = EncryptionKey::generate("master_key");
    let master2 = EncryptionKey::generate("mk2");
    let cross_key2 = master2.cross_signing_key(id.user(), KeyUsage::Master);
    let mut cross_key = master.cross_signing_key(id.user(), KeyUsage::Master);
    cross_key.keys.extend(cross_key2.keys.into_iter());

    let cmd = app.command::<UiaaWrapper<UploadUserKeysCommand>>();
    let input = upload_signing_keys::v3::Request {
        auth: Some(TestingApp::uiaa_password(id.user())),
        master_key: Some(Raw::new(&cross_key).unwrap()),
        self_signing_key: None,
        user_signing_key: None,
    };

    let response = cmd.execute(input, id.clone()).await;

    assert!(response.is_err());
    let error = match response.err().unwrap() {
        UiaaResponse::MatrixError(e) => e,
        _ => unreachable!(""),
    };
    assert_eq!(error.status_code, StatusCode::BAD_REQUEST);
    assert_eq!(error.kind(), ErrorKind::Unknown);
}

#[tokio::test]
async fn non_properly_signed_keys_are_rejected() {
    let app = TestingApp::with_modules(vec![Box::new(E2eeModule {})]).await;
    let id = app.register().await;
    let master = EncryptionKey::generate("master_key");
    let another_master = EncryptionKey::generate("another_master_key");
    let cross_key = master.cross_signing_key(id.user(), KeyUsage::Master);
    let raw_key = sign_ruma_object(id.user().as_str(), &cross_key, &[master.key_pair()]).unwrap();

    let self_signing =
        EncryptionKey::generate("self_signing").cross_signing_key(id.user(), KeyUsage::SelfSigning);
    let raw_self_signing = sign_ruma_object(
        id.user().as_str(),
        &self_signing,
        &[another_master.key_pair()],
    )
    .unwrap();

    let cmd = app.command::<UiaaWrapper<UploadUserKeysCommand>>();
    let input = upload_signing_keys::v3::Request {
        auth: Some(TestingApp::uiaa_password(id.user())),
        master_key: Some(raw_key),
        self_signing_key: Some(raw_self_signing),
        user_signing_key: None,
    };

    let response = cmd.execute(input, id.clone()).await;

    assert!(response.is_err());
    let error = match response.err().unwrap() {
        UiaaResponse::MatrixError(e) => e,
        _ => unreachable!(""),
    };
    assert_eq!(error.status_code, StatusCode::BAD_REQUEST);
    assert_eq!(error.kind(), ErrorKind::InvalidParam);
    assert_eq!(
        error.message(),
        E2eeError::KeySignatureValidation.to_string()
    );
}

#[tokio::test]
async fn upload_keys_with_conflicting_keys_with_master_keys() {
    let app = TestingApp::with_modules(vec![Box::new(E2eeModule {})]).await;
    let id = app.register().await;
    let master = EncryptionKey::generate("master_key");
    let cross_key = master.cross_signing_key(id.user(), KeyUsage::Master);
    let raw_key: Raw<CrossSigningKey> =
        sign_ruma_object(id.user().as_str(), &cross_key, &[master.key_pair()]).unwrap();

    let cmd = app.command::<UiaaWrapper<UploadUserKeysCommand>>();
    let input = upload_signing_keys::v3::Request {
        auth: Some(TestingApp::uiaa_password(id.user())),
        master_key: Some(raw_key.clone()),
        self_signing_key: Some(raw_key),
        user_signing_key: None,
    };
    let response = cmd.execute(input, id.clone()).await;

    assert!(response.is_err());
    let error = match response.err().unwrap() {
        UiaaResponse::MatrixError(e) => e,
        _ => unreachable!(""),
    };
    assert_eq!(error.status_code, StatusCode::FORBIDDEN);
    assert_eq!(error.kind(), ErrorKind::Unknown);
    assert_eq!(error.message(), E2eeError::ConflictingMasterKey.to_string());
}

#[tokio::test]
async fn update_non_master_keys() {
    let app = TestingApp::with_modules(vec![Box::new(E2eeModule {})]).await;
    let id = app.register().await;
    let master = EncryptionKey::generate("master_key");
    let cross_key = master.cross_signing_key(id.user(), KeyUsage::Master);
    let raw_key = sign_ruma_object(id.user().as_str(), &cross_key, &[master.key_pair()]).unwrap();

    let self_signing = EncryptionKey::generate("self_signing");
    let cross_self = self_signing.cross_signing_key(id.user(), KeyUsage::SelfSigning);
    let raw_self = sign_ruma_object(id.user().as_str(), &cross_self, &[master.key_pair()]).unwrap();

    let user_signing = EncryptionKey::generate("user_signing");
    let cross_user = user_signing.cross_signing_key(id.user(), KeyUsage::UserSigning);
    let raw_user = sign_ruma_object(id.user().as_str(), &cross_user, &[master.key_pair()]).unwrap();

    let cmd = app.command::<UiaaWrapper<UploadUserKeysCommand>>();
    let input = upload_signing_keys::v3::Request {
        auth: Some(TestingApp::uiaa_password(id.user())),
        master_key: Some(raw_key),
        self_signing_key: Some(raw_self),
        user_signing_key: Some(raw_user),
    };
    cmd.execute(input, id.clone()).await.unwrap();

    let self_signing = EncryptionKey::generate("self_signing2");
    let cross_self = self_signing.cross_signing_key(id.user(), KeyUsage::SelfSigning);
    let raw_self = sign_ruma_object(id.user().as_str(), &cross_self, &[master.key_pair()]).unwrap();

    let cmd = app.command::<UiaaWrapper<UploadUserKeysCommand>>();
    let input = upload_signing_keys::v3::Request {
        auth: Some(TestingApp::uiaa_password(id.user())),
        master_key: None,
        self_signing_key: Some(raw_self),
        user_signing_key: None,
    };
    cmd.execute(input, id.clone()).await.unwrap();

    let storage = app.service::<Arc<dyn KeyStorage>>();
    let keys = storage.user_cross_signing_keys(id.user()).await.unwrap();
    let key_id = self_signing.key_id();
    let signing = keys.self_signing().unwrap().deserialize().unwrap();
    assert!(signing.keys.contains_key(&key_id));
}

#[tokio::test]
async fn update_master_keys_invalidates_previously_stored_user_keys() {
    let app = TestingApp::with_modules(vec![Box::new(E2eeModule {})]).await;
    let id = app.register().await;

    // Prepare current keys
    let master = EncryptionKey::generate("master_key");
    let cross_key = master.cross_signing_key(id.user(), KeyUsage::Master);
    let raw_key = sign_ruma_object(id.user().as_str(), &cross_key, &[master.key_pair()]).unwrap();

    let self_signing = EncryptionKey::generate("self_signing");
    let cross_self = self_signing.cross_signing_key(id.user(), KeyUsage::SelfSigning);
    let raw_self = sign_ruma_object(id.user().as_str(), &cross_self, &[master.key_pair()]).unwrap();

    let user_signing = EncryptionKey::generate("user_signing");
    let cross_user = user_signing.cross_signing_key(id.user(), KeyUsage::UserSigning);
    let raw_user = sign_ruma_object(id.user().as_str(), &cross_user, &[master.key_pair()]).unwrap();

    let cmd = app.command::<UiaaWrapper<UploadUserKeysCommand>>();
    let input = upload_signing_keys::v3::Request {
        auth: Some(TestingApp::uiaa_password(id.user())),
        master_key: Some(raw_key),
        self_signing_key: Some(raw_self),
        user_signing_key: Some(raw_user),
    };
    cmd.execute(input, id.clone()).await.unwrap();

    // Update master key invalidates previous self sigining and user keys
    let master = EncryptionKey::generate("new_master_key");
    let cross_key = master.cross_signing_key(id.user(), KeyUsage::Master);
    let raw_key = sign_ruma_object(id.user().as_str(), &cross_key, &[master.key_pair()]).unwrap();

    let input = upload_signing_keys::v3::Request {
        auth: Some(TestingApp::uiaa_password(id.user())),
        master_key: Some(raw_key),
        self_signing_key: None,
        user_signing_key: None,
    };
    cmd.execute(input, id.clone()).await.unwrap();

    // Check that user only has master keys
    let storage = app.service::<Arc<dyn KeyStorage>>();
    let keys = storage.user_cross_signing_keys(id.user()).await.unwrap();

    assert!(keys.master().is_some());
    assert!(keys.self_signing().is_none());
    assert!(keys.user_signing().is_none());
}
