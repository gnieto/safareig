use std::{collections::BTreeMap, sync::Arc};

use safareig_core::{
    command::Command,
    ruma::{
        api::client::keys::upload_keys, encryption::OneTimeKey, serde::Raw, DeviceId,
        DeviceKeyAlgorithm, DeviceKeyId, OwnedDeviceKeyId,
    },
};
use safareig_e2ee::{
    command::client::upload_keys::UploadKeysCommand, module::E2eeModule, storage::KeyStorage,
};
use safareig_testing::TestingApp;

#[tokio::test]
async fn upload_two_fallback_keys_for_same_user_and_algorithm_should_keep_only_the_latest() {
    let app = TestingApp::with_modules(vec![Box::new(E2eeModule {})]).await;
    let id = app.register().await;
    let first_fallback = generate_fallback();
    let mut first_map = BTreeMap::new();
    first_map.insert(first_fallback.0, first_fallback.1);
    let second_fallback = generate_fallback();
    let mut second_map = BTreeMap::new();
    second_map.insert(second_fallback.0.clone(), second_fallback.1);

    let input = upload_keys::v3::Request {
        device_keys: Default::default(),
        one_time_keys: Default::default(),
        fallback_keys: first_map,
    };
    let cmd = app.command::<UploadKeysCommand>();
    cmd.execute(input, id.to_owned()).await.unwrap();

    let input = upload_keys::v3::Request {
        device_keys: Default::default(),
        one_time_keys: Default::default(),
        fallback_keys: second_map,
    };
    let cmd = app.command::<UploadKeysCommand>();
    cmd.execute(input, id.to_owned()).await.unwrap();

    let key_storage = app.service::<Arc<dyn KeyStorage>>();
    let (key_id, _) = key_storage
        .claim_fallback_key(id.device().unwrap(), &DeviceKeyAlgorithm::Curve25519)
        .await
        .unwrap()
        .unwrap();

    assert_eq!(second_fallback.0, key_id);
}

fn generate_fallback() -> (OwnedDeviceKeyId, Raw<OneTimeKey>) {
    let device_id = DeviceId::new();
    let id = DeviceKeyId::from_parts(DeviceKeyAlgorithm::Curve25519, &device_id);
    let value = OneTimeKey::Key(device_id.to_string());

    (id, Raw::new(&value).unwrap())
}
