use std::{collections::BTreeMap, sync::Arc};

use safareig_core::{
    command::Command,
    ruma::{
        api::client::keys::claim_keys, device_id, encryption::OneTimeKey, serde::Raw, DeviceId,
        DeviceKeyAlgorithm, DeviceKeyId, OwnedDeviceId, OwnedDeviceKeyId, UserId,
    },
};
use safareig_e2ee::{command::client::claim_keys::ClaimKeysCommand, storage::KeyStorage};
use safareig_testing::TestingApp;

use crate::command::add_fallback_key;

#[tokio::test]
async fn it_can_claim_all_the_uploaded_keys() {
    let db = TestingApp::default().await;
    let user_upload = TestingApp::new_identity();
    let user_claim = TestingApp::new_identity();
    let key_storage = db.service::<Arc<dyn KeyStorage>>();
    let keys = 5;
    for i in 0..keys {
        let device = format!("SAF{i}");
        let device_upload: OwnedDeviceId = device.into();
        let algorithm = DeviceKeyAlgorithm::SignedCurve25519;
        let key_id = DeviceKeyId::from_parts(algorithm.clone(), &device_upload);

        let otk = OneTimeKey::Key(i.to_string());
        key_storage
            .add_one_time_key(&device_upload, &key_id, &otk)
            .await
            .unwrap();
    }

    let cmd = db.command::<ClaimKeysCommand>();

    for i in 0..keys {
        let device = format!("SAF{i}");
        let device_upload: OwnedDeviceId = device.into();
        let algorithm = DeviceKeyAlgorithm::SignedCurve25519;

        let request = prepare_request_for(user_upload.user(), &device_upload, &algorithm);
        let response = cmd.execute(request, user_claim.clone()).await.unwrap();
        let device_keys = obtain_otk_for(&response, user_upload.user(), &device_upload);
        assert_eq!(device_keys.len(), 1);
    }

    let device_upload = device_id!("SAF6");
    let algorithm = DeviceKeyAlgorithm::SignedCurve25519;

    // TODO: Assert more users?
    let request = prepare_request_for(user_upload.user(), device_upload, &algorithm);
    let response = cmd.execute(request, user_claim.clone()).await.unwrap();
    let device_keys = obtain_otk_for(&response, user_upload.user(), device_upload);
    assert_eq!(device_keys.len(), 0);
}

#[tokio::test]
async fn it_can_not_claim_keys_if_user_has_no_otk_for_given_algorithm() {
    let db = TestingApp::default().await;
    let user_upload = TestingApp::new_identity();
    let user_claim = TestingApp::new_identity();
    let key_storage = db.service::<Arc<dyn KeyStorage>>();
    let device_upload: OwnedDeviceId = "SAFAREIG".into();
    let algorithm = DeviceKeyAlgorithm::SignedCurve25519;
    let key_id = DeviceKeyId::from_parts(algorithm, &device_upload);
    let otk = OneTimeKey::Key("otk".to_string());
    key_storage
        .add_one_time_key(&device_upload, &key_id, &otk)
        .await
        .unwrap();

    let cmd = db.command::<ClaimKeysCommand>();

    let request = prepare_request_for(
        user_upload.user(),
        &device_upload,
        &DeviceKeyAlgorithm::Curve25519,
    );
    let response = cmd.execute(request, user_claim.clone()).await.unwrap();
    let device_keys = obtain_otk_for(&response, user_upload.user(), &device_upload);
    assert_eq!(device_keys.len(), 0);
}

#[tokio::test]
async fn it_can_not_claim_keys_if_user_has_no_otks() {
    let db = TestingApp::default().await;
    let user_upload = TestingApp::new_identity();
    let user_claim = TestingApp::new_identity();
    let device_upload: OwnedDeviceId = "SAFAREIG".into();

    let cmd = db.command::<ClaimKeysCommand>();

    let request = prepare_request_for(
        user_upload.user(),
        &device_upload,
        &DeviceKeyAlgorithm::Curve25519,
    );
    let response = cmd.execute(request, user_claim.clone()).await.unwrap();
    let device_keys = obtain_otk_for(&response, user_upload.user(), &device_upload);
    assert_eq!(device_keys.len(), 0);
}

#[tokio::test]
async fn it_uses_fallback_key_no_one_time_keys_left_for_target_user() {
    let db = TestingApp::default().await;
    let user_upload = TestingApp::new_identity();
    let fallback_key = add_fallback_key(&db, &user_upload).await;
    let user_claim = TestingApp::new_identity();
    let device_upload = user_upload.device().unwrap();

    let cmd = db.command::<ClaimKeysCommand>();

    let request = prepare_request_for(
        user_upload.user(),
        device_upload,
        &DeviceKeyAlgorithm::Curve25519,
    );
    let response = cmd.execute(request, user_claim.clone()).await.unwrap();
    let device_keys = obtain_otk_for(&response, user_upload.user(), device_upload);
    assert_eq!(1, device_keys.len());
    let used_key_id = device_keys.keys().next().unwrap();
    assert_eq!(&fallback_key, used_key_id);
}

fn prepare_request_for(
    user: &UserId,
    device: &DeviceId,
    algorithm: &DeviceKeyAlgorithm,
) -> claim_keys::v3::Request {
    let mut users = BTreeMap::new();
    let mut devices = BTreeMap::new();
    devices.insert(device.to_owned(), algorithm.clone());
    users.insert(user.to_owned(), devices);

    claim_keys::v3::Request {
        timeout: None,
        one_time_keys: users,
    }
}

fn obtain_otk_for<'a>(
    response: &'a claim_keys::v3::Response,
    user: &'a UserId,
    device: &'a DeviceId,
) -> BTreeMap<OwnedDeviceKeyId, Raw<OneTimeKey>> {
    let user_keys = response
        .one_time_keys
        .get(user)
        .cloned()
        .unwrap_or_default();
    let device_keys = user_keys.get(device).cloned().unwrap_or_default();

    device_keys
}
