use safareig_e2ee::command::federation::claim_keys::ClaimKeysCommand;
use safareig_testing::TestingApp;

#[tokio::test]
async fn it_can_claim_all_the_uploaded_keys() {
    let app = TestingApp::default().await;

    let _cmd = app.command::<ClaimKeysCommand>();
}
