use safareig_core::{
    command::Command,
    ruma::{
        api::client::backup::{
            add_backup_keys_for_session, create_backup_version, delete_backup_version,
            BackupAlgorithm, EncryptedSessionData, KeyBackupData,
        },
        serde::{base64::Standard, Base64, Raw},
        RoomId, UInt,
    },
};
use safareig_e2ee::backup::command::{
    CreateBackupVersionCommand, DeleteBackupVersionCommand, UpdateBackupSessionKeysCommand,
};
use safareig_testing::TestingApp;

#[tokio::test]
async fn remove_key_backup() {
    let db = TestingApp::default().await;
    let id = db.register().await;
    let cmd = db.command::<CreateBackupVersionCommand>();
    let request = create_backup_version::v3::Request {
        algorithm: create_backup(),
    };
    let room_id = RoomId::new(&db.server_name());

    let response = cmd.execute(request, id.clone()).await.unwrap();
    let backup_version = response.version;

    // Update backup session key so backup is not empty
    let cmd = db.command::<UpdateBackupSessionKeysCommand>();
    let key_backup = KeyBackupData {
        first_message_index: UInt::new_wrapping(1),
        forwarded_count: UInt::new_wrapping(1),
        is_verified: true,
        session_data: EncryptedSessionData {
            ephemeral: string_as_base64("ephemeral"),
            ciphertext: string_as_base64("ciphertext"),
            mac: string_as_base64("mac"),
        },
    };
    let request = add_backup_keys_for_session::v3::Request {
        version: backup_version.clone(),
        room_id,
        session_id: "test".to_string(),
        session_data: Raw::new(&key_backup).unwrap(),
    };
    cmd.execute(request, id.clone()).await.unwrap();

    // Removing the backup should recursively remove all backup
    let cmd = db.command::<DeleteBackupVersionCommand>();

    let request = delete_backup_version::v3::Request {
        version: backup_version.clone(),
    };
    cmd.execute(request, id.clone()).await.unwrap();
}

fn create_backup() -> Raw<BackupAlgorithm> {
    let public_key = string_as_base64("public_key");
    let algorithm = BackupAlgorithm::MegolmBackupV1Curve25519AesSha2 {
        public_key,
        signatures: Default::default(),
    };

    Raw::new(&algorithm).unwrap()
}

fn string_as_base64(str: &str) -> Base64 {
    safareig_core::ruma::serde::Base64::<Standard, _>::new(str.as_bytes().to_vec())
}
