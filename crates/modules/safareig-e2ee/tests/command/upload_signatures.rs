use std::{collections::BTreeMap, sync::Arc};

use safareig_core::{
    command::Command,
    ruma::api::client::keys::{
        get_keys,
        upload_signatures::{self, v3::SignedKeys},
    },
};
use safareig_e2ee::{
    command::client::{query_keys::QueryKeysCommand, UploadSignaturesCommand},
    module::E2eeModule,
    storage::KeyStorage,
};
use safareig_federation::keys::sign_ruma_object;
use safareig_testing::TestingApp;

use crate::command::{set_user_cross_keys, upload_device_key};

#[tokio::test]
async fn upload_signatures() {
    let app = TestingApp::with_modules(vec![Box::new(E2eeModule {})]).await;
    let alice = app.register().await;
    let alice_cross_keys = set_user_cross_keys(&app, &alice).await;
    let _alice_device_key = upload_device_key(&app, &alice, alice.device().unwrap()).await;

    let bob = app.register().await;
    let _bob_cross_keys = set_user_cross_keys(&app, &bob).await;

    let storage = app.service::<Arc<dyn KeyStorage>>();
    let keys = storage.get_keys(alice.user(), None).await.unwrap();
    let bob_keys = storage.user_cross_signing_keys(bob.user()).await.unwrap();

    let mut signed_keys = BTreeMap::new();
    let alice_entry: &mut SignedKeys = signed_keys.entry(alice.user().to_owned()).or_default();

    for key in keys {
        let entity = alice.user().as_str();
        let signed_device_key =
            sign_ruma_object(entity, &key, &[alice_cross_keys.master.key_pair()]).unwrap();
        alice_entry.add_device_keys(key.device_id, signed_device_key);
    }

    let bob_entry = signed_keys.entry(bob.user().to_owned()).or_default();
    let bob_master_key = bob_keys.master().unwrap();
    let bob_signed_master_key = sign_ruma_object(
        alice.user().as_str(),
        &bob_master_key,
        &[alice_cross_keys.master.key_pair()],
    )
    .unwrap();
    let cross_id = bob_master_key
        .deserialize()
        .unwrap()
        .keys
        .iter()
        .next()
        .unwrap()
        .0
        .to_string()
        .into_boxed_str();
    bob_entry.add_cross_signing_keys(cross_id, bob_signed_master_key);

    let request = upload_signatures::v3::Request { signed_keys };
    let cmd = app.command::<UploadSignaturesCommand>();
    let _response = cmd.execute(request, alice.to_owned()).await.unwrap();

    let cmd = app.command::<QueryKeysCommand>();
    let mut alice_req_keys = BTreeMap::new();
    alice_req_keys.insert(alice.user().to_owned(), Default::default());
    let alice_request = get_keys::v3::Request {
        timeout: None,
        device_keys: alice_req_keys.clone(),
    };
    let alice_keys_response = cmd.execute(alice_request, alice.clone()).await.unwrap();
    let device_key = alice_keys_response
        .device_keys
        .get(alice.user())
        .unwrap()
        .iter()
        .next()
        .unwrap()
        .1
        .deserialize()
        .unwrap();
    assert_eq!(2, device_key.signatures.get(alice.user()).unwrap().len());
    let master_name = alice_cross_keys.master.key_id();
    assert!(device_key
        .signatures
        .get(alice.user())
        .unwrap()
        .contains_key(&master_name));

    let bob_request = get_keys::v3::Request {
        timeout: None,
        device_keys: alice_req_keys,
    };
    let bob_keys_response = cmd.execute(bob_request, bob.clone()).await.unwrap();
    let device_key = bob_keys_response
        .device_keys
        .get(alice.user())
        .unwrap()
        .iter()
        .next()
        .unwrap()
        .1
        .deserialize()
        .unwrap();
    assert_eq!(2, device_key.signatures.get(alice.user()).unwrap().len());
}
