use std::collections::BTreeMap;

use safareig_core::{
    auth::Identity,
    command::Command,
    ruma::{
        api::client::keys::{upload_keys as device_upload_keys, upload_signing_keys},
        encryption::{CrossSigningKey, DeviceKeys, KeyUsage, OneTimeKey},
        serde::{base64::Standard, Raw},
        signatures::Ed25519KeyPair,
        DeviceId, DeviceKeyAlgorithm, DeviceKeyId, EventEncryptionAlgorithm, OwnedDeviceId,
        OwnedDeviceKeyId, UserId,
    },
};
use safareig_e2ee::command::client::{upload_keys::UploadKeysCommand, UploadUserKeysCommand};
use safareig_federation::keys::sign_ruma_object;
use safareig_testing::TestingApp;
use safareig_uiaa::UiaaWrapper;

mod backup_delete;
mod client_claim_keys;
mod client_query_keys;
mod client_update_keys;
mod federation_claim_keys;
mod upload_keys;
mod upload_signatures;

pub struct EncryptionKey {
    pair: Ed25519KeyPair,
    key_name: String,
}

impl EncryptionKey {
    pub fn generate(name: &str) -> Self {
        let master_key = Ed25519KeyPair::generate().unwrap();
        let key = Ed25519KeyPair::from_der(&master_key, name.to_owned()).unwrap();

        Self {
            pair: key,
            key_name: name.to_string(),
        }
    }

    pub fn cross_signing_key(&self, user_id: &UserId, usage: KeyUsage) -> CrossSigningKey {
        let public_key = self.pair.public_key();
        let mut keys = BTreeMap::new();
        let b64 = safareig_core::ruma::serde::Base64::<Standard, _>::new(public_key.to_vec());
        let key_id = self.key_id();
        keys.insert(key_id, b64.encode());

        CrossSigningKey::new(user_id.to_owned(), vec![usage], keys, BTreeMap::new())
    }

    pub fn key_id(&self) -> OwnedDeviceKeyId {
        let device_id: OwnedDeviceId = self.key_name.clone().into();

        DeviceKeyId::from_parts(DeviceKeyAlgorithm::Ed25519, &device_id)
    }

    pub fn key_pair(&self) -> &Ed25519KeyPair {
        &self.pair
    }
}

pub async fn set_user_cross_keys(app: &TestingApp, id: &Identity) -> TestingCrossKeys {
    let master = EncryptionKey::generate("master_key");
    let cross_key = master.cross_signing_key(id.user(), KeyUsage::Master);
    let raw_key = sign_ruma_object(id.user().as_str(), &cross_key, &[master.key_pair()]).unwrap();

    let self_signing = EncryptionKey::generate("self_signing");
    let cross_self = self_signing.cross_signing_key(id.user(), KeyUsage::SelfSigning);
    let raw_self = sign_ruma_object(id.user().as_str(), &cross_self, &[master.key_pair()]).unwrap();

    let user_signing = EncryptionKey::generate("user_signing");
    let cross_user = user_signing.cross_signing_key(id.user(), KeyUsage::UserSigning);
    let raw_user = sign_ruma_object(id.user().as_str(), &cross_user, &[master.key_pair()]).unwrap();

    let cmd = app.command::<UiaaWrapper<UploadUserKeysCommand>>();
    let input = upload_signing_keys::v3::Request {
        auth: Some(TestingApp::uiaa_password(id.user())),
        master_key: Some(raw_key),
        self_signing_key: Some(raw_self),
        user_signing_key: Some(raw_user),
    };
    cmd.execute(input, id.clone()).await.unwrap();

    TestingCrossKeys {
        master,
        self_signing,
        user_signing,
    }
}

pub async fn upload_device_key(
    app: &TestingApp,
    id: &Identity,
    device_id: &DeviceId,
) -> EncryptionKey {
    let cmd = app.command::<UploadKeysCommand>();
    let key = EncryptionKey::generate(device_id.as_str());
    let device_id: OwnedDeviceId = device_id.into();
    let key_id = DeviceKeyId::from_parts(DeviceKeyAlgorithm::Ed25519, device_id.as_ref());

    let mut public_keys = BTreeMap::new();
    let b64 =
        safareig_core::ruma::serde::Base64::<Standard, _>::new(key.pair.public_key().to_vec());
    public_keys.insert(key_id, b64.to_string());

    let device_key = DeviceKeys {
        user_id: id.user().to_owned(),
        device_id,
        algorithms: vec![EventEncryptionAlgorithm::MegolmV1AesSha2],
        keys: public_keys,
        signatures: Default::default(),
        unsigned: Default::default(),
    };

    let device_key = sign_ruma_object(id.user().as_str(), &device_key, &[&key.pair]).unwrap();

    let input = device_upload_keys::v3::Request {
        device_keys: Some(device_key),
        one_time_keys: Default::default(),
        fallback_keys: Default::default(),
    };
    cmd.execute(input, id.clone()).await.unwrap();

    key
}

pub async fn add_one_time_key(app: &TestingApp, id: &Identity) {
    let cmd = app.command::<UploadKeysCommand>();

    let mut one_time_keys = BTreeMap::new();
    let key_name = DeviceId::new();
    let key1 = DeviceKeyId::from_parts(DeviceKeyAlgorithm::Curve25519, &key_name);
    let key_value = OneTimeKey::Key("patata".to_string());
    one_time_keys.insert(key1, Raw::new(&key_value).unwrap());

    let input = device_upload_keys::v3::Request {
        device_keys: None,
        one_time_keys,
        fallback_keys: Default::default(),
    };
    cmd.execute(input, id.clone()).await.unwrap();
}

pub async fn add_fallback_key(app: &TestingApp, id: &Identity) -> OwnedDeviceKeyId {
    let cmd = app.command::<UploadKeysCommand>();

    let mut fallback_keys = BTreeMap::new();
    let key_name = DeviceId::new();
    let key1 = DeviceKeyId::from_parts(DeviceKeyAlgorithm::Curve25519, &key_name);
    let key_value = OneTimeKey::Key("patata".to_string());
    fallback_keys.insert(key1.clone(), Raw::new(&key_value).unwrap());

    let input = device_upload_keys::v3::Request {
        device_keys: None,
        one_time_keys: Default::default(),
        fallback_keys,
    };
    cmd.execute(input, id.clone()).await.unwrap();

    key1
}

pub struct TestingCrossKeys {
    pub master: EncryptionKey,
    pub self_signing: EncryptionKey,
    pub user_signing: EncryptionKey,
}
