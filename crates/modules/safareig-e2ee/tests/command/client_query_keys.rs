use std::{collections::BTreeMap, sync::Arc};

use safareig_core::{
    command::Command,
    ruma::{
        api::client::keys::get_keys, encryption::OneTimeKey, DeviceId, DeviceKeyAlgorithm,
        DeviceKeyId,
    },
};
use safareig_e2ee::{
    command::client::query_keys::QueryKeysCommand, module::E2eeModule, storage::KeyStorage,
};
use safareig_testing::TestingApp;

use crate::command::set_user_cross_keys;

#[tokio::test]
async fn local_key_handler_query_should_load_known_devices() {
    let app = TestingApp::default().await;
    let id = app.register().await;
    app.upload_keys(&id).await;

    let query = app.command::<QueryKeysCommand>();
    let mut device_keys = BTreeMap::new();
    device_keys.insert(id.user().to_owned(), Vec::new());
    let request = get_keys::v3::Request {
        timeout: None,
        device_keys,
    };
    let response = query.execute(request, id.to_owned()).await.unwrap();

    assert_eq!(1, response.device_keys.len());
}

#[tokio::test]
async fn it_can_count_distinct_keys() {
    let app = TestingApp::default().await;
    let storage = app.service::<Arc<dyn KeyStorage>>();
    let device1 = DeviceId::new();
    let otk = OneTimeKey::Key("".to_string());
    let key_id_1_1 = DeviceKeyId::from_parts(DeviceKeyAlgorithm::SignedCurve25519, &device1);
    let key_id_1_2 = DeviceKeyId::from_parts(DeviceKeyAlgorithm::Ed25519, &device1);

    storage
        .add_one_time_key(&device1, &key_id_1_1, &otk)
        .await
        .unwrap();
    storage
        .add_one_time_key(&device1, &key_id_1_1, &otk)
        .await
        .unwrap();
    storage
        .add_one_time_key(&device1, &key_id_1_1, &otk)
        .await
        .unwrap();
    storage
        .add_one_time_key(&device1, &key_id_1_1, &otk)
        .await
        .unwrap();

    storage
        .add_one_time_key(&device1, &key_id_1_2, &otk)
        .await
        .unwrap();
    storage
        .add_one_time_key(&device1, &key_id_1_2, &otk)
        .await
        .unwrap();
    storage
        .add_one_time_key(&device1, &key_id_1_2, &otk)
        .await
        .unwrap();

    let counts = storage.pending_keys(&device1).await.unwrap();
    assert_eq!(2, counts.len());
    assert_eq!(
        4,
        *counts.get(&DeviceKeyAlgorithm::SignedCurve25519).unwrap()
    );
    assert_eq!(3, *counts.get(&DeviceKeyAlgorithm::Ed25519).unwrap());
}

// TODO: Add test for fetching remote keys
#[tokio::test]
async fn remote_keys_with_a_failing_remote_should_be_returned_as_failures() {}

#[tokio::test]
async fn query_self_keys_should_return_user_signing_keys() {
    let app = TestingApp::with_modules(vec![Box::new(E2eeModule {})]).await;
    let id = app.register().await;
    set_user_cross_keys(&app, &id).await;

    let query = app.command::<QueryKeysCommand>();
    let mut device_keys = BTreeMap::new();
    device_keys.insert(id.user().to_owned(), Vec::new());
    let request = get_keys::v3::Request {
        timeout: None,
        device_keys,
    };
    let response = query.execute(request, id.to_owned()).await.unwrap();

    let user_id = id.user().to_owned();
    assert!(response.user_signing_keys.contains_key(&user_id));
    assert!(response.master_keys.contains_key(&user_id));
    assert!(response.self_signing_keys.contains_key(&user_id));
}

#[tokio::test]
async fn another_user_querying_user_keys_should_noy_return_user_signing_keys() {
    let app = TestingApp::with_modules(vec![Box::new(E2eeModule {})]).await;
    let id = app.register().await;
    set_user_cross_keys(&app, &id).await;
    let another_user = app.register().await;

    let query = app.command::<QueryKeysCommand>();
    let mut device_keys = BTreeMap::new();
    device_keys.insert(id.user().to_owned(), Vec::new());
    let request = get_keys::v3::Request {
        timeout: None,
        device_keys,
    };
    let response = query.execute(request, another_user.clone()).await.unwrap();

    let user_id = id.user().to_owned();
    assert!(!response.user_signing_keys.contains_key(&user_id));
    assert!(response.master_keys.contains_key(&user_id));
    assert!(response.self_signing_keys.contains_key(&user_id));
}
