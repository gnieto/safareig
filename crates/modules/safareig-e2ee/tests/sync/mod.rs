use std::{collections::HashSet, sync::Arc};

use safareig_core::{
    ruma::{
        api::client::{filter::FilterDefinition, sync::sync_events},
        DeviceKeyAlgorithm,
    },
    StreamToken,
};
use safareig_e2ee::{
    module::E2eeModule,
    storage::{E2eeStreamStorage, KeyStorage},
    sync::E2eeSyncExtension,
};
use safareig_sync_api::{
    context::{Context, UserRoomsMemberhip},
    SyncExtension, SyncToken,
};
use safareig_testing::TestingApp;

use crate::command::{add_fallback_key, add_one_time_key};

#[tokio::test]
async fn it_syncs_one_time_key_count() {
    let app = TestingApp::with_modules(vec![Box::new(E2eeModule {})]).await;
    let id = app.register().await;

    add_one_time_key(&app, &id).await;
    add_one_time_key(&app, &id).await;
    add_one_time_key(&app, &id).await;
    add_fallback_key(&app, &id).await;

    let stream = app.service::<Arc<dyn E2eeStreamStorage>>();
    let keys = app.service::<Arc<dyn KeyStorage>>();
    let sync = E2eeSyncExtension::new(stream, keys);
    let mut response = sync_events::v3::Response::new("".to_string());
    let mut token = SyncToken::default();
    let request = sync_events::v3::Request::new();

    let context = Context {
        request: &request,
        from_token: token.clone(),
        to_event_token: StreamToken::horizon(),
        id: &id,
        filter: FilterDefinition::default(),
        sync_mode: safareig_sync_api::context::SyncMode::FullState,
        user_rooms: UserRoomsMemberhip::default(),
        ignored: HashSet::new(),
    };

    sync.fill_sync(&context, &mut response, &mut token)
        .await
        .unwrap();

    let actual_curve_otks = response
        .device_one_time_keys_count
        .remove(&safareig_core::ruma::DeviceKeyAlgorithm::Curve25519)
        .unwrap();
    assert_eq!(3, u32::try_from(actual_curve_otks).unwrap());
    assert_eq!(
        vec![DeviceKeyAlgorithm::Curve25519],
        response.device_unused_fallback_key_types.unwrap()
    );
}

#[tokio::test]
async fn sync_after_using_a_fallback_key_should_return_an_empty_response() {
    let app = TestingApp::with_modules(vec![Box::new(E2eeModule {})]).await;
    let id = app.register().await;
    add_fallback_key(&app, &id).await;

    let stream = app.service::<Arc<dyn E2eeStreamStorage>>();
    let keys = app.service::<Arc<dyn KeyStorage>>();
    let sync = E2eeSyncExtension::new(stream, keys.clone());
    let mut response = sync_events::v3::Response::new("".to_string());
    let mut token = SyncToken::default();
    let request = sync_events::v3::Request::new();

    let context = Context {
        request: &request,
        from_token: token.clone(),
        to_event_token: StreamToken::horizon(),
        id: &id,
        filter: FilterDefinition::default(),
        sync_mode: safareig_sync_api::context::SyncMode::FullState,
        user_rooms: UserRoomsMemberhip::default(),
        ignored: HashSet::new(),
    };

    keys.claim_fallback_key(id.device().unwrap(), &DeviceKeyAlgorithm::Curve25519)
        .await
        .unwrap();

    sync.fill_sync(&context, &mut response, &mut token)
        .await
        .unwrap();

    assert!(response
        .device_unused_fallback_key_types
        .unwrap()
        .is_empty());
}
