use safareig_core::{
    command::Command,
    ruma::{api::client::room::aliases, RoomAliasId},
};
use safareig_rooms::alias::command::RoomAliasesCommand;
use safareig_testing::TestingApp;

use crate::command::alias::create_alias;

#[tokio::test]
async fn it_lists_all_room_aliases_on_world_readable_rooms() {
    let db = TestingApp::default().await;
    let (id, room) = db.public_room().await;
    let alias = RoomAliasId::parse("#new_alias:test.cat").unwrap();
    create_alias(&db, room.id(), &id, &alias).await;
    let cmd = db.command::<RoomAliasesCommand>();
    let request = aliases::v3::Request {
        room_id: room.id().to_owned(),
    };

    let result = cmd.execute(request, id).await.unwrap();

    assert_eq!(2, result.aliases.len());
    assert_eq!(result.aliases[1].alias(), "new_alias");
}

// TODO: Uncomment once validations get restored
/*
#[tokio::test]
async fn denies_aliases_list_if_not_world_readable_and_not_joined() {
    let db = TestingApp::default().await;
    let (id, room) = db.private_room().await;
    let new_id = TestingApp::new_identity();
    let alias = RoomAliasId::parse("#new_alias:test.cat").unwrap();
    create_alias(&db, room.id(), &new_id, &alias);
    let cmd = db.command::<RoomAliasesCommand>();
    let request = aliases::v3::Request {
        room_id: room.id().to_owned(),
    };

    let result = cmd.execute(request, new_id).await;

    assert!(result.is_err());
    let err = result.err().unwrap();
    assert_eq!(StatusCode::FORBIDDEN, err.status_code);
}
*/

#[tokio::test]
async fn lists_room_aliases_if_not_world_readable_but_is_joined() {
    let db = TestingApp::default().await;
    let (id, room) = db.private_room().await;
    let alias = RoomAliasId::parse("#new_alias:test.cat").unwrap();
    create_alias(&db, room.id(), &id, &alias).await;
    let cmd = db.command::<RoomAliasesCommand>();
    let request = aliases::v3::Request {
        room_id: room.id().to_owned(),
    };

    let result = cmd.execute(request, id).await.unwrap();

    assert_eq!(2, result.aliases.len());
    assert_eq!(result.aliases[1].alias(), "new_alias");
}
