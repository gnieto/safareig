use std::sync::Arc;

use safareig_core::{
    command::Command,
    ruma::{api::client::alias::delete_alias, exports::http::StatusCode, RoomAliasId},
};
use safareig_rooms::alias::{command::RemoveAliasCommand, storage::AliasStorage};
use safareig_testing::TestingApp;

use crate::command::alias::create_alias;

#[tokio::test]
async fn it_removes_an_alias_if_user_is_allowed() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let alias = RoomAliasId::parse("#new_alias:test.cat").unwrap();
    create_alias(&db, room.id(), &creator, &alias).await;
    let cmd = db.command::<RemoveAliasCommand>();

    let request = delete_alias::v3::Request {
        room_alias: alias.clone(),
    };

    let _ = cmd.execute(request, creator).await.unwrap();

    let storage = db.service::<Arc<dyn AliasStorage>>();
    let alias = storage.get_alias(&alias).await.unwrap();

    assert!(alias.is_none());
}

#[tokio::test]
async fn it_returns_forbidden_if_active_user_is_not_the_creator() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let alias = RoomAliasId::parse("#new_alias:test.cat").unwrap();
    create_alias(&db, room.id(), &creator, &alias).await;
    let another_user = TestingApp::new_identity();

    let cmd = db.command::<RemoveAliasCommand>();

    let request = delete_alias::v3::Request {
        room_alias: alias.clone(),
    };

    let result = cmd.execute(request, another_user).await;
    assert!(result.is_err());
    let error = result.err().unwrap();
    assert_eq!(StatusCode::FORBIDDEN, error.status_code);
}

#[tokio::test]
async fn it_returns_not_found_on_non_existing_alias() {
    let db = TestingApp::default().await;
    let user = TestingApp::new_identity();
    let alias = RoomAliasId::parse("#nonexisting:test.cat").unwrap();
    let cmd = db.command::<RemoveAliasCommand>();

    let request = delete_alias::v3::Request {
        room_alias: alias.clone(),
    };
    let result = cmd.execute(request, user).await;

    assert!(result.is_err());
    let error = result.err().unwrap();
    assert_eq!(StatusCode::NOT_FOUND, error.status_code);
}
