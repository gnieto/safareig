use std::sync::Arc;

use safareig_core::{
    command::Command,
    ruma::{
        api::client::{alias::create_alias, error::ErrorKind},
        exports::http::StatusCode,
        RoomAliasId,
    },
};
use safareig_rooms::alias::{command::CreateAliasCommand, storage::AliasStorage};
use safareig_testing::{error::RumaErrorExtension, TestingApp};

#[tokio::test]
async fn it_can_add_a_new_alias_to_a_room() {
    let app = TestingApp::default().await;
    let (creator, room) = app.public_room().await;
    let alias = RoomAliasId::parse("#new_alias:test.cat").unwrap();
    let cmd = app.command::<CreateAliasCommand>();

    let request = create_alias::v3::Request {
        room_alias: alias.clone(),
        room_id: room.id().to_owned(),
    };

    let _ = cmd.execute(request, creator).await.unwrap();

    let storage = app.service::<Arc<dyn AliasStorage>>();
    let room_alias = storage.get_alias(&alias).await.unwrap();

    assert!(room_alias.is_some());
    assert_eq!(room_alias.unwrap().alias, alias);
}

#[tokio::test]
async fn it_refuses_to_create_same_alias_twice() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let alias = RoomAliasId::parse("#new_alias:test.cat").unwrap();
    let cmd = db.command::<CreateAliasCommand>();

    // Create the alias for first time
    let request = create_alias::v3::Request {
        room_alias: alias.clone(),
        room_id: room.id().to_owned(),
    };
    cmd.execute(request, creator.clone()).await.unwrap();

    let request = create_alias::v3::Request {
        room_alias: alias.clone(),
        room_id: room.id().to_owned(),
    };
    let result = cmd.execute(request, creator).await;

    let err = result.err().unwrap();
    assert_eq!(StatusCode::CONFLICT, err.status_code);
    assert_eq!(ErrorKind::Unknown, err.kind());
}
