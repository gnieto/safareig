mod create_alias;
mod remove_alias;
mod room_aliases;

use std::sync::Arc;

use safareig_core::{
    auth::Identity,
    command::Command,
    ruma::{
        api::federation::query::get_room_information::v1 as get_room_information,
        exports::http::StatusCode, OwnedRoomId, RoomAliasId, RoomId,
    },
};
use safareig_federation::{client::ClientError, FederationError};
use safareig_rooms::alias::{command::CreateAliasCommand, error::AliasError, AliasResolver};
use safareig_testing::{federation::response_template_from, TestingApp};
use wiremock::{
    matchers::{method, path},
    Mock, ResponseTemplate,
};

#[tokio::test]
async fn it_loads_alias_from_remote_server() {
    let (db, mock) = TestingApp::with_mocked_federation().await;
    let alias_resolver = db.service::<Arc<dyn AliasResolver>>();
    let room_id: OwnedRoomId = "!asdfasf:localhost:8448".parse().unwrap();

    Mock::given(method("GET"))
        .and(path("/_matrix/federation/v1/query/directory"))
        .respond_with(ok_alias_response(&room_id))
        .mount(&mock)
        .await;

    let alias_info = alias_resolver
        .resolve_alias(&RoomAliasId::parse("#alias:remote.test").unwrap())
        .await
        .unwrap();

    assert_eq!(room_id, alias_info.room_id);
}

#[tokio::test]
async fn it_returns_404_if_remote_alias_do_not_exist() {
    let (db, mock) = TestingApp::with_mocked_federation().await;
    let alias_resolver = db.service::<Arc<dyn AliasResolver>>();

    Mock::given(method("GET"))
        .and(path("/_matrix/federation/v1/query/directory"))
        .respond_with(ResponseTemplate::new(StatusCode::NOT_FOUND))
        .mount(&mock)
        .await;

    let alias = RoomAliasId::parse("#alias:remote.test").unwrap();
    let error = alias_resolver.resolve_alias(&alias).await.err().unwrap();

    match error {
        AliasError::FederationError(FederationError::Client(ClientError::RumaError(re))) => {
            assert_eq!(re.status_code, StatusCode::NOT_FOUND);
        }
        _ => unreachable!(""),
    }
}

#[tokio::test]
async fn it_returns_an_alias_after_creating_a_public_room() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let alias_resolver = db.service::<Arc<dyn AliasResolver>>();

    let expected_alias = format!("#{}:test.cat", creator.user().localpart());
    let alias = RoomAliasId::parse(expected_alias).unwrap();

    let response = alias_resolver.resolve_alias(&alias).await.unwrap();

    assert_eq!(response.room_id, room.id());
}

fn ok_alias_response(room_id: &RoomId) -> ResponseTemplate {
    let response = get_room_information::Response {
        room_id: room_id.to_owned(),
        servers: vec![],
    };

    response_template_from(response)
}

pub async fn create_alias(app: &TestingApp, room_id: &RoomId, id: &Identity, alias: &RoomAliasId) {
    let cmd = app.command::<CreateAliasCommand>();

    let request = safareig_core::ruma::api::client::alias::create_alias::v3::Request {
        room_alias: alias.to_owned(),
        room_id: room_id.to_owned(),
    };
    cmd.execute(request, id.clone()).await.unwrap();
}
