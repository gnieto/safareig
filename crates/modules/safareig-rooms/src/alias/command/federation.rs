use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity, command::Command,
    ruma::api::federation::query::get_room_information::v1 as get_room_information, Inject,
};

use crate::alias::AliasResolver;

#[derive(Inject)]
pub struct GetAliasFederationCommand {
    resolver: Arc<dyn AliasResolver>,
}

#[async_trait]
impl Command for GetAliasFederationCommand {
    type Input = get_room_information::Request;
    type Output = get_room_information::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;

    async fn execute(&self, input: Self::Input, _: Identity) -> Result<Self::Output, Self::Error> {
        let alias_info = self.resolver.resolve_alias(&input.room_alias).await?;
        let response = get_room_information::Response::new(alias_info.room_id, alias_info.servers);

        Ok(response)
    }
}
