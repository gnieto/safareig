use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity, command::Command, ruma::api::client::alias::get_alias, Inject,
};

use crate::alias::AliasResolver;

#[derive(Inject)]
pub struct GetAliasCommand {
    alias_lookup: Arc<dyn AliasResolver>,
}

#[async_trait]
impl Command for GetAliasCommand {
    type Input = get_alias::v3::Request;
    type Output = get_alias::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, _: Identity) -> Result<Self::Output, Self::Error> {
        let alias_info = self.alias_lookup.resolve_alias(&input.room_alias).await?;
        let response = get_alias::v3::Response::new(alias_info.room_id, alias_info.servers);

        Ok(response)
    }
}
