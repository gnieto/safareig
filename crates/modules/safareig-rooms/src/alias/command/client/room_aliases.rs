use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{auth::Identity, command::Command, ruma::api::client::room::aliases, Inject};

use crate::alias::storage::AliasStorage;

#[derive(Inject)]
pub struct RoomAliasesCommand {
    alias: Arc<dyn AliasStorage>,
    // room: RoomLoader,
}

#[async_trait]
impl Command for RoomAliasesCommand {
    type Input = aliases::v3::Request;
    type Output = aliases::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(
        &self,
        input: Self::Input,
        _id: Identity,
    ) -> Result<Self::Output, Self::Error> {
        // TODO: Recover this validation
        /*let room = self
            .room
            .get(&input.room_id)
            .await?
            .ok_or(ResourceNotFound)?;

        if !self.can_user_list_aliases(&room, &id).await? {
            return Err(safareig_core::ruma::api::client::Error {
                kind: ErrorKind::forbidden(),
                message: "room is not world readable and user is not member of the room"
                    .to_string(),
                status_code: StatusCode::FORBIDDEN,
            });
        }
        */

        let aliases = self
            .alias
            .room_aliases(&input.room_id)
            .await?
            .into_iter()
            .map(|room_alias| room_alias.alias)
            .collect();

        Ok(aliases::v3::Response { aliases })
    }
}

/*
impl RoomAliasesCommand {
    async fn can_user_list_aliases(
        &self,
        room: &Room,
        id: &Identity,
    ) -> Result<bool, safareig_core::ruma::api::client::Error> {
        let state = room.state_at_leaves().await?;

        let history_visibility = room
            .history_visibility()
            .await
            .map(|history| history.content.history_visibility)
            .unwrap_or(HistoryVisibility::Joined);

        Ok(match history_visibility {
            HistoryVisibility::WorldReadable => true,
            _ => {
                let membership = room
                    .membership(&state, id.user())
                    .await
                    .ok()
                    .map(|membership| membership.content.membership)
                    .unwrap_or(MembershipState::Leave);

                matches!(membership, MembershipState::Join)
            }
        })
    }
}
*/
