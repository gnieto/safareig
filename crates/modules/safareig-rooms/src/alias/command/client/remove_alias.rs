use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    command::Command,
    error::ResourceNotFound,
    ruma::{
        api::client::{
            alias::delete_alias,
            error::{ErrorBody, ErrorKind},
        },
        exports::http::StatusCode,
    },
    Inject,
};

use crate::alias::storage::AliasStorage;

#[derive(Inject)]
pub struct RemoveAliasCommand {
    alias: Arc<dyn AliasStorage>,
}

#[async_trait]
impl Command for RemoveAliasCommand {
    type Input = delete_alias::v3::Request;
    type Output = delete_alias::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let room_alias = self
            .alias
            .get_alias(&input.room_alias)
            .await?
            .ok_or(ResourceNotFound)?;

        if room_alias.creator != id.user() {
            return Err(safareig_core::ruma::api::client::Error {
                status_code: StatusCode::FORBIDDEN,
                body: ErrorBody::Standard {
                    kind: ErrorKind::forbidden(),
                    message: "users only can remove alias they created".to_string(),
                },
            });
        }

        let _ = self.alias.remove_alias(&input.room_alias).await?;

        Ok(delete_alias::v3::Response {})
    }
}
