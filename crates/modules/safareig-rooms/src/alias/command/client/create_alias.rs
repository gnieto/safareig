use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity, command::Command, ruma::api::client::alias::create_alias, Inject,
};

use crate::alias::storage::{AliasStorage, RoomAlias};

#[derive(Inject)]
pub struct CreateAliasCommand {
    alias: Arc<dyn AliasStorage>,
}

#[async_trait]
impl Command for CreateAliasCommand {
    type Input = create_alias::v3::Request;
    type Output = create_alias::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        // TODO: Check that is not possible to set an alias for a non-existing room

        let room_alias = RoomAlias {
            room_id: input.room_id.to_owned(),
            creator: id.user().to_owned(),
            alias: input.room_alias.clone(),
        };

        self.alias.set_alias(room_alias, false).await?;

        Ok(create_alias::v3::Response {})
    }
}
