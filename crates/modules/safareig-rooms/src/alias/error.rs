use safareig_core::{
    error::RumaExt,
    ruma::{
        api::client::error::{ErrorBody, ErrorKind},
        exports::http::StatusCode,
        OwnedRoomAliasId,
    },
    storage::StorageError,
};
use safareig_federation::FederationError;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum AliasError {
    #[error("Room not found: {0}")]
    AliasNotFound(OwnedRoomAliasId),
    #[error("Federation error")]
    FederationError(#[from] FederationError),
    #[error("Storage error")]
    StorageError(#[from] StorageError),
}

impl From<AliasError> for safareig_core::ruma::api::client::Error {
    fn from(error: AliasError) -> Self {
        match error {
            AliasError::AliasNotFound(alias) => safareig_core::ruma::api::client::Error {
                status_code: StatusCode::NOT_FOUND,
                body: ErrorBody::Standard {
                    kind: ErrorKind::NotFound,
                    message: format!("alias {alias} not found"),
                },
            },
            AliasError::FederationError(e) => e.into(),
            AliasError::StorageError(e) => e.into(),
        }
    }
}

impl From<AliasError> for safareig_core::ruma::api::error::MatrixError {
    fn from(error: AliasError) -> Self {
        let client = safareig_core::ruma::api::client::Error::from(error);
        client.to_generic()
    }
}
