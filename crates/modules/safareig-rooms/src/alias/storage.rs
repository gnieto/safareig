#[cfg(feature = "backend-sqlx")]
pub mod sqlx;

use safareig_core::{
    ruma::{OwnedRoomAliasId, OwnedRoomId, OwnedUserId, RoomAliasId, RoomId},
    storage::StorageError,
};

#[async_trait::async_trait]
pub trait AliasStorage: Send + Sync {
    async fn get_alias(&self, alias: &RoomAliasId) -> Result<Option<RoomAlias>, StorageError>;
    async fn room_aliases(&self, room_id: &RoomId) -> Result<Vec<RoomAlias>, StorageError>;
    async fn set_alias(
        &self,
        room_alias: RoomAlias,
        allow_replace: bool,
    ) -> Result<(), StorageError>;
    async fn remove_alias(&self, alias: &RoomAliasId) -> Result<Option<RoomAlias>, StorageError>;
}

#[derive(Clone, Debug)]
pub struct RoomAlias {
    pub room_id: OwnedRoomId,
    pub creator: OwnedUserId,
    pub alias: OwnedRoomAliasId,
}
