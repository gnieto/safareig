use safareig_core::{
    ruma::{RoomAliasId, RoomId},
    storage::StorageError,
};
use sqlx::{sqlite::SqliteRow, FromRow, Row, Sqlite};

use super::{AliasStorage, RoomAlias};

pub struct SqlxStorage {
    pool: sqlx::Pool<Sqlite>,
}

impl SqlxStorage {
    pub fn new(pool: sqlx::Pool<Sqlite>) -> Self {
        Self { pool }
    }
}

#[async_trait::async_trait]
impl AliasStorage for SqlxStorage {
    async fn get_alias(
        &self,
        alias: &safareig_core::ruma::RoomAliasId,
    ) -> Result<Option<RoomAlias>, StorageError> {
        let alias = alias.to_string();
        let maybe_alias: Option<RoomAlias> = sqlx::query_as(
            r#"
            SELECT * FROM room_aliases
            WHERE room_alias = ?
            "#,
        )
        .bind(alias)
        .fetch_optional(&self.pool)
        .await?;

        Ok(maybe_alias)
    }

    async fn room_aliases(
        &self,
        room_id: &safareig_core::ruma::RoomId,
    ) -> Result<Vec<RoomAlias>, StorageError> {
        let room_id = room_id.to_string();

        Ok(sqlx::query_as(
            r#"
            SELECT * FROM room_aliases
            WHERE room_id = ?
            "#,
        )
        .bind(room_id)
        .fetch_all(&self.pool)
        .await?)
    }

    async fn set_alias(
        &self,
        room_alias: RoomAlias,
        allow_replace: bool,
    ) -> Result<(), StorageError> {
        let alias = room_alias.alias.as_str();
        let room_id = room_alias.room_id.as_str();
        let creator = room_alias.creator.as_str();

        let mut sql = r#"
        INSERT INTO room_aliases (room_alias, room_id, creator)
        VALUES (?1, ?2, ?3)
        "#
        .to_string();

        if allow_replace {
            sql.push_str("ON CONFLICT DO UPDATE SET room_id = ?2");
        }

        sqlx::query(&sql)
            .bind(alias)
            .bind(room_id)
            .bind(creator)
            .execute(&self.pool)
            .await?;

        Ok(())
    }

    async fn remove_alias(&self, alias: &RoomAliasId) -> Result<Option<RoomAlias>, StorageError> {
        let maybe_alias: Option<RoomAlias> = sqlx::query_as(
            r#"
            DELETE FROM room_aliases
            WHERE room_alias = ?
            RETURNING *
            "#,
        )
        .bind(alias.as_str())
        .fetch_optional(&self.pool)
        .await?;

        Ok(maybe_alias)
    }
}

impl FromRow<'_, SqliteRow> for RoomAlias {
    fn from_row(row: &'_ SqliteRow) -> std::result::Result<Self, sqlx::Error> {
        let creator = row.get::<String, _>("creator");
        let room_id = row.get::<String, _>("room_id");
        let alias = row.get::<String, _>("room_alias");

        let creator = creator
            .parse()
            .map_err(|e| sqlx::Error::Decode(Box::new(e)))?;
        let room_id = RoomId::parse(room_id).map_err(|e| sqlx::Error::Decode(Box::new(e)))?;
        let alias = alias
            .parse()
            .map_err(|e| sqlx::Error::Decode(Box::new(e)))?;

        Ok(RoomAlias {
            room_id,
            creator,
            alias,
        })
    }
}
