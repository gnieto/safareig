use std::sync::Arc;

use safareig_core::{
    command::{Container, Module, Router},
    config::HomeserverConfig,
    database::DatabaseConnection,
    ddi::{ServiceCollectionExt, ServiceProvider, ServiceResolverExt},
    ServerInfo,
};
use safareig_federation::client::ClientBuilder;

use super::{
    command::{
        CreateAliasCommand, GetAliasCommand, GetAliasFederationCommand, RemoveAliasCommand,
        RoomAliasesCommand,
    },
    storage::AliasStorage,
    AliasResolver, LocalAliasResolver, RemoteAliasResolver,
};

#[derive(Default)]
pub struct AliasesModule {}

impl AliasesModule {
    fn create_alias_storage(db: DatabaseConnection) -> Arc<dyn AliasStorage> {
        match db {
            #[cfg(feature = "backend-sqlx")]
            DatabaseConnection::Sqlx(pool) => {
                Arc::new(crate::alias::storage::sqlx::SqlxStorage::new(pool))
            }
            _ => unimplemented!(),
        }
    }
}

#[async_trait::async_trait]
impl<R: Router> Module<R> for AliasesModule {
    fn register(&self, container: &mut Container, router: &mut R) {
        container
            .service_collection
            .service_factory(|db: &DatabaseConnection| Ok(Self::create_alias_storage(db.clone())));

        container.service_collection.service_factory(
            |config: &Arc<HomeserverConfig>,
             alias_storage: &Arc<dyn AliasStorage>,
             sp: &ServiceProvider| {
                let local = LocalAliasResolver::new(alias_storage.clone());
                let resolver: Arc<dyn AliasResolver> = if config.federation.enabled {
                    let server_info = sp.get::<ServerInfo>()?.clone();
                    let client_builder = sp.get::<Arc<ClientBuilder>>()?.clone();

                    Arc::new(RemoteAliasResolver::new(local, server_info, client_builder))
                } else {
                    Arc::new(local)
                };

                Ok(resolver)
            },
        );

        container.inject_endpoint::<_, CreateAliasCommand, _>(router);
        container.inject_endpoint::<_, GetAliasCommand, _>(router);
        container.inject_endpoint::<_, RemoveAliasCommand, _>(router);
        container.inject_endpoint::<_, RoomAliasesCommand, _>(router);
        container.inject_endpoint::<_, GetAliasFederationCommand, _>(router);
    }
}
