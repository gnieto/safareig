pub mod command;
pub mod error;
pub mod module;
pub mod storage;

use std::sync::Arc;

use safareig_core::{
    ruma::{
        api::federation::query::get_room_information::v1 as get_room_information, OwnedRoomId,
        OwnedServerName, RoomAliasId,
    },
    ServerInfo,
};
use safareig_federation::client::ClientBuilder;

use self::{error::AliasError, storage::AliasStorage};

#[async_trait::async_trait]
pub trait AliasResolver: Send + Sync {
    async fn resolve_alias(&self, room_alias: &RoomAliasId) -> Result<AliasInfo, AliasError>;
}

#[derive(Clone)]
pub struct LocalAliasResolver {
    alias: Arc<dyn AliasStorage>,
}

#[async_trait::async_trait]
impl AliasResolver for LocalAliasResolver {
    async fn resolve_alias(&self, room_alias: &RoomAliasId) -> Result<AliasInfo, AliasError> {
        let room_id = self
            .alias
            .get_alias(room_alias)
            .await?
            .map(|room_alias| room_alias.room_id)
            .ok_or_else(|| AliasError::AliasNotFound(room_alias.to_owned()))?;

        // TODO: Recover load of participating servers
        let mut servers: Vec<OwnedServerName> = vec![];

        if let Some(local_server) = room_id.server_name() {
            let local_server = OwnedServerName::from(local_server);
            if !servers.contains(&local_server) {
                servers.push(local_server);
            }
        }

        Ok(AliasInfo { room_id, servers })
    }
}

impl LocalAliasResolver {
    pub fn new(alias: Arc<dyn AliasStorage>) -> Self {
        Self { alias }
    }
}

#[derive(Clone)]
pub struct RemoteAliasResolver {
    local: LocalAliasResolver,
    server_info: ServerInfo,
    client_builder: Arc<ClientBuilder>,
}

#[async_trait::async_trait]
impl AliasResolver for RemoteAliasResolver {
    async fn resolve_alias(&self, room_alias: &RoomAliasId) -> Result<AliasInfo, AliasError> {
        if self.server_info.is_local_server(room_alias.server_name()) {
            return self.local.resolve_alias(room_alias).await;
        }

        let request = get_room_information::Request::new(room_alias.to_owned());
        let client = self
            .client_builder
            .client_for(room_alias.server_name())
            .await?;
        let response = client.auth_request(request).await?;

        Ok(AliasInfo {
            room_id: response.room_id,
            servers: response.servers,
        })
    }
}

impl RemoteAliasResolver {
    pub fn new(
        local: LocalAliasResolver,
        server_info: ServerInfo,
        client_builder: Arc<ClientBuilder>,
    ) -> Self {
        Self {
            local,
            server_info,
            client_builder,
        }
    }
}

pub struct AliasInfo {
    pub room_id: OwnedRoomId,
    pub servers: Vec<OwnedServerName>,
}
