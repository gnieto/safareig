use std::sync::Arc;

use safareig::client::room::loader::RoomLoader;
#[cfg(feature = "backend-sqlx")]
use safareig_core::storage::StorageError;
use safareig_core::{
    command::{Container, InjectServices, Module, Router, ServiceCollectionExtension},
    database::DatabaseConnection,
    ddi::{ServiceCollectionExt, ServiceProvider, ServiceResolverExt},
};

use crate::{
    command, directory::Directory, storage::PublicRoomStorage, worker::PublicRoomWorker,
    PublicRoomMapper, RoomLoaderMapper,
};

#[derive(Default)]
pub struct RoomDirectoryModule {}

impl RoomDirectoryModule {
    fn create_public_room_storage(db: DatabaseConnection) -> Arc<dyn PublicRoomStorage> {
        match db {
            #[cfg(feature = "backend-sqlx")]
            DatabaseConnection::Sqlx(pool) => {
                Arc::new(crate::storage::sqlx::PublicRoomSqliteStorage::new(pool))
            }
            _ => unimplemented!(),
        }
    }

    fn register_client<R: Router>(&self, container: &mut Container, router: &mut R) {
        container.inject_endpoint::<_, command::client::PublicRoomsCommand, _>(router);
        container.inject_endpoint::<_, command::client::PublicRoomsFilteredCommand, _>(router);
        container.inject_endpoint::<_, command::client::GetRoomVisibilityCommand, _>(router);
        container.inject_endpoint::<_, command::client::ChangeRoomVisibilityCommand, _>(router);
    }

    fn register_federation<R: Router>(&self, container: &mut Container, router: &mut R) {
        container.inject_endpoint::<_, command::federation::PublicRoomsCommand, _>(router);
        container.inject_endpoint::<_, command::federation::PublicRoomsFilteredCommand, _>(router);
    }
}

#[async_trait::async_trait]
impl<R: Router> Module<R> for RoomDirectoryModule {
    fn register(&self, container: &mut Container, router: &mut R) {
        container
            .service_collection
            .service_factory(|db: &DatabaseConnection| {
                Ok(Self::create_public_room_storage(db.clone()))
            });
        container
            .service_collection
            .service_factory(|rl: &RoomLoader| {
                let mapper: Arc<dyn PublicRoomMapper> = Arc::new(RoomLoaderMapper::new(rl.clone()));
                Ok(mapper)
            });
        container.service_collection.register::<Directory>();

        self.register_client(container, router);

        if container.config.federation.enabled {
            self.register_federation(container, router);
        }
    }

    async fn spawn_workers(&self, container: &ServiceProvider, _: &graceful::BackgroundHandler) {
        let _storage = container.get::<Arc<dyn PublicRoomStorage>>().unwrap();
        let worker = PublicRoomWorker::inject(container).unwrap();

        tokio::spawn(worker.run());
    }

    #[cfg(feature = "backend-sqlx")]
    async fn run_migrations(&self, container: &ServiceProvider) -> Result<(), StorageError> {
        let db = container.get::<DatabaseConnection>()?;
        safareig_sqlx::run_migration(db, "sqlite", "safareig-directory").await?;

        Ok(())
    }
}
