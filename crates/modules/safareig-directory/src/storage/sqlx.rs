use async_trait::async_trait;
use safareig_core::{
    pagination::{Pagination, PaginationResponse},
    ruma::{directory::PublicRoomsChunk, OwnedMxcUri, OwnedRoomAliasId, OwnedRoomId, RoomId, UInt},
    storage::StorageError,
    StreamToken,
};
use safareig_sqlx::{decode_from, decode_from_str, DirectionExt, RangeWrapper};
use sea_query::{Alias, Expr, Iden, Query, SqliteQueryBuilder};
use sea_query_binder::SqlxBinder;
use sqlx::{sqlite::SqliteRow, Row};

use super::{DirectoryFilter, PublicRoomEntry, PublicRoomStorage};
use crate::directory::PUBLIC_ROOM_DEFAULT_CHUNK_SIZE;

pub struct PublicRoomSqliteStorage {
    pool: sqlx::Pool<sqlx::Sqlite>,
}

#[derive(Iden)]
enum PublicRooms {
    Table,
    RoomId,
    CanonicalAlias,
    Name,
    JoinedMembers,
    Topic,
    WorldReadable,
    GuestCanJoin,
    AvatarUrl,
    JoinRule,
    Federable,
}

#[async_trait]
impl PublicRoomStorage for PublicRoomSqliteStorage {
    async fn public_room_chunk(
        &self,
        pagination: Pagination<StreamToken, ()>,
        filter: &DirectoryFilter,
    ) -> Result<PaginationResponse<StreamToken, PublicRoomEntry>, StorageError> {
        let wrapper = RangeWrapper(pagination.range().map(|t| t.to_string()));
        let mut builder = Query::select();
        builder
            .columns([
                PublicRooms::RoomId,
                PublicRooms::CanonicalAlias,
                PublicRooms::Name,
                PublicRooms::JoinedMembers,
                PublicRooms::Topic,
                PublicRooms::WorldReadable,
                PublicRooms::GuestCanJoin,
                PublicRooms::AvatarUrl,
                PublicRooms::JoinRule,
                PublicRooms::Federable,
            ])
            .from(PublicRooms::Table);

        if filter.federation {
            builder.and_where(Expr::col(PublicRooms::Federable).eq(filter.federation));
        }

        if let Some(room_filter) = filter.filter() {
            if let Some(term) = &room_filter.generic_search_term {
                builder.and_where(Expr::cust_with_values(
                    "((name LIKE '%' || ? || '%') OR (topic LIKE '%' || ? || '%'))",
                    [term.to_string(), term.to_string()],
                ));
            }
        }
        let limit = pagination.limit().unwrap_or(PUBLIC_ROOM_DEFAULT_CHUNK_SIZE);
        let offset = pagination
            .range()
            .from_token()
            .unwrap_or_else(StreamToken::horizon);

        builder
            .cond_where(wrapper.condition(Expr::col(Alias::new("stream_token"))))
            .order_by(PublicRooms::JoinedMembers, pagination.direction().order())
            .limit(limit as u64)
            .offset(*offset);

        let (query, values) = builder.build_sqlx(SqliteQueryBuilder);

        let room_chunk = sqlx::query_with(&query, values)
            .fetch_all(&self.pool)
            .await?
            .into_iter()
            .map(|row| row_to_chunk(&row))
            .collect::<Result<Vec<_>, _>>()?;

        let previous = pagination.range().from_token();
        let next = if room_chunk.len() as u32 == limit {
            Some(StreamToken::from(*offset + (limit as u64)))
        } else {
            None
        };
        let response = PaginationResponse::new(room_chunk, previous, next);

        Ok(response)
    }

    async fn public_room_estimate(&self) -> Result<u32, StorageError> {
        let query = "
        SELECT COUNT(*) as amount FROM public_rooms
        ";

        let amount = sqlx::query(query)
            .fetch_optional(&self.pool)
            .await?
            .and_then(|row| row.try_get::<u32, _>("amount").ok())
            .unwrap_or(0);

        Ok(amount)
    }

    async fn update_room_chunk(
        &self,
        room_id: &RoomId,
        entry: &PublicRoomEntry,
    ) -> Result<(), StorageError> {
        let query = "
        INSERT INTO public_rooms (room_id, canonical_alias, name, joined_members, topic, world_readable, guest_can_join, avatar_url, join_rule, federable)
        VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10)
        ON CONFLICT DO UPDATE SET canonical_alias = ?2, name = ?3, joined_members = ?4, topic = ?5, world_readable = ?6, guest_can_join = ?7, avatar_url = ?8, join_rule = ?9, federable = ?10
        ";
        let world_readable = i32::from(entry.chunk.world_readable);
        let guest_join = i32::from(entry.chunk.guest_can_join);
        let federable = i32::from(entry.federable);

        sqlx::query(query)
            .bind(room_id.as_str())
            .bind(
                entry
                    .chunk
                    .canonical_alias
                    .as_ref()
                    .map(|alias| alias.as_str()),
            )
            .bind(entry.chunk.name.as_ref())
            .bind(i64::from(entry.chunk.num_joined_members))
            .bind(entry.chunk.topic.as_ref())
            .bind(world_readable)
            .bind(guest_join)
            .bind(entry.chunk.avatar_url.as_ref().map(|s| s.as_str()))
            .bind(entry.chunk.join_rule.as_str())
            .bind(federable)
            .execute(&self.pool)
            .await?;

        Ok(())
    }

    async fn remove_room_chunk(&self, room_id: &RoomId) -> Result<(), StorageError> {
        sqlx::query(
            r#"
            DELETE FROM public_rooms
            WHERE room_id = ?
            "#,
        )
        .bind(room_id.as_str())
        .execute(&self.pool)
        .await?;

        Ok(())
    }
}

impl PublicRoomSqliteStorage {
    pub fn new(pool: sqlx::Pool<sqlx::Sqlite>) -> Self {
        Self { pool }
    }
}

fn row_to_chunk(row: &SqliteRow) -> Result<PublicRoomEntry, StorageError> {
    let room_id: OwnedRoomId = decode_from_str(row.try_get::<&str, _>("room_id")?)?;

    let maybe_alias = row.try_get::<Option<&str>, _>("canonical_alias")?;
    let canonical_alias: Option<OwnedRoomAliasId> = maybe_alias.map(decode_from_str).transpose()?;

    let name = row.try_get::<Option<String>, _>("name")?;
    let joined_members = row.try_get::<u32, _>("joined_members")?;
    let topic = row.try_get::<Option<String>, _>("topic")?;
    let world_readable = row.try_get::<u32, _>("world_readable")?;
    let guest_can_join = row.try_get::<u32, _>("guest_can_join")?;

    let maybe_url = row.try_get::<Option<&str>, _>("avatar_url")?;
    let avatar_url: Option<OwnedMxcUri> = maybe_url.map(decode_from);

    let maybe_join = row.try_get::<Option<&str>, _>("join_rule")?;
    let join_rule = maybe_join.map(decode_from).unwrap_or_default();

    let chunk = PublicRoomsChunk {
        canonical_alias,
        name,
        num_joined_members: UInt::from(joined_members),
        room_id,
        topic,
        world_readable: world_readable == 1,
        guest_can_join: guest_can_join == 1,
        avatar_url,
        join_rule,
        room_type: None,
    };

    let federable = row.try_get::<u32, _>("federable")?;

    Ok(PublicRoomEntry {
        chunk,
        room_type: None,
        federable: federable == 1,
    })
}
