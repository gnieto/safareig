use std::sync::Arc;

use safareig::client::room::RoomError;
use safareig_core::{
    pagination::{Boundary, Limit, Pagination},
    ruma::{api::client::directory::get_public_rooms_filtered, directory::Filter, UInt},
    Inject, StreamToken,
};

use crate::storage::{DirectoryFilter, PublicRoomStorage};

pub const PUBLIC_ROOM_DEFAULT_CHUNK_SIZE: u32 = 25;

#[derive(Clone, Inject)]
pub struct Directory {
    public_rooms: Arc<dyn PublicRoomStorage>,
}

/// `Directory` handles all the operations related to the room directory. By now, it discovers all
/// the public rooms of this homeserver.
impl Directory {
    /// Retrieves public rooms from the given token and the desired amount of rooms. Note that this
    /// function returns a struct from the client API and it will need to be converted to the
    /// federation struct before returning the response on the corresponding command.
    pub async fn public_rooms(
        &self,
        since: u32,
        limit: u32,
        show_federation_only: bool,
        filter: Option<Filter>,
    ) -> Result<get_public_rooms_filtered::v3::Response, RoomError> {
        let stream_token = StreamToken::from(since as u64);
        let query = Pagination::new(
            (Boundary::Inclusive(stream_token), Boundary::Unlimited).into(),
            (),
        )
        .with_limit(Limit::from(limit));

        let directory_filter = DirectoryFilter::new(filter, show_federation_only);

        let response = self
            .public_rooms
            .public_room_chunk(query, &directory_filter)
            .await?;

        let next_batch = response.next().map(|token| token.to_string());
        let rooms = response
            .records()
            .into_iter()
            .map(|entry| entry.chunk)
            .collect();
        let count = self.public_rooms.public_room_estimate().await?;

        Ok(get_public_rooms_filtered::v3::Response {
            chunk: rooms,
            next_batch,
            prev_batch: if since > 0 {
                Some(since.to_string())
            } else {
                None
            },
            total_room_count_estimate: Some(UInt::new_wrapping(count as u64)),
        })
    }
}
