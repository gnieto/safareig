use async_trait::async_trait;
use safareig_core::{
    pagination::{Pagination, PaginationResponse},
    ruma::{
        directory::{Filter, PublicRoomsChunk},
        room::RoomType,
        RoomId,
    },
    storage::StorageError,
    StreamToken,
};

#[cfg(feature = "backend-sqlx")]
pub mod sqlx;

#[async_trait]
pub trait PublicRoomStorage: Send + Sync {
    async fn public_room_chunk(
        &self,
        pagination: Pagination<StreamToken, ()>,
        filter: &DirectoryFilter,
    ) -> Result<PaginationResponse<StreamToken, PublicRoomEntry>, StorageError>;
    async fn public_room_estimate(&self) -> Result<u32, StorageError>;
    async fn update_room_chunk(
        &self,
        room_id: &RoomId,
        chunk: &PublicRoomEntry,
    ) -> Result<(), StorageError>;
    async fn remove_room_chunk(&self, room_id: &RoomId) -> Result<(), StorageError>;
}

pub struct DirectoryFilter {
    filter: Option<Filter>,
    federation: bool,
}

impl DirectoryFilter {
    pub fn new(filter: Option<Filter>, federation: bool) -> Self {
        DirectoryFilter { filter, federation }
    }

    pub fn filter(&self) -> Option<&Filter> {
        self.filter.as_ref()
    }

    pub fn federation(&self) -> bool {
        self.federation
    }
}

pub struct PublicRoomEntry {
    pub chunk: PublicRoomsChunk,
    pub room_type: Option<RoomType>,
    pub federable: bool,
}
