pub mod command;
mod directory;
pub mod module;
pub mod storage;
mod worker;

pub use directory::Directory;
use safareig::client::room::loader::RoomLoader;
use safareig_core::{
    ruma::{
        api::client::room::Visibility,
        directory::{PublicRoomJoinRule, PublicRoomsChunk},
        events::room::join_rules::JoinRule,
        RoomId, UInt,
    },
    storage::StorageError,
};
use storage::PublicRoomEntry;

#[async_trait::async_trait]
pub trait PublicRoomMapper: Send + Sync {
    async fn public_room_chunk(&self, room_id: &RoomId) -> Result<RoomChunkAction, StorageError>;
}

pub struct RoomLoaderMapper {
    room_loader: RoomLoader,
}

pub enum RoomChunkAction {
    Update(PublicRoomEntry),
    Ignore,
}

#[async_trait::async_trait]
impl PublicRoomMapper for RoomLoaderMapper {
    async fn public_room_chunk(&self, room_id: &RoomId) -> Result<RoomChunkAction, StorageError> {
        let room = self.room_loader.get(room_id).await.unwrap();

        let room = match room {
            Some(room) => room,
            None => {
                tracing::debug!(
                    room_id = room_id.to_string().as_str(),
                    "could not update public room data. Room was not found",
                );
                return Ok(RoomChunkAction::Ignore);
            }
        };

        let visibility = room
            .create_extra()
            .await
            .ok()
            .map(|content| content.content.visibility)
            .unwrap_or(Visibility::Private);

        if let Visibility::Private = visibility {
            return Ok(RoomChunkAction::Ignore);
        }

        let summary = room
            .room_summary()
            .await
            .map_err(|e| StorageError::Custom(e.to_string()))?;

        let public_join_rule = match summary.join_rule {
            JoinRule::Public => PublicRoomJoinRule::Public,
            JoinRule::Knock | JoinRule::KnockRestricted(_) => PublicRoomJoinRule::Knock,
            _ => {
                tracing::warn!(
                    room_id = room_id.to_string().as_str(),
                    "refusing to create a public room record because of non-expected join rule",
                );

                return Ok(RoomChunkAction::Ignore);
            }
        };

        let chunk = PublicRoomsChunk {
            canonical_alias: summary.canoncical_alias,
            name: summary.name,
            num_joined_members: UInt::from(summary.num_joined_members),
            room_id: summary.room_id,
            topic: summary.topic,
            world_readable: summary.world_readable,
            guest_can_join: summary.guest_can_join,
            avatar_url: summary.avatar_url,
            join_rule: public_join_rule,
            room_type: None,
        };

        let entry = PublicRoomEntry {
            chunk,
            room_type: summary.room_type,
            federable: summary.federable,
        };

        Ok(RoomChunkAction::Update(entry))
    }
}

impl RoomLoaderMapper {
    pub fn new(room_loader: RoomLoader) -> Self {
        Self { room_loader }
    }
}
