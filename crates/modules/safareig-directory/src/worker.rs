use std::sync::Arc;

use safareig::client::room::RoomError;
use safareig_core::{
    bus::{Bus, BusMessage},
    ruma::RoomId,
    Inject,
};

use crate::{storage::PublicRoomStorage, PublicRoomMapper, RoomChunkAction};

// TODO: Probably this should not be public
#[derive(Inject)]
pub struct PublicRoomWorker {
    bus: Bus,
    public_room: Arc<dyn PublicRoomStorage>,
    mapper: Arc<dyn PublicRoomMapper>,
}

impl PublicRoomWorker {
    pub async fn run(self) {
        tracing::info!("Spawned public room worker");
        let mut receiver = self.bus.receiver();

        while let Ok(msg) = receiver.recv().await {
            let result = match msg {
                BusMessage::RoomCreated(room_id) => self.update_room(&room_id).await,
                BusMessage::RoomJoined(room, _) => self.update_room(&room).await,
                BusMessage::RoomUpdate {
                    room_id: room,
                    is_state,
                    ..
                } => {
                    if !is_state {
                        continue;
                    }

                    if let Err(e) = self.update_room(&room).await {
                        tracing::error!(
                            room_id = room.as_str(),
                            err = e.to_string().as_str(),
                            "could not update public room"
                        );
                    }

                    Ok(())
                }
                BusMessage::Shutdown => break,
                _ => Ok(()),
            };

            if let Err(e) = result {
                tracing::error!("Could not update public: {}", e)
            }
        }
    }

    async fn update_room(&self, room_id: &RoomId) -> Result<(), RoomError> {
        tracing::debug!(
            room_id = room_id.to_string().as_str(),
            "Indexing room to public directory",
        );

        let public_room_chunk = self.mapper.public_room_chunk(room_id).await?;

        if let RoomChunkAction::Update(chunk) = public_room_chunk {
            self.public_room.update_room_chunk(room_id, &chunk).await?;
        }

        Ok(())
    }
}
