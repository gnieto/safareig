use std::sync::Arc;

use async_trait::async_trait;
use safareig::{client::room::loader::RoomLoader, core::events::EventBuilder};
use safareig_core::{
    auth::Identity,
    command::Command,
    error::ResourceNotFound,
    events::Content,
    ruma::api::client::{
        directory::{get_room_visibility, set_room_visibility},
        room::Visibility,
    },
    Inject,
};

use crate::storage::PublicRoomStorage;

#[derive(Inject)]
pub struct ChangeRoomVisibilityCommand {
    room_loader: RoomLoader,
    public_room_storage: Arc<dyn PublicRoomStorage>,
}

#[async_trait]
impl Command for ChangeRoomVisibilityCommand {
    type Input = set_room_visibility::v3::Request;
    type Output = set_room_visibility::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let room = self
            .room_loader
            .get(&input.room_id)
            .await?
            .ok_or(ResourceNotFound)?;

        // Allow only room's creator to change the visibility of a room.
        // Probably we could delegate this authorization to PowerLevels instead.
        let create_room_content = room.create_event().await?;
        let room_creator = create_room_content.content.creator;
        if let Some(creator) = room_creator {
            id.check_user(&creator)?;
        }

        if let Visibility::Private = input.visibility {
            self.public_room_storage
                .remove_room_chunk(&input.room_id)
                .await?;
        }

        let room_extra = room.create_extra().await?;
        let mut new_content = room_extra.content.clone();
        new_content.visibility = input.visibility;

        let content = Content::from_event_content(&new_content)?;
        let event_builder = EventBuilder::state(id.user(), content, &input.room_id, None);
        room.add_state(event_builder).await?;

        Ok(set_room_visibility::v3::Response {})
    }
}

#[derive(Inject)]
pub struct GetRoomVisibilityCommand {
    room_loader: RoomLoader,
}

#[async_trait]
impl Command for GetRoomVisibilityCommand {
    type Input = get_room_visibility::v3::Request;
    type Output = get_room_visibility::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, _: Identity) -> Result<Self::Output, Self::Error> {
        let room = self
            .room_loader
            .get(&input.room_id)
            .await?
            .ok_or(ResourceNotFound)?;

        let room_extra = room.create_extra().await?;

        Ok(get_room_visibility::v3::Response {
            visibility: room_extra.content.visibility,
        })
    }
}
