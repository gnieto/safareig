use std::{convert::TryFrom, str::FromStr, sync::Arc};

use async_trait::async_trait;
use safareig::client::room::RoomError;
use safareig_core::{
    auth::Identity,
    command::Command,
    ruma::{
        api::client::directory::{get_public_rooms, get_public_rooms_filtered},
        directory::RoomNetwork,
    },
    Inject, ServerInfo,
};
use safareig_federation::client::ClientBuilder;

use crate::directory::{Directory, PUBLIC_ROOM_DEFAULT_CHUNK_SIZE};

#[derive(Inject)]
pub struct PublicRoomsFilteredCommand {
    directory: Directory,
    client: Arc<ClientBuilder>,
    server_info: ServerInfo,
}

#[async_trait]
impl Command for PublicRoomsFilteredCommand {
    type Input = get_public_rooms_filtered::v3::Request;
    type Output = get_public_rooms_filtered::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, _: Identity) -> Result<Self::Output, Self::Error> {
        if let Some(server_name) = &input.server {
            if !self.server_info.is_local_server(server_name) {
                return Ok(self.federated_public_room(&input).await?);
            }
        }

        let since = input
            .since
            .as_ref()
            .and_then(|since_str| u32::from_str(since_str).ok())
            .unwrap_or(0);

        let limit = input
            .limit
            .map(|l| u32::try_from(l).unwrap_or(PUBLIC_ROOM_DEFAULT_CHUNK_SIZE))
            .unwrap_or(PUBLIC_ROOM_DEFAULT_CHUNK_SIZE);

        Ok(self
            .directory
            .public_rooms(since, limit, false, Some(input.filter))
            .await?)
    }
}

impl PublicRoomsFilteredCommand {
    async fn federated_public_room(
        &self,
        input: &get_public_rooms_filtered::v3::Request,
    ) -> Result<get_public_rooms_filtered::v3::Response, RoomError> {
        // TODO: Move to directory!
        let client = self
            .client
            .client_for(input.server.as_ref().unwrap())
            .await?;

        let request = safareig_core::ruma::api::federation::directory::get_public_rooms_filtered::v1::Request {
            limit: input.limit,
            room_network: RoomNetwork::Matrix,
            since: input.since.clone(),
            filter: Default::default(),
        };

        let response: safareig_core::ruma::api::federation::directory::get_public_rooms_filtered::v1::Response =
            client.auth_request(request).await?;

        Ok(get_public_rooms_filtered::v3::Response {
            chunk: response.chunk,
            next_batch: response.next_batch,
            prev_batch: response.prev_batch,
            total_room_count_estimate: response.total_room_count_estimate,
        })
    }
}

#[derive(Inject)]
pub struct PublicRoomsCommand {
    directory: Directory,
    client: Arc<ClientBuilder>,
    server_info: ServerInfo,
}

#[async_trait]
impl Command for PublicRoomsCommand {
    type Input = get_public_rooms::v3::Request;
    type Output = get_public_rooms::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, _: Identity) -> Result<Self::Output, Self::Error> {
        if let Some(server_name) = &input.server {
            if !self.server_info.is_local_server(server_name) {
                return Ok(self.federated_public_room(&input).await?);
            }
        }

        let since = input
            .since
            .as_ref()
            .and_then(|since_str| u32::from_str(since_str).ok())
            .unwrap_or(0);

        let limit = input
            .limit
            .map(|l| u32::try_from(l).unwrap_or(PUBLIC_ROOM_DEFAULT_CHUNK_SIZE))
            .unwrap_or(PUBLIC_ROOM_DEFAULT_CHUNK_SIZE);

        let response = self
            .directory
            .public_rooms(since, limit, false, None)
            .await?;

        Ok(get_public_rooms::v3::Response {
            chunk: response.chunk,
            next_batch: response.next_batch,
            prev_batch: response.prev_batch,
            total_room_count_estimate: response.total_room_count_estimate,
        })
    }
}

impl PublicRoomsCommand {
    async fn federated_public_room(
        &self,
        input: &get_public_rooms::v3::Request,
    ) -> Result<get_public_rooms::v3::Response, RoomError> {
        // TODO: Move to directory!
        let client = self
            .client
            .client_for(input.server.as_ref().unwrap())
            .await?;

        let request =
            safareig_core::ruma::api::federation::directory::get_public_rooms::v1::Request {
                limit: input.limit,
                room_network: RoomNetwork::Matrix,
                since: input.since.clone(),
            };

        let response: safareig_core::ruma::api::federation::directory::get_public_rooms::v1::Response =
            client.auth_request(request).await?;

        Ok(get_public_rooms::v3::Response {
            chunk: response.chunk,
            next_batch: response.next_batch,
            prev_batch: response.prev_batch,
            total_room_count_estimate: response.total_room_count_estimate,
        })
    }
}
