use std::{convert::TryFrom, str::FromStr};

use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    command::Command,
    ruma::api::federation::directory::{
        get_public_rooms::v1 as get_public_rooms,
        get_public_rooms_filtered::v1 as get_public_rooms_filtered,
    },
    Inject,
};

use crate::directory::{Directory, PUBLIC_ROOM_DEFAULT_CHUNK_SIZE};

#[derive(Inject)]
pub struct PublicRoomsCommand {
    directory: Directory,
}

#[async_trait]
impl Command for PublicRoomsCommand {
    type Input = get_public_rooms::Request;
    type Output = get_public_rooms::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        if let Some(auth) = id.to_federation_auth() {
            tracing::info!("Received public room command from: {}", auth.server);
        }

        let since = input
            .since
            .as_ref()
            .and_then(|since_str| u32::from_str(since_str).ok())
            .unwrap_or(0);

        let limit = input
            .limit
            .map(|l| u32::try_from(l).unwrap_or(PUBLIC_ROOM_DEFAULT_CHUNK_SIZE))
            .unwrap_or(PUBLIC_ROOM_DEFAULT_CHUNK_SIZE);

        let response = self
            .directory
            .public_rooms(since, limit, true, None)
            .await?;

        Ok(get_public_rooms::Response {
            chunk: response.chunk,
            next_batch: response.next_batch,
            prev_batch: response.prev_batch,
            total_room_count_estimate: response.total_room_count_estimate,
        })
    }
}

#[derive(Inject)]
pub struct PublicRoomsFilteredCommand {
    directory: Directory,
}

#[async_trait]
impl Command for PublicRoomsFilteredCommand {
    type Input = get_public_rooms_filtered::Request;
    type Output = get_public_rooms_filtered::Response;
    type Error = safareig_core::ruma::api::error::MatrixError;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        if let Some(auth) = id.to_federation_auth() {
            tracing::info!("Received public room command from: {}", auth.server);
        }
        // TODO: Allow get servers via federation if target server is not us

        let since = input
            .since
            .as_ref()
            .and_then(|since_str| u32::from_str(since_str).ok())
            .unwrap_or(0);

        let limit = input
            .limit
            .map(|l| u32::try_from(l).unwrap_or(PUBLIC_ROOM_DEFAULT_CHUNK_SIZE))
            .unwrap_or(PUBLIC_ROOM_DEFAULT_CHUNK_SIZE);

        let response = self
            .directory
            .public_rooms(since, limit, true, Some(input.filter))
            .await?;

        Ok(get_public_rooms_filtered::Response {
            chunk: response.chunk,
            next_batch: response.next_batch,
            prev_batch: response.prev_batch,
            total_room_count_estimate: response.total_room_count_estimate,
        })
    }
}
