use std::sync::Arc;

use safareig_core::{
    command::Command,
    pagination::{Pagination, Range},
    ruma::{
        api::client::{
            directory::{get_room_visibility, set_room_visibility},
            room::Visibility,
        },
        directory::Filter,
        exports::http::StatusCode,
    },
};
use safareig_directory::{
    command::client::{ChangeRoomVisibilityCommand, GetRoomVisibilityCommand},
    storage::{DirectoryFilter, PublicRoomStorage},
};

use crate::testing_app;

#[tokio::test]
async fn room_visibility_can_be_change_by_creator() {
    let app = testing_app().await;
    let (id, room) = app.public_room().await;
    let cmd = app.command::<ChangeRoomVisibilityCommand>();

    let request = set_room_visibility::v3::Request {
        room_id: room.id().to_owned(),
        visibility: Visibility::Private,
    };
    let _response = cmd.execute(request, id.clone()).await.unwrap();

    let cmd = app.command::<GetRoomVisibilityCommand>();
    let request = get_room_visibility::v3::Request {
        room_id: room.id().to_owned(),
    };
    let response = cmd.execute(request, id).await.unwrap();

    assert_eq!(Visibility::Private, response.visibility);
}

#[tokio::test]
async fn room_visibility_can_not_be_change_by_non_authorized_users() {
    let app = testing_app().await;
    let (_, room) = app.public_room().await;
    let non_authorized_user = app.register().await;
    let cmd = app.command::<ChangeRoomVisibilityCommand>();

    let request = set_room_visibility::v3::Request {
        room_id: room.id().to_owned(),
        visibility: Visibility::Private,
    };
    let response = cmd.execute(request, non_authorized_user).await;
    let error = response.err().unwrap();

    assert_eq!(error.status_code, StatusCode::UNAUTHORIZED);
}

#[tokio::test]
async fn change_room_visibility_to_private_removes_room_from_directory() {
    let app = testing_app().await;
    let (id, room) = app.public_room().await;
    let cmd = app.command::<ChangeRoomVisibilityCommand>();

    let request = set_room_visibility::v3::Request {
        room_id: room.id().to_owned(),
        visibility: Visibility::Private,
    };
    let _response = cmd.execute(request, id.clone()).await.unwrap();

    let storage = app.service::<Arc<dyn PublicRoomStorage>>();
    let query = Pagination::new(Range::all(), ());
    let filter = DirectoryFilter::new(Some(Filter::default()), false);
    let response = storage.public_room_chunk(query, &filter).await.unwrap();

    let records = response.records();
    assert_eq!(0, records.len());
}

#[tokio::test]
async fn room_visibility_can_be_retrieved() {
    let app = testing_app().await;
    let (id, room) = app.public_room().await;

    let cmd = app.command::<GetRoomVisibilityCommand>();

    let request = get_room_visibility::v3::Request {
        room_id: room.id().to_owned(),
    };
    let response = cmd.execute(request, id).await.unwrap();

    assert_eq!(Visibility::Public, response.visibility);
}

#[tokio::test]
async fn room_visibility_can_be_retrieved_in_private_rooms() {
    let app = testing_app().await;
    let (id, room) = app.private_room().await;

    let cmd = app.command::<GetRoomVisibilityCommand>();

    let request = get_room_visibility::v3::Request {
        room_id: room.id().to_owned(),
    };
    let response = cmd.execute(request, id).await.unwrap();

    assert_eq!(Visibility::Private, response.visibility);
}
