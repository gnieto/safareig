use std::sync::Arc;

use safareig_core::{
    command::Command,
    ruma::{
        api::client::directory::get_public_rooms_filtered,
        directory::{Filter, RoomNetwork},
        UInt,
    },
};
use safareig_directory::{
    command::client::PublicRoomsFilteredCommand, storage::PublicRoomStorage, PublicRoomMapper,
    RoomChunkAction,
};
use safareig_testing::TestingApp;

use crate::testing_app;

#[tokio::test]
async fn public_room_directory_pagination() {
    let test = testing_app().await;
    prepare_public_rooms(&test, 11).await;
    let cmd = test.command::<PublicRoomsFilteredCommand>();

    let first_input = get_public_rooms_filtered::v3::Request {
        since: None,
        limit: Some(UInt::new_wrapping(5)),
        server: None,
        filter: Filter {
            generic_search_term: None,
            room_types: Vec::new(),
        },
        room_network: RoomNetwork::All,
    };
    let result = cmd
        .execute(first_input, TestingApp::new_identity())
        .await
        .unwrap();
    assert_eq!(5, result.chunk.len());
    assert_eq!("5", result.next_batch.clone().unwrap());
    assert_eq!(None, result.prev_batch);
    assert_eq!(
        UInt::new_wrapping(11),
        result.total_room_count_estimate.unwrap()
    );

    let second_input = get_public_rooms_filtered::v3::Request {
        since: result.next_batch,
        limit: Some(UInt::new_wrapping(5)),
        server: None,
        filter: Filter {
            generic_search_term: None,
            room_types: Vec::new(),
        },
        room_network: RoomNetwork::All,
    };
    let result = cmd
        .execute(second_input, TestingApp::new_identity())
        .await
        .unwrap();
    assert_eq!(5, result.chunk.len());
    assert_eq!("10", result.next_batch.clone().unwrap());
    assert_eq!("5", result.prev_batch.unwrap());
    assert_eq!(
        UInt::new_wrapping(11),
        result.total_room_count_estimate.unwrap()
    );

    let third_input = get_public_rooms_filtered::v3::Request {
        since: result.next_batch,
        limit: Some(UInt::new_wrapping(5)),
        server: None,
        filter: Filter {
            generic_search_term: None,
            room_types: Vec::new(),
        },
        room_network: RoomNetwork::All,
    };
    let result = cmd
        .execute(third_input, TestingApp::new_identity())
        .await
        .unwrap();
    assert_eq!(1, result.chunk.len());
    assert!(result.next_batch.is_none());
    assert_eq!("10", result.prev_batch.unwrap());
    assert_eq!(
        UInt::new_wrapping(11),
        result.total_room_count_estimate.unwrap()
    );
}

async fn prepare_public_rooms(testing: &TestingApp, amount: u8) {
    let storage = testing.service::<Arc<dyn PublicRoomStorage>>();
    let mapper = testing.service::<Arc<dyn PublicRoomMapper>>();

    for _ in 0..amount {
        let (_, room) = testing.public_room().await;

        let entry = mapper.public_room_chunk(room.id()).await.unwrap();

        if let RoomChunkAction::Update(entry) = entry {
            storage.update_room_chunk(room.id(), &entry).await.unwrap();
        }
    }
}
