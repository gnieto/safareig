use std::sync::Arc;

use safareig::client::room::command::create_room::CreateRoomCommand;
use safareig_core::{
    command::Command,
    ruma::{
        api::client::room::{create_room, create_room::v3::CreationContent, Visibility},
        serde::Raw,
        OwnedRoomId, RoomId,
    },
};
use safareig_directory::{
    storage::PublicRoomStorage, Directory, PublicRoomMapper, RoomChunkAction,
};
use safareig_testing::TestingApp;

use crate::testing_app;

#[tokio::test]
async fn non_federable_rooms_do_not_appear_on_federation_directory() {
    let db = testing_app().await;
    let (_, federable_room) = db.public_room().await;
    let non_federable_room_id = create_non_federable_public_room(&db).await;
    let _storage = db.service::<Arc<dyn PublicRoomStorage>>();
    let directory = db.service::<Directory>();

    update_directory(&db, federable_room.id()).await;
    update_directory(&db, &non_federable_room_id).await;
    let public_rooms = directory.public_rooms(0, 5, true, None).await.unwrap();

    assert_eq!(1, public_rooms.chunk.len());
}

async fn update_directory(app: &TestingApp, room: &RoomId) {
    let storage = app.service::<Arc<dyn PublicRoomStorage>>();
    let mapper = app.service::<Arc<dyn PublicRoomMapper>>();

    let entry = mapper.public_room_chunk(room).await.unwrap();

    if let RoomChunkAction::Update(entry) = entry {
        storage.update_room_chunk(room, &entry).await.unwrap();
    }
}

async fn create_non_federable_public_room(db: &TestingApp) -> OwnedRoomId {
    let name = "Room name".parse().unwrap();

    let cmd = db.command::<CreateRoomCommand>();
    let id = db.register().await;
    let input = create_room::v3::Request {
        creation_content: Some(
            Raw::new(&CreationContent {
                federate: false,
                predecessor: None,
                room_type: None,
            })
            .unwrap(),
        ),
        power_level_content_override: None,
        initial_state: vec![],
        invite: vec![],
        invite_3pid: vec![],
        is_direct: false,
        name: Some(name),
        preset: None,
        room_alias_name: Some("alias_test".to_string()),
        room_version: None,
        topic: Some("Description".to_string()),
        visibility: Visibility::Public,
    };

    let output = cmd.execute(input, id.clone()).await.unwrap();

    output.room_id
}
