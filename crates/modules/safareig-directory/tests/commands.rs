use safareig_directory::module::RoomDirectoryModule;
use safareig_testing::TestingApp;

mod command;
mod directory;

pub async fn testing_app() -> TestingApp {
    TestingApp::with_modules(vec![Box::<RoomDirectoryModule>::default()]).await
}
