use safareig_core::command::{Container, Module, Router, ServiceCollectionExtension};

use crate::{command::WhoisCommand, Whois};

#[derive(Default)]
pub struct ServerAdminModule {}

#[async_trait::async_trait]
impl<R: Router> Module<R> for ServerAdminModule {
    fn register(&self, container: &mut Container, router: &mut R) {
        container.service_collection.register::<Whois>();
        container.inject_endpoint::<_, WhoisCommand, _>(router);
    }
}
