use std::{collections::BTreeMap, sync::Arc};

use safareig_core::{
    ruma::{
        api::client::server::get_user_info::v3::{ConnectionInfo, DeviceInfo, SessionInfo},
        UserId,
    },
    storage::StorageError,
    Inject,
};
use safareig_devices::storage::DeviceStorage;

pub mod command;
pub mod module;

#[derive(Inject, Clone)]
pub struct Whois {
    devices: Arc<dyn DeviceStorage>,
}

impl Whois {
    pub async fn whois(&self, user_id: &UserId) -> Result<WhoisData, StorageError> {
        let devices = self.devices.devices_by_user(user_id).await?;
        let devices_info = devices
            .into_iter()
            .map(|device| {
                let device_info = DeviceInfo {
                    sessions: vec![SessionInfo {
                        connections: vec![ConnectionInfo {
                            ip: device.ruma_device.last_seen_ip.clone(),
                            last_seen: device.ruma_device.last_seen_ts,
                            user_agent: None,
                        }],
                    }],
                };

                (device.ruma_device.device_id.to_string(), device_info)
            })
            .collect();

        Ok(WhoisData {
            devices: devices_info,
        })
    }
}

pub struct WhoisData {
    pub devices: BTreeMap<String, DeviceInfo>,
}
