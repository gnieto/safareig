use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    command::Command,
    ruma::api::client::server::get_user_info::{self},
    Inject,
};

use crate::Whois;

#[derive(Inject)]
pub struct WhoisCommand {
    whois: Whois,
}

#[async_trait]
impl Command for WhoisCommand {
    type Input = get_user_info::v3::Request;
    type Output = get_user_info::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        id.check_user(&input.user_id)?;
        let whois_data = self.whois.whois(id.user()).await?;

        Ok(get_user_info::v3::Response {
            user_id: Some(id.user().to_owned()),
            devices: whois_data.devices,
        })
    }
}
