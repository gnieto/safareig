use std::{collections::HashMap, sync::Arc};

use safareig_appservices::{AppServiceConfig, AppserviceRepository};
use safareig_authentication::command::{login::LoginCommand, DeactivateUserCommand};
use safareig_core::{
    auth::Identity,
    command::{Command, Container, Module, ServiceCollectionExtension},
    ddi::ServiceCollectionExt,
    ruma::{
        api::{
            appservice::{Namespace, Namespaces, Registration},
            client::{
                account::deactivate,
                session::login::{
                    self,
                    v3::{ApplicationService, Password},
                },
                uiaa::UserIdentifier,
            },
        },
        exports::http,
        UserId,
    },
};
use safareig_testing::TestingApp;
use safareig_uiaa::UiaaWrapper;

#[tokio::test]
async fn fails_with_invalid_password() {
    let db = TestingApp::default().await;
    let id = db.register().await;

    let password = Password::new(
        UserIdentifier::UserIdOrLocalpart(id.user().to_string()),
        "invalid".to_string(),
    );
    let input = login::v3::Request {
        login_info: login::v3::LoginInfo::Password(password),
        device_id: None,
        initial_device_display_name: None,
        refresh_token: false,
    };
    let cmd = db.command::<LoginCommand>();

    let result = cmd.execute(input, Identity::None).await;
    assert!(result.is_err());
}

#[tokio::test]
async fn fails_with_deactivated_user() {
    let db = TestingApp::default().await;
    let id = db.register().await;

    let deactivate = db.command::<UiaaWrapper<DeactivateUserCommand>>();
    let request = deactivate::v3::Request {
        auth: Some(TestingApp::uiaa_password(id.user())),
        id_server: None,
        erase: false,
    };
    deactivate.execute(request, id.clone()).await.unwrap();

    let password = Password::new(
        UserIdentifier::UserIdOrLocalpart(id.user().to_string()),
        "pwd".to_string(),
    );
    let input = login::v3::Request {
        login_info: login::v3::LoginInfo::Password(password),
        device_id: None,
        initial_device_display_name: None,
        refresh_token: false,
    };
    let cmd = db.command::<LoginCommand>();

    let result = cmd.execute(input, Identity::None).await;
    assert!(result.is_err());
}

#[tokio::test]
async fn login_with_valid_password() {
    let cfg = TestingApp::default_config();
    let db = TestingApp::with_config(cfg).await;
    let id = db.register().await;

    let password = Password::new(
        UserIdentifier::UserIdOrLocalpart(id.user().to_string()),
        "pwd".to_string(),
    );
    let input = login::v3::Request {
        login_info: login::v3::LoginInfo::Password(password),
        device_id: None,
        initial_device_display_name: None,
        refresh_token: false,
    };
    let cmd = db.command::<LoginCommand>();

    let _ = cmd.execute(input, Identity::None).await.unwrap();
}

#[tokio::test]
async fn login_with_appservice() {
    let cfg = TestingApp::default_config();
    let db = TestingApp::with_config_and_modules(cfg, vec![Box::<TestAppServiceModule>::default()])
        .await;
    let id = Identity::AppService(None, "svc1".to_string());
    let valid_user_id = UserId::parse("@svc1_45678:localhost:8448").unwrap();
    db.register_appservice(&valid_user_id, "svc1").await;

    let input = login::v3::Request {
        login_info: login::v3::LoginInfo::ApplicationService(ApplicationService {
            identifier: Some(UserIdentifier::UserIdOrLocalpart("svc1_45678".to_string())),
            user: None,
        }),
        device_id: None,
        initial_device_display_name: None,
        refresh_token: false,
    };
    let cmd = db.command::<LoginCommand>();

    let _ = cmd.execute(input, id).await.unwrap();
}

#[tokio::test]
async fn login_with_mismatching_appservice() {
    let cfg = TestingApp::default_config();
    let db = TestingApp::with_config_and_modules(cfg, vec![Box::<TestAppServiceModule>::default()])
        .await;
    let id = Identity::AppService(None, "svc2".to_string());
    let valid_user_id = UserId::parse("@svc1_45678:localhost:8448").unwrap();
    db.register_appservice(&valid_user_id, "svc1").await;

    let input = login::v3::Request {
        login_info: login::v3::LoginInfo::ApplicationService(ApplicationService {
            identifier: Some(UserIdentifier::UserIdOrLocalpart("svc1_45678".to_string())),
            user: None,
        }),
        device_id: None,
        initial_device_display_name: None,
        refresh_token: false,
    };
    let cmd = db.command::<LoginCommand>();

    let error = cmd.execute(input, id).await.err().unwrap();
    assert_eq!(error.status_code, http::StatusCode::FORBIDDEN);
}

#[derive(Default)]
struct TestAppServiceModule;

#[async_trait::async_trait]
impl<R: safareig_core::command::Router> Module<R> for TestAppServiceModule {
    fn register(&self, container: &mut Container, router: &mut R) {
        let repository: Arc<dyn AppserviceRepository> =
            Arc::new(TestApplicationSericeRepository::default());

        container.service_collection.service(repository);
    }
}

struct TestApplicationSericeRepository {
    services: HashMap<String, AppServiceConfig>,
}

impl Default for TestApplicationSericeRepository {
    fn default() -> Self {
        let svc1_registration = Registration {
            id: "svc1".to_string(),
            url: None,
            as_token: "as_token_svc1".to_string(),
            hs_token: "hs_token_svc1".to_string(),
            sender_localpart: "svc1".to_string(),
            namespaces: Namespaces {
                users: vec![
                    Namespace {
                        exclusive: true,
                        regex: "^@svc1bot:test.cat$".to_string(),
                    },
                    Namespace {
                        exclusive: true,
                        regex: "^@svc1_[0-9]+:test.cat$".to_string(),
                    },
                ],
                aliases: vec![],
                rooms: vec![],
            },
            rate_limited: None,
            protocols: None,
        };
        let svc1 = AppServiceConfig::new(svc1_registration, "svc1".to_string()).unwrap();

        let svc2_registration = Registration {
            id: "svc2".to_string(),
            url: None,
            as_token: "as_token_svc2".to_string(),
            hs_token: "hs_token_svc2".to_string(),
            sender_localpart: "svc2".to_string(),
            namespaces: Namespaces {
                users: vec![
                    Namespace {
                        exclusive: true,
                        regex: "^@svc2bot:test.cat$".to_string(),
                    },
                    Namespace {
                        exclusive: true,
                        regex: "^@svc2_[0-9]+:test.cat$".to_string(),
                    },
                ],
                aliases: vec![],
                rooms: vec![],
            },
            rate_limited: None,
            protocols: None,
        };
        let svc2 = AppServiceConfig::new(svc2_registration, "svc2".to_string()).unwrap();

        let mut services = HashMap::new();
        services.insert("svc1".to_string(), svc1);
        services.insert("svc2".to_string(), svc2);

        Self { services }
    }
}

impl AppserviceRepository for TestApplicationSericeRepository {
    fn get_by_id(&self, id: &str) -> Option<&AppServiceConfig> {
        self.services.get(id)
    }

    fn get_by_token(&self, token: &str) -> Option<&AppServiceConfig> {
        self.services
            .iter()
            .find(|(_, config)| config.appservice_token() == token)
            .map(|(_, config)| config)
    }

    fn all(&self) -> Vec<AppServiceConfig> {
        self.services.values().map(|c| c.to_owned()).collect()
    }
}
