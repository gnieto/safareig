use std::{collections::BTreeMap, sync::Arc};

use safareig_authentication::command::register::RegisterCommand;
use safareig_core::{
    auth::Identity,
    command::Command,
    ruma::api::client::{
        account::{
            register,
            register::{LoginType, RegistrationKind},
        },
        error::ErrorKind,
        uiaa::UiaaResponse,
    },
};
use safareig_devices::storage::DeviceStorage;
use safareig_testing::{error::RumaErrorExtension, uiaa_auth_data, TestingApp};
use safareig_uiaa::UiaaWrapper;
use safareig_users::storage::UserStorage;

#[tokio::test]
async fn returns_uiaa_info_with_stages_with_missing_auth() {
    let mut cfg = TestingApp::default_config();
    let stages = ["a".to_string(), "b".to_string(), "c".to_string()];
    cfg.uiaa.register_stages = stages.to_vec();
    let db = TestingApp::with_config(cfg).await;
    let cmd = db.command::<UiaaWrapper<RegisterCommand>>();

    let input = register::v3::Request {
        kind: RegistrationKind::User,
        password: None,
        username: None,
        device_id: None,
        initial_device_display_name: None,
        auth: None,
        inhibit_login: false,
        login_type: None,
        refresh_token: false,
    };

    let result = cmd.execute(input, Identity::None).await;

    assert!(result.is_err());
    let uiaa = match result {
        Err(UiaaResponse::AuthResponse(uiaa_info)) => uiaa_info,
        _ => unreachable!("expected UIAA info response"),
    };
    let response_stages: Vec<String> = uiaa.flows[0]
        .stages
        .iter()
        .map(|stage| stage.to_string())
        .collect();
    assert_eq!(&stages, response_stages.as_slice());
    assert!(uiaa.completed.is_empty());
    assert!(uiaa.session.is_some());
    assert!(uiaa.auth_error.is_none());
}

#[tokio::test]
async fn register_user() {
    let cfg = TestingApp::default_config();
    let db = TestingApp::with_config(cfg).await;
    let cmd = db.command::<UiaaWrapper<RegisterCommand>>();

    let input = register::v3::Request {
        kind: RegistrationKind::User,
        password: Some("secret".to_string()),
        username: Some("name".to_string()),
        device_id: None,
        initial_device_display_name: None,
        auth: None,
        inhibit_login: false,
        login_type: None,
        refresh_token: false,
    };
    let uiaa = cmd.execute(input, Identity::None).await.err().unwrap();
    let session = match uiaa {
        UiaaResponse::AuthResponse(uiaa) => uiaa.session,
        _ => None,
    };

    let auth_data = uiaa_auth_data(session, "m.login.dummy", BTreeMap::default());
    let input = register::v3::Request {
        kind: RegistrationKind::User,
        password: Some("secret".to_string()),
        username: Some("name".to_string()),
        device_id: None,
        initial_device_display_name: None,
        auth: Some(auth_data),
        inhibit_login: false,
        login_type: None,
        refresh_token: false,
    };
    let response = cmd.execute(input, Identity::None).await.unwrap();
    let user_id = response.user_id;
    let device_id = response
        .device_id
        .expect("device id returned from register");
    let access_token = response.access_token.unwrap();

    let users = db.service::<Arc<dyn UserStorage>>();
    let user = users
        .user_by_id(&user_id)
        .await
        .unwrap()
        .expect("user has been generated");
    assert_eq!(user.user_id, user_id);
    assert!(user.password.is_some());

    let devices = db.service::<Arc<dyn DeviceStorage>>();
    let device = devices
        .get_device(&user.user_id, &device_id)
        .await
        .unwrap()
        .expect("device has been generated");
    assert_eq!(device.current_auth, Some(access_token));
    assert_eq!(device.ruma_device.device_id, device_id);
}

#[tokio::test]
async fn register_with_initial_display_name() {
    let cfg = TestingApp::default_config();
    let db = TestingApp::with_config(cfg).await;
    let cmd = db.command::<UiaaWrapper<RegisterCommand>>();

    let input = register::v3::Request {
        kind: RegistrationKind::User,
        password: Some("secret".to_string()),
        username: Some("name".to_string()),
        device_id: None,
        initial_device_display_name: Some("device name".to_string()),
        auth: None,
        inhibit_login: false,
        login_type: None,
        refresh_token: false,
    };
    let uiaa = cmd.execute(input, Identity::None).await.err().unwrap();
    let session = match uiaa {
        UiaaResponse::AuthResponse(uiaa) => uiaa.session,
        _ => None,
    };

    let auth_data = uiaa_auth_data(session, "m.login.dummy", BTreeMap::default());
    let input = register::v3::Request {
        kind: RegistrationKind::User,
        password: Some("secret".to_string()),
        username: Some("name".to_string()),
        device_id: None,
        initial_device_display_name: Some("device name".to_string()),
        auth: Some(auth_data),
        inhibit_login: false,
        login_type: None,
        refresh_token: false,
    };
    let response = cmd.execute(input, Identity::None).await.unwrap();
    let user_id = response.user_id;
    let device_id = response
        .device_id
        .expect("device id returned from register");
    let access_token = response.access_token.unwrap();

    let users = db.service::<Arc<dyn UserStorage>>();
    let user = users
        .user_by_id(&user_id)
        .await
        .unwrap()
        .expect("user has been generated");
    assert_eq!(user.user_id, user_id);
    assert!(user.password.is_some());

    let devices = db.service::<Arc<dyn DeviceStorage>>();

    let device = devices
        .get_device(&user.user_id, &device_id)
        .await
        .unwrap()
        .expect("device has been generated");

    assert_eq!(
        device.ruma_device.display_name,
        Some("device name".to_string())
    );
    assert_eq!(device.current_auth, Some(access_token));
    assert_eq!(device.ruma_device.device_id, device_id);
}

#[tokio::test]
async fn does_not_register_historical_user_ids() {
    let response = register_with_username("!test").await.err().unwrap();

    let matrix_error = match response {
        UiaaResponse::MatrixError(matrix_error) => matrix_error,
        _ => panic!("expected matrix error"),
    };

    assert_eq!(matrix_error.kind(), ErrorKind::InvalidUsername);
}

#[tokio::test]
async fn does_not_register_user_with_distinct_server_name() {
    let response = register_with_username("@test:invalid.cat")
        .await
        .err()
        .unwrap();

    let matrix_error = match response {
        UiaaResponse::MatrixError(matrix_error) => matrix_error,
        _ => panic!("expected matrix error"),
    };

    assert_eq!(matrix_error.kind(), ErrorKind::InvalidUsername);
}

#[tokio::test]
async fn it_does_not_login_a_user_with_inhibit_login() {
    let response = register_with_inhibit().await.unwrap();

    assert!(response.access_token.is_none());
    assert!(response.device_id.is_none());
}

#[tokio::test]
async fn it_regsisters_appservice_user() {
    use safareig_core::ruma::user_id;

    let mut cfg = TestingApp::default_config();
    cfg.appservices
        .insert("test".to_string(), "tests/test_appservice.yaml".into());
    let db = TestingApp::with_config(cfg).await;
    let cmd = db.command::<UiaaWrapper<RegisterCommand>>();

    let input = register::v3::Request {
        kind: RegistrationKind::User,
        password: None,
        username: Some("test_name".to_string()),
        device_id: None,
        initial_device_display_name: None,
        auth: None,
        inhibit_login: false,
        login_type: Some(LoginType::ApplicationService),
        refresh_token: false,
    };
    let user_id = user_id!("@test_name:test.cat");
    let id = Identity::AppService(Some(user_id.to_owned()), "test".to_string());
    let response = cmd.execute(input, id).await.unwrap();

    assert_eq!(response.user_id, user_id);
}

async fn register_with_username(username: &str) -> Result<register::v3::Response, UiaaResponse> {
    let cfg = TestingApp::default_config();
    let db = TestingApp::with_config(cfg).await;
    let cmd = db.command::<UiaaWrapper<RegisterCommand>>();

    let input = register::v3::Request {
        kind: RegistrationKind::User,
        password: Some("secret".to_string()),
        username: Some(username.to_string()),
        device_id: None,
        initial_device_display_name: None,
        auth: None,
        inhibit_login: false,
        login_type: None,
        refresh_token: false,
    };
    let uiaa = cmd.execute(input, Identity::None).await.err().unwrap();
    let session = match uiaa {
        UiaaResponse::AuthResponse(uiaa) => uiaa.session,
        _ => None,
    };

    let auth_data = uiaa_auth_data(session, "m.login.dummy", BTreeMap::default());
    let input = register::v3::Request {
        kind: RegistrationKind::User,
        password: Some("secret".to_string()),
        username: Some(username.to_string()),
        device_id: None,
        initial_device_display_name: None,
        auth: Some(auth_data),
        inhibit_login: false,
        login_type: None,
        refresh_token: false,
    };

    cmd.execute(input, Identity::None).await
}

async fn register_with_inhibit() -> Result<register::v3::Response, UiaaResponse> {
    let cfg = TestingApp::default_config();
    let db = TestingApp::with_config(cfg).await;
    let cmd = db.command::<UiaaWrapper<RegisterCommand>>();

    let input = register::v3::Request {
        kind: RegistrationKind::User,
        password: Some("secret".to_string()),
        username: Some("user".to_string()),
        device_id: None,
        initial_device_display_name: None,
        auth: None,
        inhibit_login: true,
        login_type: None,
        refresh_token: false,
    };
    let uiaa = cmd.execute(input, Identity::None).await.err().unwrap();
    let session = match uiaa {
        UiaaResponse::AuthResponse(uiaa) => uiaa.session,
        _ => None,
    };

    let auth_data = uiaa_auth_data(session, "m.login.dummy", BTreeMap::default());
    let input = register::v3::Request {
        kind: RegistrationKind::User,
        password: Some("secret".to_string()),
        username: Some("user".to_string()),
        device_id: None,
        initial_device_display_name: None,
        auth: Some(auth_data),
        inhibit_login: true,
        login_type: None,
        refresh_token: false,
    };

    cmd.execute(input, Identity::None).await
}
