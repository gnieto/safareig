use safareig_authentication::command::availability::UsernameAvailabilityCommand;
use safareig_core::{
    auth::Identity, command::Command, ruma::api::client::account::get_username_availability,
};
use safareig_testing::TestingApp;

#[tokio::test]
async fn non_available_name() {
    let db = TestingApp::default().await;
    let user = db.register().await;

    let cmd = db.command::<UsernameAvailabilityCommand>();
    let response = cmd
        .execute(
            get_username_availability::v3::Request {
                username: user.user().localpart().to_string(),
            },
            Identity::None,
        )
        .await
        .unwrap();

    assert!(!response.available);
}

#[tokio::test]
async fn available_name() {
    let db = TestingApp::default().await;
    let cmd = db.command::<UsernameAvailabilityCommand>();

    let response = cmd
        .execute(
            get_username_availability::v3::Request {
                username: "safareig".to_string(),
            },
            Identity::None,
        )
        .await
        .unwrap();

    assert!(response.available);
}
