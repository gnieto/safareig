pub mod paseto;
pub mod random;

use std::{collections::BTreeMap, sync::Arc, time::SystemTime};

use safareig_core::{
    auth::Identity,
    config::HomeserverConfig,
    ruma::{
        api::client::uiaa::{AuthData, UserIdentifier},
        DeviceId, OwnedUserId, UserId,
    },
    Inject,
};
use safareig_uiaa::{error::UiaaError, StageData, UiaaStage};
use safareig_users::storage::UserStorage;
use serde_json::Value;
use thiserror::Error;
use tokio::runtime::Handle;

use super::PasswordHasher;

#[derive(Clone, Inject)]
pub struct LoginStage {
    users: Arc<dyn UserStorage>,
    password_hasher: Arc<dyn PasswordHasher + Send + Sync>,
    config: Arc<HomeserverConfig>,
}

impl UiaaStage for LoginStage {
    fn stage_name(&self) -> &str {
        "m.login.password"
    }

    fn params(&self, _: &mut safareig_uiaa::SessionState) -> Option<BTreeMap<String, Value>> {
        None
    }

    fn execute(
        &self,
        identity: &Identity,
        _: &mut safareig_uiaa::SessionState,
        stage_data: StageData,
    ) -> Result<(), UiaaError> {
        let auth_data = stage_data.auth_data().ok_or(UiaaError::Forbidden)?;
        let authorized = match auth_data {
            AuthData::Password(password) => {
                let uiaa_user: OwnedUserId = match &password.identifier {
                    UserIdentifier::UserIdOrLocalpart(id) => id
                        .parse()
                        .or_else(|_| format!("@{}:{}", id, self.config.server_name).parse())
                        .map_err(|_| UiaaError::Custom("invalid user identifier".to_string()))?,
                    _ => {
                        return Err(UiaaError::Custom(
                            "user identifier not implemented".to_string(),
                        ))
                    }
                };

                if uiaa_user != identity.user() {
                    return Err(UiaaError::NonRetriableForbidden);
                }

                let user = tokio::task::block_in_place(|| {
                    Handle::current()
                        .block_on(async move { self.users.user_by_id(&uiaa_user).await })
                })
                .map_err(|e| UiaaError::Custom(e.to_string()))?;

                let user = match user {
                    Some(user) => user,
                    None => return Err(UiaaError::Forbidden),
                };

                let hash = match user.password {
                    Some(password) => password,
                    None => return Err(UiaaError::Forbidden),
                };

                self.password_hasher
                    .check_password(&hash, &password.password)
            }
            _ => return Err(UiaaError::Forbidden),
        };

        if !authorized {
            tracing::error!("Returning UiaaError::Forbidden");
            return Err(UiaaError::Forbidden);
        }

        Ok(())
    }
}

#[async_trait::async_trait]
pub trait TokenHandler: Send + Sync {
    async fn generate_token(&self, user_id: &UserId, device_id: &DeviceId) -> UserTokens;
    async fn parse_token(&self, access_token: &str) -> Result<Box<dyn AccessToken>, TokenError>;
}

#[derive(Error, Debug)]
pub enum TokenError {
    #[error("Token has expired")]
    ExpiredToken,
    #[error("{0}")]
    Unknown(String),
}

pub struct UserTokens {
    access_token: String,
    refresh_token: Option<String>,
}

impl UserTokens {
    pub fn access_token(&self) -> &str {
        &self.access_token
    }

    pub fn refresh_token(&self) -> Option<&str> {
        self.refresh_token.as_deref()
    }
}

pub trait AccessToken: Send + Sync {
    fn token_identifier(&self) -> &str;
    fn user_id(&self) -> &UserId;
    fn device_id(&self) -> &DeviceId;
    fn expires(&self) -> Option<SystemTime>;
}

pub fn random_string() -> String {
    use rand::{distributions::Alphanumeric, thread_rng, Rng};

    thread_rng()
        .sample_iter(&Alphanumeric)
        .take(30)
        .map(char::from)
        .collect()
}
