use lazy_static::lazy_static;
use opentelemetry::metrics::Counter;

lazy_static! {
    pub static ref USER_REGISTRATIONS: Counter<u64> =
        opentelemetry::global::meter("safareig/client")
            .u64_counter("user_registration")
            .with_description("Amount of registered users to current homeserver")
            .init();
}
