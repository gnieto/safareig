use std::{sync::Arc, time::SystemTime};

use rusty_paseto::prelude::*;
use safareig::storage::ServerConfig;
use safareig_core::{
    config::HomeserverConfig,
    ruma::{DeviceId, OwnedDeviceId, OwnedUserId, UserId},
    storage::StorageError,
};
use serde::Deserialize;
use sqlx::types::chrono;
use time::format_description::well_known::Rfc3339;

use super::{random_string, AccessToken, TokenError, TokenHandler, UserTokens};

pub struct PasetoTokenHandler {
    server_config: Arc<dyn ServerConfig>,
    config: Arc<HomeserverConfig>,
}

impl PasetoTokenHandler {
    pub fn new(server_config: Arc<dyn ServerConfig>, config: Arc<HomeserverConfig>) -> Self {
        Self {
            server_config,
            config,
        }
    }
}

#[async_trait::async_trait]
impl TokenHandler for PasetoTokenHandler {
    async fn generate_token(&self, user_id: &UserId, device_id: &DeviceId) -> UserTokens {
        let key = initialize_symetric_key(self.server_config.as_ref())
            .await
            .unwrap();
        let expiry_time = (time::OffsetDateTime::now_utc() + self.config.auth.expiry_time)
            .format(&Rfc3339)
            .unwrap();
        let jti = random_string();

        let access_token = PasetoBuilder::<V4, Local>::default()
            .set_claim(ExpirationClaim::try_from(expiry_time).unwrap())
            .set_claim(CustomClaim::try_from(("user_id", user_id.as_str())).unwrap())
            .set_claim(CustomClaim::try_from(("device_id", device_id.as_str())).unwrap())
            .set_claim(TokenIdentifierClaim::from(jti.as_str()))
            .build(&key)
            .unwrap();

        let month = (time::OffsetDateTime::now_utc() + time::Duration::days(30))
            .format(&Rfc3339)
            .unwrap();

        let jti = random_string();
        let refresh_token = PasetoBuilder::<V4, Local>::default()
            .set_claim(ExpirationClaim::try_from(month).unwrap())
            .set_claim(CustomClaim::try_from(("user_id", user_id.as_str())).unwrap())
            .set_claim(CustomClaim::try_from(("device_id", device_id.as_str())).unwrap())
            .set_claim(TokenIdentifierClaim::from(jti.as_str()))
            .build(&key)
            .unwrap();

        UserTokens {
            access_token,
            refresh_token: Some(refresh_token),
        }
    }

    async fn parse_token(
        &self,
        access_token: &str,
    ) -> Result<Box<dyn super::AccessToken>, TokenError> {
        let key = initialize_symetric_key(self.server_config.as_ref())
            .await
            .map_err(|e| TokenError::Unknown(e.to_string()))?;
        let access_token = PasetoParser::<V4, Local>::default()
            .parse(access_token, &key)
            .map_err(|e| match e {
                GenericParserError::ClaimError { source } => match source {
                    PasetoClaimError::Expired => TokenError::ExpiredToken,
                    s => TokenError::Unknown(s.to_string()),
                },
                _ => TokenError::Unknown(e.to_string()),
            })?;

        let claims: PasetoClaims = serde_json::from_value(access_token).unwrap();

        Ok(Box::new(claims))
    }
}

pub async fn initialize_symetric_key(
    server_config: &dyn ServerConfig,
) -> Result<PasetoSymmetricKey<V4, Local>, StorageError> {
    let existing_key = server_config.get_config("paseto_key").await?;
    let symetric_key = match existing_key {
        Some(key) => PasetoSymmetricKey::from(Key::from(key.as_slice())),
        None => {
            let key = Key::try_new_random().unwrap();
            server_config.put_config("paseto_key", key.as_ref()).await?;

            PasetoSymmetricKey::from(key)
        }
    };

    Ok(symetric_key)
}

#[derive(Deserialize)]
struct PasetoClaims {
    #[serde(rename = "jti")]
    token_id: String,
    #[serde(rename = "exp")]
    expires: chrono::DateTime<chrono::Utc>,
    user_id: OwnedUserId,
    device_id: OwnedDeviceId,
}

impl AccessToken for PasetoClaims {
    fn token_identifier(&self) -> &str {
        self.token_id.as_str()
    }

    fn user_id(&self) -> &UserId {
        &self.user_id
    }

    fn device_id(&self) -> &DeviceId {
        &self.device_id
    }

    fn expires(&self) -> Option<SystemTime> {
        Some(self.expires.into())
    }
}
