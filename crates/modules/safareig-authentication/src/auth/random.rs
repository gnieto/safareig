use std::sync::Arc;

use safareig_core::ruma::{OwnedDeviceId, OwnedUserId};
use safareig_devices::storage::DeviceStorage;
use tokio::runtime::Handle;

use super::{random_string, AccessToken, TokenError, TokenHandler};

pub struct RandomTokenHandler {
    devices: Arc<dyn DeviceStorage>,
}

#[async_trait::async_trait]
impl TokenHandler for RandomTokenHandler {
    async fn generate_token(
        &self,
        _user_id: &safareig_core::ruma::UserId,
        _device_id: &safareig_core::ruma::DeviceId,
    ) -> super::UserTokens {
        super::UserTokens {
            access_token: random_string(),
            refresh_token: None,
        }
    }

    async fn parse_token(
        &self,
        access_token: &str,
    ) -> Result<Box<dyn super::AccessToken>, TokenError> {
        let (user, device) = tokio::task::block_in_place(|| {
            Handle::current()
                .block_on(async move { self.devices.get_auth(access_token.to_string()).await })
        })
        .map_err(|e| TokenError::Unknown(e.to_string()))?
        .ok_or_else(|| TokenError::Unknown("could not find auth token".to_string()))?;

        Ok(Box::new(AuthToken {
            token: access_token.to_string(),
            user_id: user,
            device,
        }))
    }
}

struct AuthToken {
    token: String,
    user_id: OwnedUserId,
    device: OwnedDeviceId,
}

impl AccessToken for AuthToken {
    fn token_identifier(&self) -> &str {
        &self.token
    }

    fn user_id(&self) -> &safareig_core::ruma::UserId {
        &self.user_id
    }

    fn device_id(&self) -> &safareig_core::ruma::DeviceId {
        &self.device
    }

    fn expires(&self) -> Option<std::time::SystemTime> {
        None
    }
}
