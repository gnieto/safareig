use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    error::ResourceNotFound,
    ruma::api::client::{
        account::{deactivate, ThirdPartyIdRemovalStatus},
        uiaa::{self},
    },
    Inject,
};
use safareig_uiaa::{SessionState, UiaaCommand};
use safareig_users::storage::UserStorage;

#[derive(Inject)]
pub struct DeactivateUserCommand {
    user_storage: Arc<dyn UserStorage>,
}

#[async_trait]
impl UiaaCommand for DeactivateUserCommand {
    type Input = deactivate::v3::Request;
    type Output = deactivate::v3::Response;
    type Error = uiaa::UiaaResponse;

    async fn execute(
        &self,
        _input: Self::Input,
        id: Identity,
        _session: Option<SessionState>,
    ) -> Result<Self::Output, Self::Error> {
        let mut user = self
            .user_storage
            .user_by_id(id.user())
            .await
            .map_err(|e| uiaa::UiaaResponse::MatrixError(e.into()))?
            .ok_or(ResourceNotFound)
            .map_err(|e| uiaa::UiaaResponse::MatrixError(e.into()))?;

        user.deactivate();

        self.user_storage
            .update_user(&user)
            .await
            .map_err(|e| uiaa::UiaaResponse::MatrixError(e.into()))?;

        Ok(deactivate::v3::Response {
            // TODO: Change this once 3pid gets implemented
            id_server_unbind_result: ThirdPartyIdRemovalStatus::Success,
        })
    }
}
