use std::{
    sync::Arc,
    time::{Duration, SystemTime},
};

use async_trait::async_trait;
use safareig_appservices::AppserviceRepository;
use safareig_core::{
    auth::Identity,
    command::Command,
    config::HomeserverConfig,
    ruma::{
        api::client::{
            error::{ErrorBody, ErrorKind},
            session::login::{self, v3::LoginInfo},
            uiaa::{UiaaResponse, UserIdentifier},
        },
        exports::http::StatusCode,
        OwnedDeviceId, OwnedUserId, UserId,
    },
    Inject,
};
use safareig_devices::storage::Device;
use safareig_users::storage::{AccountType, UserStorage};

use super::register::UserLogin;
use crate::{auth::TokenHandler, AccountError, PasswordHasher};

#[derive(Inject)]
pub struct LoginCommand {
    user_storage: Arc<dyn UserStorage>,
    config: Arc<HomeserverConfig>,
    user_login: UserLogin,
    password_hasher: Arc<dyn PasswordHasher + Send + Sync>,
    token_handler: Arc<dyn TokenHandler>,
    appservice_repository: Option<Arc<dyn AppserviceRepository>>,
}

#[async_trait]
impl Command for LoginCommand {
    type Input = login::v3::Request;
    type Output = login::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(
        &self,
        input: Self::Input,
        identity: Identity,
    ) -> Result<Self::Output, Self::Error> {
        let (user_id, device): (OwnedUserId, Device) = match &input.login_info {
            LoginInfo::Password(password) => {
                let user_id = self.user_from_identifier(&password.identifier)?;
                self.authorize_user(&user_id, &input.login_info).await?;
                let device = self
                    .update_device(
                        &user_id,
                        input.device_id.clone(),
                        input.initial_device_display_name.clone(),
                    )
                    .await?;

                (user_id, device)
            }
            LoginInfo::ApplicationService(app_service) => {
                let user_id = self.user_from_identifier(&app_service.identifier)?;
                self.authorize_appservice_user(&user_id, &input.login_info, identity)
                    .await?;
                let device = self
                    .update_device(&user_id, None, input.initial_device_display_name.clone())
                    .await?;

                (user_id, device)
            }
            LoginInfo::Token(_) => {
                tracing::error!("Token login is not supported yet");
                return Err(AccountError::UnsupportedLoginMethod("token".to_string()).into());
            }
            LoginInfo::_Custom(custom_login) => {
                return Err(
                    AccountError::UnsupportedLoginMethod(format!("{:?}", custom_login)).into(),
                );
            }
        };

        let current_auth = device.current_auth.unwrap();
        // TODO: Unwrap token parsing
        let token = self
            .token_handler
            .parse_token(current_auth.as_str())
            .await
            .unwrap();
        let expires_in = token.expires().map(|expiry_time| {
            expiry_time
                .duration_since(SystemTime::now())
                .unwrap_or_else(|_| Duration::from_secs(0))
        });

        #[cfg(not(feature = "deprecated"))]
        let response = login::v3::Response {
            user_id,
            access_token: current_auth,
            device_id: device.ruma_device.device_id,
            well_known: None,
            home_server: Some(self.config.server_name.clone()),
            expires_in,
            refresh_token: device.refresh_token,
        };
        #[cfg(feature = "deprecated")]
        let response = login::v3::Response {
            user_id,
            access_token: current_auth,
            device_id: device.ruma_device.device_id,
            well_known: None,
            home_server: Some(self.config.server_name.clone()),
            expires_in,
            refresh_token: None,
        };

        Ok(response)
    }
}

impl LoginCommand {
    async fn authorize_user(
        &self,
        user_id: &UserId,
        login_info: &LoginInfo,
    ) -> Result<(), safareig_core::ruma::api::client::Error> {
        let user = self
            .user_storage
            .user_by_id(user_id)
            .await
            .ok()
            .flatten()
            .ok_or_else(|| ::safareig_core::ruma::api::client::Error {
                body: ErrorBody::Standard {
                    kind: ErrorKind::InvalidUsername,
                    message: "missing user".to_string(),
                },
                status_code: StatusCode::FORBIDDEN,
            })?;

        if !user.is_active() {
            return Err(::safareig_core::ruma::api::client::Error {
                status_code: StatusCode::UNAUTHORIZED,
                body: ErrorBody::Standard {
                    kind: ErrorKind::UserDeactivated,
                    message: "user is deactivated".to_string(),
                },
            });
        }

        let is_valid = match (login_info, &user.password) {
            (LoginInfo::Password(password), Some(encoded_password)) => self
                .password_hasher
                .check_password(encoded_password, &password.password),
            _ => false,
        };

        if !is_valid {
            return Err(safareig_core::ruma::api::client::Error {
                status_code: StatusCode::FORBIDDEN,
                body: ErrorBody::Standard {
                    kind: ErrorKind::forbidden(),
                    message: "invalid credentials".to_string(),
                },
            });
        }

        Ok(())
    }

    async fn authorize_appservice_user(
        &self,
        user_id: &UserId,
        login_info: &LoginInfo,
        identity: Identity,
    ) -> Result<(), AccountError> {
        if let Identity::AppService(_, identity) = identity {
            let user = self
                .user_storage
                .user_by_id(user_id)
                .await
                .ok()
                .flatten()
                .ok_or(AccountError::InvalidAppserviceUser)?;

            match user.kind {
                AccountType::Appservice => (),
                AccountType::User => return Err(AccountError::InvalidAppserviceUser),
            }

            if let Some(appservice_repository) = &self.appservice_repository {
                let appservice_data = appservice_repository
                    .get_by_id(&identity)
                    .ok_or(AccountError::InvalidAppserviceUser)?;

                if !appservice_data.is_user_exclusive(user_id) {
                    return Err(AccountError::InvalidAppserviceUser);
                }
            } else {
                tracing::warn!("Appservice not supported");
                return Err(AccountError::InvalidAppserviceUser);
            }
        } else {
            return Err(AccountError::MismatchingAppservice);
        }

        Ok(())
    }

    async fn update_device(
        &self,
        user: &UserId,
        device: Option<OwnedDeviceId>,
        display_name: Option<String>,
    ) -> Result<Device, safareig_core::ruma::api::client::Error> {
        let device = self
            .user_login
            .user_login(user, device, display_name)
            .await
            .map_err(|e| match e {
                UiaaResponse::MatrixError(e) => e,
                UiaaResponse::AuthResponse(_e) => ::safareig_core::ruma::api::client::Error {
                    body: ErrorBody::Standard {
                        kind: ErrorKind::Unknown,
                        message: "uiaa error".to_string(),
                    },
                    status_code: StatusCode::INTERNAL_SERVER_ERROR,
                },
            })?;

        Ok(device)
    }

    fn user_from_identifier(
        &self,
        identifier: &Option<UserIdentifier>,
    ) -> Result<OwnedUserId, AccountError> {
        match identifier {
            Some(user_identifier) => match user_identifier {
                UserIdentifier::Email { .. }
                | UserIdentifier::Msisdn { .. }
                | UserIdentifier::PhoneNumber { .. }
                | UserIdentifier::_CustomThirdParty(_) => Err(AccountError::MissingUsername),
                UserIdentifier::UserIdOrLocalpart(id) => id
                    .parse()
                    .or_else(|_| format!("@{}:{}", id, self.config.server_name).parse())
                    .map_err(|e: safareig_core::ruma::IdParseError| AccountError::InvalidUsername),
            },
            None => Err(AccountError::MissingUsername),
        }
    }
}
