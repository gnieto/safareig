use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity, command::Command, ruma::api::client::account::get_username_availability, Inject,
};
use safareig_users::storage::{User, UserStorage};

#[derive(Inject)]
pub struct UsernameAvailabilityCommand {
    users: Arc<dyn UserStorage>,
}

#[async_trait]
impl Command for UsernameAvailabilityCommand {
    type Input = get_username_availability::v3::Request;
    type Output = get_username_availability::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, _: Identity) -> Result<Self::Output, Self::Error> {
        let all_users: Vec<_> = self
            .users
            .all_users()
            .await?
            .into_iter()
            .filter(User::is_active)
            .collect();

        let matching_user = all_users
            .iter()
            .find(|user| user.user_id.localpart() == input.username);

        Ok(get_username_availability::v3::Response {
            available: matching_user.is_none(),
        })
    }
}
