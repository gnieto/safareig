use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    command::Command,
    config::HomeserverConfig,
    ruma::api::client::session::{get_login_types, get_login_types::v3::LoginType},
    Inject,
};

#[derive(Inject)]
pub struct LoginTypesCommand {
    config: Arc<HomeserverConfig>,
}

#[async_trait]
impl Command for LoginTypesCommand {
    type Input = get_login_types::v3::Request;
    type Output = get_login_types::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, _: Self::Input, _: Identity) -> Result<Self::Output, Self::Error> {
        let flows: Vec<LoginType> = self
            .config
            .uiaa
            .login_stages
            .iter()
            .map(|stage| {
                // TODO: Allow configure default config per login stage
                LoginType::new(stage.as_str(), serde_json::Map::default()).unwrap()
            })
            .collect();

        let response = get_login_types::v3::Response::new(flows);

        Ok(response)
    }
}
