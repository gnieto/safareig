use async_trait::async_trait;
use safareig_core::{auth::Identity, command::Command, ruma::api::client::account::whoami, Inject};

#[derive(Default, Inject)]
pub struct WhoamiCommand;

#[async_trait]
impl Command for WhoamiCommand {
    type Input = whoami::v3::Request;
    type Output = whoami::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, _: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let response = whoami::v3::Response {
            user_id: id.user().to_owned(),
            device_id: id.device().map(|device| device.to_owned()),
            is_guest: false,
        };

        Ok(response)
    }
}
