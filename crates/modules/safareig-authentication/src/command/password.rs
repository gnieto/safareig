use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    error::ResourceNotFound,
    ruma::api::client::{
        account::change_password,
        uiaa::{self},
    },
    Inject,
};
use safareig_devices::service::DeviceManager;
use safareig_uiaa::{SessionState, UiaaCommand};
use safareig_users::storage::UserStorage;

use crate::PasswordHasher;

#[derive(Inject)]
pub struct ChangePasswordCommand {
    user_storage: Arc<dyn UserStorage>,
    password_hasher: Arc<dyn PasswordHasher + Send + Sync>,
    device_manager: Arc<DeviceManager>,
}

#[async_trait]
impl UiaaCommand for ChangePasswordCommand {
    type Input = change_password::v3::Request;
    type Output = change_password::v3::Response;
    type Error = uiaa::UiaaResponse;

    async fn execute(
        &self,
        input: Self::Input,
        id: Identity,
        _session: Option<SessionState>,
    ) -> Result<Self::Output, Self::Error> {
        let new_password = self.password_hasher.hash(&input.new_password);

        let mut user = self
            .user_storage
            .user_by_id(id.user())
            .await
            .map_err(|e| uiaa::UiaaResponse::MatrixError(e.into()))?
            .ok_or_else(|| uiaa::UiaaResponse::MatrixError(ResourceNotFound.into()))?;
        user.password = Some(new_password);
        self.user_storage
            .update_user(&user)
            .await
            .map_err(|e| uiaa::UiaaResponse::MatrixError(e.into()))?;

        if input.logout_devices {
            self.device_manager
                .invalidate_sessions(id.user(), id.device())
                .await
                .map_err(|e| uiaa::UiaaResponse::MatrixError(e.into()))?
        }

        Ok(change_password::v3::Response::new())
    }
}
