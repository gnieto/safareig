use async_trait::async_trait;
use safareig_core::{
    auth::Identity, command::Command, ruma::api::client::account::get_3pids, Inject,
};

#[derive(Default, Inject)]
pub struct Get3PidCommand;

#[async_trait]
impl Command for Get3PidCommand {
    type Input = get_3pids::v3::Request;
    type Output = get_3pids::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, _: Self::Input, _id: Identity) -> Result<Self::Output, Self::Error> {
        let response = get_3pids::v3::Response::new(vec![]);

        Ok(response)
    }
}
