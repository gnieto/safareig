use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    command::Command,
    ruma::api::client::session::{logout, logout_all},
    Inject,
};
use safareig_devices::{service::DeviceManager, storage::DeviceStorage};

use crate::AccountError;

#[derive(Inject)]
pub struct LogoutCommand {
    device_manager: Arc<DeviceManager>,
}

#[async_trait]
impl Command for LogoutCommand {
    type Input = logout::v3::Request;
    type Output = logout::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(
        &self,
        _input: Self::Input,
        id: Identity,
    ) -> Result<Self::Output, Self::Error> {
        let device_id = id.device().ok_or(AccountError::MissingDeviceId)?;

        self.device_manager
            .delete_device(id.user(), device_id)
            .await?;

        Ok(logout::v3::Response {})
    }
}

#[derive(Inject)]
pub struct LogoutAllCommand {
    device_manager: Arc<DeviceManager>,
    device_storage: Arc<dyn DeviceStorage>,
}

#[async_trait]
impl Command for LogoutAllCommand {
    type Input = logout_all::v3::Request;
    type Output = logout_all::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(
        &self,
        _input: Self::Input,
        id: Identity,
    ) -> Result<Self::Output, Self::Error> {
        let devices = self.device_storage.devices_by_user(id.user()).await?;

        for device in devices {
            self.device_manager
                .delete_device(id.user(), &device.ruma_device.device_id)
                .await?;
        }

        Ok(logout_all::v3::Response {})
    }
}
