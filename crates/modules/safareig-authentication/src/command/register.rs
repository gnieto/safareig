use std::{
    sync::Arc,
    time::{Duration, SystemTime},
};

use async_trait::async_trait;
use rusty_ulid::Ulid;
use safareig_appservices::Appservice;
use safareig_core::{
    auth::Identity,
    bus::{Bus, BusMessage},
    config::HomeserverConfig,
    error::error_chain,
    listener::Dispatcher,
    ruma::{
        api::client::{
            account::register::{self, LoginType, RegistrationKind},
            error::{ErrorBody, ErrorKind},
            uiaa::{self},
            Error,
        },
        exports::http::StatusCode,
        DeviceId, OwnedDeviceId, OwnedUserId, UserId,
    },
    Inject,
};
use safareig_devices::{
    service::DeviceManager,
    storage::{Device, DeviceStorage},
};
use safareig_uiaa::{SessionState, UiaaCommand};
use safareig_users::storage::{User, UserStorage};
use serde::{Deserialize, Serialize};
use serde_json::Value;

use crate::{auth::TokenHandler, metrics::USER_REGISTRATIONS, AccountError, PasswordHasher};

#[derive(Inject)]
pub struct RegisterCommand {
    user_storage: Arc<dyn UserStorage>,
    config: Arc<HomeserverConfig>,
    app_services: Appservice,
    user_login: UserLogin,
    password_hasher: Arc<dyn PasswordHasher + Send + Sync>,
    dispatcher: Arc<Dispatcher<UserRegistered>>,
    token_handler: Arc<dyn TokenHandler>,
}

#[async_trait]
impl UiaaCommand for RegisterCommand {
    type Input = register::v3::Request;
    type Output = register::v3::Response;
    type Error = uiaa::UiaaResponse;

    async fn execute(
        &self,
        input: Self::Input,
        id: Identity,
        session: Option<SessionState>,
    ) -> Result<Self::Output, Self::Error> {
        match id {
            Identity::AppService(_, appservice_id) => Ok(self
                .register_appserver(&input, appservice_id.as_str())
                .await?),
            _ => match session {
                Some(session) => Ok(self.register_user(&input, &session).await?),
                None => Err(uiaa::UiaaResponse::MatrixError(
                    AccountError::NotCurrentStage.into(),
                )),
            },
        }
    }

    fn extract_data(&self, input: &Self::Input, session: &mut SessionState) {
        let state = session.state_mut();

        if !state.contains_key("register_params") {
            let mut params = RegisterParams::from(input.clone());
            params.password = params.password.map(|p| self.password_hasher.hash(&p));

            match serde_json::to_string(&params) {
                Ok(serialized) => {
                    state.insert("register_params".to_string(), Value::String(serialized));
                }
                Err(e) => {
                    tracing::error!(
                        err = error_chain(&e).as_str(),
                        "could not serialize registration params",
                    );
                }
            }
        }
    }
}

impl RegisterCommand {
    pub async fn register_appserver(
        &self,
        input: &register::v3::Request,
        current_appservice_id: &str,
    ) -> Result<register::v3::Response, uiaa::UiaaResponse> {
        let username = input
            .username
            .as_ref()
            .ok_or(AccountError::MissingUsername)?;
        let user_id = self.sanitize_username(username)?;

        if let Some(appservice_id) = self.app_services.is_user_exclusive(&user_id) {
            if appservice_id != current_appservice_id {
                return Err(uiaa::UiaaResponse::MatrixError(Error {
                    status_code: StatusCode::FORBIDDEN,
                    body: ErrorBody::Standard {
                        kind: ErrorKind::Exclusive,
                        message: "given appservice does not control the user id namespace"
                            .to_string(),
                    },
                }));
            }
        }

        let maybe_user = self
            .user_storage
            .user_by_id(&user_id)
            .await
            .map_err(|e| uiaa::UiaaResponse::MatrixError(e.into()))?;

        if maybe_user.is_some() {
            return Err(AccountError::UsernameInUse.into());
        }

        let user = User::appservice(&user_id);
        self.user_storage
            .create_user(&user)
            .await
            .map_err(|e| uiaa::UiaaResponse::MatrixError(e.into()))?;

        let mut response = register::v3::Response::new(user_id);
        response.device_id = None;
        response.access_token = None;

        Ok(response)
    }

    pub async fn register_user(
        &self,
        input: &register::v3::Request,
        session: &SessionState,
    ) -> Result<register::v3::Response, uiaa::UiaaResponse> {
        let params = session
            .state()
            .get("register_params")
            .and_then(|v| v.as_str())
            .and_then(|v| serde_json::from_str(v).ok())
            .unwrap_or_else(|| {
                let mut params = RegisterParams::from(input.clone());
                params.password = params.password.map(|p| self.password_hasher.hash(&p));

                params
            });

        let user_id = self.sanitize_username(&params.username)?;

        if user_id.is_historical() {
            return Err(AccountError::InvalidUsername.into());
        }

        if self.is_user_exclusive(&user_id) {
            return Err(uiaa::UiaaResponse::MatrixError(Error {
                status_code: StatusCode::FORBIDDEN,
                body: ErrorBody::Standard {
                    kind: ErrorKind::Exclusive,
                    message: "username not allowed".to_string(),
                },
            }));
        }

        let maybe_user = self
            .user_storage
            .user_by_id(&user_id)
            .await
            .map_err(|e| uiaa::UiaaResponse::MatrixError(e.into()))?;

        if maybe_user.is_some() {
            return Err(AccountError::UsernameInUse.into());
        }

        let password = params.password.ok_or(AccountError::MissingPassword)?;
        let user = User::new(user_id.as_ref(), password);

        self.user_storage
            .create_user(&user)
            .await
            .map_err(|e| uiaa::UiaaResponse::MatrixError(e.into()))?;

        let device = if !params.inhibit_login {
            let device = self
                .user_login
                .user_login(
                    &user_id,
                    params.device_id.clone(),
                    params.initial_device_display_name.clone(),
                )
                .await?;

            Some(device)
        } else {
            None
        };

        USER_REGISTRATIONS.add(1, &[]);

        self.dispatcher
            .dispatch_event(UserRegistered { user })
            .await;

        let mut response = register::v3::Response::new(user_id.clone());
        if let Some(device) = device {
            let current_auth = device.current_auth.unwrap();
            let token = self
                .token_handler
                .parse_token(current_auth.as_str())
                .await
                .unwrap();
            let expires_in = token.expires().map(|expiry_time| {
                expiry_time
                    .duration_since(SystemTime::now())
                    .unwrap_or_else(|_| Duration::from_secs(0))
            });

            response.device_id = Some(device.ruma_device.device_id.to_owned());
            response.access_token = Some(current_auth);
            response.refresh_token = device.refresh_token;
            response.expires_in = expires_in;
        }

        Ok(response)
    }

    pub fn is_user_exclusive(&self, user_id: &UserId) -> bool {
        self.app_services.is_user_exclusive(user_id).is_some()
    }

    fn sanitize_username(&self, username: &str) -> Result<OwnedUserId, AccountError> {
        let server_name = &self.config.server_name;

        let user_id = username
            .parse()
            .map_err(|_| AccountError::InvalidUsername)
            .or_else(|_| {
                UserId::parse_with_server_name(username, server_name.as_ref())
                    .map_err(|_| AccountError::InvalidUsername)
            })?;

        if user_id.server_name() != self.config.server_name {
            return Err(AccountError::InvalidUsername);
        }

        if user_id.is_historical() {
            return Err(AccountError::InvalidUsername);
        }

        Ok(user_id)
    }

    fn generate_username() -> String {
        Ulid::generate().to_string().to_ascii_lowercase()
    }
}

#[derive(Deserialize, Serialize)]
struct RegisterParams {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub password: Option<String>,

    pub username: String,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub device_id: Option<OwnedDeviceId>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub initial_device_display_name: Option<String>,

    pub kind: RegistrationKind,

    pub inhibit_login: bool,

    pub login_type: Option<LoginType>,

    pub refresh_token: bool,
}

impl From<register::v3::Request> for RegisterParams {
    fn from(value: register::v3::Request) -> Self {
        Self {
            password: value.password,
            username: value
                .username
                .unwrap_or_else(RegisterCommand::generate_username),
            device_id: value.device_id,
            initial_device_display_name: value.initial_device_display_name,
            kind: value.kind,
            inhibit_login: value.inhibit_login,
            login_type: value.login_type,
            refresh_token: value.refresh_token,
        }
    }
}

#[derive(Clone, Inject)]
pub struct UserLogin {
    device_manager: Arc<DeviceManager>,
    device_storage: Arc<dyn DeviceStorage>,
    token_handler: Arc<dyn TokenHandler>,
    bus: Bus,
}

impl UserLogin {
    pub async fn user_login(
        &self,
        user_id: &UserId,
        device_id: Option<OwnedDeviceId>,
        display_name: Option<String>,
    ) -> Result<Device, uiaa::UiaaResponse> {
        let device_id = device_id.unwrap_or_else(DeviceId::new);
        let user_tokens = self.token_handler.generate_token(user_id, &device_id).await;

        let display_name = display_name.as_ref().map(|device_name| {
            let mut device_name = device_name.clone();
            device_name.truncate(50);

            device_name
        });

        let device = Device {
            ruma_device: safareig_core::ruma::api::client::device::Device {
                device_id: device_id.clone(),
                display_name,
                last_seen_ip: None,
                last_seen_ts: None,
            },
            current_auth: Some(user_tokens.access_token().to_string()),
            refresh_token: user_tokens.refresh_token().map(|token| token.to_string()),
            user: user_id.to_owned(),
        };
        self.device_manager
            .update_device(user_id, &device)
            .await
            .map_err(|e| uiaa::UiaaResponse::MatrixError(e.into()))?;

        self.device_storage
            .assign(user_tokens.access_token().to_string(), user_id, &device_id)
            .await
            .map_err(|e| uiaa::UiaaResponse::MatrixError(e.into()))?;

        self.bus
            .send_message(BusMessage::UserRegistered(user_id.to_owned()));

        Ok(device)
    }
}

pub struct UserRegistered {
    pub user: User,
}
