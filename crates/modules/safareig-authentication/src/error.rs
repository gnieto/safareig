use safareig_core::ruma::{
    api::client::{
        error::{ErrorBody, ErrorKind},
        uiaa::UiaaResponse,
        Error,
    },
    exports::http,
};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum AccountError {
    #[error("Missing username")]
    MissingUsername,
    #[error("Missing password")]
    MissingPassword,
    #[error("Username already taken")]
    UsernameInUse,
    #[error("Invalid username")]
    InvalidUsername,
    #[error("Not current stage")]
    NotCurrentStage,
    #[error("Mismatching app service")]
    MismatchingAppservice,
    #[error("Missing device identity")]
    MissingDeviceId,
    #[error("Unsupported login method {0}")]
    UnsupportedLoginMethod(String),
    #[error("User does not correspond to appservice or has not registered yet")]
    InvalidAppserviceUser,
}

impl From<AccountError> for UiaaResponse {
    fn from(error: AccountError) -> Self {
        UiaaResponse::MatrixError(error.into())
    }
}

impl From<AccountError> for safareig_core::ruma::api::client::Error {
    fn from(error: AccountError) -> Self {
        match error {
            AccountError::MissingUsername => Error {
                body: ErrorBody::Standard {
                    kind: ErrorKind::InvalidUsername,
                    message: error.to_string(),
                },
                status_code: http::StatusCode::BAD_REQUEST,
            },
            AccountError::MissingPassword => Error {
                body: ErrorBody::Standard {
                    kind: ErrorKind::InvalidParam,
                    message: error.to_string(),
                },
                status_code: http::StatusCode::BAD_REQUEST,
            },
            AccountError::UsernameInUse => Error {
                body: ErrorBody::Standard {
                    kind: ErrorKind::UserInUse,
                    message: error.to_string(),
                },
                status_code: http::StatusCode::BAD_REQUEST,
            },
            AccountError::InvalidUsername => Error {
                body: ErrorBody::Standard {
                    kind: ErrorKind::InvalidUsername,
                    message: error.to_string(),
                },
                status_code: http::StatusCode::BAD_REQUEST,
            },
            AccountError::MismatchingAppservice | AccountError::InvalidAppserviceUser => Error {
                body: ErrorBody::Standard {
                    kind: ErrorKind::forbidden(),
                    message: error.to_string(),
                },
                status_code: http::StatusCode::FORBIDDEN,
            },
            _ => Error {
                body: ErrorBody::Standard {
                    kind: ErrorKind::Unknown,
                    message: error.to_string(),
                },
                status_code: http::StatusCode::BAD_REQUEST,
            },
        }
    }
}
