use std::sync::Arc;

use safareig::storage::ServerConfig;
use safareig_appservices::AppserviceRepository;
use safareig_core::{
    command::{Container, InjectServices, Module, ServiceCollectionExtension},
    config::HomeserverConfig,
    ddi::{ServiceCollectionExt, ServiceProvider, ServiceResolverExt},
    listener::{Dispatcher, Listener},
};
use safareig_uiaa::{module::ContainerUIAAExt, UiaaStage};
use safareig_users::storage::UserStorage;

use crate::{
    auth::{paseto::PasetoTokenHandler, LoginStage, TokenHandler},
    command::{
        availability::UsernameAvailabilityCommand,
        login::LoginCommand,
        login_types::LoginTypesCommand,
        register::{RegisterCommand, UserLogin, UserRegistered},
        whoami::WhoamiCommand,
        ChangePasswordCommand, DeactivateUserCommand, Get3PidCommand, LogoutAllCommand,
        LogoutCommand,
    },
    ArgonHasher, PasswordHasher,
};

#[derive(Default)]
pub struct AuthenticationModule {}

#[async_trait::async_trait]
impl<R: safareig_core::command::Router> Module<R> for AuthenticationModule {
    fn register(&self, container: &mut Container, router: &mut R) {
        container.inject_endpoint::<_, UsernameAvailabilityCommand, _>(router);
        container.inject_endpoint::<_, LoginTypesCommand, _>(router);
        // container.inject_endpoint::<_, LoginCommand, _>(router);
        container.inject_endpoint::<_, WhoamiCommand, _>(router);
        container.inject_endpoint::<_, Get3PidCommand, _>(router);
        container.inject_endpoint::<_, LogoutCommand, _>(router);
        container.inject_endpoint::<_, LogoutAllCommand, _>(router);
        container.uiaa_endpoint::<_, DeactivateUserCommand, _>("password", router);
        container.uiaa_endpoint::<_, ChangePasswordCommand, _>("password", router);
        container.uiaa_endpoint::<_, RegisterCommand, _>("register", router);

        container.register_lazy_endpoint(
            |sp: &ServiceProvider| {
                let cmd = LoginCommand::new(
                    sp.get::<Arc<dyn UserStorage>>()?.clone(),
                    sp.get::<Arc<HomeserverConfig>>()?.clone(),
                    sp.get::<UserLogin>()?.clone(),
                    sp.get::<Arc<dyn PasswordHasher + Send + Sync>>()?.clone(),
                    sp.get::<Arc<dyn TokenHandler>>()?.clone(),
                    sp.get::<Arc<dyn AppserviceRepository>>().ok().cloned(),
                );

                Ok(cmd)
            },
            router,
        );

        container
            .service_collection
            .service_factory::<_, Arc<dyn TokenHandler>, _>(
                |sc: &Arc<dyn ServerConfig>, config: &Arc<HomeserverConfig>| {
                    let token_handler = PasetoTokenHandler::new(sc.clone(), config.clone());
                    let handler: Arc<dyn TokenHandler> = Arc::new(token_handler);

                    Ok(handler)
                },
            );
        container.service_collection.service_factory(|| {
            let hasher: Arc<dyn PasswordHasher + Send + Sync> = Arc::new(ArgonHasher::default());
            Ok(hasher)
        });

        container.service_collection.register::<UserLogin>();
        container.service_collection.service_factory_var(
            "m.login.password",
            |sp: &ServiceProvider| {
                let stage: Arc<dyn UiaaStage> = Arc::new(LoginStage::inject(sp)?);
                Ok(stage)
            },
        );

        container
            .service_collection
            .service_factory(|sp: &ServiceProvider| {
                let listeners = sp
                    .get_all::<Arc<dyn Listener<UserRegistered>>>()?
                    .into_iter()
                    .map(|t| t.1.clone())
                    .collect();

                Ok(Arc::new(Dispatcher::<UserRegistered>::new(listeners)))
            });
    }
}
