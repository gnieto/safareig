pub mod auth;
mod error;
pub(crate) mod metrics;
pub mod module;
pub use error::*;

pub mod command;

pub trait PasswordHasher {
    fn hash(&self, password: &str) -> String;
    fn check_password(&self, hash: &str, password: &str) -> bool;
}

#[derive(Default)]
pub struct ArgonHasher<'a> {
    config: argon2::Config<'a>,
}

impl<'a> PasswordHasher for ArgonHasher<'a> {
    fn hash(&self, password: &str) -> String {
        let salt = random_string();
        // Unwrap is safe here as it only depend on a well-defined config (which can be assumed, as we are using default config)
        let password =
            argon2::hash_encoded(password.as_bytes(), salt.as_bytes(), &self.config).unwrap();

        password
    }

    fn check_password(&self, hash: &str, password: &str) -> bool {
        argon2::verify_encoded(hash, password.as_bytes()).unwrap_or(false)
    }
}

fn random_string() -> String {
    use rand::{distributions::Alphanumeric, thread_rng, Rng};

    thread_rng()
        .sample_iter(&Alphanumeric)
        .take(30)
        .map(char::from)
        .collect()
}
