use safareig_core::ruma::{
    api::client::error::{ErrorBody, ErrorKind},
    exports::http::StatusCode,
};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum PresenceError {
    #[error("You are not allowed to see their presence")]
    UserNotAllowed,
}

impl From<PresenceError> for safareig_core::ruma::api::client::Error {
    fn from(error: PresenceError) -> Self {
        match error {
            PresenceError::UserNotAllowed => safareig_core::ruma::api::client::Error {
                status_code: StatusCode::FORBIDDEN,
                body: ErrorBody::Standard {
                    kind: ErrorKind::forbidden(),
                    message: error.to_string(),
                },
            },
        }
    }
}
