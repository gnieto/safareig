use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity, command::Command, ruma::api::client::presence::get_presence, Inject,
};

use crate::{error::PresenceError, storage::PresenceStorage, PresenceAllower};

#[derive(Inject)]
pub struct GetPresenceCommand {
    presence: Arc<dyn PresenceStorage>,
    presence_allower: Arc<dyn PresenceAllower>,
}

#[async_trait]
impl Command for GetPresenceCommand {
    type Input = get_presence::v3::Request;
    type Output = get_presence::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        if !self
            .presence_allower
            .is_user_allowed_to_see(id.user(), &input.user_id)
            .await?
        {
            return Err(PresenceError::UserNotAllowed.into());
        }

        let presence = self.presence.get_presence(&input.user_id).await?;

        let last_active_ago = presence.last_active_ago();
        let currently_active = presence.currently_active();

        Ok(get_presence::v3::Response {
            status_msg: presence.status_msg().map(|message| message.to_string()),
            currently_active: Some(currently_active),
            last_active_ago: Some(last_active_ago),
            presence: presence.state().clone(),
        })
    }
}
