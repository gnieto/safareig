use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity, command::Command, ruma::api::client::presence::set_presence, Inject,
};

use crate::{storage::PresenceStorage, PresenceUpdater};

#[derive(Inject)]
pub struct SetPresenceCommand {
    presence: Arc<dyn PresenceStorage>,
    presence_updater: PresenceUpdater,
}

#[async_trait]
impl Command for SetPresenceCommand {
    type Input = set_presence::v3::Request;
    type Output = set_presence::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let _ = id.check_user(&input.user_id)?;

        let presence = self.presence.get_presence(&input.user_id).await?;
        let presence_update = presence.update(input.presence.clone(), input.status_msg);
        let _ = self.presence_updater.update(presence_update).await?;

        Ok(set_presence::v3::Response::new())
    }
}
