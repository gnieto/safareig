use std::{sync::Arc, time::Duration};

use graceful::{Link, WaitResult};

use crate::{
    storage::{Presence, PresenceStorage},
    PresenceUpdater,
};

pub struct PresenceWorker {
    presence: Arc<dyn PresenceStorage>,
    presence_updater: PresenceUpdater,
    link: graceful::Link,
}

impl PresenceWorker {
    pub fn new(
        link: Link,
        presence: Arc<dyn PresenceStorage>,
        presence_updater: PresenceUpdater,
    ) -> Self {
        PresenceWorker {
            presence,
            presence_updater,
            link,
        }
    }

    pub async fn run(mut self) {
        tracing::info!("Spawned presence worker");

        loop {
            let presences = self
                .presence
                .presences_to_update()
                .await
                .unwrap_or_default();

            self.process_pending_presences(presences).await;

            match self.link.wait_sleep(Duration::from_secs(15)).await {
                WaitResult::Stop => break,
                WaitResult::Result(_) => continue,
            }
        }
    }

    async fn process_pending_presences(&self, presences: Vec<Presence>) {
        for presence in presences {
            let update_result = presence.decay();

            if let Err(e) = self.presence_updater.update(update_result).await {
                tracing::error!("could not update presence after decay: {}", e);
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use std::{
        collections::HashMap,
        ops::Sub,
        sync::{Arc, Mutex},
        time::{Duration, SystemTime},
    };

    use safareig_core::{
        bus::Bus,
        ruma::{presence::PresenceState, user_id, OwnedUserId, UserId},
        storage::StorageError,
    };
    use safareig_ephemeral::services::EphemeralStream;

    use crate::{
        storage::{Presence, PresenceStorage},
        worker::PresenceWorker,
        PresenceUpdater,
    };

    #[tokio::test]
    async fn worker_should_transition_presences_which_have_timedout() {
        let background = graceful::Background::default();
        let storage = Arc::new(InMemoryStorage::default());
        let updater = PresenceUpdater::new(storage.clone(), EphemeralStream::new(Bus::default()));
        let worker = PresenceWorker::new(background.acquire().unwrap(), storage.clone(), updater);

        let user1 = user_id!("@usuari1:localhost:8448");
        let user2 = user_id!("@usuari2:localhost:8448");
        let user3 = user_id!("@usuari3:localhost:8448");
        let user4 = user_id!("@usuari4:localhost:8448");

        let presences = vec![
            Presence::create(
                user1,
                PresenceState::Online,
                None,
                SystemTime::now().sub(Duration::from_secs(600)),
                SystemTime::now().sub(Duration::from_secs(200)),
            ),
            Presence::create(
                user2,
                PresenceState::Offline,
                None,
                SystemTime::now().sub(Duration::from_secs(600)),
                SystemTime::now().sub(Duration::from_secs(400)),
            ),
            Presence::create(
                user3,
                PresenceState::Unavailable,
                None,
                SystemTime::now().sub(Duration::from_secs(600)),
                SystemTime::now().sub(Duration::from_secs(200)),
            ),
            Presence::create(
                user4,
                PresenceState::Online,
                None,
                SystemTime::now().sub(Duration::from_secs(2)),
                SystemTime::now().sub(Duration::from_secs(5)),
            ),
        ];

        for p in &presences {
            storage.set_presence(p).await.unwrap();
        }

        worker.process_pending_presences(presences).await;

        let presence1 = storage.get_presence(user1).await.unwrap();
        let presence2 = storage.get_presence(user2).await.unwrap();
        let presence3 = storage.get_presence(user3).await.unwrap();
        let presence4 = storage.get_presence(user4).await.unwrap();

        assert_eq!(presence1.state(), &PresenceState::Unavailable);
        assert_eq!(presence2.state(), &PresenceState::Offline);
        assert_eq!(presence3.state(), &PresenceState::Unavailable);
        assert_eq!(presence4.state(), &PresenceState::Online);
    }

    #[derive(Default)]
    struct InMemoryStorage {
        presences: Arc<Mutex<HashMap<OwnedUserId, Presence>>>,
    }

    #[async_trait::async_trait]
    impl PresenceStorage for InMemoryStorage {
        async fn set_presence(&self, presence: &Presence) -> Result<(), StorageError> {
            let mut guard = self.presences.lock().unwrap();
            guard.insert(presence.user_id().to_owned(), presence.to_owned());

            Ok(())
        }

        async fn get_presence(&self, user_id: &UserId) -> Result<Presence, StorageError> {
            let guard = self.presences.lock().unwrap();
            let user_id = user_id.to_owned();
            let presence = guard
                .get(&user_id)
                .cloned()
                .unwrap_or_else(|| Presence::new(&user_id));

            Ok(presence)
        }

        async fn presences_to_update(&self) -> Result<Vec<Presence>, StorageError> {
            unimplemented!()
        }
    }
}
