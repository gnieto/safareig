#[cfg(feature = "backend-sqlx")]
pub mod sqlx;

use std::time::{Duration, SystemTime};

use safareig_core::{
    ruma::{
        api::federation::transactions::edu::PresenceUpdate, presence::PresenceState, OwnedUserId,
        UserId,
    },
    storage::StorageError,
};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Presence {
    state: PresenceState,
    user_id: OwnedUserId,
    status_msg: Option<String>,
    latest_active: SystemTime,
    latest_passive: SystemTime,
}

impl Presence {
    pub fn new(user_id: &UserId) -> Self {
        Presence {
            state: PresenceState::Offline,
            user_id: user_id.to_owned(),
            status_msg: None,
            latest_active: SystemTime::now(),
            latest_passive: SystemTime::now(),
        }
    }

    pub fn create(
        user_id: &UserId,
        state: PresenceState,
        status_msg: Option<String>,
        active: SystemTime,
        passive: SystemTime,
    ) -> Self {
        Self {
            state,
            user_id: user_id.to_owned(),
            status_msg,
            latest_active: active,
            latest_passive: passive,
        }
    }

    pub fn offline(user_id: OwnedUserId) -> Self {
        Presence {
            state: PresenceState::Offline,
            user_id,
            status_msg: None,
            latest_active: SystemTime::now(),
            latest_passive: SystemTime::now(),
        }
    }

    pub fn currently_active(&self) -> bool {
        match self.state {
            PresenceState::Online => {
                let last_active_in_seconds =
                    self.latest_active.elapsed().unwrap_or_default().as_secs();

                last_active_in_seconds < 60 * 5
            }
            _ => false,
        }
    }

    pub fn last_active_ago(&self) -> Duration {
        self.latest_passive
            .max(self.latest_active)
            .elapsed()
            .unwrap_or_default()
    }

    pub fn active_action(mut self) -> PresenceUpdateResult {
        let previous = self.state.clone();
        self.latest_active = SystemTime::now();
        self.state = PresenceState::Online;

        PresenceUpdateResult {
            previous,
            current: self,
        }
    }

    pub fn decay(mut self) -> PresenceUpdateResult {
        let previous_state = self.state.clone();

        let last_active = self.last_active_ago();

        if last_active >= Duration::from_secs(300) {
            self.state = PresenceState::Offline
        } else if last_active >= Duration::from_secs(30) {
            self.state = PresenceState::Unavailable
        }

        PresenceUpdateResult {
            previous: previous_state,
            current: self,
        }
    }

    pub fn state(&self) -> &PresenceState {
        &self.state
    }

    pub fn user_id(&self) -> &UserId {
        &self.user_id
    }

    pub fn status_msg(&self) -> Option<&str> {
        self.status_msg.as_deref()
    }

    pub fn update_from_federation(mut self, update: PresenceUpdate) -> PresenceUpdateResult {
        let previous_state = self.state.clone();

        self.status_msg = update.status_msg;
        self.state = update.presence;
        let last_active_ago: u64 = update.last_active_ago.try_into().unwrap_or(0);
        self.latest_active = SystemTime::now() - Duration::from_millis(last_active_ago);

        PresenceUpdateResult {
            previous: previous_state,
            current: self,
        }
    }

    pub fn update(
        mut self,
        new_state: PresenceState,
        status_msg: Option<String>,
    ) -> PresenceUpdateResult {
        let previous = self.state.clone();

        self.state = new_state.clone();
        self.status_msg = status_msg;

        if let PresenceState::Online = new_state {
            let update = self.active_action();

            PresenceUpdateResult {
                previous,
                current: update.presence().clone(),
            }
        } else {
            PresenceUpdateResult {
                previous,
                current: self,
            }
        }
    }
}

pub struct PresenceUpdateResult {
    previous: PresenceState,
    current: Presence,
}

impl PresenceUpdateResult {
    pub fn previous(&self) -> &PresenceState {
        &self.previous
    }

    pub fn presence(&self) -> &Presence {
        &self.current
    }

    pub fn has_changed(&self) -> bool {
        self.previous != *self.current.state()
    }
}

#[async_trait::async_trait]
pub trait PresenceStorage: Send + Sync {
    async fn set_presence(&self, presence: &Presence) -> Result<(), StorageError>;
    async fn get_presence(&self, user_id: &UserId) -> Result<Presence, StorageError>;
    async fn presences_to_update(&self) -> Result<Vec<Presence>, StorageError>;
}
