use std::{collections::HashSet, str::FromStr, sync::Arc};

use safareig_core::{
    pagination::{self, Pagination},
    ruma::{
        api::client::sync::sync_events::{self, v3::Presence},
        events::presence::{PresenceEvent, PresenceEventContent},
        serde::Raw,
        UInt, UserId,
    },
    storage::StorageError,
    Inject,
};
use safareig_ephemeral::services::{EphemeralEvent, EphemeralStream, EphemeralToken};
use safareig_sync_api::{
    context::Context, error::SyncError, SyncExtension, SyncToken, SyncTokenPart,
};

use crate::{storage::PresenceStorage, PresenceAllower};

#[derive(Inject)]
pub struct PresenceSync {
    presence_allower: Arc<dyn PresenceAllower>,
    presence: Arc<dyn PresenceStorage>,
    ephemeral: EphemeralStream,
}

impl PresenceSync {
    /// `sync_presences` builds a set of evets for each relevant presence event that has had a presence transition
    /// The given `presences` array should be loaded from the ephemeral event storage.
    pub async fn sync_presences(
        &self,
        requesting: &UserId,
        presences: impl ExactSizeIterator<Item = &UserId>,
    ) -> Result<Vec<Raw<PresenceEvent>>, StorageError> {
        let mut output = Vec::with_capacity(presences.len());

        // TODO: Probably this should be done in batch, which means that we need a bulk get_presences
        // function in the interface.
        for user_id in presences {
            let allowed = self
                .presence_allower
                .is_user_allowed_to_see(requesting, user_id)
                .await?;
            if !allowed {
                continue;
            }

            let presence = self.presence.get_presence(user_id).await?;

            let content = PresenceEvent {
                content: PresenceEventContent {
                    avatar_url: None,
                    currently_active: Some(presence.currently_active()),
                    displayname: None,
                    last_active_ago: Some(UInt::new_wrapping(
                        presence.last_active_ago().as_millis() as u64,
                    )),
                    presence: presence.state().clone(),
                    status_msg: presence.status_msg().map(|status| status.to_string()),
                },
                sender: user_id.to_owned(),
            };

            output.push(Raw::new(&content)?)
        }

        Ok(output)
    }
}

#[async_trait::async_trait]
impl SyncExtension for PresenceSync {
    #[tracing::instrument(name = "sync-presence", skip_all)]
    async fn fill_sync<'a>(
        &self,
        ctx: &'a Context<'a>,
        response: &mut sync_events::v3::Response,
        token: &mut SyncToken,
    ) -> Result<(), SyncError> {
        let ephemeral_token = ctx
            .from_token()
            .extension_token::<PresenceToken>()
            .map_err(|_| SyncError::InvalidToken)?
            .unwrap_or_default();

        let range = (
            pagination::Boundary::Exclusive(ephemeral_token.0),
            pagination::Boundary::Unlimited,
        );
        let query = Pagination::new(range.into(), ());

        let pagination_response = self.ephemeral.events(query);
        let next = pagination_response.next().cloned();
        let ephemeral = pagination_response.records();

        // Update next token up to the current ephemeal token
        if let Some(next) = next {
            token.update(PresenceToken(next));
        }

        let mut presences: HashSet<&UserId> = HashSet::new();

        ephemeral.iter().for_each(|e| {
            if let EphemeralEvent::Presence(target) = e {
                presences.insert(target);
            }
        });

        // Fill presences
        let presences = self
            .sync_presences(ctx.user(), presences.into_iter())
            .await?;

        response.presence = Presence { events: presences };

        Ok(())
    }
}

#[derive(Default)]
pub struct PresenceToken(EphemeralToken);

impl SyncTokenPart for PresenceToken {
    fn id() -> &'static str {
        "p"
    }
}

impl ToString for PresenceToken {
    fn to_string(&self) -> String {
        self.0.to_string()
    }
}

impl FromStr for PresenceToken {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(PresenceToken(EphemeralToken::try_from(s)?))
    }
}
