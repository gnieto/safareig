use std::{collections::HashMap, sync::Arc};

use async_trait::async_trait;
use safareig_core::{
    auth::FederationAuth,
    ruma::{
        api::federation::transactions::edu::{Edu, PresenceContent, PresenceUpdate},
        OwnedUserId, UInt,
    },
    storage::StorageError,
    Inject,
};
use safareig_ephemeral::services::{EphemeralAggregator, EphemeralEvent, EphemeralHandler};

use crate::{storage::PresenceStorage, PresenceUpdater};

#[derive(Inject)]
pub struct EphemeralPresenceHandler {
    presence: Arc<dyn PresenceStorage>,
    presence_updater: PresenceUpdater,
}

impl EphemeralPresenceHandler {
    async fn handle_presence_update(&self, update: PresenceUpdate) -> Result<(), StorageError> {
        let presence = self.presence.get_presence(&update.user_id).await?;
        let presence_update = presence.update_from_federation(update);
        self.presence_updater.update(presence_update).await?;

        Ok(())
    }
}

#[async_trait]
impl EphemeralHandler for EphemeralPresenceHandler {
    async fn on_federation(&self, edu: Edu, auth: &FederationAuth) {
        let content = match edu {
            Edu::Presence(p) => p,
            _ => return,
        };

        for presence_update in content.push {
            if let Err(e) = self.handle_presence_update(presence_update).await {
                tracing::error!(
                    remote = auth.server.as_str(),
                    err = e.to_string().as_str(),
                    "could not handle a presence update event",
                );
            }
        }
    }

    fn edu_aggregator(&self) -> Box<dyn EphemeralAggregator> {
        Box::new(EphemeralPresenceAggregator::new(self.presence.clone()))
    }
}

pub struct EphemeralPresenceAggregator {
    presences: HashMap<OwnedUserId, PresenceUpdate>,
    presence: Arc<dyn PresenceStorage>,
}

impl EphemeralPresenceAggregator {
    pub fn new(presence: Arc<dyn PresenceStorage>) -> Self {
        EphemeralPresenceAggregator {
            presences: HashMap::new(),
            presence,
        }
    }
}

#[async_trait]
impl EphemeralAggregator for EphemeralPresenceAggregator {
    async fn aggregate_event(&mut self, event: EphemeralEvent) {
        if let EphemeralEvent::Presence(user) = event {
            if self.presences.contains_key(&user) {
                return;
            }

            let presence = self.presence.get_presence(&user).await;

            if let Ok(presence) = presence {
                let last_active_ago =
                    UInt::new_wrapping(presence.last_active_ago().as_millis() as u64);
                let currently_active = presence.currently_active();

                let updates = PresenceUpdate {
                    user_id: presence.user_id().to_owned(),
                    presence: presence.state().clone(),
                    status_msg: presence.status_msg().map(|message| message.to_string()),
                    last_active_ago,
                    currently_active,
                };

                self.presences.insert(user, updates);
            }
        }
    }

    fn edus(self: Box<Self>) -> Vec<Edu> {
        if !self.presences.is_empty() {
            let presence_edu = Edu::Presence(PresenceContent {
                push: self.presences.values().cloned().collect(),
            });

            vec![presence_edu]
        } else {
            vec![]
        }
    }
}
