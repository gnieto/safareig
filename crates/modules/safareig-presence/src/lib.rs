use std::sync::Arc;

// use safareig::storage::{RoomStorage, Membership};
use safareig_core::{ruma::UserId, storage::StorageError, Inject};
use safareig_ephemeral::services::{EphemeralEvent, EphemeralStream};
use storage::{PresenceStorage, PresenceUpdateResult};

pub mod command;
mod ephemeral;
pub mod error;
pub mod module;
pub mod storage;
mod sync;
pub mod worker;

#[async_trait::async_trait]
pub trait PresenceAllower: Send + Sync {
    async fn is_user_allowed_to_see(
        &self,
        requesting_user: &UserId,
        target_user: &UserId,
    ) -> Result<bool, StorageError>;
}

#[derive(Clone, Inject)]
pub struct PresenceUpdater {
    storage: Arc<dyn PresenceStorage>,
    ephemeral_stream: EphemeralStream,
}

impl PresenceUpdater {
    pub async fn update(&self, updated_presence: PresenceUpdateResult) -> Result<(), StorageError> {
        self.storage
            .set_presence(updated_presence.presence())
            .await?;

        if updated_presence.has_changed() {
            let ephemeral_event =
                EphemeralEvent::Presence(updated_presence.presence().user_id().to_owned());
            self.ephemeral_stream.push_event(ephemeral_event);
        }

        Ok(())
    }

    pub async fn user_active(&self, user_id: &UserId) -> Result<(), StorageError> {
        let presence = self.storage.get_presence(user_id).await?;
        let update_result = presence.active_action();

        if update_result.has_changed() {
            self.update(update_result).await?;
        } else {
            self.storage.set_presence(update_result.presence()).await?;
        }

        Ok(())
    }

    pub async fn user_passive(&self, user_id: &UserId) -> Result<(), StorageError> {
        let presence = self.storage.get_presence(user_id).await?;
        let update_result = presence.active_action();

        if update_result.has_changed() {
            self.update(update_result).await?;
        } else {
            self.storage.set_presence(update_result.presence()).await?;
        }

        Ok(())
    }
}
