use safareig_core::{ruma::presence::PresenceState, storage::StorageError};
use safareig_sqlx::decode_from_str;
use sqlx::{sqlite::SqliteRow, types::chrono, Row, Sqlite};

use super::{Presence, PresenceStorage};

pub struct SqlxPresenceStorage {
    pool: sqlx::Pool<Sqlite>,
}

impl SqlxPresenceStorage {
    pub fn new(pool: sqlx::Pool<Sqlite>) -> Self {
        Self { pool }
    }
}

#[async_trait::async_trait]
impl PresenceStorage for SqlxPresenceStorage {
    async fn set_presence(&self, presence: &Presence) -> Result<(), StorageError> {
        let active =
            sqlx::types::chrono::DateTime::<sqlx::types::chrono::Utc>::from(presence.latest_active);
        let passive = sqlx::types::chrono::DateTime::<sqlx::types::chrono::Utc>::from(
            presence.latest_passive,
        );

        sqlx::query(
            r#"
            INSERT INTO presence (user_id, state, message, active, passive)
            VALUES (?1, ?2, ?3, ?4, ?5)
            ON CONFLICT DO UPDATE SET state = ?2, message = ?3, active = ?4, passive = ?5;
            "#,
        )
        .bind(presence.user_id.as_str())
        .bind(presence.state.as_str())
        .bind(presence.status_msg.as_ref())
        .bind(active)
        .bind(passive)
        .execute(&self.pool)
        .await?;

        Ok(())
    }

    async fn get_presence(
        &self,
        user_id: &safareig_core::ruma::UserId,
    ) -> Result<Presence, StorageError> {
        let presence = sqlx::query(
            r#"
            SELECT * FROM presence 
            WHERE user_id = ?1;
            "#,
        )
        .bind(user_id.as_str())
        .fetch_optional(&self.pool)
        .await?
        .map(TryFrom::try_from)
        .transpose()?;

        Ok(presence.unwrap_or_else(|| Presence::new(user_id)))
    }

    async fn presences_to_update(&self) -> Result<Vec<Presence>, StorageError> {
        let now = chrono::Utc::now();

        let presences = sqlx::query(
            r#"
            SELECT * FROM presence 
            WHERE (state = "online" AND active <= TIME(?1, '-30 seconds')) or ((state = "offline" OR state = "unavailable") AND passive <= TIME(?1, '-300 seconds')) 
            "#,
        ).bind(now)
        .fetch_all(&self.pool)
        .await?
        .into_iter()
        .map(TryFrom::try_from)
        .collect::<Result<Vec<Presence>, StorageError>>()?;

        Ok(presences)
    }
}

impl TryFrom<SqliteRow> for Presence {
    type Error = StorageError;

    fn try_from(row: SqliteRow) -> Result<Self, StorageError> {
        let user_id = decode_from_str(row.try_get::<&str, _>("user_id")?)?;
        let state = PresenceState::from(row.try_get::<&str, _>("state")?);
        let status_msg = row.try_get::<Option<String>, _>("message")?;
        let active = row.try_get::<chrono::DateTime<chrono::Utc>, _>("active")?;
        let passive = row.try_get::<chrono::DateTime<chrono::Utc>, _>("passive")?;

        Ok(Presence {
            state,
            user_id,
            status_msg,
            latest_active: active.into(),
            latest_passive: passive.into(),
        })
    }
}
