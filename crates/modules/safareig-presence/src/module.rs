use std::sync::Arc;

use graceful::BackgroundHandler;
use safareig_core::{
    command::{Container, InjectServices, Module, Router, ServiceCollectionExtension},
    database::DatabaseConnection,
    ddi::{ServiceCollectionExt, ServiceProvider, ServiceResolverExt},
};
use safareig_ephemeral::services::EphemeralHandler;
use safareig_sync_api::SyncExtension;

use crate::{
    command::{GetPresenceCommand, SetPresenceCommand},
    ephemeral::EphemeralPresenceHandler,
    storage::PresenceStorage,
    sync::PresenceSync,
    worker::PresenceWorker,
    PresenceUpdater,
};

#[derive(Default)]
pub struct PresenceModule {}

impl PresenceModule {
    fn storage(connection: DatabaseConnection) -> Arc<dyn PresenceStorage> {
        match connection {
            #[cfg(feature = "backend-sqlx")]
            DatabaseConnection::Sqlx(pool) => {
                Arc::new(crate::storage::sqlx::SqlxPresenceStorage::new(pool))
            }
            _ => unimplemented!(""),
        }
    }
}

#[async_trait::async_trait]
impl<R: Router> Module<R> for PresenceModule {
    fn register(&self, container: &mut Container, router: &mut R) {
        container.inject_endpoint::<_, GetPresenceCommand, _>(router);
        container.inject_endpoint::<_, SetPresenceCommand, _>(router);

        container
            .service_collection
            .service_factory_var("m.presence", |sp: &ServiceProvider| {
                let sync: Arc<dyn EphemeralHandler> =
                    Arc::new(EphemeralPresenceHandler::inject(sp)?);
                Ok(sync)
            });

        container
            .service_collection
            .service_factory_var("presence", |sp: &ServiceProvider| {
                let sync: Arc<dyn SyncExtension> = Arc::new(PresenceSync::inject(sp)?);
                Ok(sync)
            });

        container
            .service_collection
            .service_factory(|db: &DatabaseConnection| Ok(Self::storage(db.clone())));
        container.service_collection.register::<PresenceUpdater>();
    }

    async fn spawn_workers(&self, container: &ServiceProvider, background: &BackgroundHandler) {
        let storage = container.get::<Arc<dyn PresenceStorage>>().unwrap().clone();
        let presence_updater = container.get::<PresenceUpdater>().unwrap().clone();

        // Presence
        let presence =
            PresenceWorker::new(background.acquire().unwrap(), storage, presence_updater);

        tokio::spawn(presence.run());
    }
}
