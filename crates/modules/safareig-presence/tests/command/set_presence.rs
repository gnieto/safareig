use std::sync::Arc;

use safareig_core::{
    command::Command,
    ruma::{
        api::client::presence::set_presence, exports::http::StatusCode, presence::PresenceState,
    },
};
use safareig_presence::{
    command::SetPresenceCommand, module::PresenceModule, storage::PresenceStorage,
};
use safareig_testing::TestingApp;

#[tokio::test]
async fn fails_set_presence_to_non_current_user() {
    let app = TestingApp::with_modules(vec![Box::new(PresenceModule::default())]).await;
    let target = app.register().await;
    let requester = app.register().await;

    let cmd = app.command::<SetPresenceCommand>();
    let input = set_presence::v3::Request {
        user_id: target.user().to_owned(),
        presence: PresenceState::Offline,
        status_msg: None,
    };

    let result = cmd.execute(input, requester).await;

    assert!(result.is_err());
    let error = result.err().unwrap();
    assert_eq!(error.status_code, StatusCode::UNAUTHORIZED);
}

#[tokio::test]
async fn set_presence_to_current_user() {
    let app = TestingApp::with_modules(vec![Box::new(PresenceModule::default())]).await;
    let requester = app.register().await;

    let cmd = app.command::<SetPresenceCommand>();
    let input = set_presence::v3::Request {
        user_id: requester.user().to_owned(),
        presence: PresenceState::Offline,
        status_msg: Some("estat".to_string()),
    };

    let result = cmd.execute(input, requester.clone()).await;

    assert!(result.is_ok());
    let storage = app.service::<Arc<dyn PresenceStorage>>();
    let presence = storage.get_presence(requester.user()).await.unwrap();
    assert_eq!(presence.state(), &PresenceState::Offline);
    assert!(!presence.currently_active());
    assert_eq!(presence.status_msg().unwrap(), "estat");
}

#[tokio::test]
async fn set_presence_to_online_should_make_use_active() {
    let app = TestingApp::with_modules(vec![Box::new(PresenceModule::default())]).await;
    let requester = app.register().await;

    let cmd = app.command::<SetPresenceCommand>();
    let input = set_presence::v3::Request {
        user_id: requester.user().to_owned(),
        presence: PresenceState::Online,
        status_msg: Some("estat".to_string()),
    };

    let result = cmd.execute(input, requester.clone()).await;

    assert!(result.is_ok());
    let storage = app.service::<Arc<dyn PresenceStorage>>();
    let presence = storage.get_presence(requester.user()).await.unwrap();
    assert_eq!(presence.state(), &PresenceState::Online);
    assert!(presence.currently_active());
    assert_eq!(presence.status_msg().unwrap(), "estat");
}
