use std::sync::Arc;

use safareig_core::{
    command::Command,
    ruma::{
        api::client::presence::get_presence, exports::http::StatusCode, presence::PresenceState,
        user_id,
    },
};
use safareig_presence::{
    command::GetPresenceCommand,
    module::PresenceModule,
    storage::{Presence, PresenceStorage},
};
use safareig_testing::{error::RumaErrorExtension, TestingApp};

#[tokio::test]
async fn fails_get_presence_of_user_without_common_rooms() {
    let app = TestingApp::with_modules(vec![Box::new(PresenceModule::default())]).await;
    let target = app.register().await;
    let requester = app.register().await;

    let cmd = app.command::<GetPresenceCommand>();
    let input = get_presence::v3::Request {
        user_id: target.user().to_owned(),
    };

    let result = cmd.execute(input, requester).await;

    assert!(result.is_err());
    let error = result.err().unwrap();
    assert_eq!(error.status_code, StatusCode::FORBIDDEN);
    assert_eq!(error.message(), "You are not allowed to see their presence");
}

#[tokio::test]
async fn fails_get_presence_unknwon_user() {
    let app = TestingApp::with_modules(vec![Box::new(PresenceModule::default())]).await;
    let target = user_id!("@user:localhost:8448");
    let requester = app.register().await;

    let cmd = app.command::<GetPresenceCommand>();
    let input = get_presence::v3::Request {
        user_id: target.to_owned(),
    };

    let result = cmd.execute(input, requester).await;
    assert!(result.is_err());
    let error = result.err().unwrap();
    assert_eq!(error.status_code, StatusCode::FORBIDDEN);
    assert_eq!(error.message(), "You are not allowed to see their presence");
}

#[tokio::test]
async fn get_presence_on_previously_stored_user() {
    let app = TestingApp::with_modules(vec![Box::new(PresenceModule::default())]).await;
    let (target, room) = app.public_room().await;
    let requester = app.register().await;
    app.join_room(room.id().to_owned().to_owned(), requester.clone())
        .await
        .unwrap();
    let presence = Presence::new(target.user())
        .update(PresenceState::Unavailable, Some("hi!".to_string()))
        .presence()
        .clone();
    let storage = app.service::<Arc<dyn PresenceStorage>>();
    storage.set_presence(&presence).await.unwrap();

    let cmd = app.command::<GetPresenceCommand>();
    let input = get_presence::v3::Request {
        user_id: target.user().to_owned(),
    };

    let response = cmd.execute(input, requester).await.unwrap();

    assert_eq!("hi!", response.status_msg.unwrap());
    assert!(!response.currently_active.unwrap());
    assert!(response.last_active_ago.unwrap().as_secs() <= 1);
    assert_eq!(response.presence, PresenceState::Unavailable);
}
