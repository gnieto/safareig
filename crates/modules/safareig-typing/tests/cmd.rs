use std::time::Duration;

use futures::{select, FutureExt};
use safareig_core::{
    bus::{Bus, BusMessage},
    command::Command,
    ruma::{
        api::client::typing::{create_typing_event, create_typing_event::v3::Typing},
        server_name, RoomId, UserId,
    },
};
use safareig_testing::TestingApp;
use safareig_typing::command::TypingCommand;
use tokio::time::sleep;

#[tokio::test]
async fn non_activate_user_can_not_set_typing_of_another_user() {
    let db = TestingApp::default().await;
    let new_user = TestingApp::new_identity();
    let cmd = db.command::<TypingCommand>();

    let input = create_typing_event::v3::Request {
        user_id: UserId::parse("@not_same:localhost:8448").unwrap(),
        room_id: RoomId::new(server_name!("localhost:8448")),
        state: Typing::No,
    };
    let result = cmd.execute(input, new_user).await;

    assert!(result.is_err());
    // TODO: Assert string error
    // TODO: Assert 401
}

#[tokio::test]
async fn typing_notification_gets_broadcasted() {
    let db = TestingApp::default().await;
    let (creator, room) = db.public_room().await;
    let cmd = db.command::<TypingCommand>();
    let bus = db.service::<Bus>();
    let mut rx = bus.receiver();

    let input = create_typing_event::v3::Request {
        user_id: creator.user().to_owned(),
        room_id: room.id().to_owned(),
        state: Typing::Yes(Duration::from_secs(30)),
    };
    let _ = cmd.execute(input, creator.clone()).await.unwrap();

    select! {
        m = rx.recv().fuse() => {
            match m {
                Ok(BusMessage::Typing(_, _)) => (),
                _ => panic!("Expected typing message"),
            }
        },
        _ = sleep(Duration::from_secs(2)).fuse() => {
            panic!("Timeout waiting for bus message");
        },
    };
}
