use std::time::Duration;

use graceful::WaitResult;

use super::TypingTracker;

pub struct TypingWorker {
    tracker: TypingTracker,
    link: graceful::Link,
}

impl TypingWorker {
    pub fn new(link: graceful::Link, typing_tracker: TypingTracker) -> Self {
        TypingWorker {
            tracker: typing_tracker,
            link,
        }
    }

    pub async fn run(mut self) {
        tracing::info!("Spawned typing worker");

        loop {
            let removed = self.tracker.remove_expired_entries();
            if removed > 0 {
                tracing::debug!("Removed {} typing notifications", removed);
            }

            match self.link.wait_sleep(Duration::from_secs(5)).await {
                WaitResult::Stop => break,
                WaitResult::Result(_) => continue,
            }
        }
    }
}
