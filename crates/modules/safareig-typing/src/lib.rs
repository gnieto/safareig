pub mod command;
mod ephemeral;
pub mod module;
mod worker;

#[cfg(not(test))]
use std::time::Instant;
use std::{
    collections::HashMap,
    sync::{Arc, RwLock},
    time::Duration,
};

#[cfg(test)]
use mock_instant::Instant;
use safareig_core::ruma::{
    api::client::typing::create_typing_event::v3::Typing, OwnedRoomId, OwnedUserId, RoomId, UserId,
};
use safareig_ephemeral::services::{EphemeralEvent, EphemeralStream};

type UserExpires = HashMap<OwnedUserId, Instant>;

#[derive(Clone)]
pub struct TypingTracker {
    rooms: Arc<RwLock<HashMap<OwnedRoomId, UserExpires>>>,
    ephemeral: EphemeralStream,
    default_timeout: Duration,
}

impl TypingTracker {
    pub fn new(ephemeral: EphemeralStream) -> Self {
        TypingTracker {
            rooms: Default::default(),
            ephemeral,
            default_timeout: Duration::from_secs(8),
        }
    }

    pub fn start_typing(&self, room: &RoomId, user: &UserId, timeout: Option<Duration>) {
        let mut guard = self.rooms.write().unwrap();
        let user_expires = guard.entry(room.to_owned()).or_default();

        let timeout = timeout
            .unwrap_or(self.default_timeout)
            .min(Duration::from_secs(30));

        user_expires.insert(user.to_owned(), Instant::now() + timeout);

        let event = EphemeralEvent::Typing(
            user.to_owned(),
            room.to_owned(),
            Typing::Yes(self.default_timeout),
        );
        self.ephemeral.push_event(event);
    }

    pub fn stop_typing(&self, room: &RoomId, user: &UserId) {
        let mut guard = self.rooms.write().unwrap();

        if let Some(typing_users) = guard.get_mut(room) {
            let removed = typing_users.remove(user);

            if removed.is_some() {
                let event = EphemeralEvent::Typing(user.to_owned(), room.to_owned(), Typing::No);
                self.ephemeral.push_event(event);
            }
        }
    }

    pub fn remove_expired_entries(&self) -> u8 {
        let current_time = Instant::now();
        let mut removed = 0u8;
        let mut guard = self.rooms.write().unwrap();
        let mut entries_to_remove = vec![];

        for (room, expires) in guard.iter_mut() {
            expires.retain(|user, &mut i| {
                let is_expired = i < current_time;

                if is_expired {
                    removed = removed.saturating_add(1);
                    let event =
                        EphemeralEvent::Typing(user.to_owned(), room.to_owned(), Typing::No);
                    self.ephemeral.push_event(event);
                }

                !is_expired
            });

            if expires.is_empty() {
                entries_to_remove.push(room.to_owned());
            }
        }

        for room in entries_to_remove {
            guard.remove(&room);
        }

        removed
    }

    pub fn typing_users(&self, room_id: &RoomId) -> Vec<OwnedUserId> {
        let guard = self.rooms.read().unwrap();

        match guard.get(room_id) {
            Some(users) => users.keys().cloned().collect(),
            None => Vec::new(),
        }
    }
}

#[cfg(test)]
mod tests {
    use std::time::Duration;

    use mock_instant::MockClock;
    use safareig_core::{
        bus::Bus,
        ruma::{room_id, user_id},
    };
    use safareig_ephemeral::services::EphemeralStream;

    use crate::TypingTracker;

    #[tokio::test]
    async fn it_can_track_multiple_users() {
        let bus = Bus::default();
        let stream = EphemeralStream::new(bus);
        let typing_tracker = TypingTracker::new(stream);
        let room1 = room_id!("!room1:localhost");
        let room2 = room_id!("!room2:localhost");
        let user1 = user_id!("@user1:localhost");
        let user2 = user_id!("@user2:localhost");
        let user3 = user_id!("@user3:localhost");

        typing_tracker.start_typing(room1, user1, None);
        typing_tracker.start_typing(room2, user1, None);
        typing_tracker.start_typing(room1, user2, None);
        typing_tracker.start_typing(room1, user1, None);
        typing_tracker.start_typing(room2, user3, None);

        let mut room1_users = typing_tracker.typing_users(room1);
        room1_users.sort();

        let mut room2_users = typing_tracker.typing_users(room2);
        room2_users.sort();

        assert_eq!(vec![user1, user2], room1_users);
        assert_eq!(vec![user1, user3], room2_users);
    }

    #[tokio::test]
    async fn it_can_untrack_users() {
        let bus = Bus::default();
        let stream = EphemeralStream::new(bus);
        let typing_tracker = TypingTracker::new(stream);
        let room1 = room_id!("!room1:localhost");
        let room2 = room_id!("!room2:localhost");
        let user1 = user_id!("@user1:localhost");
        let user2 = user_id!("@user2:localhost");
        let user3 = user_id!("@user3:localhost");

        typing_tracker.stop_typing(room1, user1);
        typing_tracker.start_typing(room1, user1, None);
        typing_tracker.start_typing(room2, user1, None);
        typing_tracker.start_typing(room1, user2, None);
        typing_tracker.start_typing(room1, user1, None);
        typing_tracker.start_typing(room2, user3, None);
        typing_tracker.stop_typing(room1, user2);

        let mut room1_users = typing_tracker.typing_users(room1);
        room1_users.sort();

        let mut room2_users = typing_tracker.typing_users(room2);
        room2_users.sort();

        assert_eq!(vec![user1], room1_users);
        assert_eq!(vec![user1, user3], room2_users);
    }

    #[tokio::test]
    async fn it_removes_users_when_max_time_expires() {
        let bus = Bus::default();
        let stream = EphemeralStream::new(bus);
        let typing_tracker = TypingTracker::new(stream);
        let room1 = room_id!("!room1:localhost");
        let room2 = room_id!("!room2:localhost");
        let user1 = user_id!("@user1:localhost");
        let user2 = user_id!("@user2:localhost");
        let user3 = user_id!("@user3:localhost");

        typing_tracker.start_typing(room1, user1, Some(Duration::from_secs(20)));
        typing_tracker.start_typing(room2, user1, Some(Duration::from_secs(10)));
        typing_tracker.start_typing(room1, user2, Some(Duration::from_secs(30)));
        typing_tracker.start_typing(room1, user1, Some(Duration::from_secs(5)));
        typing_tracker.start_typing(room2, user3, None);

        typing_tracker.remove_expired_entries();
        let room1_users = typing_tracker.typing_users(room1);
        let room2_users = typing_tracker.typing_users(room2);
        assert_eq!(2, room1_users.len());
        assert_eq!(2, room2_users.len());

        MockClock::advance(Duration::from_secs(6));
        let expired = typing_tracker.remove_expired_entries();
        assert_eq!(1, expired);
        let room1_users = typing_tracker.typing_users(room1);
        let room2_users = typing_tracker.typing_users(room2);
        assert_eq!(1, room1_users.len());
        assert_eq!(2, room2_users.len());

        MockClock::advance(Duration::from_secs(5));
        let expired = typing_tracker.remove_expired_entries();
        assert_eq!(2, expired);
        let room1_users = typing_tracker.typing_users(room1);
        let room2_users = typing_tracker.typing_users(room2);
        assert_eq!(1, room1_users.len());
        assert_eq!(0, room2_users.len());

        MockClock::advance(Duration::from_secs(30));
        let expired = typing_tracker.remove_expired_entries();
        assert_eq!(1, expired);
        let room1_users = typing_tracker.typing_users(room1);
        let room2_users = typing_tracker.typing_users(room2);
        assert_eq!(0, room1_users.len());
        assert_eq!(0, room2_users.len());
    }
}
