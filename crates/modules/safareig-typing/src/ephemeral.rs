use std::{ops::Deref, sync::Arc, time::Duration};

use async_trait::async_trait;
use safareig_core::{
    auth::FederationAuth,
    config::HomeserverConfig,
    ruma::api::{
        client::typing::create_typing_event::v3::Typing,
        federation::transactions::edu::{Edu, TypingContent},
    },
    Inject,
};
use safareig_ephemeral::services::{EphemeralAggregator, EphemeralEvent, EphemeralHandler};

use crate::TypingTracker;

#[derive(Inject)]
pub struct EphemeralTypingHandler {
    config: Arc<HomeserverConfig>,
    tracker: TypingTracker,
}

#[async_trait]
impl EphemeralHandler for EphemeralTypingHandler {
    async fn on_federation(&self, edu: Edu, _auth: &FederationAuth) {
        let typing_content = match edu {
            Edu::Typing(typing) => typing,
            _ => return,
        };

        if typing_content.typing {
            self.tracker.start_typing(
                &typing_content.room_id,
                &typing_content.user_id,
                Some(Duration::from_secs(15)),
            );
        } else {
            self.tracker
                .stop_typing(&typing_content.room_id, &typing_content.user_id);
        };
    }

    fn edu_aggregator(&self) -> Box<dyn EphemeralAggregator> {
        Box::new(EphemeralTypingAggregator::new(self.config.clone()))
    }
}

pub struct EphemeralTypingAggregator {
    config: Arc<HomeserverConfig>,
    edus: Vec<Edu>,
}

impl EphemeralTypingAggregator {
    pub fn new(config: Arc<HomeserverConfig>) -> Self {
        Self {
            edus: Vec::new(),
            config,
        }
    }
}

#[async_trait]
impl EphemeralAggregator for EphemeralTypingAggregator {
    async fn aggregate_event(&mut self, event: EphemeralEvent) {
        if let EphemeralEvent::Typing(user_id, room_id, typing) = event {
            if user_id.server_name() == self.config.server_name.deref() {
                let content = TypingContent {
                    room_id,
                    user_id,
                    typing: match typing {
                        Typing::Yes(_) => true,
                        Typing::No => false,
                    },
                };

                self.edus.push(Edu::Typing(content));
            }
        }
    }

    fn edus(self: Box<Self>) -> Vec<Edu> {
        self.edus.clone()
    }
}
