use std::sync::Arc;

use graceful::BackgroundHandler;
use safareig_core::{
    command::{Container, InjectServices, Module, Router},
    ddi::{ServiceCollectionExt, ServiceProvider, ServiceResolverExt},
};
use safareig_ephemeral::services::{EphemeralHandler, EphemeralStream};

use crate::{
    command::TypingCommand, ephemeral::EphemeralTypingHandler, worker::TypingWorker, TypingTracker,
};

#[derive(Default)]
pub struct TypingModule {}

#[async_trait::async_trait]
impl<R: Router> Module<R> for TypingModule {
    fn register(&self, container: &mut Container, router: &mut R) {
        container.inject_endpoint::<_, TypingCommand, _>(router);

        container
            .service_collection
            .service_factory_var("m.typing", |sp: &ServiceProvider| {
                let sync: Arc<dyn EphemeralHandler> = Arc::new(EphemeralTypingHandler::inject(sp)?);
                Ok(sync)
            });

        container
            .service_collection
            .service_factory(|stream: &EphemeralStream| Ok(TypingTracker::new(stream.clone())));
    }

    async fn spawn_workers(&self, container: &ServiceProvider, background: &BackgroundHandler) {
        let typing_tracker = container.get::<TypingTracker>().unwrap();

        if let Some(link) = background.acquire() {
            let worker = TypingWorker::new(link, typing_tracker.clone());
            tokio::spawn(worker.run());
        }
    }
}
