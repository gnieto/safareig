use async_trait::async_trait;
use safareig_core::{
    auth::Identity, command::Command, ruma::api::client::typing::create_typing_event, Inject,
};

use super::TypingTracker;

#[derive(Inject)]
pub struct TypingCommand {
    typing: TypingTracker,
}

#[async_trait]
impl Command for TypingCommand {
    type Input = create_typing_event::v3::Request;
    type Output = create_typing_event::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let _ = id.check_user(&input.user_id)?;
        // TODO: Check user membership to target room
        self.typing
            .start_typing(&input.room_id, &input.user_id, None);

        Ok(create_typing_event::v3::Response::new())
    }
}
