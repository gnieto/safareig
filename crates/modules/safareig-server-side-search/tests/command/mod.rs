use safareig_core::{
    command::Command,
    ruma::{
        api::client::search::search_events::{
            self,
            v3::{Categories, Criteria, SearchResult},
        },
        events::{AnyMessageLikeEvent, AnyTimelineEvent},
        uint,
    },
};
use safareig_server_side_search::{
    command::ServerSideSearchCommand, module::ServerSideSearchModule,
};
use safareig_testing::TestingApp;

#[tokio::test]
pub async fn it_searches_accross_several_rooms() {
    let app = TestingApp::with_modules(vec![Box::<ServerSideSearchModule>::default()]).await;
    let (alice, games_room) = app.public_room().await;
    let (bob, news_room) = app.public_room().await;
    app.join_room(news_room.id().to_owned(), alice.clone())
        .await
        .unwrap();
    let (charlie, support_room) = app.public_room().await;
    app.join_room(support_room.id().to_owned(), alice.clone())
        .await
        .unwrap();
    app.join_room(support_room.id().to_owned(), bob.clone())
        .await
        .unwrap();

    app.talk(games_room.id(), &alice, "term only for alice")
        .await
        .unwrap();
    app.talk(news_room.id(), &alice, "message without the t-e-r-m")
        .await
        .unwrap();
    app.talk(news_room.id(), &alice, "term - first")
        .await
        .unwrap();
    app.talk(news_room.id(), &alice, "second - term")
        .await
        .unwrap();
    app.talk(news_room.id(), &alice, "third termsuffixed")
        .await
        .unwrap();
    app.talk(news_room.id(), &bob, "another term message")
        .await
        .unwrap();
    app.talk(support_room.id(), &alice, "A: another term message")
        .await
        .unwrap();
    app.talk(support_room.id(), &bob, "B: another term message")
        .await
        .unwrap();
    app.talk(support_room.id(), &charlie, "C: another term message")
        .await
        .unwrap();

    let cmd = app.command::<ServerSideSearchCommand>();

    let request = search_events::v3::Request {
        next_batch: None,
        search_categories: Categories {
            room_events: Some(Criteria::new("term".to_string())),
        },
    };
    let search_result = cmd.execute(request, alice.to_owned()).await.unwrap();
    assert_eq!(8, search_result.search_categories.room_events.results.len());
}

#[tokio::test]
pub async fn it_searches_accross_several_rooms_with_pagination() {
    let app = TestingApp::with_modules(vec![Box::<ServerSideSearchModule>::default()]).await;
    let (alice, games_room) = app.public_room().await;
    let (bob, news_room) = app.public_room().await;
    app.join_room(news_room.id().to_owned(), alice.clone())
        .await
        .unwrap();
    let (charlie, support_room) = app.public_room().await;
    app.join_room(support_room.id().to_owned(), alice.clone())
        .await
        .unwrap();
    app.join_room(support_room.id().to_owned(), bob.clone())
        .await
        .unwrap();

    app.talk(games_room.id(), &alice, "term only for alice")
        .await
        .unwrap();
    app.talk(news_room.id(), &alice, "message without the t-e-r-m")
        .await
        .unwrap();
    app.talk(news_room.id(), &alice, "term - first")
        .await
        .unwrap();
    app.talk(news_room.id(), &alice, "second - term")
        .await
        .unwrap();
    app.talk(news_room.id(), &alice, "third termsuffixed")
        .await
        .unwrap();
    app.talk(news_room.id(), &bob, "another term message")
        .await
        .unwrap();
    app.talk(support_room.id(), &alice, "A: another term message")
        .await
        .unwrap();
    app.talk(support_room.id(), &bob, "B: another term message")
        .await
        .unwrap();
    app.talk(support_room.id(), &charlie, "C: another term message")
        .await
        .unwrap();

    let cmd = app.command::<ServerSideSearchCommand>();

    let mut criteria = Criteria::new("term".to_string());
    criteria.filter.limit = Some(uint!(2));

    let request = search_events::v3::Request {
        next_batch: None,
        search_categories: Categories {
            room_events: Some(criteria.clone()),
        },
    };
    let search_result = cmd.execute(request, alice.to_owned()).await.unwrap();
    assert!(search_result
        .search_categories
        .room_events
        .next_batch
        .is_some());
    assert_eq!(
        "C: another term message",
        get_body(&search_result.search_categories.room_events.results[0])
    );
    assert_eq!(
        "B: another term message",
        get_body(&search_result.search_categories.room_events.results[1])
    );

    let request = search_events::v3::Request {
        next_batch: search_result.search_categories.room_events.next_batch,
        search_categories: Categories {
            room_events: Some(criteria),
        },
    };
    let search_result = cmd.execute(request, alice.to_owned()).await.unwrap();
    assert_eq!(
        "A: another term message",
        get_body(&search_result.search_categories.room_events.results[0])
    );
    assert_eq!(
        "another term message",
        get_body(&search_result.search_categories.room_events.results[1])
    );
}

#[tokio::test]
pub async fn it_does_not_return_results_from_rooms_where_user_is_not_joined() {
    let app = TestingApp::with_modules(vec![Box::<ServerSideSearchModule>::default()]).await;
    let (alice, games_room) = app.public_room().await;
    let (bob, news_room) = app.public_room().await;
    app.join_room(news_room.id().to_owned(), alice.clone())
        .await
        .unwrap();
    let (charlie, support_room) = app.public_room().await;
    app.join_room(support_room.id().to_owned(), alice.clone())
        .await
        .unwrap();
    app.join_room(support_room.id().to_owned(), bob.clone())
        .await
        .unwrap();

    app.talk(games_room.id(), &alice, "term only for alice")
        .await
        .unwrap();
    app.talk(news_room.id(), &alice, "message without the t-e-r-m")
        .await
        .unwrap();
    app.talk(news_room.id(), &alice, "term - first")
        .await
        .unwrap();
    app.talk(news_room.id(), &alice, "second - term")
        .await
        .unwrap();
    app.talk(news_room.id(), &alice, "third termsuffixed")
        .await
        .unwrap();
    app.talk(news_room.id(), &bob, "another term message")
        .await
        .unwrap();
    app.talk(support_room.id(), &alice, "A: another term message")
        .await
        .unwrap();
    app.talk(support_room.id(), &bob, "B: another term message")
        .await
        .unwrap();
    app.talk(support_room.id(), &charlie, "C: another term message")
        .await
        .unwrap();

    let cmd = app.command::<ServerSideSearchCommand>();

    let criteria = Criteria::new("term".to_string());

    let request = search_events::v3::Request {
        next_batch: None,
        search_categories: Categories {
            room_events: Some(criteria.clone()),
        },
    };
    let search_result = cmd.execute(request, bob.to_owned()).await.unwrap();
    assert_eq!(7, search_result.search_categories.room_events.results.len());

    let request = search_events::v3::Request {
        next_batch: None,
        search_categories: Categories {
            room_events: Some(criteria.clone()),
        },
    };
    let search_result = cmd.execute(request, charlie.to_owned()).await.unwrap();
    assert_eq!(3, search_result.search_categories.room_events.results.len());
}

fn get_body(search: &SearchResult) -> String {
    let event = search.result.as_ref().unwrap().deserialize().unwrap();

    match event {
        AnyTimelineEvent::MessageLike(AnyMessageLikeEvent::RoomMessage(message)) => {
            message.as_original().unwrap().content.body().to_string()
        }
        _ => unimplemented!(),
    }
}
