use std::{collections::HashMap, sync::Arc};

use safareig::{
    client::room::{loader::RoomLoader, RoomError},
    core::{events::Event, filter::RoomFilter},
    storage::EventStorage,
};
use safareig_core::{
    auth::Identity,
    command::Command,
    error::error_chain,
    pagination::{Boundary, Direction, Limit, Pagination},
    ruma::{
        api::client::search::search_events::{
            self,
            v3::{EventContextResult, ResultCategories, SearchResult},
        },
        OwnedEventId, UInt,
    },
    Inject,
};

use crate::term::TermSearcher;

#[derive(Inject)]
pub struct ServerSideSearchCommand {
    room_loader: RoomLoader,
    term_searcher: Arc<dyn TermSearcher>,
    events: Arc<dyn EventStorage>,
}

#[async_trait::async_trait]
impl Command for ServerSideSearchCommand {
    type Input = search_events::v3::Request;
    type Output = search_events::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let mut results = ResultCategories::new();
        let next_batch = input.next_batch;

        if let Some(criteria) = input.search_categories.room_events {
            let term = criteria.search_term.clone();
            let range = (
                Boundary::Unlimited,
                next_batch
                    .map(Boundary::Exclusive)
                    .unwrap_or(Boundary::Unlimited),
            );
            let limit = Limit::limit_or_default(criteria.filter.limit, 50);
            let query = Pagination::new(range.into(), criteria.to_owned())
                .with_direction(Direction::Backward)
                .with_limit(limit);
            let pagination_result = self.term_searcher.search(query).await?;

            let next_batch = pagination_result.next().cloned();
            let event_ids = pagination_result.records();
            let mut events: HashMap<OwnedEventId, Event> = self
                .events
                .get_events(&event_ids)
                .await?
                .into_iter()
                .map(|e| (e.event_ref.event_id().to_owned(), e))
                .collect();

            let mut search_results = Vec::new();
            let room_filter = RoomFilter::new(&criteria.filter);

            for event_id in event_ids {
                let event = events.remove(&event_id).unwrap();
                // TODO: Properly apply visibility rules: This just checks that the user is joined in the forward extremities.
                let _ = match self
                    .room_loader
                    .get_for_user(&event.room_id, id.user())
                    .await
                {
                    Err(error) => {
                        match error {
                            RoomError::UserNotAllowed(_) => {
                                continue;
                            }
                            _ => {
                                tracing::error!(
                                    error = error_chain(&error),
                                    "error while loading target room"
                                );
                                continue;
                            }
                        };
                    }
                    Ok(room) => room,
                };

                if !room_filter.event_allowed(&event) {
                    continue;
                }

                let search_result = SearchResult {
                    context: EventContextResult::new(),
                    rank: None,
                    result: Some(event.try_into().unwrap()),
                };

                search_results.push(search_result);
            }

            let result_amount = search_results.len() as u32;
            results.room_events.count = Some(UInt::new(result_amount.into()).unwrap());
            results.room_events.next_batch = next_batch;
            results.room_events.results = search_results;
            results.room_events.highlights = vec![term];
        }

        Ok(search_events::v3::Response::new(results))
    }
}
