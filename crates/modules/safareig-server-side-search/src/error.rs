use safareig_core::storage::StorageError;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum ServerSideSearchError {
    #[error("Storage error")]
    Storage(#[from] StorageError),
}

impl From<ServerSideSearchError> for safareig_core::ruma::api::client::Error {
    fn from(e: ServerSideSearchError) -> Self {
        match e {
            ServerSideSearchError::Storage(e) => e.into(),
        }
    }
}
