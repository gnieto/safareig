use std::sync::Arc;

use futures::FutureExt;
use graceful::WaitResult;
use safareig::{
    client::room::RoomStream,
    core::events::Event,
    storage::{Checkpoint, CheckpointStorage},
};
use safareig_core::{
    pagination::{self, PaginationResponse},
    storage::StorageError,
    StreamToken,
};
use tokio::time::{sleep, Duration};

use crate::term::tantivy::TantivyService;

// TODO: Probably this should not be public
pub struct TantivyIndexerWorker {
    room_stream: RoomStream,
    checkpoints: Arc<dyn CheckpointStorage<Checkpoint>>,
    checkpoint: Checkpoint,
    tantivy_service: Arc<TantivyService>,
    worker_id: String,
    link: graceful::Link,
}

impl TantivyIndexerWorker {
    pub async fn new(
        room_stream: RoomStream,
        checkpoints: Arc<dyn CheckpointStorage<Checkpoint>>,
        tantivy: Arc<TantivyService>,
        link: graceful::Link,
    ) -> Self {
        let worker_id = "tantivy_indexer".to_string();

        let checkpoint = checkpoints
            .get_checkpoint(&worker_id)
            .await
            .ok()
            .flatten()
            .unwrap_or(Checkpoint {
                latest: StreamToken::horizon(),
                in_flight: None,
                txn_id: None,
            });

        Self {
            room_stream,
            checkpoints,
            checkpoint,
            tantivy_service: tantivy,
            worker_id,
            link,
        }
    }

    pub async fn run(mut self) {
        tracing::debug!("Running worker {}", self.worker_id);

        loop {
            let event_result = self.retrieve_events().await;

            let wait_for_events = match &event_result {
                Ok(r) => {
                    let token = r.next().cloned();
                    let head_above_latest =
                        token.map(|t| t > self.checkpoint.latest).unwrap_or(false);
                    let has_new_content = head_above_latest || !r.is_empty();

                    !has_new_content
                }
                Err(e) => {
                    tracing::error!(
                        latest = self.checkpoint.latest.to_string().as_str(),
                        err = e.to_string().as_str(),
                        "Errored while fetching new events to send to app service",
                    );

                    true
                }
            };

            if wait_for_events {
                match self.link.wait_sleep(Duration::from_secs(15)).await {
                    WaitResult::Stop => break,
                    WaitResult::Result(_) => continue,
                }
            }

            let stream_response = event_result.unwrap();
            let max_token = stream_response.next().cloned();
            let events = stream_response.records();

            if let Err(e) = self.index_events(max_token, events).await {
                tracing::error!(
                    err = e.to_string().as_str(),
                    "could not send events to app service. Will wait some time to retry.",
                );

                let delay = sleep(Duration::from_secs(15)).boxed().fuse();
                match self.link.wait_select(delay).await {
                    WaitResult::Stop => break,
                    WaitResult::Result(_) => continue,
                }
            }
        }

        tracing::debug!("Closed worker: {}", self.worker_id);
    }

    async fn retrieve_events(
        &self,
    ) -> Result<PaginationResponse<StreamToken, (StreamToken, Event)>, StorageError> {
        let range = (
            pagination::Boundary::Exclusive(self.checkpoint.latest),
            pagination::Boundary::Unlimited,
        );

        let stream_response = self.room_stream.stream_events(range.into(), None).await?;

        Ok(stream_response)
    }

    async fn checkpoint_to(&mut self, latest: StreamToken) -> Result<(), StorageError> {
        let mut checkpoint = self.checkpoint.clone();
        checkpoint.in_flight = None;
        checkpoint.txn_id = None;
        checkpoint.latest = latest;

        self.checkpoints
            .update_checkpoint(&self.worker_id, &checkpoint)
            .await?;
        self.checkpoint = checkpoint;

        Ok(())
    }

    async fn index_events(
        &mut self,
        to: Option<StreamToken>,
        events: Vec<(StreamToken, Event)>,
    ) -> Result<(), StorageError> {
        for (token, event) in events {
            self.tantivy_service.index_event(&event).await?;
            tracing::info!("Indexing event {:?}", token);
        }

        tracing::info!("Flush tantivy");
        self.tantivy_service.flush().await?;

        if let Some(to) = to {
            self.checkpoint_to(to).await?
        }

        Ok(())
    }
}
