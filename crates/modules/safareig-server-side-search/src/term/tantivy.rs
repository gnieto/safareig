use std::sync::Arc;

use safareig::core::events::Event;
use safareig_core::{
    config::{HomeserverConfig, TantivyOptions},
    pagination::{Pagination, PaginationResponse},
    ruma::{
        api::client::search::search_events::v3::Criteria,
        events::{
            room::{
                message::RoomMessageEventContent, name::RoomNameEventContent,
                topic::RoomTopicEventContent,
            },
            TimelineEventType,
        },
        OwnedEventId,
    },
    storage::StorageError,
};
use safareig_sqlx::PaginatedIterator;
use tantivy::{
    collector::TopDocs,
    directory::MmapDirectory,
    doc,
    query::QueryParser,
    schema::{
        IndexRecordOption, Schema, TextFieldIndexing, TextOptions, FAST, INDEXED, STORED, STRING,
    },
    DateTime, DocAddress, Index, IndexReader, IndexWriter, ReloadPolicy, Searcher,
};
use tokio::sync::RwLock;

use super::TermSearcher;
use crate::error::ServerSideSearchError;

pub struct TantivyService {
    schema: Schema,
    indexer: Arc<RwLock<IndexWriter>>,
    reader: Arc<IndexReader>,
}

impl TantivyService {
    pub fn new(options: TantivyOptions, config: &HomeserverConfig) -> Self {
        let mut schema_builder = Schema::builder();
        let text_options = TextOptions::default().set_indexing_options(
            TextFieldIndexing::default()
                .set_tokenizer("en_stem")
                .set_index_option(IndexRecordOption::Basic),
        );
        schema_builder.add_text_field("event_id", STRING | STORED);
        schema_builder.add_text_field("event_type", STRING);
        schema_builder.add_text_field("content", text_options);
        schema_builder.add_date_field("origin_ts", INDEXED | FAST | STORED);
        let schema = schema_builder.build();

        let directory = MmapDirectory::open(options.path(config)).unwrap();
        let index = Index::open_or_create(directory, schema.clone()).unwrap();
        let index_writer = index.writer(10_000_000).unwrap();
        let reader = index
            .reader_builder()
            .reload_policy(ReloadPolicy::OnCommit)
            .try_into()
            .unwrap();

        Self {
            schema,
            indexer: Arc::new(RwLock::new(index_writer)),
            reader: Arc::new(reader),
        }
    }

    pub async fn index_event(&self, event: &Event) -> Result<(), StorageError> {
        let indexer = self.indexer.write().await;

        let content_to_index = match event.kind {
            TimelineEventType::RoomMessage => {
                let content = event
                    .content_as::<RoomMessageEventContent>()
                    .map_err(|e| StorageError::Custom(e.to_string()))?;
                content.body().to_string()
            }
            TimelineEventType::RoomName => {
                let content = event
                    .content_as::<RoomNameEventContent>()
                    .map_err(|e| StorageError::Custom(e.to_string()))?;

                content.name
            }
            TimelineEventType::RoomTopic => {
                let content = event
                    .content_as::<RoomTopicEventContent>()
                    .map_err(|e| StorageError::Custom(e.to_string()))?;

                content.topic
            }
            _ => {
                return Ok(());
            }
        };

        let event_id = self.schema.get_field("event_id").unwrap();
        let event_type = self.schema.get_field("event_type").unwrap();
        let content = self.schema.get_field("content").unwrap();
        let origin_ts = self.schema.get_field("origin_ts").unwrap();

        let origin_ts_value = DateTime::from_timestamp_millis(event.origin_server_ts as i64);
        tracing::info!("Adding document");
        indexer
            .add_document(doc!(
                event_id => event.event_ref.event_id().as_str(),
                event_type => event.kind.to_string().as_str(),
                content => content_to_index.as_str(),
                origin_ts => origin_ts_value,
            ))
            .map_err(|e| StorageError::Custom(e.to_string()))?;

        Ok(())
    }

    pub async fn flush(&self) -> Result<(), StorageError> {
        let indexer = self.indexer.clone();

        tokio::task::spawn_blocking(move || {
            let mut write = indexer.blocking_write();
            if let Err(error) = write.commit() {
                tracing::error!(?error, "Error while commiting the indexer");
            }
        })
        .await
        .map_err(|e| StorageError::Custom(e.to_string()))?;

        Ok(())
    }
}

#[async_trait::async_trait]
impl TermSearcher for TantivyService {
    async fn search(
        &self,
        query: Pagination<String, Criteria>,
    ) -> Result<PaginationResponse<String, OwnedEventId>, ServerSideSearchError> {
        let searcher = self.reader.searcher();
        let schema = self.schema.clone();

        let join_result = tokio::task::spawn_blocking(move || {
            let origin_ts = schema.get_field("origin_ts").unwrap();
            let content = schema.get_field("content").unwrap();
            let mut tantivy_query = format!("({})", query.context().search_term.clone());

            if let Some(token) = query.range().to().clone().token() {
                let as_millis = token.parse::<u64>().unwrap();
                let date_time = DateTime::from_timestamp_millis(as_millis as i64);
                tantivy_query.push_str(&format!(
                    " AND origin_ts:[2000-01-01T00:00:00.00Z TO {date_time:?}}}"
                ));
            }

            if let Some(keys) = &query.context().keys {
                let keys_statements: Vec<_> =
                    keys.iter().map(|key| format!("event_type:{key}")).collect();

                tantivy_query.push_str(&format!(" AND ({})", keys_statements.join(" OR ")));
            }

            let top_docs = TopDocs::with_limit(query.limit().unwrap_or(100) as usize)
                .order_by_u64_field(origin_ts);

            let query_parser = QueryParser::for_index(searcher.index(), vec![content]);
            let tantivy_query = query_parser.parse_query(&tantivy_query).unwrap();
            let r = searcher.search(&tantivy_query, &top_docs).unwrap();

            Ok(
                PaginatedIterator::new(r.into_iter().map(|t| t.1), move |a| {
                    doc_mapper(a, &searcher, &schema)
                })
                .into_pagination_response()?,
            )
        })
        .await;

        join_result.map_err(|e| StorageError::Custom(e.to_string()))?
    }
}

fn doc_mapper(
    doc: DocAddress,
    searcher: &Searcher,
    schema: &Schema,
) -> Result<(String, OwnedEventId), StorageError> {
    let retrieved_doc = searcher.doc(doc).unwrap();
    let origin_ts = schema.get_field("origin_ts").unwrap();
    let event_id = schema.get_field("event_id").unwrap();

    let token = retrieved_doc
        .get_first(origin_ts)
        .and_then(|v| v.as_date())
        .ok_or_else(|| StorageError::Custom("expected date".to_string()))?
        .into_timestamp_millis()
        .to_string();

    let event_id = retrieved_doc
        .get_first(event_id)
        .and_then(|v| v.as_text())
        .ok_or_else(|| StorageError::Custom("expected string".to_string()))?;

    let event_id = OwnedEventId::try_from(event_id).unwrap();

    Ok((token, event_id))
}
