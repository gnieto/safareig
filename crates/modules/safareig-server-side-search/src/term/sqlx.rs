use safareig_core::{
    pagination::{Pagination, PaginationResponse},
    ruma::{
        api::client::search::search_events::v3::{Criteria, SearchKeys},
        OwnedEventId,
    },
    storage::StorageError,
};
use safareig_sqlx::{decode_from_str, DirectionExt, PaginatedIterator, RangeWrapper};
use sea_query::{Alias, Cond, Expr, Iden, Query, SqliteQueryBuilder};
use sea_query_binder::SqlxBinder;
use sqlx::{Pool, Row, Sqlite};

use super::TermSearcher;
use crate::error::ServerSideSearchError;

pub struct SqlxTermSearcher {
    pool: Pool<Sqlite>,
}

impl SqlxTermSearcher {
    pub fn new(pool: Pool<Sqlite>) -> Self {
        Self { pool }
    }
}

#[async_trait::async_trait]
impl TermSearcher for SqlxTermSearcher {
    async fn search(
        &self,
        query: Pagination<String, Criteria>,
    ) -> Result<PaginationResponse<String, OwnedEventId>, ServerSideSearchError> {
        let criteria = query.context();
        let default_keys = vec![
            SearchKeys::ContentBody,
            SearchKeys::ContentName,
            SearchKeys::ContentTopic,
        ];
        let keys = criteria.keys.as_ref().unwrap_or(&default_keys);

        if keys.is_empty() {
            return Ok(PaginationResponse::empty());
        }

        let wrapper = RangeWrapper(query.range().map(|t| t.to_string()));
        let mut builder = Query::select();
        builder
            .column(Events::EventId)
            .expr_as(
                Expr::cust("cast(JSON_EXTRACT(event_data, '$.origin_server_ts') as text)"),
                Alias::new("origin_ts"),
            )
            .from(Events::Table);

        let mut cond_or = Cond::any();

        for key in keys {
            match key {
                SearchKeys::ContentBody => {
                    cond_or = cond_or.add(Expr::cust_with_values(
                        r#"(event_type = "m.room.message" AND lower(JSON_EXTRACT(event_data, '$.content.body')) LIKE '%' || ? || '%')"#,
                        [criteria.search_term.clone()]
                    ));
                }
                SearchKeys::ContentName => {
                    cond_or = cond_or.add(Expr::cust_with_values(
                        r#"(event_type = "m.room.name" AND lower(JSON_EXTRACT(event_data, '$.content.name')) LIKE '%' || ? || '%') "#,
                        [criteria.search_term.clone()]
                    ));
                }
                SearchKeys::ContentTopic => {
                    cond_or = cond_or.add(Expr::cust_with_values(
                        r#" (event_type = "m.room.topic" AND lower(JSON_EXTRACT(event_data, '$.content.topic')) LIKE '%' || ? || '%')"#,
                        [criteria.search_term.clone()]
                    ));
                }
                _ => {
                    tracing::warn!(?key, "unknown search key");
                }
            }
        }

        builder
            .cond_where(cond_or)
            .cond_where(wrapper.condition(Expr::col(Alias::new("origin_ts"))))
            .order_by(Events::OriginTs, query.direction().order())
            .limit(query.limit().unwrap_or(10) as u64);

        let (query, values) = builder.build_sqlx(SqliteQueryBuilder);

        let rows = sqlx::query_with(&query, values)
            .fetch_all(&self.pool)
            .await
            .map_err(StorageError::from)?;

        let iterator = PaginatedIterator::new(rows.into_iter(), |row| {
            let token = row.try_get::<&str, _>("origin_ts")?;
            let event_id = decode_from_str::<OwnedEventId>(row.try_get::<&str, _>("event_id")?)?;

            Ok((token.to_string(), event_id))
        });

        Ok(iterator.into_pagination_response()?)
    }
}

#[derive(Iden)]
enum Events {
    Table,
    EventId,
    OriginTs,
}
