use std::sync::Arc;

use graceful::BackgroundHandler;
use safareig_core::{
    command::{Container, Module},
    config::ServerSideSearchConfig,
    database::DatabaseConnection,
    ddi::{ServiceCollectionExt, ServiceProvider},
};

use crate::{command::ServerSideSearchCommand, term::TermSearcher};

#[derive(Default)]
pub struct ServerSideSearchModule;

impl ServerSideSearchModule {
    fn term_searcher(connection: DatabaseConnection) -> Arc<dyn TermSearcher> {
        match connection {
            #[cfg(feature = "backend-sqlx")]
            DatabaseConnection::Sqlx(pool) => {
                Arc::new(crate::term::sqlx::SqlxTermSearcher::new(pool))
            }
            _ => unimplemented!(""),
        }
    }
}

#[async_trait::async_trait]
impl<R: safareig_core::command::Router> Module<R> for ServerSideSearchModule {
    fn register(&self, container: &mut Container, router: &mut R) {
        container.inject_endpoint::<_, ServerSideSearchCommand, _>(router);

        match &container.config.server_side_search {
            ServerSideSearchConfig::Sqlite => container
                .service_collection
                .service_factory(|db: &DatabaseConnection| Ok(Self::term_searcher(db.clone()))),
            ServerSideSearchConfig::Tantivy(tantivy_options) => {
                #[cfg(not(feature = "terms-tantivy"))]
                {
                    panic!(
                        "Please compile with tantivy support before activating this configuration"
                    )
                }
                #[cfg(feature = "terms-tantivy")]
                {
                    let tantivy = Arc::new(crate::term::tantivy::TantivyService::new(
                        tantivy_options.clone(),
                        &container.config,
                    ));
                    container.service_collection.service(tantivy);
                    container.service_collection.service_factory(
                        |tantivy: &Arc<crate::term::tantivy::TantivyService>| {
                            let searcher: Arc<dyn TermSearcher> = tantivy.clone();
                            Ok(searcher)
                        },
                    )
                }
            }
            _ => {
                unimplemented!("")
            }
        };
    }

    async fn spawn_workers(
        &self,
        service_provider: &ServiceProvider,
        background: &BackgroundHandler,
    ) {
        #[cfg(feature = "terms-tantivy")]
        {
            use safareig::{
                client::room::RoomStream,
                storage::{Checkpoint, CheckpointStorage},
            };
            use safareig_core::{config::HomeserverConfig, ddi::ServiceResolverExt};

            let config = service_provider.get::<Arc<HomeserverConfig>>().unwrap();
            if let ServerSideSearchConfig::Tantivy(_) = config.server_side_search {
                let tantivy = service_provider
                    .get::<Arc<crate::term::tantivy::TantivyService>>()
                    .unwrap();
                let room_stream = service_provider.get::<RoomStream>().unwrap();
                let checkpoints = service_provider
                    .get::<Arc<dyn CheckpointStorage<Checkpoint>>>()
                    .unwrap();
                let link = background.acquire().unwrap();

                let worker = crate::worker::tantivy::TantivyIndexerWorker::new(
                    room_stream.clone(),
                    checkpoints.clone(),
                    tantivy.clone(),
                    link,
                )
                .await;

                tokio::task::spawn(worker.run());
            }
        }
    }
}
