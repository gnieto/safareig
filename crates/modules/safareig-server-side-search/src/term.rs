use safareig_core::{
    pagination::{Pagination, PaginationResponse},
    ruma::{api::client::search::search_events::v3::Criteria, OwnedEventId},
};

use crate::error::ServerSideSearchError;

#[cfg(feature = "backend-sqlx")]
pub mod sqlx;
#[cfg(feature = "terms-tantivy")]
pub mod tantivy;

#[async_trait::async_trait]
pub trait TermSearcher: Send + Sync {
    async fn search(
        &self,
        query: Pagination<String, Criteria>,
    ) -> Result<PaginationResponse<String, OwnedEventId>, ServerSideSearchError>;
}
