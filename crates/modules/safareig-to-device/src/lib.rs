pub mod command;
mod ephemeral;
mod error;
mod loader;
pub mod module;
mod sender;
pub mod storage;
mod sync;

use std::str::FromStr;

pub use error::ToDeviceError;
use safareig_core::{
    events::Content,
    ruma::events::{
        dummy::ToDeviceDummyEventContent,
        forwarded_room_key::ToDeviceForwardedRoomKeyEventContent,
        key::verification::{
            accept::ToDeviceKeyVerificationAcceptEventContent,
            cancel::ToDeviceKeyVerificationCancelEventContent,
            done::ToDeviceKeyVerificationDoneEventContent,
            key::ToDeviceKeyVerificationKeyEventContent,
            mac::ToDeviceKeyVerificationMacEventContent,
            ready::ToDeviceKeyVerificationReadyEventContent,
            request::ToDeviceKeyVerificationRequestEventContent,
            start::ToDeviceKeyVerificationStartEventContent,
        },
        room::encrypted::ToDeviceRoomEncryptedEventContent,
        room_key::ToDeviceRoomKeyEventContent,
        room_key_request::ToDeviceRoomKeyRequestEventContent,
        secret::{
            request::ToDeviceSecretRequestEventContent, send::ToDeviceSecretSendEventContent,
        },
        EventContentFromType, ToDeviceEventType,
    },
};
use serde_json::value::RawValue;

// Try convert the given payload depending on the event type.
// Since Ruma does not offer a deserialization layer anymore, we've created this new enum which will be used
// to ensure that the give content matches with the given types.
// This will cause some maintainability issues because we will need to keep a mapping of event types  to event contents.
pub enum SafareigToDeviceEventContent {
    Dummy(Box<ToDeviceDummyEventContent>),
    RoomKey(Box<ToDeviceRoomKeyEventContent>),
    RoomKeyRequest(Box<ToDeviceRoomKeyRequestEventContent>),
    ForwardedRoomKey(Box<ToDeviceForwardedRoomKeyEventContent>),
    KeyVerificationRequest(Box<ToDeviceKeyVerificationRequestEventContent>),
    KeyVerificationReady(Box<ToDeviceKeyVerificationReadyEventContent>),
    KeyVerificationStart(Box<ToDeviceKeyVerificationStartEventContent>),
    KeyVerificationCancel(Box<ToDeviceKeyVerificationCancelEventContent>),
    KeyVerificationAccept(Box<ToDeviceKeyVerificationAcceptEventContent>),
    KeyVerificationKey(Box<ToDeviceKeyVerificationKeyEventContent>),
    KeyVerificationMac(Box<ToDeviceKeyVerificationMacEventContent>),
    KeyVerificationDone(Box<ToDeviceKeyVerificationDoneEventContent>),
    RoomEncrypted(Box<ToDeviceRoomEncryptedEventContent>),
    SecretRequest(Box<ToDeviceSecretRequestEventContent>),
    SecretSend(Box<ToDeviceSecretSendEventContent>),
    Custom(Content),
}

impl SafareigToDeviceEventContent {
    pub fn from_parts(
        kind: &ToDeviceEventType,
        json: &RawValue,
    ) -> Result<Self, serde_json::Error> {
        let event_content = match kind {
            ToDeviceEventType::Dummy => SafareigToDeviceEventContent::Dummy(Box::new(
                ToDeviceDummyEventContent::from_parts(&kind.to_string(), json)?,
            )),
            ToDeviceEventType::RoomKey => SafareigToDeviceEventContent::RoomKey(Box::new(
                ToDeviceRoomKeyEventContent::from_parts(&kind.to_string(), json)?,
            )),
            ToDeviceEventType::RoomKeyRequest => {
                SafareigToDeviceEventContent::RoomKeyRequest(Box::new(
                    ToDeviceRoomKeyRequestEventContent::from_parts(&kind.to_string(), json)?,
                ))
            }
            ToDeviceEventType::ForwardedRoomKey => {
                SafareigToDeviceEventContent::ForwardedRoomKey(Box::new(
                    ToDeviceForwardedRoomKeyEventContent::from_parts(&kind.to_string(), json)?,
                ))
            }
            ToDeviceEventType::KeyVerificationRequest => {
                SafareigToDeviceEventContent::KeyVerificationRequest(Box::new(
                    ToDeviceKeyVerificationRequestEventContent::from_parts(
                        &kind.to_string(),
                        json,
                    )?,
                ))
            }
            ToDeviceEventType::KeyVerificationReady => {
                SafareigToDeviceEventContent::KeyVerificationReady(Box::new(
                    ToDeviceKeyVerificationReadyEventContent::from_parts(&kind.to_string(), json)?,
                ))
            }
            ToDeviceEventType::KeyVerificationStart => {
                SafareigToDeviceEventContent::KeyVerificationStart(Box::new(
                    ToDeviceKeyVerificationStartEventContent::from_parts(&kind.to_string(), json)?,
                ))
            }
            ToDeviceEventType::KeyVerificationCancel => {
                SafareigToDeviceEventContent::KeyVerificationCancel(Box::new(
                    ToDeviceKeyVerificationCancelEventContent::from_parts(&kind.to_string(), json)?,
                ))
            }
            ToDeviceEventType::KeyVerificationAccept => {
                SafareigToDeviceEventContent::KeyVerificationAccept(Box::new(
                    ToDeviceKeyVerificationAcceptEventContent::from_parts(&kind.to_string(), json)?,
                ))
            }
            ToDeviceEventType::KeyVerificationKey => {
                SafareigToDeviceEventContent::KeyVerificationKey(Box::new(
                    ToDeviceKeyVerificationKeyEventContent::from_parts(&kind.to_string(), json)?,
                ))
            }
            ToDeviceEventType::KeyVerificationMac => {
                SafareigToDeviceEventContent::KeyVerificationMac(Box::new(
                    ToDeviceKeyVerificationMacEventContent::from_parts(&kind.to_string(), json)?,
                ))
            }
            ToDeviceEventType::KeyVerificationDone => {
                SafareigToDeviceEventContent::KeyVerificationDone(Box::new(
                    ToDeviceKeyVerificationDoneEventContent::from_parts(&kind.to_string(), json)?,
                ))
            }
            ToDeviceEventType::RoomEncrypted => {
                SafareigToDeviceEventContent::RoomEncrypted(Box::new(
                    ToDeviceRoomEncryptedEventContent::from_parts(&kind.to_string(), json)?,
                ))
            }
            ToDeviceEventType::SecretRequest => {
                SafareigToDeviceEventContent::SecretRequest(Box::new(
                    ToDeviceSecretRequestEventContent::from_parts(&kind.to_string(), json)?,
                ))
            }
            ToDeviceEventType::SecretSend => SafareigToDeviceEventContent::SecretSend(Box::new(
                ToDeviceSecretSendEventContent::from_parts(&kind.to_string(), json)?,
            )),
            _ => {
                let value = serde_json::Value::from_str(json.get())?;
                let content = Content::new(value, kind.to_string());

                tracing::debug!(?kind, "not validating content for event type");
                SafareigToDeviceEventContent::Custom(content)
            }
        };

        Ok(event_content)
    }
}
