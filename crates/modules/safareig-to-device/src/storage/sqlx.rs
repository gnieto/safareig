use safareig_core::{
    pagination::{Pagination, PaginationResponse, Range},
    ruma::DeviceId,
    storage::StorageError,
    StreamToken,
};
use safareig_sqlx::{decode_from_str, decode_i64_to_u64, PaginatedIterator, RangeWrapper};
use sea_query::{Alias, Expr, Iden, Query, SqliteQueryBuilder};
use sea_query_binder::SqlxBinder;
use sqlx::{sqlite::SqliteRow, Row, Sqlite};

use super::{ToDeviceEvent, ToDeviceStorage};

pub struct ToDeviceSqlxStorage {
    pool: sqlx::Pool<Sqlite>,
}

impl ToDeviceSqlxStorage {
    pub fn new(pool: sqlx::Pool<Sqlite>) -> Self {
        Self { pool }
    }
}

#[async_trait::async_trait]
impl ToDeviceStorage for ToDeviceSqlxStorage {
    async fn remove_to_device_events(
        &self,
        device_id: &DeviceId,
        range: Range<StreamToken>,
    ) -> Result<(), StorageError> {
        let wrapper = RangeWrapper(range.map(|t| t.to_string()));
        let (query, values) = Query::delete()
            .from_table(ToDeviceEvents::Table)
            .and_where(Expr::col(ToDeviceEvents::DeviceId).eq(device_id.as_str()))
            .cond_where(wrapper.condition(Expr::col(ToDeviceEvents::StreamToken)))
            .build_sqlx(SqliteQueryBuilder);

        sqlx::query_with(&query, values).execute(&self.pool).await?;

        Ok(())
    }

    async fn query_to_device_events(
        &self,
        device_id: &DeviceId,
        query: &Pagination<StreamToken>,
    ) -> Result<PaginationResponse<StreamToken, ToDeviceEvent>, StorageError> {
        let wrapper = RangeWrapper(query.range().map(|t| t.to_string()));
        let (query, values) = Query::select()
            .column(ToDeviceEvents::DeviceId)
            .column(ToDeviceEvents::EventType)
            .column(ToDeviceEvents::Content)
            .column(ToDeviceEvents::Sender)
            .column(ToDeviceEvents::StreamToken)
            .expr_as("CAST(message_id as TEXT)", Alias::new("message_id"))
            .from(ToDeviceEvents::Table)
            .and_where(Expr::col(ToDeviceEvents::DeviceId).eq(device_id.as_str()))
            .cond_where(wrapper.condition(Expr::col(ToDeviceEvents::StreamToken)))
            .limit(query.limit().unwrap_or(20) as u64)
            .build_sqlx(SqliteQueryBuilder);

        let events = sqlx::query_with(&query, values)
            .fetch_all(&self.pool)
            .await?;

        let iterator = PaginatedIterator::new(events.into_iter(), |row| {
            let token = decode_i64_to_u64(row.try_get::<i64, _>("stream_token")?)?;
            let to_device_event = ToDeviceEvent::try_from(row)?;

            Ok((StreamToken::from(token), to_device_event))
        });

        iterator.into_pagination_response()
    }

    async fn insert_to_device_event(
        &self,
        device_id: &DeviceId,
        event: &ToDeviceEvent,
    ) -> Result<StreamToken, StorageError> {
        let sql = "
            INSERT INTO to_device_events (device_id, event_type, content, sender, message_id, stream_token)
            VALUES (?1, ?2, ?3, ?4, ?5, (
                SELECT COALESCE(MAX(stream_token), 0) + 1 FROM to_device_events WHERE device_id = ?1
            ))
            RETURNING stream_token
        ";

        let token = sqlx::query(sql)
            .bind(device_id.as_str())
            .bind(event.kind.to_string())
            .bind(&event.data)
            .bind(event.sender.as_str())
            .bind(event.msg_id.as_str())
            .fetch_optional(&self.pool)
            .await?
            .map::<Result<_, StorageError>, _>(|row| {
                let token = decode_i64_to_u64(row.try_get::<i64, _>("stream_token")?)?;

                Ok(StreamToken::from(token))
            })
            .transpose()?
            .ok_or_else(|| StorageError::Custom("could not insert to device event".to_string()))?;

        Ok(token)
    }
}

impl TryFrom<SqliteRow> for ToDeviceEvent {
    type Error = StorageError;

    fn try_from(row: SqliteRow) -> Result<Self, StorageError> {
        let kind = row.try_get::<&str, _>("event_type")?.into();
        let data = row.try_get::<serde_json::Value, _>("content")?;
        let sender = decode_from_str(row.try_get::<&str, _>("sender")?)?;
        let msg_id = row.try_get::<String, _>("message_id")?;

        Ok(ToDeviceEvent {
            kind,
            data,
            sender,
            msg_id,
        })
    }
}

#[derive(Iden)]
enum ToDeviceEvents {
    Table,
    DeviceId,
    StreamToken,
    EventType,
    Content,
    Sender,
}
