#[cfg(feature = "backend-sqlx")]
pub mod sqlx;

use safareig_core::{
    pagination::{Pagination, PaginationResponse, Range},
    ruma::{
        events::{AnyToDeviceEvent, ToDeviceEventType},
        serde::Raw,
        DeviceId, OwnedUserId,
    },
    storage::StorageError,
    StreamToken,
};
use serde::{Deserialize, Serialize};

use crate::error::ToDeviceError;

#[async_trait::async_trait]
pub trait ToDeviceStorage: Send + Sync {
    async fn remove_to_device_events(
        &self,
        device_id: &DeviceId,
        range: Range<StreamToken>,
    ) -> Result<(), StorageError>;
    async fn query_to_device_events(
        &self,
        device_id: &DeviceId,
        query: &Pagination<StreamToken>,
    ) -> Result<PaginationResponse<StreamToken, ToDeviceEvent>, StorageError>;
    async fn insert_to_device_event(
        &self,
        device_id: &DeviceId,
        event: &ToDeviceEvent,
    ) -> Result<StreamToken, StorageError>;
}

#[derive(Clone, Serialize, Deserialize, PartialEq, Eq, Debug)]
pub struct ToDeviceEvent {
    pub kind: ToDeviceEventType,
    pub data: serde_json::Value,
    pub sender: OwnedUserId,
    pub msg_id: String,
}

impl ToDeviceEvent {
    pub fn to_any_raw_to_device(self) -> std::result::Result<Raw<AnyToDeviceEvent>, ToDeviceError> {
        let json = serde_json::json!({
            "type": self.kind,
            "content": self.data,
            "sender": self.sender,
        });

        Ok(serde_json::from_value(json)?)
    }
}
