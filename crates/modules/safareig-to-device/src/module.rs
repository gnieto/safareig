use std::sync::Arc;

use safareig_core::{
    command::{Container, InjectServices, Module, ServiceCollectionExtension},
    database::DatabaseConnection,
    ddi::{ServiceCollectionExt, ServiceProvider},
};
use safareig_ephemeral::services::EphemeralHandler;
use safareig_sync_api::SyncExtension;

use crate::{
    command::SendToDeviceCommand, ephemeral::EphemeralToDeviceHandler, loader::ToDeviceLoader,
    sender::ToDeviceSender, storage::ToDeviceStorage, sync::ToDeviceSync,
};

#[derive(Default)]
pub struct ToDeviceModule;

impl ToDeviceModule {
    fn create_to_device(db: DatabaseConnection) -> Arc<dyn ToDeviceStorage> {
        match db {
            #[cfg(feature = "backend-sqlx")]
            DatabaseConnection::Sqlx(pool) => {
                Arc::new(crate::storage::sqlx::ToDeviceSqlxStorage::new(pool))
            }
            _ => unimplemented!(),
        }
    }
}

#[async_trait::async_trait]
impl<R: safareig_core::command::Router> Module<R> for ToDeviceModule {
    fn register(&self, container: &mut Container, router: &mut R) {
        container
            .service_collection
            .service_factory(|db: &DatabaseConnection| Ok(Self::create_to_device(db.clone())));

        container.service_collection.register::<ToDeviceLoader>();
        container.service_collection.register::<ToDeviceSender>();

        container
            .service_collection
            .service_factory_var("to-device", |sp: &ServiceProvider| {
                let sync_extension: Arc<dyn SyncExtension> = Arc::new(ToDeviceSync::inject(sp)?);

                Ok(sync_extension)
            });

        container.service_collection.service_factory_var(
            "m.direct_to_device",
            |sp: &ServiceProvider| {
                let sync_extension: Arc<dyn EphemeralHandler> =
                    Arc::new(EphemeralToDeviceHandler::inject(sp)?);

                Ok(sync_extension)
            },
        );

        container.inject_endpoint::<_, SendToDeviceCommand, _>(router);
    }
}
