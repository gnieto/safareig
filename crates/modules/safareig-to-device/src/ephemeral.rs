use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::FederationAuth, config::HomeserverConfig, ruma::api::federation::transactions::edu::Edu,
    Inject,
};
use safareig_ephemeral::services::{EphemeralAggregator, EphemeralEvent, EphemeralHandler};

use crate::sender::ToDeviceSender;

#[derive(Inject)]
pub struct EphemeralToDeviceHandler {
    config: Arc<HomeserverConfig>,
    sender: ToDeviceSender,
}

#[async_trait]
impl EphemeralHandler for EphemeralToDeviceHandler {
    async fn on_federation(&self, edu: Edu, auth: &FederationAuth) {
        let to_device_message = match edu {
            Edu::DirectToDevice(to_device) => to_device,
            _ => return,
        };

        if to_device_message.sender.server_name() != auth.server {
            tracing::warn!(
                server = auth.server.as_str(),
                "received to_device EDU from a sender distinct to the originating server",
            );

            return;
        }

        let send_result = self
            .sender
            .send_to_device(
                to_device_message.ev_type,
                to_device_message.message_id,
                &to_device_message.sender,
                to_device_message.messages,
            )
            .await;

        if let Err(e) = send_result {
            tracing::error!(
                server = auth.server.as_str(),
                err = e.to_string().as_str(),
                "errored while sending to_device messages over federation"
            );
        }
    }

    fn edu_aggregator(&self) -> Box<dyn EphemeralAggregator> {
        Box::new(EphemeralToDeviceAggregator::new(self.config.clone()))
    }
}

pub struct EphemeralToDeviceAggregator {
    edus: Vec<Edu>,
    config: Arc<HomeserverConfig>,
}

impl EphemeralToDeviceAggregator {
    pub fn new(config: Arc<HomeserverConfig>) -> Self {
        EphemeralToDeviceAggregator {
            edus: Vec::new(),
            config,
        }
    }
}

#[async_trait]
impl EphemeralAggregator for EphemeralToDeviceAggregator {
    async fn aggregate_event(&mut self, event: EphemeralEvent) {
        if let EphemeralEvent::ToDevice(target_server, content) = event {
            if self.config.server_name != target_server {
                self.edus.push(Edu::DirectToDevice(content));
            }
        }
    }

    fn edus(self: Box<Self>) -> Vec<Edu> {
        self.edus.clone()
    }
}
