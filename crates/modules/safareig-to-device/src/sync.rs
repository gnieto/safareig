use std::str::FromStr;

use safareig_core::{
    pagination,
    ruma::api::client::sync::sync_events::{self},
    Inject, StreamToken,
};
use safareig_sync_api::{
    context::Context, error::SyncError, SyncExtension, SyncToken, SyncTokenPart,
};

use crate::loader::ToDeviceLoader;

#[derive(Inject)]
pub struct ToDeviceSync {
    to_device_loader: ToDeviceLoader,
}

#[async_trait::async_trait]
impl SyncExtension for ToDeviceSync {
    #[tracing::instrument(name = "sync-to-device", skip_all)]
    async fn fill_sync<'a>(
        &self,
        ctx: &'a Context<'a>,
        response: &mut sync_events::v3::Response,
        token: &mut SyncToken,
    ) -> Result<(), SyncError> {
        if let Some(device) = ctx.device() {
            let from_token = ctx
                .from_token()
                .extension_token::<ToDeviceToken>()
                .unwrap()
                .unwrap_or_default();

            let stream_response = self
                .to_device_loader
                .load_events_for_sync(device, pagination::Boundary::Exclusive(from_token.0))
                .await
                .unwrap();

            // Update sync token
            if let Some(new_token) = stream_response.next() {
                token.update(ToDeviceToken(*new_token))
            }
            let events = stream_response.records();

            // Set to device events
            response.to_device.events = events;
        }

        Ok(())
    }
}

#[derive(Default)]
pub struct ToDeviceToken(StreamToken);

impl SyncTokenPart for ToDeviceToken {
    fn id() -> &'static str {
        "td"
    }
}

impl ToString for ToDeviceToken {
    fn to_string(&self) -> String {
        self.0.to_string()
    }
}

impl FromStr for ToDeviceToken {
    type Err = std::num::ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(ToDeviceToken(StreamToken::from_str(s)?))
    }
}
