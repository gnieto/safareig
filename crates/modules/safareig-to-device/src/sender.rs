use std::{collections::BTreeMap, ops::Deref, sync::Arc};

use safareig_core::{
    bus::{Bus, BusMessage},
    config::HomeserverConfig,
    ruma::{
        api::federation::transactions::edu::DirectDeviceContent,
        events::{AnyToDeviceEventContent, ToDeviceEventType},
        serde::Raw,
        to_device::DeviceIdOrAllDevices,
        DeviceId, OwnedServerName, OwnedTransactionId, OwnedUserId, UserId,
    },
    Inject,
};
use safareig_devices::storage::DeviceStorage;
use safareig_ephemeral::services::{EphemeralEvent, EphemeralStream};

use super::error::ToDeviceError;
use crate::{
    storage::{ToDeviceEvent, ToDeviceStorage},
    SafareigToDeviceEventContent,
};

type ToDeviceMessages = BTreeMap<DeviceIdOrAllDevices, Raw<AnyToDeviceEventContent>>;
type UserMessages = BTreeMap<OwnedUserId, ToDeviceMessages>;
type RemoteMessages = BTreeMap<OwnedServerName, UserMessages>;

#[derive(Clone, Inject)]
pub struct ToDeviceSender {
    bus: Bus,
    devices: Arc<dyn DeviceStorage>,
    config: Arc<HomeserverConfig>,
    to_devices: Arc<dyn ToDeviceStorage>,
    ephemeral: EphemeralStream,
}

impl ToDeviceSender {
    pub async fn send_to_device(
        &self,
        event_type: ToDeviceEventType,
        message_id: OwnedTransactionId,
        sender: &UserId,
        messages: UserMessages,
    ) -> Result<(), ToDeviceError> {
        let mut remote_to_device: RemoteMessages = RemoteMessages::default();

        for (user_id, messages) in messages {
            // TODO: Move to a service and split the strategy depending if it's a local user or a remote one
            if user_id.server_name() != self.config.server_name.deref() {
                let server_entry = remote_to_device
                    .entry(user_id.server_name().to_owned())
                    .or_default();
                server_entry.insert(user_id, messages);
            } else {
                for (device, message) in messages {
                    let event = Self::convert_message_to_to_device_event(
                        &event_type,
                        message_id.clone(),
                        sender,
                        &message,
                    )?;

                    match device {
                        DeviceIdOrAllDevices::AllDevices => {
                            self.send_all_devices(&user_id, &event).await?;
                        }
                        DeviceIdOrAllDevices::DeviceId(device_id) => {
                            self.insert_event_to_device_stream(&user_id, &device_id, &event)
                                .await?;
                        }
                    }
                }
            }
        }

        for (server, users) in remote_to_device {
            let device_content = DirectDeviceContent {
                sender: sender.to_owned(),
                ev_type: event_type.clone(),
                message_id: message_id.clone(),
                messages: users,
            };

            #[cfg(feature = "sytest")]
            {
                // We only allow to send to device messages to severs who shares a common room with the current server
                // Sytest tests does not assume this scenario, so we need to fake that traget sever is already tracked.
                use safareig_core::ruma::RoomId;
                self.bus.send_message(BusMessage::ServerTracked(
                    server.clone(),
                    RoomId::new(&server),
                ));
                tokio::time::sleep(std::time::Duration::from_millis(500)).await;
            }

            self.ephemeral
                .push_event(EphemeralEvent::ToDevice(server, device_content));
        }

        Ok(())
    }

    fn convert_message_to_to_device_event(
        event_type: &ToDeviceEventType,
        message_id: OwnedTransactionId,
        sender: &UserId,
        content: &Raw<AnyToDeviceEventContent>,
    ) -> Result<ToDeviceEvent, ToDeviceError> {
        // Try to parse the incoming content with the given event type. If it fails, there's a mismatch with
        // event content and event type and we won't accept the new event.
        let _ = SafareigToDeviceEventContent::from_parts(event_type, content.json())
            .map_err(ToDeviceError::Serde)?;

        let json_value: serde_json::Value = serde_json::from_str(content.json().get())?;

        let event = ToDeviceEvent {
            kind: event_type.clone(),
            msg_id: message_id.to_string(),
            sender: sender.to_owned(),
            data: json_value,
        };

        Ok(event)
    }

    async fn insert_event_to_device_stream(
        &self,
        user_id: &UserId,
        device: &DeviceId,
        event: &ToDeviceEvent,
    ) -> Result<(), ToDeviceError> {
        self.to_devices
            .insert_to_device_event(device, event)
            .await?;

        self.bus
            .send_message(BusMessage::ToDevice(user_id.to_owned(), device.to_owned()));

        Ok(())
    }

    async fn send_all_devices(
        &self,
        user_id: &UserId,
        event: &ToDeviceEvent,
    ) -> Result<(), ToDeviceError> {
        let devices = self.devices.devices_by_user(user_id).await?;

        for device in devices {
            let device_id = device.ruma_device.device_id;
            self.insert_event_to_device_stream(user_id, &device_id, event)
                .await?;
        }

        Ok(())
    }
}
