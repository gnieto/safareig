use std::sync::Arc;

use safareig_core::{
    pagination::{self, Direction, Limit, Pagination, PaginationResponse},
    ruma::{events::AnyToDeviceEvent, serde::Raw, DeviceId},
    Inject, StreamToken,
};

use super::error::ToDeviceError;
use crate::storage::ToDeviceStorage;

#[derive(Clone, Inject)]
pub struct ToDeviceLoader {
    to_device: Arc<dyn ToDeviceStorage>,
}

impl ToDeviceLoader {
    pub async fn load_events_for_sync(
        &self,
        device_id: &DeviceId,
        from: pagination::Boundary<StreamToken>,
    ) -> Result<PaginationResponse<StreamToken, Raw<AnyToDeviceEvent>>, ToDeviceError> {
        let exclusive_from = from.exclusive().unwrap();
        self.to_device
            .remove_to_device_events(
                device_id,
                (pagination::Boundary::Unlimited, exclusive_from).into(),
            )
            .await?;

        let query = Pagination::new((from, pagination::Boundary::Unlimited).into(), ())
            .with_limit(Limit::from(100u32))
            .with_direction(Direction::Forward);

        let stream_result = self
            .to_device
            .query_to_device_events(device_id, &query)
            .await?;
        let previous = stream_result.previous().cloned();
        let next = stream_result.next().cloned();

        let events = stream_result
            .records()
            .into_iter()
            .map(|to_device| to_device.to_any_raw_to_device())
            .collect::<Result<Vec<Raw<AnyToDeviceEvent>>, _>>()?;

        let stream_response = PaginationResponse::new(events, previous, next);

        Ok(stream_response)
    }
}
