use safareig_core::{
    ruma::{
        api::client::error::{ErrorBody, ErrorKind},
        exports::http::StatusCode,
    },
    storage::StorageError,
};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum ToDeviceError {
    #[error("The given content do not match with the given event type")]
    InvalidContentForEventType,
    #[error("Invalid event content")]
    Serde(#[from] ::serde_json::Error),
    #[error("Found storge error")]
    Storage(#[from] StorageError),
}

impl From<ToDeviceError> for ::safareig_core::ruma::api::client::Error {
    fn from(error: ToDeviceError) -> Self {
        match error {
            ToDeviceError::InvalidContentForEventType | ToDeviceError::Serde(_) => {
                ::safareig_core::ruma::api::client::Error {
                    body: ErrorBody::Standard {
                        kind: ErrorKind::BadJson,
                        message: error.to_string(),
                    },
                    status_code: StatusCode::BAD_REQUEST,
                }
            }
            ToDeviceError::Storage(_) => ::safareig_core::ruma::api::client::Error {
                status_code: StatusCode::INTERNAL_SERVER_ERROR,
                body: ErrorBody::Standard {
                    kind: ErrorKind::Unknown,
                    message: error.to_string(),
                },
            },
        }
    }
}
