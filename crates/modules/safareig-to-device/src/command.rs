use safareig_core::{
    auth::Identity, command::Command, ruma::api::client::to_device::send_event_to_device, Inject,
};

use super::sender::ToDeviceSender;

#[derive(Inject)]
pub struct SendToDeviceCommand {
    to_device_sender: ToDeviceSender,
}

#[async_trait::async_trait]
impl Command for SendToDeviceCommand {
    type Input = send_event_to_device::v3::Request;
    type Output = send_event_to_device::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        self.to_device_sender
            .send_to_device(
                input.event_type,
                input.txn_id.clone(),
                id.user(),
                input.messages.clone(),
            )
            .await?;

        Ok(send_event_to_device::v3::Response::new())
    }
}
