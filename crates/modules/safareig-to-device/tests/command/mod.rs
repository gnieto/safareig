use std::{collections::BTreeMap, sync::Arc};

use safareig_core::{
    bus::{Bus, BusMessage},
    command::Command,
    pagination::{Limit, Pagination, Range},
    ruma::{
        api::client::to_device::send_event_to_device,
        events::{dummy::ToDeviceDummyEventContent, AnyToDeviceEventContent, ToDeviceEventType},
        serde::Raw,
        to_device::DeviceIdOrAllDevices,
        TransactionId,
    },
};
use safareig_testing::TestingApp;
use safareig_to_device::{
    command::SendToDeviceCommand, module::ToDeviceModule, storage::ToDeviceStorage,
};

#[tokio::test]
async fn it_store_messages_for_distinct_users_and_devices() {
    let module = Box::new(ToDeviceModule);
    let db = TestingApp::with_modules(vec![module]).await;
    let user1 = db.register().await;
    let device2 = db.login(user1.user(), "pwd").await.unwrap().device_id;
    let device3 = db.login(user1.user(), "pwd").await.unwrap().device_id;
    let user2 = db.register().await;
    let device4 = db.login(user2.user(), "pwd").await.unwrap().device_id;
    let device5 = db.login(user2.user(), "pwd").await.unwrap().device_id;
    let user3 = TestingApp::new_identity();

    let dummy_content = ToDeviceDummyEventContent::new();
    let event = Raw::new(&AnyToDeviceEventContent::Dummy(dummy_content)).unwrap();

    let cmd = db.command::<SendToDeviceCommand>();
    let mut messages = BTreeMap::new();
    let mut msg_user1 = BTreeMap::new();
    msg_user1.insert(DeviceIdOrAllDevices::AllDevices, event.clone());
    let mut msg_user2 = BTreeMap::new();
    msg_user2.insert(
        DeviceIdOrAllDevices::DeviceId(device4.clone()),
        event.clone(),
    );
    messages.insert(user1.user().to_owned(), msg_user1);
    messages.insert(user2.user().to_owned(), msg_user2);
    let bus = db.service::<Bus>();
    let mut rx = bus.receiver();

    let input = send_event_to_device::v3::Request {
        event_type: ToDeviceEventType::Dummy,
        txn_id: TransactionId::new(),
        messages,
    };
    let _ = cmd.execute(input, user3).await.unwrap();

    let msg_storage = db.service::<Arc<dyn ToDeviceStorage>>();
    let query_dev1 = Pagination::new(Range::all(), ()).with_limit(Limit::from(5));
    let events_dev1 = msg_storage
        .query_to_device_events(&device2, &query_dev1)
        .await
        .unwrap();
    let events_dev1 = events_dev1.records();
    assert_eq!(1, events_dev1.len());
    let query_dev3 = Pagination::new(Range::all(), ()).with_limit(Limit::from(5));
    let events = msg_storage
        .query_to_device_events(&device3, &query_dev3)
        .await
        .unwrap();
    assert_eq!(1, events.records().len());

    let query_dev4 = Pagination::new(Range::all(), ()).with_limit(Limit::from(5));
    let events = msg_storage
        .query_to_device_events(&device4, &query_dev4)
        .await
        .unwrap();
    assert_eq!(1, events.records().len());

    let query_dev5 = Pagination::new(Range::all(), ()).with_limit(Limit::from(5));
    let events = msg_storage
        .query_to_device_events(&device5, &query_dev5)
        .await
        .unwrap();
    assert_eq!(0, events.records().len());

    let first_bus_message = rx.recv().await.unwrap();
    assert!(matches!(first_bus_message, BusMessage::ToDevice(_, _)));
    let empty_obj = serde_json::Value::Object(serde_json::Map::new());
    assert_eq!(ToDeviceEventType::Dummy, events_dev1[0].kind);
    assert_eq!(empty_obj, events_dev1[0].data);
}

#[tokio::test]
async fn it_does_not_store_mismatching_events() {
    let module = Box::new(ToDeviceModule);
    let db = TestingApp::with_modules(vec![module]).await;
    let user1 = db.register().await;
    let dummy_content = ToDeviceDummyEventContent::new();
    let event = Raw::new(&AnyToDeviceEventContent::Dummy(dummy_content)).unwrap();
    let cmd = db.command::<SendToDeviceCommand>();
    let mut messages = BTreeMap::new();
    let mut msg_user1 = BTreeMap::new();
    msg_user1.insert(DeviceIdOrAllDevices::AllDevices, event.clone());
    messages.insert(user1.user().to_owned(), msg_user1);

    let input = send_event_to_device::v3::Request {
        event_type: ToDeviceEventType::RoomKeyRequest,
        txn_id: TransactionId::new(),
        messages,
    };
    let cmd_result = cmd.execute(input, user1).await;

    assert!(cmd_result.is_err());
}
