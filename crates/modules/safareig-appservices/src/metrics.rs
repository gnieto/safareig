use lazy_static::lazy_static;
use opentelemetry::metrics::Counter;

lazy_static! {
    pub static ref OUTGOING_APPSERVICES_EVENTS: Counter<u64> =
        opentelemetry::global::meter("safareig/appservices")
            .u64_counter("outgoing_appservices_events")
            .with_description("Amount of outgoing apsservices events")
            .init();
}
