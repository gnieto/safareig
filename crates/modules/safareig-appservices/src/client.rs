use std::str::FromStr;

use reqwest::Url;
use safareig_core::ruma::{
    api::{IncomingResponse, MatrixVersion, OutgoingRequest, SendAccessToken},
    exports::http,
};
use tracing::instrument;

use crate::error::AppserviceError;

const CONSIDERING_VERSIONS: &[MatrixVersion] = &[
    MatrixVersion::V1_0,
    MatrixVersion::V1_1,
    MatrixVersion::V1_2,
    MatrixVersion::V1_3,
    MatrixVersion::V1_4,
    MatrixVersion::V1_5,
    MatrixVersion::V1_6,
    MatrixVersion::V1_7,
    MatrixVersion::V1_8,
    MatrixVersion::V1_9,
    MatrixVersion::V1_10,
    MatrixVersion::V1_11,
];

pub(crate) struct AppServiceClient {
    client: reqwest::Client,
    remote: String,
    access_token: String,
}

impl AppServiceClient {
    pub fn new(client: reqwest::Client, remote: String, access_token: String) -> Self {
        Self {
            client,
            remote,
            access_token,
        }
    }

    #[instrument(name = "appservice_client::request", err, skip(self, request), fields(path = R::METADATA.history.all_paths().next(), remote = %self.remote))]
    pub async fn request<R: OutgoingRequest>(
        &self,
        request: R,
    ) -> Result<R::IncomingResponse, AppserviceError> {
        let http_r: http::Request<Vec<u8>> = request
            .try_into_http_request(
                self.remote.as_str(),
                SendAccessToken::IfRequired(&self.access_token),
                CONSIDERING_VERSIONS,
            )
            .unwrap();
        let (parts, body) = http_r.into_parts();
        let uri_str = format!("{}?access_token={}", parts.uri, self.access_token);

        let request = self
            .client
            .request(parts.method, Url::from_str(&uri_str).unwrap())
            .headers(parts.headers)
            .body(body)
            .build()
            .unwrap();

        let result = self.client.execute(request).await?.error_for_status()?;

        let bytes = result.bytes().await?.to_vec();

        let http_resp = http::Response::new(bytes);
        let response: R::IncomingResponse = R::IncomingResponse::try_from_http_response(http_resp)
            .map_err(|e| AppserviceError::Custom(e.to_string()))?;

        Ok(response)
    }
}
