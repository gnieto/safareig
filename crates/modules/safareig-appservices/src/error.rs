use safareig_core::{
    events::EventError,
    ruma::{
        api::client::error::{ErrorBody, ErrorKind},
        exports::http,
    },
    storage::StorageError,
};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum AppserviceError {
    #[error("Invalid regex")]
    InvalidRegex(#[from] regex::Error),
    #[error("Given token is not from appservice")]
    MissingAppserviceToken,
    #[error("Unexpected appservice id: Expected {0} and got {1}")]
    UnexpectedAppService(String, String),
    #[error("Error while talking to the database")]
    StorageError(#[from] StorageError),
    #[error("Error while sending batch to appservice: {0}")]
    ReqwestError(#[from] ::reqwest::Error),
    #[error("Error while converting event")]
    EventError(#[from] EventError),
    #[error("{0}")]
    Custom(String),
}

impl From<AppserviceError> for safareig_core::ruma::api::client::Error {
    fn from(error: AppserviceError) -> Self {
        match error {
            AppserviceError::MissingAppserviceToken
            | AppserviceError::UnexpectedAppService(_, _) => {
                safareig_core::ruma::api::client::Error {
                    status_code: http::StatusCode::UNAUTHORIZED,
                    body: ErrorBody::Standard {
                        kind: ErrorKind::forbidden(),
                        message: error.to_string(),
                    },
                }
            }
            _ => safareig_core::ruma::api::client::Error {
                status_code: http::StatusCode::INTERNAL_SERVER_ERROR,
                body: ErrorBody::Standard {
                    kind: ErrorKind::Unknown,
                    message: error.to_string(),
                },
            },
        }
    }
}
