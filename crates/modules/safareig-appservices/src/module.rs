use std::sync::Arc;

use graceful::BackgroundHandler;
use safareig::{
    client::room::{loader::RoomLoader, RoomStream},
    storage::{Checkpoint, CheckpointStorage},
};
use safareig_core::{
    bus::Bus,
    command::{Container, Module, ServiceCollectionExtension},
    config::HomeserverConfig,
    ddi::{ServiceCollectionExt, ServiceProvider, ServiceResolverExt},
};
use safareig_rooms::alias::storage::AliasStorage;

use crate::{
    command::PingCommand, worker::AppServiceWorker, Appservice, AppserviceFilesystemRepository,
    AppserviceRepository,
};

#[derive(Clone, Default)]
pub struct AppserviceModule {}

#[async_trait::async_trait]
impl<R: safareig_core::command::Router> Module<R> for AppserviceModule {
    fn register(&self, container: &mut Container, router: &mut R) {
        container
            .service_collection
            .service_factory(|config: &Arc<HomeserverConfig>| {
                let repository: Arc<dyn AppserviceRepository> =
                    Arc::new(AppserviceFilesystemRepository::new(&config.appservices).unwrap());
                Ok(repository)
            });
        container.service_collection.register::<Appservice>();

        container.inject_endpoint::<_, PingCommand, _>(router);
    }

    async fn spawn_workers(
        &self,
        service_provider: &ServiceProvider,
        background: &BackgroundHandler,
    ) {
        let repository = service_provider
            .get::<Arc<dyn AppserviceRepository>>()
            .unwrap();
        let room_stream = service_provider.get::<RoomStream>().unwrap();
        let alias_storage = service_provider.get::<Arc<dyn AliasStorage>>().unwrap();
        let checkpoints = service_provider
            .get::<Arc<dyn CheckpointStorage<Checkpoint>>>()
            .unwrap();
        let room_loader = service_provider.get::<RoomLoader>().unwrap();
        let bus = service_provider.get::<Bus>().unwrap();

        for v in repository.all() {
            let app_worker = AppServiceWorker::new(
                v,
                room_stream.clone(),
                alias_storage.clone(),
                checkpoints.clone(),
                room_loader.clone(),
                bus.clone(),
                background.acquire().unwrap(),
            )
            .await;

            tokio::spawn(app_worker.run());
        }
    }
}
