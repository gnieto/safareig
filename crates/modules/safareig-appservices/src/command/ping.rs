use std::{sync::Arc, time::Duration};

use safareig_core::{
    auth::Identity,
    command::Command,
    ruma::api::{appservice::ping::send_ping, client::appservice::request_ping},
    Inject,
};

use crate::{client::AppServiceClient, error::AppserviceError, AppserviceRepository};

#[derive(Inject)]
pub struct PingCommand {
    repository: Arc<dyn AppserviceRepository>,
}

#[async_trait::async_trait]
impl Command for PingCommand {
    type Input = request_ping::v1::Request;
    type Output = request_ping::v1::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        if let Identity::AppService(_, appservice_id) = id {
            if appservice_id != input.appservice_id {
                return Err(AppserviceError::UnexpectedAppService(
                    appservice_id,
                    input.appservice_id,
                )
                .into());
            }
        } else {
            return Err(AppserviceError::MissingAppserviceToken.into());
        }

        let config =
            self.repository
                .get_by_id(&input.appservice_id)
                .ok_or(AppserviceError::Custom(
                    "could not found appsevice configuration".to_string(),
                ))?;

        // TODO: Do not construct a new client on every request
        let custom_client = AppServiceClient::new(
            reqwest::ClientBuilder::default().build().unwrap(),
            config
                .url()
                .ok_or(AppserviceError::Custom(
                    "missing URL in configuration".to_string(),
                ))?
                .to_owned(),
            config.homeserver_token().to_owned(),
        );

        let request = send_ping::v1::Request::new();
        let _ = custom_client.request(request).await?;

        Ok(request_ping::v1::Response {
            duration: Duration::from_secs(10),
        })
    }
}
