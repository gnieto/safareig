use std::{collections::HashMap, sync::Arc};

use futures::FutureExt;
use graceful::WaitResult;
use opentelemetry::KeyValue;
use safareig::{
    client::room::{loader::RoomLoader, RoomStream},
    core::{events::Event, waker::Waker},
    storage::{Checkpoint, CheckpointStorage},
};
use safareig_core::{
    bus::Bus,
    pagination::{self, PaginationResponse},
    ruma::{
        api::appservice::event::push_events::v1 as push_events, events::TimelineEventType,
        exports::http::Uri, OwnedRoomId, OwnedTransactionId, TransactionId,
    },
    storage::StorageError,
    StreamToken,
};
use safareig_rooms::alias::storage::AliasStorage;
use tokio::time::{sleep, Duration};
use tracing::instrument;

use crate::{
    client::AppServiceClient, error::AppserviceError, metrics::OUTGOING_APPSERVICES_EVENTS,
    AppServiceConfig,
};

// TODO: Probably this should not be public
pub struct AppServiceWorker {
    config: AppServiceConfig,
    alias: Arc<dyn AliasStorage>,
    room_stream: RoomStream,
    room_loader: RoomLoader,
    checkpoints: Arc<dyn CheckpointStorage<Checkpoint>>,
    custom_client: AppServiceClient,
    checkpoint: Checkpoint,
    worker_id: String,
    tracked_rooms: HashMap<OwnedRoomId, bool>,
    bus: Bus,
    link: graceful::Link,
}

impl AppServiceWorker {
    pub async fn new(
        config: AppServiceConfig,
        room_stream: RoomStream,
        alias: Arc<dyn AliasStorage>,
        checkpoints: Arc<dyn CheckpointStorage<Checkpoint>>,
        room_loader: RoomLoader,
        bus: Bus,
        link: graceful::Link,
    ) -> Self {
        // TODO: Error handling
        let url: Uri = config
            .url()
            .and_then(|url| url.parse().ok())
            .expect("Uri is set and it's parseable");
        let worker_id = format!("app_{}", config.id());

        let custom_client = AppServiceClient::new(
            reqwest::ClientBuilder::default().build().unwrap(),
            url.to_string(),
            config.homeserver_token().to_owned(),
        );
        let current = room_stream.current_stream_token().await.unwrap_or_default();
        let checkpoint = checkpoints
            .get_checkpoint(&worker_id)
            .await
            .ok()
            .flatten()
            .unwrap_or(Checkpoint {
                latest: current,
                in_flight: None,
                txn_id: None,
            });

        Self {
            config,
            room_stream,
            alias,
            room_loader,
            checkpoints,
            custom_client,
            checkpoint,
            worker_id,
            tracked_rooms: HashMap::new(),
            bus,
            link,
        }
    }

    #[instrument(name = "appservices_worker::run", skip(self), fields(appservice = self.config.id()))]
    pub async fn run(mut self) {
        tracing::debug!("Running worker {}", self.worker_id);
        if let Err(e) = self.send_pending_events().await {
            tracing::error!("Error sending pending events: {}", e);
        }

        loop {
            let event_result = self.retrieve_events().await;

            let wait_for_events = match &event_result {
                Ok(r) => {
                    let token = r.next().cloned();
                    let head_above_latest =
                        token.map(|t| t > self.checkpoint.latest).unwrap_or(false);
                    let has_new_content = head_above_latest || !r.is_empty();

                    !has_new_content
                }
                Err(e) => {
                    tracing::error!(
                        latest = self.checkpoint.latest.to_string().as_str(),
                        err = e.to_string().as_str(),
                        appservice = self.config.id(),
                        "Errored while fetching new events to send to app service",
                    );

                    true
                }
            };

            if wait_for_events {
                let wakeup = Waker::any_event(self.bus.receiver()).boxed().fuse();

                match self.link.wait_select(wakeup).await {
                    WaitResult::Stop => break,
                    WaitResult::Result(_) => continue,
                }
            }

            let stream_response = event_result.unwrap();
            let max_token = stream_response.next().cloned();
            let events = stream_response.records();

            if let Err(e) = self.send_events(max_token, events).await {
                tracing::error!(
                    err = e.to_string().as_str(),
                    "could not send events to app service. Will wait some time to retry.",
                );

                let delay = sleep(Duration::from_secs(15)).boxed().fuse();
                match self.link.wait_select(delay).await {
                    WaitResult::Stop => break,
                    WaitResult::Result(_) => continue,
                }
            }

            // TODO: Register and send metrics
        }

        tracing::debug!("Closed worker: {}", self.worker_id);
    }

    async fn retrieve_events(
        &self,
    ) -> Result<PaginationResponse<StreamToken, (StreamToken, Event)>, StorageError> {
        let range = (
            pagination::Boundary::Exclusive(self.checkpoint.latest),
            pagination::Boundary::Unlimited,
        );

        let stream_response = self.room_stream.stream_events(range.into(), None).await?;

        Ok(stream_response)
    }

    async fn send_pending_events(&mut self) -> Result<(), AppserviceError> {
        if let Some((from, to)) = self.checkpoint.in_flight {
            let range = (
                pagination::Boundary::Exclusive(from),
                pagination::Boundary::Inclusive(to),
            );

            let events = self
                .room_stream
                .stream_events(range.into(), None)
                .await?
                .records()
                .into_iter()
                .collect();

            let txn_id = self
                .checkpoint
                .txn_id
                .clone()
                .unwrap_or_else(TransactionId::new);

            let _ = self.send_events_to_appservice(txn_id, events).await?;
            self.checkpoint_to(to).await?;
        }

        Ok(())
    }

    async fn checkpoint_preflight(
        &mut self,
        current: Option<StreamToken>,
    ) -> Result<OwnedTransactionId, StorageError> {
        let mut checkpoint = self.checkpoint.clone();
        checkpoint.in_flight = Some((checkpoint.latest, current.unwrap_or(checkpoint.latest)));
        let txn_id = TransactionId::new();
        checkpoint.txn_id = Some(txn_id.clone());

        self.checkpoints
            .update_checkpoint(&self.worker_id, &checkpoint)
            .await?;
        self.checkpoint = checkpoint;

        Ok(txn_id)
    }

    async fn send_events_to_appservice(
        &mut self,
        txn_id: OwnedTransactionId,
        events: Vec<(StreamToken, Event)>,
    ) -> Result<push_events::Response, AppserviceError> {
        let mut out_events = Vec::new();
        for (_, room_event) in events.into_iter() {
            // This async function prevents doing a more functional processing.
            let is_tracked = self.is_tracked(&room_event).await;

            if is_tracked {
                out_events.push(room_event.try_into()?)
            }
        }

        let outgoing_events_count = out_events.len();
        let request = push_events::Request::new(txn_id, out_events);
        let result = self.custom_client.request(request).await?;

        OUTGOING_APPSERVICES_EVENTS.add(
            u64::try_from(outgoing_events_count).unwrap_or(0),
            &[KeyValue::new("appservice", self.config.id().to_string())],
        );

        Ok(result)
    }

    #[allow(clippy::wrong_self_convention)]
    async fn is_tracked(&mut self, room_event: &Event) -> bool {
        if room_event.kind == TimelineEventType::RoomMember {
            // If it's a RoomMember event, it may change the interest on this room. Update the decision on tracked_rooms.
            let interest = self
                .config
                .is_interested_on(self.alias.as_ref(), &self.room_loader, room_event)
                .await;
            self.tracked_rooms
                .insert(room_event.room_id.to_owned(), interest);
            return interest;
        }

        if let Some(tracked) = self.tracked_rooms.get(&room_event.room_id) {
            // If current room is tracked as either tracked or not tracked, we can short-circuit the search
            return *tracked;
        }

        // If current room id is not tracked yet, let's check for any possible match
        let interest = self
            .config
            .is_interested_on(self.alias.as_ref(), &self.room_loader, room_event)
            .await;
        self.tracked_rooms
            .insert(room_event.room_id.to_owned(), interest);

        interest
    }

    async fn checkpoint_to(&mut self, latest: StreamToken) -> Result<(), AppserviceError> {
        let mut checkpoint = self.checkpoint.clone();
        checkpoint.in_flight = None;
        checkpoint.txn_id = None;
        checkpoint.latest = latest;

        self.checkpoints
            .update_checkpoint(&self.worker_id, &checkpoint)
            .await?;
        self.checkpoint = checkpoint;

        Ok(())
    }

    #[instrument(name = "appservices_worker::send_events", skip(self), fields(events = events.len()))]
    async fn send_events(
        &mut self,
        to: Option<StreamToken>,
        events: Vec<(StreamToken, Event)>,
    ) -> Result<(), AppserviceError> {
        let txn_id = self.checkpoint_preflight(to).await?;
        let _ = self.send_events_to_appservice(txn_id, events).await?;

        if let Some(to) = to {
            self.checkpoint_to(to).await?
        }

        Ok(())
    }
}
