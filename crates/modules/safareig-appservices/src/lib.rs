pub(crate) mod client;
pub mod command;
mod error;
pub(crate) mod metrics;
pub mod module;
mod worker;

use std::{collections::HashMap, path::PathBuf, sync::Arc};

use error::AppserviceError;
use safareig::{
    client::room::{loader::RoomLoader, Room},
    core::events::Event,
};
use safareig_core::{
    ruma::{self, api::appservice::Registration, events::TimelineEventType, OwnedUserId, UserId},
    storage::StorageError,
    Inject,
};
use safareig_rooms::alias::storage::AliasStorage;

pub trait AppserviceRepository: Send + Sync {
    fn get_by_id(&self, id: &str) -> Option<&AppServiceConfig>;
    fn get_by_token(&self, token: &str) -> Option<&AppServiceConfig>;
    fn all(&self) -> Vec<AppServiceConfig>;
}

pub struct AppserviceFilesystemRepository {
    services: HashMap<String, AppServiceConfig>,
}

impl AppserviceFilesystemRepository {
    pub fn new(paths: &HashMap<String, PathBuf>) -> Result<Self, AppserviceError> {
        let mut services = HashMap::new();

        for (name, path) in paths {
            let content =
                std::fs::read(path).map_err(|e| AppserviceError::Custom(e.to_string()))?;
            let registration: Registration = serde_yaml::from_slice(content.as_slice())
                .map_err(|e| AppserviceError::Custom(e.to_string()))?;
            let app_server_config = AppServiceConfig::new(registration, name.to_owned())?;

            services.insert(name.clone(), app_server_config);
        }

        Ok(Self { services })
    }
}

impl AppserviceRepository for AppserviceFilesystemRepository {
    fn get_by_id(&self, id: &str) -> Option<&AppServiceConfig> {
        self.services.get(id)
    }

    fn get_by_token(&self, token: &str) -> Option<&AppServiceConfig> {
        self.services
            .iter()
            .find(|(_, config)| config.appservice_token() == token)
            .map(|(_, config)| config)
    }

    fn all(&self) -> Vec<AppServiceConfig> {
        self.services.values().map(|c| c.to_owned()).collect()
    }
}

#[derive(Clone, Inject)]
pub struct Appservice {
    repository: Arc<dyn AppserviceRepository>,
}

impl Appservice {
    /// Returns appservice name if user is exclusive to any namespace
    pub fn is_user_exclusive(&self, user_id: &UserId) -> Option<String> {
        self.repository
            .all()
            .iter()
            .find(|c| c.is_user_exclusive(user_id))
            .map(|c| c.name.to_owned())
    }

    /// Returns appservice name associated to the given app service token
    pub fn name_for_token(&self, token: &str) -> Option<String> {
        self.repository
            .get_by_token(token)
            .map(|c| c.name.to_owned())
    }
}

#[derive(Clone, Debug)]
pub struct AppServiceConfig {
    name: String,
    registration: Registration,
    namespaces: Namespaces,
}

impl AppServiceConfig {
    pub fn new(
        registration: ruma::api::appservice::Registration,
        name: String,
    ) -> Result<Self, AppserviceError> {
        let namespaces = Namespaces::try_from(&registration.namespaces)?;

        Ok(Self {
            registration,
            namespaces,
            name,
        })
    }

    pub fn url(&self) -> Option<&str> {
        self.registration.url.as_deref()
    }

    pub fn id(&self) -> &str {
        &self.registration.id
    }

    pub fn homeserver_token(&self) -> &str {
        &self.registration.hs_token
    }

    pub fn appservice_token(&self) -> &str {
        &self.registration.as_token
    }

    pub fn is_user_exclusive(&self, user_id: &UserId) -> bool {
        dbg!(&user_id);
        self.namespaces.users.iter().any(|namespace| {
            dbg!(namespace.exclusive) && dbg!(namespace.regex.is_match(user_id.as_str()))
        })
    }

    pub async fn is_interested_on<'a>(
        &'a self,
        alias_storage: &dyn AliasStorage,
        room_loader: &'a RoomLoader,
        room_event: &'a Event,
    ) -> bool {
        let aliases = alias_storage
            .room_aliases(&room_event.room_id)
            .await
            .unwrap_or_default();
        let alias_interested = self.namespaces.aliases.iter().any(|n| {
            aliases
                .iter()
                .any(|room_alias| n.regex.is_match(room_alias.alias.as_str()))
        });

        if alias_interested {
            return alias_interested;
        }

        let room_interested = self
            .namespaces
            .rooms
            .iter()
            .any(|n| n.regex.is_match(room_event.room_id.as_str()));

        if room_interested {
            return room_interested;
        }

        for n in self.namespaces.users.iter() {
            if n.regex.is_match(room_event.sender.as_str()) {
                return true;
            }

            if room_event.kind == TimelineEventType::RoomMember {
                let state_key_matches = room_event
                    .state_key
                    .as_ref()
                    .map(|state_key| n.regex.is_match(state_key))
                    .unwrap_or(false);

                if state_key_matches {
                    return true;
                }
            }

            // Most expensive call. Does this room contains any room member matching with the pattern?
            if let Some(room) = room_loader.get(&room_event.room_id).await.ok().flatten() {
                if let Ok(members) = self.load_room_members(&room).await {
                    let any_member = members.iter().any(|m| n.regex.is_match(m.as_str()));

                    if any_member {
                        return true;
                    }
                }
            }
        }

        false
    }

    async fn load_room_members(&self, room: &Room) -> Result<Vec<OwnedUserId>, StorageError> {
        let state = room
            .state_at_leaves()
            .await
            .map_err(|e| StorageError::Custom(e.to_string()))?;

        state.members().await
    }
}

#[derive(Clone, Debug)]
pub struct Namespaces {
    users: Vec<Namespace>,
    rooms: Vec<Namespace>,
    aliases: Vec<Namespace>,
}

impl Namespaces {
    pub fn parse_namespace(
        namespace: &[ruma::api::appservice::Namespace],
    ) -> Result<Vec<Namespace>, AppserviceError> {
        namespace.iter().map(Namespace::try_from).collect()
    }
}

impl TryFrom<&ruma::api::appservice::Namespaces> for Namespaces {
    type Error = AppserviceError;

    fn try_from(value: &ruma::api::appservice::Namespaces) -> Result<Self, Self::Error> {
        Ok(Namespaces {
            users: Self::parse_namespace(&value.users)?,
            rooms: Self::parse_namespace(&value.rooms)?,
            aliases: Self::parse_namespace(&value.aliases)?,
        })
    }
}

#[derive(Clone, Debug)]
pub struct Namespace {
    exclusive: bool,
    regex: regex::Regex,
}

impl TryFrom<&ruma::api::appservice::Namespace> for Namespace {
    type Error = AppserviceError;

    fn try_from(value: &ruma::api::appservice::Namespace) -> Result<Self, Self::Error> {
        Ok(Self {
            exclusive: value.exclusive,
            regex: regex::Regex::new(value.regex.as_str())?,
        })
    }
}
