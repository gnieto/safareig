use std::{collections::HashSet, sync::Arc};

use futures::FutureExt;
use graceful::{BackgroundHandler, Link, WaitResult};
use safareig::core::waker::Waker;
use safareig_core::{bus::Bus, ruma::OwnedUserId};
use safareig_users::storage::{User, UserStorage};

use super::push_notifications::PushNotificationFactory;
pub struct PushNotificationSupervisor {
    user_storage: Arc<dyn UserStorage>,
    users: HashSet<OwnedUserId>,
    background: BackgroundHandler,
    link: Link,
    bus: Bus,
    factory: PushNotificationFactory,
}

impl PushNotificationSupervisor {
    pub fn new(
        user_storage: Arc<dyn UserStorage>,
        background: BackgroundHandler,
        bus: Bus,
        factory: PushNotificationFactory,
    ) -> Self {
        Self {
            user_storage,
            users: HashSet::new(),
            link: background.acquire().unwrap(),
            background,
            factory,
            bus,
        }
    }

    pub async fn run(mut self) {
        let users: Vec<_> = self
            .user_storage
            .all_users()
            .await
            .unwrap_or_default()
            .into_iter()
            .filter(User::is_active)
            .collect();

        for u in users {
            let user_id = u.user_id.clone();
            self.spawn_user_worker(user_id, Some(u)).await;
        }

        loop {
            let wakeup = Waker::user_registered(self.bus.receiver()).boxed().fuse();

            match self.link.wait_select(wakeup).await {
                WaitResult::Stop => break,
                WaitResult::Result(user_id) => {
                    self.spawn_user_worker(user_id, None).await;
                }
            }
        }
    }

    async fn spawn_user_worker(&mut self, user_id: OwnedUserId, user: Option<User>) {
        let is_spawned = self.users.contains(&user_id);
        if !is_spawned {
            self.users.insert(user_id.clone());

            let is_active = match user {
                Some(user) => user.is_active(),
                None => match self.user_storage.user_by_id(&user_id).await {
                    Ok(user) => user.map(|u| u.is_active()).unwrap_or(false),
                    Err(error) => {
                        tracing::error!(?error, ?user_id, "could not load user");

                        false
                    }
                },
            };

            if !is_active {
                return;
            }

            if let Some(new_link) = self.background.acquire() {
                let push_notification_worker = self.factory.build(new_link, &user_id).await;
                tokio::spawn(push_notification_worker.run());
            } else {
                tracing::warn!("Trying to spawn a push notification worker after shutdown started");
            }
        }
    }
}
