use std::{
    sync::Arc,
    time::{Duration, SystemTime},
};

use graceful::WaitResult;

use crate::storage::NotificationStorage;

pub struct CleanNotificationsWorker {
    notifications: Arc<dyn NotificationStorage>,
    link: graceful::Link,
}

impl CleanNotificationsWorker {
    pub fn new(link: graceful::Link, notifications: Arc<dyn NotificationStorage>) -> Self {
        CleanNotificationsWorker {
            notifications,
            link,
        }
    }

    pub async fn run(mut self) {
        tracing::info!("Spawn clean notifications worker");

        loop {
            // TODO: Allow configuring the clean up threshold
            let a_week_ago = SystemTime::now()
                .checked_sub(Duration::from_secs(3600 * 24 * 7))
                .unwrap_or(SystemTime::UNIX_EPOCH);

            if let Err(error) = self
                .notifications
                .remove_notifications_before(a_week_ago)
                .await
            {
                tracing::error!(?error, "could not remove old notifications");
            }

            match self.link.wait_sleep(Duration::from_secs(3600)).await {
                WaitResult::Stop => break,
                WaitResult::Result(_) => continue,
            }
        }

        tracing::debug!("Closed clean notifications worker");
    }
}
