use std::{convert::TryFrom, sync::Arc};

use futures::FutureExt;
use graceful::WaitResult;
use safareig::{
    client::room::{loader::RoomLoader, state::RoomState, Room, RoomStream},
    core::events::Event,
    storage::{Checkpoint, CheckpointStorage},
};
use safareig_core::{
    bus::{Bus, BusMessage},
    error::error_chain,
    pagination::{self, PaginationResponse},
    ruma::{
        push::{Action, PushConditionPowerLevelsCtx, PushConditionRoomCtx, Tweak},
        serde::Raw,
        OwnedUserId, UInt, UserId,
    },
    storage::StorageError,
    ServerInfo, StreamToken,
};
use safareig_users::storage::UserStorage;

use crate::{
    metrics::NOTIFICATION_PROCESSED_EVENTS,
    storage::{Notification, NotificationStorage},
    PushRuleRepository,
};

pub struct PushRuleWorker {
    services: WorkerServices,
    checkpoint: Checkpoint,
    worker_id: String,
    server_info: ServerInfo,
}

impl PushRuleWorker {
    #[allow(clippy::too_many_arguments)]
    pub async fn new(
        room_stream: RoomStream,
        room_loader: RoomLoader,
        notifications: Arc<dyn NotificationStorage>,
        push_repository: PushRuleRepository,
        checkpoints: Arc<dyn CheckpointStorage<Checkpoint>>,
        server_info: ServerInfo,
        users: Arc<dyn UserStorage>,
        bus: Bus,
        link: graceful::Link,
    ) -> Self {
        let worker_services = WorkerServices {
            room_stream: room_stream.clone(),
            room_loader,
            notifications,
            push_rules: push_repository,
            checkpoints: checkpoints.clone(),
            users,
            bus,
            link,
        };

        let worker_id = "push_rules".to_string();
        let current = room_stream.current_stream_token().await.unwrap_or_default();
        let checkpoint = checkpoints
            .get_checkpoint(&worker_id)
            .await
            .ok()
            .flatten()
            .unwrap_or(Checkpoint {
                latest: current,
                in_flight: None,
                txn_id: None,
            });

        PushRuleWorker {
            services: worker_services,
            checkpoint,
            worker_id,
            server_info,
        }
    }

    pub async fn run(mut self) {
        tracing::info!("Spawned PushRules worker");

        loop {
            let event_result = self.retrieve_events().await;
            let wait_for_events = match &event_result {
                Ok(r) => {
                    let token = r.next().cloned();
                    let head_above_latest =
                        token.map(|t| t > self.checkpoint.latest).unwrap_or(false);
                    let has_new_content = head_above_latest || !r.is_empty();

                    !has_new_content
                }
                Err(e) => {
                    tracing::error!(
                        latest = self.checkpoint.latest.to_string().as_str(),
                        err = e.to_string().as_str(),
                        worker_id = self.worker_id.as_str(),
                        "Errored while fetching new events to send to app service",
                    );

                    true
                }
            };

            if wait_for_events {
                let mut rx = self.services.bus.receiver();
                let next = rx.recv().boxed().fuse();
                match self.services.link.wait_select(next).await {
                    WaitResult::Stop => break,
                    WaitResult::Result(_) => continue,
                }
            }

            let mut notifications = Vec::new();
            let event_result = event_result.unwrap();
            let latest_token = event_result.next().cloned();
            let events = event_result.records();
            let event_amount = events.len();

            for (event_token, e) in events {
                let process_result = self
                    .process_event(&event_token, &e, &mut notifications)
                    .await;

                if let Err(e) = process_result {
                    tracing::error!(err = error_chain(&e), "Error processing event",);
                }
            }

            if !notifications.is_empty() {
                if let Err(e) = self
                    .services
                    .notifications
                    .store_notifications(notifications.as_ref())
                    .await
                {
                    tracing::error!(
                        err = error_chain(&e).as_str(),
                        "Could not store notifications"
                    );
                    // Do not store a new checkpoint and retry later.
                    continue;
                };
            }

            NOTIFICATION_PROCESSED_EVENTS.add(u64::try_from(event_amount).unwrap_or(0), &[]);

            if let Some(token) = latest_token {
                self.checkpoint.latest = token;

                if let Err(e) = self
                    .services
                    .checkpoints
                    .update_checkpoint(&self.worker_id, &self.checkpoint)
                    .await
                {
                    // TODO: Return to the previous checkpoint
                    tracing::error!(err = e.to_string().as_str(), "Could not store checkpoint",);
                }

                self.services
                    .bus
                    .send_message(BusMessage::StreamUpdate(token));
            }
        }

        tracing::debug!(worker_id = self.worker_id.as_str(), "Closed worker",);
    }

    async fn process_event(
        &self,
        stream_token: &StreamToken,
        event: &Event,
        notifications: &mut Vec<Notification>,
    ) -> Result<(), StorageError> {
        let room = self
            .services
            .room_loader
            .get(&event.room_id)
            .await
            .map_err(|_| StorageError::Custom("could not load room".into()))?;
        if room.is_none() {
            return Ok(());
        }
        let room = room.unwrap();
        let state = room
            .state_at_leaves()
            .await
            .map_err(|e| StorageError::Custom(e.to_string()))?;

        let members: Vec<OwnedUserId> = state
            .members()
            .await?
            .into_iter()
            .filter(|user| self.server_info.is_local_server(user.server_name()))
            .collect();
        let mut push_context =
            Self::push_condition_context(&room, &state, members.len() as u16).await;
        let raw_event = Raw::new(event).unwrap();

        for m in members {
            if m == event.sender {
                continue;
            }

            let is_active_user = self
                .services
                .users
                .user_by_id(&m)
                .await?
                .map(|u| u.is_active())
                .unwrap_or(false);

            if !is_active_user {
                continue;
            }

            if let Ok(membership_event) = room.membership(&state, &m).await {
                push_context.user_id = m.to_owned();
                push_context.user_display_name =
                    membership_event.content.displayname.unwrap_or_default();
            } else {
                continue;
            }

            let ruleset = self.services.push_rules.load_rules(&m).await?;

            if let Some(push_rule) = ruleset.get_match(&raw_event, &push_context) {
                let mut notification = false;
                let mut highlight = false;

                for a in push_rule.actions() {
                    match a {
                        Action::Notify => notification = true,
                        Action::SetTweak(Tweak::Highlight(true)) => highlight = true,
                        _ => (),
                    }
                }

                let notification = Notification {
                    user_id: m.clone(),
                    event_id: event.event_ref.owned_event_id(),
                    room_id: event.room_id.to_owned(),
                    stream_token: *stream_token,
                    push_rule: push_rule.rule_id().to_owned(),
                    is_highlight: highlight,
                    is_notification: notification,
                };
                notifications.push(notification);
            }
        }

        Ok(())
    }

    async fn push_condition_context(
        room: &Room,
        state: &RoomState,
        member_count: u16,
    ) -> PushConditionRoomCtx {
        let power_levels = room.power_levels(state).await.unwrap_or_default();

        let power_levels_ctx = PushConditionPowerLevelsCtx {
            users: power_levels.users,
            users_default: power_levels.users_default,
            notifications: power_levels.notifications,
        };

        let ctx = PushConditionRoomCtx {
            room_id: room.id().to_owned(),
            member_count: UInt::new_wrapping(member_count as u64),
            user_id: UserId::parse("@a:localhost").unwrap(),
            user_display_name: "".to_string(),
            power_levels: Some(power_levels_ctx),
        };

        ctx
    }

    async fn retrieve_events(
        &self,
    ) -> Result<PaginationResponse<StreamToken, (StreamToken, Event)>, StorageError> {
        let range = (
            pagination::Boundary::Exclusive(self.checkpoint.latest),
            pagination::Boundary::Unlimited,
        );

        let events = self
            .services
            .room_stream
            .stream_events(range.into(), None)
            .await?;

        Ok(events)
    }
}

struct WorkerServices {
    room_stream: RoomStream,
    room_loader: RoomLoader,
    push_rules: PushRuleRepository,
    notifications: Arc<dyn NotificationStorage>,
    users: Arc<dyn UserStorage>,
    checkpoints: Arc<dyn CheckpointStorage<Checkpoint>>,
    bus: Bus,
    link: graceful::Link,
}
