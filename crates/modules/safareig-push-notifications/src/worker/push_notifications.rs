use std::sync::Arc;

use futures::FutureExt;
use graceful::WaitResult;
use safareig::{
    client::room::RoomStream,
    core::waker::Waker,
    storage::{Checkpoint, CheckpointStorage},
};
use safareig_core::{
    bus::{Bus, BusMessage},
    pagination::{Boundary, Pagination, PaginationResponse},
    ruma::{
        api::{
            client::push::PusherKind,
            push_gateway::send_event_notification::{
                v1 as send_push,
                v1::{Device, Notification as PushNotification, NotificationCounts},
            },
            MatrixVersion,
        },
        exports::http,
        push::{HttpPusherData, PushFormat},
        OwnedUserId, UInt, UserId,
    },
    storage::StorageError,
    StreamToken,
};

use crate::{
    metrics::OUTGOING_PUSH_EVENTS,
    storage::{Notification, NotificationStorage, PusherStorage},
    PushRuleError,
};

pub const CONSIDERING_VERSIONS: &[MatrixVersion] = &[
    MatrixVersion::V1_0,
    MatrixVersion::V1_1,
    MatrixVersion::V1_2,
    MatrixVersion::V1_3,
    MatrixVersion::V1_4,
    MatrixVersion::V1_5,
    MatrixVersion::V1_6,
    MatrixVersion::V1_7,
    MatrixVersion::V1_8,
];

pub struct PushNotificationFactory {
    worker_services: WorkerServices,
}

impl PushNotificationFactory {
    pub fn new(
        notifications: Arc<dyn NotificationStorage>,
        checkpoints: Arc<dyn CheckpointStorage<Checkpoint>>,
        room_stream: RoomStream,
        pusher_storage: Arc<dyn PusherStorage>,
        bus: Bus,
    ) -> Self {
        Self {
            worker_services: WorkerServices {
                notifications,
                checkpoints,
                room_stream,
                pusher_storage,
                bus,
            },
        }
    }

    pub async fn build(&self, link: graceful::Link, user_id: &UserId) -> PushNotificationWorker {
        PushNotificationWorker::new(self.worker_services.clone(), link, user_id).await
    }
}

pub struct PushNotificationWorker {
    services: WorkerServices,
    link: graceful::Link,
    checkpoint: Checkpoint,
    worker_id: String,
    user_id: OwnedUserId,
    pushers: Vec<Pusher>,
}

impl PushNotificationWorker {
    async fn new(worker_services: WorkerServices, link: graceful::Link, user_id: &UserId) -> Self {
        let worker_id = format!("pn_{user_id}");
        let current = worker_services
            .room_stream
            .current_stream_token()
            .await
            .unwrap_or_default();

        let checkpoint = worker_services
            .checkpoints
            .get_checkpoint(&worker_id)
            .await
            .ok()
            .flatten()
            .unwrap_or(Checkpoint {
                latest: current,
                in_flight: None,
                txn_id: None,
            });

        let pushers = worker_services
            .pusher_storage
            .all_pushers(user_id)
            .await
            .unwrap_or_default()
            .into_iter()
            .collect();

        PushNotificationWorker {
            services: worker_services,
            checkpoint,
            worker_id,
            user_id: user_id.to_owned(),
            pushers,
            link,
        }
    }

    pub async fn run(mut self) {
        if self.pushers.is_empty() {
            return;
        }

        tracing::info!(
            "Spawned Push notification worker for user: {} and {} pushers",
            self.user_id,
            self.pushers.len()
        );

        loop {
            let response = match self.retrieve_events().await {
                Ok(response) => response,
                Err(error) => {
                    tracing::error!(
                        last_checkpoint = *self.checkpoint.latest,
                        ?error,
                        "could not retrieve events",
                    );

                    let wakeup = Waker::any_event(self.services.bus.receiver())
                        .boxed()
                        .fuse();
                    match self.link.wait_select(wakeup).await {
                        WaitResult::Stop => break,
                        WaitResult::Result(_) => continue,
                    }
                }
            };

            let next_checkpoint = response.next().cloned().unwrap_or(self.checkpoint.latest);
            let events = response.records();
            tracing::debug!(
                worker_id = self.worker_id.as_str(),
                "Amount of notifications to be sent {}",
                events.len(),
            );

            if events.is_empty() {
                let rx = self.services.bus.receiver();
                let wakeup = Waker::wait_for(rx, |msg| match msg {
                    BusMessage::UpdatePushers(user) => user == &self.user_id,
                    BusMessage::RoomUpdate { .. } => true,
                    BusMessage::RoomCreated(_) | BusMessage::RoomJoined(_, _) => true,
                    _ => false,
                });

                match self.link.wait_select(wakeup.boxed().fuse()).await {
                    WaitResult::Stop => break,
                    WaitResult::Result(msg) => {
                        if let BusMessage::UpdatePushers(_) = msg {
                            let pushers = self
                                .services
                                .pusher_storage
                                .all_pushers(&self.user_id)
                                .await
                                .unwrap_or_default()
                                .into_iter()
                                .collect();
                            self.pushers = pushers;
                        }

                        continue;
                    }
                }
            }

            // TODO: check if read marker is above the current notifications
            for p in &self.pushers {
                if let PusherKind::Http(http_pusher) = &p.kind {
                    match http_pusher.format {
                        Some(PushFormat::EventIdOnly) => {
                            let last_notification: &Notification = events.last().unwrap();

                            if let Err(e) = self
                                .send_head_notification(p, http_pusher, last_notification)
                                .await
                            {
                                tracing::error!("Error sending head notification: {:?}", e);
                            }
                        }
                        _ => {
                            tracing::info!("Full message pushers is pending to be implemented");
                        }
                    }
                } else {
                    continue;
                }
            }

            self.update_checkpoint(next_checkpoint).await;
        }

        tracing::debug!("Closed worker: {}", self.worker_id);
    }

    async fn update_checkpoint(&mut self, token: StreamToken) {
        self.checkpoint.latest = token;

        if let Err(_e) = self
            .services
            .checkpoints
            .update_checkpoint(&self.worker_id, &self.checkpoint)
            .await
        {
            tracing::error!("Could not store the checkpoint. Will retry later");
        }
    }

    async fn retrieve_events(
        &self,
    ) -> Result<PaginationResponse<StreamToken, Notification>, StorageError> {
        let range = (
            Boundary::Exclusive(self.checkpoint.latest),
            Boundary::Unlimited,
        );
        tracing::debug!("Notifications range {:?}", range);
        let query = Pagination::new(range.into(), ());

        let result = self
            .services
            .notifications
            .user_notifications(&self.user_id, query)
            .await?;

        Ok(result)
    }

    async fn send_head_notification(
        &self,
        pusher: &Pusher,
        http_data: &HttpPusherData,
        notification: &Notification,
    ) -> Result<(), StorageError> {
        let notifications = self
            .services
            .notifications
            .all_notifications(&notification.user_id)
            .await?;
        let unread: u16 = notifications.values().map(|n| n.notification).sum();

        let devices = vec![Device::new(
            pusher.ids.app_id.clone(),
            pusher.ids.pushkey.clone(),
        )];
        let mut push_notification = PushNotification::new(devices);
        push_notification.event_id = Some(notification.event_id.to_owned());
        push_notification.room_id = Some(notification.room_id.to_owned());
        push_notification.counts =
            NotificationCounts::new(UInt::new_wrapping(unread.into()), Default::default());

        tracing::debug!(
            notification = format!("{push_notification:?}"),
            "Sending push notification",
        );

        let client = GatewayClient::new(reqwest::Client::new(), http_data.url.clone());
        if let Err(e) = client
            .request(send_push::Request::new(push_notification))
            .await
        {
            tracing::error!("Could not properly send a push notification");
            return Err(StorageError::Custom(e.to_string()));
        }

        OUTGOING_PUSH_EVENTS.add(1, &[]);

        Ok(())
    }
}

#[derive(Clone)]
struct WorkerServices {
    notifications: Arc<dyn NotificationStorage>,
    checkpoints: Arc<dyn CheckpointStorage<Checkpoint>>,
    room_stream: RoomStream,
    bus: Bus,
    pusher_storage: Arc<dyn PusherStorage>,
}

use std::str::FromStr;

use reqwest::Url;
use safareig_core::ruma::api::{
    client::push::Pusher, IncomingResponse, OutgoingRequest, SendAccessToken,
};

#[derive(Clone)]
pub struct GatewayClient {
    client: reqwest::Client,
    remote_endpoint: String,
}

impl GatewayClient {
    pub fn new(client: reqwest::Client, remote_endpoint: String) -> GatewayClient {
        GatewayClient {
            client,
            remote_endpoint,
        }
    }

    pub async fn request<R: OutgoingRequest>(
        &self,
        request: R,
    ) -> Result<R::IncomingResponse, PushRuleError> {
        let http_r: http::Request<Vec<u8>> = request
            .try_into_http_request(
                &self.remote_endpoint,
                SendAccessToken::None,
                CONSIDERING_VERSIONS,
            )
            .unwrap();
        let (parts, body) = http_r.into_parts();

        let uri_str = parts.uri.to_string();
        let request = self
            .client
            .request(parts.method, Url::from_str(&uri_str).unwrap())
            .body(body)
            .build()
            .unwrap();

        let result = self.client.execute(request).await?.error_for_status()?;

        let bytes = result.bytes().await?.to_vec();

        let http_resp = http::Response::new(bytes);
        let response: R::IncomingResponse = R::IncomingResponse::try_from_http_response(http_resp)
            .map_err(|_| PushRuleError::Unknown)?;

        Ok(response)
    }
}
