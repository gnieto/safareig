use lazy_static::lazy_static;
use opentelemetry::metrics::Counter;

lazy_static! {
    pub static ref OUTGOING_PUSH_EVENTS: Counter<u64> =
        opentelemetry::global::meter("safareig/push")
            .u64_counter("outgoing_push_events")
            .with_description("Amount of outgoing push events")
            .init();
    pub static ref NOTIFICATION_PROCESSED_EVENTS: Counter<u64> =
        opentelemetry::global::meter("safareig/notification")
            .u64_counter("notification_processed_events")
            .with_description("Amount of user notification events")
            .init();
}
