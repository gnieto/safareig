use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    command::Command,
    error::ResourceNotFound,
    ruma::{api::client::push, push::AnyPushRule},
    Inject,
};

use crate::PushRuleRepository;

#[derive(Inject)]
pub struct GetPushRuleActionsCommand {
    rule_set: PushRuleRepository,
}

#[async_trait]
impl Command for GetPushRuleActionsCommand {
    type Input = push::get_pushrule_actions::v3::Request;
    type Output = push::get_pushrule_actions::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let current = self.rule_set.load_rules(id.user()).await?;

        let actions = current
            .into_iter()
            .find(|rule: &AnyPushRule| rule.rule_id() == input.rule_id)
            .map(|rule: AnyPushRule| rule.actions().to_vec())
            .ok_or(ResourceNotFound)?;

        Ok(push::get_pushrule_actions::v3::Response::new(actions))
    }
}
