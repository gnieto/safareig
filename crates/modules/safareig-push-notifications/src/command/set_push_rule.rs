use async_trait::async_trait;
use reqwest::StatusCode;
use safareig_core::{
    auth::Identity,
    command::Command,
    ruma::api::client::{
        error::{ErrorBody, ErrorKind},
        push,
    },
    Inject,
};

use crate::PushRuleRepository;

#[derive(Inject)]
pub struct SetPushRuleCommand {
    rule_set: PushRuleRepository,
}

#[async_trait]
impl Command for SetPushRuleCommand {
    type Input = push::set_pushrule::v3::Request;
    type Output = push::set_pushrule::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let mut rule_set = self.rule_set.load_rules(id.user()).await?;
        rule_set
            .insert(input.rule, input.after.as_deref(), input.before.as_deref())
            .map_err(|e| safareig_core::ruma::api::client::Error {
                status_code: StatusCode::BAD_REQUEST,
                body: ErrorBody::Standard {
                    kind: ErrorKind::InvalidParam,
                    message: e.to_string(),
                },
            })?;

        self.rule_set.store_rules(id.user(), &rule_set).await?;

        Ok(push::set_pushrule::v3::Response::new())
    }
}
