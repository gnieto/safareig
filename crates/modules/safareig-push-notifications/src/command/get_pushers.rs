use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{auth::Identity, command::Command, ruma::api::client::push, Inject};

use crate::storage::PusherStorage;

#[derive(Inject)]
pub struct GetPushersCommand {
    push_storage: Arc<dyn PusherStorage>,
}

#[async_trait]
impl Command for GetPushersCommand {
    type Input = push::get_pushers::v3::Request;
    type Output = push::get_pushers::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, _: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let pushers = self.push_storage.all_pushers(id.user()).await?;

        Ok(push::get_pushers::v3::Response::new(pushers))
    }
}
