use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    command::Command,
    error::ResourceNotFound,
    ruma::{api::client::push, push::AnyPushRule},
    Inject,
};

use crate::PushRuleRepository;

#[derive(Inject)]
pub struct GetPushRuleCommand {
    rule_set: PushRuleRepository,
}

#[async_trait]
impl Command for GetPushRuleCommand {
    type Input = push::get_pushrule::v3::Request;
    type Output = push::get_pushrule::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let current = self.rule_set.load_rules(id.user()).await?;

        let push = current
            .get(input.kind, input.rule_id)
            .ok_or(ResourceNotFound)?
            .to_owned();

        let rule = match push {
            AnyPushRule::Content(push) => push.into(),
            AnyPushRule::Override(push) => push.into(),
            AnyPushRule::Room(push) => push.into(),
            AnyPushRule::Sender(push) => push.into(),
            AnyPushRule::Underride(push) => push.into(),
        };

        Ok(push::get_pushrule::v3::Response::new(rule))
    }
}
