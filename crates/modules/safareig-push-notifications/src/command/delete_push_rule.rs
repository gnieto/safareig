use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    command::Command,
    ruma::{api::client::push, push::AnyPushRule},
    Inject,
};

use crate::PushRuleRepository;

#[derive(Inject)]
pub struct DeletePushRuleCommand {
    rule_set: PushRuleRepository,
}

#[async_trait]
impl Command for DeletePushRuleCommand {
    type Input = push::delete_pushrule::v3::Request;
    type Output = push::delete_pushrule::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let mut ruleset = self.rule_set.load_rules(id.user()).await?;
        let rule = ruleset.get(input.kind, input.rule_id);

        if let Some(rule) = rule {
            match rule.to_owned() {
                AnyPushRule::Content(push) => ruleset.content.remove(&push),
                AnyPushRule::Override(push) => ruleset.override_.remove(&push),
                AnyPushRule::Room(push) => ruleset.room.remove(&push),
                AnyPushRule::Sender(push) => ruleset.sender.remove(&push),
                AnyPushRule::Underride(push) => ruleset.underride.remove(&push),
            };
        }

        self.rule_set.store_rules(id.user(), &ruleset).await?;

        Ok(push::delete_pushrule::v3::Response::new())
    }
}
