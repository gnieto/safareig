use async_trait::async_trait;
use safareig_core::{auth::Identity, command::Command, ruma::api::client::push, Inject};

use crate::PushRuleRepository;

#[derive(Inject)]
pub struct ListAll {
    rule_set: PushRuleRepository,
}

#[async_trait]
impl Command for ListAll {
    type Input = push::get_pushrules_all::v3::Request;
    type Output = push::get_pushrules_all::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, _: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let current = self.rule_set.load_rules(id.user()).await?;

        Ok(push::get_pushrules_all::v3::Response::new(current))
    }
}
