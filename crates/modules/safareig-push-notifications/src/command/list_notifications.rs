use std::{str::FromStr, sync::Arc};

use safareig::storage::EventStorage;
use safareig_core::{
    auth::Identity,
    command::Command,
    events::EventError,
    pagination::{Boundary, Direction, Limit, Pagination},
    ruma::{api::client::push::get_notifications, push::Ruleset, MilliSecondsSinceUnixEpoch, UInt},
    storage::StorageError,
    Inject, StreamToken,
};

use crate::{
    storage::{Notification, NotificationStorage},
    PushRuleError, PushRuleRepository,
};

#[derive(Inject)]
pub struct ListNotificationsCommand {
    notifications: Arc<dyn NotificationStorage>,
    events: Arc<dyn EventStorage>,
    push_rules: PushRuleRepository,
}

#[async_trait::async_trait]
impl Command for ListNotificationsCommand {
    type Input = get_notifications::v3::Request;
    type Output = get_notifications::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let stream_token = input
            .from
            .map(|token| StreamToken::from_str(token.as_ref()))
            .transpose()
            .map_err(|_| PushRuleError::InvalidToken)?
            .map(Boundary::Exclusive)
            .unwrap_or_else(|| Boundary::Unlimited);
        let limit = Limit::limit_or_default(input.limit, 25);
        let filter = NotificationFilter::from(input.only);

        let response = self
            .get_notifications(&id, limit, stream_token, &filter)
            .await?;

        Ok(response)
    }
}

impl ListNotificationsCommand {
    async fn get_notifications(
        &self,
        id: &Identity,
        limit: Limit,
        to: Boundary<StreamToken>,
        filter: &NotificationFilter,
    ) -> Result<<Self as Command>::Output, StorageError> {
        let range = (Boundary::Unlimited, to);
        let query = Pagination::new(range.into(), ())
            .with_limit(limit)
            .with_direction(Direction::Backward);
        let result = self
            .notifications
            .user_notifications(id.user(), query)
            .await?;

        let next_token = result.next().map(|token| token.to_string());
        let notifications = result
            .records()
            .into_iter()
            .rev()
            .filter(|n| filter.is_notification_allowed(n))
            .collect();

        self.create_response(id, notifications, next_token).await
    }

    pub async fn create_notification(
        &self,
        id: &Identity,
        rule_set: &Ruleset,
        notification: Notification,
    ) -> Result<get_notifications::v3::Notification, StorageError> {
        let event = self
            .events
            .get_event(&notification.event_id)
            .await?
            .ok_or_else(|| StorageError::Custom("missing user id".to_string()))?;
        let event_time = event.origin_server_ts;
        let room_id = event.room_id.to_owned();
        let read_marker = self
            .notifications
            .get_read_marker(id.user(), &event.room_id)
            .await?;

        let event = event
            .to_sync_timeline_event(id)
            .map_err(|e: EventError| StorageError::Custom(e.to_string()))?;

        let push_rule = rule_set
            .iter()
            .find(|rule| rule.rule_id() == notification.push_rule.as_str());

        Ok(get_notifications::v3::Notification {
            actions: push_rule
                .map(|rule| rule.actions().to_owned())
                .unwrap_or_default(),
            event,
            // TODO: What is this profile_tag?
            profile_tag: None,
            read: notification.stream_token <= read_marker,
            room_id,
            ts: MilliSecondsSinceUnixEpoch(UInt::new_saturating(event_time)),
        })
    }

    pub async fn create_response(
        &self,
        id: &Identity,
        notifications: Vec<Notification>,
        next_token: Option<String>,
    ) -> Result<<Self as Command>::Output, StorageError> {
        let mut matrix_notifications = Vec::with_capacity(notifications.len());
        let rule_set = self.push_rules.load_rules(id.user()).await?;

        for n in notifications.into_iter() {
            let matrix_notification = self.create_notification(id, &rule_set, n).await?;
            matrix_notifications.push(matrix_notification);
        }

        Ok(get_notifications::v3::Response {
            next_token,
            notifications: matrix_notifications,
        })
    }
}

enum NotificationFilter {
    Highlight,
    All,
}

impl NotificationFilter {
    pub fn is_notification_allowed(&self, notification: &Notification) -> bool {
        match self {
            NotificationFilter::All => true,
            NotificationFilter::Highlight => notification.is_highlight,
        }
    }
}

impl From<Option<String>> for NotificationFilter {
    fn from(only: Option<String>) -> Self {
        match only.as_deref() {
            Some("highlight") => NotificationFilter::Highlight,
            _ => NotificationFilter::All,
        }
    }
}
