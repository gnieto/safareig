use std::sync::Arc;

use async_trait::async_trait;
use safareig_core::{
    auth::Identity,
    bus::{Bus, BusMessage},
    command::Command,
    ruma::api::client::{push, push::set_pusher::v3::PusherAction},
    Inject,
};

use crate::storage::PusherStorage;

#[derive(Inject)]
pub struct SetPusherCommand {
    push_storage: Arc<dyn PusherStorage>,
    bus: Bus,
}

#[async_trait]
impl Command for SetPusherCommand {
    type Input = push::set_pusher::v3::Request;
    type Output = push::set_pusher::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        match input.action {
            PusherAction::Delete(pusher_ids) => {
                self.push_storage
                    .delete_pusher(id.user(), &pusher_ids.pushkey)
                    .await?;
            }
            PusherAction::Post(post_data) => {
                self.push_storage
                    .upsert_pusher(id.user(), &post_data.pusher)
                    .await?;
            }
        }

        self.bus
            .send_message(BusMessage::UpdatePushers(id.user().to_owned()));

        Ok(push::set_pusher::v3::Response::new())
    }
}
