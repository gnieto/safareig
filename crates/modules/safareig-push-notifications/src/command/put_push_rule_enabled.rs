use async_trait::async_trait;
use safareig_core::{
    auth::Identity, command::Command, error::ResourceNotFound, ruma::api::client::push, Inject,
};

use crate::PushRuleRepository;

#[derive(Inject)]
pub struct PutPushRuleEnabledCommand {
    rule_set: PushRuleRepository,
}

#[async_trait]
impl Command for PutPushRuleEnabledCommand {
    type Input = push::set_pushrule_enabled::v3::Request;
    type Output = push::set_pushrule_enabled::v3::Response;
    type Error = safareig_core::ruma::api::client::Error;

    async fn execute(&self, input: Self::Input, id: Identity) -> Result<Self::Output, Self::Error> {
        let mut ruleset = self.rule_set.load_rules(id.user()).await?;
        ruleset
            .set_enabled(input.kind, input.rule_id, input.enabled)
            .map_err(|_| ResourceNotFound)?;
        self.rule_set.store_rules(id.user(), &ruleset).await?;

        Ok(push::set_pushrule_enabled::v3::Response::new())
    }
}
