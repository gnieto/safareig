use safareig_client_config::{storage::AccountDataEvent, AccountDataManager};
use safareig_core::{
    events::Content,
    ruma::{events::push_rules::PushRulesEventContent, push::Ruleset, UserId},
    storage::StorageError,
    Inject,
};

use super::RuleSetStorage;

#[derive(Clone, Inject)]
pub struct AccountDataPushRuleStorage {
    account_data: AccountDataManager,
}

#[async_trait::async_trait]
impl RuleSetStorage for AccountDataPushRuleStorage {
    async fn load_rule_set(&self, user: &UserId) -> Result<Option<Ruleset>, StorageError> {
        let event = self
            .account_data
            .get_account_event_content::<PushRulesEventContent>(user)
            .await
            .map_err(|e| StorageError::Custom(e.to_string()))?
            .map(|content| content.global);

        Ok(event)
    }

    async fn update_rule_set(&self, user: &UserId, rule_set: &Ruleset) -> Result<(), StorageError> {
        let push_rule = PushRulesEventContent {
            global: rule_set.to_owned(),
        };
        let content = Content::from_event_content(&push_rule)
            .map_err(|e| StorageError::Custom(e.to_string()))?;
        let event = AccountDataEvent::Global(content);

        self.account_data
            .update_account_data(user, event)
            .await
            .map_err(|e| StorageError::Custom(e.to_string()))?;

        Ok(())
    }
}
