use std::{collections::HashMap, convert::TryFrom, time::SystemTime};

use safareig_core::{
    pagination::{Pagination, PaginationResponse, Range},
    ruma::{
        api::client::push::{Pusher, PusherIds},
        OwnedRoomId, RoomId, UserId,
    },
    storage::StorageError,
    StreamToken,
};
use safareig_sqlx::{
    decode_from_str, decode_i64_to_u64, DirectionExt, PaginatedIterator, RangeWrapper,
};
use sea_query::{Alias, Expr, Iden, Query, SqliteQueryBuilder};
use sea_query_binder::SqlxBinder;
use sqlx::{sqlite::SqliteRow, Pool, QueryBuilder, Row, Sqlite};

use super::{Notification, NotificationStorage, Notifications as UserNotifications, PusherStorage};

pub struct PushNotificationSqlxStorage {
    pool: Pool<Sqlite>,
}

impl PushNotificationSqlxStorage {
    pub fn new(pool: Pool<Sqlite>) -> Self {
        Self { pool }
    }
}

#[async_trait::async_trait]
impl PusherStorage for PushNotificationSqlxStorage {
    async fn all_pushers(
        &self,
        user_id: &safareig_core::ruma::UserId,
    ) -> Result<Vec<Pusher>, StorageError> {
        let pushers = sqlx::query(
            r#"
            SELECT * FROM pushers
            WHERE user_id = ?1
            "#,
        )
        .bind(user_id.as_str())
        .fetch_all(&self.pool)
        .await?
        .into_iter()
        .map(row_to_pusher)
        .collect::<Result<Vec<Pusher>, _>>()?;

        Ok(pushers)
    }

    async fn upsert_pusher(
        &self,
        user: &safareig_core::ruma::UserId,
        pusher: &Pusher,
    ) -> Result<(), StorageError> {
        let pusher_data = serde_json::to_value(&pusher.kind)?;

        sqlx::query(
            r#"
            INSERT INTO pushers (pushkey, user_id, app_id, app_display_name, device_display_name, profile_tag, lang, pusher_data)
            VALUES(?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8)
            ON CONFLICT DO UPDATE SET app_id = ?3, app_display_name = ?4, device_display_name = ?5, profile_tag = ?6, lang = ?7, pusher_data = ?8
            "#,
        )
        .bind(pusher.ids.pushkey.as_str())
        .bind(user.as_str())
        .bind(pusher.ids.app_id.as_str())
        .bind(pusher.app_display_name.as_str())
        .bind(pusher.device_display_name.as_str())
        .bind(pusher.profile_tag.as_ref())
        .bind(pusher.lang.as_str())
        .bind(pusher_data)
        .execute(&self.pool)
        .await?;

        Ok(())
    }

    async fn delete_pusher(
        &self,
        user_id: &safareig_core::ruma::UserId,
        push_key: &str,
    ) -> Result<(), StorageError> {
        sqlx::query(
            r#"
            DELETE FROM pushers
            WHERE user_id = ?1 AND pushkey = ?2
            "#,
        )
        .bind(user_id.as_str())
        .bind(push_key)
        .execute(&self.pool)
        .await?;

        Ok(())
    }
}

fn row_to_pusher(row: SqliteRow) -> Result<Pusher, StorageError> {
    let pushkey = row.try_get::<String, _>("pushkey")?;
    let app_id = row.try_get::<String, _>("app_id")?;
    let app_display = row.try_get::<String, _>("app_display_name")?;
    let device_display_name = row.try_get::<String, _>("device_display_name")?;
    let profile_tag = row.try_get::<Option<String>, _>("profile_tag")?;
    let lang = row.try_get::<String, _>("lang")?;
    let kind = serde_json::from_value(row.try_get::<serde_json::Value, _>("pusher_data")?)?;

    Ok(Pusher {
        ids: PusherIds { pushkey, app_id },
        kind,
        app_display_name: app_display,
        device_display_name,
        profile_tag,
        lang,
    })
}

pub struct SqlxNotificationStorage {
    pool: Pool<Sqlite>,
}

impl SqlxNotificationStorage {
    pub fn new(pool: Pool<Sqlite>) -> Self {
        Self { pool }
    }
}

#[async_trait::async_trait]
impl NotificationStorage for SqlxNotificationStorage {
    async fn all_notifications(
        &self,
        user: &UserId,
    ) -> Result<HashMap<OwnedRoomId, UserNotifications>, StorageError> {
        let sql = "
        SELECT room_id, SUM(CASE WHEN flags & (1 << 1) = (1 << 1) THEN 1 ELSE 0 END) as highlight, SUM(CASE WHEN flags & (1 << 2) = (1 << 2) THEN 1 ELSE 0 END) as notifications FROM notifications
        WHERE user_id = ?1
        GROUP BY room_id
        ";

        let notifications = sqlx::query(sql)
            .bind(user.as_str())
            .fetch_all(&self.pool)
            .await?
            .into_iter()
            .map::<Result<(OwnedRoomId, UserNotifications), StorageError>, _>(|row| {
                let highlight = row.try_get::<u16, _>("highlight")?;
                let notification = row.try_get::<u16, _>("notifications")?;
                let room_id: OwnedRoomId = decode_from_str(row.try_get::<&str, _>("room_id")?)?;

                let notifications = UserNotifications {
                    highlight,
                    notification,
                };

                Ok((room_id, notifications))
            })
            .collect::<Result<_, StorageError>>()?;

        Ok(notifications)
    }

    async fn count_notifications(
        &self,
        room: &RoomId,
        user: &UserId,
        range: Range<StreamToken>,
    ) -> Result<UserNotifications, StorageError> {
        let wrapper = RangeWrapper(range.map(|t| t.to_string()));
        let (sql, values) = Query::select()
            .expr_as(
                Expr::cust("SUM(CASE WHEN flags & (1 << 1) = (1 << 1) THEN 1 ELSE 0 END)"),
                Alias::new("highlight"),
            )
            .expr_as(
                Expr::cust("SUM(CASE WHEN flags & (1 << 2) = (1  << 2) THEN 1 ELSE 0 END)"),
                Alias::new("notifications"),
            )
            .from(Notifications::Table)
            .and_where(Expr::col(Notifications::UserId).eq(user.as_str()))
            .and_where(Expr::col(Notifications::RoomId).eq(room.as_str()))
            .cond_where(wrapper.condition(Expr::col(Alias::new("stream_token"))))
            .build_sqlx(SqliteQueryBuilder);

        let notifications = sqlx::query_with(&sql, values)
            .fetch_optional(&self.pool)
            .await?
            .map::<Result<_, StorageError>, _>(|row| {
                let highlight = row.try_get::<u16, _>("highlight")?;
                let notification = row.try_get::<u16, _>("notifications")?;

                Ok(UserNotifications {
                    highlight,
                    notification,
                })
            })
            .transpose()?
            .unwrap_or_default();

        Ok(notifications)
    }

    async fn store_notifications(
        &self,
        notifications: &[Notification],
    ) -> Result<(), StorageError> {
        let mut builder = QueryBuilder::new("INSERT INTO notifications (user_id, event_id, room_id, stream_token, push_rule, flags)");

        builder.push_values(
            notifications.iter(),
            |mut b, notification: &Notification| {
                let mut flags = 0;
                if notification.is_highlight {
                    flags |= 1 << 1;
                }

                if notification.is_notification {
                    flags |= 1 << 2;
                }

                b.push_bind(notification.user_id.as_str())
                    .push_bind(notification.event_id.as_str())
                    .push_bind(notification.room_id.as_str())
                    .push_bind(notification.stream_token)
                    .push_bind(notification.push_rule.as_str())
                    .push_bind(flags);
            },
        );

        builder.build().execute(&self.pool).await?;

        Ok(())
    }

    async fn user_notifications(
        &self,
        user_id: &UserId,
        query: Pagination<StreamToken>,
    ) -> Result<PaginationResponse<StreamToken, Notification>, StorageError> {
        let wrapper = RangeWrapper(query.range().map(|t| t.to_string()));
        let (sql, values) = Query::select()
            .expr(Expr::asterisk())
            .from(Notifications::Table)
            .and_where(Expr::col(Notifications::UserId).eq(user_id.as_str()))
            .cond_where(wrapper.condition(Expr::col(Alias::new("stream_token"))))
            .order_by(Notifications::StreamToken, query.direction().order())
            .build_sqlx(SqliteQueryBuilder);

        let notifications = sqlx::query_with(&sql, values).fetch_all(&self.pool).await?;

        let iterator = PaginatedIterator::new(notifications.into_iter(), |row| {
            let notification = Notification::try_from(row)?;

            Ok((notification.stream_token, notification))
        });

        iterator.into_pagination_response()
    }

    async fn store_read_marker(
        &self,
        user: &UserId,
        room: &RoomId,
        stream_token: &StreamToken,
    ) -> Result<(), StorageError> {
        let sql = "
            INSERT INTO read_markers (user_id, room_id, stream_token)
            VALUES (?1, ?2, ?3)
            ON CONFLICT DO UPDATE SET stream_token = ?3
        ";

        sqlx::query(sql)
            .bind(user.as_str())
            .bind(room.as_str())
            .bind(stream_token)
            .execute(&self.pool)
            .await?;

        Ok(())
    }

    async fn get_read_marker(
        &self,
        user: &UserId,
        room: &RoomId,
    ) -> Result<StreamToken, StorageError> {
        let sql = "
            SELECT stream_token FROM  read_markers
            WHERE user_id = ?1 AND room_id =?2
        ";

        let token = sqlx::query(sql)
            .bind(user.as_str())
            .bind(room.as_str())
            .fetch_optional(&self.pool)
            .await?
            .map::<Result<_, StorageError>, _>(|row| {
                let token = decode_i64_to_u64(row.try_get::<i64, _>("stream_token")?)?;

                Ok(StreamToken::from(token))
            })
            .transpose()?
            .unwrap_or_default();

        Ok(token)
    }

    async fn remove_notifications_before(&self, time: SystemTime) -> Result<(), StorageError> {
        let sql = "
            DELETE FROM notifications
            WHERE insertion <= strftime('%s', ?1);
        ";

        let time = sqlx::types::chrono::DateTime::<sqlx::types::chrono::Utc>::from(time);

        let result = sqlx::query(sql).bind(time).execute(&self.pool).await?;

        tracing::info!("Removed {} notifications", result.rows_affected());

        Ok(())
    }
}

impl TryFrom<SqliteRow> for Notification {
    type Error = StorageError;

    fn try_from(row: SqliteRow) -> Result<Self, StorageError> {
        let user_id = decode_from_str(row.try_get::<&str, _>("user_id")?)?;
        let event_id = decode_from_str(row.try_get::<&str, _>("event_id")?)?;
        let room_id = decode_from_str(row.try_get::<&str, _>("room_id")?)?;
        let token = decode_i64_to_u64(row.try_get::<i64, _>("stream_token")?)?;
        let push_rule = row.try_get::<String, _>("push_rule")?;
        let flags = decode_i64_to_u64(row.try_get::<i64, _>("flags")?)?;

        let is_highlight = (flags & (1 << 1)) == (1 << 1);
        let is_notification = (flags & (1 << 2)) == (1 << 2);

        Ok(Notification {
            user_id,
            event_id,
            room_id,
            stream_token: StreamToken::from(token),
            push_rule,
            is_highlight,
            is_notification,
        })
    }
}

#[derive(Iden)]
pub enum Notifications {
    Table,
    UserId,
    RoomId,
    StreamToken,
}
