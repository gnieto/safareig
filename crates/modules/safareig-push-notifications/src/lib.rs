pub mod command;
mod listener;
mod metrics;
mod module;
pub mod storage;
mod sync;
mod worker;

use std::sync::Arc;

pub use module::PushRulesModule;
use safareig_core::{
    ruma::{
        api::client::error::{ErrorBody, ErrorKind},
        exports::http::StatusCode,
        push::Ruleset,
        UserId,
    },
    storage::StorageError,
    Inject,
};
use thiserror::Error;

use crate::storage::RuleSetStorage;

#[derive(Inject, Clone)]
pub struct PushRuleRepository {
    storage: Arc<dyn RuleSetStorage>,
}

impl PushRuleRepository {
    /// Loads or creates the default RuleSet for the target user
    pub async fn load_rules(&self, user: &UserId) -> Result<Ruleset, StorageError> {
        let rules = self.storage.load_rule_set(user).await?;

        match rules {
            Some(r) => Ok(r),
            None => {
                let ruleset = self.initialize_pushrules(user).await?;
                Ok(ruleset)
            }
        }
    }

    pub async fn store_rules(&self, user: &UserId, rule_set: &Ruleset) -> Result<(), StorageError> {
        // TODO: This should probably need to broadcast the new rule set to the notifications worker
        self.storage.update_rule_set(user, rule_set).await?;

        Ok(())
    }

    async fn initialize_pushrules(&self, user: &UserId) -> Result<Ruleset, StorageError> {
        let default_ruleset = Ruleset::server_default(user);
        self.storage.update_rule_set(user, &default_ruleset).await?;

        Ok(default_ruleset)
    }
}

#[derive(Error, Debug)]
pub enum PushRuleError {
    #[error("Missing data to create the resource")]
    MissingData,
    #[error("Networking error: {0}")]
    Networking(#[from] ::reqwest::Error),
    #[error("Invalid stream token")]
    InvalidToken,
    #[error("Unkown")]
    Unknown,
}

impl From<PushRuleError> for ::safareig_core::ruma::api::client::Error {
    fn from(e: PushRuleError) -> Self {
        match e {
            PushRuleError::MissingData => ::safareig_core::ruma::api::client::Error {
                status_code: StatusCode::BAD_REQUEST,
                body: ErrorBody::Standard {
                    kind: ErrorKind::MissingParam,
                    message: e.to_string(),
                },
            },
            PushRuleError::Networking(e) => ::safareig_core::ruma::api::client::Error {
                status_code: StatusCode::INTERNAL_SERVER_ERROR,
                body: ErrorBody::Standard {
                    kind: ErrorKind::Unknown,
                    message: e.to_string(),
                },
            },
            PushRuleError::InvalidToken => ::safareig_core::ruma::api::client::Error {
                status_code: StatusCode::BAD_REQUEST,
                body: ErrorBody::Standard {
                    kind: ErrorKind::InvalidParam,
                    message: e.to_string(),
                },
            },
            PushRuleError::Unknown => ::safareig_core::ruma::api::client::Error {
                status_code: StatusCode::INTERNAL_SERVER_ERROR,
                body: ErrorBody::Standard {
                    kind: ErrorKind::Unknown,
                    message: "unkonwn".to_string(),
                },
            },
        }
    }
}
