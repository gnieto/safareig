use std::sync::Arc;

use safareig::{
    client::room::{loader::RoomLoader, RoomStream},
    storage::{Checkpoint, CheckpointStorage},
};
use safareig_authentication::command::register::UserRegistered;
use safareig_client_config::AccountDataManager;
use safareig_core::{
    bus::Bus,
    command::{Container, InjectServices, Module, Router, ServiceCollectionExtension},
    database::DatabaseConnection,
    ddi::{ServiceCollectionExt, ServiceProvider, ServiceResolverExt},
    listener::Listener,
    storage::StorageError,
    ServerInfo,
};
use safareig_sync_api::SyncExtension;
use safareig_users::storage::UserStorage;

use crate::{
    command::{
        delete_push_rule::DeletePushRuleCommand, get_push_rule::GetPushRuleCommand,
        get_push_rule_actions::GetPushRuleActionsCommand,
        get_push_rule_enabled::GetPushRuleEnabledCommand, get_pushers::GetPushersCommand,
        list_all::ListAll, put_push_rule_actions::PutPushRuleActionsCommand,
        put_push_rule_enabled::PutPushRuleEnabledCommand, set_push_rule::SetPushRuleCommand,
        set_pusher::SetPusherCommand, ListNotificationsCommand,
    },
    listener::RegisterDefaultPushRules,
    storage::{
        client_config::AccountDataPushRuleStorage, NotificationStorage, PusherStorage,
        RuleSetStorage,
    },
    sync::{NotificationCounter, NotificationSync},
    worker::{
        clean_notifications::CleanNotificationsWorker, push_notifications::PushNotificationFactory,
        push_rules::PushRuleWorker, push_supervisor::PushNotificationSupervisor,
    },
    PushRuleRepository,
};

#[derive(Default)]
pub struct PushRulesModule;

impl PushRulesModule {
    fn create_pusher_storage(db: DatabaseConnection) -> Arc<dyn PusherStorage> {
        match db {
            #[cfg(feature = "backend-sqlx")]
            DatabaseConnection::Sqlx(pool) => {
                Arc::new(crate::storage::sqlx::PushNotificationSqlxStorage::new(pool))
            }
            _ => unimplemented!(),
        }
    }

    fn create_notifications_storage(db: DatabaseConnection) -> Arc<dyn NotificationStorage> {
        match db {
            #[cfg(feature = "backend-sqlx")]
            DatabaseConnection::Sqlx(pool) => {
                Arc::new(crate::storage::sqlx::SqlxNotificationStorage::new(pool))
            }
            _ => unimplemented!(),
        }
    }
}

#[async_trait::async_trait]
impl<R: Router> Module<R> for PushRulesModule {
    fn register(&self, container: &mut Container, router: &mut R) {
        container
            .service_collection
            .service_factory(|c: &DatabaseConnection| {
                Ok(PushRulesModule::create_pusher_storage(c.clone()))
            });
        container
            .service_collection
            .service_factory(|c: &DatabaseConnection| {
                Ok(PushRulesModule::create_notifications_storage(c.clone()))
            });
        container
            .service_collection
            .register_arc::<AccountDataPushRuleStorage>();
        container
            .service_collection
            .register::<PushRuleRepository>();
        container
            .service_collection
            .register::<NotificationCounter>();
        container
            .service_collection
            .service_factory(|adm: &AccountDataManager| {
                let rule_set_storage: Arc<dyn RuleSetStorage> =
                    Arc::new(AccountDataPushRuleStorage::new(adm.clone()));

                Ok(rule_set_storage)
            });

        // Pushrules
        container.inject_endpoint::<_, DeletePushRuleCommand, _>(router);
        container.inject_endpoint::<_, GetPushRuleActionsCommand, _>(router);
        container.inject_endpoint::<_, GetPushRuleCommand, _>(router);
        container.inject_endpoint::<_, GetPushRuleEnabledCommand, _>(router);
        container.inject_endpoint::<_, ListAll, _>(router);
        container.inject_endpoint::<_, PutPushRuleActionsCommand, _>(router);
        container.inject_endpoint::<_, PutPushRuleEnabledCommand, _>(router);
        container.inject_endpoint::<_, SetPushRuleCommand, _>(router);

        // Pushers
        container.inject_endpoint::<_, GetPushersCommand, _>(router);
        container.inject_endpoint::<_, SetPusherCommand, _>(router);

        // Notifications
        container.inject_endpoint::<_, ListNotificationsCommand, _>(router);

        container.service_collection.service_factory_var(
            "register_default_push_rules",
            |sp: &ServiceProvider| {
                let listener: Arc<dyn Listener<UserRegistered>> =
                    Arc::new(RegisterDefaultPushRules::inject(sp)?);

                Ok(listener)
            },
        );

        container
            .service_collection
            .service_factory_var("notification", |sp: &ServiceProvider| {
                let sync_extension: Arc<dyn SyncExtension> =
                    Arc::new(NotificationSync::inject(sp)?);

                Ok(sync_extension)
            });
    }

    async fn spawn_workers(
        &self,
        container: &ServiceProvider,
        background: &graceful::BackgroundHandler,
    ) {
        let room_stream = container.get::<RoomStream>().unwrap();
        let room_loader = container.get::<RoomLoader>().unwrap();
        let notifications = container.get::<Arc<dyn NotificationStorage>>().unwrap();
        let push_repository = container.get::<PushRuleRepository>().unwrap();
        let pusher_storage = container.get::<Arc<dyn PusherStorage>>().unwrap();
        let checkpoints = container
            .get::<Arc<dyn CheckpointStorage<Checkpoint>>>()
            .unwrap();
        let server_info = container.get::<ServerInfo>().unwrap();
        let bus = container.get::<Bus>().unwrap();
        let users = container.get::<Arc<dyn UserStorage>>().unwrap();

        // Push-rules
        let worker = PushRuleWorker::new(
            room_stream.clone(),
            room_loader.clone(),
            notifications.clone(),
            push_repository.clone(),
            checkpoints.clone(),
            server_info.clone(),
            users.clone(),
            bus.clone(),
            background.acquire().unwrap(),
        )
        .await;

        let factory = PushNotificationFactory::new(
            notifications.clone(),
            checkpoints.clone(),
            room_stream.clone(),
            pusher_storage.clone(),
            bus.clone(),
        );

        let supervisor = PushNotificationSupervisor::new(
            users.clone(),
            background.clone(),
            bus.clone(),
            factory,
        );
        tokio::spawn(supervisor.run());

        let clean_worker =
            CleanNotificationsWorker::new(background.acquire().unwrap(), notifications.clone());
        tokio::spawn(clean_worker.run());

        tokio::spawn(worker.run());
    }

    async fn run_migrations(&self, provider: &ServiceProvider) -> Result<(), StorageError> {
        #[cfg(feature = "backend-sqlx")]
        {
            let db = provider
                .get::<DatabaseConnection>()
                .map_err(|e| StorageError::Custom(e.to_string()))?;
            safareig_sqlx::run_migration(db, "sqlite", "safareig-push-notifications").await?;
        }

        Ok(())
    }
}
