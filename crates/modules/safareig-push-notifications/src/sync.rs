use std::sync::Arc;

use safareig_core::{
    pagination::Boundary,
    ruma::{
        api::client::sync::sync_events::{self, UnreadNotificationsCount},
        RoomId, UserId,
    },
    storage::StorageError,
    Inject, StreamToken,
};
use safareig_sync_api::{context::Context, error::SyncError, SyncExtension, SyncToken};

use crate::storage::{NotificationStorage, Notifications};

#[derive(Inject)]
pub struct NotificationSync {
    notification_counter: NotificationCounter,
}

#[async_trait::async_trait]
impl SyncExtension for NotificationSync {
    #[tracing::instrument(name = "sync-notifications", skip_all)]
    async fn fill_sync<'a>(
        &self,
        ctx: &'a Context<'a>,
        response: &mut sync_events::v3::Response,
        token: &mut SyncToken,
    ) -> Result<(), SyncError> {
        for (room_id, joined_room) in response.rooms.join.iter_mut() {
            let notifications = self
                .notification_counter
                .notifications_for_room(room_id, ctx.user(), token.event())
                .await?;
            if notifications.highlight == 0 && notifications.notification == 0 {
                continue;
            }

            let mut unread = UnreadNotificationsCount::new();
            unread.highlight_count = Some(notifications.highlight.into());
            unread.notification_count = Some(notifications.notification.into());

            joined_room.unread_notifications = unread;
        }

        Ok(())
    }
}

#[derive(Clone, Inject)]
pub struct NotificationCounter {
    notifications: Arc<dyn NotificationStorage>,
}

impl NotificationCounter {
    pub async fn notifications_for_room(
        &self,
        room_id: &RoomId,
        user_id: &UserId,
        current: StreamToken,
    ) -> Result<Notifications, StorageError> {
        let token_at_marker = self.stream_token_at_read_marker(room_id, user_id).await?;
        let range = (
            Boundary::Exclusive(token_at_marker),
            Boundary::Inclusive(current),
        )
            .into();

        let notifications = self
            .notifications
            .count_notifications(room_id, user_id, range)
            .await?;

        Ok(notifications)
    }

    async fn stream_token_at_read_marker(
        &self,
        room_id: &RoomId,
        user_id: &UserId,
    ) -> Result<StreamToken, StorageError> {
        self.notifications.get_read_marker(user_id, room_id).await
    }
}
