pub(crate) mod client_config;
#[cfg(feature = "backend-sqlx")]
pub(crate) mod sqlx;

use std::{collections::HashMap, time::SystemTime};

use safareig_core::{
    pagination::{Pagination, PaginationResponse, Range},
    ruma::{
        api::client::push::Pusher, push::Ruleset, OwnedEventId, OwnedRoomId, OwnedUserId, RoomId,
        UserId,
    },
    storage::StorageError,
    StreamToken,
};

#[async_trait::async_trait]
pub trait RuleSetStorage: Send + Sync {
    async fn load_rule_set(&self, user: &UserId) -> Result<Option<Ruleset>, StorageError>;
    async fn update_rule_set(&self, user: &UserId, rule_set: &Ruleset) -> Result<(), StorageError>;
}

#[async_trait::async_trait]
pub trait PusherStorage: Send + Sync {
    async fn all_pushers(&self, user: &UserId) -> Result<Vec<Pusher>, StorageError>;
    async fn upsert_pusher(&self, user: &UserId, pusher: &Pusher) -> Result<(), StorageError>;
    async fn delete_pusher(&self, user: &UserId, push_key: &str) -> Result<(), StorageError>;
}

#[derive(Default, Debug)]
pub struct Notifications {
    pub highlight: u16,
    pub notification: u16,
}

#[derive(Debug)]
pub struct Notification {
    pub user_id: OwnedUserId,
    pub event_id: OwnedEventId,
    pub room_id: OwnedRoomId,
    pub stream_token: StreamToken,
    pub push_rule: String,
    pub is_highlight: bool,
    pub is_notification: bool,
}

#[async_trait::async_trait]
pub trait NotificationStorage: Send + Sync {
    async fn all_notifications(
        &self,
        user: &UserId,
    ) -> Result<HashMap<OwnedRoomId, Notifications>, StorageError>;
    async fn count_notifications(
        &self,
        room: &RoomId,
        user: &UserId,
        range: Range<StreamToken>,
    ) -> Result<Notifications, StorageError>;

    async fn store_notifications(&self, notifications: &[Notification])
        -> Result<(), StorageError>;
    async fn user_notifications(
        &self,
        user_id: &UserId,
        query: Pagination<StreamToken>,
    ) -> Result<PaginationResponse<StreamToken, Notification>, StorageError>;
    async fn store_read_marker(
        &self,
        user: &UserId,
        room: &RoomId,
        stream_token: &StreamToken,
    ) -> Result<(), StorageError>;
    async fn get_read_marker(
        &self,
        user: &UserId,
        room: &RoomId,
    ) -> Result<StreamToken, StorageError>;
    async fn remove_notifications_before(&self, time: SystemTime) -> Result<(), StorageError>;
}
