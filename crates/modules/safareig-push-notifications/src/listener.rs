use safareig_authentication::command::register::UserRegistered;
use safareig_core::{error::error_chain, listener::Listener, Inject};

use crate::PushRuleRepository;

#[derive(Inject)]
pub struct RegisterDefaultPushRules {
    push_rules: PushRuleRepository,
}

#[async_trait::async_trait]
impl Listener<UserRegistered> for RegisterDefaultPushRules {
    async fn on_event(&self, event: &UserRegistered) {
        if let Err(e) = self.push_rules.load_rules(&event.user.user_id).await {
            tracing::error!(
                err = error_chain(&e).as_str(),
                "Could not initialize push rules",
            );
        }
    }
}
