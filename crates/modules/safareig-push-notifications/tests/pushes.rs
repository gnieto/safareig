mod command;

use safareig_client_config::AccountDataManager;
use safareig_core::ruma::{
    events::{push_rules::PushRulesEventContent, GlobalAccountDataEventType},
    push::SimplePushRule,
};
use safareig_push_notifications::{PushRuleRepository, PushRulesModule};
use safareig_testing::TestingApp;

#[tokio::test]
async fn emit_event_on_pushrule_init() {
    let db = TestingApp::with_modules(vec![Box::new(PushRulesModule)]).await;
    let id = db.register().await;
    let pushrules = db.service::<PushRuleRepository>();
    let account_data = db.service::<AccountDataManager>();

    let rules = pushrules.load_rules(id.user()).await.unwrap();

    let event = account_data
        .get_account_event(id.user(), &GlobalAccountDataEventType::PushRules)
        .await
        .unwrap();

    assert!(event.is_some());
    let event_rule_set = event
        .unwrap()
        .to_event_content::<PushRulesEventContent>()
        .unwrap()
        .global;
    assert_eq!(
        rules.into_iter().count(),
        event_rule_set.into_iter().count()
    );
}

#[tokio::test]
async fn pushrule_update_emits_event_which_contains_the_new_rule() {
    let db = TestingApp::with_modules(vec![Box::new(PushRulesModule)]).await;
    let id = db.register().await;
    let pushrules = db.service::<PushRuleRepository>();
    let account_data = db.service::<AccountDataManager>();
    let (_, room) = db.public_room().await;

    let mut ruleset = pushrules
        .load_rules(id.user())
        .await
        .expect("User can generate push rules");
    let new_rule = SimplePushRule {
        actions: Vec::new(),
        default: false,
        enabled: true,
        rule_id: room.id().to_owned(),
    };
    ruleset.room.replace(new_rule);
    pushrules
        .store_rules(id.user(), &ruleset)
        .await
        .expect("User can store new rules");

    let event = account_data
        .get_account_event(id.user(), &GlobalAccountDataEventType::PushRules)
        .await
        .unwrap();

    assert!(event.is_some());
    let event_rule_set = event
        .unwrap()
        .to_event_content::<PushRulesEventContent>()
        .unwrap()
        .global;
    let maybe_new_rule = event_rule_set
        .into_iter()
        .find(|r| r.rule_id() == room.id().as_str());
    assert!(maybe_new_rule.is_some());
}
