use safareig_core::{
    command::Command,
    ruma::{
        api::client::push,
        push::{Action, AnyPushRule, NewConditionalPushRule, NewPushRule, PushCondition},
    },
};
use safareig_push_notifications::{
    command::set_push_rule::SetPushRuleCommand, PushRuleRepository, PushRulesModule,
};
use safareig_testing::TestingApp;

#[tokio::test]
async fn it_update_a_rule() {
    let db = TestingApp::with_modules(vec![Box::new(PushRulesModule)]).await;
    let id = db.register().await;
    let cmd = db.command::<SetPushRuleCommand>();
    let repository = db.service::<PushRuleRepository>();

    let new_rule = NewConditionalPushRule {
        rule_id: "test_rule".to_string(),
        conditions: vec![],
        actions: vec![Action::Notify],
    };

    let input = push::set_pushrule::v3::Request {
        scope: push::RuleScope::Global,
        rule: NewPushRule::Override(new_rule),
        after: None,
        before: None,
    };

    let _ = cmd.execute(input, id.clone()).await.unwrap();

    let new_rule = NewConditionalPushRule {
        rule_id: "test_rule".to_string(),
        conditions: vec![PushCondition::ContainsDisplayName],
        actions: vec![Action::Notify],
    };

    let input = push::set_pushrule::v3::Request {
        scope: push::RuleScope::Global,
        rule: NewPushRule::Override(new_rule),
        after: None,
        before: None,
    };

    let _ = cmd.execute(input, id.clone()).await.unwrap();

    let push_rules = repository.load_rules(id.user()).await.unwrap();
    let push: AnyPushRule = push_rules
        .into_iter()
        .find(|r: &AnyPushRule| r.rule_id() == "test_rule")
        .unwrap();

    assert!(push.enabled());
    assert_eq!(1, push.actions().len());
    let conditions = match push {
        AnyPushRule::Override(rule) | AnyPushRule::Underride(rule) => rule.conditions,
        _ => vec![],
    };
    assert_eq!(1, conditions.len());
}

#[tokio::test]
async fn it_adds_a_new_rule() {
    let db = TestingApp::with_modules(vec![Box::new(PushRulesModule)]).await;
    let id = db.register().await;
    let cmd = db.command::<SetPushRuleCommand>();
    let repository = db.service::<PushRuleRepository>();

    let new_rule = NewConditionalPushRule {
        rule_id: "test_rule".to_string(),
        conditions: vec![],
        actions: vec![Action::Notify],
    };

    let input = push::set_pushrule::v3::Request {
        scope: push::RuleScope::Global,
        rule: NewPushRule::Override(new_rule),
        after: None,
        before: None,
    };

    let _ = cmd.execute(input, id.clone()).await.unwrap();
    let push_rules = repository.load_rules(id.user()).await.unwrap();
    let push: AnyPushRule = push_rules
        .into_iter()
        .find(|r: &AnyPushRule| r.rule_id() == "test_rule")
        .unwrap();

    assert!(push.enabled());
    assert_eq!(1, push.actions().len());
    let conditions = match push {
        AnyPushRule::Override(rule) | AnyPushRule::Underride(rule) => rule.conditions,
        _ => vec![],
    };
    assert_eq!(0, conditions.len());
}
