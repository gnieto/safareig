use safareig_core::{
    command::Command,
    ruma::api::client::{
        push,
        push::{PushRule, RuleKind},
    },
};
use safareig_push_notifications::{command::get_push_rule::GetPushRuleCommand, PushRulesModule};
use safareig_testing::TestingApp;

#[tokio::test]
async fn it_returns_override_rules() {
    let db = TestingApp::with_modules(vec![Box::new(PushRulesModule)]).await;
    let user = db.register().await;
    let cmd = db.command::<GetPushRuleCommand>();
    let input = push::get_pushrule::v3::Request {
        scope: push::RuleScope::Global,
        kind: RuleKind::Override,
        rule_id: ".m.rule.suppress_notices".to_string(),
    };

    let response = cmd.execute(input, user).await.unwrap();
    let any_push: PushRule = response.rule;
    assert_eq!(0, any_push.actions.len());
    assert!(any_push.default);
    assert!(any_push.enabled);
    assert_eq!(".m.rule.suppress_notices".to_string(), any_push.rule_id);
}
