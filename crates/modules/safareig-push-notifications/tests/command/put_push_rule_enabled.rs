use safareig_core::{
    command::Command,
    ruma::{
        api::client::{push, push::RuleKind},
        push::AnyPushRule,
    },
};
use safareig_push_notifications::{
    command::put_push_rule_enabled::PutPushRuleEnabledCommand, PushRuleRepository, PushRulesModule,
};
use safareig_testing::TestingApp;

#[tokio::test]
async fn it_update_a_rule() {
    let db = TestingApp::with_modules(vec![Box::new(PushRulesModule)]).await;
    let id = db.register().await;
    let cmd = db.command::<PutPushRuleEnabledCommand>();

    let input = push::set_pushrule_enabled::v3::Request {
        scope: push::RuleScope::Global,
        kind: RuleKind::Override,
        rule_id: ".m.rule.master".to_string(),
        enabled: true,
    };

    let _ = cmd.execute(input, id.clone()).await.unwrap();
    let repository = db.service::<PushRuleRepository>();
    let push_rules = repository.load_rules(id.user()).await.unwrap();

    let enabled = push_rules
        .into_iter()
        .find(|a: &AnyPushRule| a.rule_id() == ".m.rule.master")
        .map(|a: AnyPushRule| a.enabled())
        .unwrap_or_default();

    assert!(enabled);
}
