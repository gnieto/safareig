use safareig_core::{
    command::Command,
    ruma::{
        api::client::{
            push,
            push::{RuleKind, RuleScope},
        },
        push::AnyPushRule,
    },
};
use safareig_push_notifications::{
    command::delete_push_rule::DeletePushRuleCommand, PushRuleRepository, PushRulesModule,
};
use safareig_testing::TestingApp;

#[tokio::test]
async fn it_removes_a_rule() {
    let db = TestingApp::with_modules(vec![Box::new(PushRulesModule)]).await;
    let id = db.register().await;
    let cmd = db.command::<DeletePushRuleCommand>();
    let repo = db.service::<PushRuleRepository>();

    let input = push::delete_pushrule::v3::Request {
        scope: RuleScope::Global,
        kind: RuleKind::Override,
        rule_id: ".m.rule.master".to_string(),
    };

    let _ = cmd.execute(input, id.clone()).await.unwrap();

    let rules = repo.load_rules(id.user()).await.unwrap();
    let master_rule = rules
        .into_iter()
        .find(|a: &AnyPushRule| a.rule_id() == ".m.rule.master");

    assert!(master_rule.is_none());
}
