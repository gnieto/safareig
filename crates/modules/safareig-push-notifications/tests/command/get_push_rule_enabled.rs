use safareig_core::{
    command::Command,
    ruma::api::client::{push, push::RuleKind},
};
use safareig_push_notifications::{
    command::get_push_rule_enabled::GetPushRuleEnabledCommand, PushRulesModule,
};
use safareig_testing::{assert_not_found, TestingApp};

#[tokio::test]
async fn it_returns_an_enabled_rule() {
    let db = TestingApp::with_modules(vec![Box::new(PushRulesModule)]).await;
    let id = db.register().await;
    let cmd = db.command::<GetPushRuleEnabledCommand>();

    let input = push::get_pushrule_enabled::v3::Request {
        scope: push::RuleScope::Global,
        kind: RuleKind::Override,
        rule_id: ".m.rule.invite_for_me".to_string(),
    };

    let response = cmd.execute(input, id.clone()).await.unwrap();

    assert!(response.enabled);
}

#[tokio::test]
async fn it_returns_an_disabled_rule() {
    let db = TestingApp::with_modules(vec![Box::new(PushRulesModule)]).await;
    let id = db.register().await;
    let cmd = db.command::<GetPushRuleEnabledCommand>();

    let input = push::get_pushrule_enabled::v3::Request {
        scope: push::RuleScope::Global,
        kind: RuleKind::Override,
        rule_id: ".m.rule.master".to_string(),
    };

    let response = cmd.execute(input, id.clone()).await.unwrap();

    assert!(!response.enabled);
}

#[tokio::test]
async fn it_returns_resource_not_found_if_not_exists() {
    let db = TestingApp::with_modules(vec![Box::new(PushRulesModule)]).await;
    let id = db.register().await;
    let cmd = db.command::<GetPushRuleEnabledCommand>();

    let input = push::get_pushrule_enabled::v3::Request {
        scope: push::RuleScope::Global,
        kind: RuleKind::Override,
        rule_id: "lalalala".to_string(),
    };

    let response = cmd.execute(input, id.clone()).await;

    assert_not_found(&response);
}
