use safareig_core::{
    command::Command,
    ruma::api::client::{push, push::RuleKind},
};
use safareig_push_notifications::{
    command::get_push_rule_actions::GetPushRuleActionsCommand, PushRulesModule,
};
use safareig_testing::{assert_not_found, TestingApp};

#[tokio::test]
async fn it_returns_actions_for_target_rule() {
    let db = TestingApp::with_modules(vec![Box::new(PushRulesModule)]).await;
    let id = db.register().await;
    let cmd = db.command::<GetPushRuleActionsCommand>();

    let input = push::get_pushrule_actions::v3::Request {
        scope: push::RuleScope::Global,
        kind: RuleKind::Override,
        rule_id: ".m.rule.master".to_string(),
    };

    let response = cmd.execute(input, id.clone()).await.unwrap();
    assert_eq!(0, response.actions.len());
}

#[tokio::test]
async fn it_returns_resource_not_found_if_not_exists() {
    let db = TestingApp::with_modules(vec![Box::new(PushRulesModule)]).await;
    let id = db.register().await;
    let cmd = db.command::<GetPushRuleActionsCommand>();

    let input = push::get_pushrule_actions::v3::Request {
        scope: push::RuleScope::Global,
        kind: RuleKind::Override,
        rule_id: "non-existing-id".to_string(),
    };

    let response = cmd.execute(input, id.clone()).await;

    assert_not_found(&response);
}
