use safareig_core::{
    command::Command,
    ruma::{
        api::client::{push, push::RuleKind},
        push::{Action, AnyPushRule, Tweak},
    },
};
use safareig_push_notifications::{
    command::put_push_rule_actions::PutPushRuleActionsCommand, PushRuleRepository, PushRulesModule,
};
use safareig_testing::TestingApp;

#[tokio::test]
async fn it_changes_push_rule_actions() {
    let db = TestingApp::with_modules(vec![Box::new(PushRulesModule)]).await;

    let id = db.register().await;
    let cmd = db.command::<PutPushRuleActionsCommand>();

    let input = push::set_pushrule_actions::v3::Request {
        scope: push::RuleScope::Global,
        kind: RuleKind::Override,
        rule_id: ".m.rule.master".to_string(),
        actions: vec![
            Action::Notify,
            Action::SetTweak(Tweak::Sound("test".into())),
        ],
    };

    let _ = cmd.execute(input, id.clone()).await.unwrap();
    let repository = db.service::<PushRuleRepository>();
    let push_rules = repository.load_rules(id.user()).await.unwrap();

    let actions = push_rules
        .into_iter()
        .find(|a: &AnyPushRule| a.rule_id() == ".m.rule.master")
        .map(|a: AnyPushRule| a.actions().to_vec())
        .unwrap_or_default();
    assert_eq!(2, actions.len());

    let sound_tweak = actions.iter().find(|a| match a {
        Action::SetTweak(Tweak::Sound(t)) => t == "test",
        _ => false,
    });
    assert!(sound_tweak.is_some());
}
