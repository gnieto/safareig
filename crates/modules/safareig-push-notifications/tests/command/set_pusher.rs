use safareig_core::{
    command::Command,
    ruma::{
        api::client::push::{
            get_pushers,
            set_pusher::{
                self,
                v3::{PusherAction, PusherPostData},
            },
            Pusher, PusherIds, PusherKind,
        },
        push::HttpPusherData,
    },
};
use safareig_push_notifications::{
    command::{get_pushers::GetPushersCommand, set_pusher::SetPusherCommand},
    PushRulesModule,
};
use safareig_testing::TestingApp;

#[tokio::test]
async fn it_can_set_a_pusher() {
    let db = TestingApp::with_modules(vec![Box::new(PushRulesModule)]).await;
    let id = db.register().await;
    let cmd = db.command::<SetPusherCommand>();

    let input = set_pusher::v3::Request {
        action: PusherAction::Post(PusherPostData {
            pusher: Pusher {
                ids: PusherIds {
                    pushkey: "pushkey".to_string(),
                    app_id: "appid".to_string(),
                },
                kind: PusherKind::Http(HttpPusherData::new("http://localhost".to_string())),
                app_display_name: "displayname".to_string(),
                device_display_name: "device_display".to_string(),
                profile_tag: None,
                lang: "ca".to_string(),
            },
            append: false,
        }),
    };

    let _ = cmd.execute(input, id.clone()).await.unwrap();
}

#[tokio::test]
async fn users_can_have_multiple_pushers() {
    let db = TestingApp::with_modules(vec![Box::new(PushRulesModule)]).await;
    let id = db.register().await;
    let cmd = db.command::<SetPusherCommand>();

    // Set up first pusher (pusheyA)
    let input = set_pusher::v3::Request {
        action: PusherAction::Post(PusherPostData {
            pusher: Pusher {
                ids: PusherIds {
                    pushkey: "pushkeyA".to_string(),
                    app_id: "appid".to_string(),
                },
                kind: PusherKind::Http(HttpPusherData::new("http://localhost".to_string())),
                app_display_name: "displayname".to_string(),
                device_display_name: "device_display".to_string(),
                profile_tag: None,
                lang: "ca".to_string(),
            },
            append: false,
        }),
    };
    let _ = cmd.execute(input, id.clone()).await.unwrap();

    // Set up second pusher (pusheyB)
    let input = set_pusher::v3::Request {
        action: PusherAction::Post(PusherPostData {
            pusher: Pusher {
                ids: PusherIds {
                    pushkey: "pushkeyB".to_string(),
                    app_id: "appid".to_string(),
                },
                kind: PusherKind::Http(HttpPusherData::new("http://localhost".to_string())),
                app_display_name: "displayname".to_string(),
                device_display_name: "device_display".to_string(),
                profile_tag: None,
                lang: "ca".to_string(),
            },
            append: false,
        }),
    };
    let _ = cmd.execute(input, id.clone()).await.unwrap();

    let cmd = db.command::<GetPushersCommand>();
    let input = get_pushers::v3::Request {};
    let pushers = cmd.execute(input, id.clone()).await.unwrap();

    assert_eq!(2, pushers.pushers.len());
}

#[tokio::test]
async fn users_can_update_their_pusher_data() {
    let db = TestingApp::with_modules(vec![Box::new(PushRulesModule)]).await;
    let id = db.register().await;
    let cmd = db.command::<SetPusherCommand>();
    let pusher_cmd = db.command::<GetPushersCommand>();

    // Set up first pusher (pusheyA)
    let input = set_pusher::v3::Request {
        action: PusherAction::Post(PusherPostData {
            pusher: Pusher {
                ids: PusherIds {
                    pushkey: "pushkeyA".to_string(),
                    app_id: "appid".to_string(),
                },
                kind: PusherKind::Http(HttpPusherData::new("http://localhost".to_string())),
                app_display_name: "displayname".to_string(),
                device_display_name: "device_display".to_string(),
                profile_tag: None,
                lang: "ca".to_string(),
            },
            append: false,
        }),
    };
    let _ = cmd.execute(input, id.clone()).await.unwrap();

    let input = get_pushers::v3::Request {};
    let pushers = pusher_cmd.execute(input, id.clone()).await.unwrap();
    assert_eq!(1, pushers.pushers.len());
    assert_eq!("displayname", pushers.pushers[0].app_display_name);

    // Update pusher
    let input = set_pusher::v3::Request {
        action: PusherAction::Post(PusherPostData {
            pusher: Pusher {
                ids: PusherIds {
                    pushkey: "pushkeyA".to_string(),
                    app_id: "appid".to_string(),
                },
                kind: PusherKind::Http(HttpPusherData::new("http://localhost".to_string())),
                app_display_name: "updated".to_string(),
                device_display_name: "device_display".to_string(),
                profile_tag: None,
                lang: "ca".to_string(),
            },
            append: false,
        }),
    };
    let _ = cmd.execute(input, id.clone()).await.unwrap();

    let input = get_pushers::v3::Request {};
    let pushers = pusher_cmd.execute(input, id.clone()).await.unwrap();
    assert_eq!(1, pushers.pushers.len());
    assert_eq!("updated", pushers.pushers[0].app_display_name);
}
