use std::sync::Arc;

use safareig_axum::{adapters::AccessTokenCache, RateLimiter};
use safareig_core::{
    command::{Container, Module, Router, ServiceCollectionExtension},
    ddi::{ServiceCollectionExt, ServiceProvider},
};
pub struct SytestModule {}

impl Default for SytestModule {
    fn default() -> Self {
        Self {}
    }
}

#[async_trait::async_trait]
impl<R: Router> Module<R> for SytestModule {
    fn register(&self, container: &mut Container, router: &mut R) {
        container
            .service_collection
            .service_factory(|sp: &ServiceProvider| {
                let rate_limiter: Arc<dyn RateLimiter + Send + Sync> = Arc::new(NoopRateLimiter {});

                Ok(rate_limiter)
            });

        container
            .service_collection
            .service_factory(|sp: &ServiceProvider| {
                let cache: Arc<dyn AccessTokenCache> =
                    Arc::new(safareig_axum::adapters::NoopCache::default());

                Ok(cache)
            });
    }
}

struct NoopRateLimiter {}

impl RateLimiter for NoopRateLimiter {
    fn is_route_limited(&self, route: &'static str) -> safareig_axum::RateLimited {
        tracing::warn!("Noop is route limited");

        safareig_axum::RateLimited::No
    }
}
