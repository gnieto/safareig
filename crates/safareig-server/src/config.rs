use std::{env, path::PathBuf, sync::Arc};

use safareig_core::config::HomeserverConfig;

pub fn load_config() -> Arc<HomeserverConfig> {
    let path = env::var("SAFAREIG_CONFIG").unwrap_or_else(|_| "Settings.toml".to_string());
    tracing::debug!("Using config file: {}", path);
    let cfg_file = PathBuf::from(&path);

    Arc::new(HomeserverConfig::from_file(cfg_file.as_ref()))
}
