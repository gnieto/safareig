use axum::Router;
use safareig_app::default_app;
use safareig_axum::{adapters::AxumRouter, server::ServerBuilder, AxumModule};
use safareig_core::app::AppOptions;

pub mod config;

#[cfg(feature = "sytest")]
mod sytest;

#[tokio::main]
async fn main() {
    let config = crate::config::load_config();
    let background = graceful::Background::default();

    let router = AxumRouter::new(Router::new());
    let options = AppOptions {
        spawn_worker: Some(background.handler()),
        run_migrations: true,
    };
    let mut safareig_app = default_app(router, options, config.clone());
    safareig_app = safareig_app.with_module(Box::<AxumModule>::default());

    #[cfg(feature = "sytest")]
    {
        safareig_app = safareig_app.with_module(Box::<crate::sytest::SytestModule>::default());
    }

    let server = ServerBuilder::default()
        .with_cors(true)
        .with_tracing(true)
        .with_prometheus(true)
        .with_media_endpoints(true)
        .with_app(safareig_app)
        .build()
        .unwrap();

    tracing::info!("Starting service");
    server.run(background, &config).await.unwrap();
}
