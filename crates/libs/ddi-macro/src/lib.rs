use darling::FromDeriveInput;
use proc_macro::{self, TokenStream};
use proc_macro2::Ident;
use quote::quote;
use syn::{parse_macro_input, Data, DeriveInput, Type};

#[derive(FromDeriveInput, Default)]
#[darling(default, attributes(inject))]
struct Opts {
    supress_constructor: bool,
}

#[proc_macro_derive(Inject, attributes(inject))]
pub fn derive(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input);
    let opts = Opts::from_derive_input(&input).expect("Wrong options");
    let DeriveInput { ident, .. } = input;
    let fields = match input.data {
        Data::Struct(s) => s
            .fields
            .into_iter()
            .map(|field| (field.ident.unwrap(), field.ty))
            .collect::<Vec<_>>(),
        Data::Enum(_e) => unimplemented!(),
        Data::Union(_u) => unimplemented!(),
    };

    let (field_name, field_type): (Vec<Ident>, Vec<Type>) = fields.into_iter().unzip();

    let inject = quote! {
        fn inject(sp: &::safareig_core::ddi::ServiceProvider) -> Result<Self, ::safareig_core::ddi::DDIError> {
            use safareig_core::ddi::ServiceResolverExt;

            Ok(Self{
                    #(#field_name: sp.get::<#field_type>()?.clone(),)*
            })
        }
    };

    let constructor = if opts.supress_constructor {
        quote!()
    } else {
        quote!(
            #[allow(dead_code)]
            pub fn new(#(#field_name: #field_type,)*) -> Self {
                Self { #(#field_name,)* }
            }
        )
    };

    let output = quote! {
        impl ::safareig_core::command::InjectServices for #ident {
            #inject
        }

        impl #ident {
            #constructor
        }
    };
    output.into()
}
