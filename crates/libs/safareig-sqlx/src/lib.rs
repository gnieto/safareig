use std::{collections::BTreeMap, env, num::NonZeroU128, path::PathBuf, str::FromStr};

use safareig_core::{
    database::DatabaseConnection,
    pagination::{Direction, PaginationResponse, Range, Token},
    storage::{Result, StorageError},
    StreamToken,
};
use sea_query::{Cond, Expr, SimpleExpr};
use serde::de::DeserializeOwned;
use sqlx::{migrate::Migrator, sqlite::SqliteRow, Column, Row};

pub fn ulid_to_vec(ulid: rusty_ulid::Ulid) -> Vec<u8> {
    let bytes: [u8; 16] = ulid.into();

    bytes.to_vec()
}

pub fn u128_to_vec(input: NonZeroU128) -> Vec<u8> {
    let bytes: [u8; 16] = input.get().to_be_bytes();

    bytes.to_vec()
}

pub fn decode_ulid(input: &[u8]) -> Result<rusty_ulid::Ulid> {
    rusty_ulid::Ulid::try_from(input).map_err(|_| StorageError::CorruptedData)
}

pub fn decode_stream_token(token: i64) -> Result<StreamToken> {
    let unsigned = u64::try_from(token).map_err(|_| StorageError::CorruptedData)?;

    unsigned.try_into().map_err(|_| StorageError::CorruptedData)
}

pub fn decode_i64_to_u64(input: i64) -> Result<u64> {
    u64::try_from(input).map_err(|_| StorageError::CorruptedData)
}

pub fn decode_from_str<T: FromStr>(input: &str) -> Result<T> {
    T::from_str(input).map_err(|_| StorageError::CorruptedData)
}

pub fn decode_from<'a, T: From<&'a str>>(input: &'a str) -> T {
    T::from(input)
}

pub fn decode_from_deserialize<T: DeserializeOwned>(input: &str) -> Result<T> {
    serde_json::from_str(input).map_err(|_| StorageError::CorruptedData)
}

pub struct PaginatedIterator<T: Token, O, R, IT: Iterator<Item = R>, F: Fn(R) -> Result<(T, O)>> {
    mapper: F,
    inner: IT,
    last_token: Option<T>,
    first_token: Option<T>,
    errored: Option<StorageError>,
}

impl<T: Token, O, R, IT: Iterator<Item = R>, F: Fn(R) -> Result<(T, O)>>
    PaginatedIterator<T, O, R, IT, F>
{
    pub fn new(iterator: IT, mapper: F) -> Self {
        Self {
            mapper,
            inner: iterator,
            last_token: None,
            first_token: None,
            errored: None,
        }
    }

    pub fn has_errored(&self) -> bool {
        self.errored.is_some()
    }

    pub fn errored(self) -> Option<StorageError> {
        self.errored
    }

    pub fn last_token(&self) -> Option<T> {
        self.last_token.clone()
    }

    pub fn first_token(&self) -> Option<T> {
        self.first_token.clone()
    }

    pub fn into_pagination_response(mut self) -> Result<PaginationResponse<T, O>> {
        let mut records = Vec::new();

        for record in self.by_ref() {
            records.push(record);
        }

        if self.has_errored() {
            return Err(self.errored.unwrap());
        }

        let first_token = self.first_token();
        let last_token = self.last_token();

        Ok(PaginationResponse::new(records, first_token, last_token))
    }
}

impl<T: Token, O, R, IT: Iterator<Item = R>, F: Fn(R) -> Result<(T, O)>> Iterator
    for PaginatedIterator<T, O, R, IT, F>
{
    type Item = O;

    fn next(&mut self) -> Option<Self::Item> {
        if self.errored.is_some() {
            return None;
        }

        let next = self.inner.next()?;

        match (self.mapper)(next) {
            Err(e) => self.errored = Some(e),
            Ok((token, item)) => {
                if self.first_token.is_none() {
                    self.first_token = Some(token.clone());
                }

                self.last_token = Some(token);

                return Some(item);
            }
        }

        None
    }
}

pub fn debug_row(row: &SqliteRow) {
    let mut json_row = BTreeMap::new();
    let columns = row.columns();

    for c in columns {
        let name = c.name();

        if let Ok(s) = row.try_get::<&str, _>(name) {
            json_row.insert(name.to_string(), serde_json::Value::String(s.to_string()));
            continue;
        }

        if let Ok(s) = row.try_get::<i64, _>(name) {
            json_row.insert(name.to_string(), serde_json::Value::Number(s.into()));
            continue;
        }

        if let Ok(s) = row.try_get::<serde_json::Value, _>(name) {
            json_row.insert(name.to_string(), s);
            continue;
        }

        if let Ok(s) = row.try_get::<Vec<u8>, _>(name) {
            let array = s
                .into_iter()
                .map(|e| serde_json::Value::Number(e.into()))
                .collect();
            json_row.insert(name.to_string(), serde_json::Value::Array(array));
            continue;
        }
    }

    tracing::debug!("SQL row: {:?}", json_row);
}

pub async fn run_migration(db: &DatabaseConnection, engine: &str, directory: &str) -> Result<()> {
    let path = locate_root()?;
    let folder = path.join("sqlx").join(engine).join(directory);

    let pool = db.clone().sqlx().unwrap();
    let mut m = Migrator::new(folder)
        .await
        .map_err(|e| StorageError::Custom(e.to_string()))?;
    m.set_ignore_missing(true);
    m.run(&pool)
        .await
        .map_err(|e| StorageError::Custom(e.to_string()))?;

    Ok(())
}

fn locate_root() -> Result<PathBuf> {
    let mut path = env::current_dir().map_err(|e| StorageError::Custom(e.to_string()))?;

    for _i in 0..4 {
        if path.join("sqlx").is_dir() {
            return Ok(path);
        }

        path = path
            .parent()
            .ok_or_else(|| StorageError::Custom("could not get parent flder".to_string()))?
            .to_path_buf();
    }

    Err(StorageError::Custom(
        "could not locate sqlx root".to_string(),
    ))
}

pub trait DirectionExt {
    fn order(&self) -> sea_query::Order;
}

impl DirectionExt for Direction {
    fn order(&self) -> sea_query::Order {
        match self {
            Direction::Backward => sea_query::Order::Desc,
            Direction::Forward => sea_query::Order::Asc,
        }
    }
}

pub struct RangeWrapper<T>(pub Range<T>);

impl<T: Clone + Into<SimpleExpr>> RangeWrapper<T> {
    pub fn condition(self, column_reference: Expr) -> Cond {
        let mut cond = Cond::all();

        match self.0.from().clone() {
            safareig_core::pagination::Boundary::Exclusive(token) => {
                cond = cond.add(column_reference.clone().gt(token.into()));
            }
            safareig_core::pagination::Boundary::Inclusive(token) => {
                cond = cond.add(column_reference.clone().gte(token.into()));
            }
            safareig_core::pagination::Boundary::Unlimited => (),
        }

        match self.0.to().clone() {
            safareig_core::pagination::Boundary::Exclusive(token) => {
                cond = cond.add(column_reference.lt(token.into()));
            }
            safareig_core::pagination::Boundary::Inclusive(token) => {
                cond = cond.add(column_reference.lte(token.into()));
            }
            safareig_core::pagination::Boundary::Unlimited => (),
        }

        cond
    }
}
