use std::sync::Arc;

use safareig::{
    federation::component::FederationComponent,
    services::{ClientComponent, ServerConfigModule},
};
use safareig_aggregations::module::AggregationsModule;
use safareig_appservices::module::AppserviceModule;
use safareig_authentication::module::AuthenticationModule;
use safareig_client_config::ClientConfigModule;
use safareig_core::{
    app::{App, AppOptions},
    command::Router,
    component::CoreModule,
    config::HomeserverConfig,
};
use safareig_devices::DeviceModule;
use safareig_directory::module::RoomDirectoryModule;
use safareig_e2ee::module::E2eeModule;
use safareig_ephemeral::EphemeralModule;
use safareig_federation::ServerKeysModule;
use safareig_ignored_users::IgnoredUsersModule;
use safareig_media::module::MediaModule;
use safareig_presence::module::PresenceModule;
use safareig_push_notifications::PushRulesModule;
use safareig_read_markers::module::ReadMarkersModule;
use safareig_receipts::module::ReceiptsModule;
use safareig_reporting_content::ReportingContentModule;
use safareig_room_upgrades::module::RoomUpgradesModule;
use safareig_rooms::alias::module::AliasesModule;
use safareig_server_admin::module::ServerAdminModule;
use safareig_server_notices::ServerNoticesModule;
use safareig_server_side_search::module::ServerSideSearchModule;
use safareig_spaces::module::SpacesModule;
use safareig_synapse_admin::module::SynapseAdminModule;
use safareig_sync::module::SyncModule;
use safareig_to_device::module::ToDeviceModule;
use safareig_typing::module::TypingModule;
use safareig_uiaa::module::UiaaModule;
use safareig_users::module::UsersModule;

pub fn default_app<R: Router>(
    router: R,
    options: AppOptions,
    config: Arc<HomeserverConfig>,
) -> App<R> {
    let mut safareig_app = App::new(router, options, config.clone())
        .with_module(Box::new(CoreModule::new(config.clone())))
        .with_module(Box::<ServerConfigModule>::default())
        .with_module(Box::<EphemeralModule>::default())
        .with_module(Box::<UiaaModule>::default())
        .with_module(Box::<DeviceModule>::default())
        .with_module(Box::<TypingModule>::default())
        .with_module(Box::<E2eeModule>::default())
        .with_module(Box::<ClientComponent>::default())
        .with_module(Box::<FederationComponent>::default())
        .with_module(Box::<ServerKeysModule>::default())
        .with_module(Box::<MediaModule>::default())
        .with_module(Box::<RoomDirectoryModule>::default())
        .with_module(Box::<ReceiptsModule>::default())
        .with_module(Box::<ReadMarkersModule>::default())
        .with_module(Box::<AliasesModule>::default())
        .with_module(Box::<SyncModule>::default())
        .with_module(Box::<PushRulesModule>::default())
        .with_module(Box::<SpacesModule>::default())
        .with_module(Box::<ServerNoticesModule>::default())
        .with_module(Box::<ReportingContentModule>::default())
        .with_module(Box::<ToDeviceModule>::default())
        .with_module(Box::<PresenceModule>::default())
        .with_module(Box::<ServerAdminModule>::default())
        .with_module(Box::<AuthenticationModule>::default())
        .with_module(Box::<UsersModule>::default())
        .with_module(Box::<ServerSideSearchModule>::default())
        .with_module(Box::<RoomUpgradesModule>::default())
        .with_module(Box::<AppserviceModule>::default())
        .with_module(Box::<AggregationsModule>::default())
        .with_module(Box::<ClientConfigModule>::default())
        .with_module(Box::<IgnoredUsersModule>::default());

    #[cfg(feature = "msc2965")]
    {
        safareig_app =
            safareig_app.with_module(Box::<safareig_oidc::module::OidcModule>::default());
    }

    if config.admin.enabled {
        safareig_app = safareig_app.with_module(Box::<SynapseAdminModule>::default())
    }

    safareig_app
}
