use std::sync::Arc;

use axum::{
    extract::{MatchedPath, Request},
    middleware::Next,
    response::{IntoResponse, Response},
};
use axum_ruma::{ClientIdentity, Identity, RumaResponse};
use http::StatusCode;
use safareig_core::{
    config::HomeserverConfig,
    ruma::api::client::error::{ErrorBody, ErrorKind},
};

use crate::AppState;

pub async fn synapse_auth(request: Request, next: Next) -> Response {
    // do something with `request`...
    let route = request
        .extensions()
        .get::<MatchedPath>()
        .map(|path| path.as_str().to_owned());

    if let Some(route) = route {
        if !route.starts_with("/_synapse/admin") {
            return next.run(request).await;
        }
    }

    if let Some(Identity::Client(ClientIdentity::Device(user, _))) =
        request.extensions().get::<Identity>()
    {
        let is_admin = request
            .extensions()
            .get::<Arc<AppState>>()
            .map(|state| {
                let config = state.container.service::<Arc<HomeserverConfig>>();

                config.is_admin(user)
            })
            .unwrap_or(false);

        if is_admin {
            return next.run(request).await;
        }

        tracing::warn!(
            ?user,
            "Found non-admin user trying to execute admin endpoints"
        );
    }

    let matrix_error = safareig_core::ruma::api::client::error::Error {
        status_code: StatusCode::UNAUTHORIZED,
        body: ErrorBody::Standard {
            kind: ErrorKind::Unauthorized,
            message: "user does not have permissions".to_string(),
        },
    };

    RumaResponse(matrix_error).into_response()
}
