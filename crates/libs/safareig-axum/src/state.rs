use std::sync::Arc;

use safareig_core::command::FrozenContainer;

use crate::RateLimiter;

pub struct AppState {
    pub container: FrozenContainer,
    pub rate_limiter: Option<Arc<dyn RateLimiter + Send + Sync>>,
}

impl AppState {
    pub fn new(container: FrozenContainer) -> AppState {
        let rate_limiter = container.service_opt::<Arc<dyn RateLimiter + Send + Sync>>();

        Self {
            container,
            rate_limiter,
        }
    }
}
