mod auth;
mod command;

use std::marker::PhantomData;

pub use auth::*;
pub use command::*;
use safareig_core::{
    auth::{FederationAuth, Identity},
    command::Router,
};

use self::command::register_generic;

pub struct AxumRouter(Option<axum::Router>);

impl AxumRouter {
    pub fn new(router: axum::Router) -> Self {
        Self(Some(router))
    }

    pub fn take(self) -> axum::Router {
        self.0.unwrap()
    }
}

impl Router for AxumRouter {
    fn add_route<Req, C>(&mut self, _: PhantomData<C>)
    where
        Req: ::safareig_core::ruma::api::IncomingRequest + Send + Sync + 'static,
        C: 'static
            + Send
            + Sync
            + safareig_core::command::Command<
                Input = Req,
                Output = Req::OutgoingResponse,
                Error = Req::EndpointError,
            >,
    {
        let router = self.0.take().unwrap();
        let new_router = register_generic::<C, Req>(router);

        self.0 = Some(new_router);
    }
}

#[derive(Clone)]
pub struct AxumIdentity(pub axum_ruma::Identity);

impl From<AxumIdentity> for safareig_core::auth::Identity {
    fn from(id: AxumIdentity) -> Self {
        match id.0 {
            axum_ruma::Identity::Client(client) => match client {
                axum_ruma::ClientIdentity::Device(user, device) => Identity::Device(device, user),
                axum_ruma::ClientIdentity::AppService(app, user) => Identity::AppService(user, app),
            },
            axum_ruma::Identity::Federation(auth) => Identity::Federation(FederationAuth {
                server: auth.origin().to_owned(),
                key: auth.key().to_owned(),
                signature: auth.signature().to_owned(),
            }),
            axum_ruma::Identity::Unauthenticated => Identity::None,
        }
    }
}
