use std::{
    num::NonZeroU32,
    time::{Duration, Instant},
};

use governor::RateLimiter as GovernorRateLimit;

type GovernorRateLimitType = governor::RateLimiter<
    &'static str,
    governor::state::keyed::DefaultKeyedStateStore<&'static str>,
    governor::clock::MonotonicClock,
>;

pub trait RateLimiter {
    fn is_route_limited(&self, route: &'static str) -> RateLimited;
}

pub enum RateLimited {
    No,
    Yes(Duration),
}

pub struct GovernorRateLimiter {
    rate_limit: GovernorRateLimitType,
}

impl Default for GovernorRateLimiter {
    fn default() -> Self {
        let quota = governor::Quota::per_second(NonZeroU32::new(30u32).unwrap());

        Self {
            rate_limit: GovernorRateLimit::keyed(quota),
        }
    }
}

impl RateLimiter for GovernorRateLimiter {
    fn is_route_limited(&self, route: &'static str) -> RateLimited {
        match self.rate_limit.check_key(&route) {
            Ok(_) => RateLimited::No,
            Err(until) => {
                let wait_duration = until.wait_time_from(Instant::now());

                RateLimited::Yes(wait_duration)
            }
        }
    }
}
