use std::{net::ToSocketAddrs, sync::Arc, time::Duration};

use axum::{middleware, response::IntoResponse, routing::get, Extension};
use axum_ruma::{layer::MatrixAuthorizationLayer, RumaResponse};
use axum_tracing_opentelemetry::middleware::OtelAxumLayer;
use bytes::Bytes;
use graceful::Background;
use http::StatusCode;
use opentelemetry::{global, metrics::MeterProvider};
use opentelemetry_sdk::{metrics, trace, Resource};
use prometheus::{Encoder, TextEncoder};
use safareig_core::{
    app::App,
    bus::Bus,
    config::HomeserverConfig,
    ruma::api::client::error::{ErrorBody, ErrorKind},
};
use tokio::net::TcpListener;
use tower_http::cors::{Any, CorsLayer};
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt, EnvFilter, Registry};

use crate::{
    adapters::{AxumRouter, SafareigClientAuthorizer, SafareigKeyProvider},
    instrumentation::{MetricsConfigOptions, OpentelemetryMetricsLayer},
    media::media_router,
    AppState,
};

#[derive(Default)]
pub struct ServerBuilder {
    tracing: bool,
    prometheus: bool,
    app: Option<App<AxumRouter>>,
    cors: bool,
    media_endpoints: bool,
}

impl ServerBuilder {
    pub fn with_tracing(mut self, tracing: bool) -> Self {
        self.tracing = tracing;
        self
    }

    pub fn with_prometheus(mut self, prometheus: bool) -> Self {
        self.prometheus = prometheus;
        self
    }

    pub fn with_app(mut self, app: App<AxumRouter>) -> Self {
        self.app = Some(app);
        self
    }

    pub fn with_cors(mut self, cors: bool) -> Self {
        self.cors = cors;
        self
    }

    pub fn with_media_endpoints(mut self, media_endpoints: bool) -> Self {
        self.media_endpoints = media_endpoints;
        self
    }

    pub fn build(self) -> anyhow::Result<Server> {
        if self.app.is_none() {
            return Err(anyhow::anyhow!("App required"));
        }

        Ok(Server {
            tracing: self.tracing,
            prometheus: self.prometheus,
            app: self.app.unwrap(),
            cors: self.cors,
            media_endpoints: self.media_endpoints,
        })
    }
}

pub struct Server {
    tracing: bool,
    prometheus: bool,
    app: App<AxumRouter>,
    cors: bool,
    media_endpoints: bool,
}

impl Server {
    pub async fn router(self) -> anyhow::Result<(Bus, Arc<HomeserverConfig>, axum::Router)> {
        let (container, router) = self.app.take().await;
        let bus = container.service::<Bus>();
        let config = container.service::<Arc<HomeserverConfig>>();

        let mut app = router.take();

        if self.media_endpoints {
            app = app.merge(media_router());
        }

        app = app.fallback(fallback);

        let authorization = MatrixAuthorizationLayer::new(
            container.service::<SafareigClientAuthorizer>(),
            container.service::<SafareigKeyProvider>(),
        );

        let state = AppState::new(container);
        app = app.layer(middleware::from_fn(method_not_allowed));

        if self.cors {
            let cors_layer = CorsLayer::new()
                .allow_origin(Any)
                .allow_methods(Any)
                .allow_headers(Any)
                .max_age(Duration::from_secs(3600));
            app = app.layer(cors_layer);
        }

        app = app.layer(authorization);

        if self.tracing {
            app = app
                .layer(tower_http::trace::TraceLayer::new_for_http())
                .layer(OtelAxumLayer::default());
        }

        if self.prometheus {
            // create a new prometheus registry
            let registry = prometheus::Registry::new();

            // configure OpenTelemetry to use this registry
            let exporter = opentelemetry_prometheus::exporter()
                .with_registry(registry.clone())
                .build()
                .unwrap();

            // set up a meter meter to create instruments
            let meter_provider = opentelemetry_sdk::metrics::SdkMeterProvider::builder()
                .with_reader(exporter)
                .with_view(
                    metrics::new_view(
                        metrics::Instrument::new().name("*http_request_duration*"),
                        metrics::Stream::new().aggregation(
                            metrics::Aggregation::ExplicitBucketHistogram {
                                boundaries: vec![
                                    0.0, 5.0, 10.0, 25.0, 50.0, 75.0, 100.0, 250.0, 500.0, 1000.0,
                                ],
                                record_min_max: true,
                            },
                        ),
                    )
                    .unwrap(),
                )
                .build();
            opentelemetry::global::set_meter_provider(meter_provider.clone());

            let prometheus_config =
                MetricsConfigOptions::default().with_meter(meter_provider.meter("safareig"));
            app = app
                .layer(OpentelemetryMetricsLayer::new(prometheus_config.build()))
                .route(
                    "/_safareig/metrics",
                    get(|| async move {
                        let encoder = TextEncoder::new();
                        let metric_families = registry.gather();
                        let mut result = Vec::new();
                        let _ = encoder.encode(metric_families.as_slice(), &mut result);
                        let bytes = Bytes::from(result);

                        let full_body = http_body_util::Full::new(bytes);
                        axum::response::Response::new(full_body)
                    }),
                );
        }

        app = app.layer(Extension(Arc::new(state)));

        Ok((bus, config, app))
    }

    pub async fn run(
        self,
        background: Background,
        config: &HomeserverConfig,
    ) -> anyhow::Result<()> {
        let has_tracing = self.tracing;
        setup_tracing(config);
        let (bus, config, router) = self.router().await?;

        #[cfg(feature = "tls-rustls")]
        {
            tracing::debug!("TLS - Rusttls enabled");
            use axum_server::{tls_rustls::RustlsConfig, Handle};
            tracing::debug!("TLS - Config ssl");

            if config.ssl.enabled {
                let handle = Handle::new();

                let addr = config.ssl_bind.to_socket_addrs().unwrap().next().unwrap();
                tracing::debug!("TLS - Using rust TLS. Addr: {:?}", addr);
                let config = RustlsConfig::from_pem_file(
                    config
                        .ssl
                        .cert_path
                        .as_ref()
                        .expect("cert path is configured"),
                    config
                        .ssl
                        .key_path
                        .as_ref()
                        .expect("key path is configured"),
                )
                .await
                .expect("ssl material is properly configured");

                tokio::spawn(graceful_shutdown(handle.clone(), bus));
                axum_server::bind_rustls(addr, config)
                    .handle(handle)
                    .serve(router.into_make_service())
                    .await
                    .unwrap()
            } else {
                let addr = config.bind.to_socket_addrs().unwrap().next().unwrap();
                let listener = TcpListener::bind(addr).await.unwrap();

                axum::serve(listener, router)
                    .with_graceful_shutdown(shutdown_signal(bus))
                    .await
                    .unwrap();
            }
        }
        #[cfg(not(feature = "tls-rustls"))]
        {
            if config.ssl.enabled {
                panic!("Safareig version compiled without tls support. Please compile with tls-rustls feature before using this feature")
            }

            let addr = config.bind.to_socket_addrs().unwrap().next().unwrap();
            let listener = TcpListener::bind(addr).await.unwrap();

            axum::serve(listener, router)
                .with_graceful_shutdown(shutdown_signal(bus))
                .await
                .unwrap();
        }

        if has_tracing {
            global::shutdown_tracer_provider();
        }

        // Once Axum has finished and we do not have more incoming requests, stop all
        // background jobs
        background.shutdown(Duration::from_secs(30)).await;

        Ok(())
    }
}

async fn fallback() -> RumaResponse<safareig_core::ruma::api::client::Error> {
    RumaResponse(safareig_core::ruma::api::client::Error {
        status_code: StatusCode::NOT_FOUND,
        body: ErrorBody::Standard {
            kind: ErrorKind::Unrecognized,
            message: "Unrecognized request".to_string(),
        },
    })
}

#[cfg(unix)]
pub async fn shutdown_signal(bus: Bus) {
    use std::io;

    use safareig_core::bus::BusMessage;
    use tokio::signal::unix::SignalKind;

    async fn terminate() -> io::Result<()> {
        tokio::signal::unix::signal(SignalKind::terminate())?
            .recv()
            .await;
        Ok(())
    }

    tokio::select! {
        _ = terminate() => {},
        _ = tokio::signal::ctrl_c() => {},
    };

    bus.send_message(BusMessage::Shutdown);
}

#[cfg(windows)]
pub async fn shutdown_signal(bus: Bus) {
    tokio::signal::ctrl_c()
        .await
        .expect("faild to install CTRL+C handler");

    bus.send_message(BusMessage::Shutdown);
}

#[cfg(feature = "tls-rustls")]
async fn graceful_shutdown(handle: axum_server::Handle, bus: Bus) {
    tracing::info!("Waiting for signal");
    let _ = shutdown_signal(bus).await;

    tracing::info!("Starting graceful shutdown");

    handle.graceful_shutdown(None);
}

pub async fn method_not_allowed(
    req: ::axum::extract::Request,
    next: axum::middleware::Next,
) -> impl IntoResponse {
    let resp = next.run(req).await;
    let status = resp.status();
    match status {
        StatusCode::METHOD_NOT_ALLOWED => {
            Err(RumaResponse(safareig_core::ruma::api::client::Error {
                status_code: StatusCode::METHOD_NOT_ALLOWED,
                body: ErrorBody::Standard {
                    kind: ErrorKind::Unrecognized,
                    message: "Unrecognized request".to_string(),
                },
            })
            .into_response())
        }
        _ => Ok(resp),
    }
}

fn setup_tracing(config: &HomeserverConfig) {
    let fmt_layer = ::tracing_subscriber::fmt::layer().with_target(true);
    let filter_layer = EnvFilter::try_from_default_env()
        .or_else(|_| EnvFilter::try_new("info"))
        .unwrap();

    let tracing_layer = if config.monitoring.opentelemetry_enabled {
        // Start a new jaeger trace pipeline
        global::set_text_map_propagator(opentelemetry_jaeger_propagator::Propagator::new());
        let tracer = opentelemetry_jaeger::new_agent_pipeline()
            .with_service_name(config.monitoring.service_name.clone())
            .with_trace_config(
                trace::config()
                    .with_resource(Resource::default())
                    .with_sampler(trace::Sampler::AlwaysOn),
            )
            .install_simple()
            .expect("jaeger pipeline is installable");

        Some(tracing_opentelemetry::layer().with_tracer(tracer))
    } else {
        None
    };

    #[cfg(feature = "debug")]
    {
        Registry::default()
            .with(fmt_layer)
            .with(filter_layer)
            .with(tracing_layer)
            .with(console_layer)
            .init();
    }
    #[cfg(not(feature = "debug"))]
    {
        Registry::default()
            .with(fmt_layer)
            .with(filter_layer)
            .with(tracing_layer)
            .init();
    }
}
