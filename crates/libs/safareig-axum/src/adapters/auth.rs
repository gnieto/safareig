use std::{
    num::NonZero,
    sync::{Arc, Mutex},
    time::{Duration, Instant},
};

use axum_ruma::{error::AuthError, ClientAuthorizer, ClientIdentity, FederationKeyProvider};
use lru::LruCache;
use safareig_appservices::Appservice;
use safareig_authentication::auth::{TokenError, TokenHandler};
use safareig_core::{
    config::HomeserverConfig,
    ruma::{serde::Base64, OwnedServerName, ServerName, ServerSigningKeyId, UserId},
    Inject,
};
use safareig_devices::storage::DeviceStorage;
use safareig_federation::keys::KeyProvider;

#[derive(Clone)]
pub struct SafareigClientAuthorizer {
    token: Option<Arc<dyn TokenHandler>>,
    appservices: Option<Appservice>,
    devices: Option<Arc<dyn DeviceStorage>>,
    cache: Arc<dyn AccessTokenCache>,
}

impl SafareigClientAuthorizer {
    pub fn new(
        token: Option<Arc<dyn TokenHandler>>,
        appservices: Option<Appservice>,
        devices: Option<Arc<dyn DeviceStorage>>,
        cache: Arc<dyn AccessTokenCache>,
    ) -> Self {
        Self {
            token,
            appservices,
            devices,
            cache,
        }
    }
}

#[async_trait::async_trait]
impl ClientAuthorizer for SafareigClientAuthorizer {
    async fn authorize(
        &self,
        token: &str,
        user: Option<&UserId>,
    ) -> Result<ClientIdentity, AuthError> {
        if let Some(id) = self.cache.cached_id(token) {
            return Ok(id);
        }

        if let Some(appservices) = self.appservices.as_ref() {
            if let Some(appservice) = appservices.name_for_token(token) {
                let id = ClientIdentity::AppService(appservice, user.map(|user| user.to_owned()));

                return Ok(id);
            }
        }

        let token_handler = match self.token.as_ref() {
            Some(h) => h,
            None => return Err(AuthError::TokenError("missing token handler".to_string())),
        };

        let auth_token = token_handler
            .parse_token(token)
            .await
            .map_err(|e| match e {
                TokenError::ExpiredToken => AuthError::ExpiredToken,
                _ => AuthError::TokenError(e.to_string()),
            })?;

        let user = auth_token.user_id().to_owned();
        let device = auth_token.device_id().to_owned();

        let device_storage = match self.devices.as_ref() {
            Some(d) => d,
            None => {
                return Err(AuthError::TokenError(
                    "missing token device storage".to_string(),
                ))
            }
        };

        let matches_with_current = device_storage
            .get_device(&user, &device)
            .await
            .map_err(|e| AuthError::TokenError(e.to_string()))?
            .map(|device| {
                device
                    .current_auth
                    .map(|auth| auth.as_str() == token)
                    .unwrap_or_default()
            })
            .unwrap_or_default();

        if !matches_with_current {
            return Err(AuthError::ExpiredToken);
        }

        let client = ClientIdentity::Device(user, device);

        self.cache.store_id(token, &client);

        Ok(client)
    }
}

pub trait AccessTokenCache: Send + Sync {
    fn cached_id(&self, token: &str) -> Option<ClientIdentity>;
    fn store_id(&self, token: &str, id: &ClientIdentity);
}

#[derive(Clone, Default)]
pub struct NoopCache;

impl AccessTokenCache for NoopCache {
    fn cached_id(&self, _token: &str) -> Option<ClientIdentity> {
        None
    }

    fn store_id(&self, _token: &str, _id: &ClientIdentity) {}
}

#[derive(Clone)]
pub struct LruAccessTokenCache {
    lru: Arc<Mutex<LruCache<String, CacheEntry>>>,
}

impl Default for LruAccessTokenCache {
    fn default() -> Self {
        Self {
            lru: Arc::new(Mutex::new(LruCache::new(NonZero::new(50).unwrap()))),
        }
    }
}

impl AccessTokenCache for LruAccessTokenCache {
    fn cached_id(&self, token: &str) -> Option<ClientIdentity> {
        let mut guard = self.lru.lock().unwrap();
        let entry = guard.get(token);

        match entry {
            Some(entry) => {
                let current_time = Instant::now();

                if entry.expiry >= current_time {
                    None
                } else {
                    Some(entry.value.clone())
                }
            }
            None => None,
        }
    }

    fn store_id(&self, token: &str, id: &ClientIdentity) {
        let mut guard = self.lru.lock().unwrap();
        let entry = CacheEntry {
            expiry: Instant::now()
                .checked_add(Duration::from_secs(3600))
                .unwrap(),
            value: id.clone(),
        };

        guard.put(token.to_string(), entry);
    }
}

#[derive(Clone, Inject)]
pub struct SafareigKeyProvider {
    config: Arc<HomeserverConfig>,
    key_provider: Arc<KeyProvider>,
}

#[async_trait::async_trait]
impl FederationKeyProvider for SafareigKeyProvider {
    fn server_name(&self) -> OwnedServerName {
        self.config.server_name.clone()
    }

    async fn public_key(
        &self,
        server: &ServerName,
        id: &ServerSigningKeyId,
    ) -> Result<Option<Base64>, AuthError> {
        let public_key = self.key_provider.load(server, id).await;

        Ok(public_key.map(|key| key.base64()))
    }
}

struct CacheEntry {
    expiry: Instant,
    value: ClientIdentity,
}
