use std::{any::Any, sync::Arc};

use axum::{
    extract::Extension,
    routing::{delete, get, post, put, MethodRouter},
    Router,
};
use axum_ruma::{RumaRequest, RumaResponse};
use http::{Method, StatusCode};
use safareig_core::{
    command::Command,
    ruma::api::{
        client::error::{ErrorBody, ErrorKind, RetryAfter},
        IncomingRequest, OutgoingResponse,
    },
};

use super::AxumIdentity;
use crate::{state::AppState, RateLimited};

pub(crate) async fn generic_endpoint<R, C>(
    Extension(app_state): Extension<Arc<AppState>>,
    request: RumaRequest<R>,
) -> Result<RumaResponse<R::OutgoingResponse>, RumaResponse<CommandError<R::EndpointError>>>
where
    R: IncomingRequest,
    C: Send
        + Sync
        + Any
        + Command<Input = R, Output = R::OutgoingResponse, Error = R::EndpointError>,
{
    let command = app_state.container.command::<C>().unwrap();

    if R::METADATA.rate_limited {
        if let Some(limiter) = &app_state.rate_limiter {
            let stable_path = R::METADATA
                .history
                .stable_paths()
                .next()
                .map(|(_, path)| path)
                .unwrap_or("");
            let limited = limiter.is_route_limited(stable_path);

            if let RateLimited::Yes(retry_after) = limited {
                let e = safareig_core::ruma::api::client::Error {
                    status_code: StatusCode::TOO_MANY_REQUESTS,
                    body: ErrorBody::Standard {
                        kind: ErrorKind::LimitExceeded {
                            retry_after: Some(RetryAfter::Delay(retry_after)),
                        },
                        message: "temporary limited".to_string(),
                    },
                };

                return Err(RumaResponse(CommandError::RateLimit(e)));
            }
        }
    }

    let (content, id) = request.take();
    let id = AxumIdentity(id);
    let command_result = command
        .execute(content, id.into())
        .await
        .map_err(CommandError::Error)?;
    let response = RumaResponse::<R::OutgoingResponse>(command_result);

    Ok(response)
}

trait SafareigRouterExt<S> {
    fn route_trailing(self, path: &str, method_router: MethodRouter<S>) -> Self
    where
        Self: Sized;
}

impl<S> SafareigRouterExt<S> for Router<S>
where
    S: Send + Clone + Sync + 'static,
{
    fn route_trailing(mut self, path: &str, method_router: MethodRouter<S>) -> Self {
        // Register duplciated routes until this issue with v0.6.0-RC1 gets clarified: https://github.com/tokio-rs/axum/issues/1347
        self = self.route(path, method_router.clone());
        self = self.route(&format!("{path}/"), method_router);
        self
    }
}

pub fn register_generic<C, R>(router_in: Router) -> Router
where
    R: IncomingRequest + Send + Sync + 'static,
    C: 'static
        + Send
        + Sync
        + Command<Input = R, Output = R::OutgoingResponse, Error = R::EndpointError>,
{
    let mut router = router_in;

    for i in 0..(C::OPT_PARAMS + 1) {
        let method = R::METADATA.method;

        for path in R::METADATA.history.all_paths() {
            router =
                match method {
                    Method::GET => router
                        .route_trailing(endpoint_trail(path, i), get(generic_endpoint::<R, C>)),
                    Method::PUT => router
                        .route_trailing(endpoint_trail(path, i), put(generic_endpoint::<R, C>)),
                    Method::POST => router
                        .route_trailing(endpoint_trail(path, i), post(generic_endpoint::<R, C>)),
                    Method::DELETE => router
                        .route_trailing(endpoint_trail(path, i), delete(generic_endpoint::<R, C>)),
                    _ => {
                        unreachable!()
                    }
                };
        }
    }

    router
}

fn endpoint_trail(endpoint: &str, trailing: u8) -> &str {
    if trailing == 0 {
        return endpoint;
    }

    let mut output = endpoint;

    for _ in 0..trailing {
        let latest = output.rfind('/').unwrap();
        output = &output[0..latest];
    }

    output
}

pub enum CommandError<T> {
    Error(T),
    RateLimit(safareig_core::ruma::api::client::Error),
}

impl<O: OutgoingResponse> OutgoingResponse for CommandError<O> {
    fn try_into_http_response<T: Default + bytes::BufMut>(
        self,
    ) -> Result<http::Response<T>, safareig_core::ruma::api::error::IntoHttpError> {
        match self {
            CommandError::Error(error) => error.try_into_http_response(),
            CommandError::RateLimit(error) => error.try_into_http_response(),
        }
    }
}
