use std::{future::Future, task::Poll, time::Instant};

use axum::extract::MatchedPath;
use opentelemetry::{
    metrics::{Counter, Histogram, Meter, Unit},
    Key,
};
use pin_project_lite::pin_project;
use tower_layer::Layer;
use tower_service::Service;

#[derive(Default)]
pub struct MetricsConfigOptions {
    meter: Option<Meter>,
}

impl MetricsConfigOptions {
    #[must_use]
    pub fn with_meter(mut self, meter: Meter) -> Self {
        self.meter = Some(meter);
        self
    }

    pub fn build(self) -> MetricsConfig {
        let meter = self
            .meter
            .unwrap_or_else(|| opentelemetry::global::meter("axum"));

        let http_counter = meter
            .u64_counter("http_requests_total")
            .with_description("Total amount of requests")
            .init();

        let http_duration = meter
            .f64_histogram("http_request_duration")
            .with_description("Histogram of time spent on requests")
            .with_unit(Unit::new("seconds"))
            .init();

        MetricsConfig {
            http_counter,
            http_duration,
        }
    }
}

const METHOD_KEY: Key = Key::from_static_str("method");
const ROUTE_KEY: Key = Key::from_static_str("route");
const STATUS_KEY: Key = Key::from_static_str("status");

#[derive(Clone)]
pub struct MetricsConfig {
    http_counter: Counter<u64>,
    http_duration: Histogram<f64>,
}

#[derive(Clone)]
pub struct OpentelemetryMetricsService<Svc> {
    inner: Svc,
    config: MetricsConfig,
}

impl<S> OpentelemetryMetricsService<S> {
    pub fn new(inner: S, config: MetricsConfig) -> Self {
        Self { inner, config }
    }
}

impl<S, ReqBody, ResBody> Service<http::Request<ReqBody>> for OpentelemetryMetricsService<S>
where
    S: Service<http::Request<ReqBody>, Response = http::Response<ResBody>>,
    ReqBody: http_body::Body,
    ResBody: http_body::Body,
{
    type Response = S::Response;

    type Error = S::Error;

    type Future = ResponseFuture<S::Future>;

    fn poll_ready(
        &mut self,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Result<(), Self::Error>> {
        self.inner.poll_ready(cx)
    }

    fn call(&mut self, req: http::Request<ReqBody>) -> Self::Future {
        let method = req.method().to_string();
        let route = req
            .extensions()
            .get::<MatchedPath>()
            .map(|path| path.as_str().to_owned());
        let start = Instant::now();

        let inner_call = self.inner.call(req);

        ResponseFuture {
            inner: inner_call,
            start,
            config: self.config.clone(),
            method,
            route,
        }
    }
}

pin_project! {
    pub struct ResponseFuture<F>
    {
        #[pin]
        inner: F,
        start: Instant,
        config: MetricsConfig,
        method: String,
        route: Option<String>,
    }
}

impl<F, ResBody, E> Future for ResponseFuture<F>
where
    F: Future<Output = Result<http::Response<ResBody>, E>>,
    ResBody: http_body::Body,
{
    type Output = Result<http::Response<ResBody>, E>;

    fn poll(
        self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Self::Output> {
        let this = self.project();
        let result = futures_util::ready!(this.inner.poll(cx));
        let latency = this.start.elapsed();

        match result {
            Ok(response) => {
                let labels = vec![
                    METHOD_KEY.string(this.method.to_string()),
                    ROUTE_KEY.string(this.route.clone().unwrap_or_default()),
                    STATUS_KEY.i64(response.status().as_u16() as i64),
                ];

                this.config.http_counter.add(1, &labels);

                this.config
                    .http_duration
                    .record(latency.as_secs_f64(), &labels);

                Poll::Ready(Ok(response))
            }
            Err(err) => Poll::Ready(Err(err)),
        }
    }
}

#[derive(Clone)]
pub struct OpentelemetryMetricsLayer {
    config: MetricsConfig,
}

impl OpentelemetryMetricsLayer {
    pub fn new(config: MetricsConfig) -> Self {
        Self { config }
    }
}

impl<S> Layer<S> for OpentelemetryMetricsLayer {
    type Service = OpentelemetryMetricsService<S>;

    fn layer(&self, inner: S) -> Self::Service {
        OpentelemetryMetricsService {
            inner,
            config: self.config.clone(),
        }
    }
}
