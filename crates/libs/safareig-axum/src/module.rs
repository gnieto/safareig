use std::sync::Arc;

use safareig_appservices::Appservice;
use safareig_authentication::auth::TokenHandler;
use safareig_core::{
    command::{Container, Module, Router, ServiceCollectionExtension},
    ddi::{ServiceCollectionExt, ServiceProvider, ServiceResolverExt},
};
use safareig_devices::storage::DeviceStorage;

use crate::{
    adapters::{AccessTokenCache, SafareigClientAuthorizer, SafareigKeyProvider},
    GovernorRateLimiter, RateLimiter,
};

#[derive(Default)]
pub struct AxumModule {}

#[async_trait::async_trait]
impl<R: Router> Module<R> for AxumModule {
    fn register(&self, container: &mut Container, router: &mut R) {
        #[cfg(not(feature = "sytest"))]
        {
            container
                .service_collection
                .service_factory(|sp: &ServiceProvider| {
                    let rate_limiter: Arc<dyn RateLimiter + Send + Sync> =
                        Arc::new(GovernorRateLimiter::default());

                    Ok(rate_limiter)
                });

            container
                .service_collection
                .service_factory(|sp: &ServiceProvider| {
                    let cache: Arc<dyn AccessTokenCache> =
                        Arc::new(crate::adapters::LruAccessTokenCache::default());

                    Ok(cache)
                });

            container
                .service_collection
                .register::<SafareigKeyProvider>();
            container
                .service_collection
                .service_factory(|sp: &ServiceProvider| {
                    Ok(SafareigClientAuthorizer::new(
                        sp.get::<Arc<dyn TokenHandler>>().ok().cloned(),
                        sp.get::<Appservice>().ok().cloned(),
                        sp.get::<Arc<dyn DeviceStorage>>().ok().cloned(),
                        sp.get::<Arc<dyn AccessTokenCache>>().cloned()?,
                    ))
                })
        }
    }
}
