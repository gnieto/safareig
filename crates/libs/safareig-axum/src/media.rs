use std::sync::Arc;

use axum::{
    extract::{Extension, Query},
    response::{IntoResponse, Response},
    routing::{get, post},
    Router,
};
use axum_extra::TypedHeader;
use axum_ruma::{Identity, RumaRequest, RumaResponse};
use bytes::Bytes;
use futures::Stream;
use futures_util::stream::TryStreamExt;
use safareig_core::{
    command::Command,
    ruma::api::{
        client::media::{
            create_content, get_content, get_content_as_filename, get_content_thumbnail,
        },
        IncomingRequest,
    },
};
use safareig_media::command::{
    CreateMediaCommand, CreateMediaCommandRequest, DownloadCommand, DownloadFilenameCommand,
    DownloadResponse, ThumbnailCommand,
};
use serde::Deserialize;
use tokio::io::AsyncRead;
use tokio_util::{
    codec::{BytesCodec, FramedRead},
    io::StreamReader,
};

use crate::{adapters::AxumIdentity, AppState};

// TODO: This should be moved somehow to MediaComponent to make eveything proeprly pluggable, but I do not
// know how to do that. Note that these endpoints has streaming semantics and uses APIs from the web framework
// which are hard to abstract.
pub fn media_router() -> Router {
    let mut router = Router::new();

    for path in create_content::v3::Request::METADATA.history.all_paths() {
        router = router.route(path, post(create_media));
    }

    for path in get_content_thumbnail::v3::Request::METADATA
        .history
        .all_paths()
    {
        router = router.route(path, get(thumbnail));
    }

    for path in get_content::v3::Request::METADATA.history.all_paths() {
        router = router.route(path, get(download));
    }

    for path in get_content_as_filename::v3::Request::METADATA
        .history
        .all_paths()
    {
        router = router.route(path, get(download_filename));
    }

    router
        .route("/_matrix/media/v3/thumbnail/:server_name", get(thumbnail))
        .route("/_matrix/media/r0/thumbnail/:server_name", get(thumbnail))
}

#[derive(Deserialize)]
pub struct CreateMediaRequest {
    filename: Option<String>,
}

async fn create_media(
    Extension(app_state): Extension<Arc<AppState>>,
    Query(params): Query<CreateMediaRequest>,
    TypedHeader(content_type): TypedHeader<axum_extra::headers::ContentType>,
    Extension(identity): Extension<Identity>,
    body: axum::body::Body,
) -> Result<
    RumaResponse<create_content::v3::Response>,
    RumaResponse<safareig_core::ruma::api::client::Error>,
> {
    let reader = StreamReader::new(
        body.into_data_stream()
            .map_err(|_| std::io::Error::new(std::io::ErrorKind::Other, "io error")),
    );

    let content_type = content_type.to_string();
    let split_at = std::cmp::min(content_type.len(), 50);
    let (content_type, _) = content_type.split_at(split_at);

    let request = CreateMediaCommandRequest {
        filename: params.filename,
        content_type: Some(content_type.to_string()),
        stream: Box::new(reader),
    };

    let identity = AxumIdentity(identity);
    let command = app_state.container.command::<CreateMediaCommand>().unwrap();

    Ok(RumaResponse(
        command.execute(request, identity.into()).await?,
    ))
}

async fn thumbnail(
    Extension(app_state): Extension<Arc<AppState>>,
    request: RumaRequest<get_content_thumbnail::v3::Request>,
) -> Result<DownloadWrapper, RumaResponse<safareig_core::ruma::api::client::Error>> {
    let (request, id) = request.take();
    let cmd = app_state.container.command::<ThumbnailCommand>().unwrap();
    let id = AxumIdentity(id);
    let response = cmd.execute(request, id.into()).await?;

    Ok(DownloadWrapper(response))
}

pub(crate) async fn download(
    Extension(app_state): Extension<Arc<AppState>>,
    request: RumaRequest<get_content::v3::Request>,
) -> Result<DownloadWrapper, RumaResponse<safareig_core::ruma::api::client::Error>> {
    let command = app_state.container.command::<DownloadCommand>().unwrap();
    let (request, id) = request.take();
    let id = AxumIdentity(id);

    download_content(command, request, id).await
}

pub(crate) async fn download_filename(
    Extension(app_state): Extension<Arc<AppState>>,
    request: RumaRequest<get_content_as_filename::v3::Request>,
) -> Result<DownloadWrapper, RumaResponse<safareig_core::ruma::api::client::Error>> {
    let command = app_state
        .container
        .command::<DownloadFilenameCommand>()
        .unwrap();

    let (request, id) = request.take();
    let id = AxumIdentity(id);
    let response = command.execute(request, id.into()).await?;

    Ok(DownloadWrapper(response))
}

async fn download_content(
    command: &DownloadCommand,
    request: get_content::v3::Request,
    identity: AxumIdentity,
) -> Result<DownloadWrapper, RumaResponse<safareig_core::ruma::api::client::Error>> {
    let response = command.execute(request, identity.into()).await?;

    Ok(DownloadWrapper(response))
}

pub struct DownloadWrapper(DownloadResponse);

impl IntoResponse for DownloadWrapper {
    fn into_response(self) -> axum::response::Response {
        let builder = Response::builder();

        builder
            .header(http::header::CONTENT_TYPE, self.0.content_type())
            .header(
                http::header::CONTENT_SECURITY_POLICY,
                self.0.content_security_policy(),
            )
            .header(
                http::header::CONTENT_DISPOSITION,
                self.0.content_disposition(),
            )
            .body(axum::body::Body::from_stream(into_bytes_stream(
                self.0.stream(),
            )))
            .unwrap()
    }
}

// See: https://stackoverflow.com/questions/59318460/what-is-the-best-way-to-convert-an-asyncread-to-a-trystream-of-bytes
fn into_bytes_stream<R>(r: R) -> impl Stream<Item = std::io::Result<Bytes>>
where
    R: AsyncRead,
{
    FramedRead::new(r, BytesCodec::new()).map_ok(|bytes| bytes.freeze())
}
