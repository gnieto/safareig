use std::{
    collections::BTreeMap,
    ops::Add,
    time::{Duration, SystemTime},
};

use async_trait::async_trait;
use safareig_core::ruma::{api::OutgoingResponse, ServerName};
use safareig_federation::{
    client::resolution::*,
    keys::{
        storage::{Key, KeyType},
        *,
    },
    FederationError,
};
use wiremock::ResponseTemplate;

pub struct HardcodedNameResolver {
    pub output: String,
}

#[async_trait]
impl ServerDiscovery for HardcodedNameResolver {
    async fn resolve(&self, _remote: &ServerName) -> Result<ServerNameResolution, FederationError> {
        Ok(ServerNameResolution::http(self.output.clone()))
    }
}

pub struct FailingKeyFetcher {}

#[async_trait]
impl KeyFetcher for FailingKeyFetcher {
    async fn fetch_keys(&self, _: &RequestedKeys) -> Result<FetchedKeys, FederationError> {
        Err(FederationError::Custom(
            "could not fetch keys from remote server".to_string(),
        ))
    }
}

pub struct HardcodedKeyFetcher {}

#[async_trait::async_trait]
impl KeyFetcher for HardcodedKeyFetcher {
    async fn fetch_keys(
        &self,
        requested_keys: &RequestedKeys,
    ) -> Result<FetchedKeys, FederationError> {
        let first_tuple = requested_keys.iter().next().unwrap();

        let first_key = first_tuple.1.iter().next().unwrap();

        let mut fetched_keys = FetchedKeys::new();
        let mut keys = BTreeMap::new();
        keys.insert(
            first_key.clone(),
            Key {
                content: vec![],
                key_type: KeyType::PublicKey,
                id: first_key.to_owned(),
                expire: None,
                valid_until_ts: Some(SystemTime::now().add(Duration::from_secs(3600))),
            },
        );
        fetched_keys.insert(first_tuple.0.to_owned(), keys);

        Ok(fetched_keys)
    }
}

pub fn response_template_from<T: OutgoingResponse>(response: T) -> ResponseTemplate {
    let response: http::Response<Vec<u8>> =
        response.try_into_http_response().map_err(|_| ()).unwrap();

    ResponseTemplate::new(http::StatusCode::OK).set_body_bytes(response.body().clone())
}
