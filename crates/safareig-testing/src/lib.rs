pub mod error;
pub mod federation;

use std::{
    any::{type_name, Any},
    collections::{BTreeMap, HashMap},
    net::SocketAddr,
    ops::Add,
    sync::{Arc, Mutex},
    time::{Duration, SystemTime},
};

use federation::HardcodedNameResolver;
use http::StatusCode;
use percent_encoding::percent_decode_str;
use rusty_ulid::Ulid;
use safareig::{
    client::room::{
        command::{
            create_room::CreateRoomCommand, invite_room::InviteRoomCommand,
            join_room::JoinByRoomOrAliasCommand, leave_room::LeaveRoomCommand,
            send_message::SendMessageCommand, send_state_event::SendStateEventCommand,
            state_event_for_key::StateEventForKeyCommand,
        },
        loader::RoomLoader,
        Room, RoomStream,
    },
    core::events::Event,
    federation::component::FederationComponent,
    services::{ClientComponent, ServerConfigModule},
    storage::{EventStorage, RoomTopologyStorage},
};
use safareig_appservices::module::AppserviceModule;
use safareig_authentication::{
    command::{login::LoginCommand, register::RegisterCommand},
    module::AuthenticationModule,
    PasswordHasher,
};
use safareig_client_config::{
    command::{SetAccountDataCommand, SetAccountRoomDataCommand},
    ClientConfigModule,
};
use safareig_core::{
    app::{App, AppOptions},
    auth::{FederationAuth, Identity},
    bus::BusMessage,
    command::{Command, Container, DummyRouter, FrozenContainer, Module, Router},
    component::CoreModule,
    config::*,
    ddi::{ServiceCollectionExt, ServiceProvider, ServiceResolverExt},
    pagination::{self, Pagination, Range},
    ruma::{
        api::client::{
            account::register::RegistrationKind,
            config::{set_global_account_data, set_room_account_data},
            keys::upload_keys,
            membership::{
                invite_user, invite_user::v3::InvitationRecipient, join_room_by_id_or_alias,
                leave_room,
            },
            message::send_message_event,
            profile::set_display_name,
            room::{
                create_room::{self, v3::RoomPreset},
                Visibility,
            },
            session::login::{self, v3::Password},
            state::{get_state_events_for_key, send_state_event},
            sync::sync_events::{self},
            uiaa::{self, AuthData, UiaaResponse},
            Error,
        },
        events::{
            room::{
                message::{MessageType, RoomMessageEventContent, TextMessageEventContent},
                power_levels::RoomPowerLevelsEventContent,
            },
            AnyMessageLikeEventContent, AnyStateEventContent, EventContent,
            GlobalAccountDataEventType, MessageLikeEventType, RoomAccountDataEventType,
            StateEventType, TimelineEventType,
        },
        exports::serde_json,
        serde::Raw,
        signatures::Ed25519KeyPair,
        KeyName, OwnedEventId, OwnedRoomId, OwnedServerName, RoomId, ServerName,
        ServerSigningKeyId, SigningKeyAlgorithm, TransactionId, UserId,
    },
    storage::StorageError,
};
use safareig_devices::DeviceModule;
use safareig_e2ee::{command::client::upload_keys::UploadKeysCommand, module::E2eeModule};
use safareig_ephemeral::{
    services::{EphemeralEvent, EphemeralStream},
    EphemeralModule,
};
use safareig_federation::{
    keys::storage::{Key, KeyType, ServerKeyStorage},
    ServerKeysModule,
};
use safareig_rooms::alias::module::AliasesModule;
use safareig_sync::{module::SyncModule, SyncCommand};
use safareig_typing::module::TypingModule;
use safareig_uiaa::{module::UiaaModule, UiaaWrapper};
use safareig_users::{module::UsersModule, profile::command::client::SetDisplaynameCommand};
use serde::{Deserialize, Serialize};
use tokio::sync::broadcast::Receiver;
use wiremock::MockServer;

lazy_static::lazy_static! {
    static ref SQLX_MIGRATED: Mutex<bool> = Mutex::new(false);
}

pub struct TestingApp {
    container: FrozenContainer,
}

impl TestingApp {
    pub async fn default() -> Self {
        Self::with_config(Self::default_config()).await
    }

    pub async fn with_config(config: HomeserverConfig) -> Self {
        Self::get_app(config, Vec::new()).await
    }

    pub async fn with_config_and_modules(
        config: HomeserverConfig,
        modules: Vec<Box<dyn Module<DummyRouter>>>,
    ) -> Self {
        Self::get_app(config, modules).await
    }

    pub fn default_config() -> HomeserverConfig {
        HomeserverConfig {
            server_name: "test.cat".parse().unwrap(),
            bind: "".to_string(),
            ssl_bind: "".to_string(),
            account: AccountConfig {
                account_data_max_size: 100,
            },
            federation: FederationConfig {
                skip_insecure: false,
                enabled: true,
                notary_servers: vec![],
                circuit_breaker_consecutive_failures: 10,
                rate_limit_quota_minute: 300,
            },
            ssl: SslConfig {
                enabled: false,
                cert_path: None,
                key_path: None,
            },
            appservices: HashMap::new(),
            database: Self::get_database_config(),
            uiaa: UiaaConfig {
                register_stages: vec![],
                login_stages: vec![],
                device_stages: vec![],
                shared_token: None,
                password_stages: vec![],
            },
            media: MediaConfig::default(),
            monitoring: MonitoringConfig::default(),
            content_reporting: Default::default(),
            admin: Default::default(),
            auth: Default::default(),
            server_side_search: ServerSideSearchConfig::Sqlite,
            rooms: Default::default(),
            #[cfg(feature = "msc2965")]
            oidc: OidcConfig {
                issuer: "http://localhost:1234".to_string(),
            },
        }
    }

    pub async fn with_mocked_federation() -> (Self, MockServer) {
        let server = MockServer::start().await;
        let server_keys = MockedServerKeysModule {
            server_keys: ServerKeysModule::default(),
            server: *server.address(),
        };
        let testing_app =
            Self::get_app(TestingApp::default_config(), vec![Box::new(server_keys)]).await;

        (testing_app, server)
    }

    pub async fn with_modules(modules: Vec<Box<dyn Module<DummyRouter>>>) -> Self {
        Self::get_app(TestingApp::default_config(), modules).await
    }

    async fn get_app(
        config: HomeserverConfig,
        modules: Vec<Box<dyn Module<DummyRouter>>>,
    ) -> TestingApp {
        let config = Arc::new(config);
        let router = DummyRouter {};
        let options = AppOptions {
            spawn_worker: None,
            run_migrations: true,
        };
        let mut app = App::new(router, options, config.clone())
            .with_module(Box::new(CoreModule::new(config.clone())))
            .with_module(Box::<EphemeralModule>::default())
            .with_module(Box::<ClientConfigModule>::default())
            .with_module(Box::<UiaaModule>::default())
            .with_module(Box::<DeviceModule>::default())
            .with_module(Box::<TypingModule>::default())
            .with_module(Box::<E2eeModule>::default())
            .with_module(Box::<ServerConfigModule>::default())
            .with_module(Box::<ClientComponent>::default())
            .with_module(Box::<ServerKeysModule>::default())
            .with_module(Box::<FederationComponent>::default())
            .with_module(Box::<AliasesModule>::default())
            .with_module(Box::<SyncModule>::default())
            .with_module(Box::<UsersModule>::default())
            .with_module(Box::<AppserviceModule>::default())
            .with_module(Box::new(AuthenticationTestingModule {
                auth_module: AuthenticationModule::default(),
            }));

        for module in modules {
            app = app.with_module(module);
        }

        let (container, _) = app.take().await;

        TestingApp { container }
    }

    fn get_database_config() -> DatabaseConfig {
        let db = std::env::var("SAFAREIG_TEST_DB").unwrap_or_else(|_| "sqlite".to_string());

        if db == "sqlite" {
            // Lazy static to initialize and run migrations?
            DatabaseConfig::Sqlite(SqliteOptions::testing())
        } else {
            unimplemented!("")
        }
    }

    pub fn server_name(&self) -> OwnedServerName {
        let config = self.container.service::<Arc<HomeserverConfig>>();

        config.server_name.clone()
    }

    pub fn new_identity() -> Identity {
        let device = "device".into();
        let user_id = format!(
            "@{}:test.cat",
            Ulid::generate().to_string().to_ascii_lowercase()
        )
        .parse()
        .unwrap();

        Identity::Device(device, user_id)
    }

    pub fn uiaa_password(user_id: &UserId) -> AuthData {
        let pass = uiaa::Password {
            identifier: uiaa::UserIdentifier::UserIdOrLocalpart(user_id.localpart().to_string()),
            password: "pw".to_string(),
            session: None,
        };
        AuthData::Password(pass)
    }

    pub fn command<C: Any + Send + Sync>(&self) -> &C {
        self.container
            .service_provider
            .get::<C>()
            .unwrap_or_else(|e| {
                panic!(
                    "Expected command {} to be registered. Error {}",
                    type_name::<C>(),
                    e
                )
            })
    }

    pub fn service<C: Any + Clone>(&self) -> C {
        self.container.service::<C>()
    }

    pub async fn register(&self) -> Identity {
        use safareig_core::ruma::api::client::account::register;

        let id = Self::new_identity();
        let cmd = self.command::<UiaaWrapper<RegisterCommand>>();

        let input = register::v3::Request {
            kind: RegistrationKind::User,
            password: Some("pwd".to_string()),
            username: Some(id.user().localpart().to_string()),
            device_id: None,
            initial_device_display_name: None,
            auth: None,
            inhibit_login: false,
            login_type: None,
            refresh_token: false,
        };
        let uiaa = cmd.execute(input, Identity::None).await.err().unwrap();
        let session = match uiaa {
            UiaaResponse::AuthResponse(uiaa) => uiaa.session,
            _ => None,
        };

        let auth_data = uiaa_auth_data(session, "m.login.dummy", BTreeMap::new());
        let input = register::v3::Request {
            kind: RegistrationKind::User,
            password: Some("pwd".to_string()),
            username: Some(id.user().localpart().to_string()),
            device_id: None,
            initial_device_display_name: None,
            auth: Some(auth_data),
            inhibit_login: false,
            login_type: None,
            refresh_token: false,
        };
        let response = cmd.execute(input, Identity::None).await.unwrap();

        Identity::Device(response.device_id.unwrap(), id.user().to_owned())
    }

    pub async fn register_appservice(&self, user_id: &UserId, appservice_id: &str) -> Identity {
        use safareig_core::ruma::api::client::account::register;

        let id = Identity::AppService(Some(user_id.to_owned()), appservice_id.to_string());
        let cmd = self.command::<UiaaWrapper<RegisterCommand>>();

        let input = register::v3::Request {
            kind: RegistrationKind::User,
            password: None,
            username: Some(user_id.localpart().to_string()),
            device_id: None,
            initial_device_display_name: None,
            auth: None,
            inhibit_login: false,
            login_type: Some(register::LoginType::ApplicationService),
            refresh_token: false,
        };
        let response = cmd.execute(input, id.clone()).await.unwrap();

        id
    }

    pub async fn login(
        &self,
        user: &UserId,
        password: &str,
    ) -> Result<login::v3::Response, safareig_core::ruma::api::client::Error> {
        let password = Password::new(
            uiaa::UserIdentifier::UserIdOrLocalpart(user.to_string()),
            password.to_string(),
        );

        let input = login::v3::Request {
            login_info: login::v3::LoginInfo::Password(password),
            device_id: None,
            initial_device_display_name: None,
            refresh_token: false,
        };
        let cmd = self.command::<LoginCommand>();
        cmd.execute(input, Identity::None).await
    }

    #[tracing::instrument(skip(self))]
    pub async fn public_room(&self) -> (Identity, Room) {
        let cmd = self
            .container
            .service_provider
            .get::<CreateRoomCommand>()
            .unwrap();
        let id = self.register().await;
        let room_name = "Room name".parse().unwrap();

        let input = create_room::v3::Request {
            creation_content: None,
            power_level_content_override: None,
            initial_state: vec![],
            invite: vec![],
            invite_3pid: vec![],
            is_direct: false,
            name: Some(room_name),
            preset: None,
            room_alias_name: Some(id.user().localpart().to_string()),
            room_version: None,
            topic: Some("Description".to_string()),
            visibility: Visibility::Public,
        };
        let output = cmd.execute(input, id.clone()).await.unwrap();

        let room_loader = self.container.service_provider.get::<RoomLoader>().unwrap();

        let room = room_loader.get(&output.room_id).await.unwrap().unwrap();

        (id, room)
    }

    #[tracing::instrument(skip(self))]
    pub async fn private_room(&self) -> (Identity, Room) {
        let cmd = self
            .container
            .service_provider
            .get::<CreateRoomCommand>()
            .unwrap();
        tracing::error!("Registering with cmd");
        let id = self.register().await;
        tracing::error!("!Registering");
        let room_name = "Room name".parse().unwrap();

        let input = create_room::v3::Request {
            creation_content: None,
            power_level_content_override: None,
            initial_state: vec![],
            invite: vec![],
            invite_3pid: vec![],
            is_direct: false,
            name: Some(room_name),
            preset: Some(RoomPreset::PrivateChat),
            room_alias_name: Some(id.user().localpart().to_string()),
            room_version: None,
            topic: Some("Description".to_string()),
            visibility: Visibility::Private,
        };
        let output = cmd.execute(input, id.clone()).await.unwrap();

        let room_loader = self.container.service_provider.get::<RoomLoader>().unwrap();

        let room = room_loader.get(&output.room_id).await.unwrap().unwrap();

        (id, room)
    }

    pub async fn invite_room(
        &self,
        room: &RoomId,
        inviter: &Identity,
        invitee: &Identity,
    ) -> Result<invite_user::v3::Response, Error> {
        let cmd = self.command::<InviteRoomCommand>();

        let input = invite_user::v3::Request {
            room_id: room.to_owned(),
            recipient: InvitationRecipient::UserId {
                user_id: invitee.user().to_owned(),
            },
            reason: None,
        };

        cmd.execute(input, inviter.clone()).await
    }

    pub async fn upload_keys(&self, id: &Identity) -> upload_keys::v3::Response {
        let input = upload_keys::v3::Request {
            device_keys: Default::default(),
            one_time_keys: Default::default(),
            fallback_keys: Default::default(),
        };
        let cmd = self.command::<UploadKeysCommand>();
        cmd.execute(input, id.to_owned()).await.unwrap()
    }

    pub async fn store_global_account_data(
        &self,
        id: &Identity,
        event_type: &str,
        data: Box<serde_json::value::RawValue>,
    ) -> Result<(), Error> {
        let input = set_global_account_data::v3::Request {
            user_id: id.user().to_owned(),
            event_type: GlobalAccountDataEventType::from(event_type),
            data: Raw::from_json(data),
        };
        let cmd = self.command::<SetAccountDataCommand>();
        cmd.execute(input, id.clone()).await?;

        Ok(())
    }

    pub async fn store_room_account_data(
        &self,
        id: &Identity,
        event_type: &str,
        data: Box<serde_json::value::RawValue>,
        room: &Room,
    ) -> Result<(), Error> {
        let input = set_room_account_data::v3::Request {
            user_id: id.user().to_owned(),
            event_type: RoomAccountDataEventType::from(event_type),
            data: Raw::from_json(data),
            room_id: room.id().to_owned(),
        };
        let cmd = self.command::<SetAccountRoomDataCommand>();
        cmd.execute(input, id.clone()).await?;

        Ok(())
    }

    pub async fn talk(
        &self,
        room: &RoomId,
        id: &Identity,
        msg: &str,
    ) -> Result<OwnedEventId, Error> {
        let cmd = self.command::<SendMessageCommand>();
        let content = AnyMessageLikeEventContent::RoomMessage(RoomMessageEventContent {
            msgtype: MessageType::Text(TextMessageEventContent {
                body: msg.to_string(),
                formatted: None,
            }),
            relates_to: None,
            mentions: None,
        });
        let (event_type, body) = event_content_to_raw(content);

        let input = send_message_event::v3::Request {
            room_id: room.to_owned(),
            txn_id: TransactionId::new(),
            event_type: MessageLikeEventType::from(event_type.to_string().as_str()),
            body,
            timestamp: None,
        };
        let response = cmd.execute(input, id.clone()).await?;

        Ok(response.event_id)
    }

    pub async fn send<T>(
        &self,
        room: &RoomId,
        id: &Identity,
        message: T,
    ) -> Result<OwnedEventId, Error>
    where
        T: EventContent,
        T::EventType: ToString,
    {
        let cmd = self.command::<SendMessageCommand>();
        let (event_type, body) = event_content_to_raw::<T>(message);

        let input = send_message_event::v3::Request {
            room_id: room.to_owned(),
            txn_id: TransactionId::new(),
            event_type: MessageLikeEventType::from(event_type.to_string().as_str()),
            body: body.cast(),
            timestamp: None,
        };
        let response = cmd.execute(input, id.clone()).await?;

        Ok(response.event_id)
    }

    pub async fn change_display_name(
        &self,
        id: &Identity,
        display_name: &str,
    ) -> Result<(), Error> {
        let cmd = self.command::<SetDisplaynameCommand>();

        let input = set_display_name::v3::Request {
            user_id: id.user().to_owned(),
            displayname: Some(display_name.to_string()),
        };
        cmd.execute(input, id.clone()).await?;

        Ok(())
    }

    pub fn latest_ephemeral(&self) -> Option<EphemeralEvent> {
        let ephemeral_stream = self.service::<EphemeralStream>();
        let current = ephemeral_stream.next();
        let range = (
            pagination::Boundary::Unlimited,
            pagination::Boundary::Inclusive(current),
        );

        let response = ephemeral_stream.events(Pagination::new(range.into(), ()));

        response.records().last().cloned()
    }

    pub async fn join_room(
        &self,
        room: OwnedRoomId,
        id: Identity,
    ) -> Result<join_room_by_id_or_alias::v3::Response, Error> {
        let cmd = self.command::<JoinByRoomOrAliasCommand>();

        let input = join_room_by_id_or_alias::v3::Request {
            room_id_or_alias: room.to_string().parse().unwrap(),
            server_name: Vec::new(),
            third_party_signed: None,
            reason: None,
        };

        cmd.execute(input, id).await
    }

    pub fn dummy_content() -> CustomEventContent {
        CustomEventContent {
            key: "value".to_string(),
        }
    }

    pub fn find_bus_message_containing<F: Fn(&BusMessage) -> bool>(
        mut rx: Receiver<BusMessage>,
        fun: F,
    ) -> Option<BusMessage> {
        while let Ok(msg) = rx.try_recv() {
            if fun(&msg) {
                return Some(msg);
            }
        }

        None
    }

    pub async fn find_event(&self, room_id: &RoomId, event_type: TimelineEventType) -> Vec<Event> {
        let room_stream = self.service::<RoomStream>();
        room_stream
            .stream_events(Range::all(), None)
            .await
            .unwrap()
            .records()
            .into_iter()
            .filter(|(_, e)| e.room_id == room_id && e.kind == event_type)
            .map(|(_, e)| e)
            .collect::<Vec<Event>>()
    }

    pub async fn register_with_profile(
        &self,
        displayname: Option<String>,
        _avatar: Option<String>,
    ) -> Identity {
        let id = self.register().await;

        if displayname.is_some() {
            let cmd = self.command::<SetDisplaynameCommand>();
            let input = set_display_name::v3::Request {
                user_id: id.user().to_owned(),
                displayname,
            };

            cmd.execute(input, id.clone()).await.unwrap();
        }

        id
    }

    pub async fn mutate_power_levels(
        &self,
        id: &Identity,
        room_id: &RoomId,
        mutator: impl Fn(RoomPowerLevelsEventContent) -> RoomPowerLevelsEventContent,
    ) {
        let cmd = self.command::<StateEventForKeyCommand>();
        let input = get_state_events_for_key::v3::Request {
            room_id: room_id.to_owned(),
            event_type: StateEventType::RoomPowerLevels,
            state_key: "".to_string(),
        };

        let current_state = cmd.execute(input, id.to_owned()).await.unwrap();

        let power_level = current_state
            .content
            .cast::<RoomPowerLevelsEventContent>()
            .deserialize()
            .unwrap();

        let new_power_level = mutator(power_level);

        let cmd = self.command::<SendStateEventCommand>();
        let input = send_state_event::v3::Request {
            room_id: room_id.to_owned(),
            event_type: StateEventType::RoomPowerLevels,
            state_key: "".to_string(),
            body: Raw::new(&AnyStateEventContent::RoomPowerLevels(new_power_level)).unwrap(),
            timestamp: None,
        };

        cmd.execute(input, id.to_owned()).await.unwrap();
    }

    pub fn federation_identity() -> Identity {
        let auth = FederationAuth {
            server: "localhost:8888".parse().unwrap(),
            key: "ed25519:key1".parse().unwrap(),
            signature: "".to_string(),
        };

        Identity::Federation(auth)
    }

    pub async fn generate_remote_key_for(&self, server_name: &ServerName) -> Ed25519KeyPair {
        let key_pair_content = Ed25519KeyPair::generate().unwrap();
        let version = "1".to_string();
        let key_pair = Ed25519KeyPair::from_der(&key_pair_content, version.clone()).unwrap();
        let name: Box<KeyName> = version.into();

        let key = Key {
            content: key_pair.public_key().to_vec(),
            id: ServerSigningKeyId::from_parts(SigningKeyAlgorithm::Ed25519, &name),
            expire: None,
            valid_until_ts: Some(SystemTime::now().add(std::time::Duration::from_secs(600))),
            key_type: KeyType::PublicKey,
        };

        let server_key_storage = self.container.service::<Arc<dyn ServerKeyStorage>>();
        server_key_storage
            .add_server_key(server_name, &key)
            .await
            .unwrap();

        key_pair
    }

    pub async fn leave_room(
        &self,
        room: OwnedRoomId,
        id: Identity,
    ) -> Result<leave_room::v3::Response, Error> {
        let cmd = self.command::<LeaveRoomCommand>();
        let input = leave_room::v3::Request {
            room_id: room,
            reason: None,
        };

        cmd.execute(input, id).await
    }

    pub async fn event_at_leaf(&self, room: &RoomId) -> Event {
        let leaves = self
            .container
            .service_provider
            .get::<Arc<dyn RoomTopologyStorage>>()
            .unwrap()
            .forward_extremities(room)
            .await
            .unwrap();
        let leaf = leaves.first().unwrap();

        self.container
            .service_provider
            .get::<Arc<dyn EventStorage>>()
            .unwrap()
            .get_event(leaf.event_id.event_id())
            .await
            .unwrap()
            .unwrap()
    }

    pub async fn sync(
        &self,
        id: &Identity,
        token: Option<String>,
        timeout: Option<Duration>,
    ) -> sync_events::v3::Response {
        let cmd = self.command::<SyncCommand>();

        let input = sync_events::v3::Request {
            filter: None,
            since: token,
            full_state: false,
            set_presence: Default::default(),
            timeout,
        };

        cmd.execute(input, id.clone()).await.unwrap()
    }
}

pub fn path_extractor(template: &str, uri: &str) -> Vec<String> {
    let mut path_params = Vec::new();
    let uri_parts: Vec<String> = uri.split('/').map(|p| p.to_string()).collect();
    for (i, component) in template.split('/').enumerate() {
        if component.starts_with(':') {
            if let Some(value) = uri_parts.get(i) {
                path_params.push(percent_decode_str(value).decode_utf8().unwrap().to_string());
            }
        }
    }

    path_params
}

pub fn generate_event_id() -> OwnedEventId {
    "$Rqnc-F-dvnEYJTyHq_iKxU2bZ1CI92-kuZq3a5lr5Zg"
        .parse()
        .unwrap()
}

pub fn event_content_to_raw<E>(event_content: E) -> (TimelineEventType, Raw<E>)
where
    E: EventContent,
    E::EventType: ToString,
{
    let event_type = TimelineEventType::from(event_content.event_type().to_string());
    let content_str = serde_json::to_string(&event_content).unwrap();

    (
        event_type,
        Raw::from_json(serde_json::value::RawValue::from_string(content_str).unwrap()),
    )
}

pub fn uiaa_auth_data(
    session: Option<String>,
    kind: &str,
    params: BTreeMap<String, serde_json::Value>,
) -> AuthData {
    let auth_data = serde_json::json!({
        "type": kind,
        "session": session,
        "auth_parameters": params,
    });
    let incoming: AuthData = serde_json::from_value(auth_data).unwrap();

    incoming
}

#[derive(Serialize, Deserialize)]
pub struct TestEventContent;

impl EventContent for TestEventContent {
    type EventType = &'static str;

    fn event_type(&self) -> Self::EventType {
        "safareig.test"
    }
}

#[derive(Serialize, Deserialize)]
pub struct CustomEventContent {
    pub key: String,
}

impl EventContent for CustomEventContent {
    type EventType = &'static str;

    fn event_type(&self) -> Self::EventType {
        "safareig.dummy"
    }
}

pub struct NoopHasher;

impl PasswordHasher for NoopHasher {
    fn hash(&self, password: &str) -> String {
        password.to_string()
    }

    fn check_password(&self, hash: &str, password: &str) -> bool {
        hash == password
    }
}

pub fn assert_not_found<V>(response: &Result<V, safareig_core::ruma::api::client::Error>) {
    assert!(response.is_err());
    assert_eq!(
        StatusCode::NOT_FOUND,
        response.as_ref().err().unwrap().status_code
    );
}

struct AuthenticationTestingModule {
    auth_module: AuthenticationModule,
}

#[async_trait::async_trait]
impl<R: Router> Module<R> for AuthenticationTestingModule {
    fn register(&self, container: &mut Container, router: &mut R) {
        Module::<R>::register(&self.auth_module, container, router);
        let hasher: Arc<dyn PasswordHasher + Send + Sync> = Arc::new(NoopHasher {});
        container.service_collection.service(hasher);
    }

    async fn run_migrations(&self, service_provider: &ServiceProvider) -> Result<(), StorageError> {
        Module::<R>::run_migrations(&self.auth_module, service_provider).await
    }
}

struct MockedServerKeysModule {
    pub server_keys: ServerKeysModule,
    server: SocketAddr,
}

use safareig_federation::{client::ClientBuilder, keys::LocalKeyProvider};

#[async_trait::async_trait]
impl<R: Router> Module<R> for MockedServerKeysModule {
    fn register(&self, container: &mut Container, router: &mut R) {
        Module::<R>::register(&self.server_keys, container, router);

        let output = self.server.to_string();
        container.service_collection.service_factory(
            |local: &Arc<LocalKeyProvider>, cfg: &Arc<HomeserverConfig>| {
                Ok(Arc::new(ClientBuilder::new(
                    local.clone(),
                    cfg.clone(),
                    Arc::new(HardcodedNameResolver { output }),
                )))
            },
        );
    }

    async fn run_migrations(&self, service_provider: &ServiceProvider) -> Result<(), StorageError> {
        Module::<R>::run_migrations(&self.server_keys, service_provider).await
    }
}
