use safareig_core::ruma::api::client::error::{ErrorBody, ErrorKind};

pub trait RumaErrorExtension {
    fn message(&self) -> String;
    fn kind(&self) -> ErrorKind;
}

impl RumaErrorExtension for safareig_core::ruma::api::client::Error {
    fn message(&self) -> String {
        match &self.body {
            ErrorBody::Standard { message, .. } => message.clone(),
            _ => "".to_string(),
        }
    }

    fn kind(&self) -> ErrorKind {
        match &self.body {
            ErrorBody::Standard { kind, .. } => kind.clone(),
            _ => ErrorKind::Unknown,
        }
    }
}
