use strict;
use warnings;

require SyTest::Homeserver::Safareig;

package SyTest::HomeserverFactory::Safareig;
use base qw( SyTest::HomeserverFactory );

sub _init
{
   my $self = shift;

   $self->{args} = {
      bindir => "/usr/local/bin",
   };

   $self->{impl} = "SyTest::Homeserver::Safareig";
   $self->SUPER::_init( @_ );
}

sub get_options
{
   my $self = shift;

   return (
      'd|safareig-binary-directory=s' => \$self->{args}{bindir},
      $self->SUPER::get_options(),
   );
}

sub print_usage
{
   print STDERR <<EOF
   -d, --safareig-binary-directory DIR  - path to the directory containing the
                                          safareig binaries
EOF
}

sub create_server
{
   my $self = shift;
   my %params = ( @_, %{ $self->{args}} );

   return $self->{impl}->new( %params );
}

1;