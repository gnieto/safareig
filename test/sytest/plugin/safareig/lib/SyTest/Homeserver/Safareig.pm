use strict;
use warnings;

use Future;

package SyTest::Homeserver::Safareig;
use base qw( SyTest::Homeserver SyTest::Homeserver::ProcessManager );

use Carp;
use POSIX qw( WIFEXITED WEXITSTATUS );

use SyTest::SSL qw( ensure_ssl_key create_ssl_cert );
use JSON::PP;
use Data::Dumper;

sub _init
{
   my $self = shift;
   my ( $args ) = @_;

   $self->{$_} = delete $args->{$_} for qw(
       bindir
   );

   defined $self->{bindir} or croak "Need a bindir";

   $self->{paths} = {};

   $self->SUPER::_init( $args );

   my $idx = $self->{hs_index};
   $self->{ports} = {
      bind_port                 => main::alloc_port( "bind_port[$idx]" ),
   };
}

sub start
{
   my $self = shift;

   my $hs_dir = $self->{hs_dir};
   my $output = $self->{output};

   # generate TLS key / cert
   # ...
   $self->{paths}{tls_cert} = "$hs_dir/server.crt";
   $self->{paths}{tls_cert_pem} = "$hs_dir/server.pem";
   $self->{paths}{tls_key} = "$hs_dir/server.key";
   $self->{paths}{tls_key_pkcs8} = "$hs_dir/pkcs8.key";
   $self->{paths}{matrix_key} = "$hs_dir/matrix_key.pem";

   ensure_ssl_key( $self->{paths}{tls_key} );
   create_ssl_cert( $self->{paths}{tls_cert}, $self->{paths}{tls_key}, $self->{bind_host} );

   system("openssl", "x509", "-in", $self->{paths}{tls_cert}, "-out", $self->{paths}{tls_cert_pem}, "-outform", "PEM");
   system "openssl pkcs8 -topk8 -inform PEM -outform PEM -nocrypt -in $self->{paths}{tls_key} -out $self->{paths}{tls_key_pkcs8}";

   return Future->done->then( 
      $self->_capture_weakself( '_start_safareig' )
   );
}

sub server_name
{
   my $self = shift;
   return $self->{bind_host} . ":" . $self->{ports}{bind_port};
}

sub federation_host
{
   my $self = shift;
   return $self->{bind_host};
}

sub federation_port
{
   my $self = shift;
   return $self->{ports}{bind_port};
}

sub secure_port
{
   my $self = shift;
   return $$self->{ports}{bind_port};
}

sub public_baseurl
{
    my $self = shift;
    return "https://$self->{bind_host}:" . $self->{ports}{bind_port};
}

sub write_config
{
    my $self = shift;
    my $hs_dir = $self->{hs_dir};

    my $default = "
    bind = \"localhost:8448\"
    ssl_bind = \"localhost:8448\"

    [account]
    account_data_max_size = 100

    [database]
    storage = \"Sqlite\"
    path = \"/sytest-data/\"

    [uiaa]
    register_stages = [\"m.login.dummy\"]
    login_stages = [\"m.login.password\"]

    [federation]
    enabled = false
    skip_insecure = false
    rate_limit_quota_minute = 3000000
    circuit_breaker_consecutive_failures = 100000

    [ssl]
    enabled = false

    [appservices]

    [media]
    max_upload_size = \"5M\"
    driver = \"Filesystem\"

    [server_side_search]
    driver = \"Sqlite\"

    [monitoring]
    opentelemetry_enabled = true";

    $self->write_file("Settings.toml", $default);
}

sub _start_safareig
{
   my $self = shift;

   my $output = $self->{output};
   my $loop = $self->loop;

   $output->diag( "Starting Safareig" );

   my $hs_dir = $self->{hs_dir};

   my @command = (
      $self->{bindir} . "/safareig-server > $hs_dir/log.txt 2> $hs_dir/stderr.txt",
   );

   $self->write_config();

   $output->diag( "Starting Safareig with: @command" );

   $self->_start_process_and_await_connectable(
      setup => [
         env => {
            RUST_LOG => "safareig-server=debug,safareig=debug,info",
            RUST_BACKTRACE => "full",
            SAFAREIG__BIND => "0.0.0.0:" . $self->{ports}{bind_port},
            SAFAREIG__SSL_BIND => "0.0.0.0:" . $self->{ports}{bind_port},
            SAFAREIG__SSL__ENABLED => "true",
            SAFAREIG__SSL__SKIP_INSECURE => "true",
            SAFAREIG__SSL__CERT_PATH => $self->{paths}{tls_cert_pem},
            SAFAREIG__SSL__KEY_PATH => $self->{paths}{tls_key_pkcs8},
            SAFAREIG__FEDERATION__ENABLED => "true",
            SAFAREIG__FEDERATION__SKIP_INSECURE => "true",
            SAFAREIG__DATABASE__STORAGE => "Sqlite",
            SAFAREIG__DATABASE__IN_MEMORY => "true",
            SAFAREIG__SERVER_NAME => "$self->{bind_host}:" . $self->{ports}{bind_port},
            SAFAREIG__MONITORING__SERVICE_NAME => "safareig_" . $self->{ports}{bind_port}, 
            SAFAREIG_CONFIG => $self->{hs_dir}."/Settings.toml",
            # RUST_LOG => "info",
         },
      ],
      command => [ @command ],
      connect_host => $self->{bind_host},
      connect_port => $self->{ports}{bind_port},
   )->else( sub {
      die "Unable to start Safareig: $_[0]\n";
   })->on_done( sub {
      $output->diag( "Started Safareig server" );
   });
}


1;