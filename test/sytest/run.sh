#!/bin/sh

set -ex

# mkdir -p /sytest
# git clone -q https://github.com/matrix-org/sytest.git /sytest
cd /sytest
# cd /sytest-data
#/safareig/x86_64-unknown-linux-musl/release/safareig-server
cp -R /safareig/sqlx  .


# /run-tests.pl tests/30rooms/30history-visibility.pl -B /sytest-data/denylist.txt -O tap -I Safareig -d /safareig/x86_64-unknown-linux-musl/debug
./run-tests.pl $1 --exclude-deprecated -B /sytest-data/denylist.txt -O tap -I Safareig  --work-directory /sytest-data/logs -d /safareig/x86_64-unknown-linux-musl/release --all
