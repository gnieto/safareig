#!/bin/bash
TMPFOLDER=$(mktemp -d)
docker-compose run --rm sytest | tee $TMPFOLDER/results.tap
cat $TMPFOLDER/results.tap | grep --text "^ok" | awk '{ $1=""; $2=""; print}' | sort > $TMPFOLDER/ok.txt

set -ex

diff $TMPFOLDER/ok.txt ./ok.txt