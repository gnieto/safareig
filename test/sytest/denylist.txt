# Safareig does not allow to register invalid usernames, but it does not downcases them (we do not do any other suggested mangling for non-ascii usernames)
POST /register downcases capitals in usernames

# This behaviour is not written in the spec and I'm not sure we need to enforce it.
# I do not understand why we should be rejecting this (in any case, this should be encoded on the auth rules).
Inbound /make_join rejects attempts to join rooms where all users have left

# I do not understand this test. The auth chain is missing the create room event, but it's present on the state, so it can be effectively be checked
# only with the state. With the returned state, all events can be authorized.
Outbound federation rejects send_join responses with no m.room.create event

# Temporary deny this test. This behaviour should be in the spec, otherwise could be confusing and I do not know
# how ignoring these events could affect the state resolution algorithm.
# Relevant links:
# https://github.com/matrix-org/synapse/issues/6978
# https://github.com/matrix-org/sytest/pull/821
Event with an invalid signature in the send_join response should not cause room join to fail

# There are several issues with this test. First, it's hard with serde to deserialize to a default value in case of failure (see https://users.rust-lang.org/t/solved-serde-deserialization-on-error-use-default-values/6681). This would imply that all `EventContent` fields which have a `Default` implementation should be annotated with this method in Ruma (maybe it can be derived somehow?).
# On the other hand, this test is not sending `origin` or  `origin_server_ts` as required in the spec (see https://github.com/matrix-org/matrix-doc/issues/1664)
Membership event with an invalid displayname in the send_join response should not cause room join to fail

# format query parameter is not in the spec.
# TODO: Open a spec issue to verify if it should be included on the spec?
GET /rooms/:room_id/state/m.room.member/:user_id?format=event fetches my membership event

# This test expects a lot of keys to be present on the powerlevel event. We are using Ruma,
# which prevents serializing fields which contains default values.
GET /rooms/:room_id/state/m.room.power_levels fetches powerlevels

# This test expects non-required fields to be persent, which is not required by the spec
GET /rooms/:room_id/joined_members fetches my membership

# The spec does not mention that body fields needs to be moved to event's content
POST /join/:room_id can join a room with custom content
POST /join/:room_alias can join a room with custom content

# Test is asserting expected keys which are not required by the spec
# (needs fixing on sytest)
GET /rooms/:room_id/state/m.room.power_levels can fetch levels
GET /capabilities is present and well formed for registered user

# This test expects that state events after the user left the room appears in the sync response.
Current state appears in timeline in private history with many messages after

# Current membership tracking does not allow us to support this use case
When user joins and leaves a room in the same batch, the full state is still included in the next sync

# This test belong to a non-stabilized MSC (MSC2228)
Ephemeral messages received from clients are correctly expired
Ephemeral messages received from servers are correctly expired

# This tests an internal behaviour of Synapse which is not encoded in the spec. It could be interesting to implement in the future.
Users with sufficient power-level can delete other's aliases

# Depends in https://gitlab.com/gnieto/safareig/-/issues/186
Can delete canonical alias

# Depends in decoupling safareig-rooms <-> safareig and recover validations in room list
Only room members can list aliases of a room

# The bad string in sytest is not a valid room version id (it contains `_`, but this is forbidden by the spec).
/upgrade to an unknown version is rejected

# Sytest does not send the timeout parameter
PUT /rooms/:room_id/typing/:user_id sets typing notification

# We do not support those features
Register with a recaptcha
Can register using an email address
Can login with 3pid and password using m.login.password
login types include SSO
Can login with new user via CAS
Can invite existing 3pid
Can invite existing 3pid with no ops into a private room
Can invite existing 3pid in createRoom
Can invite unbound 3pid
Can invite unbound 3pid over federation
Can invite unbound 3pid with no ops into a private room
Can invite unbound 3pid over federation with no ops into a private room
Can invite unbound 3pid over federation with users from both servers
Can accept unbound 3pid invite after inviter leaves
Can accept third party invite with /join
Guest users can join guest_access rooms
Guest users can send messages to guest_access rooms if joined
Guest users are kicked from guest_access rooms on revocation of guest_access
Guest user can set display names
Guest users are kicked from guest_access rooms on revocation of guest_access over federation
Guest user can upgrade to fully featured user
Guest user cannot upgrade other users
GET /publicRooms lists rooms
Guest users can accept invites to private rooms over federation
Guest users denied access over federation if guest access prohibited
m.room.history_visibility == "world_readable" allows/forbids appropriately for Guest users
m.room.history_visibility == "shared" allows/forbids appropriately for Guest users
m.room.history_visibility == "invited" allows/forbids appropriately for Guest users
m.room.history_visibility == "joined" allows/forbids appropriately for Guest users
m.room.history_visibility == "default" allows/forbids appropriately for Guest users
Guest non-joined users can get state for world_readable rooms
Guest non-joined users can get individual state for world_readable rooms
Guest non-joined users can get individual state for world_readable rooms after leaving
Guest non-joined users cannot send messages to guest_access rooms if not joined
Guest users can sync from world_readable guest_access rooms if joined
Guest users can sync from shared guest_access rooms if joined
Guest users can sync from invited guest_access rooms if joined
Guest users can sync from joined guest_access rooms if joined
Guest users can sync from default guest_access rooms if joined
m.room.history_visibility == "shared" allows/forbids appropriately for Real users
m.room.history_visibility == "invited" allows/forbids appropriately for Real users
m.room.history_visibility == "joined" allows/forbids appropriately for Real users
m.room.history_visibility == "default" allows/forbids appropriately for Real users
Real non-joined users can get individual state for world_readable rooms
Real users can sync from world_readable guest_access rooms if joined
Real users can sync from shared guest_access rooms if joined
Real users can sync from invited guest_access rooms if joined
Real users can sync from joined guest_access rooms if joined
Real users can sync from default guest_access rooms if joined
Only see history_visibility changes on boundaries
Backfill works correctly with history visibility set to joined
Only original members of the room can see messages from erased users
/event/ does not allow access to events before the user joined

# Safareig does not properly handle idempotence requests for registration yet
registration is idempotent, without username specified
registration is idempotent, with username specified
The operation must be consistent through an interactive authentication session

# Ruma is always sending device_id, even if it's null. This probably needs to be fixed there
registration with inhibit_login inhibits login

# Could not found any reference to the spec about this
Pushers created with a different access token are deleted on password change
Setting state twice is idempotent
Joining room twice is idempotent

# Sytest asserts more keys than needed 
Can't deactivate account with wrong password

# According to auth rules, the behaviour defined in the test is not correct: Creator should
# be allowed to kick banned user and left them in the "leave" state
Banned user is kicked and may not rejoin until unbanned
Remote banned user is kicked and may not rejoin until unbanned

# Safareig changes the power level on old rooms so no invintes and messages can be sent to the old room
# The test below assumes the power levels did not change, but this is not aligned with the spec, which suggest
# changing the power levels in the old rooms
/upgrade preserves the power level of the upgrading user in old and new rooms

# Sytest uses deprecated endpoint
Federation key API can act as a notary server via a GET request
GET /events initially
GET /initialSync initially
GET /rooms/:room_id/initialSync fetches initial sync state

# This endpoint uses an unspeced erase feature from synapse
Inbound federation redacts events from erased users

# Incoming invite v1 endpoint is not implemented (only supports up to room version v2)
Inbound federation can receive invites via v1 API