#!/bin/bash
set -x
ORIGINAL_FOLDER=$(pwd)
cd ../../
TMPFOLDER=$(mktemp -d)
cargo +stable build --bin safareig-server --features backend-sqlx,sytest,tls-rustls --release --target=x86_64-unknown-linux-musl
docker build -t safareig_complement -f test/complement/Dockerfile .

if ! [ -z "$1" ]
then
    cd $1
    git pull
else
    git clone https://github.com/matrix-org/complement.git $TMPFOLDER
    cd $TMPFOLDER
fi

COMPLEMENT_BASE_IMAGE=safareig_complement:latest go test -count=1 -v ./tests/... -json > $TMPFOLDER/test-results.json
cat $TMPFOLDER/test-results.json | jq -r 'select(.Action == "pass") | .Test' | sort > $TMPFOLDER/ok.txt
cat $TMPFOLDER/test-results.json | jq -r 'select(.Action == "fail") | .Test' | sort > $TMPFOLDER/fail.txt

set -ex
diff $ORIGINAL_FOLDER/ok.txt $TMPFOLDER/ok.txt
set +e
diff $ORIGINAL_FOLDER/fail.txt $TMPFOLDER/fail.txt