#!/bin/sh
set -ex

openssl genrsa -out key.pem 2048
openssl req -new -sha256 -key key.pem -subj "/C=CA/ST=CA/O=MyOrg, Inc./CN=$SERVER_NAME" -out $SERVER_NAME.csr
openssl x509 -req -in $SERVER_NAME.csr -CA /complement/ca/ca.crt -CAkey /complement/ca/ca.key -CAcreateserial -out server.pem -days 1 -sha256
envsubst < /etc/caddy/Caddyfile-template > /etc/caddy/Caddyfile
caddy start --config /etc/caddy/Caddyfile
SAFAREIG__SERVER_NAME=$SERVER_NAME ./safareig-server