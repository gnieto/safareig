#!/bin/sh
set -ex

export CAROOT=/tmp/cert
FILE=mkcert-v1.4.3-linux-amd64
if test -f "$FILE"; then
    echo "$FILE exists."
else
    wget https://github.com/FiloSottile/mkcert/releases/download/v1.4.3/mkcert-v1.4.3-linux-amd64
    chmod +x mkcert-v1.4.3-linux-amd64 
    ./mkcert-v1.4.3-linux-amd64 -install
fi

cd /srv/conduit
/usr/sbin/matrix-conduit