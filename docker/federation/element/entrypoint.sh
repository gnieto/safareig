#!/bin/sh
export DOCKER_HOST=$(/sbin/ip route|awk '/default/ { print $3 }')

envsubst < /app/config.template.json > /app/config.json
echo "Host ip: ${DOCKER_HOST}"


nginx -g "daemon off;"