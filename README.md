# Safareig

Safareig is a matrix homeserver written in Rust.

`Fer safareig` is a catalan expression that means "talk, chat, gossip".

## Configuration 

Copy `Settings.default.toml` to `Settings.toml` to use the default configuration. The file is commented explaining the default configuration.

By default, the homeserver uses a UIAA stage called `safareig.forbidden`, which prevents any user from registering to the homserver. You can either remove this stage from the list (to allow any registration to the homeserver) or change the stage to `m.login.registration_token` (which will require that registring users knows the content of the `shared_token` configuration property). Matrix clients which supports MSC3231 will have a specific behaviour in order to introduce the mentioned shared token. If the client does not support it, it will use a fallback stage.
 