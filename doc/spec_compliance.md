# Known issues 


## Direct messages

Direct messages are not working on, at least, Element. JS SDK has a known issue that seems that won't be fixed until there's a revamp of the direct messages via the canonical DMs.

- Tracking issue on element: https://github.com/vector-im/element-web/issues/14046
- Matrix MSC: https://github.com/matrix-org/matrix-doc/pull/2199
