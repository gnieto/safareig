CREATE TABLE IF NOT EXISTS media (
    media_id        STRING      NOT NULL PRIMARY KEY,
    filename        STRING,
    content_type    STRING    
);
