CREATE TABLE IF NOT EXISTS pushers_tmp (
    pushkey             STRING      NOT NULL PRIMARY KEY,
    user_id             STRING      NOT NULL,
    app_id              STRING      NOT NULL,
    app_display_name    STRING      NOT NULL,
    device_display_name STRING      NOT NULL,
    profile_tag         STRING,
    lang                STRING,
    pusher_data         JSON,

    FOREIGN KEY(user_id) REFERENCES users(id)
);

INSERT INTO pushers_tmp (user_id, pushkey, app_id, app_display_name, device_display_name, profile_tag, lang, pusher_data)
SELECT user_id, pushkey, app_id, app_display_name, device_display_name, profile_tag, lang, pusher_data FROM pushers;

DROP TABLE IF EXISTS pushers;
ALTER TABLE pushers_tmp RENAME TO pushers;