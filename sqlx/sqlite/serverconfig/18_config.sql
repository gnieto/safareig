CREATE TABLE IF NOT EXISTS config (
    key         STRING NOT NULL,
    value       STRING NOT NULL,

    PRIMARY KEY(key)
);
