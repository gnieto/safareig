CREATE TABLE IF NOT EXISTS account_data (
    user_id         STRING      NOT NULL,
    event_type      STRING      NOT NULL,
    room_id         STRING,
    event           JSON        NOT NULL,

    PRIMARY KEY(user_id, event_type, room_id),
    FOREIGN KEY(user_id) REFERENCES users(id)
);

CREATE TABLE IF NOT EXISTS account_data_stream (
    user_id         STRING      NOT NULL,
    event_type      STRING      NOT NULL,
    room_id         STRING,
    stream_token    NUMBER,

    FOREIGN KEY(user_id) REFERENCES users(id) 
);


CREATE UNIQUE INDEX IF NOT EXISTS accoundt_data_kv
    ON account_data (user_id, event_type, ifnull(`room_id`, ""));
