CREATE TABLE IF NOT EXISTS event_relations (
    parent              STRING,
    event_id            STRING,
    relation_type       STRING NOT NULL,
    topological_depth   NUMBER NOT NULL,
    topological_sub     NUMBER NOT NULL,
    stream_token        NUMBER NOT NULL,

    PRIMARY KEY (parent, event_id)
);

CREATE TABLE IF NOT EXISTS event_aggregations (
    event_id            STRING PRIMARY KEY,
    aggregation         JSON
);