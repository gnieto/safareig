CREATE TABLE IF NOT EXISTS server_keys (
    server_name         STRING      NOT NULL,
    key_id              STRING      NOT NULL,
    key_content         BLOB        NOT NULL,
    key_type            NUMBER      NOT NULL,
    expire              TIME,
    valid_until         TIME,

    PRIMARY KEY(server_name, key_id)
);

CREATE TABLE IF NOT EXISTS servers (
    server_name      STRING PRIMARY KEY,
    server_data      JSON   
);
