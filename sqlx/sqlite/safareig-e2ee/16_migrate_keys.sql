CREATE TABLE IF NOT EXISTS user_keys (
    user_id         STRING      NOT NULL,
    key_type        INTEGER      NOT NULL,
    key_blob        JSON        NOT NULL,

    PRIMARY KEY (user_id, key_type)
);

CREATE TABLE IF NOT EXISTS e2ee_update_stream (
    stream_token    INTEGER     PRIMARY KEY AUTOINCREMENT,
    user_id         STRING      NOT NULL,
    event_type      INTEGER     NOT NULL,
    insertion_time  TIME        NOT NULL
);

CREATE TABLE IF NOT EXISTS key_signatures (
    signing_user        STRING      NOT NULL,
    target_user         STIRNG      NOT NULL,
    signature           STRING      NOT NULL,
    key_id              STRING      NOT NULL,
    key_name            STRING      NOT NULL
);

CREATE TABLE IF NOT EXISTS server_side_backup (
    version             BLOB        NOT NULL,
    user_id             STRING      NOT NULL,
    auth_data           STRING,
    etag                STRING,
    
    PRIMARY KEY (user_id, version)
);

CREATE TABLE IF NOT EXISTS server_side_backup_key (
    version             BLOB       NOT NULL,
    user_id             STRING     NOT NULL,
    room_id             STRING     NOT NULL,
    session_id          STRING     NOT NULL,
    key_data            STRING     NOT NULL,

    FOREIGN KEY(version, user_id) REFERENCES server_side_backup(version, user_id)
);